MY_SUBPROJECT = qsqlmon
!include(go_to_top.pri) :error("cann't find go_to_top.pri")

#MY_BUILD_DIR = ../_build
#message(MY_BUILD_DIR: $$MY_BUILD_DIR)

TEMPLATE = app

QT += sql xml network
CONFIG +=     \
	warn_on     \
	qt     \
	thread   \
	uitools        \

DEFINES += QT

# tohle zajisti, aby pri exception backtrace nasel symboly z aplikace
unix:QMAKE_LFLAGS_SHAPP += -rdynamic

INCLUDEPATH = $$PWD/../libqf/include ./

TARGET = $$MY_SUBPROJECT
DESTDIR = $$MY_BUILD_DIR/bin
#message( "DESTDIR: $$DESTDIR")
#message( "TARGET: $$TARGET")

#!win32-g++: CONFIG += precompile_header
# Use Precompiled headers (PCH)
#PRECOMPILED_HEADER  = precompiled.h
precompile_header:!isEmpty(PRECOMPILED_HEADER) {
	message("using precompiled_headers")
}

DOLAR=$

LIBS +=     \
	-lqfgui$$QF_LIBRARY_DEBUG_EXT \
	-lqfcore$$QF_LIBRARY_DEBUG_EXT \

win32: LIBS += \
	-L$$MY_BUILD_DIR/bin \

unix: LIBS += \
	-L$$MY_BUILD_DIR/lib \
	-Wl,-rpath,\'$${DOLAR}$${DOLAR}ORIGIN/lib\' \
	--enable-new-dtags \

message(LIBS: $$LIBS)

FORMS +=     \
	centralwidget.ui     \
	servertreewidget.ui     \
	sqlwidget.ui     \
	dlgeditconnection.ui     \
	dlgaltertable.ui     \
	dlgcolumndef.ui     \
	dlgindexdef.ui     \

RESOURCES +=     \
	qsqlmon.qrc

RC_FILE = qsqlmon.rc

TRANSLATIONS    = qsqlmon_cz.ts

win32:CONFIG(debug, debug|release):CONFIG += console
console: message(CONSOLE)

HEADERS +=     \
	mainwindow.h     \
	sqldock.h     \
	servertreedock.h     \
	servertreeitem.h     \
	servertreemodel.h     \
	sqltextedit.h     \
	dlgeditconnection.h     \
	dlgaltertable.h     \
	dlgcolumndef.h     \
	dlgindexdef.h     \
	servertreeview.h    \
	theapp.h   \
	tableviewwidget.h  \

SOURCES +=     \
	main.cpp     \
	mainwindow.cpp     \
	sqldock.cpp     \
	servertreedock.cpp     \
	servertreeitem.cpp     \
	servertreemodel.cpp     \
	sqltextedit.cpp     \
	dlgeditconnection.cpp     \
	dlgaltertable.cpp     \
	dlgcolumndef.cpp     \
	dlgindexdef.cpp     \
	servertreeview.cpp    \
	theapp.cpp   \
	tableviewwidget.cpp  \

include(driver/driver.pri)

