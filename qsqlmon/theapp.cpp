
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "theapp.h"
#include "driver/qfhttpmysql/qfhttpmysql.h"

#include <qflogcust.h>
//======================================================
//                   InsrvdSqlDriverCreator
//======================================================
class QFHttpMySqlDriverCreator : public QSqlDriverCreatorBase
{
	public:
		virtual QSqlDriver * createObject () const {return new QFHttpMySqlDriver();}
};

//======================================================
//                   TheApp
//======================================================
SqlJournal TheApp::f_sqlJournal;

TheApp::TheApp(int & argc, char ** argv)
	: QFApplication(argc, argv)
{
	setApplicationName("qsqlmon");

	QSqlDatabase::registerSqlDriver("QFHTTPMYSQL", new QFHttpMySqlDriverCreator());

	QFXmlConfigSplittedFileLoader *ldr = new QFXmlConfigSplittedFileLoader(this);
	f_config.setConfigLoader(ldr);
}

TheApp::~TheApp()
{
	qfTrash() << QF_FUNC_NAME << "config()->dataDocument().isEmpty():" << config()->dataDocument().isEmpty();
	if(!config()->dataDocument().isEmpty()) config()->save();
}

TheApp* TheApp::instance()
{
	TheApp *a = qobject_cast<TheApp*>(QFApplication::instance());
	QF_ASSERT(a, "aplikace dosud neni inicializovana");
	return a;
}

QFXmlConfig* TheApp::config()
{
	qfTrash()  << QF_FUNC_NAME;
	QFXmlConfig *ret = QFApplication::config();
	return ret;
}

void TheApp::redirectLog()
{
	bool log_to_file = config()->value("/log", "0").toBool();
	bool redirected = false;
	if(log_to_file) {
		QString fn = config()->value("/log/file", "err.log").toString();
		FILE *f = fopen(qPrintable(fn), "wb");
		if(f) {
			redirected = true;
			QFLog::redirectDefaultLogFile(f);
		}
	}
	if(!redirected) QFLog::redirectDefaultLogFile();
}

QString TheApp::versionString() const
{
	static QString s = "1.3.0";
	return s;
}

SqlJournal * TheApp::sqlJournal()
{
	return &f_sqlJournal;
}
