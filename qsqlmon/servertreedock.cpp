#include "servertreedock.h"

#include <qaction.h>
#include <qevent.h>
#include <qframe.h>
#include <qmainwindow.h>
#include <qmenu.h>

ServerTreeDock::ServerTreeDock(QWidget *parent, Qt::WFlags flags)
    : QDockWidget(parent, flags)
{
    setObjectName("Connections tree");
    setWindowTitle(objectName());

    QWidget *w = new QWidget(this);
	ui.setupUi(w);
    setWidget(w);
}
