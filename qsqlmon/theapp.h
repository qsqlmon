
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef THEAPP_H
#define THEAPP_H

#include <qfsqlconnection.h>
#include <qfapplication.h>

#include <QWidget>

class SqlJournal : public QFSqlJournal
{
	protected:
		QStringList f_log;
	public:
		virtual void log(const QString &msg) {f_log << msg;}
		QStringList content() {return f_log;}
		void setContent(const QStringList &sl) {f_log = sl;}
};

//! TODO: write class documentation.
class  TheApp : public QFApplication
{
	Q_OBJECT;
	protected:
		static SqlJournal f_sqlJournal;
	public:
		SqlJournal* sqlJournal();
	public:
		virtual QFXmlConfig* config();
		/// nastavi logovani do souboru uvedeneho v configu nebo to stderr.
		void redirectLog();
		QString versionString() const;

		static TheApp* instance();
	public:
		TheApp(int & argc, char ** argv);
		virtual ~TheApp();
};

inline TheApp* theApp() {return TheApp::instance();}

#endif // THEAPP_H

