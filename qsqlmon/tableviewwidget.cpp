
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "tableviewwidget.h"

#include <qfaction.h>
#include <qfsqltableview.h>
#include <qfcsvimportdialogwidget.h>
#include <qfbuttondialog.h>
#include <qfdlgexception.h>

#include <QtUiTools>
#include <QHBoxLayout>
#include <QCheckBox>

#include <qflogcust.h>

//======================================================
//                        TableView
//======================================================
class  TableView : public QFSqlTableView
{
	protected:
		virtual void importCSV();
	public:
		TableView(QWidget *parent = NULL) : QFSqlTableView(parent) {}
};

void TableView::importCSV()
{
	qfLogFuncFrame();
	try {
		QFCSVImportDialogWidget *w = new QFCSVImportDialogWidget();
		{
			QUiLoader loader;
			QFile file(":/csvimportextraoptions.ui");
			file.open(QFile::ReadOnly);
			QWidget *w2 = loader.load(&file, w->extraOptionsFrame());
			file.close();
			
			QBoxLayout *ly = new QHBoxLayout(w->extraOptionsFrame());
			ly->setMargin(0);
			ly->addWidget(w2);
			w->extraOptionsFrame()->setVisible(true);
		}
		QFButtonDialog dlg;
		dlg.setXmlConfigPersistentId("QFTableView/importCSV/Dialog");
		dlg.setDialogWidget(w);
		QFCSVImportDialogWidget::ColumnMappingList lst;
		QFBasicTable *t = table();
		{
			foreach(const QFSqlField &fld, t->fields()) {
				lst << QFCSVImportDialogWidget::ColumnMapping(fld.fullName());
			}
		}
		w->setColumnMapping(lst);
		if(dlg.exec()) {
			bool load_blobs = false;
			{
				QCheckBox *cbx = w->extraOptionsFrame()->findChild<QCheckBox*>("chkLoadBlobs");
				load_blobs = cbx->isChecked();
			}
			QFBasicTable *t2 = w->table();
			lst = w->columnMapping();
			foreach(QFBasicTable::Row r2, t2->rows()) {
					//qfInfo() << r.toString();
				QFBasicTable::Row r = t->appendRow();
				foreach(const QFCSVImportDialogWidget::ColumnMapping& cm, lst) {
					int colno = cm.columnIndex();
					if(colno >= 0) {
						QString colname = cm.columnName();
						QVariant val = r2.value(colno);
						if(load_blobs) {
							if(t->field(colname).type() == QVariant::ByteArray) {
								QString file_name = val.toString();
								QFile f(file_name);
								if(f.open(QFile::ReadOnly)) {
									val = f.readAll();
								}
							}
						}
						r.setValue(cm.columnName(), val);
					}
				}
				r.post();
			}
			reload();
		}
	}
	catch(QFException &e) {
		QFDlgException::exec(e);
	}
}

//======================================================
//                        TableViewWidget
//======================================================
class TableViewFactory1 : public QFTableViewWidget::TableViewFactory
{
	public:
		virtual QFTableView* createView() const {
			//qfInfo() << "tttttttttttttttttttttttttttt";
			return new TableView();
		}
};

//======================================================
//                        TableViewWidget
//======================================================
TableViewWidget::TableViewWidget(QWidget *parent) 
	: QFTableViewWidget(parent, TableViewFactory1())
{
	//qfInfo() << "aaaaaaaaaaaaaaaaaa";
	setToolBarVisible(true);
	tableView()->setContextMenuActions(tableView()->contextMenuActionsForGroups(QFTableView::AllActions));
	tableView()->setCopyRowActionVisible(true);
}



