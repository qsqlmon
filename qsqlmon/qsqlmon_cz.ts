<!DOCTYPE TS><TS>
<context>
    <name>CentralWidget</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>rows</source>
        <translation type="unfinished">radku</translation>
    </message>
    <message>
        <source>info: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>RW</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Database</name>
    <message>
        <source>Error creating SQL connection %1</source>
        <translation>Chyba pri vytvareni pripojeni k SQL serveru %1</translation>
    </message>
    <message>
        <source>Error opening database %1</source>
        <translation>Chyba pri otevirani databaze %1</translation>
    </message>
</context>
<context>
    <name>DlgEditConnection</name>
    <message>
        <source>Connection</source>
        <translation>Pripojeni</translation>
    </message>
    <message>
        <source>Driver</source>
        <translation>Ovladac</translation>
    </message>
    <message>
        <source>Database</source>
        <translation>Database</translation>
    </message>
    <message>
        <source>Caption</source>
        <translation>Nazev</translation>
    </message>
    <message>
        <source>User</source>
        <translation>Uzivatel</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Zrusit</translation>
    </message>
</context>
<context>
    <name>DlgXmlConfig</name>
    <message>
        <source>XML Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Action &apos;%1&apos; not found !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>QT SQL Monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About Application</source>
        <translation type="unfinished">O aplikaci</translation>
    </message>
    <message>
        <source>The &lt;b&gt;QMotorek&lt;/b&gt; example demonstrates how to write modern GUI interface to a remote controled system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open the aplication config window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exit the application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Execute SQL command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show the application&apos;s About box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add database connection to the tree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Sql</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sql</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>not connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No active connection !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Server menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connection menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete connection ?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>DlgEditConnection::on_btOk_clicked() - currConnection is NULL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerTreeModel</name>
    <message>
        <source>Description</source>
        <translation type="unfinished">Popis</translation>
    </message>
    <message>
        <source>Database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Driver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerTreeWidget</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SqlWidget</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sql</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
