#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMap>

#include <qfmainwindow.h>
#include <qfsqlconnection.h>
//#include <qfsqlquerymodel.h>

#include "ui_centralwidget.h"

class ServerTreeModel;
class QAction;
class QLabel;
class QMenu;
class QTextEdit;
//class QUdpSocket;
class QModelIndex;
class ServerTreeDock;
class SqlDock;
class QFXmlConfigDocument;
class Connection;
class QFSqlQueryTableModel;

class MainWindow : public QFMainWindow
{
    Q_OBJECT

	public:
		MainWindow();
		~MainWindow();

	protected:
	//bool event(QEvent *event);
		virtual void closeEvent(QCloseEvent *e);
	//virtual void showEvent(QShowEvent * e);
		virtual void changeEvent(QEvent * e);
	//virtual void focusInEvent(QFocusEvent *e);

		void executeSqlLines(const QString &lines);

		void setStatusText(const QString& s, int where = 0);
	public slots:
		void appendInfo(const QString &s);
		void setProgressValue(double val, const  QString &label_text = QString());
	private slots:
		void treeNodeCollapsed(const QModelIndex &index);
		void treeNodeExpanded(const QModelIndex &index);
		void treeNodeDoubleClicked(const QModelIndex &index);
		void treeServersContextMenuRequest(const QPoint&);

		void executeSql();
		void executeSelectedLines();
		void wordWrapSqlEditor(bool checked);
		void executeSqlScript();
		void showSqlJournal();
		void configure();
		void about();
		void changeLog();
		void mysqlSyntax();
		void sqliteSyntax();
		void availableDrivers();
		void checkDrivers();
		void tearOffTable();

		void addServer(Connection *connection_to_copy = NULL);
		void setDbSearchPath(const QString &path);

		void tableStatusBarTextAction(const QString &text);

		void lazyInit();
	private:
		Ui::CentralWidget ui;

		ServerTreeDock *serverDock;
		SqlDock *sqlDock;

	private:
		void createActions();
		void createMenus();
		void createToolBars();
		void createStatusBar();
		void createDockWindows();
		void init();

		QMenu *menuFile;
		QMenu *menuHelp;
		QMenu *menuView;

		QMap<QString, QAction*> actionMap;

		QList<QPointer<QDialog> > tearOffDialogs;

		QFSqlQueryTableModel* queryModel(bool throw_exc = true) const throw(QFException);
		void setQueryModel(QFSqlQueryTableModel *m);
    //QList<QWidget*> statusBarWidgets;
	private:
		QAction* action(const QString& action_name, bool throw_exc = true) {
			QAction *a = actionMap.value(action_name);
			if(!a && throw_exc) QF_EXCEPTION(tr("Action '%1' not found !").arg(action_name));
			return a;
		}

	/// database z niz se provadi dotazy
		QFSqlConnection m_activeConnection;
	/// \return previously active connection
		QFSqlConnection setActiveConnection(QFSqlConnection c);

		bool execQuery(const QString& query_str);
	public:
	/**
	 *  throw exception if activeConnection is not valid.
	 */
		QFSqlConnection activeConnection(bool throw_exc = true) throw(QFException) {
			if(!m_activeConnection.isValid() && throw_exc) QF_EXCEPTION("Connection is not valid !");
			return m_activeConnection;
		}
		bool execCommand(const QString& query_str);
};

#endif
