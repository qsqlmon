MY_SUBPROJECT = fibase
!include(go_to_top.pri) :error("cann't find go_to_top.pri")

#TEMPLATE = lib 

TARGET	 = qfsqlibase
TARGET = $${TARGET}$$QF_LIBRARY_DEBUG_EXT

DEFINES += QF_PATCH
unix:IBASE_DIR = /opt/firebird # fanda QF_PATCH
win32:IBASE_DIR = c:/app/Firebird_1_5 # fanda QF_PATCH

HEADERS		= \
	qfsql_ibase.h \
	
SOURCES		= \
	main.cpp \
	qfsql_ibase.cpp

unix:INCLUDEPATH += $$IBASE_DIR/include
win32:INCLUDEPATH += $$IBASE_DIR/include 

#unix:!contains( LIBS, .*gds.* ):!contains( LIBS, .*libfb.* ):LIBS    *= -lgds
unix: { 
	LIBS += -L$$IBASE_DIR/lib -lfbclient 
	#LIBS += -L$$IBASE_DIR/lib -lfbembed 
} 

# tohle funguje, ale zase nevim proc, libgds je nazev client knihovny pro interbazi
#unix:!contains( LIBS, .*gds.* ):!contains( LIBS, .*libfb.* ):LIBS    *= -lgds

win32:!contains( LIBS, .*gds.* ):!contains( LIBS, .*fbclient.* ) {
	#!win32-borland:LIBS *= -lgds32_ms
	#win32-borland:LIBS  += gds32.lib
    #LIBS += -L./ -lfbclient 
	LIBS += -L$$IBASE_DIR/lib -lfbclient_ms 
}

include(../qsqldriverbase.pri)

###########################################
#staci to slinkovat s lib/fbclient_ms.lib
###########################################
