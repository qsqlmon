#CONFIG += qmake_debug
MY_SUBPROJECT = plugins/sqldrivers/fpsql
!include(go_to_top.pri) :error("cann't find go_to_top.pri")

TARGET	 = qfpsql
TARGET = $${TARGET}$$QF_LIBRARY_DEBUG_EXT

DEFINES += QF_PATCH
unix:PSQL_DIR = /usr/include/postgresql # fanda QF_PATCH
win32:PSQL_DIR = c:/src/postgresql-8.3.7 # fanda QF_PATCH

HEADERS		= \
	qfsql_psql.h \
	
SOURCES		= \
	main.cpp \
	qfsql_psql.cpp \

INCLUDEPATH += \
	$$MY_PATH_TO_TOP/include \

unix {
	INCLUDEPATH += \
		$$PSQL_DIR

	!contains( LIBS, .*pq.* ) {
	    LIBS	*= -lpq 
	}
}

win32 {
	INCLUDEPATH += \
		$$PSQL_DIR/src/interfaces/libpq \
		$$PSQL_DIR/src/include \

	!contains( LIBS, .*pq.* ) {
	    LIBS	*= -lpq -L$$PSQL_DIR/src/interfaces/libpq
	}
}

include(../qsqldriverbase.pri)
