MY_SUBPROJECT = qfsqlite
#CONFIG += qmake_debug
!exists(go_to_top.pri): error("can't find go_to_top.pri")
include(go_to_top.pri)

TARGET	 = $$MY_SUBPROJECT
TARGET = $${TARGET}$$QF_LIBRARY_DEBUG_EXT

HEADERS = \
	qsql_sqlite.h \

SOURCES	= \
	main.cpp \
	qsql_sqlite.cpp \

DEFINES += QF_PATCH

#message(INC $$QMAKE_INCDIR_QT)
#message(LIB $$QMAKE_LIBDIR_QT)

SQLITE_HOME = $$QMAKE_INCDIR_QT/../src/3rdparty/sqlite

message(SQLITE_HOME: $$SQLITE_HOME)

INCLUDEPATH += $$SQLITE_HOME
SOURCES += $$SQLITE_HOME/sqlite3.c

#unix {
#	SQLITE_HOME = /Programs/Sqlite/Current
#	LIBS += \
#		-lsqlite3 -L$$SQLITE_HOME/lib \
#		-lqsqlite_debug -L/Users/fanda/Programs/Qt4/Current/plugins/sqldrivers \
#}
#win32:LIBS += \
#	$$MY_BUILD_DIR/lib/libsqlite3.lib \
#	$$QT_BUILD_TREE/plugins/designer/libqfsqlqueryviewwidget.lib


include(../qsqldriverbase.pri)
