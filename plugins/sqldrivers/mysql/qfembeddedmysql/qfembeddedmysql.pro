MY_SUBPROJECT = plugins/sqldrivers/qfsqlembeddedmysql
#!exists(go_to_top.pri) :error("cann't find go_to_top.pri")
!include(go_to_top.pri) :error("cann't find go_to_top.pri")

TARGET	 = qfsqlembeddedmysql

CONFIG += qf-embedded-mysql
DEFINES += QF_EMBEDDED_MYSQL

include(../driver/qfmysql.pri)
