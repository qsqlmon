/****************************************************************************
**
** Copyright (C) 1992-2005 Trolltech AS. All rights reserved.
**
** This file is part of the plugins of the Qt Toolkit.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.trolltech.com/products/qt/opensource.html
**
** If you are unsure which license is appropriate for your use, please
** review the following information:
** http://www.trolltech.com/products/qt/licensing.html or contact the
** sales department at sales@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include <qsqldriverplugin.h>
#include <qstringlist.h>
#include "../driver/qfsql_mysql.h"

class QFMYSQLDriverPlugin : public QSqlDriverPlugin
{
public:
    QFMYSQLDriverPlugin();

    QSqlDriver* create(const QString &);
    QStringList keys() const;
};

QFMYSQLDriverPlugin::QFMYSQLDriverPlugin()
    : QSqlDriverPlugin()
{
}

QSqlDriver* QFMYSQLDriverPlugin::create(const QString &name)
{
    if (name == QLatin1String("QFMYSQL") || name == QLatin1String("QFMYSQL3")) {
        QMYSQLDriver* driver = new QMYSQLDriver();
        return driver;
    }
    return 0;
}

QStringList QFMYSQLDriverPlugin::keys() const
{
    QStringList l;
    l << QLatin1String("QFMYSQL3");
    l << QLatin1String("QFMYSQL");
    return l;
}

Q_EXPORT_STATIC_PLUGIN(QFMYSQLDriverPlugin)
Q_EXPORT_PLUGIN2(qfsqlmysql, QFMYSQLDriverPlugin)
