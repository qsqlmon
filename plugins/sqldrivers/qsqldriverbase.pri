include(../qfpluginbase.pri)
QT  = core sql
DESTDIR  = $$MY_BUILD_DIR/bin/sqldrivers

target.path     += $$MY_BUILD_DIR/bin/plugins/sqldrivers
#target.path     += $$[QT_INSTALL_PLUGINS]/sqldrivers
INSTALLS        += target

DEFINES += QT_NO_CAST_TO_ASCII QT_NO_CAST_FROM_ASCII
