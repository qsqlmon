//
// C++ Interface: sadglobals
//
// Description:
//
//
// Author: Fanda Vacek <fanda@localhost>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef QFPLUGINGLOBAL_H
#define QFPLUGINGLOBAL_H

#include <qglobal.h>

#if defined(QF_PLUGIN_BUILD_DLL)
#  define QF_PLUGIN_DECL_EXPORT Q_DECL_EXPORT
#else
#  define QF_PLUGIN_DECL_EXPORT Q_DECL_IMPORT
#endif

#endif // QFPLUGINGLOBAL_H
