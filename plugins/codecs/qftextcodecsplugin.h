#ifndef QFTEXTCODECSPLUGIN_H
#define QFTEXTCODECSPLUGIN_H

#include "../qfpluginglobal.h"

#include <QTextCodecPlugin>

class QF_PLUGIN_DECL_EXPORT QFTextCodecsPlugin : public QTextCodecPlugin
{
	Q_OBJECT;
	public:
		virtual QList<QByteArray> aliases() const;
		virtual QTextCodec* createForMib(int mib);
		virtual QTextCodec* createForName(const QByteArray &name);
		virtual QList<int> mibEnums() const;
		virtual QList<QByteArray> names() const;
	public:
		QFTextCodecsPlugin(QObject *parent = 0) : QTextCodecPlugin(parent) {}
		//~QTextCodecPlugin ()
};

#endif // QFTEXTCODECSPLUGIN_H
