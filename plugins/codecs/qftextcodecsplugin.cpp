#include "qftextcodecsplugin.h"
#include "ibm852/ibm852codec.h"
#include "fascii7/fascii7codec.h"

QList<QByteArray> QFTextCodecsPlugin::aliases() const
{
	static QList<QByteArray> ret;
	if(ret.isEmpty()) {
		ret << QByteArray("IBM852") << QByteArray("CP852") << QByteArray("852") << QByteArray("csPCp852");
		ret << QByteArray("ASCII") << QByteArray("ASCII7") << QByteArray("csASCII");
	}
	return ret;
}

QTextCodec* QFTextCodecsPlugin::createForMib(int mib)
{
	QTextCodec *ret = NULL;
	if(mib == 2010) ret = new QFIBM852Codec();
	else if(mib == 3) ret = new QFAscii7Codec();
	return ret;
}

QTextCodec* QFTextCodecsPlugin::createForName(const QByteArray &name)
{
	QTextCodec *ret = NULL;
	int mib = 0;
	QByteArray ba = name.toUpper();
	foreach(QByteArray ba1, aliases()) {
		if(ba1.toUpper() == ba) {
			if(ba.indexOf("ASCII") >= 0) mib = 3;
			else mib = 2010;
			break;
		}
	}
	ret = createForMib(mib);
	return ret;
}

QList<int> QFTextCodecsPlugin::mibEnums() const
{
	static QList<int> ret;
	if(ret.isEmpty()) {
		ret << 2010;
		ret << 3;
	}
	return ret;
}

QList<QByteArray> QFTextCodecsPlugin::names() const
{
	static QList<QByteArray> ret;
	if(ret.isEmpty()) {
		ret << QByteArray("IBM852");
		ret << QByteArray("ASCII7");
	}
	return ret;
}

Q_EXPORT_PLUGIN(QFTextCodecsPlugin)
