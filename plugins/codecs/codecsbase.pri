include(../qfpluginbase.pri)
QT  = core
DESTDIR  = $$MY_BUILD_DIR/bin/codecs

target.path     += $$MY_BUILD_DIR/bin/plugins/codecs
#target.path     += $$[QT_INSTALL_PLUGINS]/codecs
INSTALLS        += target

DEFINES += QT_NO_CAST_TO_ASCII QT_NO_CAST_FROM_ASCII
