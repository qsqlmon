
#ifndef QF_ASCII7_H
#define QF_ASCII7_H

#include <QTextCodec>
#include <QString>

class QFAscii7Codec : public QTextCodec
{
public:
    //~QFIBM852Codec();

    QByteArray name() const;
    int mibEnum() const;

    QString convertToUnicode(const char *, int, ConverterState *) const;
    QByteArray convertFromUnicode(const QChar *, int, ConverterState *) const;
};

#endif // QF_ASCII7_H
