#include "fascii7codec.h"

#include <QHash>

static unsigned char qf_UnicodeToAscii7(ushort u);
static ushort qf_Ascii7ToUnicode(uchar code);

QByteArray QFAscii7Codec::convertFromUnicode(const QChar *uc, int len, ConverterState *state) const
{
	Q_UNUSED(state);
	QByteArray ret;
	for (int i = 0; i < len; i++) {
	QChar c = uc[i];
		ret.append(qf_UnicodeToAscii7(c.unicode()));
	}
	return ret;
}

QString QFAscii7Codec::convertToUnicode(const char* chars, int len, ConverterState *state) const
{
	Q_UNUSED(state);
	QString ret;
	for (int i = 0; i < len; i++) {
	char c = chars[i];
		ret += QChar(qf_Ascii7ToUnicode(c));
	}
	return ret;
}

QByteArray QFAscii7Codec::name() const
{
	return "ASCII7";
}

int QFAscii7Codec::mibEnum() const
{
	return 3;
}

static unsigned char qf_UnicodeToAscii7(ushort u)
{
	static QHash<ushort, char> code_table;
	if(code_table.isEmpty()) {
		code_table[0x00C1] = 'A'; /// A'
		code_table[0x00C4] = 'A'; /// A:
		code_table[0x00E1] = 'a'; /// a'
		code_table[0x00E4] = 'a'; /// a:
		code_table[0x010C] = 'C'; /// C^
		code_table[0x010D] = 'c'; /// c^
		code_table[0x010E] = 'D'; /// D^
		code_table[0x010F] = 'd'; /// d^
		code_table[0x00C9] = 'E'; /// E'
		code_table[0x011A] = 'E'; /// E^
		code_table[0x00CB] = 'E'; /// E:
		code_table[0x00E9] = 'e'; /// e'
		code_table[0x00EB] = 'e'; /// e:
		code_table[0x011B] = 'e'; /// e^
		code_table[0x00CD] = 'l'; /// I'
		code_table[0x00ED] = 'i'; /// i'
		code_table[0x0143] = 'N'; /// N^
		code_table[0x0148] = 'n'; /// n^
		code_table[0x00D3] = 'O'; /// O'
		code_table[0x00D6] = 'O'; /// O:
		code_table[0x00F3] = 'o'; /// o'
		code_table[0x00F6] = 'o'; /// o:
		code_table[0x0158] = 'R'; /// R^
		code_table[0x0159] = 'r'; /// r^
		code_table[0x0160] = 'S'; /// S^
		code_table[0x0161] = 's'; /// s^
		code_table[0x0164] = 'T'; /// T^
		code_table[0x0165] = 't'; /// t^
		code_table[0x00DA] = 'U'; /// U'
		code_table[0x016E] = 'U'; /// U*
		code_table[0x00DC] = 'U'; /// U:
		code_table[0x00FA] = 'u'; /// u'
		code_table[0x016F] = 'u'; /// u*
		code_table[0x00FC] = 'u'; /// u:
		code_table[0x00DD] = 'Y'; /// Y'
		code_table[0x00FD] = 'y'; /// y'
		code_table[0x017D] = 'Z'; /// Z^
		code_table[0x017E] = 'z'; /// z^
	}
	char c = code_table.value(u, 0);
	if(c == 0) c = QChar(u).toAscii();
	return c;
}

static ushort qf_Ascii7ToUnicode(uchar code)
{
	return QChar::fromAscii(code).unicode();
}
