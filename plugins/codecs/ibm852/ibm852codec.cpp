#include "ibm852codec.h"

#include <QHash>

static unsigned char qf_UnicodeToIBM852(ushort u);
static ushort qf_IBM852ToUnicode(uchar code);

QByteArray QFIBM852Codec::convertFromUnicode(const QChar *uc, int len, ConverterState *state) const
{
	Q_UNUSED(state);
	QByteArray ret;
    for (int i = 0; i < len; i++) {
        QChar c = uc[i];
		ret.append(qf_UnicodeToIBM852(c.unicode()));
    }
    return ret;
}

QString QFIBM852Codec::convertToUnicode(const char* chars, int len, ConverterState *state) const
{
	Q_UNUSED(state);
    QString ret;
    for (int i = 0; i < len; i++) {
        char c = chars[i];
		ret += QChar(qf_IBM852ToUnicode(c));
    }
    return ret;
}

QByteArray QFIBM852Codec::name() const
{
    return "IBM852";
}

int QFIBM852Codec::mibEnum() const
{
  return 2010;
}

static QHash<ushort, char> unicode_to_ibm852_code_table()
{
	static QHash<ushort, char> code_table;
	if(code_table.isEmpty()) {
		code_table[0x00C1] = 0xb5; /// A'
		code_table[0x00C4] = 0x8e; /// A:
		code_table[0x00E1] = 0xa0; /// a'
		code_table[0x00E4] = 0x84; /// a:
		code_table[0x010C] = 0xac; /// C^
		code_table[0x010D] = 0x9f; /// c^
		code_table[0x010E] = 0xd2; /// D^
		code_table[0x010F] = 0xd4; /// d^
		code_table[0x00C9] = 0x90; /// E'
		code_table[0x011A] = 0xb7; /// E^
		code_table[0x00CB] = 0xd3; /// E:
		code_table[0x00E9] = 0x82; /// e'
		code_table[0x011B] = 0xd8; /// e^
		code_table[0x00EB] = 0x89; /// e:
		code_table[0x00CD] = 0x96; /// I'
		code_table[0x00ED] = 0xa1; /// i'
		code_table[0x0143] = 0xd5; /// N^
		code_table[0x0148] = 0xe5; /// n^
		code_table[0x00D3] = 0xe0; /// O'
		code_table[0x00D6] = 0x99; /// O:
		code_table[0x00F3] = 0xa2; /// o'
		code_table[0x00F6] = 0x94; /// o:
		code_table[0x0158] = 0xfc; /// R^
		code_table[0x0159] = 0xfd; /// r^
		code_table[0x0160] = 0xe6; /// S^
		code_table[0x0161] = 0xe7; /// s^
		code_table[0x0164] = 0x9b; /// T^
		code_table[0x0165] = 0x9c; /// t^
		code_table[0x00DA] = 0xe9; /// U'
		code_table[0x00DC] = 0x9a; /// U:
		code_table[0x00FA] = 0xa3; /// u'
		code_table[0x016E] = 0xde; /// U*
		code_table[0x016F] = 0x85; /// u*
		code_table[0x00FC] = 0x81; /// u:
		code_table[0x00DD] = 0xed; /// Y'
		code_table[0x00FD] = 0xec; /// y'
		code_table[0x017D] = 0xa6; /// Z^
		code_table[0x017E] = 0xa7; /// z^
	}
	return code_table;
}

static unsigned char qf_UnicodeToIBM852(ushort u)
{
	char c = unicode_to_ibm852_code_table().value(u, 0);
	if(c == 0) c = QChar(u).toAscii();
	return c;
}

static QHash<char, ushort> ibm852_to_unicode_code_table()
{
	static QHash<char, ushort> code_table;
	if(code_table.isEmpty()) {
		QHashIterator<ushort, char> i(unicode_to_ibm852_code_table());
		while (i.hasNext()) {
			i.next();
			code_table[i.value()] = i.key();
		}
	}
	return code_table;
}

static ushort qf_IBM852ToUnicode(uchar code)
{
	ushort ret = ibm852_to_unicode_code_table().value(code, 0);
	if(ret == 0) ret = QChar::fromAscii(code).unicode();
	return ret;
}
