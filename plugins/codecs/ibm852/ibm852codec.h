#ifndef QF_IBM852_H
#define QF_IBM852_H

#include <QTextCodec>
#include <QString>

class QFIBM852Codec : public QTextCodec 
{
public:
    //~QFIBM852Codec();

    QByteArray name() const;
    int mibEnum() const;

    QString convertToUnicode(const char *, int, ConverterState *) const;
    QByteArray convertFromUnicode(const QChar *, int, ConverterState *) const;
};

#endif // QF_IBM852_H
