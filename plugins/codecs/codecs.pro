MY_SUBPROJECT = qftextcodecsplugin
!include(go_to_top.pri) :error("cann't find go_to_top.pri")

TARGET	 = $$MY_SUBPROJECT$$QF_LIBRARY_DEBUG_EXT

#LIBQF = $$MY_PATH_TO_TOP/libqf

#DEFINES += QF_PATCH

include(codecsbase.pri)
include(ibm852/ibm852codec.pri)
include(fascii7/fascii7codec.pri)

HEADERS += \
	qftextcodecsplugin.h \

SOURCES += \
	qftextcodecsplugin.cpp \
