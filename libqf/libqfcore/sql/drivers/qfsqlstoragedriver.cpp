
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqlstoragedriver.h"

#include <qfappdbconnectioninterface.h>
#include <qfsqlquery.h>
#include <qf.h>
#include <qfexception.h>

#include <QSqlDriver>
//#include <QVariant>

#include <qflogcust.h>

//==========================================================
//                  QFSqlStorageDriver
//==========================================================
QFSqlStorageDriver QFSqlStorageDriver::storageDriver;

QFSqlStorageDriverProperties QFSqlStorageDriver::driverProperties(QFBasicTable::Row & row) const
{
	QFSqlStorageDriverProperties props = qvariant_cast<QFSqlStorageDriverProperties>(row.tableProperties().storageDriverProperties());
	return props;
}
/*
QFSqlConnection QFSqlStorageDriver::connection(QFBasicTable::Row & row) const
{
	return driverProperties(row).connection();
}

void QFSqlStorageDriver::setNumRowsAffected(int n)
{
}

int QFSqlStorageDriver::numRowsAffected() const
{
	driverProperties().numRowsAffected();
}
*/

QFStringSet QFSqlStorageDriver::referencedForeignTables(QFBasicTable::Row & row)
{
	qfLogFuncFrame();
	const QFStringMap m = driverProperties(row).foreignKeyMap();
	if(0) {
		//qfInfo() << "\t foreign keys:";
		QFStringMapIterator i(m);
		while (i.hasNext()) {
			i.next();
			qfTrash() << "\t\t" << i.key() << "-->" << i.value();
		}
	}
	QFStringSet referenced_foreign_tables;
	{
		QStringList sl = m.values();
		foreach(QString s, sl) {
			QString tbl_name;
			QFSql::parseFullName(s, NULL, &tbl_name);
			tbl_name = tbl_name.trimmed().toLower();
			if(!tbl_name.isEmpty()) {
				qfTrash() << "\t referenced_foreign_tables <<" << tbl_name;
				referenced_foreign_tables << tbl_name;
			}
		}
	}
	qfTrash() << "\t returning:" << QStringList(referenced_foreign_tables.toList()).join(", ");
	return referenced_foreign_tables;
}

QStringList QFSqlStorageDriver::tableIdsSortedAccordingToForeignKeys(QFBasicTable::Row & row)
{
	qfLogFuncFrame();
	const QFSqlStorageDriverProperties &props = driverProperties(row);
	QStringList table_ids = props.tableIds();
	qfTrash() << "\t before:" << table_ids.join(", ");
	if(!props.foreignKeyMap().isEmpty()) {
		QFSqlConnection conn = props.connection();
		/// nejdriv se musej postnout ty tabulky, ktery obsahuji masterKeys
		QStringList master_table_ids = props.foreignKeyMap().keys();
		int ix = 0;
		qfTrash() << "\t table ids::" << table_ids.join(",");
		foreach(QString s, master_table_ids) {
			qfTrash() << "\t foreign key:" << s << "-->" << props.foreignKeyMap().value(s);
			s = conn.normalizeTableName(s.section('.', 0, -2));
			int ix1 = table_ids.indexOf(s);
			qfTrash() << "\t index of" << s << ":" << ix1 << "current ix:" << ix << "current table id:" << table_ids.value(ix);
			if(ix1 > ix) {
				/// spatne poradi, prehod tabulky
				qfTrash().noSpace() << "\t swapping table_ids[" << ix << "]='" << table_ids[ix] << "' and table_ids[" << ix1 << "]='" << table_ids[ix1];
				table_ids[ix1] = table_ids[ix];
				table_ids[ix] = s;
				qfTrash().noSpace() << "\t swapped table_ids[" << ix << "]='" << table_ids[ix] << "' and table_ids[" << ix1 << "]='" << table_ids[ix1];
			}
			ix++;
		}
	}
	qfTrash() << "\t after:" << table_ids.join(", ");
	return table_ids;
}

bool QFSqlStorageDriver::postRow(QFBasicTable::Row & row) throw( QFException )
{
	qfLogFuncFrame() << "is insert:" << row.isInsert();
	bool ret = false;
	//bool check_prikeys = false;
	
	if(0) {
		int i = 0;
		foreach(QFSqlField f, row.fields()) {
			qfInfo() << "\tfield:" << f.fullName() << "\tdirty:" << row.isDirty(i);
			i++;
		}
	}
	/*
	if(connection().driverName() != "QFPSQL") {
	QF_SQL_EXCEPTION(tr("Sorry this feature is not supported for driver %1").arg(connection().driverName()));
}
	*/
	if(row.isDirty()) {
		const QFSqlStorageDriverProperties &props = driverProperties(row);
		QFSqlConnection conn = props.connection();
		if(row.isInsert()) {
			qfTrash() << "\tINSERT";
			QFStringSet referenced_foreign_tables = referencedForeignTables(row);
			int tbl_cnt = 0;
			QStringList table_ids = tableIdsSortedAccordingToForeignKeys(row);
			foreach(QString tableid, table_ids) {
				qfTrash() << "\ttable:" << tableid;
				QString table_name = tableid.section('.', -1).trimmed().toLower();
				qfTrash() << "\t\t table_name:" << table_name;
				if(tbl_cnt++ > 0) {
					/// 1. tabulku ukladej vzdy, ostatni pouze pokud jsou definovany podminky pro ulozeni jejich foreign klicu
					if(!referenced_foreign_tables.contains(table_name)) {
						qfTrash() << "\tSKIPPING" << table_name;
						continue;
					}
				}
				QFSqlTableInfo ti = conn.catalog().table(tableid);
				//qfTrash() << "\t\t table info:" << ti.toString();
				QFString table = ti.fullName();
				table = conn.fullTableNameToQtDriverTableName(table);
				QSqlDriver *drv = conn.driver();
				{
					QSqlRecord rec;
					int i = -1;
					int autoinc_ix = -1;
					//QSqlIndex pri_ix = ti.primaryIndex();
					bool has_blob_field = false;
					foreach(QFSqlField f, row.fields()) {
						i++;
						if(f.fullTableName() != tableid) continue;
						if(ti.field(f.fieldName()).isAutoIncrement()) {
							/// autoinc field vzdy pridej do insertu, mohlo by se stat, ze by nesel ulozit prazdnej radek, protoze by nebyl dirty
							row.setDirty(i);
							autoinc_ix = i;
						}
						if(ti.field(f.fieldName()).isPriKey()) {
							/// prikey field vzdy pridej do insertu, protoze ten je treba vzdy nastavit
							row.setDirty(i);
						}
						if(!row.isDirty(i)) continue;
						qfTrash() << "\tdirty field:" << f.name();
						qfTrash() << "\tnullable:" << f.isNullable();
						if(f.type() == QVariant::ByteArray) {
							qfTrash() << "\t\tBLOB size:" << row.value(i).toByteArray().size();
							has_blob_field = true;
						}
						QVariant v = row.value(i);
						//qfInfo() << "\t" << f.fieldName() << "value type:" << QVariant::typeToName(v.type());
						f.setValue(v);
						//qfInfo() << "\t\t" << "val type:" << QVariant::typeToName(f.value().type());
						f.setName(f.fieldName());
						rec.append(f);
					}
					if(!rec.isEmpty()) {
						qfTrash() << "updating table inserts" << table;
						QString s;
						s = drv->sqlStatement(QSqlDriver::InsertStatement, table, rec, has_blob_field);
						QFSqlQuery q = QFSqlQuery(conn);
						if(has_blob_field) {
							q.prepare(s);
							for(int i=0; i<rec.count(); i++) {
								//QVariant::Type type = rec.field(i).value().type();
								//qfInfo() << "\t" << rec.field(i).name() << "bound type:" << QVariant::typeToName(type);
								q.addBindValue(rec.field(i).value());
							}
							q.exec();
						}
						else {
							qfTrash() << "\texecuting:" << s;
							q.exec(s);
						}
						qfTrash() << "\tnum rows affected:" << q.numRowsAffected();
						int num_rows_affected = q.numRowsAffected();
						//setNumRowsAffected(q.numRowsAffected());
						if(num_rows_affected != 1) QF_INTERNAL_ERROR(QString("numRowsAffected() = %1, should be 1\n%2").arg(num_rows_affected).arg(s));
						if(autoinc_ix >= 0) {
							QVariant v = Qf::retypeVariant(q.lastInsertId(), row.fields()[autoinc_ix].type());
							row.setValue(autoinc_ix, v);
							row.setDirty(autoinc_ix, false);
						}
					}
				}
				if(props.foreignKeyMap().isEmpty()) break; /// insert v JOINed SELECTu ma smysl jen pro jednu tabulku, takze tu prvni v dotazu.
				else {
					/// insertni i do slave tabulek
					QSqlIndex pri_ix = ti.primaryIndex();
					for(int i=0; i<pri_ix.count(); i++) {
						QFSqlField f = pri_ix.field(i);
						QString master_key = QFSql::composeFullName(f.name(), table_name).toLower();
						qfTrash() << "\t master_key:" << master_key;
						QString slave_key = props.foreignKeyMap().value(master_key);
						if(!slave_key.isEmpty()) {
							qfTrash() << "\tsetting value of foreign key" << slave_key << "to value of master key:" << QFSql::formatValue(row.value(master_key));
							row.setValue(slave_key, row.value(master_key));
						}
					}
				}
			}
			//QF_ASSERT(!row.isDirty(), "probably bad field order in SELECT for use with foreignKeyMap");
		}
		else {
			qfTrash() << "\tEDIT";
			foreach(QString tableid, props.tableIds()) {
				qfTrash() << "\ttableid:" << tableid;
				QFSqlTableInfo ti = conn.catalog().table(tableid, !Qf::ThrowExc);
				if(!ti.isValid()) continue;
				//qfTrash() << "\tconnection().driverName():" << connection().driverName();
				QFString table = ti.fullName();
				table = conn.fullTableNameToQtDriverTableName(table);
				QSqlDriver *drv = conn.driver();
				{
					QSqlRecord edit_rec;
					int i = -1;
					bool has_blob_field = false;
					foreach(QFSqlField f, row.fields()) {
						i++;
						if(f.fullTableName() != tableid) continue;
						if(!row.isDirty(i)) continue;
						QVariant v = row.value(i);
						qfTrash() << "\ttableid:" << tableid << "fullTableName:" << f.fullTableName();
						qfTrash().noSpace() << "\tdirty field '" << f.name() << "' type(): " << f.type();
						qfTrash().noSpace() << "\tdirty value: '" << v.toString() << "' isNull(): " << v.isNull() << " type(): " << v.type();
						//if(!v.isNull()) f.clear();
						//else f.setValue(v);
						f.setValue(v);
						if(f.type() == QVariant::ByteArray) has_blob_field = true;
						f.setName(f.fieldName());
						qfTrash() << "\tfield is null: " << f.isNull();
						edit_rec.append(f);
					}
					if(!edit_rec.isEmpty()) {
						qfTrash() << "updating table edits:" << table << "(" << tableid << ")";
						if(!resultHasAllPriKeys(conn, row.fields(), tableid)) {
							//if(check_prikeys) QF_SQL_EXCEPTION(TR("The result doesn't have all pri keys for updates in table '%1'").arg(table));
							//qfWarning() << QF_FUNC_NAME << TR("The result doesn't have all pri keys for updates in table") << table;
							continue;
						}
						QString s;
						s += drv->sqlStatement(QSqlDriver::UpdateStatement, table, edit_rec, has_blob_field);
						s += " ";
						QSqlIndex pri_ix = ti.primaryIndex();
						QSqlRecord where_rec;
						for(int i=0; i<pri_ix.count(); i++) {
							QFSqlField f = pri_ix.field(i);
							/// find field in model fields
							QString fn;
							fn = QFSql::composeFullName(f.name(), tableid);
							int ix = row.fields().fieldNameToIndex(fn);
							QF_ASSERT(ix >= 0, TR("index field '%1' not found in fields").arg(fn));
							/// WHERE condition should use old values
							f.setValue(row.origValue(ix));
							qfTrash() << "\tpri index" << f.name() << ":" << f.value().toString() << "value type:" << QVariant::typeToName(f.value().type()) << "field type:" << QVariant::typeToName(f.type());
							where_rec.append(f);
						}
						QF_ASSERT(!where_rec.isEmpty(), "pri keys values not generated");

						s += drv->sqlStatement(QSqlDriver::WhereStatement, table, where_rec, false);
						qfTrash() << "\t" << s;
						QFSqlQuery q = QFSqlQuery(conn);
						qfTrash() << "\t connection signature:" << conn.signature();
						if(has_blob_field) {
							q.prepare(s);
							for(int i=0; i<edit_rec.count(); i++) q.addBindValue(edit_rec.field(i).value());
							q.exec();
						}
						else {
							q.exec(s);
						}
						qfTrash() << "\tnum rows affected:" << q.numRowsAffected();
						int num_rows_affected = q.numRowsAffected();
						//d->numRowsAffected = q.numRowsAffected();
						if(num_rows_affected > 1) {
							/// if update commant does not realy change data for ex. (UPDATE woffice.kontakty SET id=8 WHERE id = 8)
							/// numRowsAffected() returns 0.
							QF_INTERNAL_ERROR(QString("numRowsAffected() = %1, sholuld be 1 or 0\n%2").arg(num_rows_affected).arg(s));
						}
					}
				}
			}
		}
		ret = true;
	}
	if(ret) QFStorageDriver::postRow(row);
	return ret;
}

bool QFSqlStorageDriver::deleteRow(QFBasicTable::Row & row) throw( QFException )
{
	qfLogFuncFrame();
	//qfInfo() << QFLog::stackTrace();
	bool ret = false;
	/*
	if(connection().driverName() != "QFPSQL") {
	QF_SQL_EXCEPTION(tr("Sorry this feature is not supported for driver %1").arg(connection().driverName()));
}
	*/
	if(!row.isInsert()) {
		const QFSqlStorageDriverProperties &props = driverProperties(row);
		QFSqlConnection conn = props.connection();
		if(row.isDirty()) row.restoreValues();
		QFStringSet referenced_foreign_tables = referencedForeignTables(row);
		//QStringList table_ids = props.tableIds();
		//QStringList table_ids = tableIdsSortedAccordingToForeignKeys(row);
		int table_id_cnt = 0;
		foreach(QString tableid, tableIdsSortedAccordingToForeignKeys(row)) {
			QFSqlTableInfo ti = conn.catalog().table(tableid, !Qf::ThrowExc);
			if(!ti.isValid()) {
				qfTrash() << "\tskipping table" << tableid << " not found in catalog";
				continue;
			}
			/// V 1. tabulce mazej vzdy, v ostatnich pouze pokud jsou definovany podminky pro ulozeni jejich foreign klicu
			if(table_id_cnt++ > 0) {
				QString table_name = ti.tableName().toLower();
				if(!referenced_foreign_tables.contains(table_name)) {
					qfTrash() << "\tskipping table" << table_name;
					continue;
				}
			}
			
			QFString table = conn.fullTableNameToQtDriverTableName(ti.fullName());
			QSqlRecord rec;
			qfTrash() << "deleting in table" << table;
			if(!resultHasAllPriKeys(conn, row.fields(), tableid)) {
				QF_SQL_EXCEPTION(TR("The resultset doesn't have all pri keys for delete in table '%1'").arg(table));
				//qfWarning() << QF_FUNC_NAME << TR("The resultset doesn't have all pri keys for delete in table") << table;
			}
			else {
				QSqlDriver *drv = conn.driver();
				QString s;
				s += drv->sqlStatement(QSqlDriver::DeleteStatement, table, rec, false);
				s += " ";
				QSqlIndex pri_ix = ti.primaryIndex();
				rec.clear();
				for(int i=0; i<pri_ix.count(); i++) {
					QFSqlField f = pri_ix.field(i);
					QString fld_name = QFSql::composeFullName(f.name(), tableid);
					f.setValue(row.value(fld_name));
					qfTrash() << "\tpri index" << fld_name << ":" << f.value().toString();
					rec.append(f);
				}
				if(rec.isEmpty()) QF_SQL_EXCEPTION(TR("There is not a primary key to remove the row."));
				s += drv->sqlStatement(QSqlDriver::WhereStatement, table, rec, false);
				qfTrash() << "\t" << s;
				QFSqlQuery q = QFSqlQuery(conn);
				q.exec(s);
				//qfTrash() << QFLog::stackTrace();
				if(q.numRowsAffected() != 1) QF_INTERNAL_ERROR(QString("numRowsAffected() = %1, should be 1\n%2").arg(q.numRowsAffected()).arg(s));
				//if(q.numRowsAffected() != 1) QF_SQL_EXCEPTION(QString("numRowsAffected() = %1, should be 1\n%2").arg(q.numRowsAffected()).arg(s));
				ret = true;
			}
		}
	}
	return ret;
}

void QFSqlStorageDriver::fillDefaultAndAutogeneratedValues(QFBasicTable::Row & row) throw( QFException )
{
	qfTrash() << QF_FUNC_NAME;
	const QFSqlStorageDriverProperties &props = driverProperties(row);
	QFSqlConnection conn = props.connection();
	/// fill autogenerated values
	for(int i=0; i<row.fields().count(); i++) {
		QFSqlField f = row.fields()[i];
		/// can have an autogenerated fields
		qfTrash() << "\t" << f.fullTableName() << f.fieldName();
		//qfTrash() << "\t" << f.toString();
		//qfTrash() << "\t" << fields()[i].toString();
		QFSqlFieldInfo fi = conn.catalog().table(f.fullTableName(), !Qf::ThrowExc).field(f.fieldName(), !Qf::ThrowExc);
		if(fi.isValid()) {
			//qfTrash() << "\t" << __LINE__;
			QVariant v = fi.seqNextVal();
			if(v.isValid()) {
				row.setValue(i, v);
			}
			else {
				/// get default values
				v = fi.defaultValue();
				//qfTrash() << "default val:" << v.toString() << "type:" << v.type() << "is valid:" << v.isValid() << "is null:" << v.isNull();
				/// valid variant can be null, for exmple QVariant(QString()).
				row.setValue(i, v);
				/// clear dirty flag for default values, they need not to be explicitly stored
				row.setDirty(i, false);
			}
		}
	}
	qfTrash() << "\tinsert flag:" << row.isInsert();
}

void QFSqlStorageDriver::checkClonnedRow(QFBasicTable::Row & row) throw( QFException )
{
	qfLogFuncFrame();
	const QFSqlStorageDriverProperties &props = driverProperties(row);
	QFSqlConnection conn = props.connection();
	/// clear autogenerated values
	for(int i=0; i<row.fields().count(); i++) {
		QFSqlField f = row.fields()[i];
		/// can have an autogenerated fields
		qfTrash() << "\t" << f.fullTableName() << f.fieldName();
		//qfTrash() << "\t" << f.toString();
		//qfTrash() << "\t" << fields()[i].toString();
		QFSqlFieldInfo fi = conn.catalog().table(f.fullTableName(), !Qf::ThrowExc).field(f.fieldName(), !Qf::ThrowExc);
		if((fi.isValid() && fi.isAutoIncrement()) || f.isAutoValue()) {
			/// isAutoIncrement() beru z katalogu a isAutoValue() dodavaji Trollove z driveru, pro jistotu pouzivam oboji
			row.setValue(i, QVariant());
		}
	}
}

int QFSqlStorageDriver::reloadRow(QFBasicTable::Row &row) throw(QFException)
{
	qfLogFuncFrame() << "row is null:" << row.isNull();
	const QFSqlStorageDriverProperties &props = driverProperties(row);
	QFSqlConnection conn = props.connection();
	QFSqlQueryBuilder qb = props.queryBuilder();
	if(qb.isEmpty()) {
		throw QFReloadSqlRowException("empty queryBuilder");
	}
	//qfInfo() << "\t row:" << row.toString();
	qfTrash() << "\t row table query:" << qb.toString();
	//QSqlDriver *drv = connection().driver();
	/// dopln WHERE o dodatecnou podminku u vsech tabulek v SELECTU, ktere maji v resultsetu vsechny primarni klice
	foreach(QString id, props.tableIds()) {
		if(resultHasAllPriKeys(conn, row.fields(), id)) {
			QSqlRecord rec = conn.catalog().table(id).primaryIndex();
			QStringList row_where;
			for(int i=0; i<rec.count(); i++) {
				QString s = QFSql::composeFullName(rec.field(i).name(), id);
				int ix = row.fields().fieldNameToIndex(s);
				QVariant v = row.value(ix);
				qfTrash() << "\tprikey field:" << s << "index:" << ix << "value:" << v.toString();
				if(!v.isNull()) {
					s += "=" + QFSql::formatValue(row.value(ix));
					row_where << s;
				}
			}
			if(!row_where.isEmpty()) {
				//qfInfo() << __LINE__ << qb.toString();
				QFString where = qb.take(QFSqlQueryBuilder::WhereKey).trimmed();
				//qfInfo() << __LINE__ << qb.toString();
				if(!where.isEmpty()) row_where << "(" + where + ")";
				where = row_where.join(" AND ");
				qb.where(where);
				//qfInfo() << __LINE__ << qb.toString();
			}
		}
	}
	int row_cnt = 0;
	QString s = qb.toString();
	/// muze se stat, ze s.isEmpty(), napriklad, kdyz je query typu SELECT ... UNION SELECT ... 
	if(!s.isEmpty()) {
		QFSqlQuery q = QFSqlQuery(conn);
		qfTrash() << "\tquery:" << s;
		q.exec(s);
		while(q.next()) {
			if(row_cnt++) {
				throw QFReloadSqlRowException("More than 1 row returned by query.\n" + s);
			}
			for(int i=0; i<row.fields().count(); i++) {
				row.setValue(i, q.value(i));
				qfTrash() << "\t\t" << row.value(i).toString();
			}
			row.clearOrigValues();
		}

		if(!row_cnt) {
		// row is deleted
		// find its index in the table rowlist
		//QF_SQL_EXCEPTION("0 rows returned by query.\n" + s);
		}
	}
	return row_cnt;
}

bool QFSqlStorageDriver::resultHasAllPriKeys(const QFSqlConnection &conn, const QFBasicTable::FieldList &fields, const QString &tableid)
{
	qfLogFuncFrame() << "tableid:" << tableid;
	//QSqlIndex pix = connection().catalog().table(tableid).primaryIndex();
	QSqlIndex pix = conn.catalog().table(tableid, !Qf::ThrowExc).primaryIndex();
	bool ret = false;
	if(!pix.isEmpty()) {
		ret = true;
		QString tbl = conn.normalizeTableName(tableid);
		for(int i=0; i<pix.count(); i++) {
			QString s = QFSql::composeFullName(pix.field(i).name(), tbl);
			qfTrash() << "\ts:" << s;
			if(fields.fieldNameToIndex(s, false) < 0) {ret = false; break;}
		}
	}
	qfTrash() << "\treturn:" << ret;
	return ret;
}

//==========================================================
//                  QFSqlStorageDriverProperties
//==========================================================
QFSqlStorageDriverProperties::QFSqlStorageDriverProperties()
{
	*this = sharedNull();
}

QFSqlStorageDriverProperties::QFSqlStorageDriverProperties(int )
{
	d = new Data();
}

QFSqlStorageDriverProperties::QFSqlStorageDriverProperties(const QFSqlConnection & _connection)
{
	d = new Data(_connection);
}

const QFSqlStorageDriverProperties & QFSqlStorageDriverProperties::sharedNull()
{
	static QFSqlStorageDriverProperties n(1);
	return n;
}

const QFSqlConnection& QFSqlStorageDriverProperties::connection(bool throw_exc) const throw(QFException)
{
	//qfTrash() << QF_FUNC_NAME;
	if(d->connection.isValid()) return d->connection;

	QCoreApplication *core_app = QCoreApplication::instance();
	QFAppDbConnectionInterface *ifc = dynamic_cast<QFAppDbConnectionInterface*>(core_app); /// cross cast
	if(ifc) {
		QFSqlConnection &conn = ifc->connection();
		if(!conn.isValid() && throw_exc) {
			QF_EXCEPTION(TR("QFSqlQueryModel::connection() - connection is not valid!"));
		}
		return conn;
	}
	if(throw_exc) QF_EXCEPTION("No QFAppDbConnectionInterface");
	//qfTrash() << "\tusing application default connection:" << conn.signature();
	return QFAppDbConnectionInterface::dummyConnection();
}










