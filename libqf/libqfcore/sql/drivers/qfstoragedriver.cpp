
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
#include "qfstoragedriver.h"

#include <qflogcust.h>

bool QFStorageDriver::postRow(QFBasicTable::Row & row) throw( QFException )
{
	row.clearEditFlags();
	return true;
}



