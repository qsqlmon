//
// C++ Interface: qfsql
//
// Description:
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef QFSQL_H
#define QFSQL_H

#include <qfcoreglobal.h>

#include <QString>
#include <QVariant>

/**
 * some globals and enums for SQL part of libqf.
@author Fanda Vacek
*/
class QFCORE_DECL_EXPORT QFSql
{
	public:
		enum RelationKind {UnknownRelation = 0, TableRelation = 1, ViewRelation = 2, SystemTableRelation = 4, IndexRelation = 8, SequenceRelation = 16, AllRelations = 255};
	public:
		static QString relationKindAsString(QFSql::RelationKind kind);
		static void parseFullName(const QString& full_field_name, QString *field_name = NULL, QString *table_name = NULL, QString *db_name = NULL);
		static QString composeFullName(const QString& field_name, const QString& table_name = QString(), const QString& db_name = QString());
		// Prepends '.' if fieldname does not start with one.
		//static QString prependDot(const QString &fieldname);
		//static QString cutDot(const QString &fieldname);
		//! Return true if \a field_name1 fully ends with \a field_name2.
		/**
		For example: endsWith("t.field", "field") returns true,
		 endsWith("t.field", "ield") returns false,
		 endsWith("field", "t.field") returns false.
		 Comparision is case insensitive.
		*/
		static bool endsWith(const QString &field_name1, const QString &field_name2);
		//! vraci true, kdyz si mysli, ze jsou stejny (lisi se jen v predponach).
		//! @sa sqlIdCmp(const QString &sql_id);
		static bool sqlIdCmp(const QString &sql_id1, const QString &sql_id2);
		static QString nullString();

		//! vymeni vsechny apostrofy za uvozovky, aby to slo ulozit do SQL
		static QString escapeXmlForSql(const QString &xml_str);
		//! obackslashuje vsechny apostrofy, aby to slo ulozit do SQL
		static QString escapeJavaScriptForSQL(const QString &code);
		// odbackslashuje vsechny apostrofy, protoze to bylo ulozeny v SQL
		//static QString unescapeJavaScriptFromSQL(const QString &str);
		static QString escapeSqlForSQL(const QString &code);
		//static QString unescapeSqlFromSQL(const QString &str);

		/// prevede na string a prida apostrofy, kdyz je val string nebo datum.
		static QString formatValue(const QVariant &val);
};

#define FROMJOIN(tab1, id1, tab2, id2) " FROM " #tab1 JOIN(tab1, id1, tab2, id2)
#define JOIN(tab1, id1, tab2, id2) " LEFT JOIN " #tab2 " ON " #tab1 "." #id1 "=" #tab2 "." #id2 " "
#define JOINALIAS(tab1, id1, tab2, alias2, id2) " LEFT JOIN " #tab2 " AS " #alias2 " ON " #tab1 "." #id1 "=" #alias2 "." #id2 " "

#endif
