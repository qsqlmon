//
// C++ Implementation: qfsqlrecord
//
// Description: 
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "qfsqlrecord.h"

QFSqlRecord::QFSqlRecord()
	: QSqlRecord()
{
}

QFSqlRecord::QFSqlRecord(const QSqlRecord & other)
	: QSqlRecord(other)
{
}

QFSqlRecord::~QFSqlRecord()
{
}

/*
QFSqlField QFSqlRecord::field(int index) const
{
	return QFSqlField(QSqlRecord::field(index));
}

QFSqlField QFSqlRecord::field(const QString & name) const
{
	return QFSqlField(QSqlRecord::field(name));
}
*/
