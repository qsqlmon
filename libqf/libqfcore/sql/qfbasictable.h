
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFBASICTABLE_H
#define QFBASICTABLE_H

#include <qf.h>
#include <qfflags.h>
#include <qfcoreglobal.h>
#include <qfsqlfield.h>
#include <qfexception.h>
//#include <qfexplicitlyshareddata.h>
#include <qfclassfield.h>
//#include <qfstoragedriver.h>

#include <QVector>
#include <QBitArray>
#include <QPair>
#include <QVariant>
#include <QDomElement>

class QFXmlTable;
class QFStorageDriver;

//! Implicitly shared.
class QFCORE_DECL_EXPORT QFBasicTable
{
	public:
			enum Flags {EditRowsAllowed = 1, InsertRowsAllowed = 2, DeleteRowsAllowed = 4};
	public:
		void setReadOnly(bool ro = true);
		bool isReadOnly() const;
	public:
		enum CleanupDataOption {ClearFieldsRows = 1, ClearRows};
		//enum ImportOption {ImportWithColumnNames = 1, ImportAppend = 2};
		//typedef QFFlags<ImportOption> ImportOptions;
		class TextImportOptions : public QVariantMap 
		{
			public:
				QChar fieldSeparator() const {return value("fieldSeparator", QChar(',')).toChar();}
				TextImportOptions& setFieldSeparator(QChar c) {this->operator[]("fieldSeparator") = c; return *this;}
				QChar fieldQuotes() const {return value("fieldQuotes", QChar('"')).toChar();}
				TextImportOptions& setFieldQuotes(QChar c) {this->operator[]("fieldQuotes") = c; return *this;}
				bool isImportColumnNames() const {return value("importColumnNames", true).toBool();}
				TextImportOptions& setImportColumnNames(bool b) {this->operator[]("importColumnNames") = b; return *this;}
				bool isImportAppend() const {return value("importAppend", false).toBool();}
				TextImportOptions& setImportAppend(bool b) {this->operator[]("importAppend") = b; return *this;}
				int ignoreFirstLinesCount() const {return value("ignoreFirstLinesCount", 0).toInt();}
				TextImportOptions& setIgnoreFirstLinesCount(int i) {this->operator[]("ignoreFirstLinesCount") = i; return *this;}
		};
		class TextExportOptions : public QVariantMap
		{
			public:
				enum FieldQuoting {Never=1, IfNecessary, Always};
			public:
				QChar fieldSeparator() const {return value("fieldSeparator", QChar(',')).toChar();}
				TextExportOptions& setFieldSeparator(QChar c) {this->operator[]("fieldSeparator") = c; return *this;}
				QChar fieldQuotes() const {return value("fieldQuotes", QChar('"')).toChar();}
				TextExportOptions& setFieldQuotes(QChar c) {this->operator[]("fieldQuotes") = c; return *this;}
				bool isExportColumnNames() const {return value("exportColumnNames", true).toBool();}
				TextExportOptions& setExportColumnNames(bool b) {this->operator[]("exportColumnNames") = b; return *this;}
				FieldQuoting fieldQuotingPolicy() const {return (FieldQuoting)(value("fieldQuotingPolicy", IfNecessary).toInt());}
				TextExportOptions& setFieldQuotingPolicy(FieldQuoting fq) {this->operator[]("fieldQuotingPolicy") = fq; return *this;}
				QString codecName() const {return value("codecName").toString();}
				TextExportOptions& setCodecName(const QString &codec_name) {this->operator[]("codecName") = codec_name; return *this;}
				int fromLine() const {return value("fromLine", 0).toInt();}
				TextExportOptions& setFromLine(int n) {this->operator[]("fromLine") = n; return *this;}
				int toLine() const {return value("toLine", -1).toInt();}
				TextExportOptions& setToLine(int n) {this->operator[]("toLine") = n; return *this;}
		};
		//enum ExportOption {ExportWithColumnNames = 1};
		//typedef QFFlags<ExportOption> ExportOptions;
	public:
		//! Sorting information fo one field.
		struct QFCORE_DECL_EXPORT SortDef
		{
			static const bool Ascending = true;
			static const bool CaseSensitive = true;
			static const bool Ascii7Bit = true;
			int fieldIndex; //!< index of field to sort.
			bool ascending;
			bool caseSensitive;
			bool ascii7bit; //!< convert values to ASCII 7 bit for before sorting.
			SortDef(int i = -2, bool a = true, bool cs = true, bool _ascii7bit = false) : fieldIndex(i), ascending(a), caseSensitive(cs), ascii7bit(_ascii7bit) {}

			bool isValid() const {return fieldIndex >= -1;}
			//bool isSortByRowNo() const {return fieldIndex == -1;}
			//void setSortByRowNo() {fieldIndex = -1;}
		};
	public:
		typedef QList<SortDef> SortDefList;
	public:
		class QFCORE_DECL_EXPORT LessThan
		{
			public:
				inline bool operator()(int r1, int r2) const { return lessThan(r1, r2); }
				inline bool operator()(int r, const QVariant &v) const { return lessThan_helper(r, v, false); }
				inline bool operator()(const QVariant &v, int r) const { return lessThan_helper(r, v, true); }
			protected:
				const QFBasicTable &table;
				QFBasicTable::SortDefList sortedFields;

				bool lessThan(int r1, int r2) const;
		//bool lessThan(int r, const QVariant &v) const {return lessThan_helper(r, v, true);}
		//bool lessThan(const QVariant &v, int r) const {return lessThan_helper(r, v, false);}
				bool lessThan_helper(int _row, const QVariant &v, bool switch_params) const;
			public:
				virtual int cmp(const QVariant &l, const QVariant &r, const QFBasicTable::SortDef &sd) const;
			public:
				LessThan(const QFBasicTable &t);
				virtual ~LessThan() {}
		};
	protected:
		typedef QVector<int> RowIndexList;
		//! List og fields in table, implicitly shared.
	public:
		class QFCORE_DECL_EXPORT FieldList : public QList<QFSqlField>
		{
			public:
				/**
				finds index in field list, containing column \a field_name
				@param field_name fieldname in form [tablename.]fieldname
				@return field index or value lower than zero
				 */
				int fieldNameToIndex(const QString &field_name, bool throw_exc=true) const throw(QFException);
				int fieldIndex(const QString &field_name) const {return fieldNameToIndex(field_name, !Qf::ThrowExc);}
				bool isValidFieldIndex(int fld_ix) const;
		};
	protected:
		class QFCORE_DECL_EXPORT TableProperties
		{
			public:
				//enum SortOption {SortCaseSensitive = 1};
				//typedef QFFlags<SortOption> SortOptions;
			protected:
				class Data : public QSharedData
				{
					//friend class Row;
					public:
						FieldList fields;
						SortDefList sortedFields; //!< Each sorted field has one entry in this list.
						/// storageDriver je sigleton a obsahuje jenom metody nad storageDriverProperties, takze se muze pointer kopirovat, jen to hvizda
						QFStorageDriver *storageDriver;
						QVariant storageDriverProperties;
						//QFBasicTableLessThan *sortLessThan;
					public:
						Data() : storageDriver(NULL) {}
						~Data() {}
				};
			private:
				QSharedDataPointer<Data> d;
				//TableProperties(bool); // create NULL properties, this does not work for explicitly shared objects
			public:
				static const TableProperties& sharedNull();
				bool isNull() const {return d == sharedNull().d;}

				FieldList& fieldsRef() {return d->fields;}
				const FieldList& fields() const {return d->fields;}

				const SortDefList& sortDefinition() const {return d->sortedFields;}
				SortDefList& sortDefinitionRef() {return d->sortedFields;}
				void setSortDefinition(const SortDefList& sdl) {d->sortedFields = sdl;}

				void setStorageDriver(QFStorageDriver *drv);
				QFStorageDriver* storageDriver() const {return d->storageDriver;}
				
				const QVariant& storageDriverProperties() const {return d->storageDriverProperties;}
				QVariant& storageDriverPropertiesRef() {return d->storageDriverProperties;}

				bool operator==(const TableProperties &other) const {return d == other.d;}
			private:
				TableProperties(int);
			public:
				TableProperties();
		};
	public:
		//! One row in table, implicitly shared.
		class QFCORE_DECL_EXPORT Row
		{
			friend class QFSqlQueryTable;
			friend class QFTableModel;
			//friend class QFSqlQueryModel;
			protected:
				struct OrigValue
				{
					QVariant value;
					bool dirty;

					OrigValue() : dirty(false) {}
				};
				class Data : public QSharedData
				{
					friend class Row;
					public:
						QVector<QVariant> values;
						QVector<QVariant> origValues;
						QBitArray dirtyFlags;
						TableProperties tableProperties; 
						bool insert;
					public:
						Data() {}
						Data(const TableProperties &props);
				};
			private:
				QSharedDataPointer<Data> d;
				static const Row& sharedNull();
				Row(bool); // create NULL row
			public:
				QVector<QVariant>& valuesRef() {return d->values;}
				const QVector<QVariant>& values() const {return d->values;}
				QVariantMap valuesMap() const;
				//QBitArray& nullFlagsRef() {return d->nullFlags;}
			public:
				void saveValues();
				void restoreValues();
				void clearEditFlags();
				void clearOrigValues(); 
				//! Uvede radek do ModeInsert a nastavi vsem fieldum dirty flag
				void prepareForCopy();
			protected:
				void setInitialValue(int col, const QVariant &v) throw(QFException);
			public:
				QVariant origValue(int col, bool throw_exc = Qf::ThrowExc) const throw(QFException);
				QVariant origValue(const QString &field_name, bool throw_exc = Qf::ThrowExc) const throw(QFException);
				//bool hasNullFlag(int col) const throw(QFException);
				QVariant value(int col, bool throw_exc = Qf::ThrowExc) const throw(QFException);
				QVariant value(const QString &field_name, bool throw_exc = Qf::ThrowExc) const throw(QFException);
				//! Dirty flag nastavi, jen kdyz je value jina, nez ta, co uz tam byla.
				void setValue(int col, const QVariant &v) throw(QFException);
				void setValue(const QString &field_name, const QVariant &v) throw(QFException);
				// Checks if \a field_ix is not out of range.
				//bool isValidFieldIndex(int field_ix) const;
				/*
				int fieldNameToIndex(const QString &field_name) const throw(QFException)
				{
					return fields().fieldNameToIndex(field_name);
				}
				*/
				bool isDirty() const;
				bool isDirty(int field_no) const throw(QFException);
				void setDirty(int field_no, bool val = true) throw(QFException);

				bool post() throw(QFException);
				bool drop() throw(QFException);
				int reload() throw(QFException);
				void fillDefaultAndAutogeneratedValues() throw(QFException);
				
				//! returns number of fields in the row.
				int fieldCount() const {return fields().count();}
				//! returns number of values in the row, Shoul be the same as \a fieldCount() .
				int count() const {return d->values.count();}
				//! pomocna informace pro sorteni, jinak nepouzivat.
				//int rowNo() const {return d->rowNo;}
				//void clearDirty();
			//public:
				bool isInsert() const {return d->insert;}
				void setInsert(bool b = true) {d->insert = b;}
				bool isNull() const {return d == sharedNull().d;}
				//void setFields(const FieldList& flds) {d->fields = flds;}
				const TableProperties& tableProperties() const {return d->tableProperties;}
				//TableProperties& tablePropertiesRef() {return d->tableProperties;}
				const FieldList& fields() const {return d->tableProperties.fields();}
				//FieldList& fieldsRef() {return d->tableProperties.fieldsRef();}
			//public:
				//static Row nullRow();
				//bool operator<(const Row &r) const;

				QString toString(const QString &sep = "\t") const;
			public:
				Row();
				Row(const TableProperties &props);
		};
		typedef QList<Row> RowList;
	protected:
		class Data : public QSharedData
		{
			public:
				//! Properties of table usefull for Row (fields, sorting info etc.). Each row has implicitly shared copy of this struct.
				TableProperties tableProperties;
				RowList rows; ///< copy of ResultSet
				//QStringList tableids; ///< list of tablenames in active query
				RowIndexList rowIndex; ///< rows are accessed through this index (sorting, filtering, ...)
				//int currentRow; ///< index of current row in \a index
				SortDefList sortedFields;
				QFFlags<Flags> flags;
			public:
				Data() {flags << EditRowsAllowed << InsertRowsAllowed << DeleteRowsAllowed;}
				~Data() { }
		};
	private:
		class SharedDummyHelper {};
		QFBasicTable(SharedDummyHelper); /// null row constructor
		static const QFBasicTable& sharedNull();
	private:
		QSharedDataPointer<Data> d;
	public:
		bool isNull() const {return d == sharedNull().d;}
		const TableProperties& tableProperties() const {return d->tableProperties;}
		///Return unfiltered, unsorted list of rows;
		const RowList& roughRows() const {return d->rows;}
		const FieldList& fields() const {return tableProperties().fields();}
		//const RowIndexList& rowIndex() const {return d->rowIndex;}
		const RowList& rows() const {return d->rows;}

		QVariant sumValue(int field_ix) const throw(QFException);
		QVariant sumValue(const QString &field_name) const throw(QFException)
		{
			return sumValue(fields().fieldNameToIndex(field_name));
		}

		bool isEditRowsAllowed() const {return d->flags.contains(EditRowsAllowed);}
		void setEditRowsAllowed(bool b = true) {if(b) d->flags << EditRowsAllowed; else d->flags >> EditRowsAllowed;}
		bool isInsertRowsAllowed() const {return d->flags.contains(InsertRowsAllowed);}
		void setInsertRowsAllowed(bool b = true) {if(b) d->flags << InsertRowsAllowed; else d->flags >> InsertRowsAllowed;}
		bool isDeleteRowsAllowed() const {return d->flags.contains(DeleteRowsAllowed);}
		void setDeleteRowsAllowed(bool b = true) {if(b) d->flags << DeleteRowsAllowed; else d->flags >> DeleteRowsAllowed;}
	protected:
		RowList& rowsRef() {return d->rows;}
		//! clears all rows, if \a fields_options tells what else will be cleared.
		void cleanupData(CleanupDataOption fields_options);
		int rowNumberToRowIndex(int rowno) const;
		void createRowIndex();
		const RowIndexList& rowIndex() const {return d->rowIndex;}
		RowIndexList& rowIndexRef() {return d->rowIndex;}
	public:
		TableProperties& tablePropertiesRef() {return d->tableProperties;}
		FieldList& fieldsRef() {return tablePropertiesRef().fieldsRef();}
	public:
		QFSqlField& fieldRef(int fld_ix) throw(QFException);
		QFSqlField& fieldRef(const QString& field_name) throw(QFException)
		{
			return fieldRef(fields().fieldNameToIndex(field_name));
		}
		QFSqlField field(int fld_ix, bool throw_exc = true) const throw(QFException);
		QFSqlField field(const QString& field_name, bool throw_exc = true) const throw(QFException)
		{
			return field(fields().fieldNameToIndex(field_name), throw_exc);
		}

		Row& rowRef(int rowno) throw(QFException);
		//! \sa rowRef(int)
		Row row(int i, bool throw_exc = !Qf::ThrowExc) const throw(QFException);
		/// vraci posledni radek, pokud neni a je throw_exc vrha exception, jinak vraci null row.
		Row lastRow(bool throw_exc = !Qf::ThrowExc) const throw(QFException);
		//static Row nullRow() {return Row::sharedNull();}
	public:
		//! Clear all except column definitions.
		void clearData() {cleanupData(ClearRows);}
		//! frees all allocated resources (fields, columns, rows, indexes, etc.)
		void clear() {cleanupData(ClearFieldsRows);}

		bool isEmpty() const {return rowCount() <= 0;}
		bool isValidRow(int row) const;
		bool isValidField(int fld_ix) const {
			return fields().isValidFieldIndex(fld_ix);
		}
	protected:
		virtual bool removeRowAndDrop(int ri, bool call_driver_drop) throw(QFException);
		virtual Row& insertRow(int before_row, bool fill_default_and_auto_values) throw(QFException);
	public:
		//! vrati prazdny radek, ktery neprida do tabulky
		Row singleRow();
		virtual int rowCount() const;
		virtual int columnCount() const;
		virtual Row& insertRow(int before_row) throw(QFException) {
			return insertRow(before_row, true);
		}
		virtual Row& insertRowNoFillDefaultAndAutoValues(int before_row) throw(QFException) {
			return insertRow(before_row, false);
		}
		/// vlozi  _row, do tabulky, pokud je to radek z jine tabulky (neziskany metodou singleRow()), tak exception
		/// vetsinou se pouziva pro vlozeni radku ziskaneho funkci singleRow()
		virtual Row& insertRow(int before_row, const Row &_row) throw(QFException);
		virtual Row& appendRow() throw(QFException) {return insertRow(rowCount());}
		virtual Row& appendRow(const Row &_row) throw(QFException) {return insertRow(rowCount(), _row);}
		virtual bool removeRow(int ri) throw(QFException) {
			return removeRowAndDrop(ri, true);
		}
		virtual bool removeRowNoDrop(int ri) throw(QFException) {
			return removeRowAndDrop(ri, false);
		}
		// other related
		void revertRow(int ri);
		virtual void revertRow(Row &r);
		virtual bool postRow(Row &r) throw(QFException);
		bool postRow(int ri) throw(QFException);

		virtual void reload();
		//int reloadRow(int ri) throw(QFException);
		virtual int reloadRow(QFBasicTable::Row &r) throw(QFException) {Q_UNUSED(r); return 1;}
	signals:
		/// @todo pripraviji basic table ne to, ze uz nebude potomkem QObject
		//void reloaded();
		//void progressValue(double val, const QString &text = QString()) const;
	protected:
		virtual void sort(RowIndexList::iterator begin, RowIndexList::iterator end);
		virtual RowIndexList::const_iterator binaryFind(RowIndexList::const_iterator begin, RowIndexList::const_iterator end, const QVariant &val) const;
	public:
		/// sort / search / seek
		//! @param colnames Coma separaded list of fields with optional ASC or DES  and CS or ICS flag.
		//!		CS is CaseSensitive, default is CS.
		//!						For example "id, name DESC CS, salary"
		void sort(const QString &colnames) throw(QFException);
		//! For convinience.
		void sort(const SortDef &sorted_field) throw(QFException);
		virtual void sort(const SortDefList &sorted_fields) throw(QFException);
		//! pokud chci zavolta sort jen pro urcitou objast radku
		virtual void sort(const SortDefList &sorted_fields, int start_row_index, int row_count) throw(QFException);
		void resort() throw(QFException);
		//! returns index of \a val or -1. Table must be sorted ascending.
		//! Function seeks in the first sorted field.
		//! If val is QVariantList, function seek in all val values up to sortedFields.count()
		int seek(const QVariant &val) const;
		/// Prochazi tabulku radek po radku, pokus nic nenajde vraci -1
		int find(int field_ix, const QVariant &val) const;
		int find(const QString &field_name, const QVariant &val) const;
		/*
		int seek(int field_index, const QVariant &val) const;
		int seek(const QString &field_name, const QVariant &val) const
		{
			int seek_ix = fields().fieldNameToIndex(field_name);
			return seek(seek_ix, val);
		}
		*/
	public:
		 // export / import
		QString toString() const;
		static const QString CVSTableEndTag;
		void exportCSV(QTextStream &ts, const QString col_names = "*", TextExportOptions opts = TextExportOptions()) const throw(QFException);
		void importCSV(QTextStream &ts, TextImportOptions opts = TextImportOptions()) throw(QFException);
		void importTXT(QTextStream &ts, const QString &file_structure_definition) throw(QFException);
		
		QDomElement toHtmlElement(QDomDocument &owner_doc, const QString & col_names = QString()) const throw( QFException );
		//QDomElement exportXML(const QString col_names = "*") const throw(QFException);
		QFXmlTable toXmlTable(QDomDocument &owner_doc, const QString &table_name = QString(), const QString &col_names = QString()) const throw(QFException);
		QVariantMap toJson(const QString &col_names = QString()) const throw(QFException);
		/// ulozi data v tabulce jako QVariantList QVariantListu (kazdy radek je jeden QVariantList)
		QVariantList dataToVariantList() const;
		void dataFromVariantList(const QVariantList &_lst) throw(QFException);
	public:
		struct QFCORE_DECL_EXPORT ColumnDef
		{
			QString name;
			QVariant::Type type;
			int length;
			quint8 prec;

			ColumnDef(const QString &_name, QVariant::Type _type = QVariant::Invalid, int _len = -1,  quint8 _prec = 0)
				: name(_name), type(_type), length(_len), prec(_prec) {}
			ColumnDef(const QString &_name, const QString &_def);
		};
		typedef QList<ColumnDef> ColumnDefList;
	public:
		QFBasicTable();
		QFBasicTable(const QStringList &col_names);
		QFBasicTable(const ColumnDefList &col_defs);
		virtual ~QFBasicTable();
};

//typedef QFBasicTable::Row QFBasicTableRow;

#endif // QFBASICTABLE_H

