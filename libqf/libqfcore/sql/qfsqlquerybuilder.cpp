
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqlquerybuilder.h"

#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

QFSqlQueryBuilder::QFSqlQueryBuilder(const QString &query_to_parse)
{
	d = new Data;
	parse(query_to_parse, !Qf::ThrowExc);
}

QFSqlQueryBuilder::~QFSqlQueryBuilder()
{
}

QString QFSqlQueryBuilder::buildQuery() const
{
	qfTrash() << QF_FUNC_NAME;
	QFString s, qs;
	s = buildSelect();
	if(!!s) qs = "SELECT " + s;
	s = buildFrom();
	if(!!s) qs += " FROM " + s;
	s = buildWhere();
	if(!!s) qs += " WHERE " + s;
	s = buildGroupBy();
	if(!!s) qs += " GROUP BY " + s;
	s = buildHaving();
	if(!!s) qs += " HAVING " + s;
	s = buildOrderBy();
	if(!!s) qs += " ORDER BY " + s;
	s = buildRest();
	if(!!s) qs += " " + s;
	if(d->queryMap.contains(AsKey)) qs = "(" + qs + ") AS " + d->queryMap[AsKey];
	qfTrash() << "\t" << qs;
	return qs;
}

QString QFSqlQueryBuilder::build(QueryMapKey key) const
{
	QFString s;
	if(d->queryMap.contains(key)) s += d->queryMap[key];
	return s;
}

QString QFSqlQueryBuilder::buildSelect() const
{
	static const QueryMapKey key = SelectKey;
	QFString s;
	if(d->queryMap.contains(SelectFlagsKey)) s += d->queryMap[SelectFlagsKey] + " ";
	if(d->columnBenList.isEmpty()) {
		s += build(key);
	}
	else {
		QStringList sl = d->queryMap.value(key).split(",");
		QStringList sl2;
		foreach(QString s1, sl) {
			QFString s2 = s1;
			int ix = s2.lastIndexOf("AS", -1, Qt::CaseInsensitive);
			if(ix > 0) s2 = s2.slice(ix + 3);
			if(!d->columnBenList.contains(s2.trimmed())) sl2 << s1;
		}
		/// at je tam alespon neco
		if(sl2.isEmpty() && !sl.isEmpty()) sl2 << sl[0];
		s += sl2.join(", ");
	}
	return s;
}

void QFSqlQueryBuilder::setColumnBenned(const QString &column_name, bool b)
{
	if(b) {
		QString s = column_name.trimmed();
		if(s.isEmpty()) return;
		/// id sloupce nedej na benlist ani, kdyz to nekdo chce
		/// @todo neurcovat id sloupce podle jmena ale zeptat se na primary index.
		if(s.endsWith("id", Qt::CaseInsensitive) || s.endsWith(".id", Qt::CaseInsensitive)) return;
		d->columnBenList << s;
	}
	else d->columnBenList.remove(column_name);
}

/*
QString QFSqlQueryBuilder::buildFrom() const
{
	static const QueryMapKey key = FromKey;
	QFString s;
	if(d->queryMap.contains(key)) s = d->queryMap[key].toStringList().join(" ");
	return s;
}

QString QFSqlQueryBuilder::buildWhere() const
{
	static const QueryMapKey key = WhereKey;
	QFString s;
	if(d->queryMap.contains(key)) s = d->queryMap[key].toStringList().join(" ");
	return s;
}

QString QFSqlQueryBuilder::buildGroupBy() const
{
	static const QueryMapKey key = GroupByKey;
	QFString s;
	if(d->queryMap.contains(key)) s = d->queryMap[key].toStringList().join(" ");
	return s;
}

QString QFSqlQueryBuilder::buildHaving() const
{
	static const QueryMapKey key = HavingKey;
	QFString s;
	if(d->queryMap.contains(key)) s = d->queryMap[key].toStringList().join(" ");
	return s;
}

QString QFSqlQueryBuilder::buildOrderBy() const
{
	static const QueryMapKey key = OrderByKey;
	QFString s;
	if(d->queryMap.contains(key)) s = d->queryMap[key].toStringList().join(" ");
	return s;
}
*/
QString QFSqlQueryBuilder::buildRest() const
{
	QFString s, ret;
	s = build(LimitKey);
	if(!!s) ret += " LIMIT " + s;
	s = build(OffsetKey);
	if(!!s) ret += " OFFSET " + s;
	return ret;
}

QFSqlQueryBuilder& QFSqlQueryBuilder::select(const QString &fields, const QString &flags)
{
	static const QueryMapKey key = SelectKey;
	if(!flags.isEmpty()) d->queryMap[SelectFlagsKey] = flags;
	QFString s = d->queryMap[key];
	if(!!s) s += ", ";
	s += fields;
	d->queryMap[key] = s;
	return *this;
}

QFSqlQueryBuilder & QFSqlQueryBuilder::select2(const QString & table_name, const QString & fields, const QString & flags)
{
	QFString s = fields;
	QStringList sl = s.splitAndTrim(',', '\'');
	QStringList sl2;
	foreach(s, sl) sl2 << table_name + "." + s;
	s = sl2.join(",");
	return select(s, flags);
}

QFSqlQueryBuilder& QFSqlQueryBuilder::from(const QString &_table_name)
{
	static const QueryMapKey key = FromKey;
	qfTrash() << QF_FUNC_NAME;
	QString table_name = _table_name.trimmed();
	//QFString s = d->queryMap[key];
	int ix = table_name.lastIndexOf(' ');
	qfTrash() << "\t table_name:" << table_name << "ix:" << ix;
	if(ix < 0) d->recentlyMentionedRelation = table_name.trimmed();
	else d->recentlyMentionedRelation = table_name.mid(ix + 1).trimmed();
	d->queryMap[key] = table_name;
	qfTrash() << "\t recentlyMentionedRelation:" << d->recentlyMentionedRelation;
	return *this;
}

QFSqlQueryBuilder& QFSqlQueryBuilder::from(const QFSqlQueryBuilder &table)
{
	QString table_query = table.toString();
	return from(table_query);
}

QFSqlQueryBuilder& QFSqlQueryBuilder::join(const QString &t1_key, const QString &_t2_name, const QString &t2_key, const QString  &join_kind)
{
	qfTrash() << QF_FUNC_NAME;
	static const QueryMapKey key = FromKey;
	QString t1_id = t1_key.trimmed();
	QString t1_alias = d->recentlyMentionedRelation;
	int ix = t1_id.lastIndexOf('.');
	qfTrash() << "\t t1_id:" << t1_id << "ix:" << ix;
	if(ix > 0) {
		t1_alias = QFString(t1_id).slice(0, ix).trim();
		t1_id = QFString(t1_id).slice(ix+1).trim();
	}
	qfTrash() << "\t recentlyMentionedRelation:" << d->recentlyMentionedRelation;
	QString t2_name = _t2_name.trimmed();
	QString t2_alias = t2_name;
	ix = t2_name.lastIndexOf(' ');
	if(ix > 0) {
		t2_alias = QFString(t2_name).slice(ix+1).trim();
	}
	QFString fs = join_kind + " " + t2_name + " ON " + t1_alias + "." + t1_id + "=" + t2_alias + "." + t2_key;
	QFString s = d->queryMap[key];
	if(!!s) s += " ";
	s += fs;
	d->queryMap[key] = s;
	d->recentlyMentionedRelation = t2_alias;
	return *this;
}

QFSqlQueryBuilder& QFSqlQueryBuilder::join(const QString &join)
{
	qfTrash() << QF_FUNC_NAME;
	static const QueryMapKey key = FromKey;
	QFString s = d->queryMap[key];
	if(!!s) s += " ";
	s += join;
	d->queryMap[key] = s;
	return *this;
}

QFSqlQueryBuilder& QFSqlQueryBuilder::join(const QString &t1_key, const QString &t2_key)
{
	qfTrash() << QF_FUNC_NAME;
	QString t2_id = t2_key.trimmed();
	QString t2_alias;
	qfTrash() << "\t recentlyMentionedRelation:" << d->recentlyMentionedRelation;
	int ix = t2_id.lastIndexOf('.');
	if(ix > 0) {
		//d->recentlyMentionedRelation = QFString(t1_key).slice(0, ix).trim();
		t2_id = QFString(t2_key).slice(ix+1).trim();
		t2_alias = QFString(t2_key).slice(0, ix).trim();
	}
	else {
		qfWarning() << QF_FUNC_NAME << t2_key << "t2_key should hav form tablename.idfieldname";
	}
	qfTrash() << "\t recentlyMentionedRelation:" << d->recentlyMentionedRelation;
	return join(t1_key, t2_alias, t2_id);
}

QFSqlQueryBuilder& QFSqlQueryBuilder::join(const QString &t1_key, const QFSqlQueryBuilder &t2, const QString &t2_key, const QString  &join_kind)
{
	QString table_query = t2.toString();
	return join(t1_key, table_query, t2_key, join_kind);
}

QFSqlQueryBuilder& QFSqlQueryBuilder::where(const QString &cond, const QString &oper)
{
	static const QueryMapKey key = WhereKey;
	if(!cond.isEmpty()) {
		QFString s = d->queryMap[key];
		if(!!s) s += " " + oper + " ";
		d->queryMap[key] = s + "(" + cond + ")";
	}
	return *this;
}

QFSqlQueryBuilder& QFSqlQueryBuilder::groupBy(const QString &expr)
{
	static const QueryMapKey key = GroupByKey;
	QFString s = d->queryMap[key];
	if(!!s) s += " ";
	d->queryMap[key] = s + expr;
	return *this;
}

QFSqlQueryBuilder& QFSqlQueryBuilder::having(const QString &cond, const QString &oper)
{
	static const QueryMapKey key = HavingKey;
	QFString s = d->queryMap[key];
	if(!!s) s += " " + oper + " ";
	d->queryMap[key] = s + "(" + cond + ")";
	return *this;
}

QFSqlQueryBuilder& QFSqlQueryBuilder::orderBy(const QString &field_and_order_flag)
{
	static const QueryMapKey key = OrderByKey;
	QFString s = d->queryMap[key];
	if(!!s) s += ", ";
	d->queryMap[key] = s + field_and_order_flag;
	return *this;
}

QFSqlQueryBuilder& QFSqlQueryBuilder::limit(int n)
{
	d->queryMap[LimitKey] = QString::number(n);
	return *this;
}

QFSqlQueryBuilder& QFSqlQueryBuilder::offset(int n)
{
	d->queryMap[OffsetKey] = QString::number(n);
	return *this;
}

QFSqlQueryBuilder& QFSqlQueryBuilder::as(const QString &alias_name)
{
	d->queryMap[AsKey] = alias_name;
	return *this;
}

QString QFSqlQueryBuilder::take(QueryMapKey key)
{
	QString ret = d->queryMap.take(key);
	//qfInfo() << __LINE__ << ret;
	//foreach(QueryMapKey i, d->queryMap.keys()) qfInfo() << "\t" << i << ":" << d->queryMap[i];
	return ret;
}

static int keyword_pos(const QString &str, const QString &keyword)
{
    // tady je trochu problem, ze se musi brat v uvahu dvoje uvozovky ' a "
    // navic where muze byt ve vnorenym selectu
	QFString fs = str, kw = keyword.toLower();
	int pos;
	int parcnt = 0;
	bool in_apos = false;
	bool in_quotes = false;
	for(pos=0; pos<fs.len(); pos++) {
		QChar c = fs[pos].toLower();
		if(c == '\'') {in_apos = !in_apos; continue;}
		if(c == '"') {in_quotes = !in_quotes; continue;}
		if(!in_apos && !in_quotes) {
			if(c == '(') {parcnt++; continue;}
			else if(c == ')') {parcnt--; continue;}
		}
		if(parcnt == 0 && c == kw[0]) {
			if(fs.slice(pos, pos + kw.len()).toLower() == kw) break;
		}
	}
	if(pos >= fs.len()) pos = -1;
	return pos;
}

struct KeyPair
{
	QString sqlName;
	QFSqlQueryBuilder::QueryMapKey key;
	KeyPair(const QString &sql_name, QFSqlQueryBuilder::QueryMapKey _key) : sqlName(sql_name), key(_key) {}
};

bool QFSqlQueryBuilder::parse(const QString &query_string, bool throw_exc) throw(QFSqlException)
{
	qfTrash() << QF_FUNC_NAME;
	qfTrash() << "\toriginal:" << query_string;
	d->recentlyParsedQuery = query_string;
	QFString s = query_string.trimmed();
	d->queryMap.clear();
	if(!s) return true;
	static QList<KeyPair> keywords;
	if(keywords.isEmpty()) {
		//keywords << KeyPair("SELECT", SelectKey);
		keywords << KeyPair("FROM", FromKey);
		keywords << KeyPair("WHERE", WhereKey);
		keywords << KeyPair("GROUP BY", GroupByKey);
		keywords << KeyPair("HAVING", HavingKey);
		keywords << KeyPair("ORDER BY", OrderByKey);
		keywords << KeyPair("LIMIT", LimitKey);
		keywords << KeyPair("OFFSET", OffsetKey);
	}

	bool can_parse = true;
	if(keyword_pos(s, "SELECT") != 0) {
		if(throw_exc) QF_SQL_EXCEPTION(TR("query does not start with SELECT statement\n%1").arg(query_string));
		can_parse = false;
	}
	s = s.slice(QFString("SELECT").len());

	if(can_parse) do{
		//int ix = 0;
		/*
		if((ix = keyword_pos(s, "FROM")) < 0) {
			//if(throw_exc) QF_SQL_EXCEPTION(TR("query does not contain FROM statement\n%1").arg(query_string));
			break;
		}
		d->queryMap[SelectKey] = s.slice(0, ix).trim();
		s = s.slice(ix + QFString("FROM").len()).trim();
		*/
		QueryMapKey curr_key = SelectKey;
		foreach(KeyPair kw, keywords) {
			QueryMapKey found_key = InvalidKey;
			int ix = keyword_pos(s, kw.sqlName);
			if(ix < 0) {
				qfTrash() << "\t" << kw.sqlName << ": NOT FOUND";
				continue;
			}
			found_key = kw.key;
#if 0
			{
				QString curr_keyword = "SELECT";
				foreach(KeyPair kw, keywords) {
					if(kw.key == curr_key) {
						curr_keyword = kw.sqlName;
						break;
					}
				}
				qfTrash() << "\t" << curr_keyword << ":" << s.slice(0, ix).trim();
			}
#endif
			d->queryMap[curr_key] = s.slice(0, ix).trim();
			s = s.slice(ix + kw.sqlName.length());
			curr_key = found_key;
		}
		d->queryMap[curr_key] = s.trim();
#if 0
		{
			QString curr_keyword;
			foreach(KeyPair kw, keywords) {
				if(kw.key == curr_key) {
					curr_keyword = kw.sqlName;
					break;
				}
			}
			qfTrash() << "\t" << curr_keyword << ":" << s.trim();
		}
#endif
		qfTrash() << "\treconstructed:" << toString();
		return true;
	} while(false);
	d->queryMap.clear();
	return false;
}


