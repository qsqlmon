//
// C++ Implementation: qfsqlquery
//
// Description:
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include <QSqlDriver>
#include <QSqlError>

//#include <qfapplication.h>

#include "qfsqlcatalog.h"

#include "qfsqlquery.h"

//#include <QSqlResult>

#include <qflogcust.h>

//=========================================
//                          QFSqlQuery
//=========================================
QFSqlQuery::QFSqlQuery(const QFSqlConnection& conn)
	: QSqlQuery(conn)
{
	//qfTrash() << QF_FUNC_NAME << "BEGIN";
	d = new Data();
	d->connection = conn;
	//dump();
	//qfTrash() << QF_FUNC_NAME << "END";
}
/*
QFSqlQuery::QFSqlQuery(DefaultConnection def_conn)
{
	d = new Data();
	if(def_conn == ApplicationConnection) {
		d->connection = qfApp()->connection();
	}
}
*/
QFSqlQuery::QFSqlQuery(const QSqlQuery& other, const QFSqlConnection& conn)
	: QSqlQuery(other)
{
	d = new Data();
	d->connection = conn;
}

QFSqlQuery::QFSqlQuery(QSqlResult *result, QFSqlConnection conn)
	: QSqlQuery(result)
{
	//qfTrash() << QF_FUNC_NAME << "BEGIN";
	d = new Data();
	d->connection = conn;
	//dump();
	//qfTrash() << QF_FUNC_NAME << "END";
}

bool QFSqlQuery::exec() throw(QFException)
{
	bool ok = QSqlQuery::exec();
	if(!ok || (lastError().type() != QSqlError::NoError)) {
		//if(throw_exc) {
			QSqlError err = lastError();
			QString s  = err.driverText() + QF_EOLN + err.databaseText();
			QF_SQL_EXCEPTION(s);
		//}
		return false;
	}
	return true;
}

bool QFSqlQuery::exec(const QString &_query_str, bool throw_exc) throw(QFException)
{
	QFString query_str = _query_str.trimmed();
	static const int trash_qstr_len_max = 200;
	qfTrash() << "QFSqlQuery::exec() - Executing ["
			<< ((query_str.len() > trash_qstr_len_max)? query_str.slice(0, trash_qstr_len_max) + " ... ": query_str)
			<< "]";
	if(query_str.isEmpty()) {
		if(throw_exc)  QF_SQL_EXCEPTION("Empty query.");
		return false;
	}
	if(!connection().isOpen()) {
		if(throw_exc) QF_SQL_EXCEPTION("Connection is not open. " + connection().signature());
		return false;
	}
	bool ok = QSqlQuery::exec(query_str);
	if(!ok) {
		if(throw_exc) {
			QSqlError err = lastError();
			QString s  = err.driverText() + QF_EOLN + err.databaseText();
			s += QF_EOLN;
			s += QF_EOLN;
			s+= query_str;
			QF_SQL_EXCEPTION(s);
		}
		return false;
	}
	d->lastSelect = query_str.trimmed();
	//d->lastSelect = result()->executedQuery().trimmed(); executedQuery je protected
	if(!d->lastSelect.isEmpty()) {
		QFSqlJournal *journal = connection().journal();
		if(journal) {
			QFString s = d->lastSelect;
			if(s[-1] != ';') s = s + ';';
			journal->log(s);
		}
	}
	return true;
}

bool QFSqlQuery::exec(const QString &query, const ScriptParams &params, bool throw_exc) throw(QFException)
{
	QFString s = query;
	QMapIterator<QString, QString> i(params);
	while (i.hasNext()) {
		i.next();
		s = s.replace("${" + i.key() + "}", i.value());
	}
	return exec(s, throw_exc);
}

/*
QFSqlQuery::RowModes QFSqlQuery::rowMode() const
{
	//qfTrash() << "QFSqlQuery::rowMode()" << "current mode is" << d->rowMode;
	return d->rowMode;
}

void QFSqlQuery::setRowMode(QFSqlQuery::RowModes mode)
{
	d->rowMode = mode;
	//qfTrash() << "QFSqlQuery::setRowMode()" << "mode set to" << mode;
}
*/
QFSqlConnection QFSqlQuery::connection() const
{
	return d->connection;
}
/*
QFSqlConnection& QFSqlQuery::connection()
{
	return d->connection;
}
*/
bool QFSqlQuery::execScript(const QString &script_text, const QFSqlQuery::ScriptParams &params, bool throw_exc) throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	bool ret = true;
	QFString s = script_text;
	QStringList cmds = s.split(';');
	foreach(s, cmds) {
		QStringList sl = s.split('\n');
		s = "";
		foreach(QFString s1, sl) {
			if(s1.ltrim().startsWith("-- ")) continue;
			if(s1.trim().isEmpty()) continue;
			s += s1.trim() + " ";
		}
		if(!s.trim()) continue;
		qfTrash().noSpace() << "\texecuting '" << s << "'";
		ret = ret && exec(s, params, throw_exc);
	}
	return ret;
}

QString QFSqlQuery::loadScript(QFile & f, const QString & script_name) throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	if(!f.isOpen()) 	if(!f.open(QIODevice::ReadOnly)) QF_EXCEPTION(TR("Nelze otevrit soubor '%1'").arg(f.fileName()));
	QString s;
	if(!script_name.isEmpty()) {
		QStringList lines;
		QByteArray ba;
		while(!(ba = f.readLine()).isEmpty()) lines << QString(ba);
		bool append = false;
		QStringList script_lines;
		const QString s1 = "-- QF_SCRIPT " + script_name + " {";
		const QString s2 = "-- } QF_SCRIPT";
		foreach(s, lines) {
			s = s.simplified();
			if(s.startsWith(s1)) {append = true; continue;}
			if(append) {
				if(s.startsWith(s2)) break;
				script_lines << s;
			}
		}
		if(script_lines.isEmpty()) QF_EXCEPTION(TR("Skript '%1' nebyl nalezen").arg(script_name));
		s = script_lines.join("\n");
		//qfDebug() << "\tSCRIPT:" << script_name << "\n" << s;
	}
	else {
		s = f.readAll();
	}
	f.close();
	return s;
}

bool QFSqlQuery::execScript(QFile &f, const QString &script_name, const QFSqlQuery::ScriptParams &params, bool throw_exc) throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	if(!f.isOpen()) 	if(!f.open(QIODevice::ReadOnly)) QF_EXCEPTION(TR("Nelze otevrit soubor '%1'").arg(f.fileName()));
	QString s = loadScript(f, script_name);
	return execScript(s, params, throw_exc);
}


int QFSqlQuery::fieldIndex(const QString & col_name, bool throw_exc) const throw( QFException )
{
	QSqlRecord rec = record();
	int ix;
	for(ix=0; ix<rec.count(); ix++) {
		if(QFSql::endsWith(rec.field(ix).name(), col_name)) break;
	}
	if(ix >= rec.count()) {
		ix = -1;
		if(throw_exc) {
			QString s = TR("Column named '%1' not found in query record.\nAvailable columns are:\n").arg(col_name);
			for(int i=0; i<rec.count(); i++) s += rec.field(i).name() + "\n";
			QF_EXCEPTION(s);
		}
	}
	return ix;
}

QVariant QFSqlQuery::value(const QString &col_name, bool throw_exc) const throw(QFException)
{
	QVariant ret;
	int ix = fieldIndex(col_name, throw_exc);
	if(ix >= 0) ret = QSqlQuery::value(ix);
	return ret;
}

QVariantList QFSqlQuery::fetchAll(int col) 
{
	QVariantList ret;
	while(next()) ret << value(col);
	return ret;
}

QVariantList QFSqlQuery::fetchAll(const QString & col_name, bool throw_exc) throw( QFException )
{
	QVariantList ret;
	int ix = fieldIndex(col_name, throw_exc);
	if(ix >= 0) while(next()) ret << value(ix);
	return ret;
}



