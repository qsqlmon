#ifndef QFSQLCONNECTIONBASE_H
#define QFSQLCONNECTIONBASE_H

#include <QString>
#include <QMap>
#include <QSqlDatabase>
#include <QSqlIndex>
#include <QSharedData>
#include <QStringList>

#include <qfcoreglobal.h> 
#include "qfsql.h"
#include "qfsqlfield.h"
#include <qfexception.h>
//#include <qfexplicitlyshareddata.h>

/// class used to wrap QSqlDatabase to extend its functionality
/**
kazde QFSqlConnectionBase ma signaturu. Pokud chce referenci na svuj QFSqlCatalog,
vola se funkce catalog(), ta vezme signaturu tohoto pripojeni, podiva se do staticke
mapy katalogu a vrati ten se stejnou signaturou nebo exception.
 */
class QFCORE_DECL_EXPORT QFSqlConnectionBase : public QSqlDatabase
{
	public:
		typedef QMap<QString, QString> ConnectionOptions;
		//enum DriverType {UNSUPPORTED_DRIVER, PSQL, FPSQL, MYSQL, SQLITE};
		struct IndexInfo {
			QString name;
			bool unique;
			bool primary;
			QStringList fields;

			IndexInfo() : unique(false), primary(false) {}
		};
		typedef QList<IndexInfo> IndexList;
	public:
		//static QString codecName;
	public:
		// use QSqlDatabase::isValid() instead.
		//bool hasValidDriver() const {return (driver() && driver()->handle().toInt() != 0);}
		
		//! pomocna funkce pro debugging
		static QString cash2string(const QString &indent = QString());

		//! @return list of fields in table or view
		QStringList fields(const QString& tbl_name) const throw(QFSqlException);
		
		/// @return list of available tables
		QStringList tables(const QString& dbname = QString::null, QSql::TableType type = QSql::Tables) const;

		/// @return list of indexes for table \a tbl_name .
		IndexList indexes(const QString& tbl_name) const;
	
		/// @return list of available databases
		QStringList databases() const;
	
		/// @return list of available schemas in current connection
		QStringList schemas() const;
		
		QSqlIndex primaryIndex(const QString& tblname);
		
		QSqlRecord record(const QString & tablename) const;

		/// @return kind of relname.
		/// \sa RelationKindKind
		QFSql::RelationKind relationKind(const QString& relname);
	
		/**
		 * @return string unique per user,database_name,host,driver
		 */
		QString signature() const;
		//static QString signature2driverName(const QString &sig);
		//! Returns human readable textual information about current connection.
		QString info(int verbosity = 1) const;
	
		bool isOpen() const;
		/// if successfull, lastError() returns information about connection.
		void open(const ConnectionOptions &options = ConnectionOptions()) throw(QFSqlException);
		void open(const QString & user, const QString & password, const ConnectionOptions &options = ConnectionOptions()) throw(QFSqlException)
		{
			setUserName(user);
			setPassword(password);
			open(options);
		}
		void close();
				
		static int defaultPort(const QString &driver_name);

	protected:
		//void initCurrentSchema();
	public:
		//! Ma smysl jen kvuli mySql a obsahuje jmeno naposledy volane prikazem USE.
		//! Pro SQLITE je to main a pro PSQL public.
		QString currentSchema() const;
		//! U MySQL vola USE \a schema_name .
		void setCurrentSchema(const QString &schema_name) throw(QFSqlException);

		//! retrieves CREATE TABLE ... Sql script for \a tblname.
		QString createTableSqlCommand(const QString &tblname);

		//! retrieves INSERT INTO ... Sql script for \a tblname.
		QString dumpTableSqlCommand(const QString &tblname);

		//! take CREATE TABLE ... and parse fields definitions from it.
		static QStringList fieldDefsFromCreateTableCommand(const QString &cmd);
				
		//! convert n to form .dbname.tblname.fldname
		QString normalizeFieldName(const QString &n) const;
		//! convert n to form .dbname.tblname (.schemaname.tblname for PSQL)
		QString normalizeTableName(const QString &n) const;
		//! if \a n is empty return name of default database/schema for active connection.
		QString normalizeDbName(const QString &n) const;

		QStringList serverVersion() const;

		//! Qt driver nekdy neumi pracovat s nazvem tabulky schema.table, tak z toho udelej jen table, to uz umi.
		QString fullTableNameToQtDriverTableName(const QString &full_table_name) const;
	public:
		QFSqlConnectionBase();
		explicit QFSqlConnectionBase(const QSqlDatabase& qdb);
		explicit QFSqlConnectionBase(const QString &driver_name);
		//explicit QFSqlConnectionBase(QSqlDriver *drv);
		virtual ~QFSqlConnectionBase();
};

#endif // QFSQLCONNECTION_H
