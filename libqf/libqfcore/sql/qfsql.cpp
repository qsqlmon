//
// C++ Implementation: qfsql
//
// Description: 
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include <qfstring.h>
#include "qfsql.h"

#include <qflogcust.h>

QString QFSql::relationKindAsString(QFSql::RelationKind kind)
{
	static const char *kinds[] = {"UnknownRelation", "TableRelation", "ViewRelation", "SystemTableRelation", "IndexRelation", "SequenceRelation", "AllRelations"};
	//enum RelationKind {UnknownRelation, TableRelation, ViewRelation, IndexRelation};
	//static const int kind_cnt = sizeof(kinds) / sizeof(const char*);
	//if(kind < 0 || kind >= kind_cnt) kind = UnknownRelation;
	//return kinds[kind];
	int ix = -1;
	switch(kind) {
		case QFSql::UnknownRelation: ix = 0; break;
		case QFSql::TableRelation: ix = 1; break;
		case QFSql::ViewRelation: ix = 2; break;
		case QFSql::SystemTableRelation: ix = 3; break;
		case QFSql::IndexRelation: ix = 4; break;
		case QFSql::SequenceRelation: ix = 5; break;
		case QFSql::AllRelations: ix = 6; break;
	}
	if(ix >= 0) return kinds[ix];
	return "UnresolvedRelationNumber";
}

void QFSql::parseFullName(const QString& full_field_name, QString *field_name, QString *table_name, QString *db_name)
{
	QFString s = full_field_name;
	if(field_name) *field_name = s;
	if(table_name) *table_name = QString();
	if(db_name) *db_name = QString();

	int ix = s.lastIndexOf('.');
	if(ix < 0) return;
	if(field_name) *field_name = s.mid(ix+1);
	s = s.mid(0, ix);
	if(table_name) *table_name = s;
	
	ix = s.lastIndexOf('.');
	if(ix < 0) return;
	if(table_name) *table_name = s.mid(ix+1);
	s = s.mid(0, ix);
	//if(s[0] == '.') s = s.slice(1);
	if(db_name) *db_name = s;
}
		
QString QFSql::composeFullName(const QString& field_name, const QString& table_name, const QString& db_name)
{
	QFString ret = field_name;
	if(!table_name.isEmpty()) {
		if(!ret.startsWith('.')) ret = "." + ret;
		ret = table_name + ret;
		if(!db_name.isEmpty()) {
			if(!ret.startsWith('.')) ret = "." + ret;
			ret = db_name + ret;
			//if(!ret.startsWith('.')) ret = "." + ret;
		}
	}
	//if(ret == ".") ret = "";
	return ret;
	/*
	QFString ret = field_name;
	if(!table_name.isEmpty()) {
		if(!!ret) ret += ".";
		ret = table_name + ret;
		if(!db_name.isEmpty()) {
			if(!!ret) ret += ".";
			ret = db_name + ret;
		}
	}
	return ret;
	*/
}
/*		
static QString prependDot(const QString &fieldname)
{
	static const QString S_DOT = ".";
	if(fieldname.startsWith(S_DOT)) return fieldname;
	return S_DOT + fieldname;
}
*/
/*		
static QString cutDot(const QString &fieldname)
{
	if(fieldname.startsWith(".")) return fieldname.mid(1);
	return fieldname;
}
*/
bool QFSql::endsWith(const QString &field_name1, const QString &field_name2)
{
	//if(field_name1.toLower() == "zakazky.kontaktid" || field_name2.toLower() == "zakazky.kontaktid") qfTrash() << "QFSql::endsWith() field_name1:" << field_name1 << "field_name2:" << field_name2;
	/// psql (podle SQL92) predelava vsechny nazvy sloupcu, pokud nejsou v "" do lowercase, ale mixedcase se lip cte, tak at se to sparuje.
	int i;
	int l1 = field_name1.length();
	int l2 = field_name2.length();
	if(l2 > l1) return false;
	for(i=0; i<l1 && i<l2; i++) {
		QChar c1= field_name1[l1-i-1].toLower();
		QChar c2= field_name2[l2-i-1].toLower();
		//if(field_name1.toLower() == "zakazky.kontaktid" || field_name2.toLower() == "zakazky.kontaktid") qfTrash() << "\t c1:" << c1 << "c2:" << c2;
		if(c1 != c2) return false;
	}
	if(l1 == l2) return true; /// jsou stejne dlouhy, musi bejt stejny
	/// prvni je delsi, ale jeste muze mit tecku (napr. zakazky.nazev a nazev)
	if(field_name1[l1 - l2 - 1] == '.') return true;
	return false;
	/*
	QString s1 = prependDot(field_name1).toLower();
	QString s2 = prependDot(field_name2).toLower();
	return s1.endsWith(s2);
	*/
}

bool QFSql::sqlIdCmp(const QString &sql_id1, const QString &sql_id2)
{
	if(sql_id1.isEmpty() || sql_id2.isEmpty()) return false;
	if(endsWith(sql_id1, sql_id2)) return true;
	if(endsWith(sql_id2, sql_id1)) return true;
	return false;
}
		
QString QFSql::nullString()
{
	static QString s = "null";
	return s;
}

QString QFSql::escapeXmlForSql(const QString &xml_str)
{
	QString s = xml_str;
	s = s.replace("'", "''");
	return s;
}
		
QString QFSql::formatValue(const QVariant &val)
{
	QString ret = val.toString();
	switch(val.type()) {
		case QVariant::String: ret = "'" + ret + "'"; break;
		default: break;
	}
	return ret;
}

QString QFSql::escapeJavaScriptForSQL(const QString & code)
{
	QString ret = code;
	///ret.replace('\'', "\\'"); tohle podporuje jen mysql
	ret.replace('\'', "''"); /// tohle by mel byt SQL standard
	return ret;
}
/*
QString QFSql::unescapeJavaScriptFromSQL(const QString & str)
{
	QString ret = str;
	ret.replace("\\'", "'");
	return ret;
}
*/
QString QFSql::escapeSqlForSQL(const QString & code)
{
	QString ret = code;
	ret.replace('\'', "''");
	return ret;
}
/*
QString QFSql::unescapeSqlFromSQL(const QString & str)
{
	QString ret = str;
	ret.replace("\\'", "'");
	return ret;
}
*/
