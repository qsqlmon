//
// C++ Interface: qfsqlfield
//
// Description:
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef QFSQLFIELD_H
#define QFSQLFIELD_H

#include <QSqlField>
#include <QSharedDataPointer>

#include <qfcoreglobal.h>
//#include "qfclassfield.h"

/**
@author Fanda Vacek
*/
class QFCORE_DECL_EXPORT QFSqlField : public QSqlField
{
	protected:
		struct Data : public QSharedData
		{
			unsigned char canupdate:1, prikey:1;//, autoincrement:1;//, sortAscending:1;dirty:1,
			QString fullname;
			//QString caption;
			//QString tooltip;
			//int displaywidth;
			Data() : canupdate(false), prikey(false) {}
		};
	public:
		// indexes of fields in qfData() QVariantList
		//enum {TableNameIndex=0};
	private:
		QSharedDataPointer<Data> d;
	public:
		static const QFSqlField& sharedNull();
		bool isNull() const {return this->d == sharedNull().d;}
		//! true if field is primary key.
		bool isPriKey() const {return d->prikey;}
		void setPriKey(bool b = true) {d->prikey = b;}
		/// QSqlField ma autoValue property
		//bool isAutoIncrement() const {return d->autoincrement;}
		//void setAutoIncrement(bool b = true) {d->autoincrement = b;}
		//! true if updates in field can be posted to database.
		bool isUpdateable() const {return d->canupdate;}
		void setUpdateable(bool b = true) {d->canupdate = b;}
		bool isNullable() const {
			/// drivery nastavuji requiredStatus() jako !nullable
			return (requiredStatus() == QSqlField::Optional);
		}
		//! fully qualified field name (database.table.fieldname)
		//! return as much information about field name as knows
		QString fullName() const;
		void setFullName(const QString& n) {d->fullname = n;}
		//! field name returned by driver
		QString driverReportedName() const {return QSqlField::name();}
		//! only field part of fullName()
		QString fieldName() const;
		//! set when query goes to ModeReadWrite.
		QString fullTableName() const;
		/**
		 * @return name of table from which this field originates
		 */
		QString tableName() const;
		//void setTableName(const QString &name);
		//! @return database name for field
		QString dbName() const;
		//! PSQL uses schema, wehere MYSQL or SQLITE uses database.
		/// \sa dbName()
		QString schemaName() const {return dbName();}
		//void setSchema(const QString &name) {data->schema = name;}
		/*
		 * using PSQL tableID == table oid, else tableID == tableName
		 * @return
		 */
		//QString tableId() const;
		//void setTableId(const QString &id);
		/*
		const QVariant& editedValue() const {return data.editedValue;}
		QVariant& editedValue() {return data.editedValue;}
		*/
		bool isReadOnly() const;
		//! is true if the field has all information for generatin the UPDATE SQL command
		bool canUpdate() {return d->canupdate;}
		void setCanUpdate(bool b = true) {d->canupdate = b;}

		//! if value is '' and field type is numeric, set 0 instead ''.
		void setValue(const QVariant & value);

		QString toString(const QString &indent = QString()) const;

		//bool isDirty() const {return d->dirty;}
		//void setDirty(bool b = true) {d->dirty = b;}
		/*
		QString caption() const {return (d->caption.isEmpty())? fullName(): d->caption;}
		QFSqlField& setCaption(const QString &s) {d->caption = s; return *this;}
		QString toolTip() const {return d->tooltip;}
		QFSqlField& setToolTip(const QString &s) {d->tooltip = s; return *this;}
		int displayWidth() const {return d->displaywidth;}
		QFSqlField& setDisplayWidth(int w) {d->displaywidth = w; return *this;}
		*/
	private:
		QFSqlField(bool); // shared null constructor
	public:
		QFSqlField();
		QFSqlField(const QString & reported_name, QVariant::Type type = QVariant::Invalid);
		QFSqlField(const QFSqlField &other);
		QFSqlField(const QSqlField &other);
};

#endif
