#include <QVariant>
#include <QSqlDriver>
#include <QSqlRecord>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlDatabase>
#include <QStringList>
#include <QMutableListIterator>

#include "qfsqlconnection.h"
#include "qfsqlcatalog.h"
#include "qfsqlquery.h"
#include "qfsql.h"
#include "qfstring.h"

#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>
	
//=========================================
//                                       QFSqlConnection
//=========================================
QFSqlConnection::Data::~Data()
{
	//qfTrash() << QF_FUNC_NAME;
	SAFE_DELETE(catalog);
}

QFSqlConnection::QFSqlConnection()
	: QFSqlConnectionBase()
{
	d = new Data();
}

QFSqlConnection::QFSqlConnection(const QSqlDatabase& qdb)
	: QFSqlConnectionBase(qdb)
{
	d = new Data();
}

QFSqlConnection::QFSqlConnection(const QString &driver_name)
	: QFSqlConnectionBase(driver_name)
{
	d = new Data();
}

QFSqlConnection::~QFSqlConnection()
{
	//qfTrash() << QF_FUNC_NAME << signature();
	//qfTrash() << "\tref:" << ((const QFSqlConnection*)this)->d->ref;
}

void QFSqlConnection::close()
{
	qfTrash() << QF_FUNC_NAME << signature() << "isOpen():" << isOpen();
	if(isOpen()) {
		QFSqlConnectionBase::close();
		SAFE_DELETE(d->catalog);
	}
} 

void QFSqlConnection::open(const QFSqlConnectionBase::ConnectionOptions &options) throw(QFSqlException)
{
	qfTrash() << QF_FUNC_NAME << signature();
	close();
	//QString sig = signature();
	//QFSqlCatalog::catalogMap().remove(sig);
	QFSqlConnectionBase::open(options);
	SAFE_DELETE(d->catalog);
	qfTrash() << "\tcreating catalog";
	d->catalog = new QFSqlCatalog(*this);
}

QFSqlCatalog& QFSqlConnection::catalog() const throw(QFSqlException)
{
	if(!d->catalog) QF_SQL_EXCEPTION("catalog is NULL");
	return *(d->catalog);
}

