
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDBXMLCONFIG_H
#define QFDBXMLCONFIG_H

#include <qfcoreglobal.h>

#include <qfxmlconfig.h>

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFDbXmlConfig : public QFXmlConfig
{
	protected:
		QString f_dbxmlTableName;
	public:
		/// jmeno tabulky, ktera obsahuje definice xmlconfigu pro \a dbxmlkey .
		static const QString defaultDbxmlTableName;
		void setDbxmlTableName(const QString &tbl_name) {f_dbxmlTableName = tbl_name;}
		QString dbxmlTableName() const;
		/// ckey je klic do tabulky dbxmlTableName
		void loadTemplateDocument(const QString &ckey, bool throw_exc = Qf::ThrowExc) throw(QFException);
		void setDataDocument(const QString &xml_str) throw(QFException);
		void setDataDocument(const QFXmlConfigDocument &data_document) {QFXmlConfig::setDataDocument(data_document);}
	public:
		virtual ~QFDbXmlConfig() {}
};

#endif // QFDBXMLCONFIG_H

