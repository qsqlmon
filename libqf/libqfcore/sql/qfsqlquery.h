//
// C++ Interface: qfsqlquery
//
// Description: 
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef QFSQLQUERY_H
#define QFSQLQUERY_H

#include <QVariant>
#include <QVector>
#include <QList>
#include <QMap>
#include <QFile>
#include <QSqlQuery>
#include <QSqlDriver>

#include <qfcoreglobal.h>
#include <qfsqlconnection.h>
//#include <qfsqlresult.h>
#include <qfclassfield.h>

//class QFSqlQueryPrivate;

/**
Updateable SQL query

@author Fanda Vacek
*/
class QFCORE_DECL_EXPORT QFSqlQuery : public QSqlQuery
{
	friend class QFSqlQueryModel;
	public:
		//enum DefaultConnection {ApplicationConnection = 1};
		typedef QMap<QString, QString> ScriptParams;
	protected:
		class QFCORE_DECL_EXPORT Data : public QSharedData
		{
			public:
				//QFSqlQuery::Modes mode;
				//FieldList fields; ///< each row has implicitly shared copy of this fields. This fields are created after SELECT
				//QStringList tableids; ///< list of tablenames in active query
				QFSqlConnection connection;
				QString lastSelect;
				//bool sqliteFullColumnNames;
			public:
				//Data() : mode(QFSqlQuery::ModeReadOnly), currentRow(0), fieldsPreparedForUpdates(false), sqliteFullColumnNames(true) { }
				Data() { }
				~Data() { }
			public:
				//void dump() const;
		};
	private:
		QSharedDataPointer<Data> d;
	public:
		QFSqlConnection connection() const;

	protected:
		//QFSqlConnection& connection();
	
		/**
		* scan result set fields and return true if all primary key fields of \a tablename are found in it.
		*/
		bool resultHasAllPriKeys(const QString &tableid) const throw(QFException);
		/*
		 * nebude to potreba defaultni hodnoty si zjistim jinak
		 *  Reloads \a row from server.
		 * @param row row to reload
		 * @param row_must_exist if reloaded row does not exist (after change of field, which value breaks WHERE condition),
		 * 		an exception is thrown or FALSE is returned.
		 * @param throw_exc 
		 * @return TRUE if the row is successfully reloaded
		 */
		//bool reloadRow(int row, bool row_must_exist = true, bool throw_exc = true) throw(QFException);
	protected:
		QFSqlQuery(QSqlResult *result, QFSqlConnection conn);
		/*!
		 * I've to hide this constructor, in other case (if it is not introduced), calling
		 * QFSqlQuery q(str, conn) causes QFSqlQuery(QSqlQuery(str, conn), const QFSqlConnection& conn);
		 */
		QFSqlQuery(const QString& s, const QFSqlConnection &conn = QFSqlConnection());
	public:	
		QString lastSelect() const {return d->lastSelect;}
	
		//bool command(const QString &_query_str, bool throw_exc = true) throw(QFException);
		//bool command(const QString &_query_str, const ScriptParams &params, bool throw_exc = true) throw(QFException);
		bool exec() throw(QFException);
		bool exec(const QString &query_str, bool throw_exc = Qf::ThrowExc) throw(QFException);
		//! @param params map of parameters to replace. In script are all ocurences of ${param_key} replaced by params[param_key].
		bool exec(const QString &query_str, const ScriptParams &params, bool throw_exc = Qf::ThrowExc) throw(QFException);
		bool execScript(const QString &script_text, const ScriptParams &params = ScriptParams(), bool throw_exc = Qf::ThrowExc) throw(QFException);
		//! @param script_name \a script_file can contain more scripts. Script starts with '-- script_name {' and ends with '-- }'
		bool execScript(QFile &script_file, const QString &script_name, const ScriptParams &params = ScriptParams(), bool throw_exc = Qf::ThrowExc) throw(QFException);
		bool execScript(QFile &script_file, const ScriptParams &params = ScriptParams(), bool throw_exc = Qf::ThrowExc) throw(QFException) {
			return execScript(script_file, QString(), params, throw_exc);
		}
		static QString loadScript(QFile &script_file, const QString &script_name = QString()) throw(QFException);

		int fieldIndex(const QString &col_name, bool throw_exc = Qf::ThrowExc) const  throw(QFException);
		QVariant value(int col) const {return QSqlQuery::value(col);}
		QVariant value(const QString &col_name, bool throw_exc = Qf::ThrowExc) const  throw(QFException);
		QVariantList fetchAll(int col);
		QVariantList fetchAll(const QString &col_name, bool throw_exc = Qf::ThrowExc) throw(QFException);
	public:
		//QFSqlQuery(const QString &query_str = QString(), const QFSqlConnection &conn = QFSqlConnection());
		QFSqlQuery(const QFSqlConnection &conn = QFSqlConnection());
		// Construct connected query.
		//QFSqlQuery(DefaultConnection def_conn);
		//QFSqlQuery(const QFSqlQuery& other);
		QFSqlQuery(const QSqlQuery& other, const QFSqlConnection& conn);
		virtual ~QFSqlQuery() {}
};

#endif
