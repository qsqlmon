
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqlquerytable.h"

//#include <qfapplication.h>
#include <qfsqlquerybuilder.h>
#include <qfcsvreader.h>
#include <qfdatetime.h>
#include <qfsqlquery.h>
#include <qfsqlstoragedriver.h>

#include <qflogcust.h>


//==========================================================
//                                    QFSqlQueryTable
//==========================================================
QFSqlQueryTable::QFSqlQueryTable(const QFSqlConnection &_connection)
	: QFBasicTable()
{
	TableProperties &tp = tablePropertiesRef();
	tp.storageDriverPropertiesRef() = QVariant::fromValue(QFSqlStorageDriverProperties(_connection));
	tp.setStorageDriver(&QFSqlStorageDriver::storageDriver);
}

QFSqlQueryTable::~QFSqlQueryTable()
{
}

QFSqlStorageDriverProperties QFSqlQueryTable::storageDriverProperties() const
{
	const TableProperties &tp = tableProperties();
	//const QFSqlStorageDriverProperties &props = qvariant_cast<const QFSqlStorageDriverProperties &>(tp.storageDriverProperties());
	//qvariant_cast<const QFSqlStorageDriverProperties&>(tableProperties().storageDriverProperties());
	return qvariant_cast<QFSqlStorageDriverProperties>(tp.storageDriverProperties());
}

void QFSqlQueryTable::setStorageDriverProperties(const QFSqlStorageDriverProperties & props)
{
	TableProperties &tp = tablePropertiesRef();
	tp.storageDriverPropertiesRef() = QVariant::fromValue(props);
}

void QFSqlQueryTable::setQueryBuilder(const QFSqlQueryBuilder & qb)
{
	QFSqlStorageDriverProperties props = storageDriverProperties();
	props.queryBuilderRef() = qb;
	setStorageDriverProperties(props);
}

void QFSqlQueryTable::addForeignKey(const QString & master_key_name, const QString & detail_key_name)
{
	QFSqlStorageDriverProperties props = storageDriverProperties();
	props.foreignKeyMapRef()[master_key_name.toLower()] = detail_key_name;
	setStorageDriverProperties(props);
}
/*
void QFSqlQueryTable::setTableIds(const QStringList & table_ids)
{
	QFSqlStorageDriverProperties props = storageDriverProperties();
	props.tableIdsRef() = table_ids;
	setStorageDriverProperties(props);
}
*/
/*
QFSqlConnection& QFSqlQueryTable::connectionRef() throw(QFException)
{
	//qfTrash() << QF_FUNC_NAME;
	if(d->connection.isValid()) return d->connection;
	QFSqlConnection &conn = qfApp()->connection();
	if(!conn.isValid()) {
		QF_EXCEPTION(TR("QFSqlQueryModel::connection() - connection is not valid!"));
	}
	//qfTrash() << "\tusing application default connection:" << conn.signature();
	return conn;
}
*/
/*
bool QFSqlQueryTable::removeRow(int ri) throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	if(!isValidRow(ri)) {
		//qfWarning() << QF_FUNC_NAME << "removing invalid row" << ri;
		return false;
	}
	Row r = row(ri);
	deleteRowInDatabase(r);
	return QFBasicTable::removeRow(ri);
}
*/
/*
bool QFSqlQueryTable::postRow(int ri) throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	bool ret = false;
	if(!isValidRow(ri)) {
		qfWarning() << QF_FUNC_NAME << "posting nonexisting row" << ri;
		return ret;
	}
	ret = postRow(rowRef(ri));
	return ret;
}
*/
/*
bool QFSqlQueryTable::postRow(QFBasicTable::Row &r) throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	bool ret = postRowToDatabase(r);
	if(ret) ret = QFBasicTable::postRow(r);
	return ret;
}
*/
int QFSqlQueryTable::reload(const QString _query_str) throw(QFException)
{
	qfLogFuncFrame() << this;
	//bool query_built = false;
	int ret = 0;
	QFSqlStorageDriverProperties driver_props = storageDriverProperties();
	QFSqlStorageDriver *storage_driver = dynamic_cast<QFSqlStorageDriver*>(tableProperties().storageDriver());
	QF_ASSERT(storage_driver, "blbej driver");
	
	QFString query_str = _query_str.trimmed();
	bool new_query = true;
	new_query = !query_str.isEmpty();
	if(!new_query) {
		if(driver_props.queryBuilder().isEmpty()) query_str = driver_props.queryBuilder().recentlyParsedQuery();
		else query_str = driver_props.queryBuilder().toString();
		//query_built = true;
	}
	QFSqlConnection conn = driver_props.connection();
	QFSqlQuery q = QFSqlQuery(conn);
	qfTrash() << "\texecuting query:" << query_str;
	//qfInfo() << QFLog::stackTrace();
	//QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	if(query_str.isEmpty()) {
		qfWarning() << "QFSqlQueryTable::reload() - empty query";
		qfInfo() << QFLog::stackTrace();
	}
	else try {
		if(q.exec(query_str)) {
			qfTrash() << "\tOK";
			ret = q.numRowsAffected();
			if(q.isSelect()) {
				/// builder umi parsovat jen SELECTy, proto az tady.
				if(new_query) {
					QFSqlQueryBuilder qb;
					qb.parse(query_str, !Qf::ThrowExc);
					driver_props.queryBuilderRef() = qb;
				}
				cleanupData(ClearFieldsRows);
				QSqlRecord rc = q.record();
				qfTrash() << "\tFIELDS:";
				for(int i=0; i<rc.count(); i++) {
					QFSqlField fld(rc.field(i));
					QString fn;
					qfTrash() << "\t\tdriver reported name:" << fld.driverReportedName();
					if(conn.driverName().endsWith("MYSQL")) {
						fn = fld.driverReportedName();
					}
					else {
						fn = conn.normalizeFieldName(fld.driverReportedName());
					}
					fld.setFullName(fn);
					qfTrash() << "\t\tappending field:" << fld.toString() << "type:" << QVariant::typeToName(fld.type());
					fieldsRef().append(fld);
				}
				qfTrash() << "\tgetting table ids:";
				QStringList table_ids;
				foreach(QFSqlField f, fields()) {
					QFString s = f.fullTableName();
					qfTrash() << "\t\tfieldname:" << f.name() << "fullTableName:" << s;
					if(conn.driverName().endsWith("MYSQL")) {
						int ix = s.lastIndexOf('.');
						if(ix > 0) {
							/// pokud sloupec neobsahuje jmeno databaze, je to pocitany sloupec
							if(table_ids.contains(s)) continue;
							table_ids << s;
						}
					}
					/*
					else if(conn.driverName().endsWith("SQLITE")) {
						int ix = s.lastIndexOf('.');
						if(ix > 0) {
							/// pokud sloupec neobsahuje jmeno databaze, je to pocitany sloupec
							if(table_ids.contains(s)) continue;
							table_ids << s;
						}
					}
					*/
					else {
						if(table_ids.contains(s)) continue;
						table_ids << s;
					}
				}
				//qfInfo() << "\t table ids:" << table_ids.join(", ");
				driver_props.tableIdsRef() = table_ids;
				
				/// set updateable flag.
				try {
					/// pokud se stane, ze z nejakyho duvodu nebude fungovat katalog,
					/// zobrazi se select alespon read only
					foreach(QString s, driver_props.tableIds()) {
						qfTrash() << "\ttry to enable can update for" << s;
						if(storage_driver->resultHasAllPriKeys(conn, fields(), s)) {
							for(int i=0; i<fields().count(); i++) {
								QFSqlField &fld = fieldRef(i);
								if(fld.fullTableName() != s) continue;
								//qfTrash() << "\t\tOK";
								fld.setUpdateable(true);
							}
						}
					}
				}
				catch(QFException &e) {
					qfError() << "Catalog internal error:" << e.msg() << e.where();
				}

				tablePropertiesRef().storageDriverPropertiesRef() = qVariantFromValue(driver_props);
				
				/// buffer whole result set
				qfTrash() << "\tVALUES:";
				int row_no = 0;
				while(q.next()) {
					Row r(tableProperties());
					for(int i=0; i<fields().count(); i++) {
						QVariant v = q.value(i);
					/*
						if(v.type() == QVariant::Date) {
						QFDate d = v.toDate();
						d.setNull(q.isNull(i));
						v.setValue(d);
					}
					*/
						//qfTrash().noSpace() << "\t\t'" << v.toString() << "' type:" << QVariant::typeToName(v.type()) << " isNull:" << v.isNull() << " isValid:" << v.isValid();
						if(false && v.isValid() && q.isNull(i)) {
							qfInfo().noSpace() << "\t\t'" << v.toString() << "' type:" << QVariant::typeToName(v.type()) << " isNull:" << v.isNull() << " isValid:" << v.isValid();
							qfError() << "Error getting value col:" << i << "row:" << row_no;
						}
						r.setInitialValue(i, v);
					}
				//qfTrash() << "\t\t-------------";
					//r.clearOrigValues();
					rowsRef().append(r);
				}

			//if(connection().driverName().endsWith("MYSQL")) {
			//}

			// build index
			//createRowIndex();
			// free result
			// set RO flag if there are not all prikeys for this field table
			/*
			*/
			}
			if(new_query) {
				//qfInfo() << "destroy old sort definition";
				sort(QString());
			}
			QFBasicTable::reload();
		//foreach(QFSqlField fld, d->fields) qfTrash() << fld.toString() << QFEOLN;
		//qfTrash() << "\tOUT last select:" << lastSelect();
		}
		else {
			//qfInfo() << "EEE???";
		}
	}
	catch(...) {
		//QApplication::restoreOverrideCursor();
		throw;
	}
	//QApplication::restoreOverrideCursor();
	return ret;
}

int QFSqlQueryTable::reloadRow(QFBasicTable::Row &r) throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	return r.reload();
}








