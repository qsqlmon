//
// C++ Implementation: QFSqlCatalog
//
// Description:
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include <QSqlQuery>
#include <QSqlDriver>

#include "qfstring.h"
#include "qfsqlcatalog.h"
#include "qfsqlquery.h"
//#include "qfsqlconnectionbase.h"

#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

//====================================
//                           QFSqlCatalog
//====================================
/*
QFSqlCatalog::CatalogMap& QFSqlCatalog::catalogMap()
{
	static CatalogMap map;
	return map;
}

QFSqlCatalog& QFSqlCatalog::catalogRef(const QString& connection_signature) throw(QFSqlException)
{
	if(!catalogMap().contains(connection_signature))
		QF_SQL_EXCEPTION(TR("signature %1 not found in catalog map").arg(connection_signature));
	return catalogMap()[connection_signature];
}
*/

QFSqlCatalog::QFSqlCatalog(const QFSqlConnectionBase &con)
	: _d(con), d(&_d)
{
	//qfLogFuncFrame();
	//qfTrash() << "\tdatabases cnt:" << d->databases.count();
}

QFSqlCatalog::~QFSqlCatalog()
{
	//qfLogFuncFrame();
}

QStringList QFSqlCatalog::databases() throw(QFSqlException)
{
	if(!databasesUpToDate()) {
		qfLogFuncFrame();
		//qfTrash() << "\tempty catalog, create entries, driver:" << d->driver;
		QStringList dbs;
		QFSqlConnectionBase con = connection();
		qfTrash() << "\tcon driver name:" << con.driverName() << "is open:" << con.isOpen();
		//qfTrash() << "\tdriver name:" << d->driver->metaObject()->className();
		/*
		if(con.driverName().endsWith("PSQL") || con.driverName().endsWith("SQLITE")) {
			dbs = con.schemas();
		}
		else {
			dbs = con.schemas();
		}
		*/
		dbs = con.schemas();
		QSet<QString> dbs2 = QSet<QString>::fromList(d->databases.keys());
		foreach(QString s, dbs) {
			dbs2.remove(s);
			s = QFSql::composeFullName(s);
			if(!d->databases.contains(s)) {
				qfTrash() << "\tadding to cache:" << s;
				d->databases[s] = QFSqlDbInfo(this, s);
			}
		}
		foreach(QString s, dbs2) forgetTable(s);
		d->databasesUpToDate = true;
	}
	return d->databases.keys();
}

QFSqlDbInfo* QFSqlCatalog::databaseRef(const QString& _dbname, bool throw_exc) throw(QFSqlException)
{
	qfLogFuncFrame();
	QString dbname;
	QFSql::parseFullName(_dbname, &dbname);
	//dbname = QFSql::composeFullName(dbname);
	dbname = connection().normalizeDbName(dbname);
	qfTrash().noSpace() << "\tlooking for '" << dbname << "'";
	QFSqlDbInfo dbi = d->databases.value(dbname);
	if(!dbi.isValid()) {
		qfTrash() << "\t" << _dbname << "not found in cache.";
		// check if dbname exists
		//qfTrash() << "\t" << __LINE__;
		if(!databases().contains(dbname)) {
			QString s = TR("Connection does not contain database/schema name '%1'").arg(dbname);
			if(throw_exc) QF_SQL_EXCEPTION(s);
			qfTrash() << s;
			qfTrash() << "present databases:\n" << toString();
			return NULL;
		}
		qfTrash() << "\tfilling content of dbinfo" << dbname;
		d->databases[dbname] = QFSqlDbInfo(this, dbname);
		qfTrash() << "\n" << toString();
	}
	QFSqlDbInfo *ret = NULL;
	if(d->databases.contains(dbname)) ret = &(d->databases[dbname]);
	if(!ret) {
		QString s = TR("database/schema name '%1' can not be found.").arg(dbname);
		if(throw_exc) QF_SQL_EXCEPTION(s);
		qfTrash() << s;
	}
	else {
		qfTrash() << "\tOK";
	}
	return ret;
}

QFSqlTableInfo QFSqlCatalog::table(const QString& full_table_name, bool throw_exc) throw(QFSqlException)
{
	qfLogFuncFrame();
	qfTrash() << "looking for" << full_table_name;
	QString tblname,dbname;
	QFSql::parseFullName(full_table_name, &tblname, &dbname);
	return database(dbname, throw_exc).table(tblname, throw_exc);
}

QFSqlDbInfo QFSqlCatalog::database(const QString& dbname, bool throw_exc) throw(QFSqlException)
{
	qfLogFuncFrame();
	//qfTrash() << "looking for" << dbname;
	QFSqlDbInfo *ref = databaseRef(dbname, throw_exc);
	if(!ref) return QFSqlDbInfo();
	return *ref;
}

void QFSqlCatalog::forgetTable(const QString& full_table_name)
{
	QString tblname,dbname;
	QFSql::parseFullName(full_table_name, &tblname, &dbname);
	database(dbname).forgetTable(tblname);
}

void QFSqlCatalog::touchTables(const QString &dbname)
{
	qfLogFuncFrame() << dbname;
	QFSqlDbInfo *ref = databaseRef(dbname, !Qf::ThrowExc);
	if(ref) ref->touchTables();
}

void QFSqlCatalog::forgetDatabase(const QString& dbname)
{
	qfLogFuncFrame() << dbname;
	d->databases.remove(dbname);
	d->databasesUpToDate = false;
}

void QFSqlCatalog::forgetAll()
{
	d->databases.clear();
	d->databasesUpToDate = false;
}

QString QFSqlCatalog::currentSchema() const
{
	if(d->currentSchema.isEmpty()) {
		//const_cast<QFSqlCatalog*>(this)->
		d->currentSchema = connection().currentSchema();
	}
	return d->currentSchema;
}

void QFSqlCatalog::setCurrentSchema(const QString &schema_name) throw(QFSqlException)
{
	connection().setCurrentSchema(schema_name);
	d->currentSchema = connection().currentSchema();
}

QString QFSqlCatalog::toString(const QString &indent) const
{
	QString s;
	QTextStream ts(&s);
	ts << indent << "[catalog]" << endl;// << connection().signature() << endl;
	QMapIterator<QString, QFSqlDbInfo> it(d->databases);
	QString ind = indent + "  ";
	while (it.hasNext()) {
		it.next();
		ts << ind << "'" << it.key() << "'" << endl;
		ts << it.value().toString(ind+"  ") << endl;
	}
	ts.flush();
	return s;
}

QStringList QFSqlCatalog::serverVersion() const
{
	if(d->serverVersion.isEmpty()) {
		const_cast<QFSqlCatalog*>(this)->d->serverVersion = connection().serverVersion();
	}
	return d->serverVersion;
}

int QFSqlCatalog::serverVersionMajor() const
{
	QStringList sl = serverVersion();
	int ret = 0;
	if(sl.size() > 0) ret = sl[0].toInt();
	return ret; 
}

//====================================
//                           QFSqlDbInfo
//====================================
QFSqlDbInfo::QFSqlDbInfo(QFSqlCatalog * cat, const QString & full_name)
{
	d = new Data(cat, full_name);
}

const QFSqlDbInfo & QFSqlDbInfo::sharedNull()
{
	static QFSqlDbInfo n(1);
	return n;
}

/*
void QFSqlDbInfo::updateCatalog()
{
	qfLogFuncFrame();
	if(!isValid()) {
		qfWarning() << QF_FUNC_NAME << "DbInfo is not valid.";
		return;
	}
	QFSqlCatalog &cat = catalogRef();
	//qfTrash() << "\n" << cat.toString();
	QFSqlDbInfo *dbi = cat.databaseRef(dbName());
	QF_ASSERT(dbi, TR("database '%1' not found in catalog").arg(dbName()));
	*dbi = *this;
}
*/
void QFSqlDbInfo::forgetTable(const QString& name)
{
	qfLogFuncFrame() << name;
	d->tables.remove(name);
	d->tablesUpToDate = false;
}

QStringList QFSqlDbInfo::tables(int relation_kinds) throw(QFSqlException)
{
	qfLogFuncFrame() << dbName() << "isEmpty:" << d->tables.isEmpty();
	QStringList sl;
	if(!tablesUpToDate()) {
		//if(!isValid()) return sl;
		QFSqlConnectionBase conn = catalog()->connection();
		sl = conn.tables(dbName(), QSql::Tables);
		QSet<QString> orig_tables = QSet<QString>::fromList(d->tables.keys());
		foreach(QString s, sl) {
			orig_tables.remove(s);
			s = s.trimmed();
			QString s1 = QFSql::composeFullName(s, fullName());
			if(!d->tables.contains(s)) {
				qfTrash().noSpace() << "\tadding table to cache: [" << s << "] = '" << s1 << "'";
				d->tables[s] = QFSqlTableInfo(catalog(), s1, QFSql::TableRelation);
			}
		}
		sl = conn.tables(dbName(), QSql::Views);
		foreach(QString s, sl) {
			orig_tables.remove(s);
			QString s1 = QFSql::composeFullName(s, fullName());
			if(!d->tables.contains(s)) {
				qfTrash() << "\tadding view to cache:" << s;
				d->tables[s] = QFSqlTableInfo(catalog(), s1, QFSql::ViewRelation);
			}
		}
		sl = conn.tables(dbName(), QSql::SystemTables);
		foreach(QString s, sl) {
			orig_tables.remove(s);
			QString s1 = QFSql::composeFullName(s, fullName());
			if(!d->tables.contains(s)) {
				qfTrash() << "\tadding system table to cache:" << s;
				d->tables[s] = QFSqlTableInfo(catalog(), s1, QFSql::SystemTableRelation);
			}
		}
		foreach(QString s, orig_tables) forgetTable(s);
		d->tablesUpToDate = true;
	}
	if(relation_kinds == QFSql::AllRelations) return d->tables.keys();
	QStringList ret;
	TableMapIterator i(d->tables);
	while (i.hasNext()) {
		i.next();
		if(i.value().relationKind() & relation_kinds) ret << i.key();
	}
	return ret;
}

QFSqlTableInfo* QFSqlDbInfo::tableRef(const QString & _tblname, bool throw_exc) throw(QFSqlException)
{
	qfLogFuncFrame();
	//qfInfo() << QFLog::stackTrace();
	QString tblname;
	QFSql::parseFullName(_tblname, &tblname);
	qfTrash().noSpace() << "\tlooking for '" << tblname << "'";
	if(tblname.isEmpty()) {
		if(!throw_exc) return NULL;
		QF_EXCEPTION("tblname isEmpty()");
	}
	QFSqlTableInfo ti = d->tables.value(tblname);
	if(!ti.isValid()) {
		qfTrash() << "\t" << tblname << "not found in cache.";
		if(!throw_exc && !isValid()) return NULL;
		QF_ASSERT(isValid(), "not valid");
		if(!tables().contains(tblname)) {
			QString s = TR("Database/schema '%1' does not contain  table '%2'").arg(dbName()).arg(tblname);
			if(throw_exc) QF_SQL_EXCEPTION(s);
			qfLogFuncFrame() << s;
			// ochrana proti tomu, aby nekdo priradil do invalidInfo() a udelal z nej valid info.
			return NULL;
		}
		ti = d->tables.value(tblname);
	}
	if(!ti.infoLoaded()) {
		//ti = QFSqlTableInfo(catalog(), QFSql::composeFullName(tblname, dbName()));
		// load all table information except fields definition
		qfTrash() << "\tloading table info for" << tblname;
		QFSqlConnectionBase conn = catalog()->connection();
		//ti.d->kind = conn.relationKind(ti.fullName());
		qfTrash() << "\trelation kind" << QFSql::relationKindAsString(ti.relationKind());
		if(ti.relationKind() == QFSql::TableRelation) {
			// fill primary index
			ti.d->prikeys = conn.primaryIndex(ti.fullName());
		}
		ti.d->unorderedFieldNames = conn.fields(ti.fullName());
		ti.d->indexes = conn.indexes(tblname);
		ti.setInfoLoaded();
		d->tables[tblname] = ti;
		qfTrash() << "\t***TABLE INFO loaded\n" << ti.toString();
	}
	/*
	ti = d->tables[tblname];
	if(ti.isValid()) qfTrash() << "\t  OK";
	else {
		QString s = TR("Sorry, database/schema '%1' does not contain  table '%2'").arg(dbName()).arg(tblname);
		if(throw_exc) QF_SQL_EXCEPTION(s);
		qfWarning() << QF_FUNC_NAME << s;
		return NULL;
	}
	*/
	return &(d->tables[tblname]);
}

QFSqlTableInfo QFSqlDbInfo::table(const QString & tblname, bool throw_exc) throw( QFSqlException )
{
	QFSqlTableInfo *ref = tableRef(tblname, throw_exc);
	if(ref) return *ref;
	if(throw_exc) QF_SQL_EXCEPTION(tr("Table '%1' not found").arg(tblname));
	return QFSqlTableInfo();
}


QString QFSqlDbInfo::toString(const QString &indent) const
{
	QString ind = indent;
	QString s;
	QTextStream ts(&s);
	ts << ind << "[database/schema]: " << dbName() << " " << fullName() << endl;
	ind += "  ";
	QMapIterator<QString, QFSqlTableInfo> it(d->tables);
	while (it.hasNext()) {
		it.next();
		ts << ind << it.key() << endl;
		ts << it.value().toString(ind) << endl;
	}
	ts.flush();
	return s;
}

//=========================================
//                             QFSqlTableInfo
//=========================================
QFSqlTableInfo::QFSqlTableInfo(QFSqlCatalog *cat, const QString& full_name, QFSql::RelationKind rel_kind)
{
	d = new Data(cat, full_name, rel_kind);
	qfLogFuncFrame() << "rel kind:" << relationKind();
}

const QFSqlTableInfo & QFSqlTableInfo::sharedNull()
{
	static QFSqlTableInfo n(1);
	return n;
}

QString QFSqlTableInfo::tableName() const
{
	QString tbl;
	QFSql::parseFullName(fullName(), &tbl);
	return tbl;
}

QString QFSqlTableInfo::dbName() const
{
	QString tbl, db;
	QFSql::parseFullName(fullName(), &tbl, &db);
	return db;
}

const QFSqlConnectionBase::IndexList& QFSqlTableInfo::indexes()
{
	qfLogFuncFrame();
	fields();
	return d->indexes;
}

QStringList QFSqlTableInfo::fields(bool reload) throw(QFSqlException)
{
	if(!isValid()) return QStringList();
	qfLogFuncFrame();// << "reload:" << reload;
	if(!fieldsUpToDate() || reload) {
		d->fields.clear();
		qfTrash() << "\tloading fields for " << fullName();
		// load field definition from server
		QFSqlConnectionBase conn = catalog()->connection();
		QFSqlQuery q(conn);
		q.setForwardOnly(true);
		//QString s = "SELECT * FROM %1 WHERE 1=2";
		//s = s.arg(fullName());
		//q.exec(s);
		//QSqlRecord r = q.record();
		QFString full_table_name = fullName();
		QSqlRecord r = conn.record(full_table_name);
		for(int i=0; i<r.count(); i++) {
			QFSqlFieldInfo f(QFSqlField(r.field(i)), catalog());
			f.setReadOnly(false);
			f.setFullName(fullName() + "." + f.fieldName());
			qfTrash() << "\t\tfound driver reported name:" << f.driverReportedName() << "isValid():" << f.isValid() << "type:" << f.type();
			qfTrash() << "\t\tfield:" << f.toString();
			if(conn.driverName().endsWith("PSQL")) {
				// fill seqname
				QFSqlQuery q1(conn);
				q1.setForwardOnly(true);
				QFString s = fullName();
				//if(s[0] == '.') s = s.slice(1);
				q1.exec(QString("SELECT pg_get_serial_sequence('%1', '%2');").arg(s).arg(f.fieldName()));
					//qfError() << QF_FUNC_NAME << TR("Error getting the sequence information for: '%1.%2'").arg(fullName()).arg(f.fieldName());
				while(q1.next()) {
					f.setSeqName(q1.value(0).toString());
					f.setAutoIncrement(true);
					qfTrash() << "\t\tseq name:" << f.seqName();
					break;
				}
				// fill prikey flag
				if(d->prikeys.contains(f.fieldName())) f.setPriKey(true);
				//d->unorderedFieldNames.append(f.name());
			}
			else if(conn.driverName().endsWith("MYSQL")) {
				// fill prikey flag
				if(d->prikeys.contains(f.fieldName())) f.setPriKey(true);
			}
			d->fields[f.fieldName().toLower()] = f;
		}
		if(conn.driverName().endsWith("PSQL")) {
			QString s = "SELECT * FROM information_schema.columns"
					" WHERE table_name = '%1' AND table_schema = '%2'"
					" ORDER BY ordinal_position";
			s = s.arg(tableName(), dbName());
			//qfTrash() << "\t\t###################################";
			//qfTrash() << "\t\t" << s;
			q.exec(s);
			while(q.next()) {
				QFSqlFieldInfo &f = *fieldRef(q.value("column_name").toString(), Qf::ThrowExc);
				//f.setTableName(d->tablename);
				//f.setSchema(d->schema);
				// fill seqname
				f.setReadOnly(false);
				f.setFullName(fullName() + "." + q.value("column_name").toString());
				f.setDefaultValue(q.value("column_default"));
				f.setNullable(q.value("is_nullable").toString().toUpper() == "YES");
				f.setNativeType(q.value("data_type").toString());
				//qfTrash() << "\n" << catalog()->toString();
			}
		}
		if(conn.driverName().endsWith("MYSQL")) {
			int ver = catalog()->serverVersionMajor();
			QFString s;
			if(ver <= 4) {
				s = "SHOW FULL columns FROM %2.%1";
			}
			else {
				s = "SELECT * FROM information_schema.columns"
						" WHERE table_name = '%1' AND table_schema = '%2'"
						" ORDER BY ordinal_position";
			}
			s = s.arg(tableName(), dbName());
			q.exec(s);
			while(q.next()) {
				if(ver <= 4) {
					QFSqlFieldInfo &f = *fieldRef(q.value("field").toString(), Qf::ThrowExc);
					f.setReadOnly(false);
					f.setFullName(fullName() + "." + q.value("field").toString());
					f.setNullable(q.value("null").toString().toUpper() == "YES");
					QFString s_type = q.value("type").toString().toLower();
					s = s_type;
					int ix = s.indexOf("(");
					if(ix > 0) s = s.slice(0, ix);
					f.setNativeType(s);
					if(s == "enum" || s == "set") {
						int ix2 = s_type.indexOf(")");
						if(ix > 0 && ix2 > 0) {
							s = s_type.slice(ix+1, ix2);
							QStringList sl = s.splitAndTrim(',', '\'');
							f.nativeValuesRef()["enumOrSetFields"] = sl;
						}
					}
					f.setPriKey(q.value("key").toString().toUpper() == "PRI");
					f.setAutoIncrement(q.value("extra").toString().toUpper() == "AUTO_INCREMENT");
					f.setUnsigned(s_type.indexOf("unsigned") > 0);
					QVariant def_val;
					def_val = q.value("default");
					f.setComment(q.value("comment").toString());
           			         /// pokud sloupec nemuze byt NULL, nemuze byt NULL ani jeho default value.
          			          /// S vyjjimkou AUTO_INCREMENT a TIMESTAMP
					if(def_val.isNull()) {
						if(!f.isNullable() && !f.isAutoIncrement()) def_val = QVariant("");
					}
					f.setDefaultValue(def_val);
				}
				else {
					QFSqlFieldInfo &f = *fieldRef(q.value("column_name").toString(), Qf::ThrowExc);
					f.setReadOnly(false);
					f.setFullName(fullName() + "." + q.value("column_name").toString());
					f.setNullable(q.value("is_nullable").toString().toUpper() == "YES");
					f.setNativeType(q.value("data_type").toString());
					f.setPriKey(q.value("column_key").toString().toUpper() == "PRI");
					f.setAutoIncrement(q.value("extra").toString().toUpper() == "AUTO_INCREMENT");
					s = q.value("column_type").toString().toLower();
					f.setUnsigned(s.indexOf("unsigned") > 0);
					if(f.nativeType() == "enum" || f.nativeType() == "set") {
						int ix = s.indexOf("(");
						int ix2 = s.indexOf(")");
						if(ix > 0 && ix2 > 0) {
							s = s.slice(ix+1, ix2);
							QStringList sl = s.splitAndTrim(',', '\'');
							f.nativeValuesRef()["enumOrSetFields"] = sl;
						}
					}
					QVariant def_val;
					def_val = q.value("column_default");
     			               /// pokud sloupec nemuze byt NULL, nemuze byt NULL ani jeho default value.
  			                  /// S vyjjimkou AUTO_INCREMENT a TIMESTAMP
					if(def_val.isNull()) {
						if(!f.isNullable() && !f.isAutoIncrement()) def_val = QVariant("");
					}
					f.setDefaultValue(def_val);
					f.setCharacterSet(q.value("character_set_name").toString());
					f.setComment(q.value("column_comment").toString());
					if(q.value("data_type").toString() == "varchar") f.setLength(q.value("character_maximum_length").toInt());
				}
			}
		}
		else if(conn.driverName().endsWith("SQLITE")) {
			QFString fs = conn.createTableSqlCommand(fullName());
			QStringList sl = QFSqlConnectionBase::fieldDefsFromCreateTableCommand(fs);
			foreach(fs, sl) {
				// fill default values
				int ix = fs.indexOf("default", 0, Qt::CaseInsensitive);
				if(ix > 0) {
					QString nm = fs.section(' ', 0, 0);
					QFSqlFieldInfo *f = fieldRef(nm, Qf::ThrowExc);
					if(!f) {
						qfError() << QF_FUNC_NAME << TR("Found info for nonexisting field '%1'").arg(fs);
					}
					else {
						fs = fs.slice(ix);
						fs = fs.section(' ', 1, 1, QString::SectionSkipEmpty);
						if(!!fs) {
							if(fs[0] == '\'') fs = fs.slice(1, -1);
							//qfTrash() << "\tfound default value field:" << nm << "value:" << fs;
							f->setDefaultValue(fs);
						}
					}
				}
					// fill seqname
				ix = fs.indexOf("autoincrement", 0, Qt::CaseInsensitive);
				if(ix > 0) {
					QString nm = fs.section(' ', 0, 0);
					QFSqlFieldInfo *f = fieldRef(nm, Qf::ThrowExc);
					if(!f) {
						qfError() << QF_FUNC_NAME << TR("Found info for nonexisting field '%1'").arg(fs);
					}
					else {
						f->setSeqName(tableName());
						f->setAutoIncrement(true);
					}
				}
			}
		}
		d->indexes = conn.indexes(fullName());
		d->fieldsUpToDate = true;
		qfTrash() << "\t***TABLE INFO FIELDS loaded\n" << toString();
	}
	return d->unorderedFieldNames;
}

QFSqlFieldInfo* QFSqlTableInfo::fieldRef(const QString &fieldname, bool throw_exc/*, bool do_not_reload_fields*/)  throw(QFSqlException)
{
	if(!throw_exc && !isValid()) return NULL;
	qfLogFuncFrame();
	qfTrash().noSpace() << "\tlooking for: '" << fieldname << "'";
	QF_ASSERT(isValid(), "not valid");
	QString fn;
	QFSql::parseFullName(fieldname, &fn);
	fn = fn.toLower();
	if(d->fields.contains(fn)) {
		//qfTrash() << "\tfound OK";
		//qfTrash() << "\t" << d->fields[fn].toString();
		return &(d->fields[fn]);
	}
	//QF_SQL_EXCEPTION(TR("Field '%1.%2' not found in table info cash").arg(fullTableName()).arg(fieldname));
	fields(true);
	if(d->fields.contains(fn)) {
		//qfTrash() << "\tloaded OK";
		//qfTrash() << "\t" << d->fields[fn].toString();
		return &(d->fields[fn]);
	}
	if(throw_exc) {
		QString s = TR("Field %2.%1 not found and can not be loaded.").arg(fieldname).arg(tableName());
		QF_SQL_EXCEPTION(s);
	}
	//qfTrash() << "\treturning NULL";
	return NULL;
}

QString QFSqlTableInfo::toString(const QString &indent) const
{
	QString ind = indent;
	QString s;
	QTextStream ts(&s);
	ts << ind << "[table]: " << tableName() << endl;
	ind += "  ";
	ts << ind << "[primary index]: " << endl;
	QSqlIndex priix = d->prikeys;
	for(int i=0; i<priix.count(); i++) ts << ind << "  " << priix.field(i).name() << endl;
	ts << ind << "[fields]: " << endl;
	QMapIterator<QString, QFSqlFieldInfo> it(d->fields);
	while (it.hasNext()) {
		it.next();
		ts << it.value().toString(ind + "  ") << endl;
	}
	ts << ind << "[indexes]: " << endl;
	foreach(QFSqlConnectionBase::IndexInfo ii, d->indexes) {
		ts << ind << ii.name << "unique:" << ii.unique << "pri:" << ii.primary << endl;
		foreach(s, ii.fields) ts << ind << "  " << s << endl;
	}
	ts.flush();
	return s;
}

//=========================================
//                          QFSqlFieldInfo
//=========================================
const QFSqlFieldInfo & QFSqlFieldInfo::sharedNull()
{
	static QFSqlFieldInfo n(1);
	return n;
}

QVariant QFSqlFieldInfo::seqNextVal() throw(QFSqlException)
{
	QVariant ret;
	if(!isValid()) return ret;
	QFSqlConnectionBase conn = catalog()->connection();
	if(conn.driverName().endsWith("PSQL")) {
		QString s = seqName();
		if(!s.isEmpty()) {
			QSqlQuery q(conn.driver()->createResult());
			q.setForwardOnly(true);
			if(!q.exec(QString("SELECT nextval('%1');").arg(s))) {
				QF_SQL_EXCEPTION(QObject::tr("Error getting the sequence nextval('%1')").arg(s));
			}
			while(q.next()) {ret = q.value(0); break;}
		}
	}
	else if(conn.driverName().endsWith("SQLITE")) {
		QString s = seqName();
		if(!s.isEmpty()) {
			qfLogFuncFrame();
			QString t = dbName() + ".sqlite_sequence";
			QSqlQuery q(conn.driver()->createResult());
			q.setForwardOnly(true);
			bool ok = false;
			do {
				ok = q.exec(QString("SELECT seq FROM %1 WHERE name='%2'").arg(t).arg(s));
				if(!ok || !q.next()) break;
				int i = q.value(0).toInt() + 1;
				qfTrash() << "\t" << QString("UPDATE %1 SET seq = seq + 1 WHERE name='%2'").arg(t).arg(s);
				ok = q.exec(QString("UPDATE %1 SET seq = seq + 1 WHERE name='%2'").arg(t).arg(s));
				if(!ok) break;
				ok = q.exec(QString("SELECT seq FROM %1 WHERE name='%2'").arg(t).arg(s));
				if(!ok || !q.next()) break;
				int j = q.value(0).toInt();
				if(i != j) {ok = false; break;}
				qfTrash() << "\t" << "next val:" << j;
				ret = j;
				ok = true;
			} while(false);
			if(!ok) QF_SQL_EXCEPTION(QObject::tr("Error getting the sequence nextval('%1')").arg(s));
		}
	}
	return ret;
}

QString QFSqlFieldInfo::toString(const QString& indent) const
{
	QString ind = indent;
	QString s;
	QTextStream ts(&s, QIODevice::WriteOnly);
	ts << ind << "nullable: " << isNullable() << QF_EOLN;
	ts << ind << "native type: " << nativeType() << QF_EOLN;
	ts << ind << "pri key: " << isPriKey() << QF_EOLN;
	ts << ind << "auto increment: " << isAutoIncrement() << QF_EOLN;
	ts << ind << "sequence name: " << seqName() << QF_EOLN;
	ts << flush;
	s = QFSqlField::toString(ind) + s;
	return s;
}










