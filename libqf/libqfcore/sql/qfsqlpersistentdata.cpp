
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqlpersistentdata.h"

#include <qflogcust.h>

QFSqlPersistentData::QFSqlPersistentData(QFSqlPersistentData::SharedDummyHelper )
{
	d = new Data();
}

const QFSqlPersistentData & QFSqlPersistentData::sharedNull()
{
	static QFSqlPersistentData n = QFSqlPersistentData(SharedDummyHelper());
	return n;
}

QFSqlPersistentData::QFSqlPersistentData() 
{
	*this = sharedNull();
}

QFSqlPersistentData::QFSqlPersistentData(const QString &id_field_name)
{
	d = new Data();
	setIdFieldName(id_field_name);
}

QFSqlPersistentData::~QFSqlPersistentData()
{
}

bool QFSqlPersistentData::hasPersistentValue(const QString & name) const
{
	return d->persistentValues.fields().fieldIndex(name) >= 0;
}

bool QFSqlPersistentData::hasProperty(const QString & name) const
{
	return d->properties.contains(name);
}

QVariant QFSqlPersistentData::persistentValue(const QString & name, bool throw_exc) const throw( QFException )
{
	int ix = d->persistentValues.fields().fieldIndex(name);
	if(ix >= 0) return d->persistentValues.value(ix);
	if(throw_exc) QF_EXCEPTION(QString("Field '%1' does not exist.").arg(name));
	return QVariant();
}

QVariant QFSqlPersistentData::value(const QString & name, bool throw_exc) const throw( QFException )
{
	/// properties maji prednost
	if(hasProperty(name)) return d->properties.value(name);
	return persistentValue(name, throw_exc);
}

void QFSqlPersistentData::setValuesRow(const QFBasicTable & tbl)
{
	if(tbl.rowCount() == 1) setValuesRow(tbl.row(0));
	else {
		QFBasicTable::Row row = QFBasicTable::Row(tbl.tableProperties());
		row.setInsert(true);
		setValuesRow(row);
	}
}

void QFSqlPersistentData::setValue(const QString & name, const QVariant & val)
{
	if(hasPersistentValue(name)) setPersistentValue(name, val);
	else setProperty(name, val);
}

void QFSqlPersistentData::setPersistentValue(const QString & name, const QVariant & val) throw( QFException )
{
	d->persistentValues.setValue(name, val);
}

void QFSqlPersistentData::setProperty(const QString & name, const QVariant & val)
{
	d->properties[name] = val;
}

void QFSqlPersistentData::load() throw( QFException )
{
	if(!idFieldName().isEmpty()) setId(value(idFieldName()));
}

void QFSqlPersistentData::save() throw( QFException )
{
	d->persistentValues.post();
}

QVariantMap QFSqlPersistentData::valuesMap() const
{
	QVariantMap ret = d->persistentValues.valuesMap();
	QMapIterator<QString, QVariant> i(d->properties);
	while (i.hasNext()) {
		i.next();
		ret[i.key()] = i.value();
	}
	return ret;
}


