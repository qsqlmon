#include "qfsqlconnectionbase.h"
#include "qfsqlquery.h"
#include "qfsql.h"
#include "qfstring.h"

#include <QMap>
#include <QVariant>
#include <QSqlDriver>
#include <QSqlRecord>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlDatabase>
#include <QStringList>
#include <QMutableListIterator>
#include <QProcess>

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

//=========================================
//                                       QFSqlConnectionBase
//=========================================
//QString QFSqlConnectionBase::codecName;
/*
QFSqlConnectionBase::Data::Data()
{
	qfLogFuncFrame() << this << "1%%%%%%%%%%%%%%";
	qfTrash() << "\tref:" << ref;
}

QFSqlConnectionBase::Data::Data(const Data &other) : QSharedData(other)
{
	qfLogFuncFrame() << this << "2%%%%%% other" << &other;
	qfTrash() << "\tref:" << ref;
}

QFSqlConnectionBase::Data& QFSqlConnectionBase::Data::operator=(const Data &other)
{
	qfLogFuncFrame() << this << "3%%%%%%%%%%%%%";
	*this = other;
	qfTrash() << "\tref:" << ref;
	return *this;
}

QFSqlConnectionBase::Data::~Data()
{
	qfLogFuncFrame() << this << "????????????????????????";
	qfTrash() << "\tref:" << ref;
}
*/
QFSqlConnectionBase::QFSqlConnectionBase()
	: QSqlDatabase()
{
	//d = new Data();
	//initCurrentSchema();
}

QFSqlConnectionBase::QFSqlConnectionBase(const QSqlDatabase& qdb)
	: QSqlDatabase(qdb)
{
	//d = new Data();
	//initCurrentSchema();
}

QFSqlConnectionBase::QFSqlConnectionBase(const QString &driver_name) : QSqlDatabase(driver_name)
{
	//d = new Data();
	//initCurrentSchema();
	qfLogFuncFrame();
	//qfTrash() << "\tdriver name:" << driverName();
}

/*
QFSqlConnectionBase::QFSqlConnectionBase(QSqlDriver *drv)
	: QSqlDatabase(drv)
{
	qfLogFuncFrame();
	qfTrash() << "\tdriver:" << driver();
	qfTrash() << "\tdriver is open:" << driver()->isOpen();
	qfTrash() << "\tdriver name:" << driverName();
}
*/
QFSqlConnectionBase::~QFSqlConnectionBase()
{
	//qfLogFuncFrame() << "database:" << databaseName() << "is open:" << isOpen();
	//qfTrash() << "\tref:" << ((const QFSqlConnectionBase*)this)->d->ref;
}
/*
void QFSqlConnectionBase::initCurrentSchema()
{
	if(driverName().endsWith("PSQL")) d->currentSchemaName = "public";
	else if(driverName().endsWith("SQLITE")) d->currentSchemaName = "main";
	else if(driverName().endsWith("MYSQL")) d->currentSchemaName = databaseName();
}
*/
/*
QFSqlCatalog& QFSqlConnectionBase::catalogRef() throw(QFSqlException)
{
	if(d->catalog.isValid()) return d->catalog;
	QF_SQL_EXCEPTION("catalog is NULL");
}
*/
void QFSqlConnectionBase::close()
{
	if(isOpen()) {
		qfLogFuncFrame() << signature();
		QSqlDatabase::close();
	}
}

int QFSqlConnectionBase::defaultPort(const QString &driver_name)
{
	if(driver_name.endsWith("PSQL")) return 5432;
	else if(driver_name.endsWith("MYSQL")) return 3306;
	else if(driver_name.endsWith("IBASE")) return 3050;
	return 0;
}

void QFSqlConnectionBase::open(const QFSqlConnectionBase::ConnectionOptions &options) throw(QFSqlException)
{
	qfLogFuncFrame() << signature() << "driver:" << driver();
	qfTrash() << "\tdriver name:" << driver()->metaObject()->className();
	close();
	if(port() == 0) setPort(defaultPort(driverName()));
	if(!options.isEmpty()) {
		QStringList opt_list;
		foreach(QString key, options.keys()) {
			QString val = options.value(key);
			if(val.isNull()) val = key;
			else val = key + "=" + val;
			opt_list << val;
		}
		setConnectOptions(opt_list.join(","));
	}
	//qfInfo() << "password:" << password();
	if(!QSqlDatabase::open()) {
		setConnectOptions();
		throw QFSqlException(TR("Error opening database %1").arg(signature())
				+ QF_EOLN + QF_EOLN
				+ lastError().text());
	}
}
/*
QFSqlConnectionBase::DriverType QFSqlConnectionBase::driverType() const
{
	//qfLogFuncFrame();
	//qfTrash() << "\tdriver name:" << driverName();
	QFString s = driverName();
	if(s.startsWith("QPSQL")) return PSQL;
	if(s.startsWith("QMYSQL")) return MYSQL;
	if(s.endsWith("SQLITE")) return SQLITE;
	return UNSUPPORTED_DRIVER;
}
*/
QString QFSqlConnectionBase::signature() const
{
	QString s;
	s += this->databaseName();
	s+= QString("[%1]").arg(this->driverName());
	s+= this->userName() + "@";
	s+= this->hostName() + ":";
	s+= QString("%1").arg(this->port());
	return s;
}
/*
QString QFSqlConnectionBase::signature2driverName(const QString &sig)
{
	QFString s = sig;
	s = s.slice(1, s.pos(']'));
	int ix = QSqlDatabase::drivers().indexOf(s);
	QF_ASSERT(ix >= 0, TR("'%1' can not be parsed to valid driver name."));
	return s;
}
*/
QString QFSqlConnectionBase::info(int verbosity) const
{
	QString s;
	if(verbosity == 0) s = signature();
	else {
		s = TR("database '%1' on %2@%3 driver %4");
		s = s.arg(databaseName()).arg(userName()).arg(hostName()).arg(driverName());
	}
	return s;
}

static QVariant sqlite_set_pragma(QSqlQuery &q, const QString &pragma_key, const QVariant &val)
{
	qfLogFuncFrame() << pragma_key << "value:" << val.toString();
	QString qs = "PRAGMA " + pragma_key;
	if(!q.exec(qs)) QF_SQL_EXCEPTION(TR("SQL Error\nquery: %1;").arg(qs));
	q.next();
	QVariant old_val = q.value(0);
	qfTrash() << "\t oldval:" << old_val.toString();
	//qfTrash() << "\t newval:" << val.toString();
	qs = qs + "=" + QFSql::formatValue(val);
	//qfInfo() << qs;
	if(!q.exec(qs)) QF_SQL_EXCEPTION(TR("SQL Error\nquery: %1;").arg(qs));
	return old_val;
}

QStringList QFSqlConnectionBase::tables(const QString& dbname, QSql::TableType type) const
{
	qfLogFuncFrame() << "dbname:" << dbname << "type" << type;
	QStringList ret;
	if(driverName().endsWith("PSQL")) {
		QSqlQuery q(driver()->createResult());
		QString s = "SELECT table_name FROM information_schema.tables"
				" WHERE table_schema='%1' AND table_type='%2'";
		s = s.arg(dbname);
		if(type == QSql::Tables) s = s.arg("BASE TABLE");
		else if(type == QSql::Views) s = s.arg("VIEW");
		else s = s.arg("DRIVER UNSUPPORTED TYPE ERROR");
		q.exec(s);
		while(q.next()) {
			ret << q.value(0).toString();
		}
	}
	else if(driverName().endsWith("IBASE")) {
		QSqlQuery q(driver()->createResult());
		QFString s = "SELECT RDB$RELATION_NAME FROM RDB$RELATIONS"
				" WHERE RDB$SYSTEM_FLAG = %2 AND RDB$VIEW_BLR IS %1 NULL";
		if(type == QSql::Tables) s = s.arg("").arg(0);
		else if(type == QSql::Views) s = s.arg("NOT").arg(0);
		else if(type == QSql::SystemTables) s = s.arg("NOT").arg(1);
		else s = QString();
		if(!!s) {
			//qfTrash() << "\t" << s;
			q.exec(s);
			while(q.next()) {
				ret << q.value(0).toString();
			}
		}
	}
	else if(driverName().endsWith("SQLITE")) {
		QSqlQuery q(driver()->createResult());
		q.setForwardOnly(true);
		QString where;
		if(type == QSql::Tables) where = "type == 'table'";
		else if(type == QSql::Views) where = "type == 'view'";
		else where = "type == 'system'";
		QString from;
		if(dbname.isEmpty() || dbname == "main") from = "(SELECT * FROM sqlite_master UNION ALL SELECT * FROM sqlite_temp_master)";
		else from = dbname + ".sqlite_master";
		QString s;
		QVariant old_short_column_names = sqlite_set_pragma(q, "short_column_names", 0);
		QVariant old_full_column_names = sqlite_set_pragma(q, "full_column_names", 0);
		s = "SELECT name FROM %1 WHERE %2 ORDER BY name";
		s = s.arg(from).arg(where);
		qfTrash() << "\t" << s;
		if(!q.exec(s)) {
			QF_SQL_EXCEPTION(TR("Error getting table list for database '%1'\nquery: %2;").arg(dbname).arg(s));
		}
		while(q.next()) {
			s = q.value(0).toString();
			ret.append(s);
		}
		sqlite_set_pragma(q, "short_column_names", old_short_column_names);
		sqlite_set_pragma(q, "full_column_names", old_full_column_names);
	}
	else if(driverName().endsWith("MYSQL")) {
		QSqlQuery q(driver()->createResult());
		QStringList sl = serverVersion();
		int ver = 0;
		if(sl.size() > 0) {
			//qfTrash() << "\tsl[0]:" << sl[0];
			ver = sl[0].toInt();
		}
		//qfTrash() << "\tver:" << ver;
		if(ver <= 4) {
			if(type == QSql::Tables) {
				/// kvuli verzi 4, ktera nema information_schema
				QString s = "SHOW TABLES FROM %1";
				s = s.arg(dbname);
				q.exec(s);
			}
		}
		else {
			QString s = "SELECT table_name FROM INFORMATION_SCHEMA.TABLES"
					" WHERE table_schema='%1' ";
			s = s.arg(dbname);
			QString type_cond;
			if(type == QSql::Tables) type_cond = "AND table_type = 'BASE TABLE'";
			else if(type == QSql::SystemTables) type_cond = "AND table_type = 'SYSTEM VIEW'";
			else if(type == QSql::Views) type_cond = "AND table_type = 'VIEW'";
			qfTrash() << "\t" << s + type_cond;
			q.exec(s + type_cond);
		}
		qfTrash() << "\tfound:";
		while(q.next()) {
			QString s = q.value(0).toString();
			qfTrash() << s;
			ret << s;
		}
	}
	return ret;
}

QSqlRecord QFSqlConnectionBase::record(const QString & tablename) const
{
	qfLogFuncFrame() << "tblname:" << tablename;
	QFString s = fullTableNameToQtDriverTableName(tablename);
	if(driverName().endsWith("SQLITE")) {
		/// SQLITE neumi schema.tablename v prikazu PRAGMA table_info(...)
		int ix = s.lastIndexOf('.');
		if(ix >= 0) s = s.mid(ix + 1);
	}
	return QSqlDatabase::record(s);
}

QStringList QFSqlConnectionBase::fields(const QString& tbl_name) const throw(QFSqlException)
{
	qfLogFuncFrame() << "tblname:" << tbl_name;
	QFString tblname = normalizeDbName(tbl_name);
	//if(tblname[0] == '.') tblname = tblname.mid(1);
	QStringList ret;
	/*
	QSqlQuery q(driver()->createResult());
	q.setForwardOnly(true);
	QString s = "SELECT * FROM %1 WHERE 1=2";
	s = s.arg(tblname);
		//qfTrash() << "\t" << s;
	if(!q.exec(s)) {
		qfError() << QF_FUNC_NAME << TR("Error getting the field list form server table '%1'\n%2").arg(tblname).arg(s);
	}
	*/
	QSqlRecord r = record(tblname);
	if(r.isEmpty()) {
		qfWarning() << TR("table '%1' does not contains fields. (maybe it is in other schema?)").arg(tbl_name);
		//QF_SQL_EXCEPTION(TR("table '%1' does not contains fields. (maybe it is in other schema?)").arg(tbl_name));
	}
	else for(int i=0; i<r.count(); i++) {
		QString s = r.field(i).name().trimmed();
		int ix = s.lastIndexOf('.');
		if(ix >= 0) s = s.mid(ix + 1); /// jen to za posledni teckou
		qfTrash().noSpace() << "\tadding: '" << s << "'";
		ret.append(s);
	}
	return ret;
}

QFSqlConnectionBase::IndexList QFSqlConnectionBase::indexes(const QString& tbl_name) const
{
	qfLogFuncFrame() << "tblname:" << tbl_name;
	QString tblname = tbl_name;
	//if(tblname[0] == '.') tblname = tblname.mid(1);
	IndexList ret;
	QString s;
	if(driverName().endsWith("PSQL")) {
		QString s = "SELECT indexname FROM pg_indexes WHERE tablename="SARG(tbl_name);
		QSqlQuery q1(driver()->createResult());
		q1.setForwardOnly(true);
		q1.exec(s);
		while(q1.next()) {
			QString indexname = q1.value(0).toString();
			IndexInfo df;
			df.name = indexname;
			QString indexview = "SELECT indexes.relname AS indexname, indisprimary AS isprimary, indisunique AS isunique, columns.attname AS colname"
					" FROM pg_index LEFT JOIN pg_class AS indexes ON pg_index.indexrelid = indexes.oid"
					" LEFT JOIN pg_attribute AS columns ON columns.attrelid = pg_index.indrelid"
					" WHERE pg_index.indrelid='%1'::regclass AND columns.attnum = ANY (indkey)";
			indexview = indexview.arg(tblname);
			s  = "SELECT colname, isunique, isprimary FROM (%1) AS t WHERE indexname = '%2'";
			QSqlQuery q(driver()->createResult());
			q.setForwardOnly(true);
			q.exec(s.arg(indexview).arg(indexname));
			int i = 0;
			while(q.next()) {
				df.fields.append(q.value(0).toString());
				if(i++ == 0) df.unique = q.value(1).toBool();
			}
			ret.append(df);
		}
	}
	else if(driverName().endsWith("MYSQL")) {
		QString s = "SHOW INDEX FROM " + tblname;
		qfTrash() << "\t" << s;
		QSqlQuery q(driver()->createResult());
		q.setForwardOnly(true);
		q.exec(s);
		IndexInfo df;
		while(q.next()) {
			s = q.value(2).toString();
			qfTrash() << "\tkey_name:" << s;
			if(df.name != s) {
				if(!df.name.isEmpty()) ret.append(df);
				df = IndexInfo();
				df.name = s;
				df.unique = !q.value(1).toBool();
				df.primary = (df.name == "PRIMARY");
				df.fields.append(q.value(4).toString());
			}
			else {
				df.fields.append(q.value(4).toString()); //q.record().indexOf("Column_name")
			}
		}
		if(!df.name.isEmpty()) ret.append(df);
	}
	else if(driverName().endsWith("SQLITE")) {
		/// SQLITE neumi schema.tablename v prikazu PRAGMA table_info(...)
		QString tbl_name = tblname;
		{
			int ix = tbl_name.lastIndexOf('.');
			if(ix >= 0) tbl_name = tbl_name.mid(ix + 1);
		}
		QString s = "SELECT * FROM sqlite_master WHERE type='index' AND sql>'' AND tbl_name='%1'";
		s = s.arg(tbl_name);
		qfTrash() << "\t" << s;
		QSqlQuery q(driver()->createResult());
		q.setForwardOnly(true);
		q.exec(s);
		while(q.next()) {
			s = q.value(1).toString(); /// name
			qfTrash() << "\tkey_name:" << s;
			IndexInfo df;
			df.name = s;
			s = q.value(4).toString(); /// sql
			df.unique = s.indexOf(" UNIQUE ", Qt::CaseInsensitive) > 0;
			{
				int ix = s.lastIndexOf('(');
				if(ix > 0) {
					s = s.mid(ix+1);
					s = s.mid(0, s.count()-1);
					QStringList sl = s.split(',');
					df.fields = sl;
				}
			}
			if(!df.name.isEmpty()) ret.append(df);
		}
	}
	return ret;
}

QStringList QFSqlConnectionBase::databases() const
{
	qfLogFuncFrame();
	QStringList sl;
	QSqlQuery q(driver()->createResult());
    	q.setForwardOnly(true);

	if(driverName().endsWith("PSQL")) {
		q.exec(QLatin1String("SELECT datname FROM pg_database "));
		while(q.next()) {
			QString s = q.value(0).toString();
			qfTrash() << "\tfound:" << s;
			if(s.startsWith("template")) continue;
			sl.append(s);
		}
	}
	else {
		sl.append(databaseName());
	}
	qfTrash() << "\tloaded from server:" << sl.join(", ");
	return sl;
}

QStringList QFSqlConnectionBase::schemas() const
{
	qfLogFuncFrame();
	QStringList ret;
	if(driverName().endsWith("PSQL")) {
		QSqlQuery q(driver()->createResult());
		q.setForwardOnly(true);
		q.exec("SELECT n.nspname "
				" FROM pg_catalog.pg_namespace  AS n"
				" WHERE   (n.nspname NOT LIKE 'pg\\_temp\\_%' OR"
				" n.nspname = (pg_catalog.current_schemas(true))[1])"
				" ORDER BY 1");
		QSqlRecord r = q.record();
		while(q.next()) {
			QString s = q.value(0).toString();
			qfLogFuncFrame() << "loading schema" << s;
			ret.append(s);
		}
	}
	else if(driverName().endsWith("SQLITE")) {
		QSqlQuery q(driver()->createResult());
		q.setForwardOnly(true);
		q.exec(QLatin1String("PRAGMA database_list"));
		QSqlRecord r = q.record();
		while(q.next()) {
			QString s = q.value(r.indexOf("name")).toString();
			ret.append(s);
		}
	}
	else if(driverName().endsWith("MYSQL")) {
		QSqlQuery q(driver()->createResult());
		q.setForwardOnly(true);
		q.exec(QLatin1String("SHOW DATABASES;"));
		//QSqlRecord r = q.record();
		while(q.next()) {
			QString s = q.value(0).toString();
			ret.append(s);
		}
	}
	else if(driverName().endsWith("IBASE")) {
		ret << "main";
	}
	qfTrash() << "\tloaded from server:" << ret.join(", ");
	return ret;
}

bool QFSqlConnectionBase::isOpen() const
{
	if(!isValid()) return false;
	if(!QSqlDatabase::isOpen()) return false;
	//QSqlDriver *drv = driver();
	//if(!drv) return false;
	//qfLogFuncFrame() << "driver:" << drv;
	//if(!drv->handle().isValid()) return false;
	//if(!QSqlDatabase::isOpen()) return false;
	//if(!drv->isOpen()) return false;
	//qfLogFuncFrame();
	//qfTrash() << "\treturning:" << ret;
	return true;
}

QFSql::RelationKind QFSqlConnectionBase::relationKind(const QString& _relname)
{
	QFSql::RelationKind ret = QFSql::UnknownRelation;
	QString relname, dbname;
	QFSql::parseFullName(_relname, &relname, &dbname);
	if(driverName().endsWith("PSQL")) {
		QSqlQuery q(driver()->createResult());
		q.setForwardOnly(true);
		QString s = "SELECT c.relname, c.relkind, n.nspname"
				" FROM pg_class AS c"
				" LEFT JOIN pg_namespace AS n ON c.relnamespace=n.oid"
				" WHERE c.relname = '%1'";
		if(!dbname.isEmpty()) s += " AND n.nspname = '" + dbname + "'";
		if(q.exec(s.arg(relname))) {
			while(q.next()) {
				s = q.value(1).toString();
				if(s == "r") ret = QFSql::TableRelation;
				else if(s == "v") ret = QFSql::ViewRelation;
				else if(s == "i") ret = QFSql::IndexRelation;
				else if(s == "S") ret = QFSql::SequenceRelation;
				break;
			}
		}
	}
	else if(driverName().endsWith("SQLITE")) {
		QSqlQuery q(driver()->createResult());
		q.setForwardOnly(true);
		QString from;
		if(dbname.isEmpty() || dbname == "main") from = "(SELECT * FROM sqlite_master UNION ALL	SELECT * FROM sqlite_temp_master)";
		else from = dbname + ".sqlite_master";
		QString s = "SELECT type, name, tbl_name FROM %1 WHERE name = '%2'";
		s = s.arg(from).arg(relname);
		QVariant old_short_column_names = sqlite_set_pragma(q, "short_column_names", 0);
		QVariant old_full_column_names = sqlite_set_pragma(q, "full_column_names", 0);
		if(!q.exec(s)) {
			QF_SQL_EXCEPTION(TR("Error getting table list for database '%1'").arg(dbname));
		}
		// For tables, the type field will always be 'table' and the name field will be the name of the table.
		// For indices, type is equal to 'index', name is the name of the index
		//       and tbl_name is the name of the table to which the index belongs.
		else {
			while(q.next()) {
				s = q.value(0).toString();
				if(s == "table") ret = QFSql::TableRelation;
				else if(s == "view") ret = QFSql::ViewRelation;
				else if(s == "index") ret = QFSql::IndexRelation;
				break;
			}
		}
		sqlite_set_pragma(q, "short_column_names", old_short_column_names);
		sqlite_set_pragma(q, "full_column_names", old_full_column_names);
	}
	else if(driverName().endsWith("MYSQL")) {
		QSqlQuery q(driver()->createResult());
		q.setForwardOnly(true);
		/*
		QString s = "SELECT table_type FROM INFORMATION_SCHEMA.TABLES"
				" WHERE table_schema = '%1' AND table_name = '%2'";
		*/
		/// kvuli verzi 4, ktera nema information_schema
		QString s = "SHOW FULL tables FROM %1 LIKE '%2'";
		s = s.arg(dbname).arg(relname);
		if(!q.exec(s)) {
			QF_SQL_EXCEPTION(TR("Error getting table list for database '%1'\n\n%2").arg(dbname).arg(s));
		}
		else {
			while(q.next()) {
				s = q.value(1).toString();
				if(s == "BASE TABLE") ret = QFSql::TableRelation;
				else if(s == "VIEW") ret = QFSql::ViewRelation;
				else if(s == "SYSTEM VIEW") ret = QFSql::SystemTableRelation;
				break;
			}
		}
		//if(!q.exec("PRAGMA full_column_names=1")) QF_SQL_EXCEPTION(TR("SQL Error\nquery: %1;").arg(s));
	}
	return ret;
}

QStringList QFSqlConnectionBase::fieldDefsFromCreateTableCommand(const QString &cmd)
{
	qfLogFuncFrame() << cmd;
	QFString fs = cmd;
	QStringList sl;
	do {
		int ix;
		if((ix= fs.pos('(')) <0) break;
				//qfTrash() << "\tix:" << ix << "fs:" << s;
		fs = fs.slice(ix);
				//qfTrash() << "\tfields start:" << fs;
		if((ix= fs.indexOfMatchingBracket('(', ')', '\'')) <0) break;
				//qfTrash() << "\tlen:" << fs.len() << "ix:" << ix;
		fs = fs.slice(1, ix);
		//qfTrash() << "\tfields:" << fs;
		sl = fs.splitBracketed(',', '(', ')', '\'');
	} while(false);
	return sl;
}

static QString err_msg(QProcess::ProcessError err_no)
{
	switch(err_no) {
		case QProcess::FailedToStart:
			return "The process failed to start. Either the invoked program is missing, or you may have insufficient permissions to invoke the program.";
		case QProcess::Crashed:
			return "The process crashed some time after starting successfully.";
		case QProcess::Timedout:
			return "The last waitFor...() function timed out. The state of QProcess is unchanged, and you can try calling waitFor...() again.";
		case QProcess::WriteError:
			return "An error occurred when attempting to write to the process. For example, the process may not be running, or it may have closed its input channel.";
		case QProcess::ReadError:
			return "An error occurred when attempting to read from the process. For example, the process may not be running.";
		default:
			return "An unknown error occurred. This is the default return value of error().";
	}
}

QString QFSqlConnectionBase::createTableSqlCommand(const QString &tblname)
{
	qfLogFuncFrame();
	if(driverName().endsWith("SQLITE")) {
		QString db, tbl;
		QFSql::parseFullName(tblname, &tbl, &db);
		//db = normalizeDbName(db);
		QSqlQuery q(driver()->createResult());
		q.setForwardOnly(true);
		QString s = "SELECT sql FROM %1.sqlite_master"
				" WHERE type IN ('table', 'view')"
				" AND tbl_name = '%2'";
		s = s.arg(db).arg(tbl);
		//qfTrash() << '\t' << s;
		if(!q.exec(s)) {
			QSqlError err = lastError();
			qfError() << TR("Error getting sql for '%1'").arg(tblname) + QF_EOLN + err.driverText() + QF_EOLN + err.databaseText();
				//QF_SQL_EXCEPTION(s);
		}
		else 	while(q.next()) {
			return q.value(0).toString() + ";";
		}
	}
	else if(driverName().endsWith("PSQL")) {
		QString db, tbl;
		QFSql::parseFullName(tblname, &tbl, &db);
		QProcess proc;
		QString prog_name = "pg_dump";
		QStringList params;
		params << "-h";
		params << hostName();
		params << "-p";
		params << QString::number(port());
		params << "-U";
		params << userName();
		params << "-t";
		params << tbl;
		params << databaseName();
		QString s = prog_name + " " + params.join(" ");
		proc.start(prog_name, params);
		qfTrash() << "\tcalling:" << s;
		if (!proc.waitForStarted(5000)) {
			return "calling: " + s + "\n" + err_msg(proc.error());
		}
		//proc.write("Qt rocks!");
		//proc.closeWriteChannel();
		if (!proc.waitForFinished(10000)) {
			return "finished error: " + err_msg(proc.error());
		}
		s = QString();
		if(proc.exitCode() != 0) {
			s = "EXIT CODE: %1\n";
			s = s.arg(proc.exitCode());
			s += QString(proc.readAllStandardError());
			s += "\n";
		}
		QByteArray result = proc.readAll();
		return s + QString(result);
	}
	if(driverName().endsWith("MYSQL")) {
		QSqlQuery q(driver()->createResult());
		q.setForwardOnly(true);
		QString s = "SHOW CREATE TABLE %1";
		s = s.arg(tblname);
		qfTrash() << '\t' << s;
		if(!q.exec(s)) {
			QSqlError err = lastError();
			//qfTrash() << "\terr is valid:" << err.isValid();
			QString s  = TR("Error getting sql for '%1'").arg(tblname) + QF_EOLN + err.driverText() + QF_EOLN + err.databaseText();
			QF_SQL_EXCEPTION(s);
		}
		else if(q.next()) {
			return q.value(1).toString() + ";";
		}
		return QString();
	}
	return "unsupported for " + driverName();
}

QString QFSqlConnectionBase::dumpTableSqlCommand(const QString &tblname)
{
	if(driverName().endsWith("MYSQL")) {
		QSqlQuery q(driver()->createResult());
		q.setForwardOnly(true);
		QString s = "SELECT * FROM %1";
		s = s.arg(tblname);
		//qfTrash() << '\t' << s;
		if(!q.exec(s)) {
			QSqlError err = lastError();
			//qfTrash() << "\terr is valid:" << err.isValid();
			QString s  = TR("Error select from '%1'").arg(tblname) + QF_EOLN + err.driverText() + QF_EOLN + err.databaseText();
			QF_SQL_EXCEPTION(s);
		}
		else {
			QStringList sl;
			while(q.next()) {
				QSqlRecord rec = q.record();
				QStringList sl2;
				for(int i=0; i<rec.count(); i++) {
					sl2 << driver()->formatValue(rec.field(i));
				}
				sl << "(" + sl2.join(",") + ")";
			}
			QString table;
			QFSql::parseFullName(tblname, &table);
			s = "INSERT INTO %1 VALUES\n";
			s = s.arg(table) + sl.join(",\n");
			s += ";";
			return s;
		}
		return QString();
	}
	return "unsupported for " + driverName();
}

QSqlIndex QFSqlConnectionBase::primaryIndex(const QString& _tblname)
{
	qfLogFuncFrame() << "table name:" << _tblname;
	QFString tblname = _tblname;
	QSqlIndex ret;
	if(driverName().endsWith("SQLITE")) {
#if 0
		// TODO: use PRAGMA table_info(table_name)  instead of parsing CREATE command
		// s timhle je problem, protoze pragma neprijma table_name ve tvaru schema.table
		QString tblname, dbname;
		tblname = normalizeTableName(_tblname);
		QFSql::parseFullName(tblname, &tblname, &dbname);
		QString s = "PRAGMA table_info(table_name)";		     s = s.arg(tblname, dbname);
					    QSqlQuery q = exec(s);
					    while(q.next()) {
					    ret.append(QSqlField(q.value(0).toString()));
	}
#else
		/// parsing create table command
		QString s = createTableSqlCommand(tblname);
		QStringList sl =fieldDefsFromCreateTableCommand(s);
		foreach(QFString s1, sl) {
			qfTrash() << "\tfield:" << s1;//PRIMARY KEY
			if(s1.indexOf("primary key", 0, Qt::CaseInsensitive) > 0) {
				QFString fs = s1.section(' ', 0, 0, QString::SectionSkipEmpty);
				if(!!fs) {
					QVariant::Type type;
					QString type_name = s1.section(' ', 1, 1, QString::SectionSkipEmpty).toLower();
					if (type_name == QLatin1String("integer") || type_name == QLatin1String("int")) type = QVariant::Int;
					else if(type_name == QLatin1String("double") || type_name == QLatin1String("float") || type_name.startsWith(QLatin1String("numeric"))) type = QVariant::Double;
					else if (type_name == QLatin1String("blob")) type = QVariant::ByteArray;
					else type = QVariant::String; 

					ret.append(QSqlField(fs.trimmed(), type));
				}
			}
		}
#endif
	}
	else {
		tblname = fullTableNameToQtDriverTableName(tblname);
		ret = QSqlDatabase::primaryIndex(tblname);
	}
	/*
	if(driverName().endsWith("MYSQL")) {
		QString s = "SELECT column_name FROM information_schema.columns"
				" WHERE table_name = '%1' AND table_schema = '%2' AND column_key = 'PRI'"
				" ORDER BY ordinal_position";
		QString tblname, dbname;
		tblname = normalizeTableName(_tblname);
		QFSql::parseFullName(tblname, &tblname, &dbname);
		s = s.arg(tblname, dbname);
		QSqlQuery q = exec(s);
		while(q.next()) {
			ret.append(QSqlField(q.value(0).toString()));
		}
	}
	*/
	for(int i=0; i<ret.count(); i++) {
		qfTrash() << "\t" << ret.field(i).name();
	}
	return ret;
}

QString QFSqlConnectionBase::normalizeFieldName(const QString &n) const
{
	QString db, tbl, fld;
	QFSql::parseFullName(n, &fld, &tbl, &db);
	if(!tbl.isEmpty()) {
		db= normalizeDbName(db);
		return QFSql::composeFullName(fld, tbl, db);
	}
	return fld;
}

QString QFSqlConnectionBase::normalizeTableName(const QString &n) const
{
	QString db, tbl;
	QFSql::parseFullName(n, &tbl, &db);
	db = normalizeDbName(db);
	return QFSql::composeFullName(tbl, db);
}

QString QFSqlConnectionBase::normalizeDbName(const QString &n) const
{
	QFString db = n;
	if(!db) db = currentSchema();
	return QFSql::composeFullName(db);
}

QString QFSqlConnectionBase::currentSchema() const
{
	QString ret;
	if(driverName().endsWith("MYSQL")) {
		QSqlQuery q = exec("SELECT DATABASE()");
		if(lastError().isValid()) {
			QF_SQL_EXCEPTION("Error getting curent schema");
		}
		else {
			q.next();
			ret = q.value(0).toString();
		}
	}
	else if(driverName().endsWith("SQLITE")) {
		ret = "main";
	}
	else {
		ret = "main";
	}
	return ret;
}

void QFSqlConnectionBase::setCurrentSchema(const QString &schema_name) throw(QFSqlException)
{
	if(driverName().endsWith("MYSQL")) {
		QSqlQuery q(*this);
		if(!q.exec("USE " + schema_name)) {
			QF_SQL_EXCEPTION("Error setting curent schema to " + schema_name + "\n" + lastError().text());
		}
	}
}

QStringList QFSqlConnectionBase::serverVersion() const
{
	QSqlQuery q = exec("SHOW variables LIKE 'version'");
	QStringList sl;
	if(q.next()) {
		sl = q.value(1).toString().split('.');
	}
	return sl;
}

QString QFSqlConnectionBase::fullTableNameToQtDriverTableName(const QString &full_table_name) const
{
	QFString ret = full_table_name;
	if(driverName().endsWith("IBASE")) {
		/// IBASE nema zadny schema
		int ix = ret.lastIndexOf('.');
		if(ix >= 0) ret = ret.slice(ix + 1);
	}
	return ret;
}


