
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdbxmlconfig.h"

#include <qfsqlquery.h>
//#include <qfdbapplication.h>

#include <qflogcust.h>

const QString QFDbXmlConfig::defaultDbxmlTableName = "dbxml";

QString QFDbXmlConfig::dbxmlTableName() const
{
	if(f_dbxmlTableName.isEmpty()) return defaultDbxmlTableName;
	return f_dbxmlTableName;
}

void QFDbXmlConfig::loadTemplateDocument(const QString &ckey, bool throw_exc) throw(QFException)
{
	if(ckey.isEmpty()) return;
	/// kvuli designeru, aby nepadal, kdyz neni connection
	QFSqlConnection &conn = QFDbXmlConfigLoader::appConnection(!Qf::ThrowExc);
	if(!conn.isValid()) return;
	QFSqlQuery q(conn);
	QString s = "SELECT dbxml FROM %1 WHERE ckey='%2'";
	s = s.arg(dbxmlTableName()).arg(ckey);
	//qfTrash() << "\t" << s;
	q.exec(s);
	s = QString();
	if(q.next()) {
		s = q.value("dbxml").toString();
			//qfInfo() << "\tloaded:" << document().toString();
	}
	if(s.isEmpty()) {
		if(throw_exc) QF_EXCEPTION(TR("cann't load dbxml for field %1").arg(ckey));
		//else s = "<config/>";
	}
	setTemplateDocument(s);
}

void QFDbXmlConfig::setDataDocument(const QString & xml_str) throw(QFException)
{
	qfLogFuncFrame() << xml_str;
	//qfInfo() << QFLog::stackTrace();
	QFString s = xml_str;
	if(s.isEmpty()) s = "<dbxml/>";
	QFXmlConfig::setDataDocument(s);
}

