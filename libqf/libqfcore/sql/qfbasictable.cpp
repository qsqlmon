//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfbasictable.h"

#include <qfsql.h>
#include <qfdom.h>
#include <qfcsvreader.h>
#include <qf7bittextcodec.h>
#include <qfalgorithms.h>
#include <qfxmltable.h>
#include <qfstoragedriver.h>
#include <qfjson.h>
#include <qfassert.h>

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

//====================================================
//                      QFBasicTable::LessThan
//====================================================
QFBasicTable::LessThan::LessThan(const QFBasicTable &t)
	: table(t), sortedFields(t.tableProperties().sortDefinition())
{
	//qfTrash() << QF_FUNC_NAME << "sortedFields.count():" << sortedFields.count();
	for(int i=0; i<sortedFields.count(); i++) {
		if(!table.fields().isValidFieldIndex(sortedFields[i].fieldIndex)) QF_EXCEPTION(QString("Invalid sort definition. field index: %1").arg(sortedFields[i].fieldIndex));
	}
}

bool QFBasicTable::LessThan::lessThan(int r1, int r2) const
{
	//qfTrash() << QF_FUNC_NAME << "r1:" << r1 << "r2:" << r2;
	bool ret = false;
	for(int i=0; i<sortedFields.count(); i++) {
		const QFBasicTable::SortDef &sd = sortedFields[i];
		QVariant l = table.rows()[r1].value(sd.fieldIndex);
		QVariant r = table.rows()[r2].value(sd.fieldIndex);
		int res = cmp(l, r, sd);
		//qfTrash() << "\t res:" << res;
		if(res == 0) continue;
		if(sd.ascending) {ret = (res < 0); break;}
		ret = (res > 0); break;
	}
	//qfTrash() << "\t return:" << ret;
	return ret;
}

bool QFBasicTable::LessThan::lessThan_helper(int _row, const QVariant &v, bool switch_params) const
{
	//qfTrash() << QF_FUNC_NAME << "row:" << _row << "v:" << v.toString() << "switch_params:" << switch_params;
	bool ret = false;
	QVariantList vlst = v.toList();
	int cnt = 1;
	if(!vlst.isEmpty()) cnt = vlst.count();
	for(int i=0; i<cnt && i<sortedFields.count(); i++) {
		const QFBasicTable::SortDef &sd = sortedFields[i];
		QF_ASSERT(sd.ascending, "Ascending sort is needed for seek.");
		QVariant l = table.rows()[_row].value(sd.fieldIndex);
		QVariant r;
		if(vlst.isEmpty()) r = v;
		else r = vlst[i];
		int res;
		//qfTrash() << "\t l:" << l.toString() << "r:" << r.toString();
		if(switch_params) res = cmp(r, l, sd);
		else res = cmp(l, r, sd);
		if(res < 0) {ret = true; break;}
		if(res > 0) {ret = false; break;}
	}
	//qfTrash() << "\treturn:" << ret;
	return ret;
}

int QFBasicTable::LessThan::cmp(const QVariant &l, const QVariant &r, const QFBasicTable::SortDef &sd) const
{
	//qfInfo() << QF_FUNC_NAME << "l:" << l.toString() << "r:" << r.toString();
	/// NULL je nejmensi ze vsech
	if(!l.isValid() && r.isValid()) return -1;
	if(l.isValid() && !r.isValid()) return 1;
	if(!l.isValid() && !r.isValid()) return 0;
	int ret = 1;
	if(l.type() == QVariant::String) {
		if(sd.ascii7bit) {
			QByteArray la = QF7BitTextCodec::toAscii7(l.toString());
			QByteArray ra = QF7BitTextCodec::toAscii7(r.toString());
			if(!sd.caseSensitive) {
				la = la.toLower();
				ra = ra.toLower();
			}
			if(la < ra) ret = -1;
			else if(la == ra) ret = 0;
		}
		else {
			if(sd.caseSensitive) {
				ret = QString::localeAwareCompare(l.toString(), r.toString());
			}
			else {
				ret =  QString::localeAwareCompare(l.toString().toLower(), r.toString().toLower());
			}
		}
	}
	else {
		if(l == r) ret = 0;
		else switch (l.type()) {
			case QVariant::Invalid:
				ret = -1;
				break;
			case QVariant::Bool:
			case QVariant::Int:
				if(l.toInt() < r.toInt()) ret = -1;
				break;
			case QVariant::UInt:
				if(l.toUInt() < r.toUInt()) ret = -1;
				break;
			case QVariant::LongLong:
				if(l.toLongLong() < r.toLongLong()) ret = -1;
				break;
			case QVariant::ULongLong:
				if(l.toULongLong() < r.toULongLong()) ret = -1;
				break;
			case QVariant::Double:
				if(l.toDouble() < r.toDouble()) ret = -1;
				//qfInfo() << "l:" << l.toDouble() << "lestThan" << "r:" << r.toDouble() << ret;
				break;
			case QVariant::Char:
				if(l.toChar() < r.toChar()) ret = -1;
				break;
			case QVariant::Date:
				if(l.toDate() < r.toDate()) ret = -1;
				break;
			case QVariant::Time:
				if(l.toTime() < r.toTime()) ret = -1;
				break;
			case QVariant::DateTime:
				if(l.toDateTime() < r.toDateTime()) ret = -1;
				break;
			case QVariant::ByteArray:
				if(l.toByteArray() < r.toByteArray()) ret = -1;
				break;
			default:
				qfWarning() << "unsupported variant comparison, variant type:" << l.type();
				break;
		}
	}
	//qfTrash() << "\tret:" << ret;
	return ret;
}
/*
static void stable_sort(QVector<int>::iterator start, QVector<int>::iterator end, const QFBasicTableLessThan &lessThan)
{
	qfTrash() << QF_FUNC_NAME << *start << *end;
	const int span = end - start;
	if (span < 2)
		return;

    // Split in half and sort halves.
	QVector<int>::iterator middle = start + span / 2;
	stable_sort(start, middle, lessThan);
	stable_sort(middle, end, lessThan);

    // Merge
	QVector<int>::iterator lo = start;
	QVector<int>::iterator hi = middle;

	{
		QStringList sl;
		QVector<int>::iterator it = start;
		while(it != end) {
			sl << QString::number(*it);
			++it;
		}
		qfTrash() << "\tBEFORE:" << sl.join(" ");
	}
	while (lo != middle && hi != end) {
		qfTrash() << "\tlo" << *lo << "hi:" << *hi;
		if (!lessThan(*hi, *lo)) {
			++lo; // OK, *lo is in its correct position
			qfTrash() << "\tOK";
		} else {
			qfTrash() << "\tMERGE";
            // Move *hi to lo's position, shift values
            // between lo and hi - 1 one place up.
			int value = *hi;
			for (QVector<int>::iterator i = hi; i != lo; --i) {
				*i = *(i-1);
			}
			*lo = value;
			++hi;
			++lo;
			++middle;
		}
	}
	{
		QStringList sl;
		QVector<int>::iterator it = start;
		while(it != end) {
			sl << QString::number(*it);
			++it;
		}
		qfTrash() << "\tAFTER:" << sl.join(" ");
	}
}
*/

//=========================================
//                          QFBasicTable::FieldList
//=========================================
int QFBasicTable::FieldList::fieldNameToIndex(const QString &field_name, bool throw_exc) const throw(QFException)
{
	int col = -1, i = 0;
	QString s = field_name;
	foreach(QFSqlField fld, *this) {
		if(QFSql::endsWith(fld.fullName(), s)) {
			col = i;
			break;
		}
		i++;
	}
	if(col < 0) {
		s = QString("Field %1 not found in query\nAvailable fields are: %2");
		QFString s1;
		foreach(QFSqlField fld, *this) s1 += ", " + fld.fullName();
		s1 = s1.slice(2);
		if(throw_exc) {
			QF_EXCEPTION(s.arg(field_name).arg(s1));
		}
		return -1;
	}
	return col;
}

bool QFBasicTable::FieldList::isValidFieldIndex(int fld_ix) const
{
	bool ret = (fld_ix >= 0 && fld_ix < count());
	//if(!ret && throw_exc) QF_EXCEPTION(QString("index: %1 is not a valid field index. FieldsCount: %2").arg(fld_ix).arg(count()));
	return ret;
}

//=========================================
//                          QFBasicTable::TableProperties
//=========================================
QFBasicTable::TableProperties::TableProperties(int)
{
	d = new Data();
}

QFBasicTable::TableProperties::TableProperties()
{
	*this = sharedNull();
}

const QFBasicTable::TableProperties& QFBasicTable::TableProperties::sharedNull()
{
	static TableProperties n(1);
	return n;
}

void QFBasicTable::TableProperties::setStorageDriver(QFStorageDriver * drv)
{
	/// driver se nemaze, protoze je to singleton
	//if(d->storageDriver) delete storageDriver; 
	d->storageDriver = drv;
}
//=========================================
//                          QFBasicTable::Row
//=========================================
/*
QFBasicTable::Row::Data::Data(int colcnt)
	: values(colcnt), insert(false)
{
}
*/
QFBasicTable::Row::Data::Data(const TableProperties &props)
	: tableProperties(props), insert(false)
{
	FieldList flst = tableProperties.fields();
	values.resize(flst.count());
	//nullFlags.resize(flst.count());
	for(int i=0; i<flst.count(); i++) values[i] = QVariant();/// NULL je QVariant(), to kvuli QDate. (flst[i].type());
}

QFBasicTable::Row::Row()
{
	*this = sharedNull();
}

QFBasicTable::Row::Row(bool )
{
	d = new Data();
}

QFBasicTable::Row::Row(const QFBasicTable::TableProperties &props)
{
	d = new Data(props);
}

const QFBasicTable::Row& QFBasicTable::Row::sharedNull()
{
	static QFBasicTable::Row null_row(true);
	return null_row;
}

bool QFBasicTable::Row::isDirty(int field_no) const throw(QFException)
{
	if(field_no < 0 || field_no >= d->values.size()) QF_EXCEPTION(QString("field index %1 is out of range (%2)").arg(field_no).arg(d->values.size()));
	if(field_no >= d->dirtyFlags.count()) return false;
	return d->dirtyFlags.at(field_no);
	return false;
}

void QFBasicTable::Row::setDirty(int field_no, bool val) throw(QFException)
{
	qfLogFuncFrame() << "field_no:" << field_no << "val:" << val;
	if(field_no < 0 || field_no >= d->values.size()) QF_EXCEPTION(QString("field index %1 is out of range (%2)").arg(field_no).arg(d->values.size()));
	if(d->values.count() != d->origValues.count() || !isDirty()) {
		saveValues();
	}
	d->dirtyFlags[field_no] = val;
}

bool QFBasicTable::Row::isDirty() const
{
	/*
	for(int i=0; i<d->origValues.count(); i++) {
		if(d->origValues[i].dirty) return true;
	}
	return false;
	*/
	//qfInfo() << QF_FUNC_NAME << "ret:" << (d->origValues.count() > 0);
	return d->origValues.count() > 0;
}
/*
void QFBasicTable::Row::clearDirty()
{
	for(int i=0; i<d->origValues.count(); i++) {
		if(d->origValues[i].dirty) return true;
}
}
*/
QVariant QFBasicTable::Row::origValue(int col, bool throw_exc) const throw(QFException)
{
	QVariant ret;
	if(col < 0 || col >= d->origValues.size())
		if(throw_exc) QF_EXCEPTION(QString("Column %1 is out of origValues range %2").arg(col).arg(d->origValues.size()));
	return d->origValues.value(col);
}

QVariant QFBasicTable::Row::origValue(const QString & field_name, bool throw_exc) const throw( QFException )
{
	int col = fields().fieldNameToIndex(field_name, !Qf::ThrowExc);
	QVariant v;
	if(col < 0 && throw_exc)  QF_EXCEPTION(QString("Field name '%1' not found.").arg(field_name));
	else v = origValue(col, throw_exc);
	return v;
}
/*
bool QFBasicTable::hasNullFlag(int col) const throw(QFException)
{
	if(col < 0 || col >= d->nullFlags.count())
		QF_EXCEPTION(QString("Column %1 is out of range %2").arg(col).arg(d->nullFlags.count()));
	bool ret = d->nullFlags[col];
	//qfInfo().noSpace() << "col: " << col << " value: '" << v.toString() << "' type: " << QVariant::typeToName(v.type()) << " is null: " << v.isNull() << " is valid: " << v.isValid();
	return ret;
}
*/
QVariant QFBasicTable::Row::value(int col, bool throw_exc) const throw(QFException)
{
	if(col < 0 || col >= d->values.size()) {
		if(throw_exc) QF_EXCEPTION(QString("Column %1 is out of range %2").arg(col).arg(d->values.size()));
		return QVariant();
	}
	QVariant v = d->values.value(col);
	//qfInfo().noSpace() << "col: " << col << " value: '" << v.toString() << "' type: " << QVariant::typeToName(v.type()) << " is null: " << v.isNull() << " is valid: " << v.isValid();
	return v;
}

QVariant QFBasicTable::Row::value(const QString &field_name, bool throw_exc) const throw(QFException)
{
	int col = fields().fieldNameToIndex(field_name, !Qf::ThrowExc);
	QVariant v;
	if(col < 0 && throw_exc)  QF_EXCEPTION(QString("Field name '%1' not found.").arg(field_name));
	else v = value(col, throw_exc);
	return v;
}
/*
static QString v2str(const QVariant &v)
{
	if(!v.isValid()) return "INVALID";
	if(v.type() == QVariant::ByteArray) return QString(v.toByteArray().toBase64());
	if(v.type() == QVariant::String) return '\'' + v.toString() + '\'';
	return v.toString();
}
*/
void QFBasicTable::Row::setInitialValue(int col, const QVariant & v) throw( QFException )
{
	if(col < 0 || col >= d->values.size()) QF_EXCEPTION(QString("Column %1 is out of range %2").arg(col).arg(d->values.size()));
	QVariant new_val = v;
	if(v.isValid()) new_val = Qf::retypeVariant(v, fields()[col].type());
	d->values[col] = new_val;
}

void QFBasicTable::Row::setValue(int col, const QVariant &v) throw(QFException)
{
	if(d->values.count() != d->origValues.count() || !isDirty()) {
		saveValues();
	}
	if(col < 0 || col >= d->values.size())
		QF_EXCEPTION(QString("Column %1 is out of range %2").arg(col).arg(d->values.size()));
	/*
	if(fields()[col].fieldName() == "vyjadreni") {
		//QByteArray ba = v.toByteArray();
		//qfInfo() << QF_FUNC_NAME << "dirty" << isDirty() << "size:" << ba.size();
		qfInfo() << "\tcol name:" << fields()[col].fieldName() << "value:" << v2str(v) << "type:" << QVariant::typeToName(v.type());
		qfInfo() << "\t\tretyping to type:" << QVariant::typeToName(fields()[col].type());
		qfInfo() << "\t\tfield type:" << QVariant::typeToName(fields()[col].type());
	}
	*/
	QVariant new_val = v;
	if(v.isValid()) new_val = Qf::retypeVariant(v, fields()[col].type());
	QVariant old_val = d->values[col];
	/*
	if(fields()[col].fieldName() == "vyjadreni") {
		qfInfo() << "\t\told value:" << v2str(old_val) << "old type:" << old_val.typeName() << " isValid:" << old_val.isValid();
		qfInfo() << "\t\tnew value:" << v2str(new_val) << "old type:" << new_val.typeName() << " isValid:" << new_val.isValid();
	}
	*/
	if(new_val != old_val) {
		d->values[col] = new_val;
		setDirty(col, true);
	}
}

void QFBasicTable::Row::setValue(const QString &field_name, const QVariant &v) throw(QFException)
{
	//qfInfo() << QF_FUNC_NAME << "field_name" << field_name << "val:" << v.toString();
	int col = fields().fieldNameToIndex(field_name);
	setValue(col, v);
}

void QFBasicTable::Row::saveValues()
{
	//if(isDirty()) return;
	//qfTrash() << QF_FUNC_NAME << "fieldcnt:" << fields().size();
	//origValues.clear();
	d->origValues.resize(d->values.size());
	d->dirtyFlags.resize(d->values.size());
	for(int i=0; i<d->values.size(); i++) {
		d->origValues[i] = d->values[i];
		d->dirtyFlags[i] = false;
	}
}

void QFBasicTable::Row::restoreValues()
{
	for(int i=0; i<d->values.count() && i<d->origValues.count(); i++) {
		d->values[i] = d->origValues[i];
		//d->origValues[i].dirty = false;
	}
	d->origValues.clear();
}

void QFBasicTable::Row::clearOrigValues()
{
	d->origValues.clear();
	d->dirtyFlags.clear();
}
void QFBasicTable::Row::clearEditFlags()
{
	clearOrigValues();
	setInsert(false);
}

void QFBasicTable::Row::prepareForCopy()
{
	setInsert(true);
	for(int i=0; i<d->values.size(); i++) {
		setDirty(i, true);
	}
	QFStorageDriver *drv = tableProperties().storageDriver();
	if(drv) drv->checkClonnedRow(*this);
}

bool QFBasicTable::Row::post() throw( QFException )
{
	QFStorageDriver *drv = tableProperties().storageDriver();
	if(drv) return drv->postRow(*this);
	else clearEditFlags();
	return true;
}

bool QFBasicTable::Row::drop() throw( QFException )
{
	QFStorageDriver *drv = tableProperties().storageDriver();
	if(drv) return drv->deleteRow(*this);
	return true;
}

int QFBasicTable::Row::reload() throw( QFException )
{
	QFStorageDriver *drv = tableProperties().storageDriver();
	if(drv) return drv->reloadRow(*this);
	return 1;
}

void QFBasicTable::Row::fillDefaultAndAutogeneratedValues() throw( QFException )
{
	QFStorageDriver *drv = tableProperties().storageDriver();
	if(drv) return drv->fillDefaultAndAutogeneratedValues(*this);
}

/*
bool QFBasicTable::Row::operator<(const QFBasicTable::Row &r) const
{
	QFBasicTable::FieldList flds = fields();
	foreach(SortDef d, tableProperties().sortDefinition()) {
		if(d.isSortByRowNo()) {
			return rowNo() < r.rowNo();
		}
		else {
			int i = d.fieldIndex;
			QF_ASSERT(fields().isValidFieldIndex(i), QString("Invalid sort definition. field index: %1").arg(i));
			bool b;
			QVariant::Type t = flds[i].type(); // sorteni podle typu fieldu
			//QVariant::Type t = value(i).type(); // sorteni podle typu variantu
			switch(t) {
				case QVariant::Int:
					b = (value(i).toInt() < r.value(i).toInt());
					break;
				case QVariant::UInt:
					b = (value(i).toUInt() < r.value(i).toUInt());
					break;
				case QVariant::LongLong:
					b = (value(i).toLongLong() < r.value(i).toLongLong());
					break;
				case QVariant::String:
					//qfTrash() << QF_FUNC_NAME << this;
					//bool cs = tableProperties().sortOptions().contains(TableProperties::SortCaseSensitive);
					if(d.caseSensitive) {
						b = (QString::localeAwareCompare(value(i).toString(), r.value(i).toString()) < 0);
					//b = (value(i).toString() < r.value(i).toString());
					//qfTrash() << "\t" << value(i).toString() << "<" << r.value(i).toString() << ":" << b;
					}
					else {
						b = (QString::localeAwareCompare(value(i).toString().toLower(), r.value(i).toString().toLower()) < 0);
						//b = (value(i).toString().toLower() < r.value(i).toString().toLower());
						//qfTrash() << "\t" << value(i).toString().toLower().toLocal8Bit() << "<" << r.value(i).toString().toLower().toLocal8Bit() << ":" << b;
					}
					break;
				case QVariant::Date:
					b = (value(i).toDate() < r.value(i).toDate());
					break;
				default:
					qfWarning() << "unsupported variant comparison, variant type:" << t;
					return false;
			}
			if(!d.ascending) b = !b;
			if(!b) return false;
		}
	}
	return true;
}
*/
QVariantMap QFBasicTable::Row::valuesMap() const
{
	QVariantMap ret;
	for(int i=0; i<fields().count(); i++) {
		ret[fields()[i].fieldName()] = value(i);
	}
	return ret;
}

QString QFBasicTable::Row::toString(const QString &sep) const
{
	QStringList sl;
	for(int i=0; i<fields().count(); i++) {
		QString s = "[%1:%3]: %2";
		s = s.arg(fields()[i].fieldName());
		QVariant v = value(i);
		if(v.type() == QVariant::String) v = "'" + v.toString() + "'";
		sl << s.arg(v.toString()).arg(QVariant::typeToName(v.type()));
	}
	return sl.join(sep);
	/*
	QStringList sl;
	foreach(QVariant v, d->values) sl << v.toString();
	return sl.join(sep);
	*/
}

//=========================================
//                          QFBasicTable
//=========================================
QFBasicTable::QFBasicTable(QFBasicTable::SharedDummyHelper )
{
	d = new Data();
}

const QFBasicTable & QFBasicTable::sharedNull()
{
	static QFBasicTable n = QFBasicTable(SharedDummyHelper());
	return n;
}

QFBasicTable::QFBasicTable()
{
	*this = sharedNull();
}

QFBasicTable::QFBasicTable(const QStringList &col_names)
{
	d = new Data();
	for(int i=0; i<col_names.count(); i++) {
		QFSqlField fld(col_names[i], QVariant::String);
		fieldsRef().append(fld);
	}
}

QFBasicTable::QFBasicTable(const QList<QFBasicTable::ColumnDef> &col_defs)
{
	d = new Data();
	for(int i=0; i<col_defs.count(); i++) {
		const ColumnDef &cd = col_defs[i];
		QFSqlField fld(cd.name, cd.type);
		fld.setLength(cd.length);
		fld.setPrecision(cd.prec);
		fieldsRef().append(fld);
	}
}

QFBasicTable::~QFBasicTable()
{
}

void QFBasicTable::setReadOnly(bool ro)
{
	setEditRowsAllowed(!ro);
	setInsertRowsAllowed(!ro);
	setDeleteRowsAllowed(!ro);
}

bool QFBasicTable::isReadOnly() const
{
	return !isEditRowsAllowed() && !isInsertRowsAllowed() && !isDeleteRowsAllowed();
}

void QFBasicTable::cleanupData(CleanupDataOption fields_options)
{
	qfTrash() << QF_FUNC_NAME;
	switch(fields_options) {
		case ClearFieldsRows:
			qfTrash() << "\tcleaning fields";
			fieldsRef().clear();
		default:
			qfTrash() << "\tcleaning rows";
			d->rows.clear();
			createRowIndex();
	}
}

int QFBasicTable::rowCount() const
{
	//return d->rows.count();
	return rowIndex().size();
}

int QFBasicTable::columnCount() const
{
	return fields().count();
}

bool QFBasicTable::isValidRow(int row) const //throw(QFException)
{
	bool ret = (row >= 0 && row < rowCount());
	return ret;
}

QFSqlField& QFBasicTable::fieldRef(int fld_ix) throw(QFException)
{
	if(!isValidField(fld_ix))
		QF_EXCEPTION(TR("col %1 is not valid column number (count: %2)").arg(fld_ix).arg(fields().count()));
	return fieldsRef()[fld_ix];
}

QFSqlField QFBasicTable::field(int fld_ix, bool throw_exc) const throw(QFException)
{
	//qfTrash() << QF_FUNC_NAME;
	//qfTrash() << "\tcol:" << col << "colCount():" << colCount() << "d:" << d;
	bool ret = isValidField(fld_ix);
	if(ret) return fields()[fld_ix];
	if(throw_exc) QF_EXCEPTION(QString("field: %1 is not a valid field index. FieldCount: %2").arg(fld_ix).arg(fields().count()));
	return QFSqlField();
}

QFBasicTable::Row& QFBasicTable::insertRow(int before_row, bool fill_default_and_auto_values) throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;// << QFLog::stackTrace();
	Row &row = insertRow(before_row, singleRow());
	if(fill_default_and_auto_values) row.fillDefaultAndAutogeneratedValues();
	return row;
}

QFBasicTable::Row& QFBasicTable::insertRow(int before_row, const QFBasicTable::Row &_row) throw(QFException)
{
	qfLogFuncFrame() << "before_row:" << before_row << "row cnt:" << rowCount();
	if(columnCount() <= 0) QF_EXCEPTION(QObject::tr("Table has no columns, row can not be inserted."));
	if(!(tableProperties() == _row.tableProperties())) QF_EXCEPTION(QObject::tr("Inserted row comes from different table."));

	int r = (isValidRow(before_row))? before_row: rowCount();
	QFBasicTable::Row empty_row(_row);
	empty_row.setInsert();
	rowsRef().append(empty_row);
	rowIndexRef().insert(r, rows().count()-1);
	Row &row = rowRef(r);
	//if(fill_default_and_auto_values) row.fillDefaultAndAutogeneratedValues();
	return row;
}


QFBasicTable::Row QFBasicTable::singleRow()
{
	qfTrash() << QF_FUNC_NAME;// << QFLog::stackTrace();
	QFBasicTable::Row empty_row(tableProperties());
	return empty_row;
}

bool QFBasicTable::removeRowAndDrop(int ri, bool call_driver_drop) throw(QFException)
{
	//qfTrash() << QF_FUNC_NAME;
	if(!isValidRow(ri)) {
		//qfWarning() << QF_FUNC_NAME << "removing invalid row" << ri;
		return false;
	}
	int ix = rowNumberToRowIndex(ri);
	Row &row = rowsRef()[ix];
	if(call_driver_drop) row.drop();
	rowsRef().removeAt(ix);
	rowIndexRef().remove(ri);
    	/// posun zbyvajici indexy
	RowIndexList &rlst = rowIndexRef();
	for(int i=0; i<rlst.count(); i++) if(rlst[i]>ix) rlst[i]--;
	return true;
}

void QFBasicTable::revertRow(int ri)
{
	if(isValidRow(ri)) {
		Row &r = rowRef(ri);
		revertRow(r);
	}
}

void QFBasicTable::revertRow(QFBasicTable::Row &r)
{
	r.restoreValues();
}

bool QFBasicTable::postRow(QFBasicTable::Row &r) throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	r.post();
	return true;
}

bool QFBasicTable::postRow(int ri) throw(QFException)
{
	qfTrash() << QF_FUNC_NAME << ri;
	bool ret = false;
	if(isValidRow(ri)) {
		Row &r = rowRef(ri);
		ret = postRow(r);
	}
	return ret;
}

void QFBasicTable::createRowIndex()
{
	d->rowIndex.clear();
	d->rowIndex.resize(d->rows.count());
	for(int i=0; i<d->rows.count(); i++) d->rowIndex[i] = i;
}

int QFBasicTable::rowNumberToRowIndex(int rowno) const
{
	int ix = rowIndex()[rowno];
	QF_ASSERT(ix >= 0 && ix < d->rows.count(), QString("Row index corrupted, wanted row: %1, rows count: %2").arg(ix).arg(d->rows.count()));
	return ix;
}

QFBasicTable::Row& QFBasicTable::rowRef(int ri) throw(QFException)
{
	//qfTrash() << QF_FUNC_NAME;
	if(!isValidRow(ri)) QF_EXCEPTION(QString("row: %1 is out of range of rows (%2)").arg(ri).arg(rowCount()));
	return d->rows[rowNumberToRowIndex(ri)];
}

QFBasicTable::Row QFBasicTable::row(int ri, bool throw_exc) const throw(QFException)
{
	if(!isValidRow(ri)) {
		if(throw_exc) QF_EXCEPTION(QString("row: %1 is out of range of rows (%2)").arg(ri).arg(d->rows.size()));
		return QFBasicTable::Row();
	}
	return d->rows[rowNumberToRowIndex(ri)];
}

QFBasicTable::Row QFBasicTable::lastRow(bool throw_exc) const throw( QFException )
{
	return row(rowCount() - 1, throw_exc);
}

void QFBasicTable::reload()
{
	createRowIndex();
	resort();
	//emit reloaded();
}

static QString quote_CSV(const QString &s, const QFBasicTable::TextExportOptions &opts)
{
	QString ret = s;
	bool quote = false;
	if(opts.fieldQuotingPolicy() == QFBasicTable::TextExportOptions::IfNecessary) {
		if(ret.indexOf('"') >= 0) {ret = ret.replace('"', "\"\""); quote = true;}
		if(!quote && ret.indexOf('\n') >= 0) quote = true;
		if(!quote && ret.indexOf(opts.fieldSeparator()) >= 0) quote = true;
		if(!quote && ret.indexOf('#') >= 0) quote = true; /// extended CVS, # je komentar
	}
	else if(opts.fieldQuotingPolicy() == QFBasicTable::TextExportOptions::Always) {
		quote = true;
	}
	if(quote) ret = "\"" + ret + "\"";
	return ret;
}

void QFBasicTable::exportCSV(QTextStream &ts, const QString col_names, QFBasicTable::TextExportOptions opts) const throw(QFException)
{
	QList<int> ixs;
	//emit progressValue(0, tr("Exportuji CSV"));
	if(col_names == "*") {
		for(int i=0; i<fields().count(); i++) ixs.append(i);
	}
	else {
		QStringList sl = col_names.split(',', QString::SkipEmptyParts);
		foreach(QString s, sl) {
			s = s.trimmed();
			int ix = fields().fieldNameToIndex(s, !Qf::ThrowExc);
			if(ix >= 0) ixs.append(ix);
			else QFException("Field not found: " + s);
		}
	}
	if(opts.isExportColumnNames()) {
		/// export field names
		for(int i=0; i<ixs.count(); i++) {
			if(i > 0) ts << opts.fieldSeparator();
			ts << field(ixs[i]).name();
		}
		ts << '\n';
	}
	// export data
	// export data
	//int n = 0, cnt = rowCount(), steps = 100, progress_step = cnt / steps + 1;
	int n1 = opts.fromLine();
	int n2 = opts.toLine();
	if(n2 < 0) n2 = INT_MAX;
	for(int i=n1; i<rowCount() && i<=n2; i++) {
		Row r = row(i);
		//if(cnt) if(n++ % progress_step) emit progressValue(1.*n/cnt, tr("Probiha export"));
		for(int i=0; i<ixs.count(); i++) {
			if(i > 0) ts << opts.fieldSeparator();
			ts << quote_CSV(r.value(ixs[i]).toString(), opts);
		}
		ts << '\n';
	}
	//emit progressValue(-1);
}

const QString QFBasicTable::CVSTableEndTag = "#CVS_TABLE_END";

void QFBasicTable::importCSV(QTextStream &ts, TextImportOptions opts) throw(QFException)
{
	qfLogFuncFrame();
	QFString s;
	QStringList sl;
	//emit progressValue(0, tr("Importuji CSV"));
	qfTrash() << "\t opts fieldSeparator:" << opts.fieldSeparator();
	qfTrash() << "\t opts ignoreFirstLinesCount:" << opts.ignoreFirstLinesCount();
	qfTrash() << "\t opts isImportColumnNames:" << opts.isImportColumnNames();
	QFCsvReader reader(&ts, opts.fieldSeparator().toLatin1(), opts.fieldQuotes().toLatin1());
	if(!opts.isImportAppend()) clear();
	for(int i=0; i<opts.ignoreFirstLinesCount(); i++) reader.readCsvLine();
	if(opts.isImportColumnNames()) {
		sl = reader.readCsvLineSplitted();
		foreach(s, sl) {
			//qfTrash() << "\tfield:" << s;
			QFSqlField fld(s, QVariant::String);
			fieldsRef().append(fld);
		}
	}
	//int n = 0, cnt = 100, steps = 100, progress_step = cnt / steps;
	while(!ts.atEnd()) {
		//if(((n++)%steps) % progress_step) emit progressValue(1.*n / cnt, tr("Probiha import"));
		sl = reader.readCsvLineSplitted();
		if(!sl.isEmpty()) {
			if(sl[0] == CVSTableEndTag) break;
			s = sl[0];
			if(s[0] == '#') continue; /// CVS comment (my CVS extension)
			if(fields().isEmpty()) {
				/// pokud nejsou vytvoreny fieldy, treba protoze se neimpoortovaly nazvy sloupcu, udelej je z tohoto radku
				int colno = 0;
				foreach(s, sl) {
					QFSqlField fld(QString("col%1").arg(++colno), QVariant::String);
					fieldsRef().append(fld);
				}
			}
			Row r(tableProperties());
			for(int i=0; i<fields().count() && i<sl.count(); i++) {
				r.setValue(i, sl[i]);
			}
			r.clearOrigValues();
			d->rows.append(r);
		}
	}
	reload();
	//emit progressValue(-1);
}

static QList<int> parse_field_structure(QFBasicTable &table, const QString & file_structure_definition, QVariantMap & parsed_props)
{
	qfLogFuncFrame();
	QList<int> col_mapping;
	if(!file_structure_definition.isEmpty()) {
		table.fieldsRef().clear();
		QFString fs = file_structure_definition;
		QStringList lines = fs.splitAndTrim('\n');
		QFString content;
		foreach(fs, lines) {
			int ix = fs.indexOf('#');
			if(ix >= 0) fs = fs.slice(0, ix).trim();
			if(fs.isEmpty()) continue;
			content += fs;
		};
		QStringList sl = content.splitVector();
		//QFBasicTable::ColumnDefList cd_list;
		foreach(fs, sl) {
			QString key;
			QFString val;
			int ix = fs.indexOf(':');
			if(ix >= 0) {
				key = fs.slice(0, ix).trim();
				val = fs.slice(ix+1).trim();
				if(val[0] == '\'') val = val.slice(1, -1);
			}
			else key = fs.trim();
			//qfInfo() << "key:" << key << "val:" << val;
			if(val[0] == '{') parsed_props[key] = val.splitMapRecursively();
			//else if(val[0] == '[') values[key] = split_array(val);
			else parsed_props[key] = val;
			
			if(key == "columns") {
				QStringList sl2 = val.splitVector();
				int mapped_col_no = 0;
				foreach(fs, sl2) {
					ix = fs.indexOf(':');
					if(ix >= 0) {key = fs.slice(0, ix).trim(); val = fs.slice(ix+1).trim();}
					else {key = fs.trim(); val = QString();}
					if(key == "skip-columns") {
						mapped_col_no += val.toInt();
						continue;
					}
					{
						QFBasicTable::ColumnDef cd(key, val);
						QFSqlField fld(cd.name, cd.type);
						fld.setLength(cd.length);
						fld.setPrecision(cd.prec);
						table.fieldsRef().append(fld);
						qfTrash() << "\t adding field:" << fld.toString();
						qfTrash() << "\t mapped to column:" << mapped_col_no;
					}
					col_mapping << mapped_col_no++;
				}
			}
		}
	}
	return col_mapping;
}

static QChar field_delimiter_helper(const QString &_delimiter)
{
	QChar field_delimiter;
	if(_delimiter == "\\n") field_delimiter = '\n';
	else if(_delimiter == "\\t") field_delimiter = '\t';
	else if(_delimiter.count() > 0) field_delimiter = _delimiter[0];
	return field_delimiter;
}


void QFBasicTable::importTXT(QTextStream &ts, const QString &file_structure_definition) throw(QFException)
{
	qfLogFuncFrame();
	QVariantMap parsed_props;
	QList<int> col_mapping = parse_field_structure(*this, file_structure_definition, parsed_props);
	//QList<int> delimiter_positions;
	qfTrash() << "\t parsed_props:" << QFJson::variantToString(parsed_props);
	
	QString codec_name = parsed_props.value("text-codec", "utf8").toString();
	QTextCodec *codec = QTextCodec::codecForName(codec_name.toAscii());
	if(!codec) QF_EXCEPTION(QString("Cannot find text codec for name '%1'.").arg(codec_name));
	ts.setCodec(codec);
	
	QChar field_delimiter = field_delimiter_helper(parsed_props.value("field-delimiter", ";").toString());
	QChar field_quote = QFString(parsed_props.value("field-quote", QString()).toString()).value(0);
	qfTrash() << "\t field_delimiter:" << field_delimiter;
	qfTrash() << "\t field_quote:" << field_quote;
	bool preserve_empty_lines = parsed_props.value("preserve-empty-lines", 0).toBool();
	QStringList lines;
	if(field_delimiter == '\n') {
		QString s = ts.readAll();
		lines << s.trimmed();
	}
	else {
		while(!ts.atEnd()) {
			QFString line;
			int qcnt = 0;
			do {
				QString s = ts.readLine();
				qfTrash().noSpace() << "\t read line: ###" << s << "###";
				qcnt += s.count(field_quote);
				line += s;
				if(qcnt % 2) line += "\n";
			} while((qcnt % 2) && !ts.atEnd());
			qfTrash().noSpace() << "\t whole line: ###" << line << "###";
			if(line.isEmpty() && !preserve_empty_lines) continue;
			//qfInfo().noSpace() << "\t encoded line: ###" << s << "###";
			lines << line;
		}
		//qfInfo().noSpace() << "\t <EOF>";
	}
	int skip_lines = parsed_props.value("skip-lines", 0).toInt();
	QVariantMap original_column_definition = parsed_props.value("columns").toMap();
	for(int line_no=skip_lines; line_no<lines.count(); line_no++) {
		QFString line = lines[line_no];
		qfTrash().noSpace() << "\t parsing line: ###" << line << "###" << line_no << "/" << lines.count();
		QStringList field_list = line.splitAndTrim(field_delimiter, field_quote, QFString::TrimParts, QString::KeepEmptyParts);
		Row r = singleRow();
		for(int col_no=0; col_no<fields().count(); col_no++) {
			int imported_col_no = col_mapping[col_no];
			if(imported_col_no >= field_list.count()) {
				qfWarning() << QString("invalid field mapping %1->%2 for field '%3'").arg(col_no).arg(imported_col_no).arg(fields()[col_no].fieldName());
				qfInfo() << "line:" << line_no << "fields count:" << field_list.count();
				for(int i=0; i<field_list.count(); i++) qfInfo().noSpace() << i << ": '" << field_list[i] << "'";
				continue;
			}
			QFString fs = field_list[imported_col_no];
			{
				QString fld_name = field(col_no).fieldName();
				QVariantMap def_map = original_column_definition.value(fld_name).toMap();
				int start_index = def_map.value("start-index", 0).toInt();
				int end_index = def_map.value("end-index", INT_MAX).toInt();
				//qfInfo().noSpace() << "\tsource string: '" << fs << "'";
				//qfInfo() << "\t start index: " << start_index;
				//qfInfo() << "\t end index: " << end_index;
				fs = fs.slice(start_index, end_index).trim();
				//qfInfo().noSpace() << "\t [" << fld_name << "] = '" << fs << "'";
			}
			QVariant v;
			//if(!fs.isEmpty()) v = fs; /// prazdne hodnoty jsou NULL pro klaestables (je to moznast, lae zatim to asi nevyuzivam)
			/// pokud by se ukazala potreba NULL hodnoty v klaes tabulce, pridam nullable flag do definice sloupce
			v = fs;
			r.setValue(col_no, v);
		}
		//qfInfo() << "\t appending row:" << r.toString(", ");
		//r.post();
		r.clearOrigValues();
		appendRow(r);
		//rowsRef().append(r);
		//qfTrash() << "\t rowsRef().count():" << rowsRef().count();
	}
	//QFBasicTable::reload();
	//qfInfo() << "\n" << dump();
	qfTrash() << "\t row count:" << rowCount();
}

QString QFBasicTable::toString() const
{
	char sep = '\t';
	QString s;
	QTextStream ts(&s);
	//ts.setPadChar('%');
	ts << "       ";
	for(int i=0; i<fields().count(); i++) {
		if(i > 0) ts << sep;
		ts << field(i).name();
	}
	ts << '\n';
	// export data
	int no = 0;
	QString s1 = "%1.";
	for(int i=0; i<rowCount(); i++) {
		Row r = row(i);
		ts << s1.arg(++no, 5) << sep;
		for(int i=0; i<fields().count(); i++) {
			if(i > 0) ts << sep;
			ts << r.value(i).toString();
		}
		ts << '\n';
	}
	return s;
}
/*
void QFBasicTable::setSortCaseSensitive(bool cs)
{
	//qfTrash() << QF_FUNC_NAME << ics;
	if(cs) tablePropertiesRef().sortOptionsRef() << TableProperties::SortCaseSensitive;
	else   tablePropertiesRef().sortOptionsRef() >> TableProperties::SortCaseSensitive;
	//qfTrash() << "\tcurrent state:" << isSortCaseInsensitive();
}

bool QFBasicTable::isSortCaseSensitive() const
{
	TableProperties::SortOptions opts = tableProperties().sortOptions();
	//qfTrash() << QF_FUNC_NAME << (int)opts << QString::number(~TableProperties::SortCaseInsensitive, 16) << opts.contains(TableProperties::SortCaseInsensitive);
	return opts.contains(TableProperties::SortCaseSensitive);
}
*/
void QFBasicTable::sort(const QString &_colnames) throw(QFException)
{
	SortDefList sd;
	QFString colnames = _colnames;
	QStringList sl = colnames.splitAndTrim(',');
	foreach(QFString s, sl) {
		int ix = s.pos(' ');
		bool asc = true;
		bool cs = true;
		bool ascii7bit = true;
		QString colname = s;
		if(ix >= 0) {
			colname = s.slice(0, ix).trimmed();
			s = s.slice(ix+1).trimmed().toUpper();
			asc = s.indexOf("DESC") >= 0;
			cs = !(s.indexOf("ICS") >= 0);
		}
		ix = fields().fieldNameToIndex(s);
		sd.append(SortDef(ix, asc, cs, ascii7bit));
	}
	sort(sd);
}

void QFBasicTable::sort(const SortDef &sorted_field) throw(QFException)
{
	SortDefList sdl;
	if(sorted_field.isValid()) sdl << sorted_field;
	sort(sdl);
}

void QFBasicTable::sort(const QFBasicTable::SortDefList &sorted_fields) throw(QFException)
{
	//qfTrash() << QF_FUNC_NAME << QLocale().name();
	sort(sorted_fields, 0, rowCount());
}

void QFBasicTable::sort(const SortDefList & sorted_fields, int start_row_index, int row_count) throw( QFException )
{
	tablePropertiesRef().setSortDefinition(sorted_fields);
	qfTrash() << QF_FUNC_NAME << QLocale().name();
	if(sorted_fields.isEmpty()) {
		createRowIndex();
	}
	else {
		for(int i=0; i<tableProperties().sortDefinition().count(); i++) {
			//qfTrash() << "\t field index:" << tableProperties().sortDefinition()[i].fieldIndex;
			//qfTrash() << "\t ascending:" << tableProperties().sortDefinition()[i].ascending;
		}
		RowIndexList::iterator begin = rowIndexRef().begin() + start_row_index;
		RowIndexList::iterator  end = begin + row_count;
		sort(begin, end);
		//stable_sort(rowIndexRef().begin(), rowIndexRef().end(), QFBasicTableLessThan(*this));
	}
}

void QFBasicTable::resort() throw(QFException)
{
	sort(tableProperties().sortDefinition());
}

int QFBasicTable::seek(const QVariant &v) const
{
	const QFBasicTable::SortDefList &sdl = tableProperties().sortDefinition();
	if(sdl.count() == 0) QF_EXCEPTION("Empty sort definition.");
	const QFBasicTable::SortDef &sd = sdl[0];
	int seek_ix = sd.fieldIndex;
	if(!fields().isValidFieldIndex(seek_ix)) QF_EXCEPTION(QString("Invalid sort field index: %1").arg(seek_ix));
	if(!sd.ascending) QF_EXCEPTION("Ascending sort is needed for seek.");
	RowIndexList::const_iterator res = binaryFind(rowIndex().begin(), rowIndex().end(), v);
	if(res == rowIndex().end()) return -1;
	return res - rowIndex().begin();
}

int QFBasicTable::find(int field_ix, const QVariant &val) const
{
	int ret = -1;
	RowList row_lst = rows();
	for(int ix=0; ix < row_lst.count(); ix++) {
		const Row &r = row_lst.value(ix);
		if(r.value(field_ix) == val) {
			ret = ix;
		}
	}
	return ret;
}

int QFBasicTable::find(const QString &field_name, const QVariant &val) const
{
	int field_ix = fields().fieldNameToIndex(field_name);
	return find(field_ix, val);
}

QVariant QFBasicTable::sumValue(int field_ix) const throw(QFException)
{
	QVariant::Type type = field(field_ix).type();
	QVariant ret;
	switch(type) {
		case QVariant::Int:
		case QVariant::UInt:
		{
			int d = 0;
			foreach(Row r, rows()) d += r.value(field_ix).toInt();
			ret = d;
			break;
		}
		case QVariant::Double:
		default:
		{
			double d = 0;
			foreach(Row r, rows()) d += r.value(field_ix).toDouble();
			ret = d;
			break;
		}
	}
	return ret;
}

#if 0
QDomElement QFBasicTable::toXmlTableElement(QDomDocument &owner_doc, const QString col_names) const throw(QFException)
{
	QList<int> ixs;
	emit progressValue(0, tr("Exportuji XML"));
	if(col_names == "*") {
		for(int i=0; i<fields().count(); i++) ixs.append(i);
	}
	else {
		QStringList sl = col_names.split(',', QString::SkipEmptyParts);
		foreach(QString s, sl) {
			int ix = fields().fieldNameToIndex(s, !Qf::ThrowExc);
			if(ix >= 0) ixs.append(ix);
			else QFException("Field not found:" + s);
		}
	}
	QDomElement el_table = owner_doc.createElement("table");
	/// export columns
	QDomElement el_cols = el_table.appendChild(owner_doc.createElement("cols")).toElement();
	for(int i=0; i<ixs.count(); i++) {
		QFSqlField f = field(ixs[i]);
		QDomElement el = el_cols.appendChild(owner_doc.createElement("col")).toElement();
		el.setAttribute("name", f.fullName());
		el.setAttribute("caption", f.name());
		el.setAttribute("datatype", QVariant::typeToName(f.type()));
	}
	/// export data
	int n = 0, cnt = rowCount(), steps = 100, progress_step = cnt / steps + 1;
	QDomElement el_rows = el_table.appendChild(owner_doc.createElement("rows")).toElement();
	for(int i=0; i<rowCount(); i++) {
		Row r = row(i);
		if(cnt) if(n++ % progress_step) emit progressValue(1.*n/cnt, tr("Probiha export"));
		QString s;
		for(int i=0; i<ixs.count(); i++) {
			if(i > 0) s += ",";
			s += quoteCSV(r.value(ixs[i]).toString());
		}
		QDomElement el = el_rows.appendChild(owner_doc.createElement("row")).toElement();
		el.appendChild(owner_doc.createCDATASection(s));
	}
	emit progressValue(-1);
	return el_table;
}
#else
QFXmlTable QFBasicTable::toXmlTable(QDomDocument & owner_doc, const QString & table_name, const QString & col_names) const throw( QFException )
{
	qfLogFuncFrame();
	QList<int> ixs;
	//emit progressValue(0, tr("Exportuji XML"));
	if(col_names.isEmpty() || col_names == "*") {
		for(int i=0; i<fields().count(); i++) ixs.append(i);
	}
	else {
		QStringList sl = col_names.split(',', QString::SkipEmptyParts);
		foreach(QString s, sl) {
			int ix = fields().fieldNameToIndex(s, !Qf::ThrowExc);
			if(ix >= 0) ixs.append(ix);
			else QFException("Field not found:" + s);
		}
	}
	//QDomElement el_table = owner_doc.createElement("table");
	QFXmlTable t(owner_doc, table_name);
	/// export columns
	for(int i=0; i<ixs.count(); i++) {
		QFSqlField f = field(ixs[i]);
		t.appendColumn(f.fullName(), f.type());
	}
	/// export data
	//int n = 0, cnt = rowCount(), steps = 100, progress_step = cnt / steps + 1;
	for(int i=0; i<rowCount(); i++) {
		Row r = row(i);
		QFXmlTableRow t_r = t.appendRow();
		//if(cnt) if(n++ % progress_step) emit progressValue(1.*n/cnt, tr("Probiha export"));
		for(int i=0; i<ixs.count(); i++) t_r.setValue(i, r.value(ixs[i]));
	}
	//emit progressValue(-1);
	return t;
}
#endif

QDomElement QFBasicTable::toHtmlElement(QDomDocument &owner_doc, const QString & col_names) const throw( QFException )
{
	QList<int> ixs;
	//emit progressValue(0, tr("Exportuji XML"));
	if(col_names.isEmpty() || col_names == "*") {
		for(int i=0; i<fields().count(); i++) ixs.append(i);
	}
	else {
		QStringList sl = col_names.split(',', QString::SkipEmptyParts);
		foreach(QString s, sl) {
			int ix = fields().fieldNameToIndex(s, !Qf::ThrowExc);
			if(ix >= 0) ixs.append(ix);
			else QFException("Field not found:" + s);
		}
	}
	QDomElement el_table = owner_doc.createElement("table");
	el_table.setAttribute("border", 1);
	/// export columns
	QDomElement el_row = owner_doc.createElement("tr");
	for(int i=0; i<ixs.count(); i++) {
		QFDomElement el_th = owner_doc.createElement("th");
		QFSqlField f = field(ixs[i]);
		el_th.setText(f.fullName());
		el_row.appendChild(el_th);
	}
	el_table.appendChild(el_row);
	/// export data
	//int n = 0, cnt = rowCount(), steps = 100, progress_step = cnt / steps + 1;
	for(int i=0; i<rowCount(); i++) {
		Row r = row(i);
		QDomElement el_row = owner_doc.createElement("tr");
		for(int i=0; i<ixs.count(); i++) {
			QFDomElement el_td = owner_doc.createElement("td");
			el_td.setText(r.value(ixs[i]).toString());
			el_row.appendChild(el_td);
		}
		el_table.appendChild(el_row);
	}
	//emit progressValue(-1);
	return el_table;
}

QVariantMap QFBasicTable::toJson(const QString &col_names) const throw( QFException )
{
	QList<int> ixs;
	if(col_names.isEmpty() || col_names == "*") {
		for(int i=0; i<fields().count(); i++) ixs.append(i);
	}
	else {
		QStringList sl = col_names.split(',', QString::SkipEmptyParts);
		foreach(QString s, sl) {
			int ix = fields().fieldNameToIndex(s, !Qf::ThrowExc);
			if(ix >= 0) ixs.append(ix);
			else QFException("Field not found:" + s);
		}
	}
	QVariantMap obj_m;
	QVariantList flds_lst;
	for(int i=0; i<ixs.count(); i++) {
		QVariantMap fld_m;
		QFSqlField f = field(ixs[i]);
		fld_m["name"] = f.fullName();
		fld_m["type"] = QVariant::typeToName(f.type());
		flds_lst << fld_m;
	}
	obj_m["fields"] = flds_lst;
	/// export data
	QVariantList rows_lst;
	for(int i=0; i<rowCount(); i++) {
		Row r = row(i);
		QVariantList row_lst;
		for(int i=0; i<ixs.count(); i++) {
			row_lst << r.value(ixs[i]);
		}
		rows_lst << row_lst;
	}
	obj_m["rows"] = rows_lst;
	return obj_m;
}

QVariantList QFBasicTable::dataToVariantList() const
{
	QVariantList ret;
	foreach(Row r, rows()) {
		QVariantList lst;
		foreach(QVariant v, r.values()) lst << v;
		QVariant v = lst; /// pozor. 4.5 maji appent pro QList
		///ret.append(lst); tohle nefunguje
		ret.append(v);
	}
	return ret;
}

void QFBasicTable::dataFromVariantList(const QVariantList &_lst) throw(QFException)
{
	clearData();
	foreach(QVariant _v, _lst) {
		QVariantList lst = _v.toList();
		Row r(tableProperties());
		int i = 0;
		foreach(QVariant v, lst) {
			if(i < columnCount()) r.setValue(i++, v);
		}
		r.clearOrigValues();
		d->rows.append(r);
	}
	reload();
}

QFBasicTable::ColumnDef::ColumnDef(const QString & _name, const QString & _def)
	: name(_name), type(QVariant::Invalid), length(-1), prec(0) 
{
	QFString fs = _def;
	QFString::StringMap map = fs.splitMap();
	QMapIterator<QString, QString> i(map);
	while (i.hasNext()) {
		i.next();
		QString s = i.key().toLower();
		if(s == "type") {
			QString s1 = i.value().toLower();
			if(!s1.isEmpty()) {
				if(s1 == "string") type = QVariant::String;
				else if(s1 == "date") type = QVariant::Date;
				else if(s1 == "datetime") type = QVariant::DateTime;
				else if(s1 == "time") type = QVariant::Time;
				else type = QVariant::nameToType(qPrintable(s1));
				//qfInfo() << "s1:" << s1 << qPrintable(s1) << "type:" << type;
				//qfInfo() << "name:" << name << "type:" << QVariant::typeToName(type);
			}
		}
		else if(s.startsWith("len")) length = i.value().toInt();
		else if(s.startsWith("prec")) prec = i.value().toInt();
	}
}

void QFBasicTable::sort(RowIndexList::iterator begin, RowIndexList::iterator end)
{
	qSort(begin, end, LessThan(*this));
}

QFBasicTable::RowIndexList::const_iterator QFBasicTable::binaryFind(QFBasicTable::RowIndexList::const_iterator begin, QFBasicTable::RowIndexList::const_iterator end, const QVariant & val) const
{
	return qBinaryFind(begin, end, val, LessThan(*this));
}






