
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDBXMLDOCUMENT_H
#define QFDBXMLDOCUMENT_H

#include <qfcoreglobal.h>

#include <qfxmlconfigdocument.h>


//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFDbXmlDocument : public QFSplittedXmlConfigDocument
{
	public:
		virtual ~QFDbXmlDocument() {}
};
   
#endif // QFDBXMLDOCUMENT_H

