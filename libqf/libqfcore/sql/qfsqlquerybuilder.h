
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLQUERYBUILDER_H
#define QFSQLQUERYBUILDER_H

#include <qf.h>
#include <qfcoreglobal.h>
#include <qfexception.h>

#include <QSet>
#include <QSharedData>

//! Implicitly shared.
class QFCORE_DECL_EXPORT QFSqlQueryBuilder 
{
	public:
		enum QueryMapKey {InvalidKey = 0, SelectKey, SelectFlagsKey, FromKey, WhereKey, GroupByKey, HavingKey, OrderByKey, LimitKey, OffsetKey, AsKey};

		typedef QMap<QueryMapKey, QString> QueryMap;
	protected:
		class Data : public QSharedData
		{
			public:
				QueryMap queryMap;
				QString recentlyMentionedRelation;
				QString recentlyParsedQuery;
				QSet<QString> columnBenList; ///< hidden columns don't have to be reloaded (speedup on slow connection)
		};
	private:
		QSharedDataPointer<Data> d;
		
		QString build(QueryMapKey key) const;
	protected:
		//QVariantMap& queryMap() {return f_queryMap()};
		QString buildSelect() const;
		QString buildFrom() const {return build(FromKey);}
		QString buildWhere() const {return build(WhereKey);}
		QString buildGroupBy() const {return build(GroupByKey);}
		QString buildHaving() const {return build(HavingKey);}
		QString buildOrderBy() const {return build(OrderByKey);}
		//!       Build LIMIT and OFFSET clauses.
		QString buildRest() const;
		QString buildQuery() const;
	public:
		void setColumnBenned(const QString &column_name, bool b = true);
		
		bool parse(const QString &query_string, bool throw_exc = Qf::ThrowExc) throw(QFSqlException);
		QString toString() const {return buildQuery();}
		/**
		 *  Returns recently parsed query string.
		 */
		QString recentlyParsedQuery() const {return d->recentlyParsedQuery;}

		//! Removes part of query with key \a key . Returns removed part.
		QString take(QueryMapKey key);
		//QString value(QueryMapKey key);
		//! If \a parse() is not successfull, \a isEmpty() returns true.
		bool isEmpty() const {return d->queryMap.isEmpty();}
		
		/**
		 * Example:
			\code
		QFSqlQueryBuilder qb;
		qb.select("zakazky.*, jmena.psc", "distinct").select("jmena.name")
			.from("zakazky")
			.join("zakaznikID", "kontakty AS jmena", "id")
			.where("zakazky.id = 11 AND vystavilDne > '2006-01-01'")
			.where("AND jmena.zatrideni = 1")
			.orderBy(zakazky.cislo)
			.limit(1);
		\endcode
		or
		\code
		QFSqlQueryBuilder qb_inner;
		qb_inner.select("*").from("kontakty")
			.where("zatrideni = 1")
			.as(jmena);
		QFSqlQueryBuilder qb;
		qb.select("zakazky.*, jmena.psc", "distinct").select("jmena.name")
			.from("zakazky")
			.join("zakaznikID", qb_inner, "id")
			.where("zakazky.id = 11 AND vystavilDne > '2006-01-01'")
			.where("jmena.zatrideni = 1")
			.orderBy(zakazky.cislo)
			.limit(1);
		\endcode
		 * @param flags  Can be [ DISTINCT | ALL | DITINCT ON expr ]
		 */
		QFSqlQueryBuilder& select(const QString &fields, const QString &flags = QString());
		//! Jako select(const QString &fields, const QString &flags = QString()), ale pred kazdy field z fields prida table_name a tecku.
		QFSqlQueryBuilder& select2(const QString &table_name, const QString &fields, const QString &flags = QString());
		/**
		 * @param table_name For details see join() parameter \a t2_name .
		 */
		QFSqlQueryBuilder& from(const QString &table_name);
		QFSqlQueryBuilder& from(const QFSqlQueryBuilder &table);
		/**
		 *  Join table to query.
		 * @param t1_key Can be in form tablename.keyname or only keyname. In such a case a tablename is taken from previous join() or from(). See \a recentlyMentionedTable . 
		 * @param t2_name Can be table name or SELECT ... . In both cases a last word is considered to be a tablename. 
		 */
		QFSqlQueryBuilder& join(const QString &t1_key, const QString &t2_name, const QString &t2_key, const QString  &join_kind = "LEFT JOIN");
		//! @param t2_key in form tablename.keyname.
		//! See \a join(const QString &t1_key, const QString &t2_name, const QString &t2_key, const QString  &join_kind) .
		QFSqlQueryBuilder& join(const QString &t1_key, const QString &t2_key);
		//! @param t1_key in form tablename.keyname or keyname, in such a case recentlyMentionedRelation is considered to be a tablename.
		QFSqlQueryBuilder& join(const QString &t1_key, const QFSqlQueryBuilder &t2, const QString &t2_key, const QString  &join_kind = "LEFT JOIN");
		//! Tady si muzu do joinu napsat co chci (vcetne join_kind), nekdy to jinak nejde.
		QFSqlQueryBuilder& join(const QString &join);
		/**
		 * @param oper Condition can be chained using multiple calls of where(), than \a oper is used to join logical conditions.
		 */
		QFSqlQueryBuilder& where(const QString &cond, const QString &oper = "AND");
		QFSqlQueryBuilder& groupBy(const QString &expr);
		QFSqlQueryBuilder& having(const QString &cond, const QString &oper = "AND");
		/**
		 * @param field_and_order_flag order flag can be omited, in such a case ascending order is considered.
		 */
		QFSqlQueryBuilder& orderBy(const QString &field_and_order_flag);
		//QFSqlQueryBuilder& orderBy(const QStringList &fields);
		QFSqlQueryBuilder& limit(int n);
		QFSqlQueryBuilder& offset(int n);
		QFSqlQueryBuilder& as(const QString &alias_name);
	public:
		QFSqlQueryBuilder(const QString &query_to_parse = QString());
		virtual ~QFSqlQueryBuilder();
};
   
#endif // QFSQLQUERYBUILDER_H

