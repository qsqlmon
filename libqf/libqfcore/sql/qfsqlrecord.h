//
// C++ Interface: qfsqlrecord
//
// Description: 
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef QFSQLRECORD_H
#define QFSQLRECORD_H

#include <QSqlRecord>

#include <qfsqlfield.h>

/**
@deprecated not used any more
@author Fanda Vacek
*/
class QFCORE_DECL_EXPORT QFSqlRecord : public QSqlRecord
{
public:
	QFSqlRecord();
	QFSqlRecord(const QSqlRecord & other);
	~QFSqlRecord();

	//QFSqlField fieldqf(int index) const;
	//QFSqlField field(const QString & name) const;
};

#endif
