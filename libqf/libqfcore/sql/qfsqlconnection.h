#ifndef QFSQLCONNECTION_H
#define QFSQLCONNECTION_H

#include <qfsqlconnectionbase.h>
#include <qfsqlcatalog.h>

class QFSqlCatalog;

class QFCORE_DECL_EXPORT QFSqlJournal
{
	public:
		virtual void log(const QString &) {}
	public:
		virtual ~QFSqlJournal() {}
};

//! SQL connection with a catalog. Explicitly shared !
class QFCORE_DECL_EXPORT QFSqlConnection : public QFSqlConnectionBase
{
	protected:
		struct QFCORE_DECL_EXPORT Data : public QSharedData
		{
			QFSqlCatalog *catalog;
			QFSqlJournal *journal;
			
			Data() : catalog(NULL), journal(NULL) {}
			~Data();
		};
	private:
		QExplicitlySharedDataPointer<Data> d;
	public:
		//! \a open() is the place where connection catalog is created.
		void open(const ConnectionOptions &options = ConnectionOptions()) throw(QFSqlException);
		void open(const QString & user, const QString & password, const ConnectionOptions &options = ConnectionOptions()) throw(QFSqlException)
		{
			setUserName(user);
			setPassword(password);
			open(options);
		}
		void close();

		//! A catalog is explicitly shared and created when connection is opened.
		//! Catalogs are separate per opened connection, but all connection copies shares same catalog.
		QFSqlCatalog& catalog() const throw(QFSqlException);
		void setJournal(QFSqlJournal *journal) {d->journal = journal;}
		QFSqlJournal* journal() const {return d->journal;}
	public:
		QFSqlConnection();
		QFSqlConnection(const QSqlDatabase& qdb);
		explicit QFSqlConnection(const QString &driver_name);
		virtual ~QFSqlConnection();
};

#endif // QFSQLCONNECTION_H
