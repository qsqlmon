INCLUDEPATH += $$PWD

include($$PWD/drivers/drivers.pri)

HEADERS +=      \
    $$PWD/qfsql.h      \
    $$PWD/qfsqlquery.h      \
    $$PWD/qfsqlfield.h      \
    $$PWD/qfsqlcatalog.h      \
    $$PWD/qfsqlconnectionbase.h      \
    $$PWD/qfsqlconnection.h      \
	$$PWD/qfsqlquerybuilder.h     \
	$$PWD/qfdbxmlconfig.h    \
	$$PWD/qfsqlpersistentdata.h  \
	$$PWD/qfbasictable.h  \
	$$PWD/qfsqlquerytable.h    \

SOURCES +=      \
    $$PWD/qfsql.cpp      \
    $$PWD/qfsqlquery.cpp      \
    $$PWD/qfsqlfield.cpp      \
    $$PWD/qfsqlcatalog.cpp      \
    $$PWD/qfsqlconnectionbase.cpp      \
    $$PWD/qfsqlconnection.cpp      \
	$$PWD/qfsqlquerybuilder.cpp     \
	$$PWD/qfdbxmlconfig.cpp    \
	$$PWD/qfsqlpersistentdata.cpp  \
	$$PWD/qfbasictable.cpp  \
	$$PWD/qfsqlquerytable.cpp    \


