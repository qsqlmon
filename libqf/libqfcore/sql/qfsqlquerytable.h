
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLQUERYTABLE_H
#define QFSQLQUERYTABLE_H

#include <qfcoreglobal.h>
//#include <qfsqlquery.h>
#include <qfsqlstoragedriver.h>
//#include <qfsqlquerybuilder.h>

#include <qfbasictable.h>

//class QFSqlStorageDriverProperties;

//! Explicitly shared.
class QFCORE_DECL_EXPORT QFSqlQueryTable : public QFBasicTable
{
	//Q_OBJECT
	public:
		typedef QMap<QString, QString> StringMap;
	protected:
		bool resultHasAllPriKeys(const QString &tableid) const throw(QFException);

		//QFSqlConnection& connectionRef()  throw(QFException);
		QFSqlStorageDriverProperties storageDriverProperties() const;
		void setStorageDriverProperties(const QFSqlStorageDriverProperties& props);

		//StringMap& foreignKeyMapRef() {return storageDriverPropertiesRef().foreignKeyMapRef();}
		//const StringMap& foreignKeyMap() const {return storageDriverProperties().foreignKeyMap();}
	public:
		// use constructor instead
		//void setConnection(const QFSqlConnection &con) {d->connection = con;}
		//virtual QFSqlConnection connection() const throw(QFException) {return storageDriverProperties().connection();}
	public:
		//QFSqlQuery query() throw(QFException) {return storageDriverProperties().query();}

		QString recentSelect() const {return storageDriverProperties().queryBuilder().recentlyParsedQuery();}
		//QFSqlQueryBuilder& queryBuilderRef() {return storageDriverPropertiesRef().queryBuilderRef();}
		const QFSqlQueryBuilder& queryBuilder() const {return storageDriverProperties().queryBuilder();}
		void setQueryBuilder(const QFSqlQueryBuilder &qb);
		//void setRecentSelect(const QString &s) {d->recentSelect = s.trimmed();}

		//int numRowsAffected() const {return storageDriverProperties().numRowsAffected();}

		void setQuery(const QString &query_str) {setQueryBuilder(query_str);}
		virtual void reload() throw(QFException) {reload(QString());}
		//! Pokud je \a query_str neprazdny, query builder se pokusi rozparsovat \a query_str.
		//! Pokud je \a query_str prazdny, pouzije se recentSelect().
		//! vraci num rows affected
		int reload(const QString query_str) throw(QFException);
		//! Return number of reloaded rows, 1 or 0 if row was deleted by some one other.
		virtual int reloadRow(QFBasicTable::Row &r) throw(QFException);

		//bool postRow(int ri) throw(QFException) {return QFBasicTable::postRow(ri);}
		//virtual bool postRow(Row &r) throw(QFException);
		//virtual Row& insertRow(int before_row) throw(QFException);
		//virtual bool removeRow(int ri) throw(QFException);
		// deletes row only in database, not in model !!!
		//bool deleteRowInDatabase(const Row &_r) throw(QFException);
		//bool postRowToDatabase(Row &r) throw(QFException);

		/// pokud zde je neco jako setForeignKey('objednavky.Id', 'objpatapet.objednavkaId'), insertne se i do joined tabulek
		/// keys jsou ve forme tablename.fieldname
		void addForeignKey(const QString &master_key_name, const QString &detail_key_name);
		//void setTableIds(const QStringList &table_ids);
	public:
		QFSqlQueryTable(const QFSqlConnection &_connection = QFSqlConnection());
		virtual ~QFSqlQueryTable();
};

#endif // QFSQLQUERYTABLE_H

