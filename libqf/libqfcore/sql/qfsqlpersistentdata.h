
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLPERSISTENTDATA_H
#define QFSQLPERSISTENTDATA_H

#include <qfcoreglobal.h>
#include <qfbasictable.h>

#include <QSharedData>

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFSqlPersistentData 
{
	private:
		class SharedDummyHelper {};
		struct Data : public QSharedData
		{
			QVariant id;
			QString idFieldName;
			QFBasicTable::Row persistentValues;
			QVariantMap properties;
			Data() {}
			~Data() {}
		};
		QSharedDataPointer<Data> d;

		QFSqlPersistentData(SharedDummyHelper); /// null row constructor
		static const QFSqlPersistentData& sharedNull();
	public:
		void setIdFieldName(const QString &id_field_name) {d->idFieldName = id_field_name;}
		bool isNull() const {return d == sharedNull().d;}
		QString idFieldName() const {return d->idFieldName;}
		QVariant id() const {return d->id;}
		void setId(const QVariant &_id) {d->id = _id;}
	protected:
		void setValuesRow(const QFBasicTable::Row &row) {d->persistentValues = row;}
		void setValuesRow(const QFBasicTable &tbl);
	public:
		virtual bool hasPersistentValue(const QString &name) const;
		virtual bool hasProperty(const QString &name) const;
		const QVariantMap& properties() const {return d->properties;}
		virtual bool hasValue(const QString &name) const {
			return hasProperty(name) || hasPersistentValue(name);
		}
		virtual QVariant value(const QString &name, bool throw_exc = Qf::ThrowExc) const  throw(QFException);
		virtual QVariant persistentValue(const QString &name, bool throw_exc = Qf::ThrowExc) const  throw(QFException);
		/// jako value, ale pokud \a name neexistuje, vraci \a default_value .
		virtual QVariant valueOrDefault(const QString &name, const QVariant &default_value = QVariant()) const {
			if(hasValue(name)) return value(name, !Qf::ThrowExc);
			return default_value;
		}
		/// pokud to nenajde v persistentValues, nastavi property
		virtual void setValue(const QString &name, const QVariant &value);
		/// pokud to nenajde v persistentValues, vrhne vyjjimku
		virtual void setPersistentValue(const QString &name, const QVariant &value) throw(QFException);
		virtual void setProperty(const QString &name, const QVariant &value);

		/// vrati hodnoty ve mape fieldname:value
		QVariantMap valuesMap() const;
	public:
		virtual void load() throw(QFException);
		virtual void save() throw(QFException);
	public:
		/// konstruktor jen kvuli kontainerum a jejich metode value() pro neexistujici klic, jinak nepouzivat
		/// pozor i sharedNull() je explicitly sdilenej, vsechno, co se do nej ulozi, maji vsechny jeho reference.
		QFSqlPersistentData();
		QFSqlPersistentData(const QString &id_field_name); ///< tenhle je ten spravnej
		virtual ~QFSqlPersistentData();
};

#endif // QFSQLPERSISTENTDATA_H

