
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFINTERVALMAP_H
#define QFINTERVALMAP_H

#include <qfcoreglobal.h>

#include <QMap>
#include <QVariant>

//! TODO: write class documentation.
template <typename K, typename V>
struct QFIntervalMapItem
{
	K lower;
	//K upper:
	V value;

	//QFIntervalMapItem(K l, K u, V v) : lower(l), upper(u), value(v) {}
	QFIntervalMapItem(const K &l, const V &v) : lower(l), value(v) {}
};

//! TODO: write class documentation.
template <typename K, class V>
class QFIntervalMap : public QMap<K, QFIntervalMapItem<K, V> >
{
	private:
		/*
		class Data : public QSharedData
		{
			public:
				bool dirty;
				QList<QFIntervalMapItem> list;
			public:
				Data() : dirty(false) {}
		};
    	QSharedDataPointer<Data> d;
		*/
	public:
		//bool isNull() const {return d.isNull();}
		//bool isEmpty() const {return isNull() || d->list.isEmpty();}
		void insert(const K &lower_and_upper, const V &val)
		{
			QMap<K, QFIntervalMapItem<K, V> >::insert(lower_and_upper, QFIntervalMapItem<K, V>(lower_and_upper, val));
		}
		void insert(const K &lower, const K &upper, const V &val)
		{
			QMap<K, QFIntervalMapItem<K, V> >::insert(upper, QFIntervalMapItem<K, V>(lower, val));
		}
		bool contains(const K &key_from_interval) const
		{
			return lowerBound(key_from_interval)->lower <= key_from_interval;
		}
		const V value(const K &key_from_interval) const
		{
			const QFIntervalMapItem<K, V> *pi = lowerBound(key_from_interval).operator->();
			if(pi->lower <= key_from_interval) return pi->value;
			return V();
		}
		const V value(const K &key_from_interval, const V &default_val) const
		{
			const QFIntervalMapItem<K, V> *pi = lowerBound(key_from_interval).operator->();
			if(pi->lower <= key_from_interval) return pi->value;
			return default_val;
		}
	public:
		//QFIntervalMap();
};

class QFCORE_DECL_EXPORT QFIntIntervalMap : public QFIntervalMap<int, QVariant>
{
	public:
		/// keys_str je neco jako 1,3,4-6,99,100
		/// nebo 1,4,5-
		/// nebo -
		/// proste, pokud chybi cislo pred pomlckou, da se tam INT_MIN, pokud za pomlckou INT_MAX
		void insertFromString(const QString &keys_str, const QVariant &val);
	public:
};

#endif // QFINTERVALMAP_H

