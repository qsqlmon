
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfintervalmap.h"

#include <qfstring.h>

#include <qflogcust.h>
/*
QFIntervalMap::QFIntervalMap(QObject *parent)
{
}

QFIntervalMap::~QFIntervalMap()
{
}
*/

void QFIntIntervalMap::insertFromString(const QString &keys_str, const QVariant &val)
{
	qfLogFuncFrame();
	QFString s = keys_str;
	QStringList sl = s.splitAndTrim(',');
	foreach(s, sl) {
		int ix = s.indexOf('-');
		bool ok;
		QString s1 = s, s2 = s;
		if(ix >= 0) {
			s1 = s.mid(0, ix).trimmed();
			s2 = s.mid(ix+1).trimmed();
		}
		int i1 = s1.toInt(&ok);
		if(!ok) i1 = INT_MIN;
		int i2 = s2.toInt(&ok);
		if(!ok) i2 = INT_MAX;
		qfTrash() << "\t inserting:" << i1 << i2 << val.toString();
		insert(i1, i2, val);
	}
}
