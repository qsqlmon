TEMPLATE = lib
win32:TARGET = libqfjson
unix:TARGET = qfjson
message(Target: $$TARGET)

QT += #xml sql network script

CONFIG += qt dll

CONFIG += hide_symbols

#message(INCLUDEPATH: $$INCLUDEPATH)

HEADERS += \
	qfjson.h \

SOURCES += \
	qfjson.cpp \

