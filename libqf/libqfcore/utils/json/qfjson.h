
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFJSON_H
#define QFJSON_H

#include <QTextStream>
#include <QVariantMap>
#include <QString>

#ifdef QF_PATCH
#  include <qfcoreglobal.h>
#  include <qfexception.h>
#else
#  define QFCORE_DECL_EXPORT Q_DECL_EXPORT
#  include <qglobal.h>
#  include <exception>
class QFCORE_DECL_EXPORT QFException : public std::exception
{
	protected:
		QByteArray f_what;
	public:
		virtual const char* what() const throw() { return f_what.constData(); }
	public:
		QFException(const QString &s) : std::exception(), f_what(s.toAscii()) {}
		virtual ~QFException() throw() {}
};
#endif

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFJson 
{
	public:
		enum KeyQuotingPolicy {QuotedKeysAlways, QuotedKeysIfNecessary};
		static const QString FIELDS_KEY;

		class QFCORE_DECL_EXPORT ParserOptions : public QVariantMap
		{
			public:
				bool isIncludeFieldList() const {return value("includeFieldList").toBool();}
				ParserOptions& setIncludeFieldList(bool v) {(*this)["includeFieldList"] = v; return *this;}
		};
	protected:
		static void putStringToStreamEscaped(QTextStream &stream, const QString &s);
	//static QVariant readObject_helper(QTextStream &stream);
		//static void writeObject_helper(QTextStream &stream, const QVariant &o);
		static QVariant getObject_helper(const QString &str, int from_ix, int &parse_end_at_ix, const ParserOptions &opts) throw(QFException);
	public:
		//static QString escapeJsonString(const QString &s);
		/**
		 *  Function simply skips leading spaces (if any) then it finds an object closing bracket.
	 	 * @param str String to parse.
		 * @return Index of JSON object closing bracket.
			-1 If the opening bracket is found but closing bracket is not found.
			-2 If the opening bracket is not found.
			-3 If the \a str is a blank one.
		 */
		static int indexOfObjectClosingBracket(const QString &str);
		/**
		 *  Get first json object from \a str and return it as QVariant.
		 *
		 * Javascript object is converted to QVariantMap, the list is converted to QVariantList.
		 * @param str String to parse.
		 * @param consumed_char_cnt index of first unused character in \a str . If the incomplete json expresion is parsed, \a consumed_char_cnt is set to -1.
		 * @return QVariant
		 */
		static QVariant stringToVariant(const QString &str, int *consumed_char_cnt = NULL, QString *errmsg = NULL, const ParserOptions &opts = ParserOptions());
		/**
		 * Write QVariant as JSON string.
		 *
		 * @param v QVariant to convert.
		 * @param key_quoting_policy If \a key_quoting_policy is QuotedKeysAlways, all generated json object field names are enclosed in the quotes. (Standard JSON)
		 * @return QString
		 */
		static QString variantToString(const QVariant &v, KeyQuotingPolicy key_quoting_policy = QuotedKeysIfNecessary);
	public:
		//QFJson(QObject *parent = NULL);
};

#endif // QFJSON_H

