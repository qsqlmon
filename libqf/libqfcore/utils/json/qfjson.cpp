//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfjson.h"

#include <cmath>

#ifdef QFCORE_BUILD_DLL
#  include <qflogcust.h>
#  include <qfxmlkeyvals.h>
#else
#  include<QDebug>
#  define qfTrash() qDebug()
#  define qfLogFuncFrame() qDebug()
#  define QF_EXCEPTION(what) throw QFException(what)
#endif

const QString QFJson::FIELDS_KEY = "__FIELDS__";

static int get_token(const QString &delimiters, const QString s, int from_ix)
{
	const static QChar quote = '"';
	//ushort quote_unicode = quote.unicode();
	//ushort c_unicode = c.unicode();
	bool in_quotes = false;
	int i;
	for(i=from_ix; i<s.count(); i++) {
		QChar c = s[i];
		if(c == quote) in_quotes = !in_quotes;
		else if(!in_quotes) {
			//qfInfo() << c;
			for(int i1=0; i1<delimiters.count(); i1++) {
				QChar c1 = delimiters[i1];
				//qfInfo() << '\t' << c << "vs." << c1;
				if(c1 == ' ') {if(c.isSpace()) return i;}
				else if(c == c1) return i;
			}
		}
	}
	return i;
}

static int skip_spaces(const QString s, int from_ix)
{
	for(; from_ix<s.count(); from_ix++) {
		if(!s[from_ix].isSpace()) break;
	}
	return from_ix;
}

int QFJson::indexOfObjectClosingBracket(const QString & str)
{
	qfLogFuncFrame();
	//qfInfo().noSpace() << "string len: " << str.count() << " str: ###" << str.mid(100) << "###";
	int ret = -1;
	int ix = skip_spaces(str, 0);
	if(ix >= str.count()) return -3;
	QChar c = str[ix];
	if(c != '{') {
		ret = -2;
	}
	else {
		int opens = 0;
		bool in_quotes = false;
		QChar prev_c;
		for(; ix < str.count(); ix++) {
			c = str[ix];
			//qfInfo() << "ix:" << ix << "c:" << c;
			if(prev_c != '\\') {
				if(c == '"') in_quotes = !in_quotes;
				else if(!in_quotes) {
					if(c == '{') opens++;
					else if(c == '}') opens--;
				}
			}
			if(opens == 0) {
				//qfInfo() << "bracket found:" << str.mid(ix - 100, ix);
				//qfInfo() << "rest:" << str.mid(ix);
				ret = ix;
				break;
			}
			prev_c = c;
		}
	}
	//qfInfo() << "\t return:" << ret;
	return ret;
}

QVariant QFJson::getObject_helper(const QString & str, int from_ix, int & parse_end_at_ix, const QFJson::ParserOptions &opts) throw(QFException)
{
	/// qfTrash loguje hesla :)
	//qfLogFuncFrame() << str.mid(from_ix);
	QVariant ret;
	parse_end_at_ix = skip_spaces(str, from_ix);
	if(parse_end_at_ix >= str.count()) {
		parse_end_at_ix = -1;
	}
	else {
		QChar c = str[parse_end_at_ix];
		if(c == '{') {
			//qfTrash() << "\t object";
			parse_end_at_ix = skip_spaces(str, parse_end_at_ix +1);
			if(parse_end_at_ix < str.count()) {
				QVariantMap vm;
				if(str[parse_end_at_ix] == '}') {
					// empty object
				}
				else {
					//bool delimiter_found = false;
					//int field_ix = 0;
					QStringList fields;
					while(true) {
						/// find key
						int ix = parse_end_at_ix;
						parse_end_at_ix = get_token(":,}{", str, parse_end_at_ix);
						if(parse_end_at_ix >= str.count()) {parse_end_at_ix = -1; return QVariant();}
						c = str[parse_end_at_ix];
						if(c != ':') QF_EXCEPTION(QString("Parse ERROR: '%1' is not valid object key delimiter\n at %2\n near %3\n in: '%4'").arg(c).arg(parse_end_at_ix).arg(str.mid(0, parse_end_at_ix)).arg(str));
						QString key = str.mid(ix, parse_end_at_ix - ix).trimmed();
						//if(key.isEmpty()) QF_EXCEPTION(QString("Empty key at %1 parsed string: '%2'").arg(parse_end_at_ix).arg(str));
						if(!key.isEmpty() && key[0] == '"') key = key.mid(1, key.count() - 2);
						//qfTrash() << "\t\t key:" << key;
						fields << key;
						// get value
						QVariant v = getObject_helper(str, parse_end_at_ix+1, parse_end_at_ix, opts);
						if(parse_end_at_ix < 0) return QVariant();
						//qfTrash() << "\t\t value:" << v.toString() << "parse_end_at_ix:" << parse_end_at_ix;
						vm[key] = v;
						parse_end_at_ix = skip_spaces(str, parse_end_at_ix);
						if(parse_end_at_ix >= str.count()) { parse_end_at_ix = -1; return QVariant();}
						//qfTrash() << "\t\t parse_end_at_ix:" << parse_end_at_ix << "rest:" << str.mid(parse_end_at_ix);
						c = str[parse_end_at_ix];
						if(c == '}') break;
						else if(c == ',') {
							parse_end_at_ix++;
							//delimiter_found = true;
						}
						else QF_EXCEPTION(QString("Parse ERROR: '%1' is not valid object field delimiter\n at %2\n near %3\n in: '%4'").arg(c).arg(parse_end_at_ix).arg(str.mid(0, parse_end_at_ix)).arg(str));
						/*
						/// find field delimiter
						const static QString terminators = ",}";
						parse_end_at_ix = unquoted_char_pos(terminators, str, parse_end_at_ix);
						if(parse_end_at_ix < 0) {return QVariant();}
						c = str[parse_end_at_ix];
						if(c == '}') break;
						parse_end_at_ix++;
						*/
					};
					if(opts.isIncludeFieldList()) vm[FIELDS_KEY] = fields;
				}
				parse_end_at_ix++;
				ret = vm;
			}
			else {
				parse_end_at_ix = -1;
			}
		}
		else if(c == '[') {
			//qfTrash() << "\t array";
			parse_end_at_ix = skip_spaces(str, parse_end_at_ix +1);
			if(parse_end_at_ix < str.count()) {
				QVariantList vl;
				if(str[parse_end_at_ix] == ']') {
					// empty list
				}
				else 	{
					while(true) {
						// get value
						QVariant v = getObject_helper(str, parse_end_at_ix, parse_end_at_ix, opts);
						vl << v;
						if(parse_end_at_ix < 0) return QVariant();
						parse_end_at_ix = skip_spaces(str, parse_end_at_ix);
						if(parse_end_at_ix >= str.count()) { parse_end_at_ix = -1; return QVariant();}
						c = str[parse_end_at_ix];
						if(c == ']') break;
						else if(c == ',') parse_end_at_ix++;
						else QF_EXCEPTION(QString("Parse ERROR: '%1' is not valid array field delimiter\n at %2\n near %3\n in: '%4'").arg(c).arg(parse_end_at_ix).arg(str.mid(0, parse_end_at_ix)).arg(str));
					};
				}
				parse_end_at_ix++;
				ret = vl;
			}
			else {
				parse_end_at_ix = -1;
			}
		}
		else if(c == '"' || c == '\'') {
			qfTrash() << "\t string";
			QChar opening_quote = c;
			//qfInfo() << "opening_quote:" << c;
			QString s;
			for(++parse_end_at_ix; parse_end_at_ix<str.count(); parse_end_at_ix++) {
				//qfInfo() << c;
				c = str[parse_end_at_ix];
				if(c == '\\') {
					parse_end_at_ix++;
					if(parse_end_at_ix >= str.count()) {
						parse_end_at_ix = -1;
						return QVariant();
					}
					else {
						c = str[parse_end_at_ix];
						switch(c.unicode()) {
							case 'b': c = '\b'; break;
							case 'f': c = '\f'; break;
							case 'n': c = '\n'; break;
							case 'r': c = '\r'; break;
							case 't': c = '\t'; break;
							case 'u': {
								// \uxxxx, znak v unicode
								parse_end_at_ix++;
								QString us = str.mid(parse_end_at_ix, 4);
								if(parse_end_at_ix + 4 > str.count()) {
									QF_EXCEPTION(QString("Parse ERROR: '\\u%1' should have 4 digits\n at %2\n near %3\n in: '%4'").arg(us).arg(parse_end_at_ix).arg(str.mid(0, parse_end_at_ix)).arg(str));									parse_end_at_ix = -1;
								}
								bool ok;
								uint u = us.toUInt(&ok, 16);
								if(!ok) {
									QF_EXCEPTION(QString("Parse ERROR: '\\u%1' is not valid unicode coder\n at %2\n near %3\n in: '%4'").arg(us).arg(parse_end_at_ix).arg(str.mid(0, parse_end_at_ix)).arg(str));									parse_end_at_ix = -1;
								}
								c = QChar(u);
								parse_end_at_ix += 3; /// 4. znak prida for cyklus
								break;
							}
							default: break;
						}
					}
					s.append(c);
				}
				else if(c == opening_quote) {
					parse_end_at_ix++;
					break;
				}
				else {
					s.append(c);
				}
			}
#ifdef QF_PATCH
			if(s.startsWith('@')) ret = QFXmlKeyVals::stringToVariant(s);
			else ret = s;
#else
			ret = s;
#endif
		}
		else {
			QString token;
			const static QString terminators = " ,[]{};";
			int ix = parse_end_at_ix;
			parse_end_at_ix = get_token(terminators, str, parse_end_at_ix);
			//if(parse_end_at_ix < 0) {return QVariant();}
			//qfTrash() << "\t ix:" << ix << "parse_end_at_ix:" << parse_end_at_ix << "rest:" << str.mid(parse_end_at_ix);
			token = str.mid(ix, parse_end_at_ix - ix);
			/*
			while(parse_end_at_ix < str.count()) {
			c = str[parse_end_at_ix];
			if(c.isSpace()) break;
			number_str.append(c);
			parse_end_at_ix++;
		}
			*/
			token = token.toLower();
			//qfTrash() << "\t token:" << token;
			if(token == "true") {
				ret = true;
			}
			else if(token == "false") {
				ret = false;
			}
			else if(token == "null") {
				ret = QVariant();
			}
			else {
				/// this can be only number
				QString number_str = token;
				//qfTrash() << "\t number:" << number_str;
				//if(number_str.isEmpty()) QF_EXCEPTION(QString("Parse ERROR: empty string \n at %1\n in: '%2'").arg(parse_end_at_ix).arg(str));
				int signum_mantisa = 1;
				int signum_exponent = 1;
				c = number_str[0];
				if(c == '+') {number_str = number_str.mid(1);}
				if(c == '-') {signum_mantisa = -1; number_str = number_str.mid(1);}
				if(number_str.isEmpty()) {parse_end_at_ix = -1; return QVariant();}
				QString mantisa = number_str, exponent;
				bool has_point = false, has_e = false;
				ix = number_str.indexOf('e');
				if(ix >= 0) {
					exponent = number_str.mid(ix + 1);
					c = exponent[0];
					if(c == '+') {exponent = exponent.mid(1);}
					if(c == '-') {signum_exponent = -1; exponent = exponent.mid(1);}
					has_e = true;
					mantisa = number_str.mid(0, ix);
				}
				ix = mantisa.indexOf('.');
				if(ix >= 0) {
					has_point = true;
				}
				//qfTrash() << "\t\t mantisa:" << mantisa << "exponent:" << exponent;
				//qfTrash() << "\t\t has_point:" << has_point << "has_e:" << has_e;
				//QF_EXCEPTION(QString("Parse ERROR: '%1' is not valid real number symbol (%2)\n at %3\n in: '%4'").arg(exponent).arg(mantisa + operation).arg(parse_end_at_ix).arg(str));
				if(mantisa.isEmpty()) { QF_EXCEPTION(QString("Parse ERROR: Mantisa of '%1' is empty\n at %2\n near %3\n in: '%4'").arg(number_str).arg(parse_end_at_ix).arg(str.mid(0, parse_end_at_ix)).arg(str)); }
				if(has_e && exponent.isEmpty()) { QF_EXCEPTION(QString("Parse ERROR: Exponent of '%1' is empty\n at %2\n near %3\n in: '%4'").arg(number_str).arg(parse_end_at_ix).arg(str.mid(0, parse_end_at_ix)).arg(str)); }
				QVariant mantisa_n = 0;
				int exponent_n = 0;
				if(!mantisa.isEmpty()) {
					bool ok;
					if(has_point) {
						mantisa_n = mantisa.toDouble(&ok) * signum_mantisa;
						if(!ok) { QF_EXCEPTION(QString("Parse ERROR: '%1' is not valid double\n at %2\n near %3\n in: '%4'").arg(mantisa).arg(parse_end_at_ix).arg(str.mid(0, parse_end_at_ix)).arg(str)); }
					}
					else 	{
						mantisa_n = mantisa.toInt(&ok) * signum_mantisa;
						if(!ok) { QF_EXCEPTION(QString("Parse ERROR: '%1' is not valid integer\n at %2\n near %3\n in: '%4'").arg(mantisa).arg(parse_end_at_ix).arg(str.mid(0, parse_end_at_ix)).arg(str)); }
					}
				}
				if(!exponent.isEmpty()) {
					bool ok;
					exponent_n = exponent.toInt(&ok) * signum_exponent;
					if(!ok) { QF_EXCEPTION(QString("Parse ERROR: '%1' is not valid integer\n near %2\n near %3\n in: '%4'").arg(exponent).arg(parse_end_at_ix).arg(str.mid(0, parse_end_at_ix)).arg(str)); }
				}
				if(has_e) {
					double d = mantisa_n.toDouble() * std::pow(10.0, signum_exponent * exponent_n);
					ret = d;
				}
				else {
					ret = mantisa_n;
				}
			}
		}
	}
	//if(parse_end_at_ix < 0) ret = QVariant();
	return ret;
}

QVariant QFJson::stringToVariant(const QString & str, int * p_consumed_char_cnt, QString *p_errmsg, const QFJson::ParserOptions &opts)
{
	qfLogFuncFrame();
	QVariant ret;
	QString errmsg;
	int n;
	try {
		ret = getObject_helper(str, 0, n, opts);
	}
	catch(QFException &e) {
		errmsg = e.what();
	}
	if(p_consumed_char_cnt) *p_consumed_char_cnt = n;
	if(p_errmsg) *p_errmsg = errmsg;
	return ret;
}

QString QFJson::variantToString(const QVariant & v, QFJson::KeyQuotingPolicy key_quoting_policy)
{
	qfLogFuncFrame() << v.toString() << "\t type:" << v.typeName();
	QString ret;
	QTextStream stream(&ret);
	switch(v.type()) {
		case QVariant::Map: {
			stream << '{';
			QVariantMap m = v.toMap();
			QMapIterator<QString, QVariant> i(m);
			int cnt = 0;
			while(i.hasNext()) {
				if(cnt++) stream << ',';
				i.next();
				QString k = i.key();
				if(key_quoting_policy == QuotedKeysAlways) k = '"' + k + '"';
				else if(key_quoting_policy == QuotedKeysIfNecessary) {
					if(k.indexOf(':') >= 0) k = '"' + k + '"';
				}
				QVariant v1 = i.value();
				stream << k << ':';
				stream << variantToString(v1, key_quoting_policy);
			}
			stream << '}';
			break;
		}
		case QVariant::List:
		case QVariant::StringList: {
			stream << '[';
			QVariantList l = v.toList();
			int cnt = 0;
			foreach(const QVariant &v1, l) {
				if(cnt++) stream << ',';
				stream << variantToString(v1, key_quoting_policy);
			}
			stream << ']';
			break;
		}
#ifdef QF_PATCH
		case QVariant::Date:
		case QVariant::Time:
		case QVariant::DateTime: {
			QString s = QFXmlKeyVals::variantToString(v);
			stream << '"';
			stream << s;
			stream << '"';
			break;
		}
#endif
		case QVariant::String: {
			stream << '"';
			QString s = v.toString();
			putStringToStreamEscaped(stream, s);
			stream << '"';
			break;
		}
		case QVariant::Invalid: {
			stream << "null";
			break;
		}
		default: {
			stream << v.toString();
			break;
		}
	}
	return ret;
}

void QFJson::putStringToStreamEscaped(QTextStream &stream, const QString &s)
{
	for(int i=0; i<s.count(); i++) {
		QChar c = s[i];
		switch(c.unicode()) {
			case '\\': stream << '\\' << '\\'; break;
			case '"': stream << '\\' << '"'; break;
			//case '/': stream << '\\' << '/'; break;
			case '\b': stream << '\\' << 'b'; break;
			case '\f': stream << '\\' << 'f'; break;
			case '\n': stream << '\\' << 'n'; break;
			case '\r': stream << '\\' << 'r'; break;
			case '\t': stream << '\\' << 't'; break;
			default: stream << c; break;
		}
	}
}
/*
QString QFJson::escapeJsonString(const QString & s)
{
	QString ret;
	{
		QTextStream stream(&ret);
		putStringToStreamEscaped(stream, s);
	}
	return ret;
}
*/


