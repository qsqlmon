
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfprototypedobject.h"

#include <qf.h>
#include <qfjson.h>

#include <qflogcust.h>

//=================================================
//             QFPrototypedObject::Data
//=================================================

QFPrototypedObject::Data::Data()
{
	prototype = NULL;
	//qfDebug() << "Data():" << this << "prototype:" << prototype << "prototype data:" << (prototype? prototype->d: NULL);
}

QFPrototypedObject::Data::Data(const QFPrototypedObject &proto)
{
	//qfDebug() << "Data(const QFPrototypedProperties &proto): creating data from prototype:" << this << "other prototype:" << &proto << "other data:" << proto.d.data();
	QFPrototypedObject *o = new QFPrototypedObject(proto);
	prototype = o;
	//if(prototype) qfDebug() << "--Data(const QFPrototypedProperties &proto):" << this << "prototype:" << prototype << "prototype data:" << prototype->d.data();
	//else qfDebug() << "--Data(const QFPrototypedProperties &proto):" << this << "prototype:" << prototype << "prototype data:" << 0;
}

QFPrototypedObject::Data::Data(const Data &other)
	: QSharedData(other)
{
	//qfDebug() << "Data(const Data &other): creating data from other data:" << this << "other data:" << &other;
	qfWarning() << "Nikdy jsem nezamyslel klonovat QFPrototypedObject.";
	meta = other.meta;
	value = other.value;
	//qDeleteAll(children);
	children.clear();
	foreach(KeyVal kv, other.children) {
		//QFPrototypedObject *o = new QFPrototypedObject(*oo);
		children << kv;
	}
	QF_SAFE_DELETE(prototype);
	{
		QFPrototypedObject *o = new QFPrototypedObject(*other.prototype);
		prototype = o;
	}
	//if(prototype) qfDebug() << "--Data(const Data &other):" << this << "prototype:" << prototype << "prototype data:" << prototype->d.data();
	//else qfDebug() << "--Data(const Data &other):" << this << "prototype:" << prototype << "prototype data:" << 0;
}

QFPrototypedObject::Data::~Data()
{
	//qfDebug() << "~Data():" << this << "prototype:" << prototype << "prototype data:" << (prototype? prototype->d: NULL);
	//qDeleteAll(children);
	QF_SAFE_DELETE(prototype);
}

//=================================================
//             QFPrototypedObject
//=================================================
const QString QFPrototypedObject::JSON_META_KEY = QString("meta");
const QString QFPrototypedObject::JSON_VALUE_KEY = QString("value");
//const QString QFPrototypedObject::KEYS_KEY = QString("keys");
//const QString QFPrototypedObject::CHILDREN_KEY = QString("children");
const QString QFPrototypedObject::TYPE_KEY = QString("type");
const QString QFPrototypedObject::DELEGATE_KEY = QString("delegate");
const QString QFPrototypedObject::CAPTION_KEY = QString("caption");
const QString QFPrototypedObject::UNIT_KEY = QString("unit");
const QString QFPrototypedObject::HELP_KEY = QString("help");
const QString QFPrototypedObject::OPTIONS_KEY = QString("options");

QFPrototypedObject::QFPrototypedObject()
{
	d = new Data();
}

QFPrototypedObject::QFPrototypedObject(int , const QFPrototypedObject &prototype)
{
	d = new Data(prototype);
}

QFPrototypedObject::~QFPrototypedObject()
{
}

QFPrototypedObject QFPrototypedObject::prototype() const
{
	if(d->prototype) return *(d->prototype);
	return QFPrototypedObject();
}

bool QFPrototypedObject::isNull() const
{
	return !d->value.isValid() && d->meta.isEmpty() && d->prototype==NULL && d->children.isEmpty();
}

void QFPrototypedObject::setPrototype(const QFPrototypedObject &proto)
{
	QF_SAFE_DELETE(d->prototype);
	QFPrototypedObject *o = new QFPrototypedObject(proto);
	d->prototype = o;
}

QStringList QFPrototypedObject::keys(int flags) const
{
	QStringList ret;
	const QFPrototypedObject *po = this;
	while(po) {
		/// vem to pozpatku, aby mely prednost klice z 1. prototypu
		for(int i=po->d->children.count()-1; i>=0; i--) {
			KeyVal kv = po->d->children.value(i);
			ret.removeOne(kv.key); /// pokud je key v prototypu, ma prednost
			ret.prepend(kv.key);
		}
		if(!(flags & ResolvePrototypes)) break;
		po = po->d->prototype;
	}
	return ret;
}

void QFPrototypedObject::keysOnPath_helper(const QStringList &path, int path_ix, QStringList &ret) const
{
	qfLogFuncFrame() << "path:" << path.join("/") << "path_ix:" << path_ix << "curr segment:" << path.value(path_ix);
	if(path_ix == path.count()) {
		QStringList sl = keys();
		if(!sl.isEmpty()) {
			foreach(QString s, sl) {
				ret.removeOne(s); /// pokud je klic 2x ma prednost puvodni posice z drivejsiho prototypu, aby mi nedochazelo ke zmene poradi klicu pri dedeni
			}
			ret = sl + ret;
		}
	}
	else {
		QString curr_path_key = path.value(path_ix);
		const QFPrototypedObject *po = this;
		while(po) {
			qfTrash() << "\t po:" << po << "key:" << po->key();
			QVariant v = po->object(curr_path_key);
			qfTrash() << "\t curr_path_key:" << curr_path_key << "type:" << QMetaType::typeName(v.userType());
			if(v.userType() == qMetaTypeId<QFPrototypedObject>()) {
				QFPrototypedObject o = qvariant_cast<QFPrototypedObject>(v);
				o.keysOnPath_helper(path, path_ix+1, ret);
			}
			po = po->d->prototype;
		}
	}
}

QStringList QFPrototypedObject::keysOnPath(const QStringList &path) const
{
	qfLogFuncFrame() << path.join("/");
	QStringList ret;
	keysOnPath_helper(path, 0, ret);
	qfTrash() << "\t return:" << ret.join(",");
	return ret;
}
/*
QFPrototypedObject* QFPrototypedObject::child_helper(const QStringList &path, int *p_status, bool meta) const
{
	QFPrototypedObject *ret = NULL;
	int status = ValueNotFound;
	const QFPrototypedObject *po = this;
	while(po) {
		const QFPrototypedObject *o = po;
		QStringList sl;
		for(int i=0; i<path.count(); i++) {
			QString k = path.value(i);
			o = child(k, &status);
			if(!o) break;
			if(i == path.count() - 1) {
				if(meta) {
					if(!o->type().isEmpty()) { ret = (QFPrototypedObject*)o; }
					else status = ValueNotFound;
				}
				else {
					ret = (QFPrototypedObject*)o;
				}
			}
		}
		if(status > ValueNotFound) break;
		po = po->d->prototype;
	}
	if(p_status) *p_status = status;
	return ret;
}
*/
/*
QVariant QFPrototypedObject::value(const QString &key, int *p_status) const
{
	QVariant ret;
	int status = ValueNotFound;
	const QFPrototypedObject *po = this;
	while(po) {
		foreach(KeyVal kv, d->children) {
			if(kv.key == key) {
				ret = kv.value;
				if(po == this) status = ValueOwn;
				else status = ValuePrototype;
				break;
			}
		}
		if(status > ValueNotFound) break;
		po = po->d->prototype;
	}
	if(p_status) *p_status = status;
	return ret;
}
*/
QVariant QFPrototypedObject::metaValue(const QString &key) const
{
	QVariant ret;
	const QFPrototypedObject *po = this;
	while(po) {
		ret = po->d->meta.value(key);
		//qfInfo() << po << "data:" << po->dataHex() << key << "ret:" << ret.toString() << "meta:" << QFJson::variantToString(d->meta);
		if(ret.isValid()) break;
		po = po->d->prototype;
	}
	return ret;
}

QVariant QFPrototypedObject::object_value_helper(int flags, int *p_status) const
{
	QVariant ret;
	int status = ValueNotFound;
	const QFPrototypedObject *po = this;
	while(po) {
		ret = d->value;
		if(ret.isValid()) {
			status = ValueOwn;
			if(po != this) status = ValuePrototype;
			break;
		}
		if(!(flags & ResolvePrototypes)) break;
		po = po->d->prototype;
	}
	if(p_status) *p_status = status;
	return ret;
}

QVariant QFPrototypedObject::objectValue(const QVariant &default_val, int *p_status) const
{
	int status = ValueNotFound;
	QVariant ret = object_value_helper(ResolvePrototypes, &status);
	if(status == ValueNotFound) ret = default_val;
	if(p_status) *p_status = status;
	return ret;
}

QVariant QFPrototypedObject::ownObjectValue(const QVariant &default_val, int *p_status) const
{
	int status = ValueNotFound;
	QVariant ret = object_value_helper(NotResolvePrototypes, &status);
	if(status == ValueNotFound) ret = default_val;
	if(p_status) *p_status = status;
	return ret;
}

QVariant QFPrototypedObject::value_helper(const QString &_key, const QVariant &default_val, int flags, int *p_status) const
{
	QVariant ret;
	int status = ValueNotFound;
	const QFPrototypedObject *po = this;
	while(po) {
		//if(_key == "soudruh") qfInfo() << po << "for key:" << _key;
		foreach(KeyVal kv, po->d->children) {
			//if(_key == "soudruh") qfInfo() << "\t trying:" << _key;
			if(kv.key == _key) {
				//qfInfo() << "\t\t YES";
				ret = kv.value;
				if(flags & ResolveObjectValue) {
					if(ret.userType() == qMetaTypeId<QFPrototypedObject>()) {
						QFPrototypedObject o = qvariant_cast<QFPrototypedObject>(ret);
						ret = o.objectValue(QVariant(), &status);
					}
				}
				if(po == this) status |= ValueOwn;
				else {
					//if(_key == "soudruh") qfInfo() << "\t\t PROTOTYPE";
					status |= ValuePrototype;
				}
				break;
			}
		}
		if(ret.isValid()) break;
		if(!(flags & ResolvePrototypes)) break;
		status = ValueNotFound;
		po = po->d->prototype;
	}
	if(status == ValueNotFound) ret = default_val;
	if(p_status) *p_status = status;
	return ret;
}

QVariant QFPrototypedObject::metaValueOnPath_helper(const QStringList &path, int path_ix, const QString &key) const
{
	qfLogFuncFrame() << "path:" << path.join("/") << "path_ix:" << path_ix << "curr segment:" << path.value(path_ix) << "key:" << key;
	QVariant ret;
	const QFPrototypedObject *po = this;
	while(po) {
		qfTrash() << "\t po:" << po << "key:" << po->key();
		//QFPrototypedObject o = *po;
		if(path_ix == path.count()) {
			ret = po->metaValue(key);
			qfTrash() << "\t metavalue" << key << "for po is:" << ret.toString() << "type:" << ret.typeName();
		}
		else {
			QString curr_path_key = path.value(path_ix);
			QVariant v = po->object(curr_path_key);
			qfTrash() << "\t curr_path_key:" << curr_path_key << "type:" << QMetaType::typeName(v.userType());
			if(v.userType() == qMetaTypeId<QFPrototypedObject>()) {
				QFPrototypedObject o = qvariant_cast<QFPrototypedObject>(v);
				ret = o.metaValueOnPath_helper(path, path_ix+1, key);
			}
		}
		if(ret.isValid()) {
			qfTrash() << "\t Found value:" << ret.toString() << ret.typeName();
			break;
		}
		po = po->d->prototype;
	}
	return ret;
}

QVariant QFPrototypedObject::metaValueOnPath(const QStringList &path, const QString &key) const
{
	qfLogFuncFrame() << "path:" << path.join("/") << "key:" << key;
	QVariant ret = metaValueOnPath_helper(path, 0, key);
	return ret;
}

QVariant QFPrototypedObject::valueOnPath_helper(const QStringList &path, int path_ix, int &status, bool is_value_value) const
{
	qfLogFuncFrame();
	//qfInfo() << "valueOnPath_helper path:" << path.join("/") << "path_ix:" << path_ix << "curr segment:" << path.value(path_ix);
	QVariant ret;
	if(path.isEmpty()) {
		ret = objectValue(QVariant(), &status);
	}
	else {
		QString curr_path_key = path.value(path_ix);
		const QFPrototypedObject *po = this;
		while(po) {
			qfTrash() << "\t po:" << po << "key:" << po->key();
			//QFPrototypedObject o = *po;
			if(path_ix == path.count()-1) {
				QVariant v = po->object(curr_path_key);
				qfTrash() << "\t curr_path_key:" << curr_path_key << "type:" << QMetaType::typeName(v.userType());
				if(is_value_value) {
					if(v.userType() == qMetaTypeId<QFPrototypedObject>()) {
						QFPrototypedObject o = qvariant_cast<QFPrototypedObject>(v);
						int status2;
						ret = o.objectValue(QVariant(), &status2);
						status |= status2;
					}
					else ret = v;
				}
				else ret = v;
			}
			else {
				QVariant v = po->ownObject(curr_path_key);
				qfTrash() << "\t curr_path_key:" << curr_path_key << "type:" << QMetaType::typeName(v.userType());
				if(v.userType() == qMetaTypeId<QFPrototypedObject>()) {
					QFPrototypedObject o = qvariant_cast<QFPrototypedObject>(v);
					ret = o.valueOnPath_helper(path, path_ix+1, status, is_value_value);
				}
			}
			if(ret.isValid()) {
				//qfInfo() << "\t Found value:" << ret.toString() << ret.typeName();
				if(po != this) status |= ValuePrototype;
				else status |= ValueOwn;
				break;
			}
			po = po->d->prototype;
		}
	}
	return ret;
	#if 0
	qfLogFuncFrame() << "path:" << path.join("/");
	QVariant ret;
	int status = ValueNotFound;
	const QFPrototypedObject *po = this;
	bool prototyped = false;
	while(po) {
		//qfError() << "\t po:" << po << "path:" << path.join("/");
		QVariant val;
		val.setValue(*po);
		bool found = true;
		for(int i=0; i<path.count(); i++) {
			if(val.userType() != qMetaTypeId<QFPrototypedObject>()) {
				found = false;
				break;
			}
			QFPrototypedObject o = qvariant_cast<QFPrototypedObject>(val);
			QString k = path.value(i);
			//qfError() << "\t\t" << k;
			//if(k == "soudruh") { qfInfo() << "kde je soudruh"; }
			val = o.object(k, QVariant(), &status);
			if(status & ValuePrototype) prototyped = true;
			/*
			if(k == "soudruh") {
				qfInfo() << this << po << status << "val:" << QMetaType::typeName(val.userType());
				QFPrototypedObject o = qvariant_cast<QFPrototypedObject>(val);
				qfInfo() << o.dump();
			}
			*/
		}
		if(found) {
			if(is_value_value && val.userType() == qMetaTypeId<QFPrototypedObject>()) {
				QFPrototypedObject o = qvariant_cast<QFPrototypedObject>(val);
				int status2;
				val = o.objectValue(QVariant(), &status2);
				status |= status2;
			}
			//if(path.join("/") == "kovani/druh/soudruh") { qfInfo() << "val:" << val.toString(); }
			if(val.isValid()) {
				ret = val;
				if(prototyped) status |= ValuePrototype;
				break;
			}
		}
		status = ValueNotFound;
		prototyped = true;
		po = po->d->prototype;
	}
	if(p_status) *p_status = status;
	return ret;
	#endif
}

QVariant QFPrototypedObject::objectOnPath(const QStringList &path, const QVariant &default_val, int *p_status) const
{
	int status = ValueNotFound;
	QVariant ret = valueOnPath_helper(path, 0, status, false);
	if(status == ValueNotFound) ret = default_val;
	if(p_status) *p_status = status;
	return ret;
}

QVariant QFPrototypedObject::valueOnPath(const QStringList &path, const QVariant &default_val, int *p_status) const
{
	qfLogFuncFrame();
	//qfInfo() << "valueOnPath path:" << path.join("/");
	int status = ValueNotFound;
	QVariant ret = valueOnPath_helper(path, 0, status, true);
	//qfInfo() << "\t return:" << ret.toString() << ret.typeName() << "not found:" << (status == ValueNotFound);
	if(status == ValueNotFound) ret = default_val;
	if(p_status) *p_status = status;
	return ret;
}

void QFPrototypedObject::setValueOnPath_helper(const QStringList &path, const QVariant &val, bool is_value_value)
{
	//qfInfo() << path.join("/") << "=" << val.toString() << "is_value_value:" << is_value_value;
	QFPrototypedObject o = *this;
	if(path.isEmpty()) {
		o.setObjectValue(val);
	}
	else {
		for(int i=0; i<path.count(); i++) {
			QString k = path.value(i);
			if(i == path.count()-1) {
				if(is_value_value) {
					/*
					QVariant v = o.ownValue(k);
					if(v.userType() == qMetaTypeId<QFPrototypedObject>()) {
						QFPrototypedObject o2 = qvariant_cast<QFPrototypedObject>(v);
						o2.setObjectValue(val);
						v.setValue(o2);
					}
					*/
					o.setValue(k, val);
				}
				else o.setObject(k, val);
			}
			else {
				QVariant v = o.ownObject(k);
				if(v.userType() != qMetaTypeId<QFPrototypedObject>()) {
					//qfInfo() << "creating object for key" << k;
					QFPrototypedObject o2;
					o2.setObjectValue(v);
					v.setValue(o2);
					KeyVal kv(k, v);
					o.d->children << kv;
					o = o2;
				}
				else {
					o = qvariant_cast<QFPrototypedObject>(v);
				}
			}
		}
	}
}

void QFPrototypedObject::setObject(const QString &_key, const QVariant &val)
{
	bool found = false;
	for(int i=0; i<d->children.count(); i++) {
		if(d->children[i].key == _key) {
			d->children[i].value = val;
			found = true;
			break;
		}
	}
	if(!found) {
		KeyVal kv(_key, val);
		d->children << kv;
	}
	//qfError() << "setObject()" << dump();
}

void QFPrototypedObject::setObject(const QString &key, const QFPrototypedObject &val)
{
	//qfInfo() << QF_FUNC_NAME << val.dump();
	QVariant v;
	v.setValue(val);
	setObject(key, v);
}

void QFPrototypedObject::setValue(const QString &_key, const QVariant &val)
{
	bool found = false;
	for(int i=0; i<d->children.count(); i++) {
		if(d->children[i].key == _key) {
			QVariant v = d->children[i].value;
			if(v.userType() == qMetaTypeId<QFPrototypedObject>()) {
				QFPrototypedObject o = qvariant_cast<QFPrototypedObject>(v);
				o.setObjectValue(val);
				//v.setValue(o);
			}
			else d->children[i].value = val;
			found = true;
			break;
		}
	}
	if(!found) {
		KeyVal kv(_key, val);
		d->children << kv;
	}
}

QVariant QFPrototypedObject::ownValuesToVariant() const
{
	QVariant ret = ownObjectValue();
	QVariantMap m;
	foreach(KeyVal kv, d->children) {
		QVariant v = kv.value;
		if(v.userType() == qMetaTypeId<QFPrototypedObject>()) v = qvariant_cast<QFPrototypedObject>(v).ownValuesToVariant();
		if(v.isValid()) m[kv.key] = v;
	}
	if(!m.isEmpty()) {
		if(ret.isValid()) m[JSON_VALUE_KEY] = ret;
		ret = m;
	}
	return ret;
}

static QVariant valuesToVariant_helper(const QFPrototypedObject *po, const QStringList &path)
{
	qfLogFuncFrame() << "path:" << path.join("/");
	QVariant ret = po->valueOnPath(path);

	QStringList kl = po->keysOnPath(path);
	//qfInfo() << kl.join(",");
	QVariantMap m;
	foreach(QString key, kl) {
		//qfTrash() << "\t dumping child:" << key << QFJson::variantToString(p2);
		QStringList path2 = path;
		path2 << key;
		QVariant v = valuesToVariant_helper(po, path2);
		m[key] = v;
	}
	if(!m.isEmpty()) {
		if(ret.isValid()) {
			m[QFPrototypedObject::JSON_VALUE_KEY] = ret;
		}
		ret = m;
	}
	return ret;
}

QVariant QFPrototypedObject::valuesToVariant() const
{
	QVariant ret = valuesToVariant_helper(this, QStringList());
	return ret;
}

void QFPrototypedObject::clearOwnValues()
{
	setObjectValue(QVariant());
	d->children = QList<KeyVal>();
	/*
	for(int i=0; i<d->children.count(); i++) {
		QVariant v = d->children[i].value;
		if(v.userType() == qMetaTypeId<QFPrototypedObject>()) {
			QFPrototypedObject o = qvariant_cast<QFPrototypedObject>(v);
			o.clearOwnValues();
			v.setValue(o);
		}
		else v = QVariant();
		d->children[i].value = v;
		//m[kv.key] = v;
	}
	*/
}

void QFPrototypedObject::loadOwnValuesFromVariant_helper(QFPrototypedObject *po, const QStringList &path, const QVariant &json)
{
	if(json.type() == QVariant::Map) {
		QVariantMap m = json.toMap();
		QMapIterator<QString, QVariant> it(m);
		while(it.hasNext()) {
			it.next();
			QString key = it.key();
			QVariant val = it.value();
			if(key == JSON_VALUE_KEY) {
				po->setValueOnPath(path, val);
			}
			else {
				QStringList path2 = path;
				path2 << key;
				loadOwnValuesFromVariant_helper(po, path2, val);
			}
		}
	}
	else po->setValueOnPath(path, json);
}

void QFPrototypedObject::loadOwnValuesFromJson(const QString &json) throw(QFException)
{
	QString errmsg;
	QVariant v = QFJson::stringToVariant(json, NULL, &errmsg);
	if(!errmsg.isEmpty()) QF_EXCEPTION(QString("QFPrototypedObject::loadOwnValuesFromVariant ERROR\n%1").arg(errmsg));
	loadOwnValuesFromVariant_helper(this, QStringList(), v);
}

void QFPrototypedObject::loadOwnValuesFromVariant(const QVariant &json)
{
	loadOwnValuesFromVariant_helper(this, QStringList(), json);
}

static QString dump_helper(const QFPrototypedObject *po, const QStringList &path)
{
	qfLogFuncFrame() << "path:" << path.join("/");
	QString ret;
	QString indentstr;
	//indentstr.fill(' ', 2*(path.count()-1));
	//ret += indentstr + '[' + path.value(path.count()-1) + ']';
	indentstr.fill(' ', 2*path.count());

	QString p_caption = po->metaValueOnPath(path, QFPrototypedObject::CAPTION_KEY).toString();
	if(p_caption.count()) ret += '\n' + indentstr + "caption: " + p_caption;
	qfTrash() << "\t caption:" << p_caption;
	QString p_type = po->metaValueOnPath(path, QFPrototypedObject::TYPE_KEY).toString();
	if(!p_type.isEmpty()) ret += '\n' + indentstr + "type: " + p_type;
	int status;
	QVariant p_value = po->valueOnPath(path, QVariant(), &status);
	if(status != QFPrototypedObject::ValueNotFound) {
		ret += '\n' + indentstr + "value";
		if(status & QFPrototypedObject::ValueOwn) ret += " OWN";
		if(status & QFPrototypedObject::ValuePrototype) ret += " PROTO";
		ret += ": ";
		ret += QFJson::variantToString(p_value);
		qfTrash() << "\t value:" << QFJson::variantToString(p_value);
	}
	ret += '\n';
	QString p_unit = po->metaValueOnPath(path, QFPrototypedObject::UNIT_KEY).toString();
	if(!p_unit.isEmpty()) {
		ret += indentstr;
		ret += "unit:" + p_unit;
		ret += '\n';
	}
	QVariantMap p_options = po->metaValueOnPath(path, QFPrototypedObject::OPTIONS_KEY).toMap();
	if(!p_options.isEmpty()) {
		ret += indentstr;
		ret += "options:" + QFJson::variantToString(p_options);
		ret += '\n';
	}
	QString p_help = po->metaValueOnPath(path, QFPrototypedObject::HELP_KEY).toString();
	if(!p_help.isEmpty()) {
		ret += indentstr;
		ret += "help:" + p_help;
		ret += '\n';
	}
	
	QStringList kl = po->keysOnPath(path);
	//qfInfo() << kl.join(",");
	if(kl.count()) {
		//ret += indentstr + "children:\n";
		foreach(QString key, kl) {
			//qfTrash() << "\t dumping child:" << key << QFJson::variantToString(p2);
			QStringList path2 = path;
			path2 << key;
			//if(!path.isEmpty()) path2 = path + '/' + key;
			ret += indentstr + '[' + key + ']' + dump_helper(po, path2);
		}
	}
	
	return ret;
}

QString QFPrototypedObject::dataHex() const
{
	return QString::number((long)d.data(), 16);
}

QString QFPrototypedObject::dump() const
{
	qfLogFuncFrame();
	QString ret = "data: 0x" + dataHex();
	ret += dump_helper(this, QStringList());
	return ret;
}

void QFPrototypedObject::setContentJson(const QString &json) throw(QFException)
{
	QFJson::ParserOptions opts;
	opts.setIncludeFieldList(true);
	QString errmsg;
	QVariant v = QFJson::stringToVariant(json, NULL, &errmsg, opts);
	if(!errmsg.isEmpty()) QF_EXCEPTION(QString("QFPrototypedObject::setContent ERROR\n%1").arg(errmsg));
	setContentJson(v);
}

void QFPrototypedObject::setContentJson(const QVariant &json)
{
	qfLogFuncFrame() << QFJson::variantToString(json);
	QVariantMap m = json.toMap();
	if(m.isEmpty()) setObjectValue(json);
	else {
		QStringList fld_lst = m.value(QFJson::FIELDS_KEY).toStringList();
		if(fld_lst.isEmpty()) fld_lst = m.keys();
		foreach(QString key, fld_lst) {
			if(key == QFJson::FIELDS_KEY) {
				continue;
			}
			QVariant val = m.value(key);
			if(key == JSON_VALUE_KEY) {
				qfTrash() << "\t setting value:" << QFJson::variantToString(val);
				setObjectValue(val);
			}
			else if(key == JSON_META_KEY) {
				qfTrash() << "\t adding meta:" << QFJson::variantToString(val);
				QVariantMap m2 = val.toMap();
				//m2.remove(QFJson::FIELDS_KEY);
				d->meta = m2;
			}
			else {
				qfTrash() << "\t adding child:" << key;
				QFPrototypedObject o;
				o.setContentJson(val);
				QVariant v;
				v.setValue(o);
				KeyVal kv(key, v);
				d->children << kv;
			}
		}
	}
}

