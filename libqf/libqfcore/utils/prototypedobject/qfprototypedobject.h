
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFPROTOTYPEDOBJECT_H
#define QFPROTOTYPEDOBJECT_H

#include<qfcoreglobal.h>
#include<qfexception.h>

//#include<QStringList>
//#include<QDomDocument>
//#include<QDomElement>
#include <QSharedData>
#include <QVariant>

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFPrototypedObject
{
	public:
		enum ResolveFlags {NotResolvePrototypes=0, ResolvePrototypes=1, ResolveObjectValue=2};
		enum ValueFlags {ValueNotFound=0, /*ValueMeta=1,*/ ValueOwn=2, ValuePrototype=4};
		//enum GetValueFlags {PropertyNotFound=0, PropertyValuePrototype=1, PropertyValueOwn=2, PropertyValueMeta=4};

		static const QString JSON_META_KEY;
		static const QString JSON_VALUE_KEY;
		//static const QString CHILDREN_KEY;
		//static const QString KEY_KEY;
		static const QString TYPE_KEY;
		static const QString DELEGATE_KEY;
		static const QString CAPTION_KEY;
		static const QString UNIT_KEY;
		static const QString HELP_KEY;
		static const QString OPTIONS_KEY;
	private:
		struct QFCORE_DECL_EXPORT KeyVal
		{
			QString key;
			QVariant value;

			KeyVal(const QString &k = QString(), const QVariant &v = QVariant()) : key(k), value(v) {}
		};
		class QFCORE_DECL_EXPORT Data : public QSharedData
		{
			public:
				QVariantMap meta;
				QString key;
				QVariant value;
				QList<KeyVal> children;
				QFPrototypedObject *prototype;
			private:
				/// je private v QSharedData
				Data& operator=(const Data &) {return *this;}
			public:
				Data();
				Data(const QFPrototypedObject &proto);
				Data(const Data &other);
				~Data();
		};
		QExplicitlySharedDataPointer<Data> d;
	protected:
		QVariant value_helper(const QString &_key, const QVariant &default_val, int flags, int *status) const;
		QVariant object_value_helper(int flags, int *status) const;
		QVariant metaValueOnPath_helper(const QStringList &path, int path_ix, const QString &key) const;
		void keysOnPath_helper(const QStringList &path, int path_ix, QStringList &ret) const;
		//QVariant child_helper(const QStringList &path, int *p_status, bool meta) const;
		//void setContent_helper(QFPrototypedObject *po, const QVariant &json);
		QVariant valueOnPath_helper(const QStringList &path, int path_ix, int &status, bool is_value_value) const;
		void setValueOnPath_helper(const QStringList &path, const QVariant &val, bool is_value_value);
		void loadOwnValuesFromVariant_helper(QFPrototypedObject *po, const QStringList &path, const QVariant &json);
	public:
		void setPrototype(const QFPrototypedObject &proto);
		QFPrototypedObject prototype() const;

		bool isNull() const;
		
		QString key() const {return d->key;}
		void setKey(const QString &k) {d->key = k;}
		QString type() const {return d->meta.value(TYPE_KEY).toString();}
		void setType(const QString &t) {d->meta[TYPE_KEY] = t;}
		QString caption() const {return d->meta.value(CAPTION_KEY).toString();}
		void setCaption(const QString &v) {d->meta[CAPTION_KEY] = v;}
		QString help() const {return d->meta.value(HELP_KEY).toString();}
		void setHelp(const QString &v) {d->meta[HELP_KEY] = v;}
		QVariant options() const {return d->meta.value(OPTIONS_KEY);}
		void setOptions(const QVariant &v) {d->meta[OPTIONS_KEY] = v;}
		
		QStringList keys(int flags = ResolvePrototypes) const;
		QVariant objectValue(const QVariant &default_val = QVariant(), int *status = NULL) const;
		//QVariant value(const QString &key, int *p_status = NULL) const;
		QVariant metaValue(const QString &key) const;
		QVariant ownObjectValue(const QVariant &default_val = QVariant(), int *status = NULL) const;
		
		QVariant ownValue(const QString &_key, const QVariant &default_val = QVariant(), int *status = NULL) const {
			return value_helper(_key, default_val, NotResolvePrototypes | ResolveObjectValue, status);
		}
		QVariant value(const QString &_key, const QVariant &default_val = QVariant(), int *status = NULL) const {
			return value_helper(_key, default_val, ResolvePrototypes | ResolveObjectValue, status);
		}

		QVariant ownObject(const QString &_key, const QVariant &default_val = QVariant(), int *status = NULL) const {
			return value_helper(_key, default_val, NotResolvePrototypes, status);
		}
		QVariant object(const QString &_key, const QVariant &default_val = QVariant(), int *status = NULL) const {
			return value_helper(_key, default_val, ResolvePrototypes, status);
		}

		void setObjectValue(const QVariant &val) {d->value = val;}

		void setObject(const QString &_key, const QVariant &val);
		void setObject(const QString &key, const QFPrototypedObject &val);
		void setValue(const QString &key, const QVariant &val);

		QStringList keysOnPath(const QStringList &path) const;
		//QFPrototypedObject* metaChild(const QStringList &path, int *p_status) const {return child_helper(path, p_status, true);}
		//QVariant child(const QStringList &path, int *p_status) const {return child_helper(path, p_status, false);}
		/// vrati prvni nenulovou hodnotu nebo objekt nebo default_val
		QVariant objectOnPath(const QStringList &path, const QVariant &default_val = QVariant(), int *status = NULL) const;
		/// vrati prvni nenulovou hodnotu nebo hodnotu objektu, pokud je hodnota objekt nebo default_val
		QVariant valueOnPath(const QStringList &path, const QVariant &default_val = QVariant(), int *status = NULL) const;
		QVariant valueOnPath(const QString &path, const QVariant &default_val = QVariant(), int *status = NULL) const {
			return valueOnPath(QFString(path).splitAndTrim('/'), default_val, status);
		}
		void setObjectOnPath(const QStringList &path, const QVariant &val) {setValueOnPath_helper(path, val, false);}
		void setValueOnPath(const QStringList &path, const QVariant &val) {setValueOnPath_helper(path, val, true);}
		QVariant metaValueOnPath(const QStringList &path, const QString &key) const;

		/// vymaze vsechny deti, necha si jen prototyp
		void clearOwnValues();
		void loadOwnValuesFromJson(const QString &json) throw(QFException);
		void loadOwnValuesFromVariant(const QVariant &json);
		
		QVariant ownValuesToVariant() const;
		QVariant valuesToVariant() const;

		void setContentJson(const QString &json) throw(QFException);
		void setContentJson(const QVariant &json);
		
		QString dump() const;
		QString dataHex() const;
	public:
		QFPrototypedObject();
		QFPrototypedObject(int , const QFPrototypedObject &prototype);
		virtual ~QFPrototypedObject();
};
Q_DECLARE_METATYPE(QFPrototypedObject);

#endif // QFPROTOTYPEDOBJECT_H

