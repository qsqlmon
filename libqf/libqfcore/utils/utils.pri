message(including module $$PWD)

include($$PWD/fileutils/fileutils.pri)
include($$PWD/crypto/crypto.pri)
include($$PWD/getopt/getopt.pri)
include($$PWD/codecs/codecs.pri)
include($$PWD/htmlutils/htmlutils.pri)

include($$PWD/containers/containers.pri)

include($$PWD/json/json.pri)

include($$PWD/system/system.pri)
#include($$PWD/prototypedproperties/prototypedproperties.pri)
include($$PWD/prototypedobject/prototypedobject.pri)

