
#include "qfqencoding.h"

#include <QString>

#include <qflogcust.h>

/**
Difference between Q-encoding and quoted-printable

The ASCII codes for the question mark (?) and equals sign may not be represented directly as they are used to delimit the encoded-word. The ASCII code for space may not be represented directly because it could cause older parsers to split up the encoded word undesirably. To make the encoding smaller and easier to read the underscore is used to represent the ASCII code for space creating the side effect that underscore cannot be represented directly. Use of encoded words in certain parts of headers imposes further restrictions on which characters may be represented directly.

For example,
Subject: =?iso-8859-1?Q?=A1Hola,_se=F1or!?=
is interpreted as "Subject: ¡Hola, señor!".

The encoded-word format is not used for the names of the headers (for example Subject). These header names are always in English in the raw message. When viewing a message with a non-English e-mail client, the header names are usually translated by the client.
*/

static char hex_ch(int n)
{
	char ret = '!';
	if(n >= 0 && n < 10) ret = '0' + n;
	else if(n >= 10 && n < 16) ret = 'A' + (n - 10);
	return ret;
}

QByteArray QFQEncoding::encode(const QByteArray &src)
{
	QByteArray ret;
	for(int i=0; i<src.size(); i++) {
		char ch = src.at(i);
		char code[4] = {0, 0 ,0 ,0};
		int code_len = 0;
		if(ch == '=') {}
		else if(ch == '?') {}
		else if(ch == '_') {}
		else if(ch == ' ') {code[0] = '_'; code_len = 1;}
		else if(ch >= 33 && ch <= 126) {code[0] = ch; code_len = 1;}
		if(code_len == 0) {
			int n = (unsigned char)ch;
			code[0] = '=';
			code[1] = hex_ch(n / 16);
			code[2] = hex_ch(n % 16);
			code_len = 3;
		}
		ret += code;
	}
	return ret;
}

static char unhex_ch(char c)
{
	int ret = 0;
	if(c >= '0' && c <= '9') ret = c - '0';
	else if(c >= 'A' && c <= 'F') ret = c - 'A';
	else if(c >= 'a' && c <= 'f') ret = c - 'a';
	else {
		/// neco je spatne
	}
	return ret;
}

static char get_char(const QByteArray &digest, int &ix)
{
	char ret = 0;
	char ch = digest.at(ix);
	char ch2 = (ix < digest.size()-1)? digest.at(ix+1): 0;
	char ch3 = (ix < digest.size()-2)? digest.at(ix+2): 0;
	if(ch == '=') {
		/// encoded char
		int n = unhex_ch(ch2) * 16 + unhex_ch(ch3);
		if(n == 0) {
			/// nejakej error, ignoruj znaky
		}
		else {
			unsigned char uch = (unsigned char)n;
			ret = (char)uch;
		}
		ix += 3;
	}
	else if(ch == '_') {
		ret = ' ';
		ix++;
	}
	else {
		ret = ch;
		ix++;
	}
	return ret;
}

QByteArray QFQEncoding::decode(const QByteArray &digest)
{
	QByteArray ret;
	for(int ix=0; ix<digest.size(); ) {
		char ch = get_char(digest, ix);
		if(ch != 0) ret += ch;
	}
	return ret;
}

QByteArray QFQEncoding::encodeMessageHeaderUtf8(const QString &msg)
{
	qfLogFuncFrame() << msg;
	QByteArray ret("=?utf-8?Q?");
	ret += encode(msg.toUtf8());
	ret += "?=";
	qfTrash() << "\t return:" << ret;
	return ret;
}
