// C++ Implementation: crypt
//
// Description:
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "qfcrypt.h"

#include <QTime>

#include <qflogcust.h>

//===================================================================
//                                         Generator
//===================================================================
/// http://www.math.utah.edu/~pa/Random/Random.html
static unsigned defaultGenerator(unsigned val)
{
	const quint32 a = 16811;
	const quint32 c = 7;
	const quint32 max_rnd = 2147483647;
	quint32 n;
	n = (a*val + c) % max_rnd;
	return (unsigned)n;
}

//===================================================================
//                                         QFCrypt
//===================================================================
QFCrypt::QFCrypt(QFCrypt::Generator gen)
{
	f_generator = gen;
	if(f_generator == NULL) f_generator = defaultGenerator;
}

static QByteArray code_byte(quint8 b)
{
	QByteArray ret;
	/// hodnoty, ktere nejsou pismena se ukladaji jako cislo
	/// format cisla je 4 bity cislo % 10 [0-9] + 4 bity cislo / 10 [A-Z]
	char buff[] = {0,0,0};
	if((b>='A' && b<='Z') || (b>='a' && b<='z')) {
		ret.append(b);
	}
	else {
		buff[0] = b%10 + '0';
		buff[1] = b/10 + 'A';
		ret.append(buff);
	}
	return ret;
}

QByteArray QFCrypt::crypt(const QString &s, int min_length) const
{
	//QFCrypt ccc = QFCrypt(defaultGenerator);
	//ccc.crypt("");
	QByteArray dest;
	//if(isEmpty()) return dest;
	QByteArray src = s.toUtf8();
	// aby prazdny nebo kratky hesla nebyly zakrypteny kratkym stringem
	//src = src.leftJustified(10, '\0');

    	/// nahodne se vybere hodnota, kterou se string zaxoruje a ta se ulozi na zacatek
	unsigned val = (unsigned)qrand();
	val += QTime::currentTime().msec();
	val %= 256;
	if(val == 0) val = 1;/// sasd ma spatnej generator, kterej ma c == 0, tak at to neblbne pro 0 sekund
	quint8 b = (quint8)val;
	dest += code_byte(b);
	//qfError() << "<<" << b;

    // a tou se to zaxoruje
	for(int i=0; i<src.count(); i++) {
		val = f_generator(val);
		b = ((quint8)src[i]);
		//qfError() << val << "xor" << (char)b;
		b = b ^ (quint8)val;
		dest += code_byte(b);
		//qfError() << "<<" << b;
	}
	while(dest.size() < min_length) {
		val = f_generator(val);
		b = 0 ^ (quint8)val;
		dest += code_byte(b);
	}
	return dest;
}

static quint8 take_byte(const QByteArray &ba, int &i)
{
	quint8 b = ((quint8)ba[i++]);
	if((b>='A' && b<='Z') || (b>='a' && b<='z')) {
	}
	else {
		quint8 b1 = b;
		b = b1 - '0';
		if(i < ba.size()) {
			b1 = ba[i++];
			b +=  10 * (b1 - 'A');
		}
		else {
			qfError() << QF_FUNC_NAME << ": byte array corupted:" << ba.constData();
		}
	}
	return b;
}

QByteArray QFCrypt::decodeArray(const QByteArray &ba) const
{
	/// vyuziva toho, ze generator nahodnych cisel generuje pokazde stejnou sekvenci
	/// precte si seed ze zacatku \a ba a pak odxorovava nahodnymi cisly, jen to svisti
	qfLogFuncFrame() << "decoding:" << ba.constData();
	QByteArray ret;
	if(ba.isEmpty()) return ret;
	int i = 0;
	unsigned val = take_byte(ba, i);
	//qfError() << "val" << val << "i:" << i;
	while(i<ba.count()) {
		val = f_generator(val);
		quint8 b = take_byte(ba, i);
		//qfError() << val << "xor" << b;
		b = b ^ (quint8)val;
		//qfError() << "=" << (char)b;
		ret.append(b);
	}
	return ret;
}

QString QFCrypt::decrypt(const QByteArray &_ba) const
{
	/// odstran vsechny bile znaky, v zakodovanem textu nemohou byt, muzou to byt ale zalomeni radku
	QByteArray ba = _ba.simplified();
	ba.replace(' ', "");
	ba = decodeArray(ba);
	///odstran \0 na konci, byly tam asi umele pridany
	int pos = ba.size();
	while(pos > 0) {
		pos--;
		if(ba[pos] == '\0') {
		}
		else {
			pos++;
			break;
		}
	}
	ba = ba.mid(0, pos);
	return QString::fromUtf8(ba);
}
