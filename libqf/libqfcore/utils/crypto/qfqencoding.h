#ifndef QFQENCODING_H
#define QFQENCODING_H

#include <qfcoreglobal.h>

#include <QByteArray>

/**
@author Fanda Vacek
*/
class QFCORE_DECL_EXPORT QFQEncoding
{
	public:
		/// 8bit->7bit
		static QByteArray encode(const QByteArray &src);
		/// 7bit->8bit
		static QByteArray decode(const QByteArray &digest);
		static QByteArray encodeMessageHeaderUtf8(const QString &msg);
};

#endif //QFQENCODING_H
