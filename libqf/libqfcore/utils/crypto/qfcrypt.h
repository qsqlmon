//
// C++ Interface: crypt
//
// Description:
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef QF_CRYPT_H
#define QF_CRYPT_H

#include <qfcoreglobal.h>

#include <QByteArray>
#include <QString>

class QFCORE_DECL_EXPORT QFCrypt
{
	public:
		typedef unsigned (*Generator)(unsigned);
		/*
		class QFCORE_DECL_EXPORT Generator
		{
			public:
				virtual unsigned random(unsigned seed);
			public:
				virtual ~Generator() {};
		};
		*/
	protected:
		Generator f_generator;
	protected:
		QByteArray decodeArray(const QByteArray &ba) const;
	public:
		/// pouziva muj vlastni, pomerne prustrelny kryptovaci mechanismus
		/// @return libovolny string zakrypti do stringu obsahujiciho znaky 0-9, A-Z, a-z
		/// pokud je to, co se crypti moc kratky, je to docpano vatou tak, aby zakrypteny string mel minimalne \a min_length znaku, muze mit o jeden vic
		QByteArray crypt(const QString &s, int min_length = 10) const;

		/// inverzni operace k funkci crypt()
		QString decrypt(const QByteArray &ba) const;
	public:
		QFCrypt(Generator gen = NULL);
};

#endif // QF_CRYPT_H
