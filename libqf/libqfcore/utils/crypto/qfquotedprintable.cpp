
#include "qfquotedprintable.h"

#include <QString>

#include <qflogcust.h>

/**
Quoted-printable encoding RFC 2045

Any 8-bit byte value may be encoded with 3 characters, an "=" followed by two hexadecimal digits (0–9 or A–F) representing the byte's numeric value. For example, a US-ASCII form feed character (decimal value 12) can be represented by "=0C", and a US-ASCII equal sign (decimal value 61) is represented by "=3D". All characters except printable ASCII characters or end of line characters must be encoded in this fashion.

All printable ASCII characters (decimal values between 33 and 126) may be represented by themselves, except "=" (decimal 61).

ASCII tab and space characters, decimal values 9 and 32, may be represented by themselves, except if these characters appear at the end of a line. If one of these characters appears at the end of a line it must be encoded as "=09" (tab) or "=20" (space).

If the data being encoded contains meaningful line breaks, they must be encoded as an ASCII CR LF sequence, not as their original byte values. Conversely if byte values 13 and 10 have meanings other than end of line then they must be encoded as =0D and =0A.

Lines of quoted-printable encoded data must not be longer than 76 characters. To satisfy this requirement without altering the encoded text, soft line breaks may be added as desired. A soft line break consists of an "=" at the end of an encoded line, and does not cause a line break in the decoded text.
*/

static char hex_ch(int n)
{
	char ret = '!';
	if(n >= 0 && n < 10) ret = '0' + n;
	else if(n >= 10 && n < 16) ret = 'A' + (n - 10);
	return ret;
}

/// tak to neni odzkouseny, takze v tom jsou mozna chyby
QByteArray QFQuotedPrintable::encode(const QByteArray &src)
{
	QByteArray lines;
	QByteArray line;
	static const int max_row_len = 76;
	for(int i=0; i<src.size(); i++) {
		char ch = src.at(i);
		char ch2 = (i < src.size()-1)? src.at(i+1): 0; 
		char code[4] = {0, 0 ,0 ,0};
		int code_len = 0;
		if(ch == '=') {} /// zakoduj vsechny bile znaky, kvuli bilym znakum, ktere nemohou byt na konci radku, tak radsi vsechny
		else if(ch == '\t') {}
		else if(ch >= 33 && ch <= 126) {code[0] = ch; code_len = 1;}
		else if(ch == '\n' && ch2 == 0) {code[0] = '\r'; code[1] = '\n'; code_len = 2;}
		else if(ch == '\r' && ch2 == 0) {code[0] = '\r'; code[1] = '\n'; code_len = 2;}
		else if(ch == '\r' && ch2 == '\n') {code[0] = '\r'; code[1] = '\n'; code_len = 2; i++;}
		else {ch = ' ';} 
		if(code_len == 0) {
			int n = (unsigned char)ch;
			code[0] = '=';
			code[1] = hex_ch(n / 16);
			code[2] = hex_ch(n % 16);
			code_len = 3;
		}
		if(line.count() + code_len < max_row_len) line += code;
		else {
			if(lines.isEmpty()) lines = line;
			else lines += "=\r\n" + line; /// soft line break
			line = code;
		}
	}
	if(line.count()) {
		if(lines.isEmpty()) lines = line;
		else lines += "=\n" + line; /// soft line break
	}
	return lines;
}

static char unhex_ch(char c)
{
	int ret = 0;
	if(c >= '0' && c <= '9') ret = c - '0';
	else if(c >= 'A' && c <= 'F') ret = c - 'A';
	else if(c >= 'a' && c <= 'f') ret = c - 'a';
	else {
		/// neco je spatne
	}
	return ret;
}

static char get_char(const QByteArray &digest, int &ix)
{
	char ret = 0;
	char ch = digest.at(ix);
	char ch2 = (ix < digest.size()-1)? digest.at(ix+1): 0;
	char ch3 = (ix < digest.size()-2)? digest.at(ix+2): 0;
	if(ch == '=') {
		if(ch2 == '\r' && ch3 == '\n') {
			/// soft break
		}
		else {
			/// encoded char
			int n = unhex_ch(ch2) * 16 + unhex_ch(ch3);
			if(n == 0) {
				/// nejakej error, ignoruj znaky
			}
			else {
				unsigned char uch = (unsigned char)n;
				ret = (char)uch;
			}
		}
		ix += 3;
	}
	else {
		ret = ch;
		ix++;
	}
	return ret;
}

QByteArray QFQuotedPrintable::decode(const QByteArray &digest)
{
	QByteArray ret;
	for(int ix=0; ix<digest.size(); ) {
		char ch = get_char(digest, ix);
		if(ch != 0) ret += ch;
	}
	return ret;
}

