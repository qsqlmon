#ifndef QFQUOTEDPRINTABLE_H
#define QFQUOTEDPRINTABLE_H

#include <qfcoreglobal.h>

#include <QByteArray>

/**
@author Fanda Vacek
*/
class QFCORE_DECL_EXPORT QFQuotedPrintable
{
	public:
		/// 8bit->7bit
		static QByteArray encode(const QByteArray &src);
		/// 7bit->8bit
		static QByteArray decode(const QByteArray &digest);
};

#endif //QFQUOTEDPRINTABLE_H
