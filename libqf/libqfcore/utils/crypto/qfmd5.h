//
// C++ Interface: qfmd5
//
// Description: 
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef QFMD5_H
#define QFMD5_H

///use QCryptographicHash instead

#include <QString>

/**
@author Fanda Vacek
*/
class QFMD5
{
	public:
		QFMD5();
		~QFMD5();

		static QByteArray md5(const QByteArray &src);
		//! converts result of md5() to QString
		static QString digestToString(const QByteArray &digest);
};

#endif
