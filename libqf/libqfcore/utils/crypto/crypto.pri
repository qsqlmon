#message("including module crypto")

INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/qfcrypt.h \
    $$PWD/qfquotedprintable.h \
    $$PWD/qfqencoding.h \

SOURCES += \
    $$PWD/qfcrypt.cpp \
    $$PWD/qfquotedprintable.cpp \
    $$PWD/qfqencoding.cpp \
