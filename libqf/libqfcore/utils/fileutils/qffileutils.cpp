//
// C++ Implementation: qffileutils
//
// Description:
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "qffileutils.h"

#include <qfstring.h>

#include <QDir>
#include <QUrl>

#include <qflogcust.h>

QString QFFileUtils::unixSeparators(const QString &file_name)
{
	QChar sep = QDir::separator();
	QString fn = file_name;
	fn = fn.replace(sep, '/');
	return fn;
}

QString QFFileUtils::path(const QString &file_name)
{
	QFString fn = QFFileUtils::unixSeparators(file_name);
	int ix = fn.lastIndexOf('/');
	if(ix < 0) return QString();
	return fn.slice(0, ix+1);
}

QString QFFileUtils::file(const QString &file_name)
{
	QFString fn = QFFileUtils::unixSeparators(file_name);
	int ix = fn.lastIndexOf('/');
	if(ix < 0) return fn;
	return fn.slice(ix + 1);
}

QString QFFileUtils::extension(const QString &file_name)
{
	QFString fn = file_name;
	int ix = fn.lastIndexOf('.');
	if(ix < 0) return QString();
	return fn.slice(ix + 1).trim();
}

QString QFFileUtils::stripExtension(const QString &file_name)
{
	QFString fn = file_name;
	int ix = fn.lastIndexOf('.');
	if(ix < 0) return fn;
	return fn.slice(0, ix);
}

QString QFFileUtils::stripExtensions(const QString &file_name)
{
	QFString fn = file_name;
	QFString p = QFFileUtils::path(fn);
	fn = QFFileUtils::baseName(fn);
	if(!!p) fn = QFFileUtils::joinPath(p, fn);
	return fn;
}

QString QFFileUtils::baseName(const QString &path)
{
	QFString s = file(path);
	int ix = s.indexOf('.');
	if(ix < 0) return s;
	return s.slice(0, ix);
}

QString QFFileUtils::homeDir()
{
	QString s = QDir::homePath();
	qfTrash() << QF_FUNC_NAME << s;
	return s;
}

QString QFFileUtils::currDir()
{
	return QDir::currentPath();
}

QString QFFileUtils::tempDir()
{
	return QDir::tempPath();
}

QString QFFileUtils::appDir()
{
	return QCoreApplication::applicationDirPath();
}

QString QFFileUtils::appTempDir() throw(QFException)
{
	QString app_name =  QFCompat::appName();
	QString home_dir =  QFFileUtils::homeDir();
	QString tmp_dir = QFFileUtils::joinPath(home_dir, "." + app_name);
	tmp_dir = QFFileUtils::joinPath(tmp_dir, "tmp");
	QDir d(tmp_dir);
	if(!d.exists()) {
		if(!d.mkpath(tmp_dir)) {
			QF_EXCEPTION(QObject::tr("Cann't create temporary directory '%1'").arg(tmp_dir));
		}
	}
	return tmp_dir;
}

QString QFFileUtils::joinPath(const QString &path1, const QString &path2)
{
	qfLogFuncFrame() << path1 << path2;
	QFString fs = path1;
	if(!fs) {
		fs = path2;
	}
	else {
		if(!path2.isEmpty() && path2[0] != '/' && fs[-1] != '/') fs += "/"; /// resim tim chybu/vlastnost cleanPath()
		fs += path2;
		qfTrash() << "\t before cleanPath:" << fs;
	}
	/// pozor cleanPath: knihazakazek/..//companyheadercontent.inc.xml vyrobi /companyheadercontent.inc.xml
	/// z relativni cesty to udela absolutni
	fs = QDir::cleanPath(fs);
	qfTrash() << "\t return:" << fs;
	return fs;
}

QString QFFileUtils::joinPath(const QStringList & paths)
{
	QString s = paths.join("/");
	s = QDir::cleanPath(s);
	return s;
}

bool QFFileUtils::ensurePath(const QString & _path)
{
	QDir dir(_path);
	QFString path = QDir::fromNativeSeparators(dir.absolutePath());
	QStringList sl = path.splitAndTrim('/', '"', QFString::TrimParts, QString::SkipEmptyParts);
	if(path[1] == ':') {
		/// windows
		dir = QDir(sl[0]);
		sl = sl.mid(1);
	}
	else {
		/// unix
		dir = QDir("/");
		if(sl.count() > 0 && sl[0].isEmpty()) sl = sl.mid(1);
	}
	return dir.mkpath(sl.join("/"));
}

QUrl QFFileUtils::saveText(const QString & text, const QString & filename_with_path, const char *codec_name)
{
	QUrl url;
	QString dir = QFFileUtils::path(filename_with_path);
	QString file = QFFileUtils::file(filename_with_path);
	if(ensurePath(dir)) {
		QFile f(filename_with_path);
		if(f.open(QIODevice::WriteOnly)) {
			QTextStream ts(&f);
			ts.setCodec(codec_name);
			ts << text;
			f.close();
			QDir d(dir);
			QString abs_file_name = d.absoluteFilePath(file);
			url = QUrl::fromLocalFile(abs_file_name);
			//url = QUrl(f.fileName());
			//url.setScheme("file");
		}
	}
	return url;
}

bool QFFileUtils::removeDirContent(const QDir & dir)
{
	qfLogFuncFrame() << dir.absolutePath();
	bool ret = true;
	QDir d = dir;
	d.setNameFilters(QStringList());
	foreach(QFileInfo fi, d.entryInfoList()) {
		if(fi.isFile()) {
			QString fn = fi.absoluteFilePath();
			qfTrash() << "\t removing ordinary file:" << fn;
			if(!QFile::remove(fn)) {ret = false; break;}
		}
		else if(fi.isDir()) {
			QString dir_name = fi.absoluteFilePath();
			if(d.absolutePath().startsWith(dir_name)) continue; /// current dir nebo parentdir
			if(!fi.isSymLink()) {
				qfTrash() << "\t removing content of directory:" << dir_name;
				if(!removeDirContent(QDir(dir_name))) {ret = false; break;}
			}
			qfTrash() << "\t removing directory:" << QDir(dir_name).dirName();
			if(!d.rmdir(QDir(dir_name).dirName())) {ret = false; break;}
		}
	}
	qfTrash() << "\t return:" << ret;
	return ret;
}
