
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfcsvreader.h"

#include <qfstring.h>

#include <qflogcust.h>

QFCsvReader::QFCsvReader(QTextStream *ts, char _separator, char _quote)
	: fTextStream(ts), fSeparator(_separator), fQuote(_quote)
{
}

QFCsvReader::~QFCsvReader()
{
}

QString QFCsvReader::unquoteCsvField(const QString &s)
{
	if(doubleQuote.isEmpty()) doubleQuote = QString("%1%2").arg(fQuote).arg(fQuote);
	if(singleQuote.isEmpty()) singleQuote = QString("%1").arg(fQuote);
	QFString ret = s;
	if(ret[0] == fQuote) {
		ret = ret.slice(1, -1);
	}
	ret = ret.replace(doubleQuote, singleQuote);
	return ret;
}

QString QFCsvReader::readCsvLine()
{
	QFString ret;
	int qcnt = 0;
	do {
		QString s = textStream().readLine();
		qcnt += s.count(fQuote);
		ret += s;
		if(qcnt % 2) ret += "\n";
	} while((qcnt % 2) && !textStream().atEnd());
	return ret;
}

QStringList QFCsvReader::readCsvLineSplitted()
{
	QFString s = readCsvLine();
	QStringList sl = s.splitAndTrim(fSeparator, fQuote, QFString::TrimParts, QString::KeepEmptyParts);
	QStringList ret;
	foreach(s, sl) ret << unquoteCsvField(s);
	return ret;
}

QString QFCsvReader::quoteCsvField(const QString &s)
{
	if(doubleQuote.isEmpty()) doubleQuote = QString("%1%2").arg(fQuote).arg(fQuote);
	if(singleQuote.isEmpty()) singleQuote = QString("%1").arg(fQuote);
	if(charsToQuote.isEmpty()) charsToQuote = QString("%1%2%3%4").arg(fSeparator).arg('\r').arg('\n').arg('#');
	QFString ret = s;
	ret = ret.replace(singleQuote, doubleQuote);
	if(ret.indexOfOneOf(charsToQuote) >= 0) {
		ret = singleQuote + ret + singleQuote;
	}
	return ret;
}


void QFCsvReader::writeCsvLine(const QStringList &values, int option_flags)
{
	int i = 0;
	foreach(QString s, values) {
		if(i++ > 0) textStream() << fSeparator;
		textStream() << quoteCsvField(s);
	}
	if(option_flags & AppendEndl) textStream() << QF_EOLN;
}


