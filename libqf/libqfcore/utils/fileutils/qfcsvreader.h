
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFCSVREADER_H
#define QFCSVREADER_H

#include <qfcoreglobal.h>
#include <qfexception.h>


//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFCsvReader 
{
	public:
		enum {AppendEndl = 1};
	protected: 
		QTextStream *fTextStream;
		char fSeparator, fQuote;
		QString singleQuote, doubleQuote, charsToQuote; ///< pomocne promenne
	public:
		void setSeparator(char _separator) {fSeparator = _separator;}
		void setQuote(char _quote) {fQuote = _quote;}
		void setTextStream(QTextStream *ts) {
			setTextStream(ts, fSeparator, fQuote);
		}
		void setTextStream(QTextStream *ts, char _separator, char _quote = '"')
		{
			fTextStream = ts;
			setSeparator(_separator);
			setQuote(_quote);
		}
		QTextStream& textStream() throw(QFException) {
			if(!fTextStream) QF_EXCEPTION("DataStream is NULL.");
			return *fTextStream;
		}
				
		QString unquoteCsvField(const QString &s);
		QString readCsvLine();
		QStringList readCsvLineSplitted();
		QString quoteCsvField(const QString &s);
		void writeCsvLine(const QStringList &values, int option_flags = 0);
	public:
		QFCsvReader(QTextStream *ts = NULL, char _separator = ',', char _quote = '"');
		virtual ~QFCsvReader();
};
   
#endif // QFCSVREADER_H

