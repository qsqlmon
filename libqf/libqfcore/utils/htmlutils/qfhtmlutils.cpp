
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfhtmlutils.h"
//#include <qfapplication.h>
#include <qffileutils.h>
//#include <qfdlghtmlview.h>

#include <QDir>

#include <qflogcust.h>

QString QFHtmlUtils::addHtmlEnvelope(const QString &html_body_content, const QVariantMap &opts)
{
	Q_UNUSED(opts);
	QString eoln = opts.value("lineSeparator", "\n").toString();
	//QString ind_str = opts.value("lineIndent", "  ").toString();
	QString ret;
	ret += "<html>" + eoln;
	ret += "<head>" + eoln;
	ret += "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"/>" + eoln;
	ret += "<title>Data</title>" + eoln;
	ret += "<style type=\"text/css\">"
			"th {background-color:khaki; font-weight:bold;}"
			"table {border-collapse: collapse;}"
			"</style>" + eoln;
	ret += "</head>" + eoln;
	ret += "<body>" + eoln;
	ret += html_body_content;
	ret += "</body>" + eoln;
	ret += "</html>" + eoln;
	return ret;
}

QString QFHtmlUtils::addXHtmlEnvelope(const QString & xhtml_body_content, const QVariantMap & opts)
{
	QString eoln = opts.value("lineSeparator", "\n").toString();
	//QString ind_str = opts.value("lineIndent", "  ").toString();
	QString ret;
	ret += "<!DOCTYPE  html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + eoln;
	ret += "<html xmlns=\"http://www.w3.org/1999/xhtml\">" + eoln;
	ret += "<head>" + eoln;
	ret += "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"/>" + eoln;
	ret += "<title>Data</title>" + eoln;
	ret += "<style type=\"text/css\">"
			"th {background-color:khaki; font-weight:bold;}"
			"table {border-collapse: collapse;}"
			"</style>" + eoln;
	ret += "</head>" + eoln;
	ret += "<body>" + eoln;
	ret += xhtml_body_content;
	ret += "</body>" + eoln;
	ret += "</html>" + eoln;
	return ret;
}

QFDomDocument QFHtmlUtils::bodyToHtmlDocument(const QString & html_body_content)
{
	QDomDocument doc;
	QString s = addHtmlEnvelope(html_body_content);
	//qfInfo() << s;
	doc.setContent(s);
	//qfInfo() << doc.toString(4);
	return doc;
}

QFDomDocument QFHtmlUtils::bodyToXHtmlDocument(const QString & xhtml_body_content)
{
	QDomDocument doc;
	QString s = addXHtmlEnvelope(xhtml_body_content);
	//qfInfo() << s;
	doc.setContent(s);
	//qfInfo() << doc.toString(4);
	return doc;
}
#ifdef Q_OS_WIN32
static QString copy_file(const QString &orig, const QString &dest)
{
	QString ret;
	QFile f1(orig);
	QFile f2(dest);
	if(!f1.open(QIODevice::ReadOnly)) {
		ret = QString("Cann't open '%1' for reading.").arg(orig);
	}
	else if(!f2.open(QIODevice::WriteOnly)) {
		ret = QString("Cann't open '%1' for writing.").arg(dest);
	}
	else {
		QByteArray ba = f1.readAll();
		if(f2.write(ba) != ba.size()) {
			ret = QString("Error writing to file '%1'.").arg(dest);
		}
	}
	return ret;
}
#endif
void QFHtmlUtils::saveHtmlWithResources(const QString &html_content, const QString &content_base_dir, const QString &file_name, const QString &_resources_dir) throw(QFException)
{
	qfLogFuncFrame() << "file_name:" << file_name;
	if(file_name.isEmpty()) return;
	QString new_content = html_content;
	QString resources_dir = _resources_dir;
	resources_dir.replace("${FILE_NAME}", QFFileUtils::baseName(file_name));
	qfTrash() << "\t resources_dir:" << resources_dir;

	QFStringMap files_to_relocate;
	{
		/// obrazky
		QRegExp rx = QRegExp("<img\\s.*src=\"([^\"]*)\".*>");
		rx.setMinimal(true);
		files_to_relocate.unite(relocateContent(new_content, rx, resources_dir));
	}
	{
		/// css
		QRegExp rx = QRegExp("<link\\s.*href=\"([^\"]*\\.css)\".*>");
		rx.setMinimal(true);
		files_to_relocate.unite(relocateContent(new_content, rx, resources_dir));
	}

	//QString abs_dest_dir_str = QFFileUtils::joinPath(QFFileUtils::path(file_name), resources_dir);
	QString abs_dest_dir = QFFileUtils::path(file_name);
	foreach(QString orig_rsc, files_to_relocate.uniqueKeys()) {
		QString dest_rsc = files_to_relocate.value(orig_rsc);
		QString abs_orig_rsc = orig_rsc;
		if(abs_orig_rsc.startsWith("qrc:/")) {
			/// webkit umi jen qrc:/ a QT zase jen :/ , pro kopirovani je to potreba upravit na :/
			abs_orig_rsc = abs_orig_rsc.mid(3);
		}
		else if(abs_orig_rsc.startsWith(":/")) {}
		else if(QDir::isRelativePath(abs_orig_rsc)) {
			/// pokud je cesta relativni (neni to neco jako :/green-dot.png), pridej pred to content_base_dir
			abs_orig_rsc = QFFileUtils::joinPath(content_base_dir, abs_orig_rsc);
		}
		QString abs_dest_rsc = QFFileUtils::joinPath(abs_dest_dir, dest_rsc);
		/// vymaz soubor, ktery neni z /:*, pokud tam uz je, protoze se mohl od predchoziho ulozeni zmenit
		if(abs_orig_rsc.startsWith(":/") && QFile::exists(abs_dest_rsc)) {
			/// pokud tam soubor z resouces (:/*) uz je, nema cenu ho tam cpat znovu
		}
		else {
			qfTrash() << "\t ensuring dir:" << QFFileUtils::path(abs_dest_rsc);
			if(!QFFileUtils::ensurePath(QFFileUtils::path(abs_dest_rsc))) {
				QString s = TR("Nelze vytvorit cestu '%1'.").arg(QFFileUtils::path(abs_dest_rsc));
				QF_EXCEPTION(s);
				return;
			}
			/// pripadne jiz existujici soubor vymaz, resource se mohl od predchoziho ulozeni zmenit
			QFile::remove(abs_dest_rsc);
			qfTrash().noSpace() << "Copying '" << abs_orig_rsc << "' to '" << abs_dest_rsc;
#ifdef Q_OS_WIN32
			/// qt 4.5.1 maji asi chybu v QFile::copy(), asi se nepodari prejmenovani tmp souboru, protoze je otevren.
			/// bug 97172 a spousta dalsich fixed in 4.5.2
			QString errstr = copy_file(abs_orig_rsc, abs_dest_rsc);
			if(errstr.size()) {
				qfError().noSpace() << "Copy '" << abs_orig_rsc << "' to '" << abs_dest_rsc << "' unsuccessful. File error " << errstr;
			}
#else
			if(!QFile::copy(abs_orig_rsc, abs_dest_rsc)) {
				qfError().noSpace() << "Copy '" << abs_orig_rsc << "' to '" << abs_dest_rsc << "' unsuccessful.";
			}
#endif
		}
	}
	QFile f(file_name);
	if(!f.open(QIODevice::WriteOnly)) {
		QString s = TR("Nelze otevrit soubor %1 pro zapis.").arg(file_name);
		QF_EXCEPTION(s);
		return;
	}
	f.write(new_content.toUtf8());
}

QFStringMap QFHtmlUtils::relocateContent(QString & _content, QRegExp & rx, const QString &resources_dir)
{
	qfLogFuncFrame();
	QFStringMap ret;
	QString relocated_content;
	QFString content = _content;
	int pos = 0, pos1 = 0;
	while ((pos = rx.indexIn(content, pos)) != -1) {
		QFString rsc = rx.cap(1);
		if(!!rsc && !rsc.startsWith('/' && !rsc.contains(':'))) {
			/// pokud je to relativni cesta k souboru
			qfTrash() << "\t nasel jsem:" << rx.cap(0);
			QString orig_rsc = rsc;
			relocated_content += content.slice(pos1, rx.pos(1));
			if(rsc.startsWith(":/")) rsc = rsc.mid(2);
			else if(rsc.startsWith("qrc:/")) rsc = rsc.mid(5);
			else if(rsc.startsWith(resources_dir + '/')) {
				/// pokud cesta jiz zacina na resources_dir, neni treba to tam znovu pridavat, muze k tomu dojit pri opakovanem otevirani a ukladani a html dokumentu
				rsc = rsc.mid(resources_dir.length() + 1);
			}
			/// pridej pred resource resources_dir a vymen to v tom, co jsi nasel
			rsc = QFFileUtils::joinPath(resources_dir, rsc);
			qfTrash() << "\t predelal jsem to na:" << rsc;
			ret[orig_rsc] = rsc;
			relocated_content += rsc;
			pos1 = pos + rx.matchedLength();
			relocated_content += content.slice(rx.pos(1) + rx.cap(1).length(), pos1);
		}
		pos += rx.matchedLength();
	}
	relocated_content += content.slice(pos1);
	_content = relocated_content;
	return ret;
}

