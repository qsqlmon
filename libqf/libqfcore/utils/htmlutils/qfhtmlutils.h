
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFHTMLUTILS_H
#define QFHTMLUTILS_H

#include <qfcoreglobal.h>
#include <qfdom.h>
#include <qfexception.h>

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFHtmlUtils
{
	protected:
		/*
		struct FilePair {
			QString originalPath;
			QString relocatedPath;

			//FilePair(const QString &orig, const QString &reloc) : originalPath(orig), relocatedPath(reloc) {}
		};
		*/
		/// vraci mapu souboru, ktere byly relokovany (orig->new)
		static QFStringMap relocateContent(QString &content, QRegExp &rx, const QString &resources_dir);
	public:
		//! vytvori adresar file_name-resources a do nej zkopiruje vsechny obrazky odkazovane z resources.
		static void saveHtmlWithResources(const QString &html_content, const QString &content_base_dir, const QString &file_name, const QString &resources_dir = "${FILE_NAME}-resources") throw(QFException);
		/// opt - lineSeparator (default \n)
		static QString addHtmlEnvelope(const QString &html_body_content, const QVariantMap &opts = QVariantMap());
		static QFDomDocument bodyToHtmlDocument(const QString &html_body_content);
		static QString addXHtmlEnvelope(const QString &xhtml_body_content, const QVariantMap &opts = QVariantMap());
		static QFDomDocument bodyToXHtmlDocument(const QString &xhtml_body_content);
};

#endif // QFHTMLUTILS_H

