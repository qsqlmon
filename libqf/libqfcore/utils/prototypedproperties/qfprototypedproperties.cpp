
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfprototypedproperties.h"

#include <qf.h>
#include <qfjson.h>

#include <qflogcust.h>

//=================================================
//        QFPrototypedProperties::Property
//=================================================
const QString QFPrototypedProperties::Property::VALUE_KEY = QString("value");
const QString QFPrototypedProperties::Property::META_KEY = QString("meta");
const QString QFPrototypedProperties::Property::KEYS_KEY = QString("keys");
const QString QFPrototypedProperties::Property::CHILDREN_KEY = QString("children");

const QString QFPrototypedProperties::Property::TYPE_KEY = QString("type");
const QString QFPrototypedProperties::Property::CAPTION_KEY = QString("caption");
const QString QFPrototypedProperties::Property::UNIT_KEY = QString("unit");
const QString QFPrototypedProperties::Property::HELP_KEY = QString("help");
const QString QFPrototypedProperties::Property::OPTIONS_KEY = QString("options");

void QFPrototypedProperties::Property::setMetaValue(const QString &key, const QVariant &val)
{
	//QVariant v = *this;
	QVariantMap m = toMap();
	QVariantMap m2 = m.value(META_KEY).toMap();
	if(val.isValid()) m2[key] = val;
	else m2.remove(key);
	if(m2.isEmpty()) m.remove(META_KEY);
	else m[META_KEY] = m2;
	*this = m;
	//setValue(v);
}

QFPrototypedProperties::Property QFPrototypedProperties::Property::child(const QString &key) const
{
	//qfLogFuncFrame() << "key:" << key;
	QVariant v = children().value(key);
	//qfTrash() << "\t variant:" << QFJson::variantToString(v);
	Property p(v);
	//qfTrash() << "\t property:" << QFJson::variantToString(p);
	return p;
}

void QFPrototypedProperties::Property::appendChild(const QString &key, const QFPrototypedProperties::Property &p)
{
	if(key.isEmpty()) return;
	QVariantMap m = toMap();
	QStringList kl = keys();
	if(!kl.contains(key)) {
		kl << key;
		m[KEYS_KEY] = kl;
		*this = m;
	}
	setChild(key, p);
}

void QFPrototypedProperties::Property::setChild(const QString &key, const QFPrototypedProperties::Property &p)
{
	//QVariant v = *this;
	QVariantMap m = toMap();
	QVariantMap m2 = m.value(CHILDREN_KEY).toMap();
	if(p.isValid()) m2[key] = p;
	else m2.remove(key);
	if(m2.isEmpty()) m.remove(CHILDREN_KEY);
	else m[CHILDREN_KEY] = m2;
	*this = m;
	//setValue(v);
}

QFPrototypedProperties::Property QFPrototypedProperties::Property::cd(const QStringList &path) const
{
	Property p = *this;
	foreach(QString s, path) {
		p = p.child(s);
		if(!p.isValid()) break;
	}
	return p;
}

QVariant QFPrototypedProperties::Property::value(int *p_status) const
{
	int status = ValueNotFound;
	QVariant ret;
	QVariantMap m = toMap();
	if(m.isEmpty()) {
		ret = *this;
		if(ret.isValid()) status = ValueOwn;
	}
	else {
		if(m.contains(VALUE_KEY)) {
			status = ValueOwn;
			ret = m.value(VALUE_KEY);
		}
		else {
			if(!type().isEmpty()) {
				m = m.value(META_KEY).toMap();
				status = ValueTemplate;
				ret = m.value(VALUE_KEY);
			}
		}
	}
	if(p_status) *p_status = status;
	return ret;
}

void QFPrototypedProperties::Property::setValue(const QVariant &val)
{
	QVariantMap m = toMap();
	if(m.isEmpty()) *this = val;
	else {
		if(val.isValid()) {
			m[VALUE_KEY] = val;
			*this = m;
		}
		else {
			m.remove(VALUE_KEY);
			if(m.isEmpty()) *this = QVariant();
			else *this = m;
		}
	}
}
#if 0
//=================================================
//             QFPrototypedProperties::TemplateProperty
//=================================================
const QFPrototypedProperties::TemplateProperty& QFPrototypedProperties::TemplateProperty::sharedNull()
{
	static TemplateProperty n = TemplateProperty(SharedDummyHelper());
	return n;
}

QFPrototypedProperties::TemplateProperty::TemplateProperty(QFPrototypedProperties::TemplateProperty::SharedDummyHelper )
{
	d = new Data();
	d->type = Null;
}

QFPrototypedProperties::TemplateProperty::TemplateProperty()
{
	*this = sharedNull();
}
#if 0
//=================================================
//             QFPrototypedProperties::Property
//=================================================
const QFPrototypedProperties::Property& QFPrototypedProperties::Property::sharedNull()
{
	static Property n = Property(SharedDummyHelper());
	return n;
}

QFPrototypedProperties::Property::Property(QFPrototypedProperties::Property::SharedDummyHelper )
{
	d = new Data();
}

QFPrototypedProperties::Property::Property()
{
	*this = sharedNull();
}
#endif
#endif
//=================================================
//             QFPrototypedProperties
//=================================================
#if 0

const QFPrototypedProperties & QFPrototypedProperties::sharedNull()
{
	static QFPrototypedProperties n = QFPrototypedProperties(SharedDummyHelper());
	return n;
}

QFPrototypedProperties::QFPrototypedProperties(QFPrototypedProperties::SharedDummyHelper )
{
	d = new Data();
}
#endif
QFPrototypedProperties::Data::Data()
{
	//prototype = NULL;
	//qfDebug() << "Data():" << this << "prototype:" << prototype << "prototype data:" << (prototype? prototype->d: NULL);
}

QFPrototypedProperties::Data::Data(const QFPrototypedProperties &proto)
{
	//qfDebug() << "Data(const QFPrototypedProperties &proto): creating data from prototype:" << this << "other prototype:" << &proto << "other data:" << proto.d.data();
	QFPrototypedProperties *pp = new QFPrototypedProperties(proto);
	prototypes << pp;
	//if(prototype) qfDebug() << "--Data(const QFPrototypedProperties &proto):" << this << "prototype:" << prototype << "prototype data:" << prototype->d.data();
	//else qfDebug() << "--Data(const QFPrototypedProperties &proto):" << this << "prototype:" << prototype << "prototype data:" << 0;
}

QFPrototypedProperties::Data::Data(const Data &other)
	: QSharedData(other)
{
	//qfDebug() << "Data(const Data &other): creating data from other data:" << this << "other data:" << &other;
	dataRoot = other.dataRoot;
	foreach(QFPrototypedProperties *o_pp, other.prototypes) {
		QFPrototypedProperties *pp = new QFPrototypedProperties(*o_pp);
		prototypes << pp;
	}
	//if(prototype) qfDebug() << "--Data(const Data &other):" << this << "prototype:" << prototype << "prototype data:" << prototype->d.data();
	//else qfDebug() << "--Data(const Data &other):" << this << "prototype:" << prototype << "prototype data:" << 0;
}

QFPrototypedProperties::Data::~Data()
{
	//qfDebug() << "~Data():" << this << "prototype:" << prototype << "prototype data:" << (prototype? prototype->d: NULL);
	qDeleteAll(prototypes);
}
/// neni potreba, je private v QSharedData
/*
QFPrototypedProperties::Data& QFPrototypedProperties::Data::operator=(const QFPrototypedProperties::Data &other)
{
	if(other.prototype) {
		QF_SAFE_DELETE(prototype);
		prototype = new QFPrototypedProperties(*other.prototype);
	}
	else QSharedData::operator=(other);
	return *this;
}
*/
const QVariant QFPrototypedProperties::IMPOSSIBLE_VALUE = QString("_!QW@.#ER$%TY^|:");

QFPrototypedProperties::QFPrototypedProperties()
{
	d = new Data();
	//qfDebug() << "QFPrototypedProperties():" << this << "data:" << d.data(); 
}

QFPrototypedProperties::QFPrototypedProperties(int , const QFPrototypedProperties &prototype)
{
	d = new Data(prototype);
	//qfDebug() << "QFPrototypedProperties(int , const QFPrototypedProperties &prototype):" << this << "data:" << d.data();
}

QFPrototypedProperties::~QFPrototypedProperties()
{
	//qfDebug() << "~QFPrototypedProperties():" << this << "data:" << d.data();
}

void QFPrototypedProperties::setPrototype(const QFPrototypedProperties &proto)
{
	qDeleteAll(d->prototypes);
	d->prototypes.clear();
	addPrototype(proto);
}

void QFPrototypedProperties::addPrototype(const QFPrototypedProperties &proto)
{
	QFPrototypedProperties *pp = new QFPrototypedProperties(proto);
	d->prototypes << pp;
}

QVariant QFPrototypedProperties::stringToVariant(const QString &str)
{
	return QFJson::stringToVariant(str);
}

QString QFPrototypedProperties::variantToString(const QVariant &val)
{
	return QFJson::variantToString(val);
}

QVariant QFPrototypedProperties::value_helper(const QFPrototypedProperties *pp, const QStringList &path, int &status) const
{
	status = PropertyNotFound;
	QVariant ret;
	int pp_status;
	QVariant v = pp->d->dataRoot.cd(path).value(&pp_status);
	if(pp_status > Property::ValueNotFound) {
		ret = v;
		if(pp_status & Property::ValueOwn) status |= PropertyOwnValue;
		if(pp_status & Property::ValueTemplate) status |= PropertyTemplateValue;
		if(pp != this) status |= PropertyPrototypeValue;
	}
	else {
		foreach(pp, pp->d->prototypes) {
			ret = value_helper(pp, path, status);
			if(status > PropertyNotFound) break;
		}
	}
	return ret;
}

/*
bool QFPrototypedProperties::hasOwnValue(const QString &path) const
{
	QStringList sl = QFString(path).splitAndTrim('/');
	}
	bool hasValue(const QString &path) const;
	*/
/*
void QFPrototypedProperties::setValue(const QString &path, const QVariant &val);

bool QFPrototypedProperties::hasOwnValue(const QString &path) const
{
	int status;
	value(path, QVariant(), &status);
	return status & PropertyOwnValue;
}
*/
QVariant QFPrototypedProperties::value(const QStringList &path, const QVariant &default_val, int *p_status) const
{
	int status;// = PropertyNotFound;
	QVariant ret = value_helper(this, path, status);
	if(status == PropertyNotFound) ret = default_val;
	if(p_status) *p_status = status;
	return ret;
}

QVariant QFPrototypedProperties::value(const QString &path, const QVariant &default_val, int *p_status) const
{
	QStringList sl = QFString(path).splitAndTrim('/');
	QVariant ret = value(sl, default_val, p_status);
	return ret;
}

void QFPrototypedProperties::setOwnValue_helper(QFPrototypedProperties::Property &p, const QStringList &sl, const QVariant &val)
{
	if(sl.count()) {
		QString key = sl[0];
		QStringList sl1 = sl.mid(1);
		Property pp = p.child(key);
		if(sl1.isEmpty()) {
			pp.setValue(val);
		}
		else {
			setOwnValue_helper(pp, sl1, val);
		}
		p.setChild(key, pp);
	}
}

void QFPrototypedProperties::setValue(const QString &path, const QVariant &val)
{
	QStringList sl = QFString(path).splitAndTrim('/');
	setOwnValue_helper(d->dataRoot, sl, val);
}

void QFPrototypedProperties::keys_helper(const QFPrototypedProperties *pp, const QStringList &path, QStringList &found_keys) const
{
	qfLogFuncFrame();
	qfTrash() << "\t pp:" << pp;
	Property p = pp->d->dataRoot.cd(path);
	QStringList kl = p.keys();
	qfTrash() << "\t keys:" << kl.join(",");
	foreach(QString s, kl) {
		found_keys.removeOne(s); /// pokud je klic 2x ma prednost puvodni posice z drivejsiho prototypu, aby mi nedochazelo ke zmene poradi klicu pri dedeni
	}
	if(!kl.isEmpty()) found_keys = kl + found_keys; /// prototype hodnoty napred
	/// prototypy vem pozpatku, aby mely prednost klice z 1. prototypu
	QList<QFPrototypedProperties*> pplst = pp->d->prototypes;
	for(int i=pplst.count()-1; i>=0; i--) {
		pp = pplst.value(i);
		keys_helper(pp, path, found_keys);
	}
}

QStringList QFPrototypedProperties::keys(const QString &path) const
{
	qfLogFuncFrame();
	QStringList sl = QFString(path).splitAndTrim('/');
	QStringList ret;
	keys(sl);
	return ret;
}

QStringList QFPrototypedProperties::keys(const QStringList &path) const
{
	qfLogFuncFrame();
	QStringList ret;
	keys_helper(this, path, ret);
	return ret;
}

QVariant QFPrototypedProperties::metaValue(const QStringList &path, const QString &key) const
{
	QVariant ret;
	do {
		Property p = d->dataRoot.cd(path);
		if(p.isValid()) {
			ret = p.metaValue(key);
			if(ret.isValid()) break;
		}
		foreach(QFPrototypedProperties *pp, d->prototypes) {
			ret = pp->metaValue(path, key);
			if(ret.isValid()) break;
		}
	} while(false);
	return ret;
}

QVariant QFPrototypedProperties::metaValue(const QString &path, const QString &key) const
{
	QStringList sl = QFString(path).splitAndTrim('/');
	return metaValue(sl, key);
}

void QFPrototypedProperties::setXmlTemplate_helper(const QDomNode &parent_nd, QFPrototypedProperties::Property &parent_property)
{
	qfLogFuncFrame() << "parent el:" << parent_nd.toElement().tagName();
	for(QDomElement el=parent_nd.firstChildElement(); !el.isNull(); el = el.nextSiblingElement()) {
		if(el.tagName() == "value") {
			parent_property.setMetaValue(Property::VALUE_KEY, stringToVariant(el.text()));
		}
		else if(el.tagName() == "default") {
			/// deprecated
			parent_property.setMetaValue(Property::VALUE_KEY, stringToVariant(el.text()));
		}
		else if(el.tagName() == "help") {
			parent_property.setHelp(el.text());
		}
		else if(el.tagName() == "options") {
			parent_property.setOptions(QFJson::stringToVariant(el.text()).toMap());
		}
		else {
			qfTrash() << "\t creating property for tag:" << el.tagName();
			Property p;
			if(el.hasAttribute("type")) p.setType(el.attribute("type"));
			if(el.hasAttribute("caption")) p.setCaption(el.attribute("caption"));
			if(el.hasAttribute("unit")) p.setUnit(el.attribute("unit"));
			setXmlTemplate_helper(el, p);
			parent_property.appendChild(el.tagName(), p);
		}
	}
}

void QFPrototypedProperties::setXmlTemplate(const QDomDocument &doc)
{
	qfLogFuncFrame();
	Property p;
	setXmlTemplate_helper(doc, p);
	d->dataRoot = p;
	qfTrash() << QFJson::variantToString(d->dataRoot);
}

static void dump_helper(const QFPrototypedProperties &pp, const QStringList &path, QString &ret)
{
	qfLogFuncFrame();// << "path:" << path;
	//QString ret;
	QString indentstr;
	indentstr.fill(' ', 2*(path.count()-1));
	ret += indentstr + '[' + path.value(path.count()-1) + ']';
	indentstr.fill(' ', 2*path.count());
	QString p_caption = pp.metaValue(path, QFPrototypedProperties::Property::CAPTION_KEY).toString();
	if(p_caption.count()) ret += '\n' + indentstr + "caption: " + p_caption;
	qfTrash() << "\t caption:" << p_caption;
	QString p_type = pp.metaValue(path, QFPrototypedProperties::Property::TYPE_KEY).toString();
	if(!p_type.isEmpty()) {
		ret += '\n' + indentstr + "type: " + p_type;
		ret += '\n' + indentstr + "value";
		int status;
		QVariant v = pp.value(path, QVariant(), &status);
		if(status == QFPrototypedProperties::PropertyNotFound) ret += " NOT FOUND";
		else {
			if(status & QFPrototypedProperties::PropertyOwnValue) ret += " OWN";
			if(status & QFPrototypedProperties::PropertyTemplateValue) ret += " TEMPL";
			if(status & QFPrototypedProperties::PropertyPrototypeValue) ret += " PROTO";
		}
		ret += ": ";
		ret += QFJson::variantToString(v);
		qfTrash() << "\t value:" << QFJson::variantToString(v);
	}
	ret += '\n';
	QVariantMap p_options = pp.metaValue(path, QFPrototypedProperties::Property::OPTIONS_KEY).toMap();
	if(!p_options.isEmpty()) {
		ret += indentstr;
		ret += "options:" + QFJson::variantToString(p_options);
		ret += '\n';
	}
	QString p_help = pp.metaValue(path, QFPrototypedProperties::Property::HELP_KEY).toString();
	if(!p_help.isEmpty()) {
		ret += indentstr;
		ret += "help:" + p_help;
		ret += '\n';
	}
	/*
	QStringList kl = pp.keys(path);
	//qfInfo() << kl.join(",");
	if(kl.count()) {
		//ret += indentstr + "children:\n";
		foreach(QString key, kl) {
			//qfTrash() << "\t dumping child:" << key << QFJson::variantToString(p2);
			QString path2 = key;
			if(!path.isEmpty()) path2 = path + '/' + key;
			ret += indentstr + '[' + key + ']' + dump_helper(path2, indent+2);
		}
	}
	*/
	//return ret;
}

QString QFPrototypedProperties::dump()
{
	qfLogFuncFrame();
	QString ret = "\n";
	qfPrototypedPropertiesWalk<QString&>(*this, QStringList(), dump_helper, ret);
	return ret;
}

