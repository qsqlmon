
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFPROTOTYPEDPROPERTIES_H
#define QFPROTOTYPEDPROPERTIES_H

#include<qfcoreglobal.h>

#include<QStringList>
#include<QDomDocument>
#include<QDomElement>
#include <QSharedData>
#include <QVariant>

//! Trida je sdilena explicitne, aby zmeny v prototypu byly citelne ve vsech objektech, ktere tento prototyp odkazuji
class QFCORE_DECL_EXPORT QFPrototypedProperties 
{
	public:
		enum GetValueFlags {PropertyNotFound=0, PropertyPrototypeValue=1, PropertyOwnValue=2, PropertyTemplateValue=4};
	public:
		class QFCORE_DECL_EXPORT Property : public QVariant
		{
			friend class QFPrototypedProperties;
			public:
				enum ValueFlags {ValueNotFound=0, ValueTemplate=1, ValueOwn=2};
				static const QString VALUE_KEY;
				static const QString META_KEY;
				static const QString CHILDREN_KEY;
				static const QString KEYS_KEY;
				
				static const QString TYPE_KEY;
				static const QString CAPTION_KEY;
				static const QString UNIT_KEY;
				static const QString HELP_KEY;
				static const QString OPTIONS_KEY;
			protected:
				void setMetaValue(const QString &key, const QVariant &val);
				QVariant metaValue(const QString &key) const {return toMap().value(META_KEY).toMap().value(key);}
			public:
				void appendChild(const QString &key, const Property &p);
				void setChild(const QString &key, const Property &p);
				Property cd(const QStringList &path) const;
			public:
				QVariant value(int *status = NULL) const;
				QString type() const {return metaValue(TYPE_KEY).toString();}
				QString caption() const {return metaValue(CAPTION_KEY).toString();}
				QString unit() const {return metaValue(UNIT_KEY).toString();}
				QString help() const {return metaValue(HELP_KEY).toString();}
				QVariantMap options() const {return metaValue(OPTIONS_KEY).toMap();}
				QStringList keys() const {return toMap().value(KEYS_KEY).toStringList();} /// seznam deti v poradi, jak byly pridavany
				QVariantMap children() const {return toMap().value(CHILDREN_KEY).toMap();}
				Property child(const QString &key) const;
				
				void setValue(const QVariant &val);
				//QMap<QString, TemplateProperty>& childrenRef() {return d->children;}
				void setType(const QString &v) {setMetaValue(TYPE_KEY, v);}
				void setCaption(const QString &v) {setMetaValue(CAPTION_KEY, v);;}
				void setUnit(const QString &v) {setMetaValue(UNIT_KEY, v);}
				void setHelp(const QString &v) {setMetaValue(HELP_KEY, v);}
				void setOptions(const QVariantMap &v) {setMetaValue(OPTIONS_KEY, v);}

				Property& operator=(const QVariant &other) {QVariant::operator=(other); return *this;}
			public:
				Property() : QVariant() {}
				Property(const QVariant &other) : QVariant(other) {}
		};
		#if 0
		class QFCORE_DECL_EXPORT TemplateProperty
		{
			public:
				enum Type {Null = 0, PathFragment, Text, File, Dir, Bool, Int, Float, List};
			private:
				struct Data : public QSharedData
				{
					Type type;
					QVariant value;
					QString caption;
					QString unit;
					QString help;
					QVariantMap options;
					QMap<QString, TemplateProperty> children;
				};
				QSharedDataPointer<Data> d;
				class SharedDummyHelper {};
				TemplateProperty(SharedDummyHelper);
				static const TemplateProperty& sharedNull();
			public:
				bool isNull() const {return d == sharedNull().d;}
				bool canHaveValue() const {return d->type > PathFragment;}
				const QMap<QString, TemplateProperty>& children() const {return d->children;}
				Type type() const {return d->type;}
				QVariant value() const {return d->value;}
				QString caption() const {return d->caption;}
				QString unit() const {return d->unit;}
				QString help() const {return d->help;}
				QVariantMap options() const {return d->options;}
				
				QMap<QString, TemplateProperty>& childrenRef() {return d->children;}
				void setType(Type t) {d->type = t;}
				void setValue(const QVariant &v) {d->value = v;}
				void setCaption(const QString &v) {d->caption = v;}
				void setUnit(const QString &v) {d->unit = v;}
				void setHelp(const QString &v) {d->help = v;}
				void setOptions(const QVariantMap &v) {d->options = v;}
			public:
				TemplateProperty();
		};
		typedef QMap<QString, TemplateProperty> TemplatePropertyMap;
		
		//typedef QVariant Property;
		typedef QVariantMap PropertyMap;
		#endif
	private:
		class Data : public QSharedData
		{
			public:
				Property dataRoot;
				QList<QFPrototypedProperties*> prototypes;
			private:
				/// je private v QSharedData
				Data& operator=(const Data &) {return *this;}
			public:
				Data();
				Data(const QFPrototypedProperties &proto);
				Data(const Data &other);
				~Data();
		};
		QExplicitlySharedDataPointer<Data> d;
		//class SharedDummyHelper {};
		//QFXmlProperties(SharedDummyHelper);
		//static const QFXmlProperties& sharedNull();
	public:
		static const QVariant IMPOSSIBLE_VALUE;
	protected:
		QVariant value_helper(const QFPrototypedProperties *pp, const QStringList &path, int &status) const;
		void keys_helper(const QFPrototypedProperties *pp, const QStringList &path, QStringList &found_keys) const;
		//QVariant ownValue_helper(const QStringList &path, bool &found) const;
		void setOwnValue_helper(QFPrototypedProperties::Property &p, const QStringList &sl, const QVariant &val);
		void setXmlTemplate_helper(const QDomNode &parent_nd, Property &parent_property);
		//QString dump_helper(const QStringList &_path, int indent);
		
		static QVariant stringToVariant(const QString &str);
		static QString variantToString(const QVariant &val);
	public:
		QStringList keys(const QString &path) const;
		QStringList keys(const QStringList &path) const;
		//bool isNull() const {return d->templ.isEmpty() && d->prototype == NULL;}
		void setXmlTemplate(const QDomDocument &doc);
		//void setData(const QVariantMap &m);
		void setPrototype(const QFPrototypedProperties &proto);
		void addPrototype(const QFPrototypedProperties &proto);
		
		//bool hasOwnValue(const QString &path) const;
		//bool hasValue(const QString &path) const;
		QVariant metaValue(const QString &path, const QString &key) const;
		QVariant metaValue(const QStringList &path, const QString &key) const;
		QVariant value(const QString &path, const QVariant &default_val = QVariant(), int *status = NULL) const;
		QVariant value(const QStringList &path, const QVariant &default_val = QVariant(), int *status = NULL) const;
		
		//void clearValue(const QString &path) {setValue(path, QVariant());}
		void setValue(const QString &path, const QVariant &val);

		QString dump();
	public:
		QFPrototypedProperties();
		/// int parametr je dummy, abych to odlisil od copy constructoru
		QFPrototypedProperties(int , const QFPrototypedProperties &prototype);
		virtual ~QFPrototypedProperties();
};

/*! \relates QFPrototypedProperties
projde rekurzivne vsechny nody properties a zavola pro ne statickou fci zadanou parametrem \a pmf.
priklad volani:
\code
static void dump_helper(const QFPrototypedProperties &pp, const QStringList &path, QString &ret);
qfPrototypedPropertiesWalk<QString&>(pp, QStringList(), dump_helper, ret);
\endcode
To, ze prochazi vsechny deti a ne \a root_nd , ma svuj smysl, kdyby se prochazel i root, spatne by se
implementoval pripad, kdy je to potreba udelat jen pro deti.
@param _path cesta kde se ma zacit prochazet
@see http://www.glenmccl.com/ptr_cmp.htm
*/
template<typename P>
void qfPrototypedPropertiesWalk(const QFPrototypedProperties &pp, const QStringList &_path, void (*pmf)(const QFPrototypedProperties &, const QStringList &, P), P param)
{
	QStringList kl = pp.keys(_path);
	//qfInfo() << kl.join(",");
	if(kl.count()) {
		//ret += indentstr + "children:\n";
		foreach(QString key, kl) {
			//qfTrash() << "\t dumping child:" << key << QFJson::variantToString(p2);
			QStringList path = _path;
			path << key;
			pmf(pp, path, param);
			qfPrototypedPropertiesWalk<P>(pp, path, pmf, param);
		}
	}
}

#if 0
/*! \relates QFPrototypedProperties
projde rekurzivne vsechny nody properties a zavola pro ne member fci zadanou parametrem \a pmf.
priklad volani: qfPrototypedPropertiesWalk(prototype, this, &MainWindow::prototypeWalk);
@see http://www.glenmccl.com/ptr_cmp.htm
*/
template<class T, typename P>
void qfPrototypedPropertiesWalk(const QFPrototypedProperties &pp, const QStringList &_path, T *obj, void (T::*pmf)(const QFPrototypedProperties &pp, const QStringList &, P), P param)
{
	QStringList path = _path;
	QStringList kl = pp.keys(path);
	//qfInfo() << kl.join(",");
	if(kl.count()) {
		//ret += indentstr + "children:\n";
		foreach(QString key, kl) {
			//qfTrash() << "\t dumping child:" << key << QFJson::variantToString(p2);
			path << key;
			(obj->*pmf)(pp, path, param);
			qfPrototypedPropertiesWalk(pp, path, obj, pmf, param);
		}
	}
}
#endif
Q_DECLARE_METATYPE(QFPrototypedProperties::Property);
#endif // QFPROTOTYPEDPROPERTIES_H

