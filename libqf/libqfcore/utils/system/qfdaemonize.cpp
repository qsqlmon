
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdaemonize.h"

//#include <qfexception.h>

//#include <stdexcept>

#include <qflogcust.h>

//=================================================
//                                    QFDaemonize
//=================================================
#if defined Q_OS_UNIX

#include <sys/stat.h>
#include <sys/resource.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>

static bool redirect_fd(int fd, const QString & file_name, int flags, mode_t mode = 0)
{
	QByteArray ba = file_name.toAscii();
	if(ba.isEmpty()) ba = "/dev/null";
	//QByteArray ba2 = "redirecting " + ba + " to fd " + QByteArray::number(fd) + '\n';
	//write(STDERR_FILENO, ba2.constData(), ba2.size());
	int file = open(ba.constData(), flags, mode);
	if(file < 0) {
		ba = "Error open: " + ba;
		perror(ba.constData());
		return false;
	}
	if(dup2(file, fd) < 0) {
		ba = "Error dup2() " + ba + " to fd " + QByteArray::number(fd);
		perror(ba.constData());
		return false;
	}
	if(close (file) < 0) {
		ba = "Error closing " + ba + " fd " + QByteArray::number(file);
		perror(ba.constData());
		return false;
	}
	//QByteArray ba2 = "redirecting " + ba + " to fd " + QByteArray::number(fd) + '\n';
	//write(STDERR_FILENO, ba2.constData(), ba2.size());
	return true;
}

void QFDaemonize::daemonize(const QString &dir,
			   const QString &stdinfile,
			   const QString &stdoutfile,
			   const QString &stderrfile)
{
	umask(0);
	/*
	rlimit rl;
	if (getrlimit(RLIMIT_NOFILE, &rl) < 0)
	{
		//can't get file limit
		perror("can't get file limit");
	}
	*/
	pid_t pid;
	if ((pid = fork()) < 0)
	{
		//Cannot fork!
		perror("Cannot fork!");
	} else if (pid != 0) { //parent
		exit(0);
	}
	
	setsid();

	QByteArray ba;
	ba = dir.toAscii();
	if (!dir.isEmpty() && chdir(ba.constData()) < 0) {
		// Oops we couldn't chdir to the new directory
		perror("Oops we couldn't chdir to the new directory");
	}
	/*
	if (rl.rlim_max == RLIM_INFINITY) {
		rl.rlim_max = 1024;
	}
	
	// Close all open file descriptors
	for (unsigned int i = 0; i < rl.rlim_max; i++) {
		close(i);
	}
	*/
	bool ok = false;
	do {
		if(!redirect_fd(STDIN_FILENO, stdinfile, O_RDONLY)) break;
		if(!redirect_fd(STDOUT_FILENO, stdoutfile, O_WRONLY|O_CREAT|O_APPEND, S_IRUSR|S_IWUSR)) break;
		if(!redirect_fd(STDERR_FILENO, stderrfile, O_WRONLY|O_CREAT|O_APPEND, S_IRUSR|S_IWUSR)) break;
		ok = true;
	} while(false);
	if(!ok) {
		QByteArray ba = "ERRORS occured EXITING with FAILURE\n";
		write(STDERR_FILENO, ba.constData(), ba.size());
		exit(EXIT_FAILURE);
	}
}
#else
void daemonize(const QString &dir,
			   const QString &stdinfile,
			   const QString &stdoutfile,
			   const QString &stderrfile)
{
	//QByteArray ba = "not supported on this platform\n";
	//write(STDERR_FILENO, ba.constData(), ba.size());
}
#endif
