#include <stdio.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netpacket/packet.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

int main(int argc, char *argv[])
{
  ifreq interface;
  ifconf interfaceConf;
  int sock;
  unsigned int count = 0;
  sockaddr_in *ipAddr;

  if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
  {
    perror("Problem se socket");
    return -1;
  }
  interfaceConf.ifc_req = NULL;
  do
  {
    interfaceConf.ifc_len = ++count * sizeof(ifreq);
    interfaceConf.ifc_req = (ifreq *)realloc(interfaceConf.ifc_req, interfaceConf.ifc_len);
    if (ioctl(sock, SIOCGIFCONF, &interfaceConf) == -1)
    {
      perror("Problém s ioctl");
      return -1;
    }
  } while (interfaceConf.ifc_len / sizeof(ifreq) == count);
  count = interfaceConf.ifc_len / sizeof(ifreq);
  printf("Počet rozhraní: %u\n", count);
  for(unsigned int i = 0; i < count; i++)
  {
	  interface = interfaceConf.ifc_req[i];
    printf("\nNázev rozhraní: %s\n", interfaceConf.ifc_req[i].ifr_name);
    ipAddr = (sockaddr_in *)&interfaceConf.ifc_req[i].ifr_addr;
    printf("IP adresa rozhraní: %s\n", inet_ntoa((in_addr)ipAddr->sin_addr));
    strncpy(interface.ifr_name, interfaceConf.ifc_req[i].ifr_name, IFNAMSIZ);
    if (ioctl(sock, SIOCGIFINDEX, &interface) == -1)
    {
      perror("Problém s ioctl");
    }
    printf("Index rozhraní: %d\n", interface.ifr_ifindex);
    if (ioctl(sock, SIOCGIFNETMASK, &interface) == -1)
    {
      perror("Problém s ioctl");
    }
    ipAddr = (sockaddr_in *)&interface.ifr_netmask;
    printf("Síťová maska: %s\n", inet_ntoa((in_addr)ipAddr->sin_addr));
    if (ioctl(sock, SIOCGIFFLAGS, &interface) == -1)
    {
      perror("Problém s ioctl");
    }
    if (interface.ifr_flags & IFF_UP)
    {
      printf("Rozhraní \"běží\" (je aktivováno).\n");
    } 
    else
    {
      printf("Rozhraní \"neběží\" (není aktivováno).\n");
    }
    if (interface.ifr_flags & IFF_LOOPBACK)
    {
      printf("Jedná se o zpětnou smyčku.\n");
    }
    if (interface.ifr_flags & IFF_POINTOPOINT)
    {
      printf("Jedná se o \"dvoubodový spoj\".\n");
    }
    if (interface.ifr_flags & IFF_PROMISC)
    {
      printf("Je v promiskuitním režimu.\n");
    }
    printf("\n");
  }
  free(interfaceConf.ifc_req);
  close(sock);
}
