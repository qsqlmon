
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFHWINFO_H
#define QFHWINFO_H

#include <qfcoreglobal.h>

#include <QList>
#include <QVariantMap>

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFHWInfo 
{
	public:
		/// v pripade uspechu vraci 0
		static QList<QVariantMap> getInterfacesInfo();
		static QString macAddressToString(const QByteArray &addr);
};

#endif // QFHWINFO_H

