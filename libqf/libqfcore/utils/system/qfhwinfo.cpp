
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfhwinfo.h"

#include <QStringList>
#include <QVariantMap>

#include <qflogcust.h>

#if defined Q_OS_UNIX

#include <stdio.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netpacket/packet.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <errno.h>

static void process_ifc_entry(int sock, ifreq interface, QVariantMap &entry)
{
    sockaddr_in *ip_addr;
    {
        entry["name"] = QString::fromAscii(interface.ifr_name);
        qfDebug() << "interface name:" << entry.value("name").toString();

        {
            //
            // Get the MAC address for this interface
            //
            if ( ioctl ( sock, SIOCGIFHWADDR, &interface ) != 0 ) {
                // We failed to get the MAC address for the interface
                qfDebug() << "Get iface HW address failed:" << entry.value("name").toString();
            }
            else {
                QByteArray ba ( ( const char * ) interface.ifr_ifru.ifru_hwaddr.sa_data, 6 );
                entry["hw-address"] = ba;
                qfDebug() << "\t HW address:" << QFHWInfo::macAddressToString(ba);
            }
        }
        
        ip_addr = ( sockaddr_in * ) &interface.ifr_addr;
        entry["ip"] = QString::fromAscii(inet_ntoa ( ( in_addr ) ip_addr->sin_addr));
        qfDebug() << "\t IP address:" << entry.value("ip").toString();
        
        strncpy ( interface.ifr_name, interface.ifr_name, IFNAMSIZ );
        if ( ioctl ( sock, SIOCGIFINDEX, &interface ) == -1 ) {
            qfDebug() << "ioctl error:" << strerror(errno);
        }
        else {
            entry["interface-index"] = interface.ifr_ifindex;
            qfDebug() << "\t interface index:" << entry.value("interface-index").toString();
        }
        if ( ioctl ( sock, SIOCGIFNETMASK, &interface ) == -1 ) {
            //qfError() << "ioctl error:" << strerror(errno);
        }
        else {
            ip_addr = ( sockaddr_in * ) &interface.ifr_netmask;
            entry["mask"] = QString::fromAscii(inet_ntoa ( ( in_addr ) ip_addr->sin_addr));
            qfDebug() << "\t network mask:" << entry.value("mask").toString();
        }
        if ( ioctl ( sock, SIOCGIFFLAGS, &interface ) == -1 ) {
            qfDebug() << "ioctl error:" << strerror(errno);
        }
        else {
            entry["active"] = ( interface.ifr_flags & IFF_UP ) != 0;
            qfDebug() << "\t active:" << entry.value("active").toString();

            entry["loopback"] = ( interface.ifr_flags & IFF_LOOPBACK ) != 0;
            qfDebug() << "\t loopback:" << entry.value("loopback").toString();

            entry["point-to-point"] = ( interface.ifr_flags & IFF_POINTOPOINT ) != 0;
            qfDebug() << "\t point-to-point:" << entry.value("point-to-point").toString();
            
            entry["promiscuit-mode"] = ( interface.ifr_flags & IFF_PROMISC ) != 0;
            qfDebug() << "\t promiscuit-mode:" << entry.value("promiscuit-mode").toString();
        }
    }
}

QList<QVariantMap> QFHWInfo::getInterfacesInfo()
{
    qfDebug() << QF_FUNC_NAME;
    QList<QVariantMap> ret;
    //QStringList found_ifaces;
    
    int sock;

    if ( ( sock = socket ( PF_INET, SOCK_DGRAM, IPPROTO_UDP ) ) == -1 ) {
        qfError() << "Open socket failed.";
        return ret;
    }
#if 0
    {
        /// tenhle zpusob najde wlan, ale nenajde odpojenou ethernetku
        ifreq interface;
        ifconf interface_conf;
        unsigned int count = 0;
        
        interface_conf.ifc_req = NULL;
        do {
            interface_conf.ifc_len = ++count * sizeof ( ifreq );
            interface_conf.ifc_req = ( ifreq * ) realloc ( interface_conf.ifc_req, interface_conf.ifc_len );
            if ( ioctl ( sock, SIOCGIFCONF, &interface_conf ) == -1 ) {
                qfError() << "ioctl error:" << strerror(errno);
                return ret;
            }
        } while ( interface_conf.ifc_len / sizeof ( ifreq ) == count );
        count = interface_conf.ifc_len / sizeof ( ifreq );
        //qfDebug() << "interface cnt:" << count;
        
        for ( unsigned int i = 0; i < count; i++ ) {
            QVariantMap entry;
            interface = interface_conf.ifc_req[i];
            process_ifc_entry(sock, interface, entry);
            ret << entry;
            found_ifaces << entry.value("name").toString();
        }
        free ( interface_conf.ifc_req );
    }
    #endif
    {
        /// tenhle zpusob najde vsechno
        struct ifreq iface_request; // Interface request
        struct if_nameindex *curr_ifc_name; // Ptr to interface name index
        struct if_nameindex *ifc_names; // Ptr to interface name index
        
        //
        // Initialize this function
        //
        curr_ifc_name = ( struct if_nameindex * ) NULL;
        ifc_names = ( struct if_nameindex * ) NULL;
        #ifndef SIOCGIFADDR
        // The kernel does not support the required ioctls
        qfError() << "The kernel does not support the required ioctls (SIOCGIFADDR)";
        return ret;
        #endif
        
        //
        // Obtain a list of dynamically allocated structures
        //
        curr_ifc_name = ifc_names = if_nameindex();
        
        //
        // Walk thru the array returned and query for each interface's
        // address
        //
        for(; * ( char * ) curr_ifc_name != 0; curr_ifc_name++ ) {
            //
            // Determine if we are processing the interface that we
            // are interested in
            //
            QString iface_name = curr_ifc_name->if_name;
            qfDebug() << "processing interface:" << iface_name;
            //if(found_ifaces.contains(iface_name)) continue;
            
            strncpy ( iface_request.ifr_name, curr_ifc_name->if_name, IF_NAMESIZE );
            QVariantMap entry;
            process_ifc_entry(sock, iface_request, entry);
            ret << entry;
        }
        
        //
        // Clean up things and return
        //
        if_freenameindex ( ifc_names );
    }
    close ( sock );
	return ret; 
}

#if 0
#include <stdio.h>
#include <string.h>
#include <net/if.h>
#include <sys/ioctl.h>

static QMap<QString, QByteArray> get_ifc_mac() {
    qfDebug() << QF_FUNC_NAME;
    QMap<QString, QByteArray> ret;
    int sock_desc; // Socket descriptor
    struct ifreq iface_request; // Interface request
    struct if_nameindex *curr_ifc_name; // Ptr to interface name index
    struct if_nameindex *ifc_names; // Ptr to interface name index

    //
    // Initialize this function
    //
    curr_ifc_name = ( struct if_nameindex * ) NULL;
    ifc_names = ( struct if_nameindex * ) NULL;
#ifndef SIOCGIFADDR
    // The kernel does not support the required ioctls
    qfError() << "The kernel does not support the required ioctls (SIOCGIFADDR)";
    return ret;
#endif

    //
    // Create a socket that we can use for all of our ioctls
    //
    sock_desc = socket ( PF_INET, SOCK_STREAM, 0 );
    if ( sock_desc < 0 ) {
        // Socket creation failed, this is a fatal error
        qfError() << "Open socket failed.";
        return ret;
    }

    //
    // Obtain a list of dynamically allocated structures
    //
    curr_ifc_name = ifc_names = if_nameindex();

    //
    // Walk thru the array returned and query for each interface's
    // address
    //
    for ( curr_ifc_name; * ( char * ) curr_ifc_name != 0; curr_ifc_name++ ) {
        //
        // Determine if we are processing the interface that we
        // are interested in
        //
        qfDebug() << "processing interface:" << curr_ifc_name->if_name;
        strncpy ( iface_request.ifr_name, curr_ifc_name->if_name, IF_NAMESIZE );

        //
        // Get the MAC address for this interface
        //
        if ( ioctl ( sock_desc, SIOCGIFHWADDR, &iface_request ) != 0 ) {
            // We failed to get the MAC address for the interface
            qfWarning() << "Get iface HW address failed:" << curr_ifc_name->if_name;
            continue;
        }
        QByteArray ba ( ( const char * ) iface_request.ifr_ifru.ifru_hwaddr.sa_data, 6 );
        ret[QString::fromLatin1 ( curr_ifc_name->if_name ) ] = ba;
    }

    //
    // Clean up things and return
    //
    if_freenameindex ( ifc_names );
    close ( sock_desc );
    return ret;
}
#endif
#if 0
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <linux/if.h>

QByteArray QFHWInfo::getMACAddress() {
    struct ifreq ifr;
    struct ifreq *IFR;
    struct ifconf ifc;
    char buf[1024];
    int s, i;
    int ok = 0;

    s = socket ( AF_INET, SOCK_DGRAM, 0 );
    if ( s==-1 ) {
        return QByteArray();
    }

    ifc.ifc_len = sizeof ( buf );
    ifc.ifc_buf = buf;
    ioctl ( s, SIOCGIFCONF, &ifc );

    IFR = ifc.ifc_req;
    for ( i = ifc.ifc_len / sizeof ( struct ifreq ); --i >= 0; IFR++ ) {
        strcpy ( ifr.ifr_name, IFR->ifr_name );
        if ( ioctl ( s, SIOCGIFFLAGS, &ifr ) == 0 ) {
            if ( ! ( ifr.ifr_flags & IFF_LOOPBACK ) ) {
                if ( ioctl ( s, SIOCGIFHWADDR, &ifr ) == 0 ) {
                    ok = 1;
                    break;
                }
            }
        }
    }

    close ( s );
    if ( ok ) {
        QByteArray addr;
        addr.clear();
        for ( int i=0; i<6; i++ ) {
            addr.append ( * ( ( char* ) ( ifr.ifr_hwaddr.sa_data ) + i ) );
        }
        //bcopy( ifr.ifr_hwaddr.sa_data, addr, 6);
        return addr;
    }
    return QByteArray();
}
#endif
#elif defined Q_OS_WIN32

// Link with Iphlpapi.lib
#include <winsock2.h>
#include <iphlpapi.h>
//#include <stdio.h>
//#include <stdlib.h>
//#pragma comment(lib, "IPHLPAPI.lib")

#define WORKING_BUFFER_SIZE 15000
#define MAX_TRIES 3

#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))

/* Note: could also use malloc() and free() */

QList<QVariantMap> QFHWInfo::getInterfacesInfo()
{
    QList<QVariantMap> ret;

    /* Declare and initialize variables */
    //DWORD dwSize = 0;
    DWORD dwRetVal = 0;

    //unsigned int i = 0;

    // Set the flags to pass to GetAdaptersAddresses
    ULONG flags = GAA_FLAG_INCLUDE_PREFIX;

    // default to unspecified address family (both)
    ULONG family = AF_UNSPEC;

    LPVOID lpMsgBuf = NULL;

    PIP_ADAPTER_ADDRESSES pAddresses = NULL;
    ULONG outBufLen = 0;
    ULONG Iterations = 0;

    PIP_ADAPTER_ADDRESSES pCurrAddresses = NULL;
    //PIP_ADAPTER_UNICAST_ADDRESS pUnicast = NULL;
    //PIP_ADAPTER_ANYCAST_ADDRESS pAnycast = NULL;
    //PIP_ADAPTER_MULTICAST_ADDRESS pMulticast = NULL;
    //IP_ADAPTER_DNS_SERVER_ADDRESS *pDnServer = NULL;
    //IP_ADAPTER_PREFIX *pPrefix = NULL;
    /*
    if (argc != 2) {
    printf(" Usage: getadapteraddresses family\n");
    printf("        getadapteraddresses 4 (for IPv4)\n");
    printf("        getadapteraddresses 6 (for IPv6)\n");
    printf("        getadapteraddresses A (for both IPv4 and IPv6)\n");
    exit(1);
    }

    if (atoi(argv[1]) == 4)
    family = AF_INET;
    else if (atoi(argv[1]) == 6)
    family = AF_INET6;

    printf("Calling GetAdaptersAddresses function with family = ");
    if (family == AF_INET)
    printf("AF_INET\n");
    if (family == AF_INET6)
    printf("AF_INET6\n");
    if (family == AF_UNSPEC)
    printf("AF_UNSPEC\n\n");
    */
    family = AF_INET;
    // Allocate a 15 KB buffer to start with.
    outBufLen = WORKING_BUFFER_SIZE;

    do {
        pAddresses = ( IP_ADAPTER_ADDRESSES * ) MALLOC ( outBufLen );
        if ( pAddresses == NULL ) {
            qfError() << "Memory allocation failed for IP_ADAPTER_ADDRESSES struct";
            return ret;
        }

        dwRetVal = GetAdaptersAddresses ( family, flags, NULL, pAddresses, &outBufLen );
        if ( dwRetVal == ERROR_BUFFER_OVERFLOW ) {
            FREE ( pAddresses );
            pAddresses = NULL;
        } else {
            break;
        }
        Iterations++;
    } while ( ( dwRetVal == ERROR_BUFFER_OVERFLOW ) && ( Iterations < MAX_TRIES ) );

    if ( dwRetVal == NO_ERROR ) {
        // If successful, output some information from the data we received
        pCurrAddresses = pAddresses;
        while ( pCurrAddresses ) {
            QVariantMap entry;
            entry["name"] = pCurrAddresses->AdapterName;
            if ( pCurrAddresses->PhysicalAddressLength != 0 ) {
                QByteArray ba;
                for ( int i=0; i< ( int ) pCurrAddresses->PhysicalAddressLength; i++ ) {
                    ba.append ( * ( ( char* ) ( pCurrAddresses->PhysicalAddress ) + i ) );
                }
                entry["hw-address"] = ba;
                break;
            }
            ret << entry;
            pCurrAddresses = pCurrAddresses->Next;
        }
    } else {
        if ( dwRetVal == ERROR_NO_DATA ) {
            //printf("\tNo addresses were found for the requested parameters\n");
        } else {
            qfError() << "Call to GetAdaptersAddresses failed with error:" << dwRetVal;

            if ( FormatMessage ( FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL,
                                 dwRetVal, MAKELANGID ( LANG_NEUTRAL, SUBLANG_DEFAULT ),
                                 // Default language
                                 ( LPTSTR ) & lpMsgBuf, 0, NULL ) ) {
                qfError() << "Error:" << lpMsgBuf;
                LocalFree ( lpMsgBuf );
            }
        }
    }
    if ( pAddresses ) {
        FREE ( pAddresses );
    }
    return ret;
}

#endif

QString QFHWInfo::macAddressToString ( const QByteArray & addr ) {
    QStringList sl;
    foreach ( char c, addr ) {
        QString s = QString::number ( ( int ) ( unsigned char ) c, 16 );
        if ( s.count() < 2 ) s = '0' + s;
        sl << s;
    }
    return sl.join ( ":" );
}
// kate: indent-mode cstyle; space-indent on; indent-width 4; 
