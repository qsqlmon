
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDAEMONIZE_H
#define QFDAEMONIZE_H

#include <qfcoreglobal.h>

#include <QString>

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFDaemonize
{
	public:
		//! daemonize the currently running programming
		//! Note: the calls to strerror are not thread safe, but that should not matter
		//! as the application is only just starting up when this function is called
		//! \param[in] dir which dir to ch to after becoming a daemon
		//! \param[in] stdinfile file to redirect stdin to, default "/dev/null" 
		//! \param[in] stdoutfile file to redirect stdout from, default "/dev/null"
		//! \param[in] stderrfile file to redirect stderr to, default "/dev/null"
		static void daemonize(const QString &dir = "/",
					   const QString &stdinfile = QString(),
						const QString &stdoutfile = QString(),
						const QString &stderrfile = QString());
};

#endif // QFDAEMONIZE_H

