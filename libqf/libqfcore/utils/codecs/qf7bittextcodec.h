
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QF7BITTEXTCODEC_H
#define QF7BITTEXTCODEC_H

#include <qfcoreglobal.h>

#include <QTextCodec>


//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QF7BitTextCodec : public QTextCodec
{
	public:
		QF7BitTextCodec() {}
		virtual ~QF7BitTextCodec() {}

		virtual QByteArray name() const {return QByteArray("7BIT");}
		virtual QList<QByteArray> aliases() const;
		virtual int mibEnum() const {return 30000;}
		virtual QString convertToUnicode(const char *chars, int len, ConverterState *state) const ;
		virtual QByteArray fromUnicode(const QString &str) const;

		static char toAscii7(const QChar &unicode_char);
		static QByteArray toAscii7(const QString &unicode_str);
};
   
#endif // QF7BITTEXTCODEC_H

