
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qf7bittextcodec.h"

#include <qflogcust.h>

QList<QByteArray> QF7BitTextCodec::aliases() const
{
	static QList<QByteArray> ret;
	if(ret.isEmpty()) ret << QByteArray("ASCII-7");
	return ret;
}

QString QF7BitTextCodec::convertToUnicode(const char *chars, int len, ConverterState *) const
{
	QString s;
	s.reserve(len);
	for(int i=0; i<len; i++) s += QChar(chars[i]);
	return s;
}

QByteArray QF7BitTextCodec::fromUnicode(const QString &str) const
{
	return toAscii7(str);
}

char QF7BitTextCodec::toAscii7(const QChar &unicode_char)
{
	static QMap<ushort, uchar> code_table;
	if(code_table.isEmpty()) {
		code_table[0x00C1] = 'A'; /// A'
		code_table[0x00E1] = 'a'; /// a'
		code_table[0x010C] = 'C'; /// C^
		code_table[0x010D] = 'c'; /// c^
		code_table[0x010E] = 'D'; /// D^
		code_table[0x010F] = 'd'; /// d^
		code_table[0x00C9] = 'E'; /// E'
		code_table[0x00E9] = 'e'; /// e'
		code_table[0x011A] = 'E'; /// E^
		code_table[0x011B] = 'e'; /// e^
		code_table[0x00CD] = 'I'; /// I'
		code_table[0x00ED] = 'i'; /// i'
		code_table[0x0143] = 'N'; /// N^
		code_table[0x148] = 'n'; /// n^
		code_table[0x00D3] = 'O'; /// O'
		code_table[0x00F3] = 'o'; /// o'
		code_table[0x0158] = 'R'; /// R^
		code_table[0x0159] = 'r'; /// r^
		code_table[0x0160] = 'S'; /// S^
		code_table[0x0161] = 's'; /// s^
		code_table[0x0164] = 'T'; /// T^
		code_table[0x0165] = 't'; /// t^
		code_table[0x00DA] = 'U'; /// U'
		code_table[0x00FA] = 'u'; /// u'
		code_table[0x016E] = 'U'; /// U*
		code_table[0x016F] = 'u'; /// u*
		code_table[0x00DD] = 'Y'; /// Y'
		code_table[0x00FD] = 'y'; /// y'
		code_table[0x017D] = 'Z'; /// Z^
		code_table[0x017E] = 'z'; /// z^
	}
	char c = code_table.value(unicode_char.unicode(), 0);
	if(c == 0) c = unicode_char.toAscii();
	return c;
}

QByteArray QF7BitTextCodec::toAscii7(const QString &unicode_str)
{
	int len = unicode_str.length();
	QByteArray ret;
	ret.reserve(len);
	for(int i=0; i<len; i++) {
		ret += toAscii7(unicode_str[i]);
	}
	return ret;
}
