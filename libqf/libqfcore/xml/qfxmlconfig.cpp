
#include "qfxmlconfig.h"

#include <qffileutils.h>
#include <qfappinterface.h>
#include <qfsqlquerytable.h>
#include <qfsqlquery.h>
#include <qfappdbconnectioninterface.h>

#include <QFile>
#include <QDir>
#include <QCoreApplication>

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

//================================================================
//                                              QFXmlConfig
//================================================================
const QString QFXmlConfig::AppInternalsPath = "/app_internals";

QFXmlConfig::QFXmlConfig(const QFDomDocument &template_document, const QFDomDocument &data_document)
	: f_templateDoc(template_document), f_dataDoc(data_document)
{
}

QFXmlConfig::~QFXmlConfig()
{
	//SAFE_DELETE(configLoader);
}

void QFXmlConfig::setConfigLoader(QFXmlConfigLoader *loader)
{
	//qfWarning() << QF_FUNC_NAME << "configLoader:" << configLoader;
	//qfWarning() << "\t new configLoader:" << loader;
	//SAFE_DELETE(configLoader);
	f_configLoader = loader;
}

QFXmlConfigLoader * QFXmlConfig::configLoader(bool throw_exc) throw( QFException )
{
	if(!f_configLoader && throw_exc) QF_EXCEPTION("Loader is NULL.");
	return f_configLoader;
}

QFXmlConfigElement QFXmlConfig::createPersistentPath(const QString &xml_config_persistent_id)
{
	//qfLogFuncFrame();
	QFXmlConfigElement el = createAppInternalsPath();
	//qfInfo() << "mkcd" << xml_config_persistent_id;
	el = el.mkcd(xml_config_persistent_id);
	return el;
}

QFXmlConfigElement QFXmlConfig::persistentPath(const QString &xml_config_persistent_id)
{
	QFXmlConfigElement el = createAppInternalsPath();
	return el.cd(xml_config_persistent_id, !Qf::ThrowExc);
}

QFXmlConfigElement QFXmlConfig::createAppInternalsPath()
{
	//qfInfo() << "mkcd" << AppInternalsPath;
	QFXmlConfigElement el = dataDocument().mkcd(AppInternalsPath);
	el.setAttribute("hidden", "1");
	//setDocumentDirty(true);
	return el;
}

QFXmlConfigElement QFXmlConfig::appInternalsPath() const
{
	QFXmlConfigElement el = templateDocument().cd(AppInternalsPath, !Qf::ThrowExc);
	return el;
}

void QFXmlConfig::setTemplateDocument(const QFXmlConfigDocument & template_document)
{
	f_templateDoc = template_document;
	f_templateDoc.expandPrototypes();
	//qfInfo() << "expanded template:" << f_templateDoc.toString();
	clearData();
}

void QFXmlConfig::setTemplateDocument(const QString & xml_str) throw( QFException )
{
	qfTrash() << QF_FUNC_NAME;
	QFXmlConfigDocument doc;
	doc.setContent(xml_str);
	setTemplateDocument(doc);
	clearData();
}

void QFXmlConfig::setDataDocument(const QString & xml_str) throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	QFXmlConfigDocument doc;
	doc.setContent(xml_str);
	setDataDocument(doc);
}

QString QFXmlConfig::version() const
{
	QFDomElement el = templateDocument().firstChildElement();
	return el.attribute("version");
}

/*
QVariant QFXmlConfig::value(const QFXmlConfigElement &template_el) const
{
	qfTrash() << QF_FUNC_NAME << template_el.path();
	QFXmlConfigElement el = dataDocument().cd(template_el.path(), !Qf::ThrowExc);
	if(el.hasValue()) return decryptValue(template_el, el.value());
	return decryptValue(template_el, template_el.value());
}
*/
/*
QVariant QFXmlConfig::value(const QString & path) const
{
	QFXmlConfigElement data_el = dataDocument().cd(path, !Qf::ThrowExc);
	QFXmlConfigElement template_el = templateDocument().cd(path, !Qf::ThrowExc);
	if(!!data_el) return decryptValue(template_el, data_el.value());
	if(!!template_el) return decryptValue(template_el, template_el.value());
	return QVariant();
}
*/

bool QFXmlConfig::isPathValid(const QString & path) const
{
	qfLogFuncFrame() << path;
	bool ret = false;
	QFXmlConfigElement template_el = templateDocument().cd(path, !Qf::ThrowExc);
	if(!!template_el) {
		ret = true;
	}
	return ret;
}

static QString VALUE = "value";

QVariant QFXmlConfig::valuesToVariant_helper(const QFDomElement &t_el) const
{
	QVariantMap ret;
	QFXmlConfigElement template_el(t_el);
	if(template_el.type() > QFXmlConfigElement::PathFragment) {
		ret[VALUE] = value(template_el.path());
	}
	for(QFDomElement el=t_el.firstChildElement(); !el.isNull(); el=el.nextSiblingElement()) {
		if(QFXmlConfigElement::isServiceElement(el)) {
		}
		else {
			ret[el.tagName()] = valuesToVariant_helper(el);
		}
	}
	return ret;
}

QVariant QFXmlConfig::valuesToVariant() const
{
	QFXmlConfigElement template_el = templateDocument().cd("/", !Qf::ThrowExc);
	//qfInfo() << template_el.toString();
	return valuesToVariant_helper(template_el);
}

void QFXmlConfig::toTable_helper(const QFDomElement &t_el, QFBasicTable &table, const ToTableOptions &opts) const
{
	qfLogFuncFrame() << t_el.path();
	//static QString not_found_data_value = "<QF_NOT_FOUND_DATA_VALUE>";
	QFXmlConfigElement template_el(t_el);
	if(template_el.type() > QFXmlConfigElement::PathFragment) {
		QVariant val;
		QVariant template_val = template_el.value(QVariant());
		if(opts.dirtyValuesOnly()) {
			QFXmlConfigElement data_el = dataDocument().cd(template_el.path(), !Qf::ThrowExc);
			val = data_el.value(QVariant());
			qfTrash() << "\t data value:" << val.toString() << "is valid:" << val.isValid();
			/// pokud je hodnota stejna jako v templatu, vyhod ji
			if(val == template_val) val = QVariant();
		}
		qfTrash() << "\t element:" << template_el.tagName() << "value:" << val.toString() << "is valid:" << val.isValid();
		if(val.isValid()) {
			if(template_el.type() == QFXmlConfigElement::Bool) {
				bool b = val.toBool();
				val = (b)? opts.trueValueString(): opts.falseValueString();
			}
			QFBasicTable::Row &r = table.appendRow();
			QString path = template_el.path();
			r.setValue("path", path);
			r.setValue("value", val.toString());
			r.setValue("type", template_el.attribute("type"));
			r.setValue("unit", template_el.unit());
			QStringList sl_captions;
			QFXmlConfigElement el1 = template_el;
			while(!el1.isNull()) {
				sl_captions.prepend(el1.caption());
				el1 = QFDomElement(el1.parentNode().toElement());
			}
			if(!sl_captions.isEmpty()) {
				/// vymaz jmeno prvniho z listu, to je root node, ten neni v path uveden
				sl_captions.removeFirst();
			}
			r.setValue("captions", sl_captions.join(opts.captionSeparator()));
			r.post();
		}
	}
	for(QFDomElement el=template_el.firstChildElement(); !el.isNull(); el=el.nextSiblingElement()) {
		if(QFXmlConfigElement::isServiceElement(el)) {
			continue;
		}
		else if(el.tagName() == VALUE) {
			continue;
		}
		else {
			toTable_helper(el, table, opts);
		}
	}
}

QFBasicTable QFXmlConfig::toTable(const ToTableOptions &opts) const
{
	QStringList sl = QStringList() << "path" << "captions" << "value" << "type" << "unit";
	QFBasicTable ret(sl);
	QFXmlConfigElement template_el = templateDocument().cd("/", !Qf::ThrowExc);
	//qfInfo() << dataDocument().toString();
	toTable_helper(template_el, ret, opts);
	return ret;
}

QVariant QFXmlConfig::value(const QFXmlConfigElement &template_el, const QVariant &default_value) const
{
	qfTrash() << QF_FUNC_NAME << template_el.path() << "default_value:" << default_value.toString();
	QFXmlConfigElement data_el = dataDocument().cd(template_el.path(), !Qf::ThrowExc);
	qfTrash() << "\t data path:" << data_el.path();
	qfTrash() << "\t data_el:" << data_el.toString();
	QVariant ret = default_value;
	if(!!data_el) {
		qfTrash() << "\t nalezeno v datech";
		ret = decryptValue(template_el, data_el.value(default_value));
	}
	else if(!!template_el) {
		qfTrash() << "\t nalezeno v template";
		ret = decryptValue(template_el, template_el.value(default_value));
	}
	else {
		qfTrash() << "\t default";
	}
	qfTrash() << "\t return:" << ret.toString();
	return ret;
	//return decryptValue(template_el, el.value(default_value));
}

QVariant QFXmlConfig::value(const QString & path, const QVariant & default_value) const
{
	qfLogFuncFrame() << path;
	QVariant ret = default_value;
	QFXmlConfigElement template_el = templateDocument().cd(path, !Qf::ThrowExc);
	QFXmlConfigElement data_el = dataDocument().cd(path, !Qf::ThrowExc);
	do {
		if(!!data_el) {
			QFDomElement el_value = data_el.firstChildElement(VALUE);
			if(!el_value.isNull()) {
				qfTrash() << "\t nalezeno v datech";
				ret = decryptValue(template_el, data_el.value(default_value));
				break;
			}
		}
		if(!!template_el) {
			qfTrash() << "\t nalezeno v template";
			ret = decryptValue(template_el, template_el.value(default_value));
			break;
		}
		qfTrash() << "\t default";
	} while(false);
	qfTrash() << "\t return:" << ret.toString();
	return ret;
}

QString QFXmlConfig::valueUnit(const QString & path, const QString & default_unit) const
{
	qfLogFuncFrame() << path;
	QString ret = default_unit;
	QFXmlConfigElement template_el = templateDocument().cd(path, !Qf::ThrowExc);
	if(!!template_el) {
		qfTrash() << "\t nalezeno v template";
		ret = template_el.unit();
	}
	return ret;
}

void QFXmlConfig::valuesFromVariant_helper(const QVariantMap &vals, const QString &path)
{
	QMapIterator<QString, QVariant> it(vals);
	while (it.hasNext()) {
		it.next();
		QString key = it.key();
		if(key == VALUE) {
			setValue(path, it.value());
		}
		else {
			QString p = path + '/' + key;
			valuesFromVariant_helper(it.value().toMap(), p);
		}
	}
}

void QFXmlConfig::valuesFromVariant(const QVariant &v)
{
	valuesFromVariant_helper(v.toMap(), QString());
}

void QFXmlConfig::setValue(const QFXmlConfigElement &template_el, const QVariant &val)
{
	qfLogFuncFrame() << template_el.path();
	if(!template_el) return;

	bool changed = false;
	QVariant template_val = decryptValue(template_el, template_el.value(QVariant()));
	qfTrash() << "\t template val:" << template_val.toString() << "new val:" << val.toString();
	if(val == template_val) {
		/// vymaz v datech tento zaznam
		QFXmlConfigElement data_el = dataDocument().cd(template_el.path(), !Qf::ThrowExc);
		if(!data_el.isNull()) {
			qfTrash() << "\t deleting element <value> on path:" << data_el.path();
			QFDomElement el_value = data_el.firstChildElement(VALUE);
			if(!el_value.isNull()) {
				data_el.removeChild(el_value);
				qfTrash() << "\t DELETED";
				changed= true;
			}
		}
	}
	else {
		QFXmlConfigElement el_data = dataDocument().mkcd(template_el.path());
	//el_data.setAttribute("type", template_el.attribute("type"));
		QFXmlConfigElement el_template_parent = QFDomElement(template_el.parentNode().toElement());
		if(el_template_parent.childrenHasExclusiveCheck() && val.toBool()) {
			qfTrash().color(QFLog::Yellow) << "\tswitch others off";
		//QFXmlConfigElement el_data_parent = QFDomElement(el_data.parentNode().toElement());
		//QFXmlConfigElement el_data_parent = dataDocument().cd(el_template_parent.path(), !Qf::ThrowExc);
			for(QFDomElement el1 = el_template_parent.firstChildElement(); !!el1; el1 = el1.nextSiblingElement()) {
				QFXmlConfigElement el2 = el1;
				if(QFXmlConfigElement::isServiceElement(el2)) continue;
				qfTrash().color(QFLog::Yellow) << "\t\telement::" << el2.tagName() << "type=" << el2.attribute("type");
				if(el2.type() == QFXmlConfigElement::Bool) {
					qfTrash().color(QFLog::Yellow) << "\t\tswitch off:" << el2.tagName();
					bool b = false;
					QFXmlConfigElement el3 = dataDocument().cd(el2.path(), !Qf::ThrowExc);
					if(!!el3) el3.setValue(false, &b);
					changed = changed || b;
				}
			}
		}
		{
			bool b;
			el_data.setValue(cryptValue(template_el, val), &b);
			changed = changed || b;
		}
		qfTrash().color(QFLog::Yellow) << "\t" << el_data.toString();
	}
	if(changed) setDataDirty(true);
	qfTrash() << "\t data dirty:" << isDataDirty();
}

void QFXmlConfig::setValue(const QString & path, const QVariant & val)
{
	qfLogFuncFrame() << "path:" << path << "value:" << val.toString();
	QFXmlConfigElement template_el = templateDocument().cd(path, !Qf::ThrowExc);
	if(!!template_el) {
		qfTrash() << template_el.path() << "=" << QFXmlConfigElement::variantToString(val);
		setValue(template_el, val);
	}
	else {
		/// asi to budou persistent data, ktery pochopitelne nemaji v template protejsek
		if(!path.startsWith(AppInternalsPath)) qfWarning() << "Path" << path << "is neither from template nor a persistent path" << AppInternalsPath;
		QFXmlConfigElement data_el = dataDocument().mkcd(path);
		bool b;
		data_el.setValue(val, &b);
		qfTrash() << data_el.path() << "=" << QFXmlConfigElement::variantToString(val) << "dirty:" << b;
		if(b) setDataDirty(b);
	}
}

static bool dataLint_deleteEmptyElements(const QFDomElement &el_parent)
{
	bool ret = false;
	QList<QDomNode> kill_list;
	for(QFDomElement el = el_parent.firstChildElement(); !!el; el = el.nextSiblingElement()) {
		if(el.tagName() == VALUE) ret = true;
		else {
			bool has_val = dataLint_deleteEmptyElements(el);
			if(!has_val) kill_list << el;
			ret = ret || has_val; /// pokud ma alespon jedno dite hodnotu, nesmi se rodic vymazat
		}
	}
	//qfInfo() << el_parent.path() << "has value:" << ret;
	QDomNode parent_nd = el_parent;
	foreach(QDomNode nd, kill_list) {
		//qfInfo() << "\t removing:" << nd.toElement().tagName();
		parent_nd.removeChild(nd);
	}
	return ret;
}

void QFXmlConfig::dataLint()
{
	qfLogFuncFrame();
	dataLint_deleteEmptyElements(dataDocument().firstChildElement());
}

bool QFXmlConfig::load() throw(QFException)
{
	qfLogFuncFrame();
	return configLoader()->load(this);
}

void QFXmlConfig::save() throw(QFException)
{
	qfLogFuncFrame() << "data dirty:" << isDataDirty();
	if(isDataDirty()) {
		dataLint();
		configLoader()->save(this);
	}
}

void QFXmlConfig::clearData()
{
	QFXmlConfigDocument doc;
	setDataDocument(doc);
}

void QFXmlConfig::restoreDefaultData_walk(const QFDomElement & parent_el)
{
	static QString app_internals = AppInternalsPath.mid(1); /// cut leading '/'
	for(QFDomElement el=parent_el.firstChildElement(); !!el; el=el.nextSiblingElement()) {
		if(el.tagName() == "value") {
			QFXmlConfigElement el1 = templateDocument().cd(parent_el.path(), !Qf::ThrowExc);
			if(!!el1) {
				QFXmlConfigElement el2 = parent_el;
				el2.setValue(el1.value(QVariant()));
			}
		}
		else if(QFXmlConfigElement::isServiceElement(el)) {
		}
		else if(el.tagName() == app_internals && el.path().startsWith(AppInternalsPath)) { /// && je dobre, tag se musi jmenovat app_internals a musi to byt element v korenovem adresari
		}
		else {
			restoreDefaultData_walk(el);
		}
	}
}

void QFXmlConfig::restoreDefaultData()
{
	restoreDefaultData_walk(dataDocument().rootElement());
}

void QFXmlConfig::detachTemplate()
{
	f_templateDoc = f_templateDoc.clone();
}

void QFXmlConfig::detachData()
{
	f_dataDoc = f_dataDoc.clone();
}

QVariant QFXmlConfig::cryptValue(const QFXmlConfigElement &el, const QVariant &val) const
{
	if(el.type() == QFXmlConfigElement::Text) {
		if(el.options().attribute("crypted", false).toBool()) {
			return QString(f_crypter.crypt(val.toString()));
		}
	}
	return val;
}

QVariant QFXmlConfig::decryptValue(const QFXmlConfigElement &el, const QVariant &val) const
{
	QVariant ret = val;
	QFXmlConfigElement::Type type = el.type();
	if(type == QFXmlConfigElement::Text) {
		if(el.options().attribute("crypted", false).toBool()) {
			//qfInfo() << "crypted:" << val.toString().toAscii();
			QString s = f_crypter.decrypt(val.toString().toAscii());
			//qfInfo() << "decrypted:" << s;
			ret = s;
		}
	}
	else if(type == QFXmlConfigElement::Int) {
		ret = ret.toInt();
	}
	else if(type == QFXmlConfigElement::Float) {
		ret = ret.toDouble();
	}
	else if(type == QFXmlConfigElement::Bool) {
		ret = ret.toBool();
	}
	return ret;
}

//================================================================
//                                              QFXmlConfigLoader
//================================================================
/*
QFXmlConfigLoader::~QFXmlConfigLoader()
{
	//qfWarning() << QF_FUNC_NAME << "configLoader:" << this;
}
*/
//================================================================
//                                              QFXmlConfigFileLoader
//================================================================
QString& QFXmlConfigFileLoader::configLoadingStatus()
{
	static QString s;
	return s;
}

static QString app_name()
{
	return QFCompat::appName();
}

static QString app_config_name()
{
	return app_name() + ".conf.xml";
}

QString QFXmlConfigFileLoader::resourceConfigName()
{
	qfTrash() << QF_FUNC_NAME;
	QString config_name = app_config_name();
	//QString config_path = QFFileUtils::joinPath("." + app_name(), config_name);
	QString s = QFFileUtils::joinPath(":", config_name);
	qfTrash() << "ret:" << s;
	return s;
}

QString QFXmlConfigFileLoader::resourceConfigVersion() throw(QFException)
{
	QFile f;
	f.setFileName(resourceConfigName());
	QFXmlConfigDocument doc;
	doc.setContent(f);
	QFXmlConfig cf(doc);
	return cf.version();
}

QString QFXmlConfigFileLoader::accessibleTemplateConfigFileName(const QString &config_file_name)
{
	qfTrash() << QF_FUNC_NAME;
	QStringList err;
	QFString fs;
	do {
		QFile f;
		QString config_name = config_file_name;
		if(config_name.isEmpty()) config_name = app_config_name();
		//QString config_path = QFFileUtils::joinPath("." + app_name(), config_name);

		/// pokus se najit jmeno a umisteni
		///  currdir
		fs = QFFileUtils::joinPath(QFFileUtils::currDir(), config_name);
		qfTrash() << "\t at currDir ?" << fs;
		f.setFileName(fs);
		err << fs;
		if(f.exists()) break;

		/// potom v adresari aplikace
		fs = QCoreApplication::applicationDirPath();
		fs = QFFileUtils::joinPath(fs, config_name);
		qfTrash() << "\t at applicationDirPath ?" << fs;
		f.setFileName(fs);
		err << fs;
		if(f.exists()) break;

		/// potom v resourcech
		fs = resourceConfigName();
		qfTrash() << "\t at resourceConfigName ?" << fs;
		f.setFileName(fs);
		err << fs;
		if(f.exists()) break;

		fs = QString();
	} while(0);
	configLoadingStatus() = err.join("\n");
	//if(fs.isEmpty()) qfWarning() << "findAccessibleConfigFileName(): none of folowing config files fount\n" << configLoadingStatus();
	qfTrash() << "\t returning:" << fs;// << "at" << configLoadingStatus();
	return fs;
}
/*
QString QFXmlConfigFileLoader::accessibleDataConfigFileName(const QString config_file_name)
{
	qfTrash() << QF_FUNC_NAME;
	QStringList err;
	QFString fs;
	do {
		QFile f;
		QString config_name = config_file_name;
		if(config_name.isEmpty()) config_name = app_config_name();
		//QString config_path = QFFileUtils::joinPath("." + app_name(), config_name);

		/// pokus se najit jmeno a umisteni
		///doma
		fs = QFFileUtils::joinPath(qfApp()->settingsDir(), config_name);
		qfTrash() << "\t at home ?" << fs;
		f.setFileName(fs);
		err << fs;
		if(f.exists()) break;

		fs = QString();
	} while(0);
	configLoadingStatus() = err.join("\n");
	//if(fs.isEmpty()) qfWarning() << "findAccessibleConfigFileName(): none of folowing config files fount\n" << configLoadingStatus();
	qfTrash() << "\t returning:" << fs;// << "at" << configLoadingStatus();
	return fs;
}
*/
bool QFXmlConfigFileLoader::load(QFXmlConfig *doc, bool throw_exc) throw(QFException)
{
	qfLogFuncFrame();
	QF_ASSERT(doc, "config is NULL");
	/// nahrej config
	QFString fs = f_templateFileName;
	if(!fs) fs = accessibleTemplateConfigFileName();
	QFile f(fs);
	qfTrash() << "\t opening:" << f.fileName();
	if(!f.exists()) {
		//qfWarning() << "resources:";
		//QStringList sl = QDir(":/").entryList();
		//foreach(QString s, sl) qfWarning() << "\t" << s;
		if(throw_exc) QF_EXCEPTION(tr("Error open configuration template '%1'.\n%2").arg(fs).arg(configLoadingStatus()));
		return false;
	}
	QFXmlConfigDocument d;
	d.setContent(f);
	doc->setTemplateDocument(d);
	return true;
}

void QFXmlConfigFileLoader::save(QFXmlConfig *doc) throw(QFException)
{
	Q_UNUSED(doc);
	QF_EXCEPTION(QF_FUNC_NAME + QString(" not implemented yet."));
	/*!
	Zatim neni implementovano, musel by se zmergeovat obsah template a dat a ulozit to jako novy template, nevim, jestli by to vubec k necemu bylo
	*/
}

//================================================================
//                                              QFXmlConfigSplittedFileLoader
//================================================================
QString QFXmlConfigSplittedFileLoader::accessibleTemplateConfigFileName(const QString & config_file_name)
{
	QFString fs = config_file_name;
	if(!fs) fs = resourceConfigName();
	QStringList err;
	err << fs;
	configLoadingStatus() = err.join("\n");
	return fs;
}

QString QFXmlConfigSplittedFileLoader::dataFileNameWithPath()
{
	QFString config_name = f_dataFileName;
	if(config_name.isEmpty() || config_name.indexOf('/') < 0) {
		/// pokud je jmeno prazdne nebo neobsahuje cestu
		/// hledej ho na znamych adresach, pripadne s defaultnim jmenem
		if(config_name.isEmpty()) config_name = app_config_name();
		QString settings_dir;
		{
			QCoreApplication *core_app = QCoreApplication::instance();
			QFAppInterface *ifc = dynamic_cast<QFAppInterface*>(core_app); /// cross cast
			if(ifc) settings_dir = ifc->settingsDir();
			//QF_EXCEPTION("No QFAppConfigInterface");
		}
		config_name = QFFileUtils::joinPath(settings_dir, config_name);
	}
	return config_name;
}

bool QFXmlConfigSplittedFileLoader::load(QFXmlConfig * doc, bool throw_exc) throw( QFException )
{
	qfLogFuncFrame();
	bool ret = QFXmlConfigFileLoader::load(doc, throw_exc);
	if(!ret) return ret;

	/// nahrej data
	QFXmlConfigDocument xml_doc;
	QFString s = dataFileNameWithPath();
	if(QFile::exists(s)) {
		QFile f(s);
		if(f.open(QIODevice::ReadOnly)) {
			xml_doc.setContent(f);
		}
		else {
			if(throw_exc) QF_EXCEPTION(QObject::tr("Can not open configuration file '%1' for reading.").arg(f.fileName()));
		}
	}
	doc->setDataDocument(xml_doc);
	return true;
}

void QFXmlConfigSplittedFileLoader::save(QFXmlConfig * doc) throw( QFException )
{
	qfTrash() << QF_FUNC_NAME;
	QFString fs = dataFileNameWithPath();
	QFString path = QFFileUtils::path(fs);
	if(!QFFileUtils::ensurePath(path)) QF_EXCEPTION(QObject::tr("Cann't create configuration directory '%1'").arg(path));

	QFile f(fs);
	if(!f.open(QIODevice::WriteOnly))
		QF_EXCEPTION(QObject::tr("Can not open configuration file '%1' for writing.").arg(f.fileName()));
	qfInfo() << "QFXmlConfig: saving file: " << f.fileName();
	doc->dataDocument().save(f);
}

//================================================================
//                                              QFDbXmlConfigLoader
//================================================================
QFDbXmlConfigLoader::QFDbXmlConfigLoader(QObject *parent)
	: QFXmlConfigLoader(parent)
{
	setDataFieldName("dbxml");
	//setTemplateFieldName("dbxml");
}

bool QFDbXmlConfigLoader::load(QFXmlConfig *doc, bool throw_exc) throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	QF_ASSERT(doc, "config is NULL");
	QFSqlQuery q(QFDbXmlConfigLoader::appConnection());
	if(doc->templateDocument().isEmpty()) {
		q.exec(templateQuery());
		if(q.next()) {
			QString s = q.value(0).toString();
			QFXmlConfigDocument xml_doc;
			xml_doc.setContent(s);
			doc->setTemplateDocument(xml_doc);
		}
		else {
			if(throw_exc) QF_EXCEPTION(tr("Template query exec error.\n %1").arg(templateQuery()));
			return false;
		}
	}
	{
		q.exec(dataQuery());
		if(q.next()) {
			QString s = q.value(dataFieldName()).toString();
			QFXmlConfigDocument xml_doc;
			xml_doc.setContent(s);
			doc->setDataDocument(xml_doc);
		}
		else {
			if(throw_exc) QF_EXCEPTION(tr("Data query query returned 0 rows.\n %1").arg(dataQuery()));
			return false;
		}
	}
	return true;
}

void QFDbXmlConfigLoader::save(QFXmlConfig *doc) throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	if(doc) {
		QFSqlQueryTable t(appConnection());
		t.reload(dataQuery());
		QString s = QFSql::escapeXmlForSql(doc->dataDocument().toString(2));
		qfInfo() << "saving config to db field " << dataFieldName();
		QFBasicTable::Row &r = t.rowRef(0);
		r.setValue(dataFieldName(), s);
		t.postRow(r);
	}
}

QFSqlConnection & QFDbXmlConfigLoader::appConnection(bool throw_exc)
{
	QCoreApplication *core_app = QCoreApplication::instance();
	QFAppDbConnectionInterface *ifc = dynamic_cast<QFAppDbConnectionInterface*>(core_app); /// cross cast
	if(ifc) return ifc->connection();
	if(throw_exc) QF_EXCEPTION("No QFDbAppConfigInterface");
	return QFAppDbConnectionInterface::dummyConnection();
}



