
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfxmlkeyvals.h"

#include <qfsql.h>
#include <qfassert.h>

#include <QRect>

#include <qflogcust.h>

QFXmlKeyVals::QFXmlKeyVals(const QFDomElement &el)
{
	d = new Data;
	setElement(el);
}

QFXmlKeyVals::~QFXmlKeyVals()
{
}

void QFXmlKeyVals::setElement(const QFDomElement &el)
{
	d->element = el;
	d->keyMap.clear();
}

void QFXmlKeyVals::checkKeyMap() const
{
	static const QString sKeyValElement = "keyval";
	static const QString sKeyAttr = "key";
	//if(isNull()) return QDomElement();
	if(d->keyMap.isEmpty()) {
		//qfInfo() << d->element.toString();
		QFDomElement el = d->element.firstChildElement(sKeyValElement);
		for(; !el.isNull(); el = el.nextSiblingElement(sKeyValElement)) {
			//qfTrash() << "\tcaching key:" << el.attribute(sKeyAttr);
			const_cast<QFXmlKeyVals*>(this)->d->keyMap[el.attribute(sKeyAttr)] = el;
		}
	}
}

QDomElement QFXmlKeyVals::keyValElement(const QString &key) const
{
	//qfTrash() << QF_FUNC_NAME << key;
	checkKeyMap();
	return d->keyMap.value(key);
}

bool QFXmlKeyVals::contains(const QString &key) const
{
	return !keyValElement(key).isNull();
}


QVariant QFXmlKeyVals::value(const QString &key, const QVariant &default_val) const
{
	qfTrash() << QF_FUNC_NAME << key;
	QDomElement el = keyValElement(key);
	if(el.isNull()) {
		qfTrash().noSpace() << "\treturning default value: '" << default_val.toString() << "'";
		return default_val;
	}
	qfTrash().noSpace() << "\tfound value: '" << el.text() << "'";
	return stringToVariant(el.text());
}

QVariant QFXmlKeyVals::valueKeyEndsWith(const QString &key, const QVariant &default_val) const
{
	qfTrash() << QF_FUNC_NAME << key;
	keyValElement(QString()); /// napln keyMap
	QMapIterator<QString, QFDomElement> i(d->keyMap);
	while(i.hasNext()) {
		i.next();
		//qfTrash() << "\t trying:" << i.key();
		if(QFSql::endsWith(i.key(), key)) {
			QFDomElement el = i.value();
			qfTrash().noSpace() << "\tfound value: '" << el.text() << "'";
			return stringToVariant(el.text());
		}
	}
	qfTrash().noSpace() << "\treturning default value: '" << default_val.toString() << "'";
	return default_val;
}

void QFXmlKeyVals::setValue(const QString &key, const QVariant &val)
{
	//qfTrash() << QF_FUNC_NAME;
	QFDomElement el = keyValElement(key);
	QString s = variantToString(val);
	if(el.isNull()) {
		QF_ASSERT(!d->element.isNull(), "keyval is NULL");
		el = d->element.ownerDocument().createElement("keyval");
		d->element.appendChild(el);
		el.setAttribute("key", key);
		el.setText(s);
		d->keyMap[key] = el;
	}
	else {
		el.setText(s);
	}
	d->dirty = true;
}

static QString &escapedLeadingAt(QString &s)
{
	if (s.length() > 0 && s.at(0) == QLatin1Char('@'))
		s.prepend(QLatin1Char('@'));
	return s;
}

static QString &unescapedLeadingAt(QString &s)
{
	if (s.startsWith(QLatin1String("@@")))
		s.remove(0, 1);
	return s;
}

/// vykradeno z QString QSettingsPrivate::variantToString(const QVariant &v).
QString QFXmlKeyVals::variantToString(const QVariant &v)
{
	qfLogFuncFrame() << v.toString();
	QString result;
	switch (v.type()) {
		case QVariant::Invalid:
			result = QLatin1String("@Invalid()");
			break;

		case QVariant::ByteArray: {
			QByteArray a = v.toByteArray();
			result = QLatin1String("@ByteArray(");
			//result += QString::fromLatin1(a.constData(), a.size());
			QString s = QLatin1String(a.toBase64());
			result += s;
			result += QLatin1Char(')');
			//qfTrash().noSpace() << "\t '" << a.constData() << "' -> '" << result << "'";
			break;
		}
		case QVariant::Date: {
			result = QLatin1String("@Date(");
			result += v.toDate().toString(Qt::ISODate);
			result += QLatin1Char(')');
			break;
		}
		case QVariant::Time: {
			result = QLatin1String("@Time(");
			result += v.toTime().toString(Qt::ISODate);
			result += QLatin1Char(')');
			break;
		}
		case QVariant::DateTime: {
			result = QLatin1String("@DateTime(");
			result += v.toDateTime().toString(Qt::ISODate);
			result += QLatin1Char(')');
			break;
		}
		case QVariant::String:
		case QVariant::LongLong:
		case QVariant::ULongLong:
		//case QVariant::Int:
		//case QVariant::UInt:
		//case QVariant::Bool:
		//case QVariant::Double:
		{
			result = v.toString();
			result = escapedLeadingAt(result);
			break;
		}
		case QVariant::Rect: {
			QRect r = qvariant_cast<QRect>(v);
			result += QLatin1String("@Rect(");
			result += QString::number(r.x());
			result += QLatin1Char(' ');
			result += QString::number(r.y());
			result += QLatin1Char(' ');
			result += QString::number(r.width());
			result += QLatin1Char(' ');
			result += QString::number(r.height());
			result += QLatin1Char(')');
			break;
		}
		case QVariant::Size: {
			QSize s = qvariant_cast<QSize>(v);
			result += QLatin1String("@Size(");
			result += QString::number(s.width());
			result += QLatin1Char(' ');
			result += QString::number(s.height());
			result += QLatin1Char(')');
			break;
		}
		case QVariant::Point: {
			QPoint p = qvariant_cast<QPoint>(v);
			result += QLatin1String("@Point(");
			result += QString::number(p.x());
			result += QLatin1Char(' ');
			result += QString::number(p.y());
			result += QLatin1Char(')');
			break;
		}
		case QVariant::PointF: {
			QPointF p = qvariant_cast<QPointF>(v);
			result += QLatin1String("@PointF(");
			result += QString::number(p.x());
			result += QLatin1Char(' ');
			result += QString::number(p.y());
			result += QLatin1Char(')');
			break;
		}
		case QVariant::List: {
			QVariantList lst = qvariant_cast<QVariantList>(v);
			result += QLatin1String("@List(");
			int i = 0;
			foreach(QVariant v, lst) {
				if(i++ > 0) result += QLatin1Char(',');
				result += variantToString(v);
				//qfInfo() << result;
			}
			result += QLatin1Char(')');
			break;
		}
		case QVariant::Map: {
			QVariantMap m = qvariant_cast<QVariantMap>(v);
			result += QLatin1String("@Map(");
			QMapIterator<QString, QVariant> i(m);
			int ix = 0;
			while(i.hasNext()) {
				i.next();
				QString s = i.key();
				if(ix++ > 0) result += QLatin1Char(',');
				result += s.replace(":", "\\:");
				result += ':';
				result += variantToString(i.value());
			}
			result += QLatin1Char(')');
			break;
		}
		case QVariant::Bool: {
			result += QLatin1String("@Bool(");
			result += v.toString();
			result += QLatin1Char(')');
			break;
		}
		case QVariant::Int: {
			result += QLatin1String("@Int(");
			result += QString::number(v.toInt());
			result += QLatin1Char(')');
			break;
		}
		case QVariant::UInt: {
			result += QLatin1String("@UInt(");
			result += QString::number(v.toUInt());
			result += QLatin1Char(')');
			break;
		}
		case QVariant::Double: {
			result += QLatin1String("@Double(");
			/// QT standardne double formatuji funkci QVariant(double).toString(), ktera pouziva QString::number(d, 'g', 10),
			/// coz je u penez (castky nad 99 milionu) nekdy blby 12345678.25
			/// takze zvedame presnost na 16, coz bu melo uz stacit
			/// pozn. 'g': Use the shorter of %e or %f
			///		precision: For g and G specifiers: This is the maximum number of significant digits to be printed.
			result += QString::number(v.toDouble(), 'g', 16);
			result += QLatin1Char(')');
			break;
		}
		default: {
			QByteArray a;
			{
				QDataStream s(&a, QIODevice::WriteOnly);
				s << v;
			}

			result = QLatin1String("@Variant(");
			result += QString::fromLatin1(a.constData(), a.size());
			result += QLatin1Char(')');
			break;
		}
	}
	qfTrash() << "\t return:" << result; 
	return result;
}

static QStringList splitArgs(const QString &s, int idx)
{
	int l = s.length();
	Q_ASSERT(l > 0);
	Q_ASSERT(s.at(idx) == QLatin1Char('('));
	Q_ASSERT(s.at(l - 1) == QLatin1Char(')'));

	QStringList result;
	QString item;

	for (++idx; idx < l; ++idx) {
		QChar c = s.at(idx);
		if (c == QLatin1Char(')')) {
			Q_ASSERT(idx == l - 1);
			result.append(item);
		} else if (c == QLatin1Char(' ')) {
			result.append(item);
			item.clear();
		} else {
			item.append(c);
		}
	}

	return result;
}

QVariant QFXmlKeyVals::stringToVariant(const QString &s)
{
	//qfTrash() << QF_FUNC_NAME;
	if (s.length() > 3 && s.at(0) == QLatin1Char('@') && s.at(s.length() - 1) == QLatin1Char(')')) {

		if (s.startsWith(QLatin1String("@ByteArray("))) {
			QByteArray ba = s.mid(11, s.size() - 12).toAscii();
			ba = QByteArray::fromBase64(ba);
			//qfTrash().noSpace() << "\t '" << s << "' -> '" << ba.constData() << "'";
			return ba;
		}
		else if (s.startsWith(QLatin1String("@Variant("))) {
			QByteArray a(s.toLatin1().mid(9));
			QDataStream stream(&a, QIODevice::ReadOnly);
			QVariant result;
			stream >> result;
			return result;
		}
		else if (s.startsWith(QLatin1String("@DateTime("))) {
			QFString fs = s.trimmed();
			fs = fs.slice(10, -1);
			fs = dateTimeStringToIso(fs);
			QDateTime d = QDateTime::fromString(fs, Qt::ISODate);
			QVariant result = d;
			return result;
		}
		else if (s.startsWith(QLatin1String("@Date("))) {
			QFString fs = s.trimmed();
			fs = fs.slice(6, -1);
			QDate d = QDate::fromString(fs, Qt::ISODate);
			QVariant result = d;
			return result;
		}
		else if (s.startsWith(QLatin1String("@Time("))) {
			QFString fs = s.trimmed();
			fs = fs.slice(6, -1);
			QTime d = QTime::fromString(fs, Qt::ISODate);
			QVariant result = d;
			return result;
		}
		else if (s.startsWith(QLatin1String("@Rect("))) {
			QStringList args = splitArgs(s, 5);
			if (args.size() == 4) {
				return QVariant(QRect(args[0].toInt(), args[1].toInt(), args[2].toInt(), args[3].toInt()));
			}
		}
		else if (s.startsWith(QLatin1String("@Size("))) {
			QStringList args = splitArgs(s, 5);
			if (args.size() == 2) {
				return QVariant(QSize(args[0].toInt(), args[1].toInt()));
			}
		}
		else if (s.startsWith(QLatin1String("@Point("))) {
			QStringList args = splitArgs(s, 6);
			if (args.size() == 2) {
				return QVariant(QPoint(args[0].toInt(), args[1].toInt()));
			}
		}
		else if (s.startsWith(QLatin1String("@PointF("))) {
			QStringList args = splitArgs(s, 6);
			if (args.size() == 2) {
				return QVariant(QPointF(args[0].toDouble(), args[1].toDouble()));
			}
		}
		else if (s.startsWith(QLatin1String("@List("))) {
			//qfTrash().noSpace() << "\tvalue: '" << s << "'";
			QList<QVariant> ret;
			QFString fs = s.mid(5);
			QStringList sl = fs.splitBracketed(',', '(', ')', '\'', QFString::LeavePartsUnchanged, QString::KeepEmptyParts);
			foreach(fs, sl) {
				//qfTrash().noSpace() << "\t\tslice: '" << fs << "'";
				QVariant v = stringToVariant(fs);
				ret << v;
			}
			return ret;
		}
		else if (s.startsWith(QLatin1String("@Map("))) {
			QMap<QString, QVariant> m;
			QFString fs = s.mid(4);
			QStringList sl = fs.splitBracketed(',', '(', ')', '\'', QFString::LeavePartsUnchanged, QString::KeepEmptyParts);
			foreach(fs, sl) {
				int ix;
				QChar prev_c;
				for(ix=0; ix<fs.len(); ix++) {
					if(fs[ix] == ':' && prev_c != '\\') {break;}
					prev_c = fs[ix];
				}
				QF_ASSERT(ix >= 0, "ERROR find key: Map item string should contain ':' character '" + fs + "'");
				QVariant v = stringToVariant(fs.slice(ix + 1));
				fs = fs.slice(0, ix);
				fs.replace("\\:", ":");
				m[fs] = v;
			}
			return m;
		}
		else if (s.startsWith(QLatin1String("@Bool("))) {
			QFString fs = s;
			return fs.slice(6, -1).toBool();
		}
		else if (s.startsWith(QLatin1String("@Int("))) {
			QFString fs = s;
			return fs.slice(5, -1).toInt();
		}
		else if (s.startsWith(QLatin1String("@UInt("))) {
			QFString fs = s;
			return fs.slice(6, -1).toInt();
		}
		else if (s.startsWith(QLatin1String("@Double("))) {
			QFString fs = s;
			return fs.slice(8, -1).toDouble();
		}
		else if (s == QLatin1String("@Invalid()")) {
			return QVariant();
		}
	}

	QString tmp = s;
	return QVariant(unescapedLeadingAt(tmp));
}

QFXmlKeyVals& QFXmlKeyVals::operator+=(const QMap<QString, QVariant> &key_vals)
{
	QMapIterator<QString, QVariant> i(key_vals);
	while(i.hasNext()) {
		i.next();
		setValue(i.key(), i.value());
	}
	return *this;
}

static inline QString line_indent(const QString &ind, int level)
{
	QString s;
	for(int i=0; i<level; i++) s += ind;
	return s;
}
#define IND(level) offset + line_indent(ind, level)
QString QFXmlKeyVals::toHtml(const QVariantMap &opts) const
{
	QString offset = opts.value("lineOffset").toString();
	QString eoln = opts.value("lineSeparator", "\n").toString();
	QString ind = opts.value("lineIndent", "  ").toString();
	QString ret;
	if(!isEmpty()) {
		QStringList sl = keys();
		ret += IND(0) + "<table border=1>" + eoln;
		foreach(QString s, sl) {
			ret += IND(1) + "<tr>" + eoln;
			ret += IND(2) + "<th>" + s + "</th>" + eoln;
			ret += IND(2) + "<td>" + value(s).toString() + "</td>" + eoln;
			ret += IND(1) + "</tr>" + eoln;
		}
		ret += IND(0) + "</table>" + eoln;
	}
	return ret;
}

QString QFXmlKeyVals::toString() const
{
	QStringList sl;
	foreach(QString s, keys()) {
		sl << "'" + s + "': " + value(s).toString();
	}
	return "{" + sl.join(", ") + "}";
}

QString QFXmlKeyVals::dateTimeStringToIso(const QString & str)
{
	if(str.length() > 10 && str[10] != 'T') {
		QFString fs = str;
		fs = fs.slice(0, 10) + 'T' + fs.slice(11);
		return fs;
	}
	return str;
}

