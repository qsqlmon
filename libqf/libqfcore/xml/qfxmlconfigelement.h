#ifndef QFXMLCONFIGELEMENT_H
#define QFXMLCONFIGELEMENT_H

#include <qfcoreglobal.h> 
#include <qfdom.h>

#include <QVariant>
#include <QCoreApplication> /// declares Q_DECLARE_TR_FUNCTIONS

/**
  QFXmlConfigElement is used for XML based application configuration files.
 */
class QFCORE_DECL_EXPORT QFXmlConfigElement : public QFDomElement
{
	Q_DECLARE_TR_FUNCTIONS(QFXmlConfigElement);
	public:
		enum Type {Null = 0, PathFragment, Text, File, Dir, Bool, Int, Float, List};
	protected:
		/*
		class Data : public QSharedData
		{
			public:
				Type typeCache;
				//bool crypted;
				Data() : typeCache(TypeUnparsed) {}
		};
	private:
		QSharedDataPointer<Data> d;
		*/
	protected:
		//QVariant text2value(const QString &s) const;
	public:
		/// returns element on \a path. Path can be absolute or relative to the current element
		QFXmlConfigElement cd(const QString& path, bool throw_exc = true) const throw(QFException)
		{
			return QFDomElement::cd(path, throw_exc);
		}
		/// \sa QFDomElement::mkcd(const QString&)
		QFXmlConfigElement mkcd(const QString& path)
		{
			return QFDomElement::mkcd(path);
		}
		// get config value or throw an exception if the Element is null.
		// If value (child element value) does not exist  default() is returned.
		//QVariant value() const throw(QFException);
		/// Return true if config element does not contain value tag.
		bool hasValue() const;
		bool hasDefaultValue() const;
		/// get config value or defaultvalue defined in document or \a default_val if the Element is null.
		/// nemuzu dat default_val = QVariant(), protoze jinak mi pro default_val typu QString vola value(path, default_val)
		virtual QVariant value(const QVariant &default_val) const;
		/// return value or defaultvalue defined in document
		//virtual QVariant value() const;
		QVariant value(const QString &path, const QVariant &default_val) const;
		/// set config value or throw an exception if the Element is null.
		/// @param changed_flag is set to true, if new value is different than the old one
		virtual void setValue(const QVariant &val, bool *changed_flag = NULL) throw(QFException);
		/// check if the \a path exists, if it is not, create it. Set element value.
		/// \sa QFDomElement::mkdir()
		QFXmlConfigElement setValue(const QString& path, const QVariant &val, bool *changed_flag = NULL) throw(QFException);

		//! get attribute \a opt_name of child \a option .
		QFString option(const QString &opt_name, const QString &default_val = QString()) const;
		void setOption(const QString &opt_name, const QString &val);

		Type type() const;
		//QVariant::Type variantType() const;
		bool isLeaf() const;
		QString caption() const;
		QString unit() const;
		QString help() const;
		const QFDomElement options() const;
		QVariant defaultValue() const;
		bool isCheckable() const;
		bool childrenHasExclusiveCheck() const;
		QStringList childConfigElementNames() const;
		//bool isChecked() const;
		//void setChecked(bool b);
		static bool isServiceElement(const QDomElement &el);

		static QString variantToString(const QVariant &v);
		QVariant stringToVariant(const QString &s) const;
	public:
		QFXmlConfigElement(const QFDomElement& el = QFDomElement()) : QFDomElement(el) { }
		virtual ~QFXmlConfigElement() {}
};

#endif // QFXMLCONFIGELEMENT_H

