# Input

INCLUDEPATH += $$PWD

HEADERS +=        \
    $$PWD/qfdom.h        \
	$$PWD/qfxmlconfigpersistenter.h        \
	$$PWD/qfxmlconfigelement.h     \
	$$PWD/qfxmlconfig.h    \
	$$PWD/qfxmlconfigdocument.h    \
	$$PWD/qfxmltable.h   \
	$$PWD/qfxmlkeyvals.h  \

SOURCES +=        \
    $$PWD/qfdom.cpp        \
	$$PWD/qfxmlconfigpersistenter.cpp        \
	$$PWD/qfxmlconfig.cpp       \
	$$PWD/qfxmlconfigdocument.cpp       \
	$$PWD/qfxmlconfigelement.cpp     \
	$$PWD/qfxmltable.cpp   \
	$$PWD/qfxmlkeyvals.cpp  \


