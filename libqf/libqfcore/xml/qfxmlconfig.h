#ifndef QFXMLCONFIG_H
#define QFXMLCONFIG_H

#include <qfcoreglobal.h>
#include <qfcrypt.h>
#include <qfclassfield.h>
#include <qfxmlconfigdocument.h>

#include <QPointer>
#include <qfbasictable.h>

class QFXmlConfigLoader;
class QFSqlConnection;

//! Tady chybi dokumentace.
class QFCORE_DECL_EXPORT QFXmlConfig
{
	Q_DECLARE_TR_FUNCTIONS(QFXmlConfig);
	friend class QFXmlConfigWidget;
	//QF_FIELD_RW(bool, is, set, TemplateDirty);
	QF_FIELD_RW(bool, is, set, DataDirty);
	protected:
		/// tady je struktura konfigu i data dohromady
		QFXmlConfigDocument f_templateDoc;
		/// obsahuje jen data, struktura je v f_templateDoc, v pripade, ze jsou data namichana s template (jednosouborovy config),
		/// referencuji f_templateDoc i f_dataDoc stejny dokument.
		QFXmlConfigDocument f_dataDoc;
		QPointer<QFXmlConfigLoader> f_configLoader;
		QFCrypt f_crypter;
	public:
		void setCrypter(const QFCrypt &crypter) {f_crypter = crypter;}

		virtual QFXmlConfigElement cd(const QString &path, bool throw_exc = Qf::ThrowExc) throw(QFException) {
			return templateDocument().cd(path, throw_exc);
		}

		//! cesta, kam se v konfigu ukladaji informace nezobrazovane uzivateli v QFDlgXmlConfig
		static const QString AppInternalsPath;

		/// XMLConfig DOES NOT take ownership of \a loader . Nemuze, XmlConfig je implicitne sdilen a kdo by to mel vymazat?
		void setConfigLoader(QFXmlConfigLoader *loader);
		QFXmlConfigLoader* configLoader(bool throw_exc = Qf::ThrowExc) throw(QFException);

		virtual QFXmlConfigElement createAppInternalsPath();
		QFXmlConfigElement appInternalsPath() const;
		virtual QFXmlConfigElement createPersistentPath(const QString &xml_config_persistent_id);
		QFXmlConfigElement persistentPath(const QString &xml_config_persistent_id);
	public:
		QFXmlConfigDocument& templateDocument() {return f_templateDoc;}
		const QFDomDocument& templateDocument() const {return f_templateDoc;}
		void setTemplateDocument(const QFXmlConfigDocument &template_document);
		void setTemplateDocument(const QString &xml_str) throw(QFException);

		QFXmlConfigDocument& dataDocument() {return f_dataDoc;}
		const QFDomDocument& dataDocument() const {return f_dataDoc;}
		void setDataDocument(const QFXmlConfigDocument &data_document)
		{
			f_dataDoc = data_document;
			setDataDirty(false);
		}
		void setDataDocument(const QString &xml_str) throw(QFException);

		//virtual void setTemplateDirty(bool b = true) {setDocumentDirty(b);}
		//virtual bool isTemplateDirty() const {return isDocumentDirty();}

		//! Vrati verzi configu jako string.
		QString version() const;
	protected:
		QVariant cryptValue(const QFXmlConfigElement &el, const QVariant &val) const;
		QVariant decryptValue(const QFXmlConfigElement &el, const QVariant &val) const;
		void restoreDefaultData_walk(const QFDomElement &parent_el);
		
	public:
		virtual bool load() throw(QFException);
		virtual void save() throw(QFException);

		/// vymaze v datech vetve, ktery neobsahuji element value
		/// neni k tomu, aby z dat vymazaval vetve, ktery nejsou v template, takovy tool je potreba teprve napsat
		/// pouziva se na to, aby se neukladaly do dat vetve, ktere maji stejnou hodnotu, jako template, zbytecne
		virtual void dataLint();
		
		/// ucel teto dvojice funkci je, aby dokument mohl zasahovat do ukladani a nacitani hodnot,
		/// napriklad, kdyz chci mit data v samostatnem XML dokumentu.
		/// Take pri pouzivani techto metod funguje spravne isDirty() funkce;
		/// Pokud \a value neni nalezena a defaultvalue neni definovana vraci \a default_value.
		virtual QVariant value(const QFXmlConfigElement &template_el, const QVariant &default_value = QVariant()) const;
		/// Pokud \a value neni nalezena vraci \a default_value.
		QVariant value(const QString &path, const QVariant &default_value = QVariant()) const;

		/// vraci jednotku pro hodnotu na ceste \a path .
		QString valueUnit(const QString &path, const QString &default_unit = QString()) const;

		/// el je element z documentu
		virtual void setValue(const QFXmlConfigElement &template_el, const QVariant &val);
		void setValue(const QString &path, const QVariant &val);
	public:
		/// vradi to mapu map vsech hodnot, i tech z template
		/// asi to bude chtit parametr, kterej to prepne tak, aby vratil jen hodnoty odlisny od template
		QVariant valuesToVariant() const;
		void valuesFromVariant(const QVariant &v);

		class ToTableOptions
		{
			protected:
				QVariantMap opts;
			public:
				//ToTableOptions& setReturnTemplateValues(bool b) {(*this)["ReturnTemplateValues"] = b; return *this;}
				//bool returnTemplateValues() const {return this->value("ReturnTemplateValues").toBool();}
				/// pouze takove hodnoty, ktere se lisi od template value
				ToTableOptions& dirtyValuesOnly(bool b) {opts["dirtyValuesOnly"] = b; return *this;}
				bool dirtyValuesOnly() const {return opts.value("dirtyValuesOnly").toBool();}
				ToTableOptions& captionSeparator(const QString &sep) {opts["captionSeparator"] = sep; return *this;}
				QString captionSeparator() const {return opts.value("captionSeparator").toString();}
				ToTableOptions& trueValueString(const QString &str) {opts["trueValueString"] = str; return *this;}
				QString trueValueString() const {return opts.value("trueValueString", "true").toString();}
				ToTableOptions& falseValueString(const QString &str) {opts["falseValueString"] = str; return *this;}
				QString falseValueString() const {return opts.value("falseValueString", "false").toString();}
			public:
				//ToTableOptions() : QVariantMap() {}
		};
		QFBasicTable toTable(const ToTableOptions &opts) const;

		/// vraci true, pokud \a path existuje v template dokumentu
		bool isPathValid(const QString &path) const;
		
		void clearTemplate() {f_templateDoc = QFXmlConfigDocument();}
		void clearData();
		void clear() {clearTemplate(); clearData();}
		//bool isEmpty() const {return f_templateDoc.isEmpty() && f_dataDoc.isEmpty();}
		bool isTemplateEmpty() const {return f_templateDoc.isEmpty();}
		bool isDataEmpty() const {return f_dataDoc.isEmpty();}
		void restoreDefaultData();

		/// dokuments are explicitly shared, this functions clone documents, so config has now own separate copy.
		void detachTemplate();
		void detachData();
		void detach() {detachData(); detachTemplate();}
	protected:
		QVariant valuesToVariant_helper(const QFDomElement &template_el) const;
		void valuesFromVariant_helper(const QVariantMap &vals, const QString &path);
		void toTable_helper(const QFDomElement &template_el, QFBasicTable &table, const ToTableOptions &opts) const;
	public:
		QFXmlConfig() {}
		QFXmlConfig(const QFDomDocument &template_document, const QFDomDocument &data_document = QFXmlConfigDocument());
		virtual ~QFXmlConfig();
};

class QFCORE_DECL_EXPORT QFXmlConfigLoader : public QObject
{
	Q_OBJECT;
	public:
		virtual bool load(QFXmlConfig *doc, bool throw_exc = Qf::ThrowExc) throw(QFException) = 0;
		virtual void save(QFXmlConfig *doc) throw(QFException) = 0;
	public:
		QFXmlConfigLoader(QObject *parent = NULL) : QObject(parent) {}
		virtual ~QFXmlConfigLoader() {}
};

class QFCORE_DECL_EXPORT QFXmlConfigFileLoader : public QFXmlConfigLoader
{
	Q_OBJECT;
	protected:
		QString f_templateFileName;
	public:
		//! Information about a recent config loading process.
		//! If everything was OK, returns empty string.
		static QString& configLoadingStatus();

		//! Vrati verzi configu v resourcich aplikace jako string.
		static QString resourceConfigVersion() throw(QFException);
		//! returns ':/app_name.conf.xml'
		static QString resourceConfigName();
	protected:
		/// prohleda vsechno mozny ve snaze najit soubor s konfiguraci, pokud ho nalezne, vrati jeho jmeno, jinak prazdny string.
		/// pokud neni zadana hodnota \a config_file_name pouzi se app_config_name().
		/*! Funkce se snazi najit \a config_file_name v nasledujicich umistenich:
			* \li curr_dir/$(APP_NAME).conf.xml
			* \li APP_DIR/$(APP_NAME).conf.xml
			* \li resource :/$(APP_NAME).conf.xml
		*/
		virtual QString accessibleTemplateConfigFileName(const QString &config_file_name = QString());
		/// prohleda vsechno mozny ve snaze najit soubor s konfiguraci, pokud ho nalezne, vrati jeho jmeno, jinak prazdny string.
		/// pokud neni zadana hodnota \a config_file_name pouzi se app_config_name().
		/*! Funkce se snazi najit \a config_file_name v nasledujicich umistenich:
		 * \li curr_dir/$(APP_NAME).conf.xml
		 * \li app_dir/$(APP_NAME).conf.xml
		 * \li :/$(APP_NAME).conf.xml
		 */
		//static QString accessibleDataConfigFileName(const QString config_file_name = QString());
	public:
		/*!
		* Nahraje konfiguracni soubor.
	 	* Pokud neni zadano \a file_with_path pouzije se funkce accessibleTemplateConfigFileName()
		* Pokud neni zadny soubor nalezen je nahran prazdny config nebo vrzena vyjimka, to zalezi na \a throw_exc .
		* @return true pokud se podari neco nahrat.
		 */
		virtual bool load(QFXmlConfig *doc, bool throw_exc = Qf::ThrowExc) throw(QFException);
		virtual void save(QFXmlConfig *doc) throw(QFException);
	public:
		QFXmlConfigFileLoader(QObject *parent = NULL) : QFXmlConfigLoader(parent) {}
		QFXmlConfigFileLoader(const QString template_file_name, QObject *parent = NULL) : QFXmlConfigLoader(parent), f_templateFileName(template_file_name) { }
		//virtual ~QFXmlConfigFileLoader() {}
};

class QFCORE_DECL_EXPORT QFXmlConfigReadOnlyFileLoader : public QFXmlConfigFileLoader
{
	Q_OBJECT;
	protected:
	public:
		virtual void save(QFXmlConfig *doc) throw(QFException) {Q_UNUSED(doc);}
	public:
		QFXmlConfigReadOnlyFileLoader(QObject *parent = NULL) : QFXmlConfigFileLoader(parent) {}
		QFXmlConfigReadOnlyFileLoader(const QString template_file_name, QObject *parent = NULL)
			: QFXmlConfigFileLoader(template_file_name, parent) {}
};

class QFCORE_DECL_EXPORT QFXmlConfigSplittedFileLoader : public QFXmlConfigFileLoader
{
	Q_OBJECT;
	protected:
		QString f_dataFileName;
		QString dataFileNameWithPath();
		virtual QString accessibleTemplateConfigFileName(const QString &config_file_name = QString());
	public:
		virtual bool load(QFXmlConfig *doc, bool throw_exc = Qf::ThrowExc) throw(QFException);
		virtual void save(QFXmlConfig *doc) throw(QFException);
	public:
		QFXmlConfigSplittedFileLoader(QObject *parent = NULL) : QFXmlConfigFileLoader(parent) {}
		QFXmlConfigSplittedFileLoader(const QString template_file_name, const QString data_file_name = QString(), QObject *parent = NULL)
	: QFXmlConfigFileLoader(template_file_name, parent), f_dataFileName(data_file_name) {}
};

class QFCORE_DECL_EXPORT QFDbXmlConfigLoader : public QFXmlConfigLoader
{
	Q_OBJECT;
	//Q_DECLARE_TR_FUNCTIONS(QFDbXmlConfigLoader);
		//QF_FIELD_RW(QString, t, setT, emplateFieldName);
	QF_FIELD_RW(QString, d, setD, ataFieldName);
	public:
		static QFSqlConnection& appConnection(bool throw_exc = Qf::ThrowExc);
	public:
		virtual bool load(QFXmlConfig *doc, bool throw_exc = Qf::ThrowExc) throw(QFException);
		virtual void save(QFXmlConfig *doc) throw(QFException);

		//! potomek musi vratit query, ktera vrati radek obsahujici config template, pouzije se prvni field z vraceneho resultsetu.
		virtual QString templateQuery() = 0;
		//! potomek musi vratit UPDATEABLE (resultset musi obsahovat primarni klice) query, ktera vrati radek obsahujici config data ve fieldu dataFieldName().
		virtual QString dataQuery() = 0;
		// @param field_name ve tvaru table.field
		//void setDbTemplateField(const QString &field_name) {f_dbTemplateField = field_name;}
		// @param field_name ve tvaru table.field
		//void setDbDataField(const QString &field_name) {f_dbDataField = field_name;}
	public:
		QFDbXmlConfigLoader(QObject *parent = NULL);
		virtual ~QFDbXmlConfigLoader() {}
};


#endif // QFXMLCONFIG_H

