
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfxmltable.h"

#include <qfcsvreader.h>
#include <qfsql.h>
#include <qfbasictable.h>
#include <qfassert.h>

#include <qflogcust.h>

//===========================================================
//                                          QFXmlTableColumnList
//===========================================================
int QFXmlTableColumnList::indexOf(const QString &col_name) const
{
	//qfTrash() << QF_FUNC_NAME << col_name;
	//int i = nameMap.value(col_name, -1); /// tohle je na picu, protoze kazdej radek ma vlastni kopii columns.
	for(int i=0; i<count(); i++) {
		const QFXmlTableColumnDef &cd = at(i);
		//qfTrash() << "\ttrying:" << cd.name;
		if(QFSql::endsWith(cd.name, col_name)) {
			//const_cast<QFXmlTableColumnList*>(this)->nameMap[col_name] = i;
			//qfTrash() << "\treturn:" << i << "of" << count();
			return i;
		}
	}
	//qfTrash() << "\treturn:" << -1;
	return -1;
}

QFXmlTableColumnDef& QFXmlTableColumnList::appendColumn(const QDomElement &el)
{
	//qfTrash() << QF_FUNC_NAME << "colcnt before:" << count();
	QFXmlTableColumnDef cd;
	cd.name = el.attribute("name");
	cd.type = QVariant::nameToType(qPrintable(el.attribute("datatype")));
	//cd.caption = el.attribute("caption");
	//cd.halign = el.attribute("halign");
	/*
	Qt::Alignment alignment;
	QString align = el.attribute("halign");
	if(align == "left") alignment |= Qt::AlignLeft;
	else if(align == "right") alignment |= Qt::AlignRight;
	else if(align == "center") alignment |= Qt::AlignHCenter;
	align = el.attribute("valign");
	if(align == "top") alignment |= Qt::AlignTop;
	else if(align == "bottom") alignment |= Qt::AlignBottom;
	else if(align == "center") alignment |= Qt::AlignVCenter;
	*/
	append(cd);
	return last();
}

QFXmlTableColumnDef QFXmlTableColumnList::columnForName(const QString &col_name, bool throw_exc) const throw(QFException)
{
	int ix = indexOf(col_name);
	if(ix < 0 && throw_exc) {
		QF_EXCEPTION(QString("Cant find column name '%1'").arg(col_name));
	}
	return value(ix);
}

QStringList QFXmlTableColumnList::columnNames() const
{
	QStringList ret;
	for(int i=0; i<count(); i++) {
		ret << at(i).name;
	}
	return ret;
}

/*
void QFXmlTableColumnList::createNameMap()
{
	nameMap.clear();
	for(int i=0; i<count(); i++) {
		const QFXmlTableColumnDef &cd = at(i);
		nameMap[cd.name] = i;
	}
}
*/
//===========================================================
//                                     QFXmlTableRow::Data
//===========================================================
void QFXmlTableRow::Data::flush()
{
	//qfTrash() << QF_FUNC_NAME << "dirty:" << dataDirty;
	if(dataDirty) {
		QString s;
		QTextStream ts(&s);
		QFCsvReader csv(&ts);
		csv.writeCsvLine(dataList);
		//qfInfo() << QF_FUNC_NAME << "flushing CDATA:" << s;
		QFDomElement el = element;
		QDomCDATASection cdata = el.firstChild().toCDATASection();
		if(cdata.isNull()) {
			cdata = el.ownerDocument().createCDATASection(s);
			el.insertBefore(cdata, QDomNode());
		}
		else cdata.setData(s);
		dataDirty = false;
	}
}

//===========================================================
//                                          QFXmlTableRow
//===========================================================
const QFXmlTableRow& QFXmlTableRow::null()
{
	static QFXmlTableRow n(0);
	return n;
}

QFXmlTableRow::QFXmlTableRow(int)
{
	d = new Data();
};

QFXmlTableRow::QFXmlTableRow()
{
	*this = null();
}

QFXmlTableRow::QFXmlTableRow(const QFDomElement &el, const QFXmlTableColumnList &cols)
{
	if(el.isNull()) *this = null();
	else {
		d = new Data();
		d->columns = cols;
		d->element = el;
		{
			QDomElement el_kv = d->element.firstChildElement("keyvals");
			if(!el_kv.isNull()) {
				//qfInfo() << el_kv.text();
				d->keyVals.setElement(el_kv);
			}
		}
	}
	//init();
}

QStringList& QFXmlTableRow::dataRef()
{
	if(isNull()) return d->dataList;
	if(d->dataList.isEmpty() && d->columns.count()>0) {
		QDomCDATASection cdata = d->element.firstChild().toCDATASection();
		if(!cdata.isNull()) {
			QString s = cdata.data();
			QTextStream ts(&s);
			QFCsvReader rd(&ts);
			d->dataList = rd.readCsvLineSplitted();
		}
		while(d->dataList.count() < d->columns.count()) d->dataList << QString();
	}
	return d->dataList;
}

const QStringList& QFXmlTableRow::data() const
{
	if(isNull()) return d->dataList;
	if(d->dataList.isEmpty() && d->columns.count()>0) const_cast<QFXmlTableRow*>(this)->dataRef();
	return d->dataList;
}

QVariant QFXmlTableRow::value(int col) const
{
	if(isNull()) return QVariant();
	if(0<=col && col<data().count() && col < d->columns.count()) {
		const QFXmlTableColumnDef &cd = d->columns.at(col);
		QString s = data()[col];
		/// prazdna hodnota je null
		if(s.isEmpty()) return QVariant();
		switch(cd.type) {
			case QVariant::Int:
				return s.toInt();
			case QVariant::UInt:
				return s.toUInt();
			case QVariant::LongLong:
				return s.toLongLong();
			case QVariant::Double:
				//qfInfo() << s << "todouble->" << s.toDouble();
				return s.toDouble();
			case QVariant::Date:
				return QDate::fromString(s, Qt::ISODate);
			case QVariant::Time:
				return QTime::fromString(s, Qt::ISODate);
			case QVariant::DateTime:
				//qfInfo() << "s:" << s;
				if(!s.isEmpty()) return QDateTime::fromString(s, Qt::ISODate);
				return QDateTime();
			case QVariant::Bool:
				return QFString(s).toBool();
			case QVariant::String:
				return s;
			case QVariant::ByteArray:
				return s.toUtf8();
			default:
				return "unsupported type " + QString::number(cd.type);
		}
	}
	return QVariant();
}

QVariant QFXmlTableRow::value(const QString &col_or_key_name) const throw(QFException)
{
	if(isNull()) return QVariant();
	int ix = d->columns.indexOf(col_or_key_name);
	if(ix < 0) {
		/// try keyvals
		QVariant v = d->keyVals.value(col_or_key_name);
		if(!v.isValid()) {
			QStringList col_names = d->columns.columnNames();
			QStringList keys = d->keyVals.keys();
			QF_EXCEPTION(QString("key '%1' not found in columns or keyvals\n\ncolumns:\n%2\n\nkeys:\n%3").arg(col_or_key_name).arg(col_names.join(",")).arg(keys.join(",")));
		}
		return v;
	}
	return value(ix);
}

QVariant QFXmlTableRow::value(const QString &col_or_key_name, const QVariant &default_val) const
{
	if(isNull()) return QVariant();
	int ix = d->columns.indexOf(col_or_key_name);
	if(ix < 0) {
		/// try keyvals
		//qfInfo() << col_or_key_name << "in keyvals" << d->keyVals.keys().join(",");
		QVariant v = d->keyVals.value(col_or_key_name);
		if(!v.isValid()) v = default_val;
		return v;
	}
	return value(ix);
}

void QFXmlTableRow::setValue(int col, const QVariant &val)
{
	if(isNull()) return;
	if(0<=col && col<data().count() && col < d->columns.count()) {
		//qfTrash() << QF_FUNC_NAME << "col:" << col << "val:" << val.toString();
		const QFXmlTableColumnDef &cd = d->columns.at(col);
		QString s;
		switch(cd.type) {
			case QVariant::Date: {
				QDate dt = val.toDate();
				s = dt.toString(Qt::ISODate);
			}
			default:
				s = val.toString();
		}
		dataRef()[col] = s;
		d->dataDirty = true;
	}
}

void QFXmlTableRow::setValue(const QString &col_or_key_name, const QVariant &val)
{
	if(isNull()) return;
	int ix = d->columns.indexOf(col_or_key_name);
	if(ix < 0) {
		/// try keyvals
		if(d->keyVals.isNull()) {
			QFDomElement el = d->element.mkcd("keyvals");
			d->keyVals.setElement(el);
		}
		d->keyVals.setValue(col_or_key_name, val);
		return;
	}
	setValue(ix, val);
}
/*
void QFXmlTableRow::flushData()
{
	if(isNull()) return;
	d->flush();
}
*/
QFXmlTable QFXmlTableRow::firstChildTable() const
{
	if(isNull()) return QDomElement();
	QDomElement el = d->element.firstChildElement("tables").firstChildElement("table");
	return QFXmlTable(el);
}

QFXmlTable QFXmlTableRow::firstChildTable(const QString &table_name) const
{
	qfTrash() << QF_FUNC_NAME << "table_name:" << table_name;
	if(isNull()) return QDomElement();
	QDomElement el = d->element.firstChildElement("tables").firstChildElement("table");
	for(; !el.isNull(); el = el.nextSiblingElement("table")) {
		qfTrash() << "\ttrying table name:" << el.attribute("name");
		if(el.attribute("name") == table_name) {
			qfTrash() << "\tOK";
			return el;
		}
	}
	qfTrash() << "\tNOT_FOUND";
	return QFXmlTable();
}

QFXmlTable QFXmlTableRow::nextSiblingTable(const QFXmlTable &t) const
{
	//if(t.isNull()) return t;
	return QFXmlTable(t.element().nextSiblingElement("table"));
}

void QFXmlTableRow::appendTable(const QFXmlTable &t)
{
	if(isNull()) return;
	if(t.isNull()) return;
	QFDomElement el = d->element.mkcd("tables");
	el.appendChild(t.element());
}

QString QFXmlTableRow::toString() const
{
	QString ret;
	if(isNull()) ret = "NULL row";
	else if(d->dataDirty) {
		ret = "CACHE: ";
		QTextStream ts(&ret);
		QFCsvReader csv(&ts);
		csv.writeCsvLine(d->dataList);
	}
	else {
		ret = "CDATA: ";
		QDomCDATASection cdata = d->element.firstChild().toCDATASection();
		ret += cdata.data();
	}
	return ret;
}


//===========================================================
//                                          QFXmlTable
//===========================================================
const QFXmlTable& QFXmlTable::null()
{
	static QFXmlTable n(0);
	return n;
}

QFXmlTable::QFXmlTable(int)
{
	d = new Data();
};

QFXmlTable::QFXmlTable()
{
	*this = null();
};

QFXmlTable::QFXmlTable(QDomDocument &doc, const QString &table_name)
{
	qfLogFuncFrame() << "table_name:" << table_name;
	d = new Data();
	d->element = doc.createElement("table");
	if(!table_name.isEmpty()) d->element.setAttribute("name", table_name);
	//init();
}

QFXmlTable::QFXmlTable(const QDomElement &el)
{
	init(el);
}
/*
QFXmlTable::QFXmlTable(const QFXmlTableDocument & doc)
{
	QDomElement el = doc.firstChildElement();
	init(el);
}
*/
void QFXmlTable::init(const QDomElement & el)
{
	if(el.isNull()) *this = null();
	else {
		d = new Data();
		d->element = el;
	}
	QFDomElement el1 = d->element.cd("keyvals", !Qf::ThrowExc);
	d->keyVals.setElement(el1);
}

const QFXmlTableColumnList& QFXmlTable::columns() const
{
	if(isNull()) return d->columnList;
	if(d->columnList.isEmpty()) const_cast<QFXmlTable*>(this)->columnsRef();
	return d->columnList;
}

QFXmlTableColumnList& QFXmlTable::columnsRef()
{
	if(isNull()) return d->columnList;
	if(d->columnList.isEmpty()) {
		QFDomElement el = d->element.cd("cols", !Qf::ThrowExc).firstChildElement("col");
		for(; !!el; el = el.nextSiblingElement("col")) {
			d->columnList.appendColumn(el);
		}
	}
	return d->columnList;
}

QFXmlTableRow QFXmlTable::firstRow() const
{
	QFDomElement el = d->element.firstChildElement("rows").firstChildElement("row");
	return QFXmlTableRow(el, columns());
}

QFXmlTableRow QFXmlTable::nextRow(const QFXmlTableRow &r) const
{
	QFDomElement el = r.element();
	el = el.nextSiblingElement("row");
	return QFXmlTableRow(el, columns());
}

QFXmlTableRow QFXmlTable::appendRow()
{
	if(isNull()) return QFXmlTableRow();
	QFDomElement el = d->element.mkcd("rows");
	QFDomElement el_row = el.ownerDocument().createElement("row");
	el.appendChild(el_row);
	//qfTrash() << QF_FUNC_NAME << "el:" << el_row.tagName();
	return QFXmlTableRow(el_row, columns());
}

QFXmlTableRow QFXmlTable::removeRow(const QFXmlTableRow & r)
{
	if(isNull()) return QFXmlTableRow();
	QFXmlTableRow ret = nextRow(r);
	QFDomElement row_el = r.element();
	QDomElement parent_el = row_el.parentNode().toElement();
	if(parent_el.tagName() == "rows") { /// jen pro jistotu, melo by to tak byt
		parent_el.removeChild(row_el);
	}
	return ret;
}

QFXmlTableColumnDef& QFXmlTable::appendColumn(const QString &name, QVariant::Type type, const QString &header)
{
	//qfTrash() << QF_FUNC_NAME << name << "col cnt before append:" << columns().count();
	QF_ASSERT(!isNull(), "table is Null");
	QFDomElement el_cols = d->element.mkcd("cols");
	QFDomElement el = el_cols.ownerDocument().createElement("col");
	el.setAttribute("name", name);
	el.setAttribute("datatype", QVariant::typeToName(type));
	QString s = header;
	if(s.isEmpty()) QFSql::parseFullName(name, &s);
	el.setAttribute("header", s);
	//qfTrash() << "\tNECHAAAAAAAAAAAAAAAAAAAPU" << columns().count();;
	QFXmlTableColumnDef &ret = columnsRef().appendColumn(el);
	/// tohle musi byt az tady, protoze columnsRef() vola appendColumn() taky
	el_cols.appendChild(el);
	//qfTrash() << "\tporad NECHAAAAAAAAAAAAAAAAAAAPU" << columns().count();;
	//qfTrash() << "\tcol cnt after append:" << columns().count();
	return ret;
}

QDomElement QFXmlTable::columnElement(const QString &col_name) const
{
	qfLogFuncFrame();
	QDomElement ret;
	int ix = columns().indexOf(col_name);
	QFDomElement el = d->element.firstChildElement("cols");
	el = el.firstChildElement("col");
	for(int i=0; i<ix && !!el; i++) el = el.nextSiblingElement("col");
	if(!!el && ix >= 0) {
		ret = el;
	}
	return ret;
}

void QFXmlTable::setColumnHeader(const QString &col_name, const QString &caption)
{
	qfLogFuncFrame();
	QDomElement el = columnElement(col_name);
	if(!el.isNull()) {
		qfTrash() << "\tchanging to:" << caption;
		el.setAttribute("header", caption);
	}
}

QString QFXmlTable::columnHeader(const QString &col_name) const
{
	QString ret;
	QDomElement el = columnElement(col_name);
	if(!el.isNull()) {
		ret = el.attribute("header");
	}
	return ret;
}

void QFXmlTable::setColumnFooter(const QString &col_name, const QString &caption)
{
	qfLogFuncFrame();
	QDomElement el = columnElement(col_name);
	if(!el.isNull()) {
		qfTrash() << "\tchanging to:" << caption;
		el.setAttribute("footer", caption);
	}
}

QString QFXmlTable::columnFooter(const QString &col_name) const
{
	QString ret;
	QDomElement el = columnElement(col_name);
	if(!el.isNull()) {
		ret = el.attribute("footer");
	}
	return ret;
}

QString QFXmlTable::columnHAlignment(const QString &col_name) const
{
	QString ret;
	QDomElement el = columnElement(col_name);
	if(!el.isNull()) {
		ret = el.attribute("halign");
	}
	if(ret.isEmpty()) {
		/// pokud neni hodnota predepsana, vem ji z dat
		QFXmlTableColumnDef cd = columns().columnForName(col_name, !Qf::ThrowExc);
		if(cd.isValid()) {
			switch(cd.type) {
				case QVariant::Int:
				case QVariant::UInt:
				case QVariant::LongLong:
				case QVariant::ULongLong:
				case QVariant::Double:
					ret = "right";
					break;
				default:
					break;
			}
		}
	}
	return ret;
}

void QFXmlTable::setColumnAlignment(const QString &col_name, Qt::Alignment alignment)
{
	qfLogFuncFrame();
	QDomElement el = columnElement(col_name);
	if(!el.isNull()) {
		if(alignment & Qt::AlignLeft) el.setAttribute("halign", "left");
		else if(alignment & Qt::AlignRight) el.setAttribute("halign", "right");
		else if(alignment & Qt::AlignCenter || alignment & Qt::AlignHCenter) el.setAttribute("halign", "center");
		else el.setAttribute("halign", QString());
		
		if(alignment & Qt::AlignTop) el.setAttribute("valign", "top");
		else if(alignment & Qt::AlignBottom) el.setAttribute("valign", "bottom");
		else if(alignment & Qt::AlignCenter || alignment & Qt::AlignVCenter) el.setAttribute("valign", "center");
		else el.setAttribute("valign", QString());
	}
}

QVariant QFXmlTable::value(const QString &_key_name, const QVariant &default_val, bool key_ends_with) const
{
	qfLogFuncFrame() << _key_name;
	QString path;
	QFString key_name = _key_name.trimmed();
	{
		int ix = key_name.lastIndexOf('/');
		if(ix >= 0) {
			path = key_name.mid(0, ix+1);
			key_name = key_name.mid(ix+1);
		}
	}
	//qfTrash() << "\t table is null:" << t.isNull();
	//qfTrash() << "\t path:" << path;
	//qfTrash() << "\t data_src:" << ds;
	QVariant ret;
	if(!path.isEmpty()) {
		QFXmlTable t = cd(path, !Qf::ThrowExc);
		ret = t.value(key_name);
	}
	else {
		if(key_name.startsWith("SUM(", Qt::CaseInsensitive)) {
			key_name = key_name.slice(4, -1).trimmed();
			ret = sum(key_name);
		}
		else if(key_name.startsWith("AVG(", Qt::CaseInsensitive)) {
			key_name = key_name.slice(4, -1).trimmed();
			ret = average(key_name);
		}
		else if(key_name.startsWith("CNT(", Qt::CaseInsensitive)) {
			key_name = key_name.slice(4, -1).trimmed();
			ret = rowCount();
		}
		else if(key_name.startsWith("CAPTION(", Qt::CaseInsensitive)) {
			key_name = key_name.slice(8, -1).trimmed();
			int ix = columns().indexOf(key_name);
			if(ix < 0) ret = _key_name;
			else ret = columnHeader(key_name);
		}
		else {
			if(key_ends_with ) ret = d->keyVals.valueKeyEndsWith(key_name, default_val);
			else ret = d->keyVals.value(key_name, default_val);
		}
	}
	//qfInfo() << d->keyVals.element().toString();
	//if(!v.isValid()) v = default_val;
	qfTrash() << "\treturn:" << ret.toString();
	return ret;
}

void QFXmlTable::setValue(const QString &key_name, const QVariant &val)
{
	if(d->keyVals.isNull()) {
		QFDomElement el = d->element.mkcd("keyvals");
		d->keyVals.setElement(el);
	}
	d->keyVals.setValue(key_name, val);
}

QFXmlKeyVals& QFXmlTable::keyValsRef()
{
	QF_ASSERT(!isNull(), "NULL don't hav a keyvals.");
	if(d->keyVals.isNull()) {
		QFDomElement el = d->element.mkcd("keyvals");
		d->keyVals.setElement(el);
	}
	return d->keyVals;
}

QVariant QFXmlTable::sum(const QString &col_name) const
{
	qfTrash() << QF_FUNC_NAME << col_name;
	int ix = columns().indexOf(col_name);
	if(ix >= 0) return sum(ix);
	return QVariant();
}

QVariant QFXmlTable::sum(int col_index) const
{
	qfTrash() << QF_FUNC_NAME << "col index:" << col_index;
	if(col_index < 0 || col_index >= columns().count()) return QVariant();
	QVariant::Type t = columns()[col_index].type;
	QVariant ret;
	//qfInfo() << "type:" << QVariant::typeToName(t);
	if(t == QVariant::Int) {
		int s = 0;
		for(QFXmlTableRow r=firstRow(); !r.isNull(); r=nextRow(r)) { s += r.value(col_index).toInt(); }
		ret = s;
	}
	else {
		double s = 0;
		for(QFXmlTableRow r=firstRow(); !r.isNull(); r=nextRow(r)) { s += r.value(col_index).toDouble(); }
		//qfInfo() << "ret:" << s;
		ret = s;
	}
	return ret;
}

QVariant QFXmlTable::average(const QString & col_name) const
{
	qfTrash() << QF_FUNC_NAME << col_name;
	int ix = columns().indexOf(col_name);
	if(ix >= 0) return average(ix);
	return QVariant();
}

QVariant QFXmlTable::average(int col_index) const
{
	QVariant ret;
	double s = sum(col_index).toDouble();
	int n = rowCount();
	if(n > 0) ret = s / n;
	return ret;
}

static inline QString line_indent(const QString &ind, int level)
{
	QString s;
	for(int i=0; i<level; i++) s += ind;
	return s;
}

#define IND(level) offset + line_indent(ind, level)

QString QFXmlTable::toHtml(const QVariantMap &opts) const
{
	QVariantMap opts2 = opts;
	QString offset = opts.value("lineOffset").toString();
	QString eoln = opts.value("lineSeparator", "\n").toString();
	QString ind = opts.value("lineIndent", "  ").toString();
	QString ret;
	ret += tableName();
	ret += IND(0) + "<table border=1>" + eoln;
	const QFXmlTableColumnList &cols = columns();
	ret += IND(1) + "<tr>" + eoln;
	foreach(const QFXmlTableColumnDef &cd, cols) {
		ret += IND(2) + "<th>";
		QString name = columnHeader(cd.name);
		if(name.isEmpty()) QFSql::parseFullName(cd.name, &name);
		ret += name;
		ret += "</th>" + eoln;
	}
	ret += IND(1) + "</tr>" + eoln;
	for(QFXmlTableRow r = firstRow(); !r.isNull(); r = nextRow(r)) {
		ret += IND(1) + "<tr>" + eoln;
		for(int i=0; i<cols.count(); i++) {
			ret += IND(2) + "<td>";
			ret += r.value(i).toString();
			ret += "</td>" + eoln;
		}
		ret += IND(1) + "</tr>" + eoln;
		opts2["lineOffset"] = IND(3);
		for(QFXmlTable t = r.firstChildTable(); !t.isNull(); t = r.nextSiblingTable(t)) {
			ret += IND(1) + "<tr>" + eoln;
			ret += IND(2) + "<td colspan="IARG(cols.count())">" + eoln;
			ret += t.toHtml(opts2);
			ret += IND(2) + "</td>" + eoln;
			ret += IND(1) + "</tr>" + eoln;
		}
		if(!r.keyVals().isEmpty()) {
			ret += IND(1) + "<tr>" + eoln;
			ret += IND(2) + "<td colspan="IARG(cols.count())">" + eoln;
			opts2["lineOffset"] = IND(3);
			ret += r.keyVals().toHtml(opts2);
			ret += IND(2) + "</td>" + eoln;
			ret += IND(1) + "</tr>" + eoln;
		}
	}
	ret += IND(0) + "</table>" + eoln;
	/// keyvals
	if(!keyVals().isEmpty()) {
		opts2["lineOffset"] = IND(0);
		ret += keyVals().toHtml(opts2);
	}
	return ret;
}

QFBasicTable QFXmlTable::toBasicTable(const QString &column_list) const
{
	qfLogFuncFrame();
	QFBasicTable ret;
	const QFXmlTableColumnList &cols = columns();
	QList<int> ixs;
	if(column_list == "*") {
		for(int i=0; i<cols.count(); i++) ixs.append(i);
	}
	else {
		QSet<QString> sl = QSet<QString>::fromList(column_list.split(',', QString::SkipEmptyParts));
		for(int i=0; i<cols.count(); i++) {
			const QFXmlTableColumnDef &cd = cols[i];
			if(sl.contains(cd.name)) {
				ixs.append(i);
				sl.remove(cd.name);
			}
		}
		if(!sl.isEmpty()) {
			QStringList lst = sl.toList();
			qfError() << "Next column names will not be exported:" << lst.join(", ");
		}
	}
	foreach(int ix, ixs) {
		const QFXmlTableColumnDef &cd = cols[ix];
		QFSqlField fld(cd.name, cd.type);
		ret.fieldsRef().append(fld);
	}
	for(QFXmlTableRow xr = firstRow(); !xr.isNull(); xr = nextRow(xr)) {
		QFBasicTable::Row &br = ret.appendRow();
		int i = 0;
		foreach(int ix, ixs) {
			//qfInfo() << "value:" << xr.value(i).toString();
			br.setValue(i++, xr.value(ix));
		}
		br.post();
	}
	return ret;
}

int QFXmlTable::rowCount() const
{
	int n = 0;
	for(QFXmlTableRow r = firstRow(); !r.isNull(); r = nextRow(r)) n++;
	return n;
}

QFXmlTable QFXmlTable::cd(const QString & path, bool throw_exc) const throw( QFException )
{
	qfLogFuncFrame() << "path:" << path << "throw_exc:" << throw_exc;
	qfTrash() << "\t isNull():" << isNull();
	QFDomElement el = element();
	qfTrash() << "\t el isNull():" << el.isNull();
	qfTrash() << "\t current element tagname:" << el.tagName();
	qfTrash() << "\t current element:" << el.toString();
	QDomDocument doc = el.ownerDocument();
	qfTrash() << "\t document:" << doc.toString(2);
	if(isNull()) {
		if(throw_exc) QF_EXCEPTION("table is NULL");
		return QFXmlTable();
	}
	QFString fs = path;
	if(fs[0] == '/') {
		qfTrash() << "\t Finding ROOT";
		el = doc.documentElement();
		qfTrash() << "\t root element name:" << el.tagName();
		fs = fs.slice(1);
	}
	QStringList sl = fs.split('/', QString::SkipEmptyParts);
	foreach(QString s, sl) {
		qfTrash() << "\t path fragment:" << s;
		if(s == "..") {
			while(true) {
				el = el.parentNode().toElement();
				if(!el) {
					if(throw_exc) throw QFException(QObject::tr("0('%1') path not found.").arg(path));
					return QFXmlTable();
				}
				if(el.tagName() == "table") break;
			}
		}
		else {
			QFDomElement el1 = el.cd("rows", throw_exc);
			if(!el1) {
				if(throw_exc) throw QFException(QObject::tr("1('%1') path not found.").arg(path));
				return QFXmlTable();
			}
			for(el1=el1.firstChildElement("row"); !el1.isNull(); el1=el1.nextSiblingElement("row")) {
				QFDomElement el2 = el1.cd("tables", throw_exc);
				if(!el2) {
					if(throw_exc) throw QFException(QObject::tr("2('%1') path not found.").arg(path));
					return QFXmlTable();
				}
				for(el2=el2.firstChildElement("table"); !el2.isNull(); el2=el2.nextSiblingElement("table")) {
					QString name = el2.attribute("name");
					if(name == s) break;
				}
				if(!el2.isNull()) {el1 = el2; break;}
			}
			if(!el1) {
				if(throw_exc) throw QFException(QObject::tr("3('%1') path not found.").arg(path));
				return QFXmlTable();
			}
			el = el1;
		}
	}
	if(!el) {
		if(throw_exc) throw QFException(QObject::tr("4('%1') path not found.").arg(path));
		return QFXmlTable();
	}
	return QFXmlTable(el);
}

QFXmlTable QFXmlTable::clone(const QString &table_name) const
{
	if(isNull()) return QFXmlTable();
	//QDomDocument doc = d->element.ownerDocument();
	QDomNode nd = d->element.cloneNode(true);
	QDomElement el = nd.toElement();
	if(!el.isNull()) {
		QFXmlTable ret(0);
		el.setAttribute("name", table_name);
		ret.d->element = el;
		ret.d->columnList = d->columnList;
		ret.d->keyVals = d->keyVals;
		return ret;
	}
	return QFXmlTable();
}

//===========================================================
//                                          QFXmlTableDocument
//===========================================================
QFXmlTable QFXmlTableDocument::toTable()
{
	QDomElement el = firstChildElement();
	if(el.isNull()) {
		el = createElement("root");
		appendChild(el);
	}
	QFXmlTable ret(el);
	return ret;
}





