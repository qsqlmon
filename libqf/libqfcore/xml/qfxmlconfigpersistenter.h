#ifndef QFXMLCONFIGPERSISTENTER_H
#define QFXMLCONFIGPERSISTENTER_H

#include <qfcoreglobal.h>
#include <qfxmlconfig.h>

#include <QString>
 
class QFXmlConfigDocument;

//! Abstract interface to make object persisten in XML config.
class QFCORE_DECL_EXPORT QFXmlConfigPersistenter
{
	protected:
		QString f_configId;
		bool persistentDataLoaded;
	protected:
		//! Defaultni implementace vraci qfApp()->config().
		virtual QFXmlConfig* config();
	public:
		virtual QString xmlConfigPersistentId() const {return f_configId;}
		/// pozor vola-li se z konstruktoru, nezavola se loadPersistentData() konstruhovaneho objektu, protoze jeste neexistuje.
		void setXmlConfigPersistentId(const QString &id, bool load_persistent_data = true);

		QString ensurePersistentPath();
		QString persistentPath();
		void setPersistentValue(const QString &path_relative_to_persistent_path, const QVariant &val);
		QVariant persistentValue(const QString &path_relative_to_persistent_path, const QVariant &default_val = QVariant());

		/// pozor, vola-li se z virtualniho destruktoru, zavola se stejne jen pro objekt, ktery se prave destruuje, potomci uz jsou zdestruovany
		virtual void savePersistentData() {}
		virtual void loadPersistentData() {}
	public:
		QFXmlConfigPersistenter(const QString _id = QString());
		virtual ~QFXmlConfigPersistenter();
};

#endif // XMLCONFIGPERSISTENTER_H

