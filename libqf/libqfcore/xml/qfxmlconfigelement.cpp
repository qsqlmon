#include <QVariant>

#include "qfxmlconfigelement.h"

#include <qfxmlkeyvals.h>

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>
/*
QVariant QFXmlConfigElement::value() const throw(QFException)
{
	if(this->isNull()) QF_EXCEPTION("Element is NULL.");
	QVariant v;
	QFDomElement el = firstChildElement("value");
	if(!el.isNull()) {
		v = text2value(el.text());
	}
	//QF_EXCEPTION(TR("Element <%1> does not contain child <value>.").arg(nodeName()));
	return v;
}
*/

bool QFXmlConfigElement::hasValue() const
{
	return !firstChildElement("value").isNull();
}

bool QFXmlConfigElement::hasDefaultValue() const
{
	return !firstChildElement("default").isNull();
}

QVariant QFXmlConfigElement::value(const QVariant &default_val) const
{
	qfLogFuncFrame() << "default_val:" << default_val.toString();
	if(this->isNull()) {
		qfTrash() << "\t NULL node";
		qfTrash() << "\t using default_value parameter:" << default_val.toString();
		return default_val;
	}
	qfTrash() << "\t node name:" << tagName();
	//if(tagName() == "pp38") qfInfo() << toString();
	QFDomElement el = firstChildElement("value");
	if(el.isNull()) {
		if(hasDefaultValue()) {
			qfTrash() << "\t using document defined default value:" << defaultValue().toString();
			return defaultValue();
		}
		qfTrash() << "\t using default_value parameter:" << default_val.toString();
		return default_val;
	}
	QFString s = el.text();
	//if(option("crypted") == "1") s = s.decrypt();
	qfTrash() << "\t found value:" << s << "type:" << type() << attribute("type");
	QVariant v = stringToVariant(s);
	//v = Qf::retypeVariant(v, variantType());
	return v;
}
/*
QVariant QFXmlConfigElement::value() const
{
	if(isNull()) return QVariant();
	QFDomElement el = firstChildElement("value");
	if(el.isNull()) return defaultValue();
	QFString s = el.text();
	//if(option("crypted") == "1") s = s.decrypt();
	return stringToVariant(s);
}
*/
QVariant QFXmlConfigElement::value(const QString &path, const QVariant &default_val) const
{
	if(this->isNull()) return default_val;
	QFDomElement el = cd(path, !Qf::ThrowExc);
	return QFXmlConfigElement(el).value(default_val);
}

void QFXmlConfigElement::setValue(const QVariant &val, bool *changed_flag) throw(QFException)
{
	qfLogFuncFrame();
	if(this->isNull()) QF_EXCEPTION("QFXmlConfigElement::set() - element is NULL");
	bool changed = false;
	QFDomElement el = firstChildElement("value");
	if(el.isNull()) {
		el = ownerDocument().createElement("value");
		appendChild(el);
	}
	QFString s = variantToString(val);
	QString old = el.text();
	qfTrash().noSpace() << "\t old: '" << old << "'";
	qfTrash().noSpace() << "\t new: '" << s << "'";
	if(s != old) {
		el.setText(s);
		changed = true;
	}
	qfTrash() << "\t changed:" << changed;
	if(changed_flag) *changed_flag = changed;
}

QFXmlConfigElement QFXmlConfigElement::setValue(const QString& path, const QVariant &val, bool *changed_flag) throw(QFException)
{
	if(this->isNull()) QF_EXCEPTION("QFXmlConfigElement::set() - element is NULL");
	QFXmlConfigElement el = mkcd(path);
	el.setValue(val, changed_flag);
	return el;
}

QFString QFXmlConfigElement::option(const QString &opt_name, const QString &default_val) const
{
	QFDomElement el = options();
	QFString s = el.attribute(opt_name);
	if(s.isNull()) return default_val;
	return s;
}

void QFXmlConfigElement::setOption(const QString &opt_name, const QString &val)
{
	QFDomElement el = options();
	el.setAttribute(opt_name, val);
}

QFXmlConfigElement::Type QFXmlConfigElement::type() const
{
	if(isNull()) return QFXmlConfigElement::Null;
	QString s = attribute("type");
	if(s.isEmpty()) return QFXmlConfigElement::PathFragment;
	if(s == "text") return QFXmlConfigElement::Text;
	if(s == "bool") return QFXmlConfigElement::Bool;
	if(s == "int") return QFXmlConfigElement::Int;
	if(s == "float") return QFXmlConfigElement::Float;
	if(s == "list") return QFXmlConfigElement::List;
	if(s == "file") return QFXmlConfigElement::File;
	if(s == "dir") return QFXmlConfigElement::Dir;
	/// unknown is text
	return QFXmlConfigElement::Text;
}
/*
QVariant::Type QFXmlConfigElement::variantType() const
{
	QVariant::Type ret = QVariant::Invalid;
	switch(type()) {
		case Bool:
			ret = QVariant::Bool;
			break;
		case Int:
			ret = QVariant::Int;
			break;
		case Float:
			ret = QVariant::Double;
			break;
		default:
			ret = QVariant::String;
			break;
	}
	return ret;
}
*/
bool QFXmlConfigElement::isServiceElement(const QDomElement &el)
{
	static QSet<QString> config_elements;
	if(config_elements.isEmpty()) config_elements << "value" << "default" << "help" << "options";
	return config_elements.contains(el.tagName());
}

bool QFXmlConfigElement::isLeaf() const
{
	//qfTrash() << QF_FUNC_NAME;
	/// nema zadne deti, ktere by sly konfigurovat
	for(QFDomElement el = firstChildElement(); !!el; el = el.nextSiblingElement()) {
		if(isServiceElement(el)) continue;
		return false;
	}
	return true;
}

QString QFXmlConfigElement::caption() const
{
	if(isNull()) return QString();
	QString caption = attribute("caption", nodeName());
	if(!caption.isEmpty()) {
		//qfInfo() << "to localize:" << caption;
		QByteArray ba = caption.toUtf8();
		caption = QCoreApplication::translate("confxml", ba.constData(), 0, QCoreApplication::UnicodeUTF8);
		//qfInfo() << "localized:" << caption; 
	}
	return caption;
}

QString QFXmlConfigElement::unit() const
{
	if(isNull()) return QString();
	return attribute("unit");
}

QString QFXmlConfigElement::help() const
{
	if(isNull()) return QString();
	QFDomElement el = firstChildElement("help");
	if(el.isNull()) return QString();
	return el.text();
}

const QFDomElement QFXmlConfigElement::options() const
{
	if(isNull()) return QFDomElement();
	QFDomElement el = firstChildElement("options");
	return el;
}

QVariant QFXmlConfigElement::defaultValue() const
{
	if(isNull()) return QVariant();
	QFDomElement el = firstChildElement("default");
	if(el.isNull()) return QVariant();
	QString s = el.text();
	//return text2value(s);
	QVariant v = stringToVariant(s);
	//v = Qf::retypeVariant(v, variantType());
	return v;
}

bool QFXmlConfigElement::isCheckable() const
{
	return type() == QFXmlConfigElement::Bool;
}
/*
bool QFXmlConfigElement::isChecked() const
{
	return attribute("checked").toBool();
}

void QFXmlConfigElement::setChecked(bool b)
{
	if(isCheckable()) {
		if(b && hasExclusiveCheck()) {
			QFDomElement el = parentNode().toElement();
			for(el = el.firstChildElement(); !!el; el = el.nextSiblingElement()) {
				el.setAttribute("checked", "false");
			}
		}
		setAttribute("checked", (b)? "true": "false");
	}
}
*/
bool QFXmlConfigElement::childrenHasExclusiveCheck() const
{
	return options().attribute("exclusivecheck").toBool();
}

QString QFXmlConfigElement::variantToString(const QVariant &v)
{
	return QFXmlKeyVals::variantToString(v);
	/// typ je pouze typ editoru, takze ukladam hodnotu v cetne typu, jinak bych neulozil treba QVariantMap
	/*
	QString ret;
	switch(type()) {
		case File:
		case Dir:
		case List:
		case Text:
			ret = v.toString();
			break;
		case Bool:
			ret = (v.toBool())? "1": "0";
			break;
		case Int:
			ret = QString::number(v.toInt());
			break;
		case Float:
			ret = QString::number(v.toDouble());
			break;
		default:
			break;
	}
	return ret;
	*/
}

QVariant QFXmlConfigElement::stringToVariant(const QString &s) const
{
	QVariant ret;
	ret = QFXmlKeyVals::stringToVariant(s);
	/*
	switch(type()) {
		case Bool:
			ret = QFString(s).toBool();
			break;
		case Int:
			ret = s.toInt();
			break;
		case Float:
			ret = s.toDouble();
			break;
		default:
			ret = QFXmlKeyVals::stringToVariant(s);
			break;
	}
	*/
	return ret;
}

QStringList QFXmlConfigElement::childConfigElementNames() const
{
	QStringList sl;
	for(QFDomElement el = firstChildElement(); !!el; el = el.nextSiblingElement()) {
		if(QFXmlConfigElement::isServiceElement(el)) continue;
		sl << el.tagName();
		//QFXmlConfigElement cel = el;
		//if(cel.attribute("hidden").toBool()) return false;
	}
	return sl;
}


