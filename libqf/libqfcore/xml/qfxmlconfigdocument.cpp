#include <QFile>
#include <QDir>
//#include <QApplication>

#include <qffileutils.h>
//#include <qfmessage.h> 

#include "qfxmlconfigdocument.h"

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

//================================================================
//                                              QFXmlConfigDocument
//================================================================
void QFXmlConfigDocument::expandPrototypes() throw( QFException )
{
	expandPrototypes_helper(rootElement());
	//qfInfo() << toString();
}

void QFXmlConfigDocument::expandPrototypes_helper(const QFDomElement & _parent_el) throw( QFException )
{
	static const QString PROTOTYPE = "prototype";
	QFDomElement el_parent = _parent_el;
	for(QFDomElement el = el_parent.firstChildElement(); !!el; el = el.nextSiblingElement()) {
		if(el.hasAttribute(PROTOTYPE)) {
			QString path = el.attribute(PROTOTYPE);
			if(!path.isEmpty()) {
				qfTrash() << "looking for prototype:" << el.path() << "/" << path;
				QFDomElement el_proto = el.cd(path, !Qf::ThrowExc);
				if(!!el_proto) {
					qfTrash() << "el_proto found:" << el_proto.path();
					//QFDomElement el_next = el.nextSiblingElement();
					el_proto = el_proto.cloneNode(true).toElement();
					el_proto.setTagName(el.tagName());
					QFDomElement el_old = el;
					el = el_parent.insertBefore(el_proto, el).toElement();
					el.setAttribute("hidden", false);
					qfTrash() << "el_proto inserted:" << el.path();
					el_parent.removeChild(el_old);
				}
			}
		}
		expandPrototypes_helper(el);
	}
}
