#ifndef QFDOM_H
#define QFDOM_H

#include <QCoreApplication>
#include <QDomDocument>
#include <QSharedDataPointer>

#include <qf.h>
//#include <qfsearchdirs.h>

class QFile;
class QFSearchDirs;
//===================================================================
//                      QFDomNode
//===================================================================
/// Wrapper for QFDomNode. Extended functionality
class QFCORE_DECL_EXPORT QFDomNode : public QDomNode
{
	public:
		QFDomNode(const QDomNode& nd = QDomNode()) : QDomNode(nd) {}

		operator bool() const {return !isNull();}

		QString toString(int indent = 2) const;
};

//===================================================================
//                      QFDomElement
//===================================================================
/// Wrapper for QDomElement. Extended functionality
class QFCORE_DECL_EXPORT QFDomElement : public QDomElement
{
	protected:
		QFDomElement mkcdHelper(const QString& path, bool mkdir, bool throw_exc) const throw(QFException);
	public:
		QFDomElement(const QDomElement& el = QDomElement()) : QDomElement(el) {}

		/// returns element on \a path. Path can be absolute or relative to the current element
		QFDomElement cd(const QString& path, bool throw_exc = Qf::ThrowExc) const throw(QFException);
		/// Ensures that \a path exists.
		/// If \a path exists works like cd(), if it is not, function create necessarry elements.
		/// @return QFDomElement on the \a path
		QFDomElement mkcd(const QString& path) const;

		/// Return XPath to this element;
		QString path() const;

		bool hasAttribute(const QString &attr_name) const;
		QFString attribute(const QString &attr_name, const QString &def_val = QString()) const;
		//void setAttribute(const QString &attr_name, const QString &val);

		/// @sa QDomElement::text(const QString&)
		QString text() const {return QDomElement::text();}
		/// @returns text on path or def_val if the path is invalid
		/// @sa text(const QString&)
		QString text(const QString& path, const QString& def_val = QString()) const;
		void setText(const QString& s);

		void removeChildren();

		operator bool() const {return !isNull();}

		//! Prida vsechny atributy z \a src_el a jeho deti do urovne \a level k vlastnim atributum.
		//! level 0 bez deti
		//! level 1 znamena deti
		//! level 2 znamena deti a jejich deti
		//! @arg only_new pokud element jiz kopirovany atribut ma, neni prepsan
		void copyAttributesFrom(const QDomElement &src_el, int level = 0, bool only_new = false);

		QString toString(int indent = 2) const;
		//bool fromString(const QString &s, QDomDocument *owner_doc, bool throw_exc = Qf::ThrowExc);
};

//===================================================================
//                      QFDomDocument
//===================================================================
/// Wrapper for QDomDocument. Extended functionality
class QFCORE_DECL_EXPORT QFDomDocument : public QDomDocument
{
	Q_DECLARE_TR_FUNCTIONS(QFDomDocument);
	private:
		struct Data : public QSharedData
		{
			QVariantMap properties;
			/// cache id->element
			QMap<QString, QDomElement> idMap;
		};
		QSharedDataPointer<Data> d;
	public:
		static QString includedFromAttributeName;
		static QString includeSrcAttributeName;
	public:
		const QVariantMap& properties() const {return d->properties;}
		QVariantMap& propertiesRef() {return d->properties;}
		
		QVariant property(const QString &prop_name, const QVariant &default_value = QVariant()) const {return d->properties.value(prop_name, default_value);}
		void setProperty(const QString &prop_name, const QVariant &value) {d->properties[prop_name] = value;}
		
		QDomElement findId(const QString &id, bool throw_exc = Qf::ThrowExc) const;
		void copyAttributesFromId(QDomElement &el, const QString &id, int level = 0, bool only_new = false);

		//QString absoluteFileName() const {return d->absolutefilename;}
		/// potrebuju to kvuli include
		/// jaky soubor jsem chtel otevrit (muze to byt relativni cesta)
		QString fileName() const {return property("fileName").toString();}
		void setFileName(const QString &fname) {setProperty("fileName", fname);}
		//void setAbsoluteFileName(const QString &fname) {d->absolutefilename = fname;}
		//bool saveMD5() const {return d->saveMD5;}
		//void setSaveMD5(bool b = true) {d->saveMD5 = b;}
		QByteArray sha1Sum();
		// if md5 is not empty compares current content od document with saved md5
		// \sa saveHash()
		//bool hashChanged() const;
	public:
		/// pokud se to podari, vraci true
		bool setContent(QFile &f, bool throw_exc = Qf::ThrowExc) throw(QFException);
		bool setContent(const QString& s, bool throw_exc = Qf::ThrowExc) throw(QFException);
		bool setContentAndResolveIncludes(const QString& file_path, const QFSearchDirs &search_dirs, bool throw_exc = Qf::ThrowExc) throw(QFException);

		void save(QFile &f, int indent = 2) throw(QFException);
		QString toString(int indent = 2) const;

		inline QFDomElement cd(const QString& path, bool throw_exc = true) const throw(QFException)
		{
			return QFDomElement(documentElement()).cd(path, throw_exc);
		}
		
		inline QFDomElement rootElement() const { return firstChildElement(); }

		QFDomElement mkcd(const QString& path);

		/// vrati novy dokument, ktery je deep copy tohoto
		QFDomDocument clone() const;
		bool isEmpty() const;

	protected:
		void resolveIncludes_helper(const QDomNode &nd, const QFSearchDirs &search_dirs) throw(QFException);
		// pouziva se, pokud si chceme insertnute nody nejak pozmenit, treba poznacit.
		/*
		virtual void resolveIncludesHook(QDomElement &parent_include_element, QDomNode &inserted_node, const QFDomDocument &inserted_document) {
			Q_UNUSED(parent_include_element);
			Q_UNUSED(inserted_node);
			Q_UNUSED(inserted_document);
		}
		*/
		void unresolveIncudes_helper(const QFDomElement &_el, QList<QFDomDocument > &list);
	public:
		//! Nahradi vsechny include elementy v dokumentu jejich obsahem.
		void resolveIncludes(const QFSearchDirs &search_dirs) throw(QFException);
		/// najde vsechny inkludovany soubory a vrati je v listu
		/// 1. prvek listu je hlavni dokument
		QList<QFDomDocument > unresolveIncudes();
		
		QDomElement elementFromString(const QString &s, bool throw_exc = Qf::ThrowExc);
		static QDomElement elementFromString(QDomDocument doc, const QString &s, bool throw_exc = Qf::ThrowExc);
	public:
		QFDomDocument() : QDomDocument()
		{
			d = new Data();
		}
		//! vytvori dokument s documentElement() jmenem \a root_name .
		QFDomDocument(const QString &root_name);
		QFDomDocument(const QDomDocument &doc) : QDomDocument(doc)
		{
			d = new Data();
		}
		virtual ~QFDomDocument() {}
};

/// \file qfdom.h
/*! \relates QFDomNode
 projde rekurzivne vsechny deti XML nodu \a root a zavola pro ne statickou fci zadanou parametrem \a pmf.
 priklad volani: qfDomWalk(firstChild(), domWalk);
 To, ze prochazi vsechny deti a ne \a root_nd , ma svuj smysl, kdyby se prochazel i root, spatne by se
 implementoval pripad, kdy je to potreba udelat jen pro deti. Pokud to je treba udelat i pro root node, staci napsat
 \code
		eraseXmlid_walk(el1, 0);
		qfDomWalk(el1, eraseXmlid_walk, 0);
 \endcode
 @see http://www.glenmccl.com/ptr_cmp.htm
 */
template<typename P>
void qfDomWalk(const QDomNode &root_nd, void (*pmf)(const QDomNode &, P), P param)
{
	QDomNodeList lst = root_nd.childNodes();
	for(int i=0; i<lst.count(); i++) {
		QDomNode nd = lst.item(i);
		pmf(nd, param);
		qfDomWalk(nd, pmf, param);
	}
}

/*! \relates QFDomNode
 projde rekurzivne vsechny deti XML nodu \a root a zavola pro ne member fci zadanou parametrem \a pmf.
 priklad volani: qfDomWalk(firstChild(), this, &MainWindow::domWalk);
 @see http://www.glenmccl.com/ptr_cmp.htm
 */
template<class T, typename P>
void qfDomWalk(const QDomNode &root_nd, T *obj, void (T::*pmf)(const QDomNode &, P), P param)
{
	QDomNodeList lst = root_nd.childNodes();
	for(int i=0; i<lst.count(); i++) {
		QDomNode nd = lst.item(i);
		(obj->*pmf)(nd, param);
		qfDomWalk(nd, obj, pmf, param);
	}
}

#endif // QFDOM_H
