
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFXMLTABLE_H
#define QFXMLTABLE_H

#include <qfcoreglobal.h>

#include <qfdom.h>
//#include <qfexplicitlyshareddata.h>
#include <qfxmlkeyvals.h>

class QFBasicTable;

struct QFXmlTableColumnDef
{
	QString name; //, caption;
	QVariant::Type type;
	//QString halign;
	bool isValid() const {return type != QVariant::Invalid;}
	
	QFXmlTableColumnDef() : type(QVariant::Invalid) {}
};

class QFXmlTableColumnList : public QList<QFXmlTableColumnDef>
{
	protected:
		//QHash<QString, int> nameMap;
	public:
		int indexOf(const QString &col_name) const;
		QFXmlTableColumnDef columnForName(const QString &col_name, bool throw_exc = Qf::ThrowExc) const throw(QFException);
		QFXmlTableColumnDef& appendColumn(const QDomElement &el);
		QStringList columnNames() const;
};

class QFXmlTable;

//! explicitly shared.
//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFXmlTableRow
{
	private:
		struct QFCORE_DECL_EXPORT Data : public QSharedData
		{
			QFDomElement element;
			QStringList dataList;
			QFXmlTableColumnList columns;
			QFXmlKeyVals keyVals;
			bool dataDirty;

			void flush();

			Data() : dataDirty(false) {}
			~Data() {flush();}
		};
		//QExplicitlySharedDataPointer<Data> d;
		QSharedDataPointer<Data> d;

		QFXmlTableRow(int); /// null row constructor
		static const QFXmlTableRow& null();

	protected:
		QStringList& dataRef();
		const QStringList& data() const;
	public:
		const QFDomElement& element() const  {return d->element;}
		bool isNull() const {return element().isNull();}

		const QFXmlKeyVals& keyVals() const {return d->keyVals;}

		QVariant value(int col) const;
		QVariant value(const QString &col_or_key_name) const throw(QFException);
		QVariant value(const QString &col_or_key_name, const QVariant &default_val) const;
		void setValue(int col, const QVariant &val);
		void setValue(const QString &col_or_key_name, const QVariant &val);
		/// Vsechny zmeny v radku jsou zapisovanu do bufferu, tato funkce je prepise do elementu.
		/// Uz neni potreba, data se flushnou v destruktoru Data::~Data().
		//void flushData();
		/// Mrkni na zmeny v datech a pokud jsou zapis je do elementu.
		//void checkUnflushedData() const { if(d->dataDirty) 	const_cast<QFXmlTableRow*>(this)->flushData(); }

		QFXmlTable firstChildTable() const;
		QFXmlTable firstChildTable(const QString &table_name) const;
		QFXmlTable nextSiblingTable(const QFXmlTable &t) const;
		void appendTable(const QFXmlTable &t);

		QString toString() const;
	public:
		QFXmlTableRow();
		QFXmlTableRow(const QFDomElement &el, const QFXmlTableColumnList &cols = QFXmlTableColumnList());
		//! Destruktor radku zapise rovnez vsechny pripadne zmeny do elementu.
		virtual ~QFXmlTableRow() {
			//checkUnflushedData();
		}
};

class QFXmlTableDocument;

//! explicitly shared.
//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFXmlTable
{
	private:
		struct Data : public QSharedData
		{
			QFDomElement element;
			QFXmlTableColumnList columnList;
			QFXmlKeyVals keyVals;
		};
		//QExplicitlySharedDataPointer<Data> d;
		QSharedDataPointer<Data> d;
		
		//! Create empty null table;
		QFXmlTable(int);
		static const QFXmlTable& null();
		void init(const QDomElement &el);
	public:
		QFXmlTableRow firstRow() const;
		/// pokud uz neni dalsi radek, vraceny radek isNull()
		QFXmlTableRow nextRow(const QFXmlTableRow &r) const;
		QFXmlTableRow appendRow();
		/// odstrani radek a vrati radek nasledujici, pokud uz neni vrati null row
		QFXmlTableRow removeRow(const QFXmlTableRow &r);

		const QFXmlTableColumnList& columns() const;
		QFXmlTableColumnList& columnsRef();
		/// vraci element, ktery definuje vlastnosti sloupce
		QDomElement columnElement(const QString &col_name) const;
		void setColumnHeader(const QString &col_name, const QString &header);
		QString columnHeader(const QString &col_name) const;
		void setColumnFooter(const QString &col_name, const QString &footer);
		QString columnFooter(const QString &col_name) const;
		/// pokud je tabulka v reportu generovana z dat, je sloupec takto zarovnan
		void setColumnAlignment(const QString &col_name, Qt::Alignment alignment);
		QString columnHAlignment(const QString &col_name) const;
		
		//! pokud je \a caption == QString(), vezme se to za posledni teckou z \a name nebo cely \a name , kdyz tam neni tecka.
		QFXmlTableColumnDef& appendColumn(const QString &name, QVariant::Type type = QVariant::String, const QString &caption = QString());

		bool isNull() const {return d->element.isNull();}
		//! @param key_ends_with if true key name is compared using function QFSql::endsWith().
		/// key_name muze byt vcetne cesty
		/// pokud se vyskytuje agregacni funkce, musi byt okolo jmena fieldu, napr. soucty/SUM(cena)
		QVariant value(const QString &key_name, const QVariant &default_val = QVariant(), bool key_ends_with = true) const;
		void setValue(const QString &key_name, const QVariant &val);

		QFXmlKeyVals& keyValsRef();
		const QFXmlKeyVals& keyVals() const {return d->keyVals;}

		const QFDomElement& element() const {return d->element;}
		//const QFDomElement& element()  {flushData(); return d->element;}
		QVariant sum(const QString &col_name) const;
		QVariant sum(int col_index) const;
		/// pokud by musel delit 0, vraci QVariant().
		QVariant average(const QString &col_name) const;
		QVariant average(int col_index) const;

		/// bach na ni, vzdy prochazi a pocita vsechny childelementy s tagem row.
		int rowCount() const;

		QString tableName() const {return d->element.attribute("name");}

		/// options:
		/// lineSeparator: cim se budou oddelovat radky, default je '\n'
		/// lineIndent: cim se budou uvozovat vnorene urovne radku, default jsou dve mezery
		/// lineOffset: cim se budou uvozovat vschny radky, default je QString()
		QString toHtml(const QVariantMap &opts = QVariantMap()) const;
		QString toString(int indent = 2) const {return element().toString(indent);}
		QFBasicTable toBasicTable(const QString &column_list = "*") const;

		/// prochazi data dokumentu, cesta musi byt slozena z nazmu tabulek, muze byt i relativni a obsahovat .. (o tabulku vyse)
		/// vraci tabulku na ceste, pokud existuje
		QFXmlTable cd(const QString &path, bool throw_exc = Qf::ThrowExc) const throw(QFException);

		QFXmlTable clone(const QString &table_name = QString()) const;
	public:
		// pozor i sharedNull() je explicitly sdilenej, vsechno, co se do nej ulozi, maji vsechny jeho reference.
		QFXmlTable();
		//! vytvori prazdnou tabulku se jmenem \a table_name a jeji prazdny element, ktery vlastni dokument \a doc .
		QFXmlTable(QDomDocument &doc, const QString &table_name);
		//! predpoklada, ze doc je XML obsahujici data ve formatu XML table
		//QFXmlTable(QDomDocument &doc);
		//! obali \a el tabulkou.
		QFXmlTable(const QDomElement &el);
		//QFXmlTable(const QFXmlTableDocument &doc);
		virtual ~QFXmlTable() {}
};

class QFCORE_DECL_EXPORT QFXmlTableDocument : public QDomDocument
{
	public:
		//QFXmlTableDocument & operator=( const QDomDocument & doc ) {QDomDocument::operator=(doc); return *this;}
		//! vrati tabulku, ktera obrabi tento dokument
		QFXmlTable toTable();
	public:
		QFXmlTableDocument() : QDomDocument() {}
		QFXmlTableDocument(const QDomDocument &doc) : QDomDocument(doc) {}
		~QFXmlTableDocument() {}
};

Q_DECLARE_METATYPE(QFXmlTableDocument);

#endif // QFXMLTABLE_H

