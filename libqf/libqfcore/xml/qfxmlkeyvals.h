
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFXMLKEYVALS_H
#define QFXMLKEYVALS_H

#include <qfcoreglobal.h>
#include <qfdom.h>

#include <QSharedData>

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFXmlKeyVals
{
	private:
		struct Data : public QSharedData
		{
			QFDomElement element;
			QMap<QString, QFDomElement> keyMap;
			bool dirty;

			Data() : dirty(false) {}
		};
		QSharedDataPointer<Data> d;
	protected:
		//const QFDomElement& element() const {return d->element;}
		QDomElement keyValElement(const QString &key) const;
		void checkKeyMap() const;
	public:
		void setElement(const QFDomElement &el);
		QFDomElement element() const {return d->element;}
		bool isNull() const {return d->element.isNull();}
		bool isEmpty() const {checkKeyMap(); return d->keyMap.isEmpty();}

		QStringList keys() const {return d->keyMap.keys();}
		bool contains(const QString &key) const;
		//! staci aby klic koncil na \a key . @see QFSql::endsWith() .
		QVariant valueKeyEndsWith(const QString &key, const QVariant &default_val = QVariant()) const;
		QVariant value(const QString &key, const QVariant &default_val = QVariant()) const;
		void setValue(const QString &key, const QVariant &val);

		static QString variantToString(const QVariant &v);
		static QVariant stringToVariant(const QString &s);

		QString toHtml(const QVariantMap &opts) const;
		QString toString() const;

		bool isDirty() const {return d->dirty;}

		static QString dateTimeStringToIso(const QString &str);

		QFXmlKeyVals& operator+=(const QMap<QString, QVariant> &key_vals);
	public:
		QFXmlKeyVals(const QFDomElement &el = QFDomElement());
		virtual ~QFXmlKeyVals();
};

#endif // QFXMLKEYVALS_H

