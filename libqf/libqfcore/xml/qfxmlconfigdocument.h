
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFXMLCONFIGDOCUMENT_H
#define QFXMLCONFIGDOCUMENT_H

#include <qfcoreglobal.h>

#include <qfxmlconfigelement.h>

#include <qfdom.h>


//! Tady chybi dokumentace.
class QFCORE_DECL_EXPORT QFXmlConfigDocument : public QFDomDocument
{
	protected:
		void expandPrototypes_helper(const QFDomElement &parent_el) throw(QFException);
	public:
		//! Nahradi vsechny include elementy v dokumentu jejich obsahem.
		void expandPrototypes() throw(QFException);
		inline QFXmlConfigElement cd(const QString& path, bool throw_exc = true) const throw(QFException)
		{
			return QFDomDocument::cd(path, throw_exc);
		}
	public:
		QFXmlConfigDocument() : QFDomDocument() {}
		QFXmlConfigDocument(const QFDomDocument &doc) : QFDomDocument(doc) {}
		virtual ~QFXmlConfigDocument() {}
};

   
#endif // QFXMLCONFIGDOCUMENT_H

