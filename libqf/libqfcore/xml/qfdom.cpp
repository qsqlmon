
#include "qfdom.h"

#include <qfexception.h>
#include <qfstring.h>
//#include <qfmd5.h>
#include <qffileutils.h>
#include <qfsearchdirs.h>

#include <QFile>
#include <QStringList>
#include <QDir>
#include <QTextStream>
#include <QTextCodec>
#include <QCryptographicHash>

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>
/*
static void print_owner(const QDomNode &nd, int)
{
	qfInfo() << "node:" << nd.nodeName() << "ownerDocument root:" << nd.ownerDocument().firstChildElement().tagName();
}
*/
//===================================================================
//                      QFDomNode
//===================================================================
QString QFDomNode::toString(int indent) const
{
	QString s;
	QTextStream ts(&s);
	QFDomNode::save(ts, indent);
	return s;
}

//===================================================================
//                      QFDomElement
//===================================================================
QFDomElement QFDomElement::cd(const QString& path, bool throw_exc) const throw(QFException)
{
	return mkcdHelper(path, false, throw_exc);
}

QFDomElement QFDomElement::mkcd(const QString& path) const
{
	return mkcdHelper(path, true, false);
}

QString QFDomElement::path() const
{
	//qfTrash() << QF_FUNC_NAME << tagName();
	if(isNull()) return QString();
	//sl << tagName();
	QStringList sl;
	QFDomElement el = *this;
	while(!!el) {
		sl.prepend(el.tagName());
		el = el.parentNode().toElement();
		//qfTrash() << "\tel:" << el.tagName();
	}
	QString ret;
	if(!sl.isEmpty()) {
		/// vymaz jmeno prvniho z listu, to je root node, ten neni v path uveden
		sl[0] = QString();
		ret = sl.join("/");
	}
	return ret;
}

bool QFDomElement::hasAttribute(const QString &attr_name) const
{
	QDomAttr nd = ((QFDomElement*)this)->attributeNode(attr_name);
	return !nd.isNull();
}

QFString QFDomElement::attribute(const QString &attr_name, const QString &def_val) const
{
	//qfTrash() << QF_FUNC_NAME;
	//qfTrash() << "\n" << toString();
	QDomAttr nd = ((QFDomElement*)this)->attributeNode(attr_name);
	if(nd.isNull()) return def_val;
	return nd.value();
}
/*
void QFDomElement::setAttribute(const QString &attr_name, const QString &val)
{
	QDomAttr nd = attributeNode(attr_name);
	if(nd.isNull()) {
		nd = ownerDocument().createAttribute(attr_name);
		setAttributeNode(nd);
	}
	nd.setValue(val);
}
*/
QString QFDomElement::text(const QString& path, const QString& def_val) const
{
	QFDomElement el = cd(path, false);
	if(el.isNull()) return def_val;
	return el.text();
}

void QFDomElement::setText(const QString& str)
{
	QDomNode nd = firstChild();
	QDomText eltxt = nd.toText();
	if(eltxt.isNull()) {
		eltxt = ownerDocument().createTextNode(str);
		insertBefore(eltxt, nd);
	}
	else {
		eltxt.setData(str);
	}
}

QString QFDomElement::toString(int indent) const
{
	QString s;
	QTextStream ts(&s);
	QDomElement::save(ts, indent);
	return s;
}

QFDomElement QFDomElement::mkcdHelper(const QString& path, bool mkdir, bool throw_exc) const throw(QFException)
{
	//qfLogFuncFrame() << path << "mkdir:" << mkdir;
	if(isNull()) {
		if(throw_exc) QF_EXCEPTION("element is NULL");
		return QFDomElement();
	}
	QDomElement el = *this;
	QDomDocument doc = el.ownerDocument();
	QFString fs = path;
	if(fs[0] == '/') {
		el = doc.firstChildElement();
		//qfTrash() << "\t root element name:" << el.tagName();
		fs = fs.slice(1);
	}
	QStringList sl = fs.split('/', QString::SkipEmptyParts);
	foreach(QString s, sl) {
		//qfTrash() << "\t path fragment:" << s;
		if(s == "..") {
			QFDomElement el1 = el.parentNode().toElement();
			if(!el1) {
				if(throw_exc)
					throw QFException(QObject::tr("QFDomElement::mkcdHelper('%1') path not found and should not be created (param mkdir == false).").arg(path));
				return el1;
			}
			el = el1;
		}
		else {
			QFDomElement el1 = el.firstChildElement(s).toElement();
			if(!el1) {
				//qfTrash() << "\t NOT FOUND";
				if(!mkdir) {
					if(throw_exc) {
						//qfTrash() << "\t doc:" << doc.toString();
						throw QFException(QObject::tr("QFDomElement::mkcdHelper('%1') path not found and should not be created (param mkdir == false).").arg(path));
					}
					return el1;
				}
				el1 = doc.createElement(s); 
				//qfTrash() << QF_FUNC_NAME;
				//qfTrash() << "creating element:" << el1.tagName();
				el.appendChild(el1);
			}
			//else qfTrash() << "\t FOUND";
			el = el1;
		}
	}
	return el;
}

void QFDomElement::removeChildren()
{
	QDomNodeList lst = childNodes();
	for(int i=0; i<lst.count(); i++) {
		removeChild(lst.item(i));
	}
}

static void copyAttributesFrom_helper(QDomElement &dest_el, const QDomElement &src_el, int &level, bool only_new)
{
	if(dest_el.isNull()) return;
	if(src_el.isNull()) return;
	if(0 && level > 0) {
		qfInfo() << "SRC tag name:" << src_el.tagName() << "id:" << src_el.attribute("id") << "level:" << level << "only_new:" << only_new;
		qfInfo() << "DEST tag name:" << dest_el.tagName() << "id:" << dest_el.attribute("id") << "level:" << level;
	}
	QDomNamedNodeMap attrs = src_el.attributes();
	for(int i=0; i<attrs.count(); i++) {
		QDomAttr attr = attrs.item(i).toAttr();
		if(attr.name() == "id") continue;
		//qfInfo() << "setting dest attr name:" << attr.name() << "to:" << attr.value() << " already exists:" << dest_el.hasAttribute(attr.name());
		if(!only_new || (only_new && !dest_el.hasAttribute(attr.name()))) {
			//if(level > 0) { qfInfo() << "setting dest attr name:" << attr.name() << "=" << attr.value(); }
			//!!!! dest_el.setAttributeNode(attr); /// tohle je asi pruser, protoze elementy nemusi nutne pochazet ze stejneho dokumentu
			dest_el.setAttribute(attr.name(), attr.value());
		}
	}
	if(level > 0) {
		QDomElement d_el, s_el;
		for(d_el=dest_el.firstChildElement(), s_el=src_el.firstChildElement(); !d_el.isNull() && !s_el.isNull(); d_el=d_el.nextSiblingElement(), s_el=s_el.nextSiblingElement()) {
			level--;
			copyAttributesFrom_helper(d_el, s_el, level, only_new);
		}
	}
}

void QFDomElement::copyAttributesFrom(const QDomElement & src_el, int level, bool only_new)
{
	//qfLogFuncFrame() << src_el.attribute("id") << "level:" << level;
	copyAttributesFrom_helper(*this, src_el, level, only_new);
}

//===================================================================
//                       QFDomDocument
//===================================================================
QString QFDomDocument::includedFromAttributeName = "_includedFrom";
QString QFDomDocument::includeSrcAttributeName = "_includeSrc";

QFDomDocument::QFDomDocument(const QString & root_name)
	: QDomDocument()
{
	d = new Data();
	QByteArray ba = "<?xml version='1.0' encoding='UTF-8'?><" + root_name.toAscii() + "/>";
	QDomDocument::setContent(ba);
			//QDomElement el = createElement(root_name);
			//appendChild(el);
}

bool QFDomDocument::setContent(QFile &f, bool throw_exc) throw(QFException)
{
	qfLogFuncFrame() << f.fileName();
	if(!f.isOpen()) {
		if(!f.open(QIODevice::ReadOnly)) {
			QString s = QObject::tr("ERROR open file '%1'").arg(f.fileName());
			//qCritical() << s;
			if(throw_exc) QF_EXCEPTION(s);
			qfWarning() << s;
			return false;
		}
		//qfTrash() << "\topening:" << f.fileName() << f.isOpen();
		//qfTrash() << "\tcontent:" << f.readAll();
		//f.seek(0);
	}

	QString err_msg;
	int err_line, err_col;
	if(!QDomDocument::setContent(&f, false, &err_msg, &err_line, &err_col)) {
		err_msg = tr("ERROR ") + f.fileName()
				+ QF_EOLN + QF_EOLN
				+ tr("line: %1 ").arg(err_line)
				+ tr("col: %1").arg(err_col)
				+ QF_EOLN + QF_EOLN
				+ err_msg;
		if(throw_exc) QF_EXCEPTION(err_msg);
		qfWarning() << err_msg;
		return false;
	}

	setFileName(f.fileName());
	return true;
}

bool QFDomDocument::setContent(const QString &s, bool throw_exc) throw(QFException)
{
	qfLogFuncFrame();
	if(s.isEmpty()) {
		*this = QFDomDocument();
		return true;
	}
	QString err_msg;
	int err_line, err_col;
	if(!QDomDocument::setContent(s, false, &err_msg, &err_line, &err_col)) {
		err_msg = QObject::tr("ERROR", "QFDomDocument::setContent")
				+ QF_EOLN + QF_EOLN
				+ QObject::tr("line: %1 ", "QFDomDocument::setContent").arg(err_line)
				+ QObject::tr("col: %1", "QFDomDocument::setContent").arg(err_col)
				+ QF_EOLN + QF_EOLN
				+ err_msg;
		//qfInfo() << s;
		if(throw_exc) QF_EXCEPTION(err_msg);
		qfWarning() << err_msg;
		return false;
	}
	return true;
}

bool QFDomDocument::setContentAndResolveIncludes(const QString & file_path, const QFSearchDirs & search_dirs, bool throw_exc) throw( QFException )
{
	qfLogFuncFrame() << file_path;
	bool ret = false;
	//setAbsoluteFileName(QString());
	QString abs_file_path = search_dirs.findFile(file_path);
	if(abs_file_path.isEmpty()) {
		if(throw_exc) QF_EXCEPTION(tr("File '%1' can not be resolved trying %2.").arg(file_path).arg(search_dirs.dirs().join(", ")));
	}
	else {
		QFile f(abs_file_path);
		ret = setContent(f, throw_exc);
		if(ret) {
			setFileName(file_path); /// pokud je dokument oteviran s relativni cestou, musi byt i zde cesta relativni, jinak nebude fungovat resolveIncludes()
			if(throw_exc) {
				resolveIncludes(search_dirs);
			}
			else try {
				resolveIncludes(search_dirs);
			}
			catch(QFException &e) {
				qfWarning() << e.msg();
			}
			//setAbsoluteFileName(f.fileName());
		}
	}
	return ret;
}

QString QFDomDocument::toString(int indent) const
{
	QString s;
	{
		QTextStream ts(&s);
		QDomDocument::save(ts, indent);
	}
	s = s.replace("'", "\""); /// aby to slo ulozit do SQL jako string ohraniceny apostrofy
	return s;
}

void QFDomDocument::save(QFile &f, int indent) throw(QFException)
{
	if(!f.isOpen()) {
		if(!f.open(QIODevice::WriteOnly)) {
			QString s = QObject::tr("QFDomDocument::save", "QFXmlTreeModel::save(QFile &f): ERROR open file '%1'").arg(f.fileName());
			//qCritical() << s;
			throw QFException(s);
		}
	}
	QTextStream ts(&f);
	QDomDocument::save(ts, indent);
	setFileName(f.fileName());
}

QByteArray QFDomDocument::sha1Sum()
{
	QCryptographicHash hash(QCryptographicHash::Sha1);
	hash.addData(toString(0).toUtf8());
	return hash.result();
	//qfTrash().noSpace() << "\tMD5: " << s1;
}
/*
bool QFDomDocument::hashChanged() const
{
	if(d->md5.size() == 0) return false; // md5 not saved
	QByteArray ba = QFMD5::md5(toString(0).toUtf8());
	qfTrash() << QF_FUNC_NAME;
	qfTrash() << "\told hash:" << QFMD5::digestToString(d->md5);
	qfTrash() << "\tnew hash:" << QFMD5::digestToString(ba);
	return  ba != d->md5;
}
*/

QFDomDocument QFDomDocument::clone() const
{
	qfLogFuncFrame();
#if 0
	/// tohle mi v Qt 4.4.3 pada nevim proc a souvisi to podle me s resolveIncludes
	qfDomWalk(*this, print_owner, 0);
	QDomDocument doc = cloneNode(true).toDocument();
#else
	/// takze pouzivam dar pritele Messera
	QString s = toString(0);
	QDomDocument doc;
	doc.setContent(s);
#endif
	return doc;
}

bool QFDomDocument::isEmpty() const
{
	//qfInfo() << "isNull():" << isNull() << "isDocument:" << isDocument();
	return documentElement().isNull();
}

QFDomElement QFDomDocument::mkcd(const QString& path)
{
	if(isEmpty()) {
		//QString s = documentName();
		//if(s.isEmpty()) s = "root";
		QDomElement el = createElement("root");
		appendChild(el);
	}
	return QFDomElement(documentElement()).mkcd(path);
}
/*
static QString recent_include_file(const QDomElement &_el)
{
	qfLogFuncFrame();
	QFString ret;
	QFDomElement el = _el;
	while(!!el) {
		QFString attr = el.attribute(QFDomDocument::includedFromAttributeName);
		qfTrash() << "\t element:" << el.tagName() << "path:" << el.path();
		if(!!attr) {
			ret = attr;
			break;
		}
		el = el.parentNode().toElement();
	}
	qfTrash() << "\t return:" << ret;
	return ret;
}
*/
void QFDomDocument::resolveIncludes_helper(const QDomNode &_parent_nd, const QFSearchDirs &search_dirs) throw(QFException)
{
	//qfLogFuncFrame();
	//QString current_path = _current_path;
	//if(current_path.isEmpty()) current_path = QFFileUtils::path(fileName());
	//qfTrash() << "\tcurrent path:" << current_path;
	QDomNode nd_parent = _parent_nd;
	while(1) {
		bool include_found = false;
		for(QFDomElement el=nd_parent.firstChildElement(); !!el; ) {
			//qfTrash() << "\ttag:" << el.tagName();
			if(el.tagName() == "include") {
				qfTrash().color(QFLog::Yellow) << QF_FUNC_NAME << "\tcurrent element path:" << el.path();
				include_found = true;
				QFString file_name = el.attribute("src").trimmed();
				if(!file_name) file_name = el.attribute("href").trimmed();
				if(!file_name) QF_EXCEPTION("Empty or missing 'src' or 'href' attribute in <include> element.");
				QFString absolute_file_name = file_name;
				QString include_src = file_name;
				//qfTrash() << "\tsrc:" << s << "is relative:" << QDir::isRelativePath(s);
				//qfTrash() << "\tfileName:" << fileName();
				//qfTrash() << "\tQFFileUtils::path(fileName()):" << QFFileUtils::path(fileName());
				//qfTrash() << "\tQFFileUtils::joinPath(QFFileUtils::path(fileName()), s):" << QFFileUtils::joinPath(QFFileUtils::path(fileName()), s);
				qfTrash() << "\t included file src:" << file_name;
				bool global_include = false;
				if(file_name.value(0) == '[' && file_name.value(-1) == ']') { /// nemuzu pouzit <> jako v C, protoze XML atribut nesmi obsahovat znaky '<' a '&'
					/// hleda se relativne k searchDirs
					file_name = file_name.slice(1, -1);
					global_include = true;
				}
				if(QDir::isRelativePath(file_name)) {
					if(global_include) {
						/// hleda se relativne k searchDirs
						absolute_file_name = search_dirs.findFile(file_name);
					}
					else {
						/// zjisti z jakeho souboru je nainkludovan soucasny dokument
						QString included_from_file = /*recent_include_file(el);
						if(included_from_file.isEmpty()) included_from_file =*/ fileName();
						qfTrash() << "\t included_from_file:" << included_from_file;
						QString included_from_path = QFFileUtils::path(included_from_file);
						qfTrash() << "\t included_from_path:" << included_from_path;
						file_name = QFFileUtils::joinPath(included_from_path, file_name);
						qfTrash() << "\t searchng for file:" << file_name;
						if(QDir::isRelativePath(file_name)) {
							//qfTrash() << "\t relative path";
							/// pokud je soubor inkludovan ze souboru s relativni cestou, hledej ho na vsech cestach (takhle si delam unionfs pro reporty)
							absolute_file_name = search_dirs.findFile(file_name);
						}
						else {
							//qfTrash() << "\t absolute path";
							absolute_file_name = file_name;
						}
					}
				}
				else {
					absolute_file_name = file_name;
				}
				if(absolute_file_name.isEmpty()) QF_EXCEPTION(tr("File '%1' can not be resolved trying %2.").arg(file_name).arg(search_dirs.dirs().join(", ")));
				QFile f(absolute_file_name);
				qfTrash() << "\t resolved file name:" << absolute_file_name;
				QFDomDocument doc;
				doc.setContentAndResolveIncludes(file_name, search_dirs, Qf::ThrowExc);
				QFDomElement included_doc_root_el = doc.firstChildElement();
				//qfTrash() << "\tfirst child:" << el1.tagName();
				//QDomDocumentFragment frag;
				QDomElement el_next = el.nextSiblingElement();
				for(QDomElement imported_el = included_doc_root_el.firstChildElement(); !imported_el.isNull(); imported_el = imported_el.nextSiblingElement()) {
					QFDomElement el_to_insert = importNode(imported_el, true).toElement();
					//qfDomWalk(el_to_insert, print_owner, 0);
					if(!!el_to_insert) {
						el_to_insert.setAttribute(QFDomDocument::includedFromAttributeName, file_name);
						el_to_insert.setAttribute(QFDomDocument::includeSrcAttributeName, include_src);
						qfTrash().color(QFLog::Yellow) << "\t including element:" << el_to_insert.tagName() << "from:" << el_to_insert.attribute(QFDomDocument::includedFromAttributeName);
					}
					//resolveIncludesHook(el, nd_to_insert, doc);
					/// vloz pred element <include ... >
					nd_parent.insertBefore(el_to_insert, el);
				}
				/// vymaz element <include ... >, protoze misto neho uz je obsah inkludovaneho souboru
				nd_parent.removeChild(el);
				el = el_next;
				//qfInfo() << this->toString();
			}
			else {
				resolveIncludes_helper(el, search_dirs);
				el=el.nextSiblingElement();
			}
		}
		if(!include_found) break;
	}
}

void QFDomDocument::resolveIncludes(const QFSearchDirs &search_dirs) throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	resolveIncludes_helper(*this, search_dirs);
	//qfTrash() << toString();
}

QDomElement QFDomDocument::elementFromString(QDomDocument doc, const QString &s, bool throw_exc)
{
	/*
	if(!import_to_document) {
	if(throw_exc) QF_EXCEPTION("ownerDocument is NULL");
	return false;
}
	*/
	QDomDocument doc1;
	if(!doc1.setContent(s)) {
		if(throw_exc) QF_EXCEPTION("Error seting element content");
		return QDomElement();
	}
	QDomNode nd = doc.importNode(doc1.documentElement(), true);
	QDomElement el = nd.toElement();
	return el;
}

QDomElement QFDomDocument::elementFromString(const QString &s, bool throw_exc)
{
	return elementFromString(*this, s, throw_exc);
}

static QDomElement findId_helper(const QDomElement &el, const QString &id)
{
	if(el.attribute("id") == id) return el;
	for(QDomElement el1=el.firstChildElement(); !el1.isNull(); el1=el1.nextSiblingElement()) {
		QDomElement el2 = findId_helper(el1, id);
		if(!el2.isNull()) return el2;
	}
	return QDomElement();
}

QDomElement QFDomDocument::findId(const QString & id, bool throw_exc) const
{
	QDomElement ret;
	if(d->idMap.contains(id)) {
		ret = d->idMap.value(id);
	}
	else {
		ret = findId_helper(rootElement(), id);
		if(ret.isNull()) {
			if(throw_exc) QF_EXCEPTION(QString("Id '%1' not found.").arg(id));
		}
		const_cast<QFDomDocument*>(this)->d->idMap[id] = ret;
	}
	return ret;
}

void QFDomDocument::copyAttributesFromId(QDomElement & _el, const QString & id, int level, bool only_new)
{
	QDomElement src_el = findId(id, !Qf::ThrowExc);
	QFDomElement el(_el);
	el.copyAttributesFrom(src_el, level, only_new);
}

void QFDomDocument::unresolveIncudes_helper(const QFDomElement &_el, QList<QFDomDocument > &list)
{
	QDomElement el_parent = _el;
	QMap<QString, QList<QDomElement> > included_elements;
	QString current_include_src;
	for(QFDomElement el=el_parent.firstChildElement(); !!el; el = el.nextSiblingElement()) {
		QString s = el.attribute(QFDomDocument::includedFromAttributeName);
		if(!s.isEmpty()) {
			QString include_src = el.attribute(QFDomDocument::includeSrcAttributeName);
			if(!include_src.isEmpty() && include_src != current_include_src) {
				QDomElement el_inc = createElement("include");
				el_inc.setAttribute("src", include_src);
				el_parent.insertBefore(el_inc, el);
				current_include_src = include_src;
			}
			included_elements[s] << el;
			//el_parent.removeChild(el);
		}
		unresolveIncudes_helper(el, list);
	}
	QMap< QString, QDomDocument > included_documents;
	foreach(QString fn, included_elements.keys()) {
		QFDomDocument doc("inc");
		doc.setFileName(fn);
		QDomElement doc_root_el = doc.rootElement();
		QList<QDomElement> lst = included_elements.value(fn);
		foreach(QDomElement el, lst) {
			QDomNode nd = el_parent.removeChild(el);
			nd = doc.importNode(nd, true);
			el = nd.toElement();
			el.removeAttribute(QFDomDocument::includeSrcAttributeName);
			el.removeAttribute(QFDomDocument::includedFromAttributeName);
			doc_root_el.appendChild(el);
		}
		list << doc;
		//included_documents[fn] = doc;
	}
}

QList<QFDomDocument > QFDomDocument::unresolveIncudes()
{
	//QString main_doc_fn = _main_doc_fn;
	//if(main_doc_fn.isEmpty()) main_doc_fn = fileName();
	//if(main_doc_fn.isEmpty()) main_doc_fn = "[maindoc]";
	QList<QFDomDocument > ret;
	QFDomDocument doc = clone();
	ret << doc;
	unresolveIncudes_helper(doc.rootElement(), ret);
	return ret;
}






