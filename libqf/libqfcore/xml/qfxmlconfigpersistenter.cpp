#include "qfxmlconfigpersistenter.h"

#include <qfappconfiginterface.h>
#include <qfxmlconfigdocument.h>
#include <qffileutils.h>

#include <QRect>

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

QFXmlConfigPersistenter::QFXmlConfigPersistenter(const QString _id) 
{
	f_configId = _id;
	persistentDataLoaded = false;
}

QFXmlConfigPersistenter::~QFXmlConfigPersistenter()
{
	qfTrash() << QF_FUNC_NAME;
}
		
void QFXmlConfigPersistenter::setXmlConfigPersistentId(const QString &id, bool load_persistent_data)
{
	f_configId = id;
	if(load_persistent_data && !xmlConfigPersistentId().isEmpty()) loadPersistentData();
}
/*
QFXmlConfigElement QFXmlConfigPersistenter::makePersistentPath(const QFXmlConfigDocument &_doc)
{
	qfTrash() << QF_FUNC_NAME;
	//qfTrash() << "\tdoc is empty:" << doc.isEmpty();
	if(xmlConfigPersistentId().isEmpty()) return QFXmlConfigElement();
	QFXmlConfigDocument doc = _doc;
	if(doc.isEmpty()) doc = qfApp()->config();
	if(doc.isEmpty()) return QFXmlConfigElement();
	QFXmlConfigElement el = doc.createAppInternalsPath();
	el = el.mkcd(xmlConfigPersistentId());
	return el;
}

QFXmlConfigElement QFXmlConfigPersistenter::persistentPath(const QFXmlConfigDocument &_doc) const
{
	qfTrash() << QF_FUNC_NAME;
	if(xmlConfigPersistentId().isEmpty()) return QFXmlConfigElement();
	QFXmlConfigDocument doc = _doc;
	if(doc.isEmpty()) doc = qfApp()->config();
	if(doc.isEmpty()) return QFXmlConfigElement();
	QFXmlConfigElement el = doc.appInternalsPath();
	el = el.cd(xmlConfigPersistentId(), !Qf::ThrowExc);
	return el;
}
*/
		
QFXmlConfig* QFXmlConfigPersistenter::config()
{
	//qfLogFuncFrame();
	QCoreApplication *core_app = QCoreApplication::instance();
	QFAppConfigInterface *ifc = dynamic_cast<QFAppConfigInterface*>(core_app); /// cross cast
	if(ifc) return ifc->config();
	QF_EXCEPTION("No QFAppConfigInterface");
	return NULL;
}

QString QFXmlConfigPersistenter::ensurePersistentPath()
{
	QString ret;
	QFXmlConfigElement el = config()->createPersistentPath(xmlConfigPersistentId());
	//qfInfo() << "created" << el.path();
	if(!!el) ret = el.path();
	return ret;
}
		
QString QFXmlConfigPersistenter::persistentPath()
{
	QString ret;
	QFXmlConfigElement el = config()->persistentPath(xmlConfigPersistentId());
	if(!!el) ret = el.path();
	//qfInfo() << "xmlConfigPersistentId:" << xmlConfigPersistentId() << "path:" << ret;
	return ret;
}

void QFXmlConfigPersistenter::setPersistentValue(const QString & path_relative_to_persistent_path, const QVariant & val)
{
	qfLogFuncFrame() << "path_relative_to_persistent_path:" << path_relative_to_persistent_path << "value:" << QFXmlConfigElement::variantToString(val);
	QString path = ensurePersistentPath();
	//qfInfo() << "ensured:" << path;
	if(!path.isEmpty()) {
		path = QFFileUtils::joinPath(path, path_relative_to_persistent_path);
		qfTrash() << path << "=" << QFXmlConfigElement::variantToString(val);
		config()->setValue(path, val);
	}
}

QVariant QFXmlConfigPersistenter::persistentValue(const QString & path_relative_to_persistent_path, const QVariant & default_val)
{
	qfLogFuncFrame() << "path_relative_to_persistent_path:" << path_relative_to_persistent_path << "default_val:" << default_val.toString();
	QVariant ret = default_val;
	QString path = persistentPath();
	if(!path.isEmpty()) {
		path = QFFileUtils::joinPath(path, path_relative_to_persistent_path);
		qfTrash() << "path:" << path;
		ret = config()->value(path, default_val);
	}
	qfTrash() << "\t return:" << ret.toString();
	return ret;
}
