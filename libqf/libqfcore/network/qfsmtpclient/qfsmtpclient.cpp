
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsmtpclient.h"

#include <qf.h>
#include <qffileutils.h>
#include <qfqencoding.h>

#include <QTcpSocket>
#include <QSslSocket>
#include <QHostInfo>
#include <QFile>

#include <qflogcust.h>

QFSmtpClient::QFSmtpClient(const QString &server, int port, QFSmtpClient::SocketType sock_type)
{
	qfLogFuncFrame() << server << port;
	f_socketType = sock_type;
	setId(0);
	f_mailId = 0;
	serverAddress = server;
	serverPort = port;

	socket = NULL;
	f_socketStream = NULL;
	if(socketType() == SocketTCP) {
		qfTrash() << "\t creating TCP socket";
		if(serverPort == 0) serverPort = 25;
		socket = new QTcpSocket( this );
		connect ( socket, SIGNAL( connected() ), this, SLOT( connected() ) );
	}
#ifndef QF_SMTPCLIENT_NO_SSL

	else if(socketType() == SocketSSL) {
		qfTrash() << "\t creating SSL socket";
		if(serverPort == 0) serverPort = 465;
		socket = new QSslSocket( this );
		connect ( socket, SIGNAL( encrypted() ), this, SLOT( connected() ) );
		connect(socket, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(sslErrors(QList<QSslError>)));
	}
#endif
	connect ( socket, SIGNAL( readyRead() ), this, SLOT( readyRead() ) );
	state = Init;
}

QFSmtpClient::~QFSmtpClient()
{
	SAFE_DELETE(f_socketStream);
	delete socket;
}

QTextStream * QFSmtpClient::socketStream() throw( QFException )
{
	if(!f_socketStream) {
		if(socket == NULL) QF_EXCEPTION(tr("Socket is NULL."));
		f_socketStream = new QTextStream(socket);
	}
	return f_socketStream;
}

void QFSmtpClient::setContent(const QString &from, const QString &to,
			      const QString &subject, const QString &_body, const AttachmentList &attachments)
{
	static const QByteArray boundary = "--------umGAiCGouB62q0Xi4nGngu";
	QByteArray body;
	QByteArray s =  "Content-Type: multipart/mixed; boundary=\"${BOUNDARY}\"\n"
			"MIME-Version: 1.0\n";
	body += s.replace("${BOUNDARY}", boundary);
	
	s = "\nThis is a message with multiple parts in MIME format."
			"\n--${BOUNDARY}\n"
			"Content-Type: text/plain; charset=\"utf-8\"\n"
			//"Content-Type: text/plain; format=flowed; delsp=yes; charset=\"utf-8\"\n"
			"MIME-Version: 1.0\n"
			"Content-Transfer-Encoding: base64\n\n";
	body += s.replace("${BOUNDARY}", boundary);

	body += _body.toUtf8().toBase64();

	s = "\n\n--${BOUNDARY}";
	body += s.replace("${BOUNDARY}", boundary);

	foreach(Attachment att, attachments) {
		QByteArray content = att.content;
		if(!att.isLoaded()) {
			QFile f(att.fileName);
			if(f.open(QIODevice::ReadOnly)) {
				content = f.readAll();
			}
			else {
				qfWarning() << tr("Attachment file '%1' can't be open for reading.");
				continue;
			}
		}
		{
			s = "\nContent-Disposition: attachment; filename=${FILE_NAME}\n"
					"Content-Type: application/${APPLICATION}; name=${FILE_NAME}\n"
					"MIME-Version: 1.0\n"
			"Content-Transfer-Encoding: Base64\n\n";
			//QByteArray fname = QFFileUtils::file(att.fileName).toLatin1();
			QByteArray encoded_fname = QFQEncoding::encodeMessageHeaderUtf8(att.fileName);
			body += s.replace("${FILE_NAME}", encoded_fname)
					.replace("${APPLICATION}", QFFileUtils::extension(att.fileName).toLatin1());

			QByteArray ba;
			const int chunk_len = 48;
			for(int i=0; i<content.size(); i += chunk_len) {
				if(i > 0) ba += '\n';
				ba += content.mid(i, chunk_len).toBase64();
			} 
			body += ba;

			s = "\n\n--${BOUNDARY}";
			body += s.replace("${BOUNDARY}", boundary);
		}
	}
	body += "--\n"; /// The last boundary must have two hyphens at the end.

	message = "From: " + from.toLatin1() +
			"\nTo: " + to.toLatin1() +
			"\nSubject: " + QFQEncoding::encodeMessageHeaderUtf8(subject) +
			"\nDate: " + QDateTime::currentDateTime().toString().toAscii() +
			"\nMessage-ID: <welloffice." + QString::number(mailId()).toAscii() + "@" + from.section('@', 1).toAscii() + ">" +
			"\n" + body + "\n";

	//message = body + '\n';
	message.replace("\n", "\r\n");
	message.replace("\r\n.\r\n", "\r\n..\r\n");

	//qfInfo() << message;

	this->from = from;
	//this->to = to;
	rcpts = QFString(to).splitAndTrim(',');
	//qfInfo() << "rcpts:" << rcpts.join(",") << "to:" << to;
}

void QFSmtpClient::sendAndDestroy()
{
	emit status(id(), StatusStart, tr("Transfer started"));
	//QString addr = rcpt.mid(rcpt.indexOf( '@' )+1);
	QHostInfo::lookupHost(serverAddress, this, SLOT(dnsLookupHelper(QHostInfo)));
}

void QFSmtpClient::dnsLookupHelper(const QHostInfo &host)
{
	qfTrash() << QF_FUNC_NAME;
	if (host.error() != QHostInfo::NoError) {
		emit status(id(), StatusError, tr( "Server lookup failed:\n\n" ) + host.errorString());
		//qfDebug() << "Lookup failed:" << host.errorString();
		return;
	}
	else {
		connectionTimeoutTimerId = startTimer(60000); /// mas minutu na pripojeni
		QHostAddress address = host.addresses()[0];
		qfDebug() << "\tconnecting to" << address.toString();
		emit status(id(), StatusInfo, tr("Connecting to %1").arg(address.toString()));

		if(socketType() == SocketTCP) {
			socket->connectToHost(address.toString(), serverPort);
		}
#ifndef QF_SMTPCLIENT_NO_SSL
		else if(socketType() == SocketSSL) {
			if (!QSslSocket::supportsSsl()) {
				emit status(id(), StatusError, tr( "Secure Socket Client:\n\nThis system does not support OpenSSL." ));
				return;
			}
			sslSocket()->connectToHostEncrypted(address.toString(), serverPort);
		}
#endif
	}
}

void QFSmtpClient::timerEvent(QTimerEvent *event)
{
	qfDebug() << "Connection timeout Timer ID:" << event->timerId();
	emit status(id(), StatusError, tr("Server connect timeout."));
	delete this;
}

void QFSmtpClient::connected()
{
	qfTrash() << QF_FUNC_NAME;
	killTimer(connectionTimeoutTimerId);
	qfDebug() << "\tconnected to" << socket->peerName();
	emit status(id(), StatusInfo, tr( "Connected to %1" ).arg( socket->peerName() ) );
}

void QFSmtpClient::sendAnswer(const QByteArray & ans)
{
	QString info = line.trimmed();
	info.replace("<CR>", QString());
	info.replace("<LF>", QString());
	emit status(id(), StatusInfo, info);
	if(state == Body) qfDebug() << "\tC:" << ans.mid(100) + " ... may be trimmed";
	else qfDebug() << "\tC:" << ans;
	*socketStream() << ans + "\r\n";
	socketStream()->flush();
}

void QFSmtpClient::readyRead()
{
	//qfTrash() << QF_FUNC_NAME;
    	/// SMTP is line-oriented
	if(!socket->canReadLine()) return;
/*
	ehlo me
	250-out.smtp.cz
	250-PIPELINING
	250-SIZE 62914560
	250-ETRN
	250-STARTTLS
	250-AUTH PLAIN LOGIN
	250-AUTH=PLAIN LOGIN
	250 8BITMIME
*/
	response = QString();
	line = QString();
	do {
		line = socket->readLine();
		if(line.length() < 4) {
			emit status(id(), StatusError, tr("Unexpected server answer line '%1' (too short).").arg(line));
			delete this;
			return;
		}
		response += line;
	} while( socket->canReadLine() && line[3] != ' ' ); /// uz tam zadnej radek neni nebo posledni prectenej radek nema za kodem pomlcku
	qfDebug() << "\tS:" << response.trimmed();
	QString code = line.mid(0, 3);

	if ( state == Init && code[0] == '2' ) {
	        /// banner was okay, let's go on
		sendAnswer("ehlo me");
		if(authMethod == AuthNone) state = Mail;
		else state = Auth;
	}
	else  if ( state == Auth && code[0] == '2' ) {
		if(authMethod != AuthLogin) QF_EXCEPTION(tr("Only AUTH LOGIN is currently supported."));

		sendAnswer("auth login");
		state = AuthLoginUser;
	}
	else  if ( state == AuthLoginUser && code[0] == '3' ) {
		QByteArray ba = authParams.value("login").toByteArray();
		QByteArray s = ba.toBase64();
		sendAnswer(s);
		state = AuthLoginPassword;
	}
	else  if ( state == AuthLoginPassword && code[0] == '3' ) {
		QByteArray ba = authParams.value("password").toByteArray();
		QByteArray s = ba.toBase64();
		sendAnswer(s);
		state = Mail;
	}
	else  if ( state == Mail && code[0] == '2' ) {
 	       /// HELO response was okay (well, it has to be)
		sendAnswer("mail from: <" + from.toLatin1() + ">");
		state = Rcpt;
	}
	else if ( state == Rcpt && code[0] == '2' ) {
		//qfInfo() << "rcpts:" << rcpts.join(",");
		if(rcpts.count()) {
			QByteArray rcpt = rcpts.takeFirst().toLatin1();
			sendAnswer("rcpt to: <" + rcpt + ">");
			if(rcpts.isEmpty()) state = Data;
		}
		else QF_EXCEPTION("Empty recipient list.");		
	}
	else if ( state == Data && code[0] == '2' ) {
		sendAnswer("data");
		state = Body;
	}
	else if ( state == Body && code[0] == '3' ) {
		sendAnswer(message + "\r\n.");
		state = Quit;
	}
	else if ( state == Quit && code[0] == '2' ) {
		sendAnswer("quit");
	        /// here, we just close.
		state = Close;
		emit status(id(), StatusOk, tr( "Message sent" ) );
		//qfInfo() << "emit sent(f_mailId, StatusOk)";
		emit sent(f_mailId, StatusOk);
	}
	else if ( state == Close ) {
		emit status(id(), StatusInfo, "Deleteing client " + line);
		qfTrash() << "\tdelete this";
		delete this;
		return;
	}
	else {
        // something broke.
		/*
		QMessageBox::warning( qApp->activeWindow(),
							  tr( "Qt Mail Example" ),
							  tr( "Unexpected reply from SMTP server:\n\n" ) +
									  response );
		*/
		emit status(id(), StatusError, tr( "Unexpected reply from SMTP server:\n\n" ) + response );
		emit sent(f_mailId, StatusError);
		state = Close;
		delete this;
		return;
	}
}

#ifndef QF_SMTPCLIENT_NO_SSL
void QFSmtpClient::sslErrors(QList< QSslError > errors)
{
	Q_UNUSED(errors);
	foreach (const QSslError &error, errors) qfDebug() << error.errorString();
	sslSocket()->ignoreSslErrors();
}

QSslSocket * QFSmtpClient::sslSocket() throw( QFException )
{
	QSslSocket *ssl_sock = qobject_cast<QSslSocket*>(socket);
	if(!ssl_sock) QF_EXCEPTION(tr("Socket is NULL or not type of QSslSocket."));
	return ssl_sock;
}
#endif

