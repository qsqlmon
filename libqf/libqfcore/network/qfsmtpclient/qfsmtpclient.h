
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSMTPCLIENT_H
#define QFSMTPCLIENT_H

#include <qfcoreglobal.h>
#include <qfclassfield.h>
#include <qfexception.h>


#include <QSslError>

class QTcpSocket;
class QSslSocket;
class QTextStream;
class QHostInfo;

#ifdef Q_OS_WIN
//#define QF_SMTPCLIENT_NO_SSL
#endif

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFSmtpClient : public QObject 
{
	Q_OBJECT;
	QF_FIELD_RW(int, i, setI, d);
	public:
		enum SocketType {SocketTCP, SocketSSL};
		enum StatusCode {StatusOk = 0, StatusError, StatusInfo, StatusStart};
		enum SmtpAuth {AuthNone = 0, AuthLogin, AuthPlain};
		struct Attachment
		{
			QString fileName;
			QByteArray content;

			bool isLoaded() const {return !content.isEmpty();}
		};
		typedef QList<Attachment> AttachmentList;
	signals:
		void status(int client_id, int status_code, const QString &);
		/// emituje se pokud mail uspesne odesel
		/// @param mail_id je cislo nastavene metodou setMailId()
		/// @param status je StatusOk nebo StatusError
		void sent(int mail_id, int status);
	private slots:
		void dnsLookupHelper(const QHostInfo &host);
		void readyRead();
		void connected();
	private:
		virtual void timerEvent(QTimerEvent *event);
	protected:
		QTextStream* socketStream() throw(QFException);
		void sendAnswer(const QByteArray &ans);
#ifndef QF_SMTPCLIENT_NO_SSL
	protected:
		QSslSocket *sslSocket() throw(QFException);
	private slots:
		void sslErrors(QList<QSslError> errs);
#endif
	protected:
		enum State { Init, Auth, AuthLoginUser, AuthLoginPassword, Mail, Rcpt, Data, Body, Quit, Close };

		SocketType f_socketType;
		QString serverAddress;
		int serverPort;
		
		SmtpAuth authMethod;
		QVariantMap authParams;

		QByteArray message;
		QString from;//, to;//, subject;
		QStringList rcpts;
		QTcpSocket *socket;
		QTextStream * f_socketStream;
		int state;
		QString response, line;
		int connectionTimeoutTimerId;
		int f_mailId;
	public:
		SocketType socketType() const {return f_socketType;}
		void setMailId(int mail_id) {f_mailId = mail_id;}
		int mailId() const {return f_mailId;}
		void setAuthMethod(SmtpAuth auth_method, const QVariantMap &auth_params = QVariantMap()) {
			authMethod = auth_method;
			authParams = auth_params;
		}
		void setContent(const QString &from, const QString &to,
				const QString &subject, const QString &body, const AttachmentList &attachments = AttachmentList());
		//! posle mail a pak zavola delete this.
		void sendAndDestroy();
	public:
		QFSmtpClient(const QString &server, int port = 0, SocketType sock_type = SocketTCP);
		virtual ~QFSmtpClient();
};

#endif // QFSMTPCLIENT_H

