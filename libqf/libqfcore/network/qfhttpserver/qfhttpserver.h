
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFHTTPSERVER_H
#define QFHTTPSERVER_H

#include <qfhttprequestheader.h>

#include <qfcoreglobal.h>

#include <QTcpServer>

//! TODO: write class documentation.
class  QFCORE_DECL_EXPORT QFServerClient : public QObject 
{
	Q_OBJECT;
	protected:
		QTcpSocket *f_socket;
		static int clientCount; 
		int f_clientNo;
		QStringList f_searchPath;
		QByteArray requestData;
		int contentLength;
		bool inHeader;
		int parsedToPos;
	protected:
		virtual void processRequestData(const QByteArray &ba);
	public slots:
		virtual void readSocket();
		virtual void socketDisconnected();
		virtual void socketError();
	signals:
		//void socketClosed(QFServerClient *client);
	protected:
		virtual QStringList searchPath() {return f_searchPath;}
		virtual QByteArray getObject(const QFHttpRequestHeader &hp, bool *resolved = NULL/*, const QByteArray &post_data = QByteArray()*/);
		virtual QByteArray resolveAndLoadFile(const QString &_path, bool *resolved = NULL);
		/// defaultni implementace zavola QString::fromUtf8()
		virtual QString objectToUnicode(const QByteArray &http_object);
	public:
		QTcpSocket *socket() {return f_socket;}
		int clientNo() {return f_clientNo;}
		virtual void processRequest(const QFHttpRequestHeader &hdr);
	public:
		QFServerClient(QTcpSocket *_socket, QObject *parent = NULL);
		virtual ~QFServerClient();
};

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFHttpServer : public QTcpServer 
{
	Q_OBJECT;
	protected:
		//QList<QFServerClient*> clients;
	protected:
		virtual QFServerClient* createClient(QTcpSocket *_socket);
	protected slots:
		virtual void clientConnected();
		//virtual void clientDisconnected(QFServerClient *client);
	public:
		QFHttpServer(QObject *parent = NULL);
		virtual ~QFHttpServer(); 
};

#endif // QFHTTPSERVER_H

