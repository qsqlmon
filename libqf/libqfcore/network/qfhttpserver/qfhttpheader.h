
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFHTTPHEADER_H
#define QFHTTPHEADER_H

#include <QHttpRequestHeader>
#include <QSharedDataPointer>
#include <QVariantMap>

#include <qfcoreglobal.h>

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFHttpRequestHeader 
{
	public:
		//typedef QPair<QString, QString> KeyValue;
		//typedef QList<KeyValue> ArgList;
		typedef QMap<QString, QString> StringMap;
	protected:
		struct Data : public QSharedData
		{
			StringMap arguments;
			QString path;
			QByteArray data;
			QHttpRequestHeader header;
		};
		QSharedDataPointer<Data> d;
	protected:
		static QString percentDecode(const QString &encoded_str);
		void parseUrl();
		void parseQueryString(const QString &s);
	public:
		const QHttpRequestHeader& header() const {return d->header;}
		QString method() const {return d->header.method();}
		QString path() const {return d->path;}
		QString url() const;
		QString refererUrl() const;
		//void setPath(const QString &_path) {f_path = _path;}
		QString argument(const QString &key) const;
		QString arguments() const;
		const QByteArray data() const {return d->data;}
		//QVariantMap postedDataJson() const;

		QString fillArgumentValues(const QString &html_src) const;
		QString clearUnusedValues(const QString &html_src) const;
		
		static StringMap queryStringToMap(const QString &str);
	private:
		QFHttpRequestHeader(int);
		static const QFHttpRequestHeader& staticNull(); 
	public:
		QFHttpRequestHeader();
		QFHttpRequestHeader(const QByteArray & ba);
};
   
#endif // QFHTTPHEADER_H

