
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfhttpserver.h"

#include <qffileutils.h>

#include <QTcpSocket>
//#include <QHttpRequestHeader>

#include <qflogcust.h>

//=============================================================
//                             QFServerClient
//=============================================================
 int QFServerClient::clientCount = 0;

QFServerClient::QFServerClient(QTcpSocket * _socket, QObject * parent)
	: QObject(parent), f_socket(_socket)
{
	f_clientNo = ++clientCount;
	connect(_socket, SIGNAL(readyRead()), this, SLOT(readSocket()));
	connect(_socket, SIGNAL(disconnected()), this, SLOT(socketDisconnected()));
	connect(_socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(socketError()));
}

QFServerClient::~QFServerClient()
{
	qfDebug() << "client:" << clientNo() <<  "destroyied.";
	SAFE_DELETE(f_socket);
}

void QFServerClient::readSocket()
{
	qfLogFuncFrame();
	static QString content_length_str = "Content-Length:";
	QByteArray ba;
	ba.resize(int(socket()->bytesAvailable()));
	qint64 n = socket()->read(ba.data(), ba.size());
	if(n < 0) {
		qfError() << socket()->errorString();
		ba.resize(0);
		socket()->close();
	}
	else if(n == 0) {
		ba.resize(0);
	}
	qfTrash() << "\t new data:" << ba;
	if(requestData.isEmpty()) {
		contentLength = 0;
		inHeader = true;
		parsedToPos = 0;
	}
	requestData += ba;
	bool whole_request = false;
	if(!ba.isEmpty()) {
		if(inHeader) {
			while(true) {
				int ix = ba.indexOf("\r\n", parsedToPos);
				if(ix < 0) {
					/// data ustipnuta v pulce radku
					/*
					qfError() << "Corrupted HTTP header:" << requestData << "at pos" << parsedToPos;
					whole_request = true;
					inHeader = false;
					*/
					break;
				}
				QByteArray line = ba.mid(parsedToPos, ix +2 - parsedToPos);
				qfTrash() << "\t parsing header line:" << line.trimmed();
				parsedToPos = ix + 2;
				if(line.startsWith("\r\n")) {
					/// end of header
					qfTrash() << "\t end of header";
					inHeader = false;
					break;
				}
				else {
					QString s = QString(line).trimmed();
					if(s.startsWith(content_length_str, Qt::CaseInsensitive)) {
						contentLength = s.mid(content_length_str.length()).trimmed().toInt();
						qfTrash() << "\t found contentLength:" << contentLength;
					}
				}
			}
		}
		if(!inHeader) { /// pozor tady nemuze byt else
			qfTrash() << "header size:" << requestData.size() << "waiting for size:" << (parsedToPos + contentLength) << "contentLength:" << contentLength;
			if(requestData.size() >= parsedToPos + contentLength) {
				if(requestData.size() > parsedToPos + contentLength) {
					qfWarning() << "Extra data in HTTP header:" << requestData << "size:" << requestData.size() << "expected:" << (parsedToPos + contentLength) << "contentLength:" << contentLength;
				}
				whole_request = true;
			}
		}
	}

	if(whole_request) {
		if(!requestData.isEmpty()) processRequestData(requestData);
		socket()->close();
	}
}

void QFServerClient::processRequestData(const QByteArray & ba)
{
	qfDebug().color(QFLog::Yellow) << "client" << clientNo() << "sending:";
	qfDebug().color(QFLog::Yellow) << ba;
	QFHttpRequestHeader hdr(ba);
	processRequest(hdr);
}

void QFServerClient::socketDisconnected()
{
	//emit socketClosed(this);
	deleteLater();
}

void QFServerClient::socketError()
{
	if(socket()->error() != QTcpSocket::RemoteHostClosedError) {
		qfError() << "client:" << clientNo() << "socket error:" << socket()->errorString();
	}
	else {
		qfDebug() << "client:" << clientNo() <<  "socket:" << socket()->errorString();
	}
	//emit socketClosed(socket()->socketDescriptor());
}


void QFServerClient::processRequest(const QFHttpRequestHeader & hdr)
{
	qfLogFuncFrame();
	qfTrash() << hdr.method() << hdr.path();
	//typedef QPair<QString, QString> PA;
	//foreach(PA p, hdr.header().values()) qfTrash().color(QFLog::Green) << p.first << ":" << p.second;
	qfTrash() << "client" << clientNo();
	qfTrash() << "\thost:" << hdr.header().value("host");
	qfTrash() << "\tmethod:" << hdr.method();
	qfTrash() << "\twhole path:" << hdr.header().path();
	qfTrash() << "\tpath:" << hdr.path();
	qfTrash() << "arguments:" << hdr.arguments();
	QByteArray msg;
	QHttpResponseHeader response_hdr;
	bool resolved = false;
	msg = getObject(hdr, &resolved);
	if(!resolved) {
		response_hdr.setStatusLine(404, "Not found.");
		//qfTrash() << "sending response:/n" << response_hdr.toString();
		msg = resolveAndLoadFile("notfound.html");
		msg.replace("${url}", hdr.url().toAscii());
	}
	else {
		response_hdr.setStatusLine(200, "OK");
	}
	qfTrash() << "sending response:/n" << msg;
	socket()->write(response_hdr.toString().toUtf8());
	socket()->write(msg);
}

QByteArray QFServerClient::resolveAndLoadFile(const QString & _path, bool *resolved_ptr)
{
	qfLogFuncFrame() << "path:" << _path;
	QByteArray ret;
	bool resolved = false;
	QString path = _path;
	if(path == "/") path = "/index.html";
	foreach(QString s, searchPath()) {
		s = QFFileUtils::joinPath(s, path);
		if(QFile::exists(s)) {
			QFile f(s);
			if(f.open(QFile::ReadOnly)) {
				qfTrash() << "found file:" << f.fileName();
				ret = f.readAll();
				resolved = true;
				break;
			}
		}
	}
	if(resolved_ptr) *resolved_ptr = resolved;
	return ret;
}

QByteArray QFServerClient::getObject(const QFHttpRequestHeader & hdr, bool *resolved)
{
	//Q_UNUSED(post_data);
	qfLogFuncFrame() << "path:" << hdr.path();
	return resolveAndLoadFile(hdr.path(), resolved);
}

QString QFServerClient::objectToUnicode(const QByteArray & http_object)
{
	QString ret = QString::fromUtf8(http_object);
	return ret;
}

//=============================================================
//                             QFHttpServer
//=============================================================
QFHttpServer::QFHttpServer(QObject *parent)
	: QTcpServer(parent)
{
	connect(this, SIGNAL(newConnection()), this, SLOT(clientConnected()));
}

QFHttpServer::~QFHttpServer()
{
	//qDeleteAll(clients);
}

void QFHttpServer::clientConnected()
{
	QTcpSocket *cli_sock = nextPendingConnection();
	if(cli_sock != NULL) {
		qfTrash() << "Client connected:" << cli_sock->peerAddress().toString() << "port:" << cli_sock->peerPort();
		QFServerClient *sc = createClient(cli_sock);
		//clients << sc;
		//connect(sc, SIGNAL(socketClosed(QFServerClient *)), this, SLOT(clientDisconnected(QFServerClient *)));
		qfTrash() << "Adding client:" << sc->clientNo();
	}
}
/*
void QFHttpServer::clientDisconnected(QFServerClient * client)
{
	int ix = clients.indexOf(client);
	if(ix >= 0) {
		clients.removeAt(ix);
		qfInfo() << "Client disconnected:" << client->clientNo();
		client->deleteLater();
	}
	//qfTrash() << QFLog::stackTrace();
}
*/
QFServerClient * QFHttpServer::createClient(QTcpSocket * _socket)
{
	return new QFServerClient(_socket, this);
}






