
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfhttpheader.h"

#include <qf.h>
#include <qfstring.h>

//#include <QRegExp>

#include <qflogcust.h>

QFHttpRequestHeader::QFHttpRequestHeader()
{
	*this = staticNull();
}

QFHttpRequestHeader::QFHttpRequestHeader(const QByteArray & ba)
{
	d = new Data();
	d->header = QHttpRequestHeader(ba);
	int ix = ba.indexOf("\r\n\r\n");
	if(ix >= 0) {
		d->data = ba.mid(ix+4);
	}
	parseUrl();
}

QFHttpRequestHeader::QFHttpRequestHeader(int )
{
	d = new Data();
}

const QFHttpRequestHeader & QFHttpRequestHeader::staticNull()
{
	static QFHttpRequestHeader static_null(1);
	return static_null;
}

void QFHttpRequestHeader::parseUrl()
{
	QString s = header().path();
	int ix = s.indexOf('?');
	if(ix >= 0) {
		d->path = s.mid(0, ix);
		s = s.mid(ix+1);
		parseQueryString(s);
	}
	else {
		d->path = s;
	}
	parseQueryString(data());
}

QFHttpRequestHeader::StringMap QFHttpRequestHeader::queryStringToMap(const QString &str)
{
	qfLogFuncFrame() << str;
	StringMap ret;
	if(!str.isEmpty()) {
		QStringList sl = str.split("&");
		foreach(QString s, sl) {
			int ix = s.indexOf('=');
			QString key, val;
			if(ix >= 0) {
				key = percentDecode(s.mid(0, ix));
				val = percentDecode(s.mid(ix+1));
			}
			else {
				key = percentDecode(s);
			}
			qfTrash().noSpace() << "\t [" << key << "] = '" << val << "'";
			ret[key] = val;
		}
	}
	return ret;
}

void QFHttpRequestHeader::parseQueryString(const QString &str)
{
	qfLogFuncFrame() << str;
	if(!str.isEmpty()) {
		StringMap m = queryStringToMap(str);
		d->arguments.unite(m);
	}
}

QString QFHttpRequestHeader::argument(const QString & key) const
{
	QString ret = d->arguments.value(key);
	return ret;
}

QString QFHttpRequestHeader::percentDecode(const QString &encoded_str)
{
	QString ret;
	for(int i=0; i<encoded_str.length(); i++) {
		QChar c = encoded_str[i];
		if(c == '%') {
			QString s = encoded_str.mid(i+1, 2);
			char ch = (char)s.toInt(NULL, 16);
			c = ch;
			//qfInfo() << "%" << s << "->" << c;
			i+=2;
		}
		else if(c == '+') {
			c = ' ';
		}
		ret += c;
	}
	return ret;
}

QString QFHttpRequestHeader::fillArgumentValues(const QString & html_src) const
{
	QString src = html_src;
	QMapIterator<QString, QString> i(d->arguments);
	while (i.hasNext()) {
		i.next();
		src.replace("%(" + i.key() + ")", i.value());
	}
	return src;
}

QString QFHttpRequestHeader::clearUnusedValues(const QString & html_src) const
{
	/// find all variables in page, variable has form ${arg_name}
	//qfInfo() << QF_FUNC_NAME;
	QRegExp rx("\\%\\((\\w[\\-\\w]*)\\)");
	QSet<QString> args;
	int ix = 0;
	while((ix = rx.indexIn(html_src, ix)) >= 0) {
		QString s = rx.capturedTexts()[1];
		args << s;
		ix += rx.matchedLength();
	}
	QString src = html_src;
	QString var_str = "%(%1)";
	foreach(QString s, args) {
		//qfInfo() << var_str.arg(s) << "->" << argument(s);
		src.replace(var_str.arg(s), QString());
	}
	return src;
}

/*
QVariantMap QFHttpRequestHeader::postedDataJson() const
{
	QVariant v = stringToJson(data());
	return v.toMap();
}
*/

QString QFHttpRequestHeader::refererUrl() const
{
	return header().value("Referer");
}

QString QFHttpRequestHeader::arguments() const
{
	QStringList sl;
	QMapIterator<QString, QString> i(d->arguments);
	while (i.hasNext()) {
		i.next();
		sl << i.key() + "=" + i.value();
	}
	return sl.join("&");
}

QString QFHttpRequestHeader::url() const
{
	return header().path();
}
