
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution 
//

#ifndef QFSCRIPTDRIVER_H
#define QFSCRIPTDRIVER_H

#include <qf.h>
#include <qfcoreglobal.h>
#include <qfexception.h>

#include <QObject>
#include <QScriptValue>

class QScriptEngine;
class QFSqlConnection;
class QFSearchDirs;

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFScriptDriver : public QObject 
{
	Q_OBJECT;
	protected:
		QScriptEngine *f_scriptEngine;
		static QStringList f_path;
	public:
		QString resolveIncludes(const QString &code, const QString &code_file);
	public:
		/// return valid or create new QScriptEngine
		virtual QScriptEngine *scriptEngine();
	public:
		virtual QStringList availableScriptNames(const QString &path = QString()) {Q_UNUSED(path); return QStringList();}
		//! Pokud nenalezne \a function_name pokusi se nahrat a spustit \a script_name
		virtual QScriptValue resolveFunction(const QString &function_name, const QString &script_name, bool throw_exc = Qf::ThrowExc);
		//! Pokud nenalezne \a function_name pokusi se nahrat modul a najit ji tam
		virtual QScriptValue resolveModuleObjectFunction(const QString & function_name, const QString & module_name, bool throw_exc = Qf::ThrowExc);
		//! pokusi se najit v databazi \a script_name
		/// pokud nic nenajde vraci QString()
		/// defaultni implementace hleda ve filesystemu
		/// @param found_file_name je nastaven na skutecne jmeno souboru, pokud byl skript nalezen nebo nenalezen
		virtual QString loadScriptCode(const QString &script_name, QString *found_file_name = NULL);
		virtual QString loadModuleObjectCode(const QString & module_object_name, QString *found_file_name = NULL);
		//QScriptValue evaluateScriptCode(const QString &script_code, bool throw_exc = Qf::ThrowExc) throw(QFException);
		virtual void saveScriptCode(const QString &script_name, const QString &script_code) {Q_UNUSED(script_name); Q_UNUSED(script_code);}
		virtual void deleteScriptCode(const QString &script_name) {Q_UNUSED(script_name);}
		QStringList functionList(const QString &script_name);
	protected:
		static QFSearchDirs& searchDirsRef();
	public:
		static const QFSearchDirs& searchDirs() {return searchDirsRef();}
		static void prependSearchDir(const QString &dir);
		
		virtual QScriptValue evaluate(const QString &script_code, bool throw_exc = Qf::ThrowExc) throw(QFException);
		//! \a function_name je plne kvalifikovane jmeno funkce vcetne objektu
		//! pokud je \a this_object invalidni, pouzije se pri volani funkce jako this objekt pred posledni teckou v \a function_name.
		QScriptValue callFunction(const QString function_name, const QString &script_name, const QScriptValue & this_object = QScriptValue(), const QScriptValueList & args = QScriptValueList(), bool throw_exc = Qf::ThrowExc) throw(QFException);
		// vygeneruje object_name jako module_name.replace('.', '_') a zavola callFunction(object_name + '.' + function_name, module_name, ...)
		//! \a function_name je jmeno funkce bez objektu
		//! pokud je \a this_object invalidni, pouzije se pri volani funkce jako tak, jak je, tedy invalidni.
		QScriptValue callModuleObjectFunction(const QString function_name, const QString &module_name, const QScriptValue & this_object = QScriptValue(), const QScriptValueList & args = QScriptValueList(), bool throw_exc = Qf::ThrowExc) throw(QFException);
		//virtual QScriptValue evaluateFirstFunction(const QString &script_name, bool throw_exc = Qf::ThrowExc) throw(QFException);
		virtual QScriptValue callFunction(QScriptValue &fn, const QScriptValue & this_object = QScriptValue(), const QScriptValueList & args = QScriptValueList(), bool throw_exc = Qf::ThrowExc) throw(QFException);

		/// zavola delete na f_scriptEngine, coz zpusobi vytvoreni novaho pri pristim zavolani funkce scriptEngine(), takze se to chova jako reset enginu
		void resetEngine();

		QString dumpContext();
	public slots:
		/// ve skriptu nefunguje driver.evaluateScript("qf.js"), objekt Qf se vytvori, ale nezustane v namespace
		/// kdyz zavolam scriptDriver()->evaluateScript("qf.js"), tak to jde.
		/// je to asi tim, ze pokud to volam se skriptu, jsem uz jednou ve funkci QScriptEngine::evaluate(), a kdyz se zavola podruhy ze sebe, vytvori se novy scope
		QScriptValue evaluateScript(const QString &script_name, bool throw_exc = Qf::ThrowExc) throw(QFException);
		//! defaultni implementace pro \a module_name "foo.bar.baz" hleda foo/bar/baz.js
		QScriptValue moduleObject(const QString &module_name, bool throw_exc = Qf::ThrowExc) throw(QFException);
		virtual void log(int level, const QString &message);
		void logError(const QString &message) {log(QFLog::LOG_ERR, message);}
		void logWarning(const QString &message) {log(QFLog::LOG_WARN, message);}
		void logInfo(const QString &message) {log(QFLog::LOG_INF, message);}
		void logDebug(const QString &message) {log(QFLog::LOG_DEB, message);}
		void logTrash(const QString &message) {log(QFLog::LOG_TRASH, message);}
	public:		
		QScriptValue variantToScriptValue(const QVariant &v);
		static QVariant scriptValueToVariant(const QScriptValue &v);
		static QString scriptValueToJsonString(const QScriptValue &sv);
	public:
		QFScriptDriver(QObject *parent = NULL); 
		virtual ~QFScriptDriver();
};
   
//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFSqlScriptDriver : public QFScriptDriver 
{
	Q_OBJECT;
#ifdef QF_SCRIPT_DRIVER_CACHE_SCRIPT_CODES
	protected:
		struct ScriptCode
		{
			QString code;
			//QDateTime modificationTS;
			ScriptCode(const QString &_code = QString()) : code(_code) {}
		};
		typedef QMap<QString, ScriptCode> ScriptCodeMap;
		static ScriptCodeMap scriptCodeMap;

	public:
		static void clearScriptCodeCache() {scriptCodeMap.clear();}
#endif
	protected:
		QString f_scriptTableName;
		QString f_keyFieldName;
		QString f_codeFieldName;
	protected:
		/// najde v \a s vsechny ${...} a nahradi je odpovidajicim stringem
		QString correctNames(const QString &s);
		static QFSqlConnection& appConnection();
	public:
		virtual QScriptEngine *scriptEngine();
		QString scriptTableName() const {return f_scriptTableName;}
		void setScriptTableName(const QString &tbl_name) {f_scriptTableName = tbl_name;}
		QString keyFieldName() const {return f_keyFieldName;}
		void setKeyFieldName(const QString &name) {f_keyFieldName = name;}
		QString codeFieldName() const {return f_codeFieldName;}
		void setCodeFieldName(const QString &name) {f_codeFieldName = name;}
		
		//QString domainRootPath() const {return f_domainRootPath;}
		//void setDomainRootPath(const QString &s) {f_domainRootPath = s;}

		virtual QStringList availableScriptNames(const QString &path = QString());

		//virtual void loadModule(const QString &module_name, bool throw_exc = Qf::ThrowExc) throw(QFException);

		virtual QString loadScriptCode(const QString &script_name, QString *found_file_name = NULL);
		virtual QString loadModuleObjectCode(const QString & module_object_name, QString *found_file_name = NULL);
		virtual void saveScriptCode(const QString &script_name, const QString &script_code);
		virtual void deleteScriptCode(const QString &script_name);
	public slots:
		virtual QScriptValue execSql(const QString &query);
	public:
		QFSqlScriptDriver(QObject *parent = NULL);
		//virtual ~QFSqlScriptDriver();
};

#endif // QFSCRIPTDRIVER_H

