
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfscriptdriver.h"

#include <qffileutils.h>
#include <qfsqlquery.h>
#include <qfxmltable.h>
#include <qfappdbconnectioninterface.h>
#include <qfjson.h>
#include <qfsearchdirs.h>

#include <QScriptEngine>
#include <QScriptValueIterator>

#include <qflogcust.h>

//=========================================================
//                              QFScriptDriver
//=========================================================
QStringList QFScriptDriver::f_path;

QFScriptDriver::QFScriptDriver(QObject *parent) 
	: QObject(parent), f_scriptEngine(NULL)
{
}

QFScriptDriver::~QFScriptDriver()
{
}

QScriptEngine * QFScriptDriver::scriptEngine()
{
	if(!f_scriptEngine) {
		f_scriptEngine = new QScriptEngine(this);
		//qfInfo() << "created script engine:" << f_scriptEngine;
		QScriptValue sv = f_scriptEngine->newQObject(this);
		f_scriptEngine->globalObject().setProperty("driver", sv);
		QString code = loadModuleObjectCode("qf.qf");
		evaluate(code);
#if defined QT_DEBUG
		evaluate("qf.config.isDebug = true");
#endif
	}
	return f_scriptEngine;
}

QScriptValue QFScriptDriver::resolveFunction(const QString & function_name, const QString & script_name, bool throw_exc)
{
	qfTrash() << QF_FUNC_NAME << "fn:" << function_name << "script:" << script_name << "throw_exc:" << throw_exc;
	//QScriptValue sv = scriptEngine()->evaluate(function_name);
	QScriptValue sv = scriptEngine()->evaluate("qf.getObject("SARG(function_name)")");

	if(!sv.isFunction()) {
		sv = QScriptValue();
		if(!script_name.isEmpty()) {
			QString found_fn;
			QString code = loadScriptCode(script_name, &found_fn);
			if(!found_fn.isEmpty()) {
				evaluate(code, throw_exc);
				//sv = scriptEngine()->evaluate(function_name);
				sv = scriptEngine()->evaluate("qf.getObject("SARG(function_name)")");
				if(throw_exc && !sv.isFunction()) QF_EXCEPTION(tr("Skript '%1' neobsahuje funkci '%2'.").arg(script_name).arg(function_name));
			}
			else if(throw_exc) QF_EXCEPTION(tr("Skript '%1' nebyl nalezen.").arg(script_name));
		}
		else if(throw_exc) QF_EXCEPTION(tr("Nazev skriptu je prazdny.").arg(script_name));
	}
	return sv;
}

QScriptValue QFScriptDriver::resolveModuleObjectFunction(const QString & function_name, const QString & module_name, bool throw_exc)
{
	qfLogFuncFrame() << "fn:" << function_name << "module_name:" << module_name << "throw_exc:" << throw_exc;
	QString fn = module_name + "." + function_name;
	/// tohle by melo fungovat stejne, ale pokud modul obsahuje klicove slovo javascriptu, tak NE
	///QScriptValue sv = scriptEngine()->evaluate(fn);
	QScriptValue sv = scriptEngine()->evaluate("qf.getObject("SARG(fn)")");

	if(!sv.isFunction()) {
		sv = moduleObject(module_name, throw_exc);
		if(sv.isObject()) {
			sv = scriptEngine()->evaluate("qf.getObject("SARG(fn)")");
			if(throw_exc && !sv.isFunction()) QF_EXCEPTION(tr("Modul '%1' neobsahuje funkci '%2'.").arg(module_name).arg(function_name));
		}
		else {
			sv = QScriptValue();
			if(throw_exc) QF_EXCEPTION(tr("Modul '%1' neni objekt ani funkce.").arg(module_name));
		}
	}
	return sv;
}

QStringList QFScriptDriver::functionList(const QString & script_name)
{
	QStringList fnc_lst;
	QString found_fn;
	QString code = loadScriptCode(script_name, &found_fn);
	if(!found_fn.isEmpty()) {
		QRegExp rx("function\\s+([A-Za-z]\\w*)\\s*\\(\\s*\\)");
		int ix = 0;
		while((ix = rx.indexIn(code, ix)) != -1) {
			ix += rx.matchedLength();
			fnc_lst << rx.capturedTexts()[1];
		}
	}
	return fnc_lst;
}
/*
QStringList & QFScriptDriver::searchPathRef()
{
	if(f_path.isEmpty()) {
		f_path << QFFileUtils::joinPath(QFFileUtils::appDir(), "script");
		f_path << ":/libqf/script";
	}
	return f_path;
}
*/
QString QFScriptDriver::resolveIncludes(const QString & code, const QString & code_file)
{
	qfLogFuncFrame() << "code_file:" << code_file;
	//qfTrash() << "code:\n" << code;

	QString ret = code;
	/*
	{
		static const QString s_wrap = "#wrap_module";
		if(ret.indexOf(s_wrap) >= 0) {
			QRegExp rx_declare("#declare_module\\s*\\(([A-Za-z_][A-Za-z0-9_]*)\\)");
			int ix = rx_declare.indexIn(ret);
			if(ix < 0) QF_EXCEPTION("#wrap_module withouth #declare_module in " + code_file);

			QString declare_replace = rx_declare.cap(0);
			QString obj_name = rx_declare.cap(1).trimmed();
			if(obj_name.isEmpty()) QF_EXCEPTION("#declare_module() object name is empty");

			static const QString s_prefix = "if(!qf.getObject('${MODULE_NAME}')) { (function()";
			static const QString s_postfix = ")(); }";
			static const QString s_declare = "qf.declare('${MODULE_NAME}', %1);";

			ret.replace(declare_replace, s_declare.arg(obj_name));
			ret.replace(s_wrap, s_prefix);
			ret += s_postfix;
			//qfInfo() << ret;
		}
	}
	*/
	static QRegExp re_include;
	if(re_include.isEmpty()) {
		re_include = QRegExp("#include\\s*\"(.+)\"");
		re_include.setMinimal(true);
	}
	int pos = 0;
	while ((pos = re_include.indexIn(code, pos)) != -1) {
		QString inc = re_include.cap(0);
		QString fn = re_include.cap(1);
		qfTrash() << "\t found:" << inc;
		pos += re_include.matchedLength();
		QFSearchDirs sd = searchDirs();
		//QStringList path = searchPathRef();
		QString code_file_path = QFFileUtils::path(code_file);
		//if(!path.contains(code_file_path)) searchPathRef().prepend(code_file_path);
		if(!code_file_path.isEmpty()) {
			searchDirsRef().prependDir(code_file_path);
		}
		QString found_fn;
		QString inc_code = loadScriptCode(fn, &found_fn);
		searchDirsRef() = sd;
		if(found_fn.isEmpty()) {
			qfWarning() << "can't resolve include " << inc;
		}
		else {
			ret.replace(inc, inc_code);
			qfTrash() << "\t resolved";
		}
	}
	return ret;
}

static QString replace_module_name(const QString &code, const QString & module_object_name)
{
	static const QString s_mn = "${MODULE_NAME}";
	QString s = code;
	return s.replace(s_mn, module_object_name);
}

QString QFScriptDriver::loadModuleObjectCode(const QString & module_object_name, QString *found_file_name)
{
	qfLogFuncFrame() << "module_object_name:" << module_object_name;
	QString fn = module_object_name;
	fn = fn.replace('.', '/') + ".js";
	QString ffn;
	QString code = QFScriptDriver::loadScriptCode(fn, &ffn);
	if(!ffn.isEmpty()) {
		code = replace_module_name(code, module_object_name);
		if(found_file_name) *found_file_name = ffn;
	}
	return code;
}

QString QFScriptDriver::loadScriptCode(const QString & script_name, QString *found_file_name)
{
	qfLogFuncFrame() << "script_name:" << script_name;
	QString ret;
	QString found_fn;
	QString fn = searchDirs().findFile(script_name);
	/*
	foreach(QString s, searchPathRef()) {
		QString s2 = QFFileUtils::joinPath(s, script_name);
		qfTrash() << "\t trying:" << s2;
		if(QFile::exists(s2)) {
			fn = s2;
			break;
		}
	}
	*/
	if(fn.isEmpty()) {
		if(QFile::exists(script_name)) fn = script_name;
	}
	if(!fn.isEmpty()) {
		QFile f(fn);
		if(f.open(QFile::ReadOnly)) {
			qfTrash() << "\t opening script:" << fn;
			found_fn = fn;
			QByteArray ba = f.readAll();
			ret = QString::fromUtf8(ba.constData(), ba.size());
			ret = resolveIncludes(ret, fn);
		}
	}
	if(found_file_name) *found_file_name = found_fn;
	return ret;
}


/*
QScriptValue QFScriptDriver::evaluateFirstFunction(const QString & script_name, bool throw_exc) throw( QFException )
{
	QStringList fnc_lst = functionList(script_name);
	if(fnc_lst.isEmpty()) QF_EXCEPTION(tr("Skript %1 nebyl nalezen nebo neobsahuje zadne funkce.").arg(script_name));
	QScriptValue sv;
	sv = evaluateFunction(fnc_lst[0], script_name, throw_exc);
	return sv;
}
*/

static QString number_lines(const QString &code)
{
	QStringList code_lines = code.split('\n');
	QStringList lines_with_numbers;
	int n = 0;
	foreach(QString s, code_lines) lines_with_numbers << QString::number(++n) + ": " + s;
	return lines_with_numbers.join("\n");
}

QScriptValue QFScriptDriver::evaluate(const QString & script_code, bool throw_exc) throw( QFException )
{
	qfLogFuncFrame();
	QScriptEngine *eng = scriptEngine();
	qfTrash().color(QFLog::Yellow) << script_code.mid(0, 380);
	//qfTrash() << dumpContext();
	QScriptValue ret = eng->evaluate(script_code);
	if(throw_exc && eng->hasUncaughtException()) {
		QString s = tr("Vyjjimka \n%3\n pri provadeni skriptu na radku %1\n%2\n\n%4")
				.arg(eng->uncaughtExceptionLineNumber())
				.arg(eng->uncaughtExceptionBacktrace().join("\n")).arg(ret.toString()).arg(number_lines(script_code));
		QF_EXCEPTION(s);
	}
	if(ret.isError() && throw_exc) {
		QF_EXCEPTION(ret.toString());
	}
	qfTrash() << "\t return:" << scriptValueToJsonString(ret);
	return ret;
}

QScriptValue QFScriptDriver::evaluateScript(const QString & script_name, bool throw_exc) throw( QFException )
{
	QString found_fn;
	QString script_code = loadScriptCode(script_name, &found_fn);
	if(found_fn.isEmpty()) {
		if(throw_exc) QF_EXCEPTION(tr("Skript '%1' nebyl nalezen.").arg(script_name));
		else {
			qfWarning() << "script code" << script_name << "load error";
			return QScriptValue();
		}
	}
	return evaluate(script_code, throw_exc);
}

QScriptValue QFScriptDriver::moduleObject(const QString & module_name, bool throw_exc) throw( QFException )
{
	qfLogFuncFrame() << "module_name:" << module_name << "throw_exc:" << throw_exc;
	/// tohle by melo fungovat stejne, ale pokud modul obsahuje klicove slovo javascriptu, tak NE
	///QScriptValue sv = scriptEngine()->evaluate(module_name);
	QScriptValue sv = scriptEngine()->evaluate("qf.getObject("SARG(module_name)")");
	qfTrash() << "\t sv:" << sv.toString();
	if(!sv.isObject() || sv.isError()) {
		qfTrash() << "\t not found, try to load it";
		QString ffn;
		QString code = loadModuleObjectCode(module_name, &ffn);
		if(ffn.isEmpty()) {
			if(throw_exc) QF_EXCEPTION(tr("Can't load code for module object: '%1'.").arg(module_name));
			return QScriptValue();
		}
		evaluate(code, throw_exc);
		sv = scriptEngine()->evaluate("qf.getObject("SARG(module_name)")");
		//sv = scriptEngine()->evaluate(module_name);
		//qfInfo() << QFScriptDriver::scriptValueToJsonString(sv);
		if(!sv.isObject() || sv.isError()) {
			if(sv.isError()) {
				if(throw_exc) QF_EXCEPTION(tr("Load module object error '%1'.").arg(module_name));
			}
			else {
				if(throw_exc) QF_EXCEPTION(tr("Load module object error '%1' is neighter object nor function.").arg(module_name));
			}
			sv = QScriptValue();
		}
	}
	return sv;
}
/*
QScriptValue QFScriptDriver::callObjectFunction(const QString function_name, const QString & object_name, const QScriptValue & this_object, const QScriptValueList & args, bool throw_exc) throw( QFException )
{
	//QString object_name = module_name;
	//object_name.replace('.', '_');
	return callFunction(module_name + "." + function_name, object_name, this_object, args, throw_exc);
}
*/

QScriptValue QFScriptDriver::callModuleObjectFunction(const QString function_name, const QString & module_object_name, const QScriptValue & this_object, const QScriptValueList & args, bool throw_exc) throw( QFException )
{
	qfLogFuncFrame();
	qfTrash().color(QFLog::Green) << "function_name:" << function_name << "module_name:" << module_object_name;
	QScriptValue sv = resolveModuleObjectFunction(function_name, module_object_name, throw_exc);
	qfTrash() << "\t fn:" << sv.toString();
	qfTrash() << "\t this_object:" << this_object.toString();
	sv = callFunction(sv, this_object, args, throw_exc);
	return sv;
}

QScriptValue QFScriptDriver::callFunction(const QString function_name, const QString & script_name, const QScriptValue & this_object, const QScriptValueList & args, bool throw_exc) throw( QFException )
{
	qfLogFuncFrame();
	qfTrash().color(QFLog::Green) << "function_name:" << function_name << "script_name:" << script_name;
	QScriptValue sv = resolveFunction(function_name, script_name, throw_exc);
	QScriptValue this_o = this_object;
	if(!this_o.isValid()) {
		QStringList sl = function_name.split('.');
		if(sl.count() > 1) {
			sl.removeLast();
			this_o = evaluate(sl.join("."), throw_exc);
		}
	}
	qfTrash() << "\t fn:" << sv.toString();
	qfTrash() << "\t this_object:" << this_o.toString();
	sv = callFunction(sv, this_o, args, throw_exc);
	return sv;
}

QScriptValue QFScriptDriver::callFunction(QScriptValue & fn, const QScriptValue & this_object, const QScriptValueList & args, bool throw_exc) throw( QFException )
{
	qfLogFuncFrame();
	if(throw_exc && !fn.isFunction()) {
		QString s = tr("Objekt neni funkce.");
		QF_EXCEPTION(s);
	}
	QScriptValue ret = fn.call(this_object, args);
	QScriptEngine *eng = fn.engine();
	if(throw_exc && eng->hasUncaughtException()) {
		QString s = tr("Vyjjimka ve funkci na radku %1\n%2\n\n%3")
				.arg(eng->uncaughtExceptionLineNumber())
				.arg(eng->uncaughtExceptionBacktrace().join("\n"))
				.arg(number_lines(fn.toString()));
		QF_EXCEPTION(s);
	}
	return ret;
}

void QFScriptDriver::log(int level, const QString & message)
{
	//qfInfo() << QF_FUNC_NAME;
	QFLog(level, QString("%1:%2").arg("#script").arg('#')) << message;
}

QScriptValue QFScriptDriver::variantToScriptValue(const QVariant & v)
{
	QScriptValue ret;
	//qfInfo() << "value:" << v.toString() << "type:" << QVariant::typeToName(v.type());
	switch(v.type()) {
		case QVariant::String:
			ret = QScriptValue(scriptEngine(), v.toString());
			break;
		case QVariant::Bool:
			ret = QScriptValue(scriptEngine(), v.toBool());
			break;
		case QVariant::Int:
			ret = QScriptValue(scriptEngine(), v.toInt());
			break;
		case QVariant::UInt:
			ret = QScriptValue(scriptEngine(), v.toUInt());
			break;
		case QVariant::Double:
			//qfInfo() << "double:" << v.toDouble();
			ret = QScriptValue(scriptEngine(), v.toDouble());
			break;
		case QVariant::Time:
		case QVariant::Date:
		case QVariant::DateTime:
			ret = scriptEngine()->newDate(v.toDateTime());
			break;
		case QVariant::List:
		{
			ret = scriptEngine()->newArray();
			quint32 ix = 0;
			foreach(QVariant v1, v.toList()) {
				ret.setProperty(ix++, variantToScriptValue(v1));
			}
		}
			break;
		case QVariant::Map:
		{
			ret = scriptEngine()->newObject();
			//qfInfo() << "map" << ret.toString();
			QMapIterator<QString, QVariant> it(v.toMap());
			while (it.hasNext()) {
				it.next();
				QString key = it.key();
				QVariant val = it.value();
				ret.setProperty(key, variantToScriptValue(val));
			}
		}
			break;
		default:
			if(v.isNull()) ret = scriptEngine()->nullValue();
			else ret = scriptEngine()->newVariant(v);
	}
	return ret;
}

QVariant QFScriptDriver::scriptValueToVariant(const QScriptValue & sv)
{
	qfLogFuncFrame() << "sv:" << sv.toString() << "__qfTypeName:" << sv.property("__qfTypeName").toString();
	QVariant ret;
	if(sv.isObject()) {
		if(sv.property("__qfTypeName").toString() == "qf.sql.ResultSet") {
			QFXmlTableDocument doc;
			QFXmlTable xt = doc.toTable();
			{
				QScriptValue flds = sv.property("fields");
				QScriptValueIterator it(flds);
				while (it.hasNext()) {
					it.next();
					//qfInfo() << it.name() << ": " << it.value().toString();
					QScriptValue fld = it.value();
					QByteArray ba = fld.property("type").toString().toAscii();
					xt.appendColumn(fld.property("name").toString(), QVariant::nameToType(ba.constData()));
				}
			}
			{
				QScriptValue rows = sv.property("rows");
				QScriptValueIterator it(rows);
				while (it.hasNext()) {
					it.next();
					QScriptValue row = it.value();
					QFXmlTableRow xr = xt.appendRow();
					QScriptValueIterator it2(row.property("values"));
					while (it2.hasNext()) {
						it2.next();
						xr.setValue(it2.name().toInt(), it2.value().toVariant());
					}
				}
			}
			ret.setValue(doc);
			//qfInfo() << scriptValueToJsonString(sv);
			//qfInfo() << doc.toString();
		}
		else {
			ret = sv.toVariant();
		}
	}
	else {
		qfTrash() << "\t isDate:" << sv.isDate() << "toDate:" << sv.toDateTime().toString();
		ret = sv.toVariant();
	}
	qfTrash() << "\t return:" << ret.toString() << "type:" << QVariant::typeToName(ret.type());
	return ret;
}

QString QFScriptDriver::scriptValueToJsonString(const QScriptValue & sv)
{
	//qfLogFuncFrame() << sv.toString();
	//static int limit = 0;
	QString ret;
	if(sv.isString()) {
		ret = QFJson::variantToString(sv.toString());
	}
	else if(sv.isArray()) {
		QStringList sl;
		int len = sv.property("length").toInt32();
		for(int i=0; i<len; i++) {
			QScriptValue v = sv.property((quint32)i);
			sl << scriptValueToJsonString(v);
		}
		ret = '[' + sl.join(",") + ']';
	}
	else if(sv.isObject()) {
		QStringList sl;
		QScriptValueIterator it(sv);
		while(it.hasNext()) {
			it.next();
			QString k = it.name();
			QScriptValue v = it.value();
			QString val_str;
			if(k == "prototype") {
				continue;
				//val_str = v.toString();
			}
			else if(k == "stack") {
				val_str = "stackTrace";
			}
			else {
				if(sv.isFunction()) {
					val_str = "function()";
				}
				else {
					//qfTrash() << "OBJECT key:" << k << "value:" << v.toString();
					val_str = scriptValueToJsonString(v);
				}
			}
			QString s = k + ":" + val_str;
			sl << s;
		}
		ret = '{' + sl.join(",") + '}';
	}
	else {
		ret = sv.toString();
	}
	//qfTrash() << "\treturn:" << ret;
	return ret;
}

QString QFScriptDriver::dumpContext()
{
	QStringList sl;
	QScriptContext *ctx = scriptEngine()->currentContext();
	sl << "============ CONTEXT DUMP =============";
	sl << "context addr: " + ((ctx)? QString::number((long)ctx, 16): "NULL");
	if(ctx) {
		{
			QStringList sl1;
			QScriptContext *ctx1 = ctx->parentContext();
			while(ctx1) {
				sl1 << QString::number((long)ctx1, 16);
				ctx1 = ctx1->parentContext();
			}
			sl << "parents: \n\t" + sl1.join("\n\t");
		}
		sl << "engine addr: " + QString::number((long)ctx->engine(), 16);
		sl << "this object: " + ctx->thisObject().toString();
		sl << "callee: " + ctx->callee().toString();
		sl << "backtrace: \n\t" + ctx->backtrace().join("\n\t");
	}
	return sl.join("\n");
}

//=========================================================
//                              QFSqlScriptDriver
//=========================================================
#ifdef QF_SCRIPT_DRIVER_CACHE_SCRIPT_CODES
QFSqlScriptDriver::ScriptCodeMap QFSqlScriptDriver::scriptCodeMap;
#endif
QFSqlScriptDriver::QFSqlScriptDriver(QObject * parent)
	: QFScriptDriver(parent)
{
	f_scriptTableName = "scripts";
	f_keyFieldName = "ckey";
	f_codeFieldName = "code";
}

QStringList QFSqlScriptDriver::availableScriptNames(const QString &path)
{
	static QString s_qs = "SELECT ${KEY_FLDNAME} FROM ${SCRIPT_TBLNAME} %1 ORDER BY ${KEY_FLDNAME}";
	QStringList ret;

	QFSqlQuery q(appConnection());
	QString domain_filter = path.trimmed();
	if(!domain_filter.isEmpty()) {
		domain_filter = QString("WHERE ${KEY_FLDNAME} LIKE '%1.%'").arg(domain_filter);
	}
	QString qs = s_qs.arg(domain_filter);
	qs = correctNames(qs);
	//qfInfo() << qs;
	q.exec(qs);
	while(q.next()) {
		ret << q.value(0).toString();
		//qfTrash() << "\t src from DB:" << src;
	}
	return ret;
}

QString QFSqlScriptDriver::loadScriptCode(const QString & script_name, QString * found_file_name_ptr)
{
	qfLogFuncFrame() << script_name;
	QString src;
	QString found_fn;
#ifdef QF_SCRIPT_DRIVER_CACHE_SCRIPT_CODES
	if(scriptCodeMap.contains(script_name)) {
		src = scriptCodeMap.value(script_name).code;
		found_fn = script_name;
	}
	else
#endif	
	{
		QFSqlQuery q(appConnection());
		static const QString s_qs = "SELECT ${CODE_FLDNAME} FROM ${SCRIPT_TBLNAME} WHERE ${KEY_FLDNAME}='%1'";
		QString qs = s_qs.arg(script_name);
		qs = correctNames(qs);
		qfTrash() << qs;
		q.exec(qs);
		if(q.next()) {
			src = q.value(0).toString();
			//src = unescapeScriptCodeFromSQL(q.value(0).toString());
			src = resolveIncludes(src, script_name);
#ifdef QF_SCRIPT_DRIVER_CACHE_SCRIPT_CODES
			scriptCodeMap[script_name] = ScriptCode(src);
#endif
			found_fn = script_name;
			qfTrash() << "\t found";
		}
	}
	if(found_fn.isEmpty()) {
		src = QFScriptDriver::loadScriptCode(script_name, &found_fn);
#ifdef QF_SCRIPT_DRIVER_CACHE_SCRIPT_CODES
		if(!found_fn.isEmpty()) scriptCodeMap[script_name] = ScriptCode(src);
#endif
	}
	if(found_file_name_ptr) *found_file_name_ptr = found_fn;
	//qfInfo() << src;
	return src;
}

QString QFSqlScriptDriver::loadModuleObjectCode(const QString & module_object_name, QString * found_file_name)
{
	qfLogFuncFrame() << "module_object_name:" << module_object_name;
	QString ffn;
	QString code = QFSqlScriptDriver::loadScriptCode(module_object_name, &ffn);
	if(ffn.isEmpty()) {
		code = QFScriptDriver::loadModuleObjectCode(module_object_name, &ffn);
	}
	else {
		code = replace_module_name(code, module_object_name);
	}
	if(found_file_name) *found_file_name = ffn;
	return code;
}

void QFSqlScriptDriver::saveScriptCode(const QString & script_name, const QString & script_code)
{
	qfTrash() << QF_FUNC_NAME << script_name;
	QString qs;
	qs = "INSERT INTO ${SCRIPT_TBLNAME} (${KEY_FLDNAME}, ${CODE_FLDNAME})"
		" VALUES('%1', '%2')"
		" ON DUPLICATE  KEY UPDATE ${CODE_FLDNAME} = VALUES(${CODE_FLDNAME})";
	qs = qs.arg(script_name).arg(script_code);
	qs = correctNames(qs);
	QFSqlQuery q(appConnection());
	//qfInfo() << qs;
	q.exec(qs);
}

void QFSqlScriptDriver::deleteScriptCode(const QString & script_name)
{
	qfTrash() << QF_FUNC_NAME << script_name;
	QString qs;
	qs = "DELETE FROM ${SCRIPT_TBLNAME}"
			" WHERE ${KEY_FLDNAME} = '%1'";
	qs = qs.arg(script_name);
	qs = correctNames(qs);
	QFSqlQuery q(appConnection());
	//qfInfo() << qs;
	q.exec(qs);
}

QScriptValue QFSqlScriptDriver::execSql(const QString & query)
{
	qfLogFuncFrame() << "query:" << query;
	QScriptValue ret;
	QFSqlQuery q(appConnection());
	q.exec(query);
	if(q.isSelect()) {
		QSqlRecord rec = q.record();
		QScriptEngine *eng = scriptEngine();
		QScriptValue fields = eng->newArray(rec.count());
		for(int i=0; i<rec.count(); i++) {
			QSqlField fld = rec.field(i);
			QScriptValue field = eng->newObject();
			field.setProperty("name", QScriptValue(eng, fld.name()));
			field.setProperty("type", QScriptValue(eng, QVariant::typeToName(fld.type())));
			fields.setProperty((quint32)i, field);
		}
		qfTrash() << "\t fields:" << scriptValueToJsonString(fields);
		qfTrash() << "\t looking for QFSqlResultSet, script engine:" << scriptEngine();
		QScriptValue resultset_ctor = evaluate("qf.require('qf.sql.ResultSet')");
		//if(!ctor.isFunction()) { QF_EXCEPTION("QFSqlResultSet javascript initialization error."); }
		//qfTrash() << ctor.toString();
		if(!resultset_ctor.isFunction()) { QF_EXCEPTION("qf.sql.Sql.ResultSet javascript initialization error."); }

		//qfInfo() << "\t fields:" << scriptValueToJsonString(fields);
		QScriptValueList ctor_params_for_fields;
		ctor_params_for_fields << fields;
		QScriptValue result_set = resultset_ctor.construct(ctor_params_for_fields);
		//qfInfo() << "\t result_set:" << scriptValueToJsonString(result_set);
		QScriptValue fn_append_row = result_set.property("appendRow");
		if(!fn_append_row.isFunction()) { QF_EXCEPTION("qf.sql.ResultSet.appendRow() javascript lookup error."); }
		QScriptValue sqlrow_ctor = evaluate("qf.require('qf.sql.Row')");
		//QScriptValue fn_set_value = ctor.property("setValue");
		//if(!fn_set_value.isFunction()) { QF_EXCEPTION("SqlRow::setValue() javascript lookup error."); }
		QScriptValueList set_value_args;
		set_value_args << QScriptValue() << QScriptValue();
		//qfInfo() << "SqlRow:" << eng->evaluate("SqlRow.prototype.setValue").toString();
		//qfInfo() << "SqlRow.prototype:" << scriptValueToJsonString(eng->evaluate("SqlRow.prototype"));
		/// pozor QtScript funkce prototype() vraci jiny objekt (property __proto__) nez obj.prototype volany z Javascriptu
		/// var o = new Object();
		/// (o.__proto__ === Object.prototype); // this evaluates to true
		/// presto tohle funguje
		QScriptValue set_value = eng->evaluate("qf.sql.Row.prototype.setValue");
		/// tohle ne
		///QScriptValue set_value = sqlrow_ctor.prototype().property("setValue");
		/// tohle taky ne
		///QScriptValue set_value = sqlrow_ctor.property("__proto__").property("setValue");
		//qfInfo() << "setValue:" << set_value.toString();
		if(!set_value.isFunction()) { QF_EXCEPTION("QFSqlRow::setValue() javascript lookup error."); }
		while(q.next()) {
			QScriptValue row = sqlrow_ctor.construct(ctor_params_for_fields);
			/// tohle funguje
			///QScriptValue set_value = row.property("setValue");
			//if(!set_value.isFunction()) { QF_EXCEPTION("SqlRow::setValue() javascript lookup error."); }
			for(int i=0; i<rec.count(); i++) {
				QVariant val = q.value(i);
				set_value_args[0] = QScriptValue(eng, i);
				set_value_args[1] = variantToScriptValue(val);
				//qfInfo() << "value:" << val.toString() << "type:" << QVariant::typeToName(val.type());
				//set_value.call(row, set_value_args);
				callFunction(set_value, row, set_value_args);
			}
			//qfTrash() << "appendRow:" << scriptValueToJsonString(append_row) << append_row.toString();
			//qfTrash() << "appending row:" << scriptValueToJsonString(row);
			callFunction(fn_append_row,result_set, QScriptValueList() << row);
		}
		ret = result_set;
	}
	//qfTrash() << "execSql ret:" << scriptValueToJsonString(ret);
	return ret;
}

QString QFSqlScriptDriver::correctNames(const QString & _s)
{
	QString s = _s;
	s.replace("${SCRIPT_TBLNAME}", scriptTableName());
	s.replace("${KEY_FLDNAME}", keyFieldName());
	s.replace("${CODE_FLDNAME}", codeFieldName());
	return s;
}

void QFScriptDriver::resetEngine()
{
	if(f_scriptEngine) {
		qfTrash().color(QFLog::Yellow) << "deleting script engine";
		SAFE_DELETE(f_scriptEngine);
	}
}

QScriptEngine * QFSqlScriptDriver::scriptEngine()
{
	return QFScriptDriver::scriptEngine();
	/*
	if(!f_scriptEngine) {
		QFScriptDriver::scriptEngine();
		/// QFSqlResultSet nahraju hned, protoze nahravani ve funkci execSql(), sice funguje, ale je-li tato funkce volana ze skriptu, QFSqlResultSet stejne nezustane v namespace enginu. 
		evaluateScript("qfsql.js");
		QScriptValue ctor = f_scriptEngine->evaluate("QFSqlResultSet");
		if(!ctor.isFunction()) qfError() << "QFSqlResultSet javascript initialization error.";
	}
	return f_scriptEngine;
	*/
}

QFSqlConnection & QFSqlScriptDriver::appConnection()
{
	QCoreApplication *core_app = QCoreApplication::instance();
	QFAppDbConnectionInterface *ifc = dynamic_cast<QFAppDbConnectionInterface*>(core_app); /// cross cast
	if(ifc) return ifc->connection();
	QF_EXCEPTION("No QFDbAppConfigInterface");
	return QFAppDbConnectionInterface::dummyConnection();
}

QFSearchDirs & QFScriptDriver::searchDirsRef()
{
	static QFSearchDirs sd;
	if(sd.dirs().isEmpty()) {
		sd.appendDir(QFFileUtils::joinPath(QFFileUtils::appDir(), "script"));
		sd.appendDir(":/libqfcore/script");
	}
	return sd;
}

void QFScriptDriver::prependSearchDir(const QString & dir)
{
	searchDirsRef().prependDir(dir);
}







