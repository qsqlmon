//driver.logInfo("executing script qf.js");
if(typeof qf == "undefined") {
//driver.logInfo("creating script object qf");
(function(){
	qf = {}

	qf.global = this;

	qf.config = {isDebug: false}

	qf.log = {};
	qf.log.trash = function(msg) {
		//driver.logInfo("log.trash");
		if(qf.config.isDebug) driver.logTrash(msg);
	}
	qf.log.debug = function(msg) {
		if(qf.config.isDebug) driver.logDebug(msg);
	};
	qf.log.info = function(msg) {driver.logInfo(msg);};
	qf.log.warning = function(msg) {driver.logWarning(msg);};
	qf.log.error = function(msg) {driver.logError(msg);};

	qf._reprString = function(o) {
		return ('"' + o.replace(/(["\\])/g, '\\$1') + '"' )
				.replace(/[\f]/g, "\\f" )
				.replace(/[\b]/g, "\\b" )
				.replace(/[\n]/g, "\\n" )
				.replace(/[\t]/g, "\\t" )
				.replace(/[\r]/g, "\\r");
	};

	qf._reprDate = function(db) {
		var year = db.getFullYear();
		var dd = db.getDate();
		var mm = db.getMonth()+1;

		var hh = db.getHours();
		var mins = db.getMinutes();

		function leadingZero(nr) {
			if (nr < 10) nr = "0" + nr;
			return nr;
		}
		if(hh == 24) hh = '00';

		var time = leadingZero(hh) + ':' + leadingZero(mins);
		return '"' + year + '-' + mm + '-' + dd + 'T' + time + '"';
	};

	qf.toJson = function(o) {
		var objtype = typeof(o);
		if (objtype == "undefined") {
			return "undefined";
		} else if (objtype == "number" || objtype == "boolean") {
			return o + "";
		} else if (o === null) {
			return "null";
		}
		if (objtype == "string") {
			//return "string";
			return qf._reprString(o);
		}
		if(objtype == 'object' && o.getFullYear) {
			return qf._reprDate(o);
		}
		var me = arguments.callee;
		if (objtype != "function" && typeof(o.length) == "number") {
			var res = [];
			for (var i = 0; i < o.length; i++) {
				var val = me(o[i]);
				if (typeof(val) != "string") {
					val = "undefined";
				}
				res.push(val);
			}
			return "[" + res.join(",") + "]";
		}
        // it's a function with no adapter, bad
		if (objtype == "function")
		return null;
		res = [];
		for (var k in o) {
			var useKey;
			if (typeof(k) == "number") {
			 //useKey = '"' + k + '"';
				useKey = '' + k;
			} else if (typeof(k) == "string") {
			 //useKey = qf._reprString(k);
				useKey = '' + k;
			} else {
                // skip non-string or number keys
				continue;
			}
			val = me(o[k]);
			if (typeof(val) != "string") {
                // skip non-serializable values
				continue;
			}
			res.push(useKey + ":" + val);
		}
		return "{" + res.join(",") + "}";
	}

	function get_property(/*Array*/parts, /*Boolean*/create, /*Object*/context)
	{
		qf.log.trash("get_property " + parts.join('.'));
		var obj = context || qf.global;
		for(var i=0, p; obj&&(p=parts[i]); i++){
			obj = (p in obj ? obj[p] : (create ? obj[p]={} : undefined));
			qf.log.trash('\t' + p + " - obj: " + obj);
			if(typeof obj == 'undefined' && create) {
				var module_name = parts.join('.');
				throw new Error("get_property(): create path error, possibly module name contains reserved word. Path: " + module_name);
			}
		}
		return obj; // mixed
	}

	qf.getObject = function(/*String*/name, /*Boolean*/create, context){
		// summary:
		//		Get a property from a dot-separated string, such as "A.B.C"
		//	description:
		//		Useful for longer api chains where you have to test each object in
		//		the chain, or when you have an object reference in string format.
		//	name:
		//		Path to an property, in the form "A.B.C".
		//	context:
		//		Optional. Object to use as root of path. Defaults to
		//		'dojo.global'. Null may be passed.
		//	create:
		//		Optional. If true, Objects will be created at any point along the
		//		'path' that is undefined.
		qf.log.trash("qf.getObject() " + name);
		var parts = name.split(".");
		var obj = get_property(parts, create, context);
		qf.log.trash('\t return: ' + obj);
		return obj;
	}

	qf.getProperty = function(parent_object, /*String*/name){
		// summary:
		//		Get a property from a dot-separated string, such as "A.B.C"
		//	description:
		//		Useful for longer api chains where you have to test each object in
		//		the chain, or when you have an object reference in string format.
		//	name:
		//		Path to an property, in the form "A.B.C".
		qf.log.trash("qf.getProperty() " + name);
		return qf.getObject(name, false, parent_object);
	}

	qf.getPropertyNum = function(parent_object, /*String*/name){
		qf.log.trash("qf.getPropertyNum() " + name);
		var ret = qf.getProperty(parent_object, name);
		if(!ret) ret = 0;
		return ret;
	}

	qf.declare = function(/*String*/module_object_name, /*Object*/object)
	{
		qf.log.trash('qf.declare(): name:' + module_object_name);
		var parts = module_object_name.split(".");
		var prop = parts.pop();
		if(prop.length == 0) throw new Error("qf.declare(): Bad name " + module_object_name);

		var obj = get_property(parts, true);
		//qf.log.trash('qf.declare(): ' + parts.join('.') + ': ' + eval(parts.join('.')));
		//qf.log.trash('qf.declare(): module object ' + parts.join('.') + " :" + obj);
		//qf.log.trash('qf.declare(): Setting property ' + parts.join('.') + "['" + prop + "'] to " + object);
		obj[prop] = object;
		//qf.log.trash('\t' + module_object_name + " - obj: " + eval(module_object_name));
		//if(typeof obj[prop] == 'undefined') {
		//	throw new Error("qf.declare(): create object error, possibly module name contains reserved word. Module: " + module_object_name);
		//}
		//qf.log.trash('qf.declare(): module object ' + module_object_name + ': ' + eval(module_object_name));
	}

	qf.wrapModule = function(/*String*/module_object_name, /*function*/mod_fn)
	{
		if(!qf.getObject(module_object_name)) {
			var obj = mod_fn();
			qf.declare(module_object_name, obj);
		}
	}

	qf.require = function(/*String*/name)
	{
		var obj = qf.getObject(name);
		if(typeof obj == "undefined") {
			//driver.logInfo('Loading module: ' + name);
			qf.log.trash('Loading module: ' + name);
			driver.moduleObject(name);
			obj = qf.getObject(name);
			if(typeof obj == "undefined") {
				throw new Error("qf.require(): Load module object error, object name: " + name);
			}
		}
		return obj;
	}

})();

}
