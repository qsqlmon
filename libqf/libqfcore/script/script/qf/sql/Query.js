qf.wrapModule('${MODULE_NAME}',
function() {

	function Query() {}

	/// vraci ResultSet
	Query.exec = function(query_str)
	{
		return driver.execSql(query_str);
	}
	return Query;
});
