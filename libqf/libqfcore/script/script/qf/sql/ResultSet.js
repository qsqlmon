qf.wrapModule('${MODULE_NAME}',
function() {

	var Row = qf.require('qf.sql.Row');

	function ResultSet(flds)
	{
		//driver.logInfo("flds: " + qf.toJson(flds));
		//driver.logInfo("arguments.length: " + arguments.length);
		if(arguments.length > 0) {
			this.fields = flds;
		}
		else {
			this.fields = new Array();
		}
		this.rows = new Array();
	}

	ResultSet.prototype.__qfTypeName = '${MODULE_NAME}';

	ResultSet.prototype.emptyRow = new Row();

	ResultSet.prototype.row = function(ix)
	{
		if(ix >= 0 && ix < this.rows.length) {
			return this.rows[ix];
		}
		return this.emptyRow;
	}

	ResultSet.prototype.appendRow = function(r)
	{
	//driver.logInfo("appendRow before row count: " + this.rows.length);
	//driver.logInfo("row: " + Qf.toJson(r));
		this.rows.push(r);
	//driver.logInfo("appendRow after row count: " + this.rows.length);
	}

	ResultSet.prototype.fieldCount = function()
	{
		return this.fields.length;
	}

	ResultSet.prototype.rowCount = function()
	{
		return this.rows.length;
	}


	return ResultSet;

});
