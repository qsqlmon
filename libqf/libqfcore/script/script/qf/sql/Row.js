qf.wrapModule('${MODULE_NAME}',
function() {

	function Row(flds)
	{
		if(arguments.length > 0) {
			this.fields = flds;
			this.values = new Array(flds.length);
		}
		else {
			this.fields = new Array();
			this.values = new Array();
		}
	}

	Row.prototype.value = function(ix)
	{
		var ret = undefined;
		if(typeof(ix) == 'number') {
			if(ix < this.values.length) ret = this.values[ix];
		}
		else {
			var fld_name = ix.toString();
			for(i=0; i<this.fields.length; i++) {
				var fld = this.fields[i];
				if(fld.name == fld_name) {
					ret = this.values[i];
				}
			}
		}
		return ret;
	}

	Row.prototype.setValue = function(ix, val)
	{
	//driver.logInfo("setValue index: " + ix + " value: " + val);
		if(typeof(ix) == 'number') {
		//driver.logInfo("numerical index, values length: " + this.values.length);
		//driver.logInfo("old value: " + this.values[ix]);
			if(ix < this.values.length) this.values[ix] = val;
		//driver.logInfo("new values length: " + this.values.length);
		}
		else {
			var fld_name = ix.toString();
			for(i=0; i<this.fields.length; i++) {
				var fld = this.fields[i];
				if(fld.name == fld_name) {
					this.values[i] = val;
				}
			}
		}
	}

	Row.prototype.fieldCount = function()
	{
		return this.fields.length;
	}

	return Row;

});
