#CONFIG += qmake_debug
MY_SUBPROJECT = libqfcore
!exists(go_to_top.pri) :error("cann't find go_to_top.pri")
include(go_to_top.pri)

TEMPLATE = lib
win32:TARGET = $$MY_SUBPROJECT
unix:TARGET = $$replace(MY_SUBPROJECT, lib, )
TARGET = $${TARGET}$$QF_LIBRARY_DEBUG_EXT
message(Target: $$TARGET)

QT += xml sql network script
QT	-= gui

CONFIG += qt dll
#staticlib
#dll

win32:DESTDIR = $$MY_BUILD_DIR/bin
unix:DESTDIR = $$MY_BUILD_DIR/lib
#DLLDESTDIR = $$MY_BUILD_DIR/bin
INCLUDEPATH += $$PWD/../include

DEFINES +=    \
    QFCORE_BUILD_DLL

# Use Precompiled headers (PCH)
#CONFIG += precompile_header
#PRECOMPILED_HEADER  = precompiled.h
#precompile_header:!isEmpty(PRECOMPILED_HEADER) {
#	message("using precompiled_headers")
#}

win32 {
LIBS += -liphlpapi
}

CONFIG += hide_symbols

#corelib because the core is reserved for the Linux core dump
include(corelib/corelib.pri)
include(xml/xml.pri)
include(sql/sql.pri)
include(utils/utils.pri)
include(script/script.pri)
include(network/network.pri)

RESOURCES +=    \
#	$${MY_SUBPROJECT}.qrc

TRANSLATIONS    =                          \
	$${MY_SUBPROJECT}.cs_CZ.ts                          \
	$${MY_SUBPROJECT}.fr_FR.ts                          \

#tohle spravne nechodi, nevim jestli je chyba u trollu nebo u me
#target.path = $$DLLDESTDIR
#INSTALLS        += target

#message(INCLUDEPATH: $$INCLUDEPATH)
message("DEFINES: $$DEFINES")


