<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name></name>
    <message>
        <source>Field &apos;%1&apos; not found</source>
        <translation type="obsolete">Sloupec &apos;%1&apos; nenalezen</translation>
    </message>
    <message>
        <location filename="sql/qfsqlconnectionbase.cpp" line="190"/>
        <location filename="sql/qfsqlconnectionbase.cpp" line="197"/>
        <source>SQL Error
query: %1;</source>
        <translation>SQL Error
dotaz: %1;</translation>
    </message>
</context>
<context>
    <name>QFBasicTable</name>
    <message>
        <location filename="sql/qfbasictable.cpp" line="672"/>
        <source>col %1 is not valid column number (count: %2)</source>
        <translation>%1 není platné číslo sloupce (počet sloupců: %2)</translation>
    </message>
    <message>
        <location filename="sql/qfbasictable.cpp" line="1095"/>
        <source>Exportuji XML</source>
        <translation></translation>
    </message>
    <message>
        <location filename="sql/qfbasictable.cpp" line="1122"/>
        <source>Probiha export</source>
        <translation>Probíhá export</translation>
    </message>
</context>
<context>
    <name>QFBasicTable::FieldList</name>
    <message>
        <location filename="sql/qfbasictable.cpp" line="221"/>
        <source>Field %1 not found in query
Available fields are: %2</source>
        <translation>Sloupec %1 nenalezen v dotazu
Dostupné sloupce: %2</translation>
    </message>
</context>
<context>
    <name>QFDate</name>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="16"/>
        <source>pondeli</source>
        <translation>pondělí</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="16"/>
        <source>utery</source>
        <translation>úterý</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="16"/>
        <source>streda</source>
        <translation>středa</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="16"/>
        <source>ctvrtek</source>
        <translation>čtvrtek</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="16"/>
        <source>patek</source>
        <translation>pátek</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="16"/>
        <source>sobota</source>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="16"/>
        <source>nedele</source>
        <translation>neděle</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="26"/>
        <source>Po</source>
        <comment>jmeno dne</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="27"/>
        <source>Ut</source>
        <comment>jmeno dne</comment>
        <translation>Út</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="28"/>
        <source>St</source>
        <comment>jmeno dne</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="29"/>
        <source>Ct</source>
        <comment>jmeno dne</comment>
        <translation>Čt</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="30"/>
        <source>Pa</source>
        <comment>jmeno dne</comment>
        <translation>Pá</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="31"/>
        <source>So</source>
        <comment>jmeno dne</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="32"/>
        <source>Ne</source>
        <comment>jmeno dne</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="43"/>
        <source>leden</source>
        <comment>jmeno mesice</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="44"/>
        <source>unor</source>
        <comment>jmeno mesice</comment>
        <translation>únor</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="45"/>
        <source>brezen</source>
        <comment>jmeno mesice</comment>
        <translation>březen</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="46"/>
        <source>duben</source>
        <comment>jmeno mesice</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="47"/>
        <source>kveten</source>
        <comment>jmeno mesice</comment>
        <translation>květen</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="48"/>
        <source>cerven</source>
        <comment>jmeno mesice</comment>
        <translation>červen</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="49"/>
        <source>cervenec</source>
        <comment>jmeno mesice</comment>
        <translation>červenec</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="50"/>
        <source>srpen</source>
        <comment>jmeno mesice</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="51"/>
        <location filename="corelib/qfdatetime.cpp" line="64"/>
        <source>zari</source>
        <comment>jmeno mesice</comment>
        <translation>září</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="52"/>
        <source>rijen</source>
        <comment>jmeno mesice</comment>
        <translation>říjen</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="53"/>
        <source>listopad</source>
        <comment>jmeno mesice</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="54"/>
        <source>prosinec</source>
        <comment>jmeno mesice</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="56"/>
        <source>ledna</source>
        <comment>jmeno mesice</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="57"/>
        <source>unora</source>
        <comment>jmeno mesice</comment>
        <translation>února</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="58"/>
        <source>brezna</source>
        <comment>jmeno mesice</comment>
        <translation>března</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="59"/>
        <source>dubna</source>
        <comment>jmeno mesice</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="60"/>
        <source>kvetna</source>
        <comment>jmeno mesice</comment>
        <translation>května</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="61"/>
        <source>cervna</source>
        <comment>jmeno mesice</comment>
        <translation>června</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="62"/>
        <source>cervence</source>
        <comment>jmeno mesice</comment>
        <translation>července</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="63"/>
        <source>srpna</source>
        <comment>jmeno mesice</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="65"/>
        <source>rijna</source>
        <comment>jmeno mesice</comment>
        <translation>října</translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="66"/>
        <source>listopadu</source>
        <comment>jmeno mesice</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfdatetime.cpp" line="67"/>
        <source>prosince</source>
        <comment>jmeno mesice</comment>
        <translation></translation>
    </message>
</context>
<context>
    <name>QFDbXmlConfig</name>
    <message>
        <location filename="sql/qfdbxmlconfig.cpp" line="33"/>
        <source>cann&apos;t load dbxml for field %1</source>
        <translation>nelze nahrát dbxml pro sloupec %1</translation>
    </message>
</context>
<context>
    <name>QFDbXmlConfigLoader</name>
    <message>
        <location filename="xml/qfxmlconfig.cpp" line="664"/>
        <source>Template query exec error.
 %1</source>
        <translation>Template query exec error.
 %1</translation>
    </message>
    <message>
        <source>Template query returned 0 rows.
 %1</source>
        <translation type="obsolete">Template query returned 0 rows.
 %1</translation>
    </message>
    <message>
        <location filename="xml/qfxmlconfig.cpp" line="677"/>
        <source>Data query query returned 0 rows.
 %1</source>
        <translation>Dotaz nevrátil žádné řádky.
%1</translation>
    </message>
</context>
<context>
    <name>QFDomDocument</name>
    <message>
        <location filename="xml/qfdom.cpp" line="249"/>
        <source>ERROR </source>
        <translation>ERROR</translation>
    </message>
    <message>
        <location filename="xml/qfdom.cpp" line="251"/>
        <source>line: %1 </source>
        <translation>řádek: %1 </translation>
    </message>
    <message>
        <location filename="xml/qfdom.cpp" line="252"/>
        <source>col: %1</source>
        <translation>sloupec: %1</translation>
    </message>
    <message>
        <location filename="xml/qfdom.cpp" line="295"/>
        <location filename="xml/qfdom.cpp" line="470"/>
        <source>File &apos;%1&apos; can not be resolved trying %2.</source>
        <translation>Soubor &apos;%1&apos; nelze najít, pokusy: %2.</translation>
    </message>
</context>
<context>
    <name>QFHtmlUtils</name>
    <message>
        <location filename="utils/htmlutils/qfhtmlutils.cpp" line="170"/>
        <source>Nelze otevrit soubor %1 pro zapis.</source>
        <translation>Nelze otevřít soubor %1 pro zápis.</translation>
    </message>
    <message>
        <location filename="utils/htmlutils/qfhtmlutils.cpp" line="147"/>
        <source>Nelze vytvorit cestu &apos;%1&apos;.</source>
        <translation>Nelze vytvořit cestu &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>QFScriptDriver</name>
    <message>
        <location filename="script/scriptdriver/qfscriptdriver.cpp" line="66"/>
        <source>Skript &apos;%1&apos; neobsahuje funkci &apos;%2&apos;.</source>
        <translation>Skript &apos;%1&apos; neobsahuje funkci &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="script/scriptdriver/qfscriptdriver.cpp" line="68"/>
        <location filename="script/scriptdriver/qfscriptdriver.cpp" line="283"/>
        <source>Skript &apos;%1&apos; nebyl nalezen.</source>
        <translation>Skript &apos;%1&apos; nebyl nalezen.</translation>
    </message>
    <message>
        <location filename="script/scriptdriver/qfscriptdriver.cpp" line="70"/>
        <source>Nazev skriptu je prazdny.</source>
        <translation>Název skriptu je prazdný.</translation>
    </message>
    <message>
        <location filename="script/scriptdriver/qfscriptdriver.cpp" line="367"/>
        <source>Objekt neni funkce.</source>
        <translation>Objekt není funkce.</translation>
    </message>
    <message>
        <location filename="script/scriptdriver/qfscriptdriver.cpp" line="87"/>
        <source>Modul &apos;%1&apos; neobsahuje funkci &apos;%2&apos;.</source>
        <translation>Modul &apos;%1&apos; neobsahuje funkci &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="script/scriptdriver/qfscriptdriver.cpp" line="91"/>
        <source>Modul &apos;%1&apos; neni objekt ani funkce.</source>
        <translation>Modul &apos;%1&apos; není objekt ani funkce.</translation>
    </message>
    <message>
        <location filename="script/scriptdriver/qfscriptdriver.cpp" line="266"/>
        <source>Vyjjimka 
%3
 pri provadeni skriptu na radku %1
%2

%4</source>
        <translation>Vyjjímka 
%3
 při provádění skriptu na řádku %1
%2

%4</translation>
    </message>
    <message>
        <location filename="script/scriptdriver/qfscriptdriver.cpp" line="304"/>
        <source>Can&apos;t load code for module object: &apos;%1&apos;.</source>
        <translation>Nelze najít zdrojový kód pro modulární objekt &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="script/scriptdriver/qfscriptdriver.cpp" line="313"/>
        <source>Load module object error &apos;%1&apos;.</source>
        <translation>Chyba při nahrávání modulu &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="script/scriptdriver/qfscriptdriver.cpp" line="317"/>
        <source>Load module object error &apos;%1&apos; is neighter object nor function.</source>
        <translation>Chyba při nahrávání modulárního objektu: &apos;%1&apos; není ani objekt ani funkce.</translation>
    </message>
    <message>
        <location filename="script/scriptdriver/qfscriptdriver.cpp" line="373"/>
        <source>Vyjjimka ve funkci na radku %1
%2

%3</source>
        <translation>Vyjjímka ve funkci na řádku %1
%2

%3</translation>
    </message>
</context>
<context>
    <name>QFSmtpClient</name>
    <message>
        <location filename="network/qfsmtpclient/qfsmtpclient.cpp" line="150"/>
        <source>Server lookup failed:

</source>
        <translation>Neúspěšné připojení k serveru:</translation>
    </message>
    <message>
        <location filename="network/qfsmtpclient/qfsmtpclient.cpp" line="158"/>
        <source>Connecting to %1</source>
        <translation>Připojuji se k %1</translation>
    </message>
    <message>
        <location filename="network/qfsmtpclient/qfsmtpclient.cpp" line="166"/>
        <source>Secure Socket Client:

This system does not support OpenSSL.</source>
        <translation>SSL:

systém nepodporuje OpenSSL.</translation>
    </message>
    <message>
        <location filename="network/qfsmtpclient/qfsmtpclient.cpp" line="178"/>
        <source>Server connect timeout.</source>
        <translation>Čas vymezený pro připojení k serveru vypršel.</translation>
    </message>
    <message>
        <location filename="network/qfsmtpclient/qfsmtpclient.cpp" line="187"/>
        <source>Connected to %1</source>
        <translation>Připojeno k %1</translation>
    </message>
    <message>
        <location filename="network/qfsmtpclient/qfsmtpclient.cpp" line="282"/>
        <source>Message sent</source>
        <translation>Zpráva odeslána</translation>
    </message>
    <message>
        <location filename="network/qfsmtpclient/qfsmtpclient.cpp" line="300"/>
        <source>Unexpected reply from SMTP server:

</source>
        <translation>Neočekávaná odpověď SMTP servru:

</translation>
    </message>
    <message>
        <location filename="network/qfsmtpclient/qfsmtpclient.cpp" line="319"/>
        <source>Socket is NULL or not type of QSslSocket.</source>
        <translation>Socket je NULL nebo není typu QSslSocket.</translation>
    </message>
    <message>
        <location filename="network/qfsmtpclient/qfsmtpclient.cpp" line="141"/>
        <source>Transfer started</source>
        <translation>Začátek přenosu</translation>
    </message>
    <message>
        <location filename="network/qfsmtpclient/qfsmtpclient.cpp" line="60"/>
        <source>Socket is NULL.</source>
        <translation>Socket is NULL.</translation>
    </message>
    <message>
        <location filename="network/qfsmtpclient/qfsmtpclient.cpp" line="96"/>
        <source>Attachment file &apos;%1&apos; can&apos;t be open for reading.</source>
        <translation>Nelze otevřít přílohu &apos;%1&apos; pro čtení.</translation>
    </message>
    <message>
        <location filename="network/qfsmtpclient/qfsmtpclient.cpp" line="223"/>
        <source>Unexpected server answer line &apos;%1&apos; (too short).</source>
        <translation>Server vrátil překvapivý řádek &apos;%1&apos; (příliš krátký).</translation>
    </message>
    <message>
        <location filename="network/qfsmtpclient/qfsmtpclient.cpp" line="239"/>
        <source>Only AUTH LOGIN is currently supported.</source>
        <translation>Vsoučasnosti je podporována pouze metoda AUTH LOGIN.</translation>
    </message>
</context>
<context>
    <name>QFSqlCatalog</name>
    <message>
        <location filename="sql/qfsqlcatalog.cpp" line="100"/>
        <source>Connection does not contain database/schema name &apos;%1&apos;</source>
        <translation>Připojená databáze neobsahuje databázi/schéma jménem &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="sql/qfsqlcatalog.cpp" line="113"/>
        <source>database/schema name &apos;%1&apos; can not be found.</source>
        <translation>Databáze/schéma jménem &apos;%1&apos; nebyla nalezena.</translation>
    </message>
</context>
<context>
    <name>QFSqlConnectionBase</name>
    <message>
        <location filename="sql/qfsqlconnectionbase.cpp" line="138"/>
        <source>Error opening database %1</source>
        <translation>Chyba při otevírání databáze %1</translation>
    </message>
    <message>
        <location filename="sql/qfsqlconnectionbase.cpp" line="180"/>
        <source>database &apos;%1&apos; on %2@%3 driver %4</source>
        <translation>databáze &apos;%1&apos; na %2@%3 ovladač %4</translation>
    </message>
    <message>
        <location filename="sql/qfsqlconnectionbase.cpp" line="251"/>
        <source>Error getting table list for database &apos;%1&apos;
query: %2;</source>
        <translation>Chyba při načítání seznamu tabulek pro databázi &apos;%1&apos;
dotaz: %2;</translation>
    </message>
    <message>
        <location filename="sql/qfsqlconnectionbase.cpp" line="552"/>
        <source>Error getting table list for database &apos;%1&apos;</source>
        <translation>Chyba při načítání seznamu tabulek pro databázi &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="sql/qfsqlconnectionbase.cpp" line="580"/>
        <source>Error getting table list for database &apos;%1&apos;

%2</source>
        <translation>Chyba při načítání seznamu tabulek pro databázi &apos;%1&apos;

%2</translation>
    </message>
    <message>
        <location filename="sql/qfsqlconnectionbase.cpp" line="650"/>
        <location filename="sql/qfsqlconnectionbase.cpp" line="702"/>
        <source>Error getting sql for &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="sql/qfsqlconnectionbase.cpp" line="724"/>
        <source>Error select from &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="sql/qfsqlconnectionbase.cpp" line="328"/>
        <source>table &apos;%1&apos; does not contains fields. (maybe it is in other schema?)</source>
        <translation>tabulka &apos;%1&apos; neobsahuje sloupce. (možná je v jiné databázi?)</translation>
    </message>
</context>
<context>
    <name>QFSqlDbInfo</name>
    <message>
        <location filename="sql/qfsqlcatalog.cpp" line="317"/>
        <source>Database/schema &apos;%1&apos; does not contain  table &apos;%2&apos;</source>
        <translation>Databáse/schema &apos;%1&apos; neobsahuje tabulku &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="sql/qfsqlcatalog.cpp" line="359"/>
        <source>Table &apos;%1&apos; not found</source>
        <translation>Tabulka &apos;%1&apos; nebyla nalezena</translation>
    </message>
</context>
<context>
    <name>QFSqlQuery</name>
    <message>
        <location filename="sql/qfsqlquery.cpp" line="176"/>
        <location filename="sql/qfsqlquery.cpp" line="208"/>
        <source>Nelze otevrit soubor &apos;%1&apos;</source>
        <translation>Nelze otevřít soubor &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="sql/qfsqlquery.cpp" line="194"/>
        <source>Skript &apos;%1&apos; nebyl nalezen</source>
        <translation>Skript &apos;%1&apos; nebyl nalezen</translation>
    </message>
    <message>
        <location filename="sql/qfsqlquery.cpp" line="224"/>
        <source>Column named &apos;%1&apos; not found in query record.
Available columns are:
</source>
        <translation>Jméno sloupce &apos;%1&apos; nebylo nalezeno ve výsledku dotazu.
Dostupná jména jsou:
</translation>
    </message>
</context>
<context>
    <name>QFSqlQueryBuilder</name>
    <message>
        <location filename="sql/qfsqlquerybuilder.cpp" line="365"/>
        <source>query does not start with SELECT statement
%1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QFSqlStorageDriver</name>
    <message>
        <location filename="sql/drivers/qfsqlstoragedriver.cpp" line="273"/>
        <source>index field &apos;%1&apos; not found in fields</source>
        <translation>sloupec indexu &apos;%1&apos; nebyl nalezen v seznamu sloupců</translation>
    </message>
    <message>
        <location filename="sql/drivers/qfsqlstoragedriver.cpp" line="348"/>
        <source>The resultset doesn&apos;t have all pri keys for delete in table &apos;%1&apos;</source>
        <translation>V datech nejsou všechny primární klíče pro mazání v tabulce &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="sql/drivers/qfsqlstoragedriver.cpp" line="365"/>
        <source>There is not a primary key to remove the row.</source>
        <translation>Neexistuje primární klíč pro odebrání řádku.</translation>
    </message>
</context>
<context>
    <name>QFSqlStorageDriverProperties</name>
    <message>
        <location filename="sql/drivers/qfsqlstoragedriver.cpp" line="551"/>
        <source>QFSqlQueryModel::connection() - connection is not valid!</source>
        <translation>QFSqlQueryModel::connection() - neplatné připojení!</translation>
    </message>
</context>
<context>
    <name>QFSqlTableInfo</name>
    <message>
        <location filename="sql/qfsqlcatalog.cpp" line="629"/>
        <source>Field %2.%1 not found and can not be loaded.</source>
        <translation>Sloupec %2.%1 nebyl nalezen v cache a nelze jej nahrát.</translation>
    </message>
    <message>
        <location filename="sql/qfsqlcatalog.cpp" line="573"/>
        <location filename="sql/qfsqlcatalog.cpp" line="591"/>
        <source>Found info for nonexisting field &apos;%1&apos;</source>
        <translation>Nalezeno info pro neexistující pole &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Field &apos;%1&apos; not found and can not be loaded.</source>
        <translation type="obsolete">Sloupec &apos;%1&apos; a nelze jej nahrát.</translation>
    </message>
    <message>
        <location filename="sql/qfsqlcatalog.h" line="178"/>
        <source>Field &apos;%1&apos; not found</source>
        <translation>Sloupec &apos;%1&apos; nenalezen</translation>
    </message>
</context>
<context>
    <name>QFString</name>
    <message>
        <location filename="corelib/qfstring.cpp" line="558"/>
        <source>jedna</source>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="559"/>
        <location filename="corelib/qfstring.cpp" line="560"/>
        <source>jeden</source>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="561"/>
        <source>dva</source>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="562"/>
        <source>tri</source>
        <translation>tři</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="563"/>
        <source>ctyri</source>
        <translation>čtyři</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="564"/>
        <source>pet</source>
        <translation>pět</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="565"/>
        <source>sest</source>
        <translation>šest</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="566"/>
        <source>sedm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="567"/>
        <source>osm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="568"/>
        <source>devet</source>
        <translation>devět</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="571"/>
        <location filename="corelib/qfstring.cpp" line="580"/>
        <source>deset</source>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="572"/>
        <source>dvacet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="573"/>
        <source>tricet</source>
        <translation>třicet</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="574"/>
        <source>ctyricet</source>
        <translation>čtyřicet</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="575"/>
        <source>padesat</source>
        <translation>padesát</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="576"/>
        <source>sedesat</source>
        <translation>šedesát</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="577"/>
        <source>sedmdesat</source>
        <translation>sedmdesát</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="578"/>
        <source>osmdesat</source>
        <translation>osmdesát</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="579"/>
        <source>devadesat</source>
        <translation>devadesát</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="581"/>
        <source>jedenact</source>
        <translation>jedenáct</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="582"/>
        <source>dvanact</source>
        <translation>dvanáct</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="583"/>
        <source>trinact</source>
        <translation>třináct</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="584"/>
        <source>ctrnact</source>
        <translation>čtrnáct</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="585"/>
        <source>patnact</source>
        <translation>patnáct</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="586"/>
        <source>sestnact</source>
        <translation>šestnáct</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="587"/>
        <source>sedmnact</source>
        <translation>sedmnáct</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="588"/>
        <source>osmnact</source>
        <translation>osmnáct</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="589"/>
        <source>devatnact</source>
        <translation>devatnáct</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="592"/>
        <source>sto</source>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="593"/>
        <source>dveste</source>
        <translation>dvěstě</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="594"/>
        <source>trista</source>
        <translation>třista</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="595"/>
        <source>ctyrista</source>
        <translation>čtyřista</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="596"/>
        <source>petset</source>
        <translation>pětset</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="597"/>
        <source>sestset</source>
        <translation>šestset</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="598"/>
        <source>sedmset</source>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="599"/>
        <source>osmset</source>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="600"/>
        <source>devetset</source>
        <translation>devětset</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="615"/>
        <source>nula</source>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="619"/>
        <source>milion</source>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="620"/>
        <source>miliony</source>
        <translation></translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="621"/>
        <source>milionu</source>
        <translation>milionů</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="625"/>
        <location filename="corelib/qfstring.cpp" line="627"/>
        <source>tisic</source>
        <translation>tisíc</translation>
    </message>
    <message>
        <location filename="corelib/qfstring.cpp" line="626"/>
        <source>tisice</source>
        <translation>tisíce</translation>
    </message>
</context>
<context>
    <name>QFXmlConfigFileLoader</name>
    <message>
        <location filename="xml/qfxmlconfig.cpp" line="554"/>
        <source>Error open configuration template &apos;%1&apos;.
%2</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="xml/qfxmlconfig.cpp" line="631"/>
        <source>Cann&apos;t create configuration directory &apos;%1&apos;</source>
        <translation>Nelze vytvořit konfigurační adresář &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="sql/qfbasictable.cpp" line="697"/>
        <source>Table has no columns, row can not be inserted.</source>
        <translation>Tabulka nemá sloupce, řádek nelze vložit.</translation>
    </message>
    <message>
        <location filename="corelib/qf.cpp" line="18"/>
        <source>dd.MM.yyyy</source>
        <comment>defaultDateFormat</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="sql/qfsqlcatalog.cpp" line="681"/>
        <location filename="sql/qfsqlcatalog.cpp" line="709"/>
        <source>Error getting the sequence nextval(&apos;%1&apos;)</source>
        <translation>Chyba při získání sekvenční hodnoty nextval(&apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="utils/fileutils/qffileutils.cpp" line="110"/>
        <source>Cann&apos;t create temporary directory &apos;%1&apos;</source>
        <translation>Nelze vytvořit dočasný adresář &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="xml/qfdom.cpp" line="145"/>
        <location filename="xml/qfdom.cpp" line="157"/>
        <source>QFDomElement::mkcdHelper(&apos;%1&apos;) path not found and should not be created (param mkdir == false).</source>
        <translation>QFDomElement::mkcdHelper(&apos;%1&apos;) cesta nebyla nalezena a nemá být vytvořena (param mkdir == false).</translation>
    </message>
    <message>
        <source>QFXmlTreeModel::setContent(QFile &amp;f): ERROR open file &apos;%1&apos;</source>
        <translation type="obsolete">QFXmlTreeModel::setContent(QFile &amp;f): ERROR otvírání souboru &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="xml/qfdom.cpp" line="274"/>
        <source>ERROR</source>
        <comment>QFDomDocument::setContent</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="xml/qfdom.cpp" line="276"/>
        <source>line: %1 </source>
        <comment>QFDomDocument::setContent</comment>
        <translation>řádek: %1 </translation>
    </message>
    <message>
        <location filename="xml/qfdom.cpp" line="277"/>
        <source>col: %1</source>
        <comment>QFDomDocument::setContent</comment>
        <translation>sloupec: %1</translation>
    </message>
    <message>
        <location filename="xml/qfdom.cpp" line="332"/>
        <source>QFDomDocument::save</source>
        <comment>QFXmlTreeModel::save(QFile &amp;f): ERROR open file &apos;%1&apos;</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="xml/qfxmlconfig.cpp" line="635"/>
        <source>Can not open configuration file &apos;%1&apos; for writing.</source>
        <translation>Nelze vytvořit konfigurační soubor &apos;%1&apos; pro zápis.</translation>
    </message>
    <message>
        <location filename="xml/qfxmlconfig.cpp" line="619"/>
        <source>Can not open configuration file &apos;%1&apos; for reading.</source>
        <translation>Nelze otevřít konfigurační soubor &apos;%1&apos; pro čtení.</translation>
    </message>
    <message>
        <location filename="corelib/qf.cpp" line="19"/>
        <source>hh:mm:ss</source>
        <comment>defaultTimeFormat</comment>
        <translation>hh:mm:ss</translation>
    </message>
    <message>
        <location filename="corelib/qf.cpp" line="20"/>
        <source>dd.MM.yyyy hh:mm:ss</source>
        <comment>defaultDateTimeFormat</comment>
        <translation>dd.MM.yyyy hh:mm:ss</translation>
    </message>
    <message>
        <location filename="sql/qfbasictable.cpp" line="698"/>
        <source>Inserted row comes from different table.</source>
        <translation>Vkládaný řádek pochází z jiné tabulky.</translation>
    </message>
    <message>
        <location filename="xml/qfxmltable.cpp" line="680"/>
        <source>0(&apos;%1&apos;) path not found.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="xml/qfxmltable.cpp" line="689"/>
        <source>1(&apos;%1&apos;) path not found.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="xml/qfxmltable.cpp" line="695"/>
        <source>2(&apos;%1&apos;) path not found.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="xml/qfxmltable.cpp" line="705"/>
        <source>3(&apos;%1&apos;) path not found.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="xml/qfxmltable.cpp" line="712"/>
        <source>4(&apos;%1&apos;) path not found.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="xml/qfdom.cpp" line="235"/>
        <source>ERROR open file &apos;%1&apos;</source>
        <translation>Chyba při otevírání souboru %1</translation>
    </message>
</context>
</TS>
