
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfappinterface.h"

#include <qffileutils.h>

#include <qflogcust.h>

QFAppInterface::QFAppInterface() 
{
}

QString QFAppInterface::tempDir()
{
	return QFFileUtils::tempDir();
}

QString QFAppInterface::settingsDir() const
{
	QString app_name =  QFCompat::appName();
	QString home_dir =  QFFileUtils::homeDir();
	QString settings_dir = QFFileUtils::joinPath(home_dir, "." + app_name);
	return settings_dir;
}

int QFAppInterface::versionNumber()
{
	QString s = versionString();
	//qfInfo() << s;
	/// pokud je verze napr. 1.2.3-rc2
	/// oddelej -rc2
	int ix = s.indexOf("-");
	if(ix >= 0) s = s.mid(0, ix);
	QStringList sl = s.split('.');
	int ver[] = {0, 0, 0};
	for(int i=0; i<sl.count() && i < 3; i++) {
		ver[i] = sl.value(i).toInt();
	}
	int ret = 0;
	for(int i=0; i < 3; i++) {
		ret = 100 * ret + ver[i];
		//qfInfo() << ver[i] << ret;
	}
	return ret;
}

int QFAppInterface::versionMajor()
{
	int ret = versionNumber() / 10000;
	return ret;
}
