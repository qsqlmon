
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdatetime.h"

#include <qflogcust.h>

QString QFDate::dayName(const QDate &d)
{
	static QStringList day_names;
	if(day_names.isEmpty()) {
		day_names << tr("pondeli") << tr("utery") << tr("streda") << tr("ctvrtek") << tr("patek") << tr("sobota") << tr("nedele");
	}
	if(d.isValid()) return day_names[d.dayOfWeek() - 1];
	return QString();
}

QString QFDate::dayAbbreviation(int cislo_dne)
{
	static QMap<int, QString> jmena_dnu;
	if(jmena_dnu.isEmpty()) {
		jmena_dnu[0] = tr("Po", "jmeno dne");
		jmena_dnu[1] = tr("Ut", "jmeno dne");
		jmena_dnu[2] = tr("St", "jmeno dne");
		jmena_dnu[3] = tr("Ct", "jmeno dne");
		jmena_dnu[4] = tr("Pa", "jmeno dne");
		jmena_dnu[5] = tr("So", "jmeno dne");
		jmena_dnu[6] = tr("Ne", "jmeno dne");
	}
	return jmena_dnu.value(cislo_dne);
}

QString QFDate::monthName(const QDate & d, bool sklonovat)
{
	int month = d.month();
	static QMap<int, QString> jmena;
	static QMap<int, QString> jmena2;
	if(jmena.isEmpty()) {
		jmena[1] = tr("leden", "jmeno mesice");
		jmena[2] = tr("unor", "jmeno mesice");
		jmena[3] = tr("brezen", "jmeno mesice");
		jmena[4] = tr("duben", "jmeno mesice");
		jmena[5] = tr("kveten", "jmeno mesice");
		jmena[6] = tr("cerven", "jmeno mesice");
		jmena[7] = tr("cervenec", "jmeno mesice");
		jmena[8] = tr("srpen", "jmeno mesice");
		jmena[9] = tr("zari", "jmeno mesice");
		jmena[10] = tr("rijen", "jmeno mesice");
		jmena[11] = tr("listopad", "jmeno mesice");
		jmena[12] = tr("prosinec", "jmeno mesice");
		
		jmena2[1] = tr("ledna", "jmeno mesice");
		jmena2[2] = tr("unora", "jmeno mesice");
		jmena2[3] = tr("brezna", "jmeno mesice");
		jmena2[4] = tr("dubna", "jmeno mesice");
		jmena2[5] = tr("kvetna", "jmeno mesice");
		jmena2[6] = tr("cervna", "jmeno mesice");
		jmena2[7] = tr("cervence", "jmeno mesice");
		jmena2[8] = tr("srpna", "jmeno mesice");
		jmena2[9] = tr("zari", "jmeno mesice");
		jmena2[10] = tr("rijna", "jmeno mesice");
		jmena2[11] = tr("listopadu", "jmeno mesice");
		jmena2[12] = tr("prosince", "jmeno mesice");
	}
	if(sklonovat) return jmena2.value(month);
	return jmena.value(month);
}

