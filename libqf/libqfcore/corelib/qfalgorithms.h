#ifndef QFALGORITHMS_H
#define QFALGORITHMS_H

template <typename RandomAccessIterator, typename T, typename LessThan>
Q_OUTOFLINE_TEMPLATE RandomAccessIterator qfBinaryFind(RandomAccessIterator begin, RandomAccessIterator end, const T &value, LessThan less_than)
{
    int l = 0;
    int r = end - begin - 1;
    if (r < 0)
        return end;
    int i = (l + r + 1) / 2;

    while (r != l) {
        if (less_than(value, begin[i]))
            r = i - 1;
        else
            l = i;
        i = (l + r + 1) / 2;
    }
    if (less_than(begin[i], value) || less_than(value, begin[i]))
        return end;
    else
        return begin + i;
}

#endif // QFALGORITHMS_H
