
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qftreeitembase.h"

QFTreeItemBase::QFTreeItemBase(QFTreeItemBase *_parent)
{
	f_parent = NULL;
	setParent(_parent);
}

QFTreeItemBase::~QFTreeItemBase()
{
	if(f_parent) f_parent->unlinkChild(this);
	clearChildren();
}


void QFTreeItemBase::setParent(QFTreeItemBase *_parent)
{
	if(f_parent) f_parent->unlinkChild(this);
	f_parent = _parent;
	if(f_parent) f_parent->f_children << this;
}

void QFTreeItemBase::clearChildren()
{
	foreach(QFTreeItemBase *it, children()) {
		/// optimalizace, aby dite v destruktoru nemazelo zbytecne samo sebe z children, kdyz se to pak udela najednou.
		if(it) {
			it->f_parent = NULL;
			delete it;
		}
	}
	f_children.clear();
}

void QFTreeItemBase::unlinkChild(QFTreeItemBase *child)
{
	if(f_children.isEmpty()) return;
	int i = f_children.indexOf(child);
	if(i >= 0) f_children.removeAt(i);
}

