
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDATETIME_H
#define QFDATETIME_H

#include <qf.h>
#include <qfcoreglobal.h>

#include <QDate>
#include <QCoreApplication>

/*
//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFDateTime : public QDateTime
{
	public:
		QFDateTime(QObject *parent = NULL);
		virtual ~QFDateTime();
};
*/
//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFDate : public QDate
{
	Q_DECLARE_TR_FUNCTIONS(QFDate);
	protected:
		bool f_null;
	public:
		bool isNull() const {return f_null;}
		void setNull(bool b) {f_null = b;}

		QString toString() {return QDate::toString(Qf::defaultDateFormat());}

		static QString dayName(const QDate &d);
		static QString dayAbbreviation(int cislo_dne);
		static QString monthName(const QDate &d, bool sklonovat = false);
	public:
		QFDate(const QDate &d = QDate()) : QDate(d), f_null(false) {}
		//virtual ~QFDateTime();
};
//Q_DECLARE_METATYPE(QFDate);
#endif // QFDATETIME_H

