#include "qfcompat.h"

#include <qfstring.h>
#include <qffileutils.h>
//#include <qfapplication.h>

#include <QDir>
#include <QFileInfo>
#include <QCoreApplication>

QString QFCompat::appExeName()
{
	QString s = QFileInfo(QCoreApplication::applicationFilePath()).baseName();
	return s;
}

QString QFCompat::appName()
{
	QFString s = QCoreApplication::applicationName();
	if(s.isEmpty()) {
		s = appExeName();
		int ix = s.indexOf('.');
		if(ix > 0) s = s.slice(0, ix);
		s = s.toLower();
	}
	return s;
}

QString QFCompat::appDir()
{
	return QFFileUtils::appDir();
}



