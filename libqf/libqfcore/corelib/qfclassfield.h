#ifndef QFCLASSFIELD_H
#define QFCLASSFIELD_H
/*
#ifndef QF_FIELD_INTERNAL
#define QF_FIELD_INTERNAL(ptype, perm_getter, getter_prefix, perm_setter, setter_prefix, name_rest) \
	protected: ptype f_##getter_prefix##name_rest; \
	perm_getter: ptype getter_prefix##name_rest() const {return f_##getter_prefix##name_rest;} \
	perm_setter: ptype setter_prefix##name_rest() {ptype fp = f_##getter_prefix##name_rest; f_##getter_prefix##name_rest = fp; return fp;}
#endif

#define QF_FIELD_R(ptype, getter_prefix, setter_prefix, name_rest) \
		QF_FIELD_INTERNAL(ptype, public, getter_prefix, protected, setter_prefix, name_rest)
#define QF_FIELD_W(ptype, getter_prefix, setter_prefix, name_rest) \
		QF_FIELD_INTERNAL(ptype, protected, getter_prefix, public, setter_prefix, name_rest)
#define QF_FIELD_RW(ptype, getter_prefix, setter_prefix, name_rest) \
		QF_FIELD_INTERNAL(ptype, public, getter_prefix, public, setter_prefix, name_rest)
*/

#define QF_FIELD_R(ptype, getter_prefix, setter_prefix, name_rest) \
	protected: ptype f_##getter_prefix##name_rest; \
	public: ptype getter_prefix##name_rest() const {return f_##getter_prefix##name_rest;} \
	protected: void setter_prefix##name_rest(const ptype &val) {f_##getter_prefix##name_rest = val;}

#define QF_FIELD_W(ptype, getter_prefix, setter_prefix, name_rest) \
	protected: ptype f_##getter_prefix##name_rest; \
	protected: ptype getter_prefix##name_rest() const {return f_##getter_prefix##name_rest;} \
	public: void setter_prefix##name_rest(const ptype &val) {f_##getter_prefix##name_rest = val;}

#define QF_FIELD_RW(ptype, getter_prefix, setter_prefix, name_rest) \
	protected: ptype f_##getter_prefix##name_rest; \
	public: ptype getter_prefix##name_rest() const {return f_##getter_prefix##name_rest;} \
	public: void setter_prefix##name_rest(const ptype &val) {f_##getter_prefix##name_rest = val;}
/*
//bohuzel to vypada, ze moc neexpanduje makra, takze takhle definovanou property vubec nenajde
#define QF_PROPERTY_RW(ptype, getter_prefix, setter_prefix, name_rest) \
	Q_PROPERTY(ptype getter_prefix##name_rest READ getter_prefix##name_rest WRITE setter_prefix##name_rest) \
	QF_FIELD_RW(ptype, getter_prefix, setter_prefix, name_rest)
*/
#endif // QFCLASSFIELD_H
