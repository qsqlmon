
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfappdbconnectioninterface.h"

#include <qfsqlconnection.h>

#include <qflogcust.h>

QFAppDbConnectionInterface::QFAppDbConnectionInterface() 
{
	f_connection = NULL;
}

QFAppDbConnectionInterface::~ QFAppDbConnectionInterface()
{
	qfLogFuncFrame();
	if(f_connection) {
		f_connection->close();
		delete f_connection;
	}
}

QFSqlConnection & QFAppDbConnectionInterface::connection()
{
	/// musim to takhle delat, protoze drivery se nahravaji pri tvorbe objektu aplikace
	/// takze nemuzu f_connection vytvorit v konstruktoru, protoze jeste neni nahran
	/// driver plugin.
	//qfTrash() << QF_FUNC_NAME << "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&";
	if(!f_connection) f_connection = new QFSqlConnection();
	return *f_connection;
}

QFSqlQuery QFAppDbConnectionInterface::execSql(const QString & query) throw( QFException )
{
	QFSqlQuery q(connection());
	q.exec(query);
	return q;
}

QFSqlConnection & QFAppDbConnectionInterface::dummyConnection()
{
	static QFSqlConnection c;
	return c;
}


