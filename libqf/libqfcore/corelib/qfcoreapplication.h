
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFCOREAPPLICATION_H
#define QFCOREAPPLICATION_H

#include "qfappconfiginterface.h"
#include "qfappinterface.h"

#include <qfcoreglobal.h>

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFCoreApplication : public QCoreApplication, public QFAppConfigInterface, public QFAppInterface 
{
	Q_OBJECT;
	public:
		static QFCoreApplication* instance();
	public:
		QFCoreApplication(int & argc, char ** argv );
		virtual ~QFCoreApplication();
};

inline QFCoreApplication* qfApp() {return QFCoreApplication::instance();}

#endif // QFCOREAPPLICATION_H

