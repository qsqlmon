# Input

INCLUDEPATH += $$PWD

win32:!win32-g++ {
	HEADERS +=              \
		$$PWD/msvc/DbgHelp.h              \
		$$PWD/msvc/SimpleSymbolEngine.h

	SOURCES +=              \
		$$PWD/msvc/SimpleSymbolEngine.cpp

	LIBS += $$PWD/msvc/DbgHelp.lib

	# po nmake install nainstaluje DbgHelp.dll do $$DLLDESTDIR
	dbghelp.path = $$DLLDESTDIR
	dbghelp.files = $$PWD/msvc/DbgHelp.dll
	INSTALLS += dbghelp
}

HEADERS +=              \
    $$PWD/qfexception.h              \
    $$PWD/qfassert.h              \
    $$PWD/qfstring.h              \
    $$PWD/qfstacktrace.h              \
    $$PWD/qfclassfield.h              \
    $$PWD/qfptrlist.h              \
    $$PWD/qfcoreglobal.h              \
#$$PWD/qfexplicitlyshareddata.h              \
    $$PWD/qflog.h              \
    $$PWD/qflogcust.h              \
    $$PWD/qfcompat.h              \
	$$PWD/qf.h              \
	$$PWD/qfflags.h            \
	$$PWD/qftreeitembase.h           \
	$$PWD/qfalgorithms.h           \
	$$PWD/qfdatetime.h          \
	$$PWD/qftime.h         \
	$$PWD/qfappconfiginterface.h       \
	$$PWD/qfappinterface.h      \
	$$PWD/qfappdbconnectioninterface.h     \
	$$PWD/qfcoreapplication.h    \
	$$PWD/qfcoredbapplication.h   \
	$$PWD/qfsearchdirs.h  \

SOURCES +=              \
    $$PWD/qfexception.cpp              \
    $$PWD/qfstacktrace.cpp              \
    $$PWD/qfstring.cpp              \
#    $$PWD/qfcoreglobal.cpp              \
    $$PWD/qflog.cpp              \
    $$PWD/qfcompat.cpp              \
	$$PWD/qf.cpp              \
	$$PWD/qftreeitembase.cpp           \
	$$PWD/qfdatetime.cpp          \
	$$PWD/qftime.cpp         \
	$$PWD/qfappconfiginterface.cpp       \
	$$PWD/qfappinterface.cpp      \
	$$PWD/qfappdbconnectioninterface.cpp     \
	$$PWD/qfcoreapplication.cpp    \
	$$PWD/qfcoredbapplication.cpp   \
	$$PWD/qfsearchdirs.cpp  \


