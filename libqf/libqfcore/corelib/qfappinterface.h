
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFAPPINTERFACE_H
#define QFAPPINTERFACE_H

#include <qfcoreglobal.h>

#include <QString>


//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFAppInterface 
{
	public:
		//! vraci QFFileUtils::tempDir()
		virtual QString tempDir();
		//! vraci $HOME/.appname.
		virtual QString settingsDir() const;
		//! vraci settingsDir, pokud neexistuje, vytvori ho.
		//QDir ensureSettingsDir() throw(QFException);
		
		virtual QString versionString() {return "0.0.1";}
		/// generuje se z versionString() a je to major, minor, patch ve tvaru MMNNPP
		int versionNumber();
		int versionMajor();
		
		//virtual void redirectLog();
	public:
		QFAppInterface();
		virtual ~QFAppInterface() {}
};

#endif // QFAPPINTERFACE_H

