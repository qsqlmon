#ifndef QFSTRING_H
#define QFSTRING_H

#include <limits.h>
#include <qfcoreglobal.h>

#include <QCoreApplication> /// kvuli Q_DECLARE_TR_FUNCTIONS
#include <QString>
#include <QStringList>
#include <QTime>
#include <QDate>
#include <QVariantMap>

class QFCORE_DECL_EXPORT QFString : public QString
{
	Q_DECLARE_TR_FUNCTIONS(QFString);
	public:
		enum TrimBehavior { LeavePartsUnchanged, TrimParts };
		typedef QMap<QString, QString> StringMap;
	public:
		QFString() : QString() {}
		QFString(int n) : QString(QString::number(n)) {}
		QFString(QChar ch) : QString(ch) {}
		QFString(const QString & s) : QString(s) {}
		QFString(const QVariant &v);
		QFString(const char* str) : QString(str) {}
		//QFString(const std::string & str) : QString(str) {}
		~QFString() {}

	public:
		inline int len() const {return length();}

		QFString ltrim() const;
		QFString rtrim() const;
		QFString trim() const {return trimmed();}


		int pos(QChar what_char, QChar quote = 0) const;

		/// jako QString indexOf(), ale hledane slovo musi byt slovo a nemuze byt soucasti delsiho slova
		int indexOfWord(const QString &word, Qt::CaseSensitivity cs = Qt::CaseSensitive) const;
		
		/**
		* python's like slice
		* @param start from index, can be negative
		* @param end to index, cam be begative or omitted
		*/
		QFString slice(int start, int end = INT_MAX) const;
		/*
		QFString arg ( const QFString & a, int fieldWidth = 0, const QChar & fillChar = QLatin1Char( ' ' ) ) const
		{return QString::arg(a, fieldWidth, fillChar);}
		QFString arg ( int a, int fieldWidth = 0, int base = 10, const QChar & fillChar = QLatin1Char( ' ' ) ) const
		{return QString::arg(a, fieldWidth, base, fillChar);}
		QFString arg ( uint a, int fieldWidth = 0, int base = 10, const QChar & fillChar = QLatin1Char( ' ' ) ) const
		{return QString::arg(a, fieldWidth, base, fillChar);}
		QFString arg ( long a, int fieldWidth = 0, int base = 10, const QChar & fillChar = QLatin1Char( ' ' ) ) const
		{return QString::arg(a, fieldWidth, base, fillChar);}
		QFString arg ( ulong a, int fieldWidth = 0, int base = 10, const QChar & fillChar = QLatin1Char( ' ' ) ) const
		{return QString::arg(a, fieldWidth, base, fillChar);}
		QFString arg ( qlonglong a, int fieldWidth = 0, int base = 10, const QChar & fillChar = QLatin1Char( ' ' ) ) const
		{return QString::arg(a, fieldWidth, base, fillChar);}
		QFString arg ( qulonglong a, int fieldWidth = 0, int base = 10, const QChar & fillChar = QLatin1Char( ' ' ) ) const
		{return QString::arg(a, fieldWidth, base, fillChar);}
		QFString arg ( short a, int fieldWidth = 0, int base = 10, const QChar & fillChar = QLatin1Char( ' ' ) ) const
		{return QString::arg(a, fieldWidth, base, fillChar);}
		QFString arg ( ushort a, int fieldWidth = 0, int base = 10, const QChar & fillChar = QLatin1Char( ' ' ) ) const
		{return QString::arg(a, fieldWidth, base, fillChar);}
		QFString arg ( QChar a, int fieldWidth = 0, const QChar & fillChar = QLatin1Char( ' ' ) ) const
		{return QString::arg(a, fieldWidth, fillChar);}
		QFString arg ( char a, int fieldWidth = 0, const QChar & fillChar = QLatin1Char( ' ' ) ) const
		{return QString::arg(a, fieldWidth, fillChar);}
		QFString arg ( double a, int fieldWidth = 0, char fmt = 'g', int prec = -1, const QChar & fillChar = QLatin1Char( ' ' ) ) const
		{return QString::arg(a, fieldWidth, fmt, prec, fillChar);}
		*/
		QFString qfarg(const QVariant &v) const;

		//! Split string and optionaly trim splitted parts
		/// If fs ends with \a sep, an empty string is appended at the end of string list.
		/// if fs contain n \a sep chars FStringList will contain n+1 strings.
		/// Empty string produce empty string list
		/// @param trim_parts trim strings and remove quotes in  list after parsing
		QStringList splitAndTrim(QChar sep = '\t', QChar quote = 0, TrimBehavior trim_parts = QFString::TrimParts, SplitBehavior keep_empty_parts = QString::SkipEmptyParts) const;

		bool toBool() const;
		QDate toDate() const;
		/**
		str: '11' time: 11:00:00.000
		str: '11.3' time: 00:00:11.300
		str: '33:5' time: 09:05:00.000
		str: '0:333:5' time: 05:33:05.000
		str: '12:30:12' time: 12:30:12.000
		str: '23.98' time: 00:00:23.980
		str: '13:5:06.1' time: 13:05:06.100
		*/
		QTime toTime() const;

		//! to local8bit
		const char* str() const;

		//! finds matching bracket, if string don't start with opening bracket, the outermost closing bracket in the string is found..
		/// @return -1 if the opening or matching bracket is not found. String length if no brackets are found.
		int indexOfMatchingBracket(char opening_bracket = '(', char closing_bracket = ')', char quote = '\'') const;
		//! Splits string using only separators neither in brackets nor in quotes, quoted brackets are ignored.
		//! If trim parts is on, also enclosing brackets (if any) are removed form splitted parts and the content is trimmed.
		//! If part is empty (after optional trimming) and \a keep_empty_parts != \a KeepEmptyParts, part is ignored.
		//! The outermost brackets (if any) are allways ignored.
		QStringList splitBracketed(char sep = ',', char opening_bracket = '(', char closing_bracket = ')', char quote = '\0',
								   TrimBehavior trim_parts = QFString::TrimParts,
								   SplitBehavior keep_empty_parts = QString::SkipEmptyParts) const;
		//! The same like splitBracketed(',', '{', '}', '\'').
		QStringList splitVector() const {return splitBracketed(',', '{', '}', '\'', QFString::TrimParts, QString::KeepEmptyParts);}
		StringMap splitMap() const;
		QVariantMap splitMapRecursively(char quote = '\'') const;
		int indexOfOneOf(const QString &chars, char quote = '\0') const;
		//! pattern can contain any number of '*' standing for 'any text.'
		bool matchWild(const QString &pattern, Qt::CaseSensitivity cs = Qt::CaseSensitive);
		//!Check if string match a pattern and return list of all wild char replacements.
		/// FIXME: neni odladena, mozna nefunguje spravne.
		QStringList findWildParts(const QString& pattern, char wild_char = '*', Qt::CaseSensitivity cs = Qt::CaseSensitive) const;

		//operator bool() const {return !isEmpty();} // tohle zlobi pri scitani stringu
		bool operator!() const {return isEmpty();}
		/// jako QList::value()
		//! \a i can be negative, then position is counted from the end of string.
		QChar value(int ix) const;
		QChar operator[](int i) const {return value(i);}
		QCharRef operator[](int i);
		QFString& operator=(const QFString &s) {QString::operator=(s); return *this;}
		QFString& operator+=(const QString& s)
		{
			QString::operator+=(s);
			return *this;
		}
		const QFString operator+(const QString& s)
		{
			//return QFString(*this + s);
			QFString ret = *this;
			ret += s;
			return ret;
		}
		const QFString operator+(char c) {return operator+(QString(c));}
		const QFString operator+(const char* str) {return operator+(QString(str));}

		friend QFCORE_DECL_EXPORT const QFString operator+(const char* str, const QFString &s);
		friend QFCORE_DECL_EXPORT const QFString operator+(char c, const QFString &s);

		/// "N(3,2)" oddeluj tisice, 2 des. mista
		/// "N(0,2)" neoddeluj tisice, 2 des. mista
		/// "N(3)" oddeluj tisice, 0 des. mist bez des. tecky
		/// "N(3,0)" oddeluj tisice, 0 des. mist s des. teckou
		static QString number(double d, const QString &format);
		static QString decimalNumber(double d, int decimals);

		int toOBTime() const;
		static QFString fromOBTime(int sec);
	protected:
		static QString castkaSlovy_helper(int cislo, int tisice);
	public:
		static QString castkaSlovy(int castka);
};

//typedef QFLib::String QFString;

#endif // QFSTRING_H

