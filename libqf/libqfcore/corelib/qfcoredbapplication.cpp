
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfcoredbapplication.h"

#include <qflogcust.h>

QFCoreDbApplication::QFCoreDbApplication(int & argc, char ** argv)
	: QFCoreApplication(argc, argv)
{
}

QFCoreDbApplication::~QFCoreDbApplication()
{
}

QFCoreDbApplication * QFCoreDbApplication::instance()
{
	QFCoreDbApplication *a = qobject_cast<QFCoreDbApplication*>(QCoreApplication::instance());
	QF_ASSERT(a, "aplikace dosud neni inicializovana");
	return a;
}

