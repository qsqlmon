
//
// Author: Frantisek Vacek <$MAIL_ADDRESS>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qf.h"

#include <QMetaMethod>
#include <QPointer>

#include <math.h>

//#define QF_LOG_MODULE_DOMAIN_NAME "qf_cpp"
#include <qflogcust.h>

QString Qf::defaultDateFormatStr = QObject::tr("dd.MM.yyyy", "defaultDateFormat");
QString Qf::defaultTimeFormatStr = QObject::tr("hh:mm:ss", "defaultTimeFormat");
QString Qf::defaultDateTimeFormatStr = QObject::tr("dd.MM.yyyy hh:mm:ss", "defaultDateTimeFormat");

QObject* Qf::findParentOfType(QObject *pObject, const char *type_name, bool throw_exc) throw(QFException)
{
	qfTrash() << QF_FUNC_NAME << "type_name:" << type_name;
	QObject *o = pObject->parent();
	while(o) {
		qfTrash() << "checking parent:" << o->metaObject()->className();
		if(o->inherits(type_name)) break;
		o = o->parent();
	}
	if(!o && throw_exc) QF_EXCEPTION(QString("Can not find parent of type '%1'").arg(type_name));
	return o;
}

QString Qf::printable(const QVariant & v)
{
	QString ret;
	switch(v.type()) {
		case QVariant::String:
			ret = '\'' + v.toString() + '\'';
			break;
		case QVariant::Date:
			ret = v.toDate().toString(defaultDateFormat());
			break;
		case QVariant::Time:
			ret = v.toTime().toString();
			break;
		case QVariant::DateTime:
			ret = v.toDateTime().toString(defaultDateTimeFormat());
			break;
		default:
			ret = v.toString();
	}
	return ret;
}


double Qf::round(double d, int decimals)
{
	double e = ::pow(10, decimals);
	return (::round(d * e) / e);
}

bool Qf::isEqual(double d1, double d2, double epsilon)
{
	//const static double VERYSMALL = 1.0E-20;
	const static double EPSILON = 1.0E-8;
	double abs_diff = fabs(d1 - d2);
	if(abs_diff < epsilon) {
		return true;
	}
	double max_abs  = qMax(fabs(d1), fabs(d2));
	return (abs_diff / max_abs) < EPSILON;
}
/*
QString Qf::escapeJsonString(const QString &s)
{
	QString ret = s;
	//qfInfo().noSpace() << "unescaped: '" << ret << "'";
	ret.replace('\\', "\\\\").replace('"', "\\\"")
			.replace('\f', "\\f")
			.replace('\b', "\\b")
			.replace('\n', "\\n")
			.replace('\t', "\\t")
			.replace('\r', "\\r");
	//qfInfo().noSpace() << "escaped: '" << ret << "'";
	return ret;
}

QString Qf::unescapeJsonString(const QString &s)
{
	QString ret = s;
	ret.replace("\\\"", "\"")
			.replace("\\f", "\f")
			.replace("\\b", "\b")
			.replace("\\n", "\n")
			.replace("\\t", "\t")
			.replace("\\r", "\r")
			.replace("\\\\", "\\");
	QRegExp rx_uni("\\\\u([0-9a-f]{4})");
	while(rx_uni.indexIn(ret) >= 0) {
		//qfInfo().noSpace() << "cap(0): '" << rx_uni.cap(0) << "'";
		//qfInfo().noSpace() << "cap(1): '" << rx_uni.cap(1) << "'";
		int n = rx_uni.cap(1).toInt(NULL, 16);
		QChar c(n);
		ret.replace(rx_uni.cap(0), c);
	}
	return ret;
}

QString Qf::jsonToString(const QVariant & v)
{
	//qfLogFuncFrame();
	QString ret;
	if(v.type() == QVariant::Map) {
		QStringList sl;
		QVariantMap m = v.toMap();
		QMapIterator<QString, QVariant> i(m);
		while(i.hasNext()) {
			i.next();
			QString k = i.key();
			QVariant v = i.value();
			QString s = '"' + k + '"' + ":" + jsonToString(v);
			//qfTrash() << "\tadding to list:" << s;
			sl << s;
		}
		ret = '{' + sl.join(",") + '}';
	}
	else if(v.type() == QVariant::List || v.type() == QVariant::StringList) {
		QStringList sl;
		QVariantList l = v.toList();
		foreach(const QVariant &v1, l) {
			sl << jsonToString(v1);
		}
		ret = '[' + sl.join(",") + ']';
	}
	else if(v.type() == QVariant::String) {
		ret = '"' + Qf::escapeJsonString(v.toString()) + '"';
	}
	else {
		ret = v.toString();
		if(ret.isEmpty()) ret = "null";
	}
	//qfTrash() << "\treturn:" << ret;
	return ret;
}

static QStringList split_json(const QString &str)
{
	QStringList ret;
	QFString fs = str;
	while(!!fs) {
		int opens = 0;
		//int sep_pos = -1;
		QChar quote;
		QFString s = fs;
		int i;
		QChar prev_c;
		for(i=0; i<fs.len(); i++) {
			QChar c = s[i];
			if(prev_c != '\\') {
				if(quote.isNull()) {
					if(c == '"' || c == '\'') quote = c;
					else if(c == '{' || c == '[') {opens++;}
					else if(c == '}' || c == ']') opens--;
					if(opens == 0 && c == ',') {
						break;
					}
				}
				else {
					if(c == quote) quote = QChar();
				}
			}
			prev_c = c;
		}
		s = fs.slice(0, i).trim();
		if(!!s) ret << s;
		//qfInfo() << "fs pred:" << fs;
		//qfInfo() << "s:" << s;
		fs = fs.slice(i+1); /// skip separator
		//qfInfo() << "fs po:" << fs;
	}
	return ret;
}

QVariant Qf::stringToJson(const QString & str)
{
	qfLogFuncFrame() << str;
	QVariant ret;// = s.splitMapRecursively('"');
	QFString s = str.trimmed();
	if(s[0] == '{') {
		QStringList sl = split_json(s.slice(1, -1));
		QVariantMap vm;
		foreach(s, sl) {
			int ix = s.indexOf(':');
			if(ix > 0) {
				QFString key = s.slice(0, ix).trim();
				if(key[0] == '"') key = key.slice(1, -1);
				qfTrash() << "\t key:" << key;
				s = s.slice(ix+1).trim();
				qfTrash() << "\t val:" << s;
				QVariant v = stringToJson(s);
				vm[key]  = v;
			}
		}
		ret = vm;
	}
	else if(s[0] == '[') {
		/// array
		s = s.slice(1, -1);
		QStringList sl = split_json(s);
		QVariantList vl;
		foreach(s, sl) {
			QVariant v = stringToJson(s);
			//qfTrash() << "array part:" << s << "converted to:" << jsonToString(v);
			vl << v;
		}
		ret = vl;
	}
	else if(s[0] == '"') {
		ret = Qf::unescapeJsonString(s.slice(1, -1));
	}
	else {
		if(s == "true" || s == "false") ret = s.toBool();
		else if(s == "null") ret = QVariant();
		else do {
			bool ok;
			ret = s.toInt(&ok);
			if(ok) break;
			ret = s.toDouble(&ok);
			if(ok) break;
		} while(false);
	}
	qfTrash() << "\t RETURN:" << jsonToString(ret);
	return ret;
}
*/
void Qf::connectSlotsByName(QObject * signal_object, QObject * slot_object)
{
	if (!slot_object || !signal_object) return;
	
	const QMetaObject *mo = slot_object->metaObject();
	Q_ASSERT(mo);
	const QObjectList list = qFindChildren<QObject *>(signal_object, QString());
	for (int i = 0; i < mo->methodCount(); ++i) {
		const char *slot = mo->method(i).signature();
		Q_ASSERT(slot);
		if (slot[0] != 'o' || slot[1] != 'n' || slot[2] != '_') continue;
		
		bool foundIt = false;
		for(int j = 0; j < list.count(); ++j) {
			const QObject *co = list.at(j);
			QByteArray objName = co->objectName().toAscii();
			int len = objName.length();
			if (!len || qstrncmp(slot + 3, objName.data(), len) || slot[len+3] != '_') continue;
			
			const QMetaObject *smo = co->metaObject();
			int sigIndex = smo->indexOfMethod(slot + len + 4);
			if (sigIndex < 0) { // search for compatible signals
				int slotlen = qstrlen(slot + len + 4) - 1;
				for (int k = 0; k < co->metaObject()->methodCount(); ++k) {
					if (smo->method(k).methodType() != QMetaMethod::Signal) continue;

					if (!qstrncmp(smo->method(k).signature(), slot + len + 4, slotlen)) {
						sigIndex = k;
						break;
					}
				}
			}
			if (sigIndex < 0) continue;
			if (QMetaObject::connect(co, sigIndex, slot_object, i)) {
				foundIt = true;
				break;
			}
		}
		if (foundIt) {
            		/// we found our slot, now skip all overloads
			while (mo->method(i + 1).attributes() & QMetaMethod::Cloned) ++i;
		}
		else if (!(mo->method(i).attributes() & QMetaMethod::Cloned)) {
			/// zakomentovany kvuli inmonitoru
			//qWarning("Qf::connectSlotsByName: No matching signal for %s", slot);
		}
	}
}

		
QVariant Qf::retypeVariant(const QVariant _val, QVariant::Type type)
{
	//qfTrash().noSpace() << QF_FUNC_NAME << " '" << _val.toString() << "' type:" << _val.typeName() << " to type:" << QVariant::typeToName(type);
	/*
	if(type == QVariant::Date && _val.userType() == QMetaType::type("QFDate")) {
	return _val;
}
	*/
	if(_val.type() == type) return _val;
	//if(!_val.isValid()) return _val;
	if(!_val.isValid() || _val.isNull()) return QVariant(type);
	QFString val = _val.toString();
	switch(type) {
		case QVariant::Bool:
			return QVariant(val.toBool());
		case QVariant::LongLong:
			if(_val.type() == QVariant::Double) return _val.toLongLong();
			return QVariant(val.toLongLong());
		case QVariant::ULongLong:
			if(_val.type() == QVariant::Double) return _val.toULongLong();
			return QVariant(val.toULongLong());
		case QVariant::Int:
			if(_val.type() == QVariant::Bool) return ((_val.toBool())? 1: 0);
			if(_val.type() == QVariant::Double) return _val.toInt();
			return QVariant(val.toInt());
		case QVariant::UInt:
			if(_val.type() == QVariant::Double) return _val.toUInt();
			return QVariant(val.toUInt());
		case QVariant::Double:
			return QVariant(val.toDouble());
		case QVariant::Date:
			if(val.isEmpty()) {
				return QVariant(QDate());
			}
			else	{
				if(_val.type() == QVariant::DateTime) return _val.toDate();
				return val.toDate();
				//if(QLocale().name() == "cs_CZ") return QVariant(QDate::fromString(val, "d.M.yyyy") );
				//return QVariant(QDate::fromString(val, Qt::ISODate) );
			}
		case QVariant::Time:
			if (val.isEmpty()) {
				return QVariant(QTime());
			}
			else {
				if(_val.type() == QVariant::DateTime) return _val.toTime();
				return QVariant(QTime::fromString(val, Qt::ISODate));
			}
		case QVariant::DateTime:
			//qfInfo() << "val:" << val;
			if (val.isEmpty()) {
				return QVariant(QDateTime());
			}
			if (val.length() == 14) {
            			/// TIMESTAMPS with format yyyyMMddhhmmss
				val.insert(4, QLatin1Char('-')).insert(7, QLatin1Char('-')).insert(10, QLatin1Char('T')).insert(13, QLatin1Char(':')).insert(16, QLatin1Char(':'));
			}
			return QVariant(QDateTime::fromString(val, Qt::ISODate));
			/*
			case QVariant::ByteArray: {

			QByteArray ba;
			if (d->preparedQuerys) {
			ba = QByteArray(f.outField, f.bufLength);
	} else {
			unsigned long* fl = mysql_fetch_lengths(d->result);
			ba = QByteArray(d->row[field], fl[field]);
	}
			return QVariant(ba);
	}
			*/
		case QVariant::String:
		default:
			return QVariant(val);
	}
	qfWarning() << "retypeVariant(): unknown data type" << QVariant::typeToName(type);
	return QVariant();
}

void Qf::deleteChildObjects(QObject * parent_object)
{
	if(!parent_object) return;
	QObject destr_o;
	foreach(QObject *o, parent_object->children()) {
		o->setParent(&destr_o);
	}
}
