
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFFLAGS_H
#define QFFLAGS_H

#include <QFlags>

#include <qfcoreglobal.h>

//! Extends QFlags by operators >>(...) and <<(...)
template<typename Enum>
class QFCORE_DECL_EXPORT QFFlags : public QFlags<Enum>
{
	public:
		QFFlags() : QFlags<Enum>() {}
		//QFFlags(uint mask) : QFlags<Enum>() {*this = mask;}
		QFFlags(const QFlags<Enum> & other) : QFlags<Enum>(other) {}

		bool contains(Enum f) const {return ((int)(*this)) & f;}
		QFFlags& operator<<(Enum f) {QFlags<Enum>::operator|=(f); return *this;}
		QFFlags& operator>>(Enum f) {QFlags<Enum>::operator&=(~f); return *this;}
		//QFFlags& operator>>(int _i) {QFlags<Enum>::operator&=(~_i); return *this;}
};
   
#endif // QFFLAGS_H

