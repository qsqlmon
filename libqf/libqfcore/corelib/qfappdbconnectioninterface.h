
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFAPPDBCONNECTIONINTERFACE_H
#define QFAPPDBCONNECTIONINTERFACE_H

#include <qfcoreglobal.h>
#include <qfsqlquery.h>

class QFSqlConnection;

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFAppDbConnectionInterface 
{
	protected:
		QFSqlConnection *f_connection;
	public:
		virtual QFSqlConnection& connection();
		QFSqlQuery execSql(const QString &query) throw(QFException);

		/// protoze se connection() vraci referenci a nekdy potrebuju vedet, ze se to nepodarilo a nevrhnout vyjjimku, vraci se toto connection
		static QFSqlConnection& dummyConnection();
	public:
		QFAppDbConnectionInterface();
		virtual ~QFAppDbConnectionInterface();
};

#endif // QFAPPDBCONNECTIONINTERFACE_H

