
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFAPPCONFIGINTERFACE_H
#define QFAPPCONFIGINTERFACE_H

#include <qfcoreglobal.h>
#include <qfxmlconfig.h>

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFAppConfigInterface 
{
	protected:
		QFXmlConfig f_config;
	public:
		virtual QFXmlConfig* config();
		/// XMLConfig DOES NOT take ownership of \a loader . Nemuze, XmlConfig je implicitne sdilen a kdo by to mel vymazat?
		void setConfigLoader(QFXmlConfigLoader *loader);

		//! path muze zacinat na appconfig://, globalconfig://, localconfig://, userconfig://
		//! pokud neobsahuje oddelovac ://, prida se pred cestu appconfig://
		virtual QVariant configValue(const QString &path, const QVariant &default_value = QVariant());
		virtual void setConfigValue(const QString &path, const QVariant &value);
	public:
		QFAppConfigInterface();
		virtual ~QFAppConfigInterface() {}
};

#endif // QFAPPCONFIGINTERFACE_H

