
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsearchdirs.h"

#include <qffileutils.h>

#include <QDir>

#include <qflogcust.h>

void QFSearchDirs::setDirs(const QString & s)
{
	QStringList sl = s.split("::", QString::SkipEmptyParts);
	dirsRef() = sl;
}

void QFSearchDirs::appendDir(const QString & path)
{
	int ix = dirs().indexOf(path);
	if(ix < 0) dirsRef() << path;
}

void QFSearchDirs::prependDir(const QString & path)
{
	QStringList &sl = dirsRef();
	if(!sl.isEmpty() && sl[0] == path) return;
	sl.prepend(path);
}

QString QFSearchDirs::findFile(const QString & file_name) const
{
	qfLogFuncFrame() << file_name;
	QString ret;
	if(QDir::isAbsolutePath(file_name)) {
		if(QFile::exists(file_name)) ret = file_name;
	}
	else {
		if(dirs().isEmpty()) {
/*			if(QFile::exists(file_name)) {
				ret = file_name;
				break;
			}*/
		}
		else {
			foreach(QString dir, dirs()) {
				dir = QFFileUtils::joinPath(dir, file_name);
				qfTrash() << "\t trying:" << dir;
				if(QFile::exists(dir)) {
					qfTrash() << "\t\t SUCCES";
					ret = dir;
					break;
				}
			}
		}
	}
	/*
	if(!ret.isEmpty() && versionControl() == GetLastVersion) {
		QString dir_path = QFFileUtils::dir(ret);
		QString fn = QFFileUtils::file(ret);
		QDir dir(dir_path, fn + ".*", QDir::SortFlags( QDir::Name | QDir::IgnoreCase ), QDir::Readable | QDir::Files);
		QStringList sl = dir.entryList();
		if(sl.count()) {
			int max = 0;
			foreach(QString s, sl) {
				int i = QFFileUtils::extension(s).toInt();
				max = qMax(max, i);
			}
			if(max < 0) ret = QString("%1.%2").arg(ret).arg(max);
		}
	}
	*/
	ret = QDir::fromNativeSeparators(ret);
	return ret;
}

