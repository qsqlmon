
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfappconfiginterface.h"

#include <qfassert.h>

#include <qflogcust.h>

QFAppConfigInterface::QFAppConfigInterface() 
{
}

QFXmlConfig * QFAppConfigInterface::config()
{
	if(f_config.templateDocument().isEmpty()) {
		qfLogFuncFrame();
		f_config.load();
		QF_ASSERT(!f_config.templateDocument().isEmpty(), ("Configuration not found."));
	}
	return &f_config;
}

void QFAppConfigInterface::setConfigLoader(QFXmlConfigLoader * loader)
{
	f_config.setConfigLoader(loader);
}

QVariant QFAppConfigInterface::configValue(const QString & path, const QVariant & default_value)
{
	qfLogFuncFrame() << "path:" << path;
	QVariant ret;
	ret = config()->value(path, default_value);
	qfTrash() << "\t return:" << ret.toString();
	return ret;
}

void QFAppConfigInterface::setConfigValue(const QString & path, const QVariant & value)
{
	qfLogFuncFrame() << "path:" << path;
	config()->setValue(path, value);
}
/*
void QFAppConfigInterface::redirectLog()
{
	bool log_to_file = config()->value("/log", "0").toBool();
	bool redirected = false;
	if(log_to_file) {
		QString fn = config()->value("/log/file", "err.log").toString();
		FILE *f = fopen(qPrintable(fn), "wb");
		if(f) {
			redirected = true;
			QFLog::redirectDefaultLogFile(f);
		}
	}
	if(!redirected) QFLog::redirectDefaultLogFile();
}
*/
