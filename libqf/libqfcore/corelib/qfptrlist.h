//
// C++ Interface: qflist
//
// Description: 
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef QFPTRLIST_H
#define QFPTRLIST_H

#include <QList>
#include <QMap>
#include <QSharedDataPointer>

#include <qf.h>
#include <qfcoreglobal.h>

/**
@author Fanda Vacek
 */
template <class T>
class QFPtrList : public QList<T*>
{
	private:
		/// this is the hack
		/// using d I can recognize, that lists refcount is 1
		QSharedDataPointer<QSharedData> d;
	public:
		QFPtrList();
		~QFPtrList();

		void clear();
};

template <class T>
QFPtrList<T>::QFPtrList()
	: QList<T*>()
{
	d = new QSharedData();
}

template <class T>
QFPtrList<T>::~QFPtrList()
{
	int refcnt = ((const QFPtrList<T>*)this)->d->ref;
	if(refcnt == 1) this->clear();
}

template <class T>
void QFPtrList<T>::clear()
{
	//qfTrash() << QF_FUNC_NAME;
	for(int i=0; i<this->count(); i++) {
		//qfTrash() << i;
		SAFE_DELETE((*this)[i]);
	}
	QList<T*>::clear();
}

/**
@author Fanda Vacek
 */
template <class K, class V>
class QFPtrMap : public QMap<K, V*>
{
	private:
		QSharedDataPointer<QSharedData> d;
	public:
		QFPtrMap();
		~QFPtrMap();

		void clear();
};

template <class K, class V>
QFPtrMap<K, V>::QFPtrMap()
	: QMap<K, V*>()
{
	d = new QSharedData();
}

template <class K, class V>
QFPtrMap<K, V>::~QFPtrMap()
{
	//qfTrash() << QF_FUNC_NAME;
	int refcnt = ((const QFPtrMap<K, V>*)this)->d->ref;
	//qfTrash() << "\trefcnt:" << refcnt;
	if(refcnt == 1) this->clear();
}

template <class K, class V>
		void QFPtrMap<K, V>::clear()
{
	//foreach(V *i, (QMap<K, V*>::values())) SAFE_DELETE(i);
	QList<V*> l = this->values();
	for(int i=0; i<l.count(); i++) {
		SAFE_DELETE(l[i]);
	}
	QMap<K, V*>::clear();
}

#endif
