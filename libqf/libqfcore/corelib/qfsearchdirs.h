
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSEARCHDIRS_H
#define QFSEARCHDIRS_H

#include <qfcoreglobal.h>

#include <QStringList>

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFSearchDirs 
{
	public:
		//enum VersionControl {IgnoreVersioning=0, GetLastVersion};
	protected:
		QStringList f_dirList;
		//VersionControl f_versionControl;
	public:
		QStringList dirs() const {return const_cast<QFSearchDirs*>(this)->dirsRef();}
		QStringList& dirsRef() {return f_dirList;} 
		void setDirs(const QStringList &sl) {dirsRef() = sl;}
		/// oddelovac je kvuli windows "::"
		void setDirs(const QString &s);
		//! Pokud uz tam \a path je, neprida se, stejne by na ni nedosla rada a umoznuje to volat funkci opakovane bez nasledku.
		void appendDir(const QString &path);
		//! Pokud uz tam \a path je na prvnim miste, neprida se, bylo by to zbytecny a umoznuje to volat funkci opakovane bez nasledku.
		void prependDir(const QString &path);
		/// pokud je file_name absolutni cesta, pouze overi jeho existenci.
		/// pokud je relativni, prida pred nej vsechny searchDirs() a vrati prvni existujici soubor nebo prazdny string.
		/// @param get_last_version pokud najde v adresari, kde je soubor neco jako jmeno.pripona.001, vrati soubor s nejvyssim cislem coby posledni verzi
		QString findFile(const QString &file_name) const;
		
		//VersionControl versionControl() const {return f_versionControl;}
	public:
		QFSearchDirs(const QStringList &dirs = QStringList()) : f_dirList(dirs) {}
		//QFSearchDirs(const QStringList &dirs = QStringList(), VersionControl version_control = IgnoreVersioning) : f_dirList(dirs), f_versionControl(version_control) {}
		//virtual ~QFSearchDir();
};

#endif // QFSEARCHDIRS_H

