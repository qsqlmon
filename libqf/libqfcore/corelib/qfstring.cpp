#include "qfstring.h"

#include <qf.h>

#include <QTime>
#include <QStringList>
#include <QVariant>

#include <qflogcust.h>

//=============================================================
//                    QFString
//=============================================================
QFString::QFString(const QVariant &v)
	: QString()
{
	static QString s = "%1";
	if(v.isValid()) 	switch(v.type()) {
		case QVariant::Char:
			*this = s.arg(v.toChar());
			break;
		case QVariant::Int:
			*this = s.arg(v.toInt());
			break;
		case QVariant::UInt:
			*this = s.arg(v.toUInt());
			break;
		case QVariant::LongLong:
			*this = s.arg(v.toLongLong());
			break;
		case QVariant::ULongLong:
			*this = s.arg(v.toULongLong());
			break;
		case QVariant::Double:
			*this = s.arg(v.toDouble());
			break;
		case QVariant::String:
			*this = v.toString();
			break;
		case QVariant::Date:
			*this = s.arg(v.toDate().toString(Qf::defaultDateFormat()));
			break;
		case QVariant::Time:
			*this = s.arg(v.toTime().toString(Qt::TextDate));
			break;
		default:
			qfError() << "Unsupported variant type" << v.typeName();
			*this = s.arg("UNSUPPORTED");
			break;
	}
}

QFString QFString::ltrim() const
{
    QFString fs = *this;
    int i;
    for(i=0; i<fs.len() && fs[i].isSpace(); i++) ;
    fs = fs.slice(i);
    return fs;
}

//-------------------------------------------------------------
QFString QFString::rtrim() const
{
    QFString fs = *this;
    int i;
    for(i=fs.len(); i>0 && fs[i-1].isSpace(); i--) ;
    fs = fs.slice(0, i);
    return fs;
}

//-------------------------------------------------------------
QFString QFString::slice(int start, int end) const
{
	//qfTrash() << QF_FUNC_NAME;
    int l = len();
    if(start < 0) start += l;
    if(end < 0) end += l;
    if(start < 0) start = 0;
    if(end < 0) end = 0;
    if(end > l) end = l;
    if(start > end) start = end;
	//qfTrash() << "\tstart:" << start << "end:" << end;
	//qfTrash() << "\tthis:" << *this;
	return mid(start, end - start);
	//qfTrash() << "\tfs:" << fs;
}

const char* QFString::str() const
{
	static QByteArray ba;
	ba = toLocal8Bit();
	return ba.data();
	// tohle jsem nasel ve zdrojacich k lupdate (main.c)
	//fprintf( stderr, "Updating '%s'...\n", (*t).toLatin1().data() );
}

//-------------------------------------------------------------
QChar QFString::value(int ix) const
{
	if(ix < 0) ix += len();
	if(ix < 0 || ix >= len()) return QChar();
	return QString::at(ix);
}
/*
QChar QFString::operator[](int i) const
{
	return value(i);
}
*/
QCharRef QFString::operator[](int i)
{
	static QString null_char = " ";
	if(i < 0) i += len();
	if(i < 0 || i >= len()) {null_char[0] = QChar(); return null_char[0];}
	return QString::operator[](i);
}

int QFString::indexOfMatchingBracket(char opening_bracket, char closing_bracket, char quote) const
{
	int opens = 0;
	bool was_open = false;
	bool in_quotes = false;
	QFString s = *this;
	int i;
	QChar prev_c;
	for(i=0; i<len(); i++) {
		QChar c = s[i];
		if(prev_c != '\\') {
			if(quote && c == quote) {in_quotes = !in_quotes;}
			if(!in_quotes) {
				if(c == opening_bracket) {opens++; was_open = true;}
				else if(c == closing_bracket) opens--;
				if(was_open && opens==0) {
				//qfTrash() << "BINKO" << i << "len:" << len();
					return i;
				}
			}
		}
		prev_c = c;
	}
	if(opens != 0) return -1;
	return i;
}

QStringList QFString::splitAndTrim(QChar sep, QChar quote, TrimBehavior trim_parts, SplitBehavior keep_empty_parts) const
{
	QStringList ret;
	QFString fs = *this, s;
	bool first_scan = true;
	while(!!fs) {
		if(first_scan) first_scan = false;
		else fs = fs.slice(1); // remove separator
		int ix = fs.pos(sep, quote);
		if(ix >= 0) {
			s = fs.slice(0, ix);
			fs = fs.slice(ix);
		}
		else {
			s = fs;
			fs = QString();
		}
		if(trim_parts == TrimParts) {
			s = s.trim();
			if(s[0] == quote && s[-1] == quote) s = s.slice(1, -1);
		}
		if(!!s) ret.append(s);
		else if(keep_empty_parts == KeepEmptyParts) ret.append(s);
	}
	return ret;
}

int QFString::indexOfOneOf(const QString &chars, char quote) const
{
	QFString s = *this;
	bool in_quotes = false;
	for(int i=0; i<len(); i++) {
		if(quote && s[i] == quote) {in_quotes = !in_quotes;}
		if(!in_quotes) {
			for(int j=0; j<chars.length(); j++) {
				if(s[i] == chars[j]) return i;
			}
		}
	}
	return -1;
}

int QFString::pos(QChar what, QChar quote) const
{
	QFString s = *this;
	bool in_quotes = false;
	for(int i=0; i<len(); i++) {
		if(!quote.isNull() && s[i] == quote) {in_quotes = !in_quotes;}
		if(!in_quotes) {
			if(s[i] == what) return i;
		}
	}
	return -1;
}

int QFString::indexOfWord(const QString & word, Qt::CaseSensitivity cs) const
{
	qfLogFuncFrame() << "looking for " << word << "in" << *this;
	QString s = "\\b%1\\b";
	QRegExp rx(s.arg(word), cs);
	int ix = rx.indexIn(*this);
	qfTrash() << "\t return:" << ix;
	return ix;
}

QStringList QFString::splitBracketed(char sep, char opening_bracket, char closing_bracket, char quote, TrimBehavior trim_parts, SplitBehavior keep_empty_parts) const
{
	QStringList ret;
	QFString fs = this->trimmed(), s;
	//qfInfo() << "split bracketed:" << fs << "fs[0]" << fs[0];
	if(fs[0] == opening_bracket) {
		int ix = fs.indexOfMatchingBracket(opening_bracket, closing_bracket, quote);
		//qfInfo().noSpace() << "\t ix: " << ix << " len: " << fs.len() << "'" << fs << "'";
		if(ix == fs.len() - 1) fs = fs.slice(1, -1);
	}
	//qfInfo() << "\t fs bez zavorek:" << fs;
	//QString separators = QString(sep) + opening_bracket;
	while(!!fs) {
		int opens = 0;
		//int sep_pos = -1;
		bool in_quotes = false;
		QFString s = fs;
		int i;
		QChar prev_c;
		for(i=0; i<fs.len(); i++) {
			QChar c = s[i];
			if(prev_c != '\\') {
				if(quote && c == quote) {in_quotes = !in_quotes;}
				if(!in_quotes) {
					if(c == opening_bracket) {opens++;}
					else if(c == closing_bracket) opens--;
					if(opens == 0 && c == sep) {
						break;
					}
				}
			}
			prev_c = c;
		}
		s = fs.slice(0, i);
		//qfInfo() << "fs pred:" << fs;
		//qfInfo() << "s:" << s;
		fs = fs.slice(i+1); /// skip separator
		//qfInfo() << "fs po:" << fs;
		if(trim_parts == TrimParts) {
			s = s.trim();
			if(s[0] == opening_bracket && s[-1] == closing_bracket) {
				s = s.slice(1, -1);
				s = s.trim();
			}
		}
		if(keep_empty_parts != KeepEmptyParts) {
			if(!!s) ret << s;
		}
		else {
			ret << s;
		}
	}
	return ret;
}

QFString::StringMap QFString::splitMap() const
{
	StringMap ret;
	QStringList sl = splitVector();
	foreach(QString s, sl) {
		int ix = s.indexOf(':');
		if(ix > 0) ret[s.mid(0, ix).trimmed()]  = s.mid(ix+1).trimmed();
		else ret[s.trimmed()];
	}
	return ret;
}

static QVariantMap splitMapRecursively_helper(const QFString &str, char quote)
{
	QVariantMap ret;
	QStringList sl = str.splitBracketed(',', '{', '}', quote, QFString::TrimParts, QString::SkipEmptyParts);
	foreach(QFString s, sl) {
		int ix = s.indexOf(':');
		if(ix > 0) {
			QString key = s.slice(0, ix).trim();
			s = s.slice(ix+1).trim();
			if(s[0] == '{') ret[key] = splitMapRecursively_helper(s, quote);
			else ret[key]  = s;
		}
		else ret[s.trim()];
	}
	return ret;
}

QVariantMap QFString::splitMapRecursively(char quote) const
{
	return splitMapRecursively_helper(*this, quote);
}
/*
static bool index_of_helper(const QFString &pattern, const QFString &s)
{
	if(!s) return -1;
	if(!pattern) return 0;
	for(int i=0; i<s.length(); i++) {
		bool found = true;
		int j;
		for(j=0; j<pattern.length(); j++) {
			if((i + j) >= s.length()) {found = false; break;}
			QChar cp = pattern[j];
			if(cp != '?') {
				if(cp != s[i + j]) {found = false; break;}
			}
		}
		if(found) return i;
	}
	return -1;
}
*/
bool QFString::matchWild(const QString &_pattern, Qt::CaseSensitivity cs)
{
	//qfTrash() << QF_FUNC_NAME << _pattern << "vs." << *this << this->lastIndexOf(QString());
	QStringList sl;
	QFString fs = *this;
	QFString patt = _pattern;
	if(patt[0] == '*') sl << QString();
	sl =  sl + patt.split('*', QString::SkipEmptyParts);
	if(patt[-1] == '*') sl << QString();
	int pos=0;
	int cnt = 0;
	foreach(QFString s, sl) {
		int ix;
		if(!s) {
			if(cnt++ == 0) ix = 0;
			else ix = fs.length();
		}
		else {
			if(cnt++ == 0) ix = fs.indexOf(s, 0, cs);
			else ix = fs.lastIndexOf(s, -1, cs);
		}
		//qfTrash().noSpace() << "\tpos:" << pos << " fs: '" << fs << "' s: '" << s << "' ix: " << ix << " last index: " << fs.lastIndexOf(s) << " " << cnt;
		if(ix < pos) return false;
		pos = ix + s.len();
	}
	return true;
}

QStringList QFString::findWildParts(const QString& _pattern, char wild_char, Qt::CaseSensitivity cs) const
{
	QStringList ret, sl;
	QFString fs = *this, patt = _pattern;
	if(patt[0] == '*') sl << QString();
	sl = patt.split(wild_char, QString::SkipEmptyParts);
	if(patt[-1] == '*') sl << QString();
	int pos=0;
	int cnt = 0;
	foreach(QFString s, sl) {
		int ix;
		if(cnt++ == 0) ix = fs.indexOf(s, 0, cs);
		else ix = fs.lastIndexOf(s, -1, cs);
		if(ix < 0) return QStringList();
		ret.append(fs.slice(pos, ix));
		pos += ix + s.len();
	}
	return ret;
}

bool QFString::toBool() const
{
	static const QString false_str = "false";
	QString s = (*this).trim();
	if(s.isEmpty()) return false;
	if(s.length() == 1) {
		if(s[0] == '0') return false;
		else if(s[0] == 'n' || s[0] == 'N' || s[0] == 'f' || s[0] == 'F') return false;
		return true;
	}
	else {
		if(s.compare(false_str, Qt::CaseInsensitive) == 0) return false;
		bool b;
		if(s.toInt(&b) == 0) if(b) return false;
	}
	return true;
}

QDate QFString::toDate() const
{
	QDate ret;
	QStringList sl = split(QRegExp("[\\.\\-/]"));
	//foreach(QString s, sl) qfInfo() << "\t->" << s;
	if(sl.count()) {
		int dd[] = {0,0,0}; /// Y,M,D
		QString s = sl[0].trimmed();
		int d = s.toInt();
		if(d > 99) {
			/// ISO date
			dd[0] = d;
			for(int i=1; i<3 && i<sl.count(); i++) {dd[i] = sl[i].toInt();}
		}
		else {
			/// ocekava dd.mm.[yyyy]
			for(int i=0; i<3 && i<sl.count(); i++) {dd[2-i] = sl[i].toInt();}
			if(dd[0] == 0) dd[0] = QDate::currentDate().year();
			else if(dd[0] < 100) {
				if(dd[0] <= 50) dd[0] = 2000 + dd[0];
				else dd[0] = 1900 + dd[0];
			}
			if(dd[1] == 0) dd[1] = QDate::currentDate().month();
			//for(int i=0; i<3; i++) qfInfo() << "\t->" << dd[i];
		}
		ret = QDate(dd[0], dd[1], dd[2]);
	}
	return ret;
}

QTime QFString::toTime() const
{
	QTime ret;
	QStringList sl = split(':');
	//foreach(QString s, sl) qfInfo() << "\t->" << s;
	if(sl.count()) {
		int dd[] = {0,0,0,0}; /// h,m,s,ms

		int ix = sl.last().indexOf('.');
		if(ix >= 0) {
			QFString s = sl.last();
			sl[sl.count()-1] = s.slice(0, ix);
			s = s.slice(ix + 1) + "00";
			sl << s.slice(0, 3);
		}
		else {
			if(sl.count() == 1) {
				/// hodiny
				sl << QString() << QString();
			}
			else if(sl.count() == 2) {
				/// hodiny:minuty
				sl << QString();
			}
			sl << QString();
		}
		int ii = 3;
		for(int i=sl.count()-1; i>=0 && ii>=0; i--, ii--) {
			dd[ii] = sl[i].toInt();
		}
		if(dd[2] > 59) {dd[1] += dd[2] / 60; dd[2] = dd[2] % 60;}
		if(dd[1] > 59) {dd[0] += dd[1] / 60; dd[1] = dd[1] % 60;}
		if(dd[0] > 23) {dd[0] = dd[0] % 24;}
		ret = QTime(dd[0], dd[1], dd[2], dd[3]);
	}
	return ret;
}

QFString QFString::qfarg(const QVariant &v) const
{
	return QString::arg(QFString(v));
}
//--------------- friends ----------------------------
const QFString operator+(char c, const QFString &s)
{
    QFString ret = c;
    ret += s;
    return ret;
}
const QFString operator+(const char* str, const QFString &s)
{
    QFString ret = str;
    ret += s;
    return ret;
}

//-------------------------------------------------------------

QString QFString::number(double d, const QString &_format)
{
	QFString format = _format;
	QString ret;
	if(format[0] == 'N') {
		bool put_dec_point = true;
		bool negative = (d < 0);
		if(negative) d = -d;
		format = format.slice(2, -1).trim();
		int pos = format.indexOf(',');
		int pokolika, desmist;
		if(pos >= 0) {
			pokolika = format.slice(0, pos).toInt();
			desmist = format.slice(pos + 1).toInt();
		}
		else {
			pokolika = format.toInt();
			desmist = 0;
			put_dec_point = false;
		}
		QString cela_cast = "0";//QString::number((int)d);
		QFString des_cast = QString::number(d, 'f', desmist);
		int dotpos = des_cast.indexOf('.');
		if(dotpos < 0) {
			cela_cast = des_cast;
			des_cast = QString();
		}
		else {
			cela_cast = des_cast.slice(0, dotpos);
			des_cast = des_cast.slice(dotpos+1);
		}
		des_cast = des_cast.leftJustified(desmist, '0', true);
		//qfInfo() << d << cela_cast << "." << des_cast;

		/// cela cast
		int len = cela_cast.length();
		if(pokolika > 0) {
			for(int i=0; i<len; i++) {
				if(i % pokolika == 0 && i > 0) ret.prepend(' ');
				ret.prepend(cela_cast[len - i - 1]);
			}
		}
		else {
			ret = cela_cast;
		}
		if(desmist > 0) ret += "." + des_cast;
		else if(desmist == 0 && put_dec_point) ret += '.';
		if(negative) ret.prepend('-');
		//qfInfo() << "ret:" << ret;
	}
	else ret = QString::number(d);
	return ret;
}

QString QFString::decimalNumber(double d, int decimals)
{
	qfLogFuncFrame(); 
	QFString ret;
	if(decimals < 0) ret = QString::number(d);
	else {
		//d = Qf::round(d, decimals);
		/// f je dulezity, u 
		ret = QString::number(d, 'f', decimals);
		qfTrash() << "\t number():" << ret;
		int dot_ix = ret.indexOf('.');
		if(dot_ix < 0) {
			ret += '.' + QString().fill('0', decimals);
		}
		else {
			int des_len = dot_ix + decimals + 1;
			ret = ret.slice(0, des_len);
			if(ret.length() < des_len) ret = ret.leftJustified(des_len, '0');
		}
	}
	qfTrash() << "\t return:" << ret;
	return ret;
}

QString QFString::castkaSlovy_helper(int cislo, int tisice)
{
	//qfInfo() << cislo << tisice;
	static QMap<int, QString> jednotky;
	static QMap<int, QString> desitky;
	static QMap<int, QString> stovky;
	if(jednotky.isEmpty()) {
		jednotky[0] = QString();
		jednotky[1] = tr("jedna");
		jednotky[1001] = tr("jeden");
		jednotky[1000001] = tr("jeden");
		jednotky[2] = tr("dva");
		jednotky[3] = tr("tri");
		jednotky[4] = tr("ctyri");
		jednotky[5] = tr("pet");
		jednotky[6] = tr("sest");
		jednotky[7] = tr("sedm");
		jednotky[8] = tr("osm");
		jednotky[9] = tr("devet");
		
		desitky[0] = QString();
		desitky[1] = tr("deset");
		desitky[2] = tr("dvacet");
		desitky[3] = tr("tricet");
		desitky[4] = tr("ctyricet");
		desitky[5] = tr("padesat");
		desitky[6] = tr("sedesat");
		desitky[7] = tr("sedmdesat");
		desitky[8] = tr("osmdesat");
		desitky[9] = tr("devadesat");
		desitky[10] = tr("deset");
		desitky[11] = tr("jedenact");
		desitky[12] = tr("dvanact");
		desitky[13] = tr("trinact");
		desitky[14] = tr("ctrnact");
		desitky[15] = tr("patnact");
		desitky[16] = tr("sestnact");
		desitky[17] = tr("sedmnact");
		desitky[18] = tr("osmnact");
		desitky[19] = tr("devatnact");
			
		stovky[0] = QString();
		stovky[1] = tr("sto");
		stovky[2] = tr("dveste");
		stovky[3] = tr("trista");
		stovky[4] = tr("ctyrista");
		stovky[5] = tr("petset");
		stovky[6] = tr("sestset");
		stovky[7] = tr("sedmset");
		stovky[8] = tr("osmset");
		stovky[9] = tr("devetset");
	}
	QString ret;
	int deset_az_20 = cislo % 100;
	if(cislo == 1) ret = jednotky.value((cislo%10) + tisice);
	else if(deset_az_20 > 10 && deset_az_20 < 20) {
		ret = stovky.value(cislo/100) + desitky.value(deset_az_20);
	}
	else ret = stovky.value(cislo/100) + desitky.value((cislo%100)/10) + jednotky.value((cislo%10));
	return ret;
}

QString QFString::castkaSlovy(int castka)
{
	QString ret;
	if(castka == 0) return tr("nula");
	/// miliony
	int n = castka / 1000000;
	if(n == 0);
	else if(n == 1) ret = castkaSlovy_helper(n, 1000000) + tr("milion");
	else if(n < 5) ret = castkaSlovy_helper(n, 1000000) + tr("miliony");
	else ret = castkaSlovy_helper(n, 1000000) + tr("milionu");
	/// tisice
	n = (castka % 1000000) / 1000;
	if(n == 0);
	else if(n == 1) ret += castkaSlovy_helper(n, 1000) + tr("tisic");
	else if(n < 5) ret += castkaSlovy_helper(n, 1000) + tr("tisice");
	else ret += castkaSlovy_helper(n, 1000) + tr("tisic");
	/// jednotky
	n = (castka % 1000);
	if(n == 0);
	else if(n == 1) ret += castkaSlovy_helper(n, 0);
	else if(n < 5) ret += castkaSlovy_helper(n, 0);
	else ret += castkaSlovy_helper(n, 0);
	return ret;
}

int QFString::toOBTime() const
{
	QStringList sl = splitAndTrim(':');
	int hod = sl.value(0).toInt();
	int min = sl.value(1).toInt();
	int sec = sl.value(2).toInt();
	return sec + 60 * min + 3600 * hod;
}

QFString QFString::fromOBTime(int sec)
{
	QString fmt = "%1:%2:%3";
	return fmt
	.arg((sec / (60*60)))
	.arg((sec % (60*60) / 60), 2, 10, QChar('0'))
	.arg((sec % 60), 2, 10, QChar('0'));
}
