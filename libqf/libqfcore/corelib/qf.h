
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QF_H
#define QF_H

#include <qfcoreglobal.h>
#include <qfexception.h>

#include <QObject>
#include <QMap>
#include <QSet>

#include <stdint.h>

typedef unsigned char byte;
//typedef byte *pbyte;
typedef unsigned short word;
typedef QMap<QString, QString> QFStringMap;
typedef QMapIterator<QString, QString> QFStringMapIterator;
typedef QSet<QString> QFStringSet;

#define QF_SAFE_DELETE(p) if(p) {delete p; p = 0;}
#define SAFE_DELETE(p) QF_SAFE_DELETE(p)
#define TR(str) QObject::tr(str)

/*
#ifdef __GNUC__
	#define QF_UNUSED  __attribute__((unused))
#else   // !__GNUC__
	#define QF_UNUSED
#endif

//#define USE_QFLIB_NAMESPACE
// bohuzel, MOC s tim pak ma moc problemu
#ifdef USE_QFLIB_NAMESPACE
    #define QFLIB_BEGIN  namespace QFLib {
    #define QFLIB_END    }
#else
    #define QFLIB_BEGIN
    #define QFLIB_END
	//#define QF QF
#endif
*/
//class QFException;
//! Tady chybi dokumentace.
class QFCORE_DECL_EXPORT Qf
{
	public:
		enum {ThrowExc = 1};
		/// HeaderSectionInitialSizeRole
		enum ItemDataRole {ColumnSqlIdRole = Qt::UserRole+1,
			AddColumnToBenListRole, HeaderSectionInitialSizeRole,
			FieldTypeRole, FieldIsNullableRole, FieldNativeTypeRole,
			RawValueRole, ValueIsNullRole, FirstUnusedRole
		};

		typedef QMap<QString, QVariant> OptionMap;

		static QString defaultDateFormatStr;
		static QString defaultTimeFormatStr;
		static QString defaultDateTimeFormatStr;
		static QString defaultDateFormat() {return defaultDateFormatStr;}
		static QString defaultTimeFormat() {return defaultTimeFormatStr;}
		static QString defaultDateTimeFormat() {return defaultDateTimeFormatStr;}

		/// u stringu prida apostrofy, datum naformatuje yyyy-mm-dd
		static QString printable(const QVariant &v);

		/// zaokrouhli d na decimals desetinych mist
		/// decimals muze byt i zaporne cislo
		static double round(double d, int decimals);
		static bool isEqual(double d1, double d2, double epsilon = 1.0E-20);

		/// replaced by QFJson object
		//static QString escapeJsonString(const QString &s);
		//static QString unescapeJsonString(const QString &s);
		//static QString jsonToString(const QVariant &json);
		//static QVariant stringToJson(const QString &str);

		static QVariant retypeVariant(const QVariant _val, QVariant::Type type);

		/**
		 * V seznamu nadrazench objektu pro objekt \a pObject najde takovy,
		 * ktery mapredka vydedeneho z \a type_name.
		 */
		static QObject* findParentOfType(QObject *pObject, const char *type_name, bool throw_exc = Qf::ThrowExc) throw(QFException);

		static void connectSlotsByName(QObject *signal_object, QObject *slot_object);

		static void deleteChildObjects(QObject *parent_object);
};

template <class T>
class QFCORE_DECL_EXPORT QFDeletePointer
{
	//private:
		T *p;
	private:
		void setPointer(T *_p) {
			if(p) delete p;
			p = _p;
		}

		inline QFDeletePointer(const QFDeletePointer<T> &t) {setPointer(NULL);}
		inline QFDeletePointer<T> &operator=(const QFDeletePointer<T> &p) {setPointer(NULL);}
	public:
		inline QFDeletePointer<T> &operator=(T* _p) {setPointer(_p); return *this;}
		inline bool isNull() const { return !p; }
		inline operator bool() const {return p != NULL;}
		inline T* operator->() const { return p; }
		inline T& operator*() const { return *p; }
		inline operator T*() const { return p; }
	public:
		inline QFDeletePointer() : p(NULL) {}
		inline QFDeletePointer(T *_p) : p(_p) {}
		inline ~QFDeletePointer() {setPointer(NULL);}
};

/**
 * V seznamu nadrazench objektu pro objekt \a pObject najde takovy,
 * ktery ma predka vydedeneho z \a T .
 */
template <class T>
T qfFindParent(const QObject *_o, bool throw_exc = Qf::ThrowExc) throw(QFException)
{
	T t = NULL;
	QObject *o = const_cast<QObject*>(_o);
	while(o) {
		o = o->parent();
		if(!o) break;
		t = qobject_cast<T>(o);
		if(t) break;
	}
	if(!t && throw_exc) {
		//QF_ASSERT(t, "carramba");
		QF_EXCEPTION(QString("object 0x%1 has not any parent of requested type.").arg((ulong)_o, 0, 16));
	}
	return t;
}
/*
class QWidget;

inline QWidget* qfTryToFindParentWidget(const QObject *_o)
{
	return qfFindParent<QWidget*>(_o, !Qf::ThrowExc);
}
*/
//! projde rekurzivne vsechny deti objektu \a root a zavola pro ne fci zadanou parametrem \a pmf.
/// priklad volani: qfObjectWalk(menuBar(), this, &MainWindow::objectWalk);
/// @see http://www.glenmccl.com/ptr_cmp.htm
template<class T, typename P>
void qfObjectWalk(QObject *root, T *obj, void (T::*pmf)(QObject*, P), P param)
{
	QObjectList lst = root->children();
	foreach(QObject *o, lst) {
		(obj->*pmf)(o, param);
		qfObjectWalk(o, obj, pmf, param);
	}
}

#define QF_CONNECT(o1, s1, o2, s2) \
	if(!connect(o1, s1, o2, s2)) \
		QF_EXCEPTION(QString("Cann't connect 0x%1 %2 to  0x%3 %4.")\
		.arg((ulong)o1, 0, 16).arg(#s1)\
		.arg((ulong)o2, 0, 16).arg(#s2));

#define SARG(s) "'" + QFString(s) + "'"
#define IARG(i) "" + QFString(i) + ""
/// money
#define MARG(d) "" + QFString::decimalNumber(d, 2) + ""

#endif // QF_H

