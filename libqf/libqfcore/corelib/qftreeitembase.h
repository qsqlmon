
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFTREEITEMBASE_H
#define QFTREEITEMBASE_H

#include <qfcoreglobal.h>

#include <QList>

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFTreeItemBase 
{
	protected:
	public:
		QFTreeItemBase *f_parent;
		QList<QFTreeItemBase*> f_children;
	public:
		QList<QFTreeItemBase*>& childrenRef() {return f_children;}
		const QList<QFTreeItemBase*>& children() const {return f_children;}
		int childrenCount() const {return children().count();}
		virtual QFTreeItemBase* parent() const {return f_parent;}
		virtual QFTreeItemBase* child(int ix) const {return f_children.value(ix);}
		void  setParent(QFTreeItemBase *_parent);
		virtual void clearChildren();
		void unlinkChild(QFTreeItemBase *child);
	public:
		QFTreeItemBase(QFTreeItemBase *parent);
		virtual ~QFTreeItemBase();
};

#endif // QFTREEITEMBASE_H

