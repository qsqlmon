
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFCOREDBAPPLICATION_H
#define QFCOREDBAPPLICATION_H

#include "qfappdbconnectioninterface.h"
#include "qfcoreapplication.h"

#include <qfcoreglobal.h>

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFCoreDbApplication : public QFCoreApplication, public QFAppDbConnectionInterface
{
	Q_OBJECT;
	public:
		static QFCoreDbApplication* instance();
	public:
		QFCoreDbApplication(int & argc, char ** argv);
		virtual ~QFCoreDbApplication();
};

inline QFCoreDbApplication* qfDbApp() {return QFCoreDbApplication::instance();}

#endif // QFCOREDBAPPLICATION_H

