#ifndef QF_COMPAT_H
#define QF_COMPAT_H

#include <QString>

#include <qfcoreglobal.h>
/*
Using the GNU Compiler Collection (GCC)
5.41 Function Names as Strings:
On the other hand, `#ifdef __FUNCTION__' does not have any special meaning inside a function, since the preprocessor does not do anything special with the identifier __FUNCTION__.

#ifndef __FUNCTION__
#define __FUNCTION__ "__FUNCTION__ undefined on this platform"
#endif

#ifndef __PRETTY_FUNCTION__
#define __PRETTY_FUNCTION__ __FUNCTION__
#endif
*/

#define QF_FUNC_NAME Q_FUNC_INFO /// az od verze Qt 4.3
/*
#if defined( _MSC_VER ) && _MSC_VER >= 900  // Visual C++ (and Intel C++)
	#define QF_FUNC_NAME ""
#elif defined( __GNUC__ )
	#define QF_FUNC_NAME __PRETTY_FUNCTION__
#endif
*/
#ifdef WIN32
#define QF_EOLN "\r\n"
#else
#define QF_EOLN "\n"
#endif

/// Objekt zajistujici kompatibilitu systemovych funkci v ruznych OS
class QFCORE_DECL_EXPORT QFCompat
{
public:
	/// vraci nazev souboru, ktery obsahoval spustenou aplikaci vcetne pripony.
	static QString appExeName();
	/// zkusi qApp->applicationName(), pokud je prazdne,
	/// vraci nazev souboru, ktery obsahoval spustenou aplikaci bez pripadne pripony v lowercase.
	/// Napr. pro 'Sased.exe' vrati 'sased'
	/// win95 vraci jmeno aplikace v libovolne velikosti pisma, takze nema cenu delat resources case sensitive.
	static QString appName();
	static QString appDir();
};

#endif // QF_COMPAT_H
