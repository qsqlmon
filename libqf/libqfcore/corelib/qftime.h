
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFTIME_H
#define QFTIME_H

#include <qfcoreglobal.h>

#include <QTime>

//! TODO: write class documentation.
class QFCORE_DECL_EXPORT QFTime : public QTime
{
	public:
		int daySecs() const {return QTime().secsTo(*this);}
		QFTime operator+(const QTime &tm) {return QFTime(daySecs() + QTime().secsTo(tm));}
		QFTime operator-(const QTime &tm) {return QFTime(daySecs() - QTime().secsTo(tm));}
		QFTime& operator+=(const QTime &tm) {
			*this = addSecs(QTime().secsTo(tm));
			return *this;
		}
		QFTime& operator-=(const QTime &tm) {
			*this = addSecs(QTime().secsTo(tm));
			return *this;
		}
	public:
		QFTime() : QTime() {}
		QFTime(int h, int m, int s = 0, int ms = 0) : QTime(h, m, s, ms) {}
		QFTime(int day_secs);
		QFTime(const QTime &tm) : QTime(tm) {}
};

#endif // QFTIME_H

