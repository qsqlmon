
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qftime.h"

#include <qfstring.h>

#include <qflogcust.h>

QFTime::QFTime(int day_secs)
	: QTime()
{
	qfTrash() << QF_FUNC_NAME << "day_secs:" << day_secs;
	*this = addSecs(day_secs);
	qfTrash() << "\tnew value:" << QFString(*this);
}

