
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfcoreapplication.h"

#include <qfassert.h>

#include <qflogcust.h>

QFCoreApplication::QFCoreApplication(int & argc, char ** argv)
	: QCoreApplication(argc, argv)
{
}

QFCoreApplication::~QFCoreApplication()
{
}

QFCoreApplication * QFCoreApplication::instance()
{
	QFCoreApplication *a = qobject_cast<QFCoreApplication*>(QCoreApplication::instance());
	QF_ASSERT(a, "aplikace dosud neni inicializovana");
	return a;
}
