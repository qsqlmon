//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFREPORTITEM_H
#define QFREPORTITEM_H

#include <qfguiglobal.h>
#include <qfdom.h>
#include <qftreeitembase.h>
#include <qfxmltable.h>
#include <qfgraphics.h>

//#include <QObject>
#include <QPointF>
#include <QSizeF>
#include <QRectF>
#include <QTextLayout>
#include <QPicture> 

class QFReportProcessor;
class QFReportItemMetaPaint;

class QFReportItemFrame;
class QFReportItemBand;
class QFReportItemDetail;
class QFReportItemMetaPaintFrame;
/*
class QFObject : public QObject
{
 //Q_OBJECT
	public:
		QFObject(QObject *parent = NULL) : QObject(parent) {}

		QObjectList& childrenRef() {return d_ptr->children;}
};
*/
/*
//! TODO: write class documentation.
class  QFReportItem : public QFTreeItemBase<QFReportItem>
{
	protected:
		//void clearChildren();
	public:
		QFReportItem(QFReportItem *_parent, const QFDomElement &el);
		virtual ~QFReportItem();
};
*/
//! Base class of report elements.
class  QFReportProcessorItem : public QFTreeItemBase
{
	//Q_OBJECT;
	friend class QFReportProcessorScriptDriver;
	public:
		static const double Epsilon;
		static const QString INFO_IF_NOT_FOUND_DEFAULT_VALUE;
		//typedef QFGraphics::Layout Layout;
		enum PrintResultValue {
			PrintNotPrintedYet = 0,
			PrintOk = 1, ///< tisk se zdaril
			PrintNotFit ///< nevytiskl se item nebo vsechny jeho deti, zalomi se stranka/sloupec se a zkusi se to znovu
		};
		enum PrintResultFlags {
			FlagNone = 0,
			FlagPrintAgain = 1, ///< detail se sice vesel, aje protoze data obsahuji dalsi radky, tiskni ho dal, pouziva se s PrintOk.
			//FlagPrintWidthNotFit = 2, ///< Text se nevesel na sirku, takze se neda nic delat, pouziva se s PrintNotFit
			FlagPrintNeverFit = 4 ///< tisk se nepodaril a nikdy se nepodari, pouziva se s PrintNotFit
   			//FlagNotFirstDetailNotFit = 8 ///< u vnorenych tabulek se muze stat, ze se vnorena tabulka nevejde na stranku, vnorena tablka je jeden detail nadrazene tabulky, v takovem pripade se nastavi tento flag, pouziva se s PrintNotFit
		};
		struct PrintResult
		{
			quint16 value;
			quint16 flags;
			PrintResult() : value(0), flags(0) {}
			PrintResult(PrintResultValue val, PrintResultFlags f = FlagNone) : value((quint16)val), flags((quint16)f) {}
			QString toString() const {return QString("value: %1 flags: %2").arg(value).arg(flags);}
 		};
	public:
		typedef QFGraphics::Point Point;
		class Size : public QFGraphics::Size
		{
			public:
				Size() : QFGraphics::Size(0, 0) {}
				Size(qreal x, qreal y) : QFGraphics::Size(x, y) {}
				Size(const QFGraphics::Size &s) : QFGraphics::Size(s) {}
				Size(const QSizeF &s) : QFGraphics::Size(s) {}

				qreal sizeInLayout(QFGraphics::Layout ly)
				{
					if(ly == QFGraphics::LayoutHorizontal) return width();
					return height();
				}

				Size& addSizeInLayout(const Size &sz, QFGraphics::Layout ly)
				{
					if(ly == QFGraphics::LayoutHorizontal) {
						setWidth(width() + sz.width());
						setHeight(qMax(height(), sz.height()));
					}
					else {
						setHeight(height() + sz.height());
						setWidth(qMax(width(), sz.width()));
					}
					return *this;
				}
		};
		class Rect : public QFGraphics::Rect 
		{
			public:
				enum Flag {
					LeftFixed = 1,
					TopFixed = 2,
					BottomFixed = 4,
					RightFixed = 8,
					FillLayout = 16, /// tento item se natahne ve smeru layoutu tak, aby vyplnil cely bounding_rect
					ExpandChildrenFrames = 32, /// viz. atribut expandChildrenFrames v qfreport.rnc
					LayoutHorizontalFlag = 64, /// rect ma layout ve smeru x
					LayoutVerticalFlag = 128, /// rect ma layout ve smeru y, pokud je kombinace LayoutX a LayoutY nesmyslna predpoklada se LayoutX == 0 LayoutY == 1
				};
				enum Unit {UnitInvalid = 0, UnitMM, UnitPercent};
			public:
				unsigned flags;
				Unit horizontalUnit, verticalUnit;
			public:
				bool isAnchored()  const
				{
					return flags & (LeftFixed | TopFixed | BottomFixed | RightFixed);
				}
				bool isRubber(QFGraphics::Layout ly)  const
				{
					if(ly == QFGraphics::LayoutHorizontal) return (width() == 0 && horizontalUnit == UnitMM);
					return (height() == 0 && verticalUnit == UnitMM);
				}
				Unit unit(QFGraphics::Layout ly)  const
				{
					if(ly == QFGraphics::LayoutHorizontal) return horizontalUnit;
					return verticalUnit;
				}

				qreal sizeInLayout(QFGraphics::Layout ly) const
				{
					return Size(size()).sizeInLayout(ly);
				}
				Rect& setSizeInLayout(qreal sz, QFGraphics::Layout ly)
				{
					if(ly == QFGraphics::LayoutHorizontal) setWidth(sz);
					else setHeight(sz);
					return *this;
				}
				Rect& cutSizeInLayout(const Rect &rect, QFGraphics::Layout ly)
				{
					if(ly == QFGraphics::LayoutHorizontal) {
						if(rect.right() > right()) setLeft(right());
						else setLeft(rect.right());
					}
					else {
						if(rect.bottom() > bottom()) setTop(bottom());
						else setTop(rect.bottom());
					}
					return *this;
				}

				QString toString() const {
					return QString("rect(%1, %2, size[%3%4, %5%6]) [%7]")
							.arg(Point(topLeft()).toString(), Point(bottomRight()).toString())
							.arg(size().width()).arg(unitToString(horizontalUnit))
							.arg(size().height()).arg(unitToString(verticalUnit))
							.arg(flagsToString(flags));
				}

				static QString unitToString(Unit u) {
					QString ret;
					switch(u) {
						case UnitMM: ret = "mm"; break;
						case UnitPercent: ret = "%"; break;
						default: ret = "invalid";
					}
					return ret;
				}
				
				static QString flagsToString(unsigned flags) {
					QString ret;
					if(flags & LeftFixed) ret += 'L';
					if(flags & TopFixed) ret += 'T';
					if(flags & RightFixed) ret += 'R';
					if(flags & BottomFixed) ret += 'B';
					if(flags & FillLayout) ret += 'F';
					if(flags & ExpandChildrenFrames) ret += 'E';
					if(flags & LayoutHorizontalFlag) ret += 'X';
					if(flags & LayoutVerticalFlag) ret += 'Y';
					return ret;
				}
			private:
				void init() {
					flags = 0;
					horizontalUnit = verticalUnit = UnitMM;
				}
			public:
				Rect() : QFGraphics::Rect() {init();}
				//Rect(qreal x, qreal y) : QRectF(x, y) {init();}
				Rect(const QPointF &topLeft, const QSizeF &size) : QFGraphics::Rect(topLeft, size) {init();}
				Rect(qreal x, qreal y, qreal width, qreal height) : QFGraphics::Rect(x, y, width, height) {init();}
				Rect(const QRectF &r) : QFGraphics::Rect(r) {init();}
				Rect(const QFGraphics::Rect &r) : QFGraphics::Rect(r) {init();}
		};
	public:
		struct ChildSize {
			qreal size;
			Rect::Unit unit;
			ChildSize(qreal s = 0, Rect::Unit u = Rect::UnitMM) : size(s), unit(u) {}
		};
	public:
		struct Image
		{
			QPicture picture;
			//QPixmap pixmap;
			QImage image;

			//bool isPixmap() const {return !pixmap.isNull();}
			bool isPicture() const {return !picture.isNull();}
			bool isImage() const {return !image.isNull();}
			Size size() const {
				if(isImage()) return QSizeF(image.size());
				//if(isPixmap()) return pixmap.size();
				if(isPicture()) return QSizeF(picture.boundingRect().size());
				return Size();
			}
			bool isNull() const {return picture.isNull() && image.isNull();}
			//const QImage& toQImage() const {return image;}

			Image() {}
			//Image(const QPixmap &px) : pixmap(px) {}
			Image(const QPicture &pc) : picture(pc) {}
			Image(const QImage &im) : image(im) {}
		};
	public:
		virtual QFReportItemFrame* toFrame() {return NULL;}
		virtual  QFReportItemBand* toBand()  {return NULL;}
		virtual  QFReportItemDetail* toDetail()  {return NULL;}
	protected:
		QFXmlTable findDataTable(const QString &name);
	protected:
		//! vrati band, ktery item obsahuje nebo NULL.
		virtual QFReportItemBand* parentBand();
		//! vrati detail, ktery item obsahuje nebo item, pokud je item typu detail nebo NULL.
		virtual QFReportItemDetail* currentDetail();

		QVariant value(const QString &data_src, const QString &domain = "row", const QVariantList &params = QVariantList(), const QVariant &default_value = QFReportProcessorItem::INFO_IF_NOT_FOUND_DEFAULT_VALUE, bool sql_match = true);
		// vrati text slozeny z nodeTextu vsech deti
		//QString nodeChildrenText(const QDomNode &nd) {return nodeChildrenValue().toString();}
		/// poukud ma node jen jedno dite vrati to jeho hodnotu vcetne typu, pokud je deti vic, udela to z nich jeden string
		QVariant concatenateNodeChildrenValues(const QDomNode &nd) ;
		QString nodeText(const QDomNode &nd) ;
		QVariant nodeValue(const QDomNode &nd) ;

		//! Pokud byl predchozi result PrintNotFit a soucasny opet PrintNotFit, znamena to, ze se item uz nikdy nevejde,
		//! zavedenim tohoto fieldu zabranim nekonecnemu odstrankovavani.
		PrintResult checkPrintResult(PrintResult res);

		void updateChildren() {
			if(!childrenSynced()) { syncChildren(); }
		}
		virtual bool childrenSynced();
		virtual void syncChildren();
		void deleteChildren();
	public:
		QFReportProcessor *processor;
		//Rect dirtyRect;
		QFDomElement element;
		Rect designedRect;

		/// Pokud ma frame keepAll atribut a dvakrat za sebou se nevytiskne, znamena to, ze se nevytiskne uz nikdy.
		bool keepAll;
		bool recentlyPrintNotFit;
		//PrintResult recentPrintResult;
	public:
		//! Vraci atribut elementu itemu.
		//! Pokud hodnota \a attr_name je ve tvaru 'script:funcname', zavola se scriptDriver processoru, jinak se vrati atribut.
		QString elementAttribute(const QString &attr_name, const QString &default_val = QString());

		virtual QFReportProcessorItem* parent() const {return static_cast<QFReportProcessorItem*>(this->QFTreeItemBase::parent());}
		virtual QFReportProcessorItem* childAt(int ix) const {return static_cast<QFReportProcessorItem*>(this->children()[ix]);}
		//! Print item in form, that understandable by QFReportPainter.
		virtual PrintResult printMetaPaint(QFReportItemMetaPaint *out, const Rect &bounding_rect) = 0;
		//! Print item in HTML element form.
		virtual PrintResult printHtml(QDomElement &out) {Q_UNUSED(out); return PrintOk;}
		/// vrati definovanou velikost pro item a layout
		virtual ChildSize childSize(QFGraphics::Layout parent_layout) {Q_UNUSED(parent_layout); return ChildSize();}

		QFReportItemFrame* parentFrame() const
		{
			if(parent()) return parent()->toFrame();
			return NULL;
		}
		static const bool IncludingParaTexts = true;
		/// nekdy je potreba jen dotisknout texty a ramecky vytisknout znova, pak je \a including_para_texts == false
		virtual void resetIndexToPrintRecursively(bool including_para_texts) {Q_UNUSED(including_para_texts);}
		virtual bool isBreak() {return false;}

		virtual QString toString(int indent = 2, int indent_offset = 0);
		
		QFReportItemMetaPaint* createMetaPaintItem(QFReportItemMetaPaint *parent);
	public:
		QFReportProcessorItem(QFReportProcessor *proc, QFReportProcessorItem *parent, const QFDomElement &el);
		virtual ~QFReportProcessorItem();
};

//! TODO: write class documentation.
class QFReportItemBreak : public QFReportProcessorItem
{
	//Q_OBJECT;
	protected:
		bool breaking;
	public:
		virtual bool isBreak() {return true;}

		virtual ChildSize childSize(QFGraphics::Layout parent_layout) {Q_UNUSED(parent_layout); return ChildSize(0, Rect::UnitInvalid);}
		virtual PrintResult printMetaPaint(QFReportItemMetaPaint *out, const Rect &bounding_rect);
	public:
		QFReportItemBreak(QFReportProcessor *proc, QFReportProcessorItem *parent, const QFDomElement &el);
};

//! TODO: write class documentation.
class QFReportItemFrame : public QFReportProcessorItem
{
	//Q_OBJECT;
	public:
		bool isRubber(QFGraphics::Layout ly) {
			ChildSize sz = childSize(ly);
			return (sz.size == 0 && sz.unit == Rect::UnitMM);
		}
		// list obsahujici rozmery deti ve frame
		//QList<qreal> childrenSizesInLayout;
		//QList<qreal> childrenSizesInOrthogonalLayout;
		//! children, kterym se ma zacit pri tisku
		int indexToPrint;

		QFGraphics::Layout layout;
		static QFGraphics::Layout orthogonalLayout(QFGraphics::Layout l) {
			if(l == QFGraphics::LayoutHorizontal) return QFGraphics::LayoutVertical;
			if(l == QFGraphics::LayoutVertical) return QFGraphics::LayoutHorizontal;
			return QFGraphics::LayoutInvalid;
		}
		QFGraphics::Layout orthogonalLayout() const {return orthogonalLayout(layout);}

		qreal hinset, vinset;
		Qt::Alignment alignment;
		// parent frame ve funkci printMetaPaint nastavi vsem detem tuto velikost na rozmer,
		// ktery mu pripadne v zavislosti na definici rozmeru ostatnich deti.
		// Hodnota 0 znamena, ze kam natece, tam natece
		//qreal metaPaintLayoutLength;
		// rozmer v druhem smeru, nez je layout.
		//qreal metaPaintOrthogonalLayoutLength;
	protected:
		virtual ChildSize childSize(QFGraphics::Layout parent_layout);
		virtual QFReportItemFrame* toFrame() {return this;}

		virtual PrintResult printMetaPaintChildren(QFReportItemMetaPaint *out, const QFReportProcessorItem::Rect &bounding_rect);
		//! Nastavi u sebe a u deti indexToPrint na nulu, aby se vytiskly na dalsi strance znovu.
		virtual void resetIndexToPrintRecursively(bool including_para_texts);
		QFGraphics::Layout parentLayout() const
		{
			QFReportItemFrame *frm = parentFrame();
			if(!frm) return QFGraphics::LayoutInvalid;
			return frm->layout;
		}

		//void alignChildren(QFReportItemMetaPaintFrame *mp, const QFReportProcessorItem::Rect &dirty_rect);

		virtual PrintResult printMetaPaint(QFReportItemMetaPaint *out, const Rect &bounding_rect);
		virtual PrintResult printHtml(QDomElement &out);
	public:
		QFReportItemFrame(QFReportProcessor *proc, QFReportProcessorItem *parent, const QFDomElement &el);
		virtual ~QFReportItemFrame() {}
};

//! TODO: write class documentation.
class QFReportItemBand : public QFReportItemFrame
{
	//Q_OBJECT;
	protected:
		QFXmlTable f_dataTable;
		/// pokud obsah f_dataTable nepochazi z dat, ale nahraje se dynamicky pomoci elementu <data domain="sql">, ulozi se sem dokument, ktery data z tabulky drzi
		QFXmlTableDocument f_dataTableOwnerDocument;
		bool dataTableLoaded;

		virtual  QFReportItemBand* toBand()  {return this;}
	public:
		virtual PrintResult printMetaPaint(QFReportItemMetaPaint *out, const Rect &bounding_rect);

		virtual QFXmlTable& dataTable();
		virtual void resetIndexToPrintRecursively(bool including_para_texts);
	public:
		QFReportItemBand(QFReportProcessor *proc, QFReportProcessorItem *parent, const QFDomElement &el);
		virtual ~QFReportItemBand() {}
};

//! TODO: write class documentation.
class QFReportItemDetail : public QFReportItemFrame
{
	//Q_OBJECT;
	protected:
		QFXmlTableRow f_dataRow;
		int f_currentRowNo;

		virtual QFReportItemDetail* toDetail() {return this;}
	public:
		virtual PrintResult printMetaPaint(QFReportItemMetaPaint *out, const Rect &bounding_rect);
		virtual PrintResult printHtml(QDomElement &out);

		const QFXmlTableRow& dataRow() const {return f_dataRow;}
		/// cislo prave tisteneho radku, pocitano od nuly.
		int currentRowNo() const {return f_currentRowNo;}
	public:
		QFReportItemDetail(QFReportProcessor *proc, QFReportProcessorItem *parent, const QFDomElement &el);
		virtual ~QFReportItemDetail() {}
};

//! TODO: write class documentation.
class QFReportItemReport : public QFReportItemBand
{
	//Q_OBJECT;
	protected:
		/// body a report ma tu vysadu, ze se muze vickrat za sebou nevytisknout a neznamena to print forever.
		//virtual PrintResult checkPrintResult(PrintResult res) {return res;}
	public:
		virtual PrintResult printMetaPaint(QFReportItemMetaPaint *out, const Rect &bounding_rect);

		virtual QFXmlTable& dataTable();
	public:
		QFReportItemReport(QFReportProcessor *proc, const QFDomElement &el);
		virtual ~QFReportItemReport() {}
};

//! TODO: write class documentation.
class QFReportItemBody : public QFReportItemDetail
{
	//Q_OBJECT;
	protected:
		/// body a report ma tu vysadu, ze se muze vickrat za sebou nevytisknout a neznamena to print forever.
		//virtual PrintResult checkPrintResult(PrintResult res) {return res;}
	public:
		QFReportItemBody(QFReportProcessor *proc, QFReportProcessorItem *parent, const QFDomElement &el)
	: QFReportItemDetail(proc, parent, el) {}
		virtual ~QFReportItemBody() {}

		//virtual PrintResult printMetaPaint(QFReportItemMetaPaint *out, const Rect &bounding_rect);
};

//! TODO: write class documentation.
class QFReportItemTable : public QFReportItemBand
{
	//Q_OBJECT;
	protected:
		QDomDocument fakeBandDocument;
		QFDomElement fakeBand;
		void createFakeBand();
		virtual bool childrenSynced() {return children().count() > 0;}
		virtual void syncChildren();
	public:
		virtual PrintResult printMetaPaint(QFReportItemMetaPaint *out, const Rect &bounding_rect);
	public:
		QFReportItemTable(QFReportProcessor *proc, QFReportProcessorItem *parent, const QFDomElement &el);
		virtual ~QFReportItemTable() {}
};

//! TODO: write class documentation.
class QFReportItemPara : public QFReportItemFrame
{
	//Q_OBJECT;
	protected:
		/// tiskne se printed text od indexToPrint, pouziva se pouze v pripade, ze text pretece na dalsi stranku
		QString printedText;
		QTextLayout textLayout;
	protected:
		virtual PrintResult printMetaPaintChildren(QFReportItemMetaPaint *out, const QFReportProcessorItem::Rect &bounding_rect);

		QString paraText();
	public:
		virtual void resetIndexToPrintRecursively(bool including_para_texts);
		virtual PrintResult printMetaPaint(QFReportItemMetaPaint *out, const Rect &bounding_rect);
		virtual PrintResult printHtml(QDomElement &out);
	public:
		QFReportItemPara(QFReportProcessor *proc, QFReportProcessorItem *parent, const QFDomElement &el);
		virtual ~QFReportItemPara() {}
};

//! TODO: write class documentation.
class QFReportItemImage : public QFReportItemFrame
{
	//Q_OBJECT;
	protected:
		QString src;
		bool childrenSyncedFlag;
		QFDomElement fakeLoadErrorPara;
		QDomDocument fakeLoadErrorParaDocument;
	protected:
		virtual bool childrenSynced();
		virtual void syncChildren();
		virtual PrintResult printMetaPaintChildren(QFReportItemMetaPaint *out, const QFReportProcessorItem::Rect &bounding_rect);
	public:
		QFReportItemImage(QFReportProcessor *proc, QFReportProcessorItem *parent, const QFDomElement &el)
	: QFReportItemFrame(proc, parent, el), childrenSyncedFlag(false) {}
};

//! TODO: write class documentation.
class QFReportItemGraph : public QFReportItemImage
{
	//Q_OBJECT;
	protected:
		virtual void syncChildren();
		virtual PrintResult printMetaPaintChildren(QFReportItemMetaPaint *out, const QFReportProcessorItem::Rect &bounding_rect);
	public:
		QFReportItemGraph(QFReportProcessor *proc, QFReportProcessorItem *parent, const QFDomElement &el)
	: QFReportItemImage(proc, parent, el) {}
};

#endif // QFREPORTITEM_H

