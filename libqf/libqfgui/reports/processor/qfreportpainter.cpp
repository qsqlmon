
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfreportpainter.h"

#include <qflogcust.h>
#include <typeinfo>

//=================================================
//                              QFReportItemMetaPaint
//=================================================
//const QString QFReportItemMetaPaint::currentPageReportSubstitution = "@{#}";
const QString QFReportItemMetaPaint::pageCountReportSubstitution = "@{n}";
//const QString QFReportItemMetaPaint::checkOnReportSubstitution = "@{check:1}";
const QString QFReportItemMetaPaint::checkReportSubstitution = "@{check:${STATE}}";
const QRegExp QFReportItemMetaPaint::checkReportSubstitutionRegExp = QRegExp("@\\{check:(\\d)\\}");

QFReportItemMetaPaint::QFReportItemMetaPaint()
	: QFTreeItemBase(NULL)
{
	f_layoutSettings = NULL;
}

QFReportItemMetaPaint::QFReportItemMetaPaint(QFReportItemMetaPaint *_parent, QFReportProcessorItem *report_item)
	: QFTreeItemBase(_parent)
{
	QF_ASSERT(report_item, "report_item is NULL.");
	QF_ASSERT(report_item->processor, "report_item->processor is NULL.");
	context = report_item->processor->context();
	reportElement = report_item->element;
	f_layoutSettings = NULL;
}

QFReportItemMetaPaint::~ QFReportItemMetaPaint()
{
	SAFE_DELETE(f_layoutSettings);
}

QFReportItemMetaPaint::LayoutSetting * QFReportItemMetaPaint::layoutSettings()
{
	if(!f_layoutSettings) {
		f_layoutSettings = new LayoutSetting;
	}
	return f_layoutSettings;
}

void QFReportItemMetaPaint::paint(QFReportPainter *painter, unsigned mode)
{
	foreach(QFTreeItemBase *_it, children()) {
		QFReportItemMetaPaint *it = static_cast<QFReportItemMetaPaint*>(_it);
		it->paint(painter, mode);
	}
}

void QFReportItemMetaPaint::setInset(qreal hinset, qreal vinset)
{
	if(insetHorizontal() == hinset && insetVertical() == vinset) {
	}
	else {
		layoutSettings()->hinset = hinset;
		layoutSettings()->vinset = vinset;
	}
}


void QFReportItemMetaPaint::shiftChildren(const QFReportProcessorItem::Point offset)
{
	foreach(QFTreeItemBase *_it, children()) {
		QFReportItemMetaPaint *it = static_cast<QFReportItemMetaPaint*>(_it);
		it->renderedRect.translate(offset);
		it->shiftChildren(offset);
	}
}

void QFReportItemMetaPaint::expandChildrenFramesRecursively()
{
	//qfTrash() << "EXPANDING " << reportElement.tagName();
#if 0
	/// ukazalo se, ze nema smysl expandovat deti pouse u itemu, ktere byly expandovany, protoze se muze stat, ze item, ktery nepotrebuje expandovat
	/// ma deti, ktere to potrebuji
	if(renderedRect.flags & QFReportProcessorItem::Rect::LayoutHorizontalFlag) {
		/// ve smeru layoutu natahni jen posledni dite az po inset
		QFReportItemMetaPaint *it = lastChild();
		if(it) {
			if(!Qf::isEqual(it->renderedRect.right(), renderedRect.right() - insetHorizontal())) {
				it->renderedRect.setRight(renderedRect.right() - insetHorizontal());
				it->expandChildrenFramesRecursively();
			}
		}
	}
	else {
		/// ve smeru layoutu natahni jen posledni dite az po inset
		QFReportItemMetaPaint *it = lastChild();
		if(it) {
			if(!Qf::isEqual(it->renderedRect.bottom(), renderedRect.bottom() - insetVertical())) {
				it->renderedRect.setBottom(renderedRect.bottom() - insetVertical());
				it->expandChildrenFramesRecursively();
			}
		}
	}
	foreach(QFTreeItemBase *_it, children) {
		/// ve smeru ortogonalnim k layoutu natahni vsechny deti
		QFReportItemMetaPaint *it = static_cast<QFReportItemMetaPaint*>(_it);
		if(renderedRect.flags & QFReportProcessorItem::Rect::LayoutHorizontalFlag) {
			if(!Qf::isEqual(it->renderedRect.bottom(), renderedRect.bottom() - insetVertical())) {
				it->renderedRect.setBottom(renderedRect.bottom() - insetVertical());
				it->expandChildrenFramesRecursively();
			}
		}
		else {
			if(!Qf::isEqual(it->renderedRect.right(), renderedRect.right() - insetHorizontal())) {
				it->renderedRect.setRight(renderedRect.right() - insetHorizontal());
				it->expandChildrenFramesRecursively();
			}
		}
	}
#else
	if(renderedRect.flags & QFReportProcessorItem::Rect::LayoutHorizontalFlag) {
		/// ve smeru layoutu natahni jen posledni dite az po inset
		QFReportItemMetaPaint *it = lastChild();
		if(it && it->isExpandable()) {
			it->renderedRect.setRight(renderedRect.right() - insetHorizontal());
		}
	}
	else {
		/// ve smeru layoutu natahni jen posledni dite az po inset
		QFReportItemMetaPaint *it = lastChild();
		if(it && it->isExpandable()) {
			it->renderedRect.setBottom(renderedRect.bottom() - insetVertical());
		}
	}
	foreach(QFTreeItemBase *_it, children()) {
		/// ve smeru ortogonalnim k layoutu natahni vsechny deti
		QFReportItemMetaPaint *it = static_cast<QFReportItemMetaPaint*>(_it);
		if(it->isExpandable()) {
			if(renderedRect.flags & QFReportProcessorItem::Rect::LayoutHorizontalFlag) {
				it->renderedRect.setBottom(renderedRect.bottom() - insetVertical());
			}
			else {
				it->renderedRect.setRight(renderedRect.right() - insetHorizontal());
			}
		}
	}
	if(isExpandable()) {
		foreach(QFTreeItemBase *_it, children()) {
			QFReportItemMetaPaint *it = static_cast<QFReportItemMetaPaint*>(_it);
			it->expandChildrenFramesRecursively();
		}
	}
#endif
}

void QFReportItemMetaPaint::alignChildren()
{
	qfLogFuncFrame();
	Rect dirty_rect = renderedRect.adjusted(insetHorizontal(), insetVertical(), -insetHorizontal(), -insetVertical());
	qfTrash() << "\t dirty_rect:" << dirty_rect.toString();
	qfTrash() << "\t alignment:" << alignment();
	qfTrash() << "\t layout:" << layout();
	if(dirty_rect.isValid()) {
		if(alignment() & ~(Qt::AlignLeft | Qt::AlignTop)) {
			Point offset;
			/// ve smeru layoutu posun cely blok
			{
				Rect r1;
				/// vypocitej velikost potisknuteho bloku
				for(int i=0; i<childrenCount(); i++) {
					QFReportItemMetaPaint *it = childAt(i);
					qfTrash() << "\t\t item potisknuty blok:" << it->renderedRect.toString();
					if(i == 0) r1 = it->renderedRect;
					else r1 = r1.united(it->renderedRect);
				}
				qfTrash() << "\t potisknuty blok:" << r1.toString();
				qreal al = 0, d;
				if(layout() == QFGraphics::LayoutHorizontal) {
					if(alignment() & Qt::AlignHCenter) al = 0.5;
					else if(alignment() & Qt::AlignRight) al = 1;
					d = dirty_rect.width() - r1.width();
					if(al > 0 && d > 0)  {
						offset.rx() = d * al - (r1.left() - dirty_rect.left());
					}
				}
				else if(layout() == QFGraphics::LayoutVertical) {
					if(alignment() & Qt::AlignVCenter) al = 0.5;
					else if(alignment() & Qt::AlignBottom) al = 1;
					d = dirty_rect.height() - r1.height();
					if(al > 0 && d > 0)  {
						offset.ry() = d * al - (r1.top() - dirty_rect.top());
					}
				}
			}
			qfTrash() << "\t offset ve smeru layoutu:" << offset.toString();
			/// v orthogonalnim smeru kazdy item
			for(int i=0; i<childrenCount(); i++) {
				QFReportItemMetaPaint *it = childAt(i);
				const Rect &r1 = it->renderedRect;
				qfTrash() << "\t\titem renderedRect:" << r1.toString();
				qreal al = 0, d;

				if(orthogonalLayout() == QFGraphics::LayoutHorizontal) {
					offset.rx() = 0;
					if(alignment() & Qt::AlignHCenter) al = 0.5;
					else if(alignment() & Qt::AlignRight) al = 1;
					d = dirty_rect.width() - r1.width();
					if(al > 0 && d > 0)  {
						qfTrash() << "\t\thorizontal alignment:" << al;
						offset.rx() = d * al - (r1.left() - dirty_rect.left());
					}
				}
				else if(orthogonalLayout() == QFGraphics::LayoutVertical) {
					offset.ry() = 0;
					al = 0;
					if(alignment() & Qt::AlignVCenter) al = 0.5;
					else if(alignment() & Qt::AlignBottom) al = 1;
					d = dirty_rect.height() - r1.height();
					if(al > 0 && d > 0)  {
						qfTrash() << "\t\tvertical alignment:" << al;
						offset.ry() = d * al - (r1.top() - dirty_rect.top());
					}
				}
				qfTrash() << "\t\talign offset:" << offset.toString();
				if(!offset.isNull()) it->shift(offset);
			}
		}
	}
}

QString QFReportItemMetaPaint::dump(int indent)
{
	QString indent_str;
	indent_str.fill(' ', indent);
	const char *type_name = typeid(*this).name();
	QString ret = QString("%1[%2] 0x%3 '%4'").arg(indent_str).arg(type_name).arg((qulonglong)this, 0, 16).arg(reportElement.tagName());
	QFReportItemMetaPaintFrame *frm = dynamic_cast<QFReportItemMetaPaintFrame*>(this);
	if(frm) ret += " : " + frm->renderedRect.toString();
	ret += "\n";
	foreach(QFTreeItemBase *_it, children()) {
		QFReportItemMetaPaint *it = static_cast<QFReportItemMetaPaint*>(_it);
		ret += it->dump(indent + 2);
	}
	return ret;
}

//=================================================
//                              QFReportItemMetaPaintReport
//=================================================
QFReportItemMetaPaintReport::QFReportItemMetaPaintReport(QFReportProcessorItem *report_item)
	: QFReportItemMetaPaint(NULL, report_item)
{
	QString s = report_item->elementAttribute("orientation", "portrait");
	if(s == "landscape") orientation = QPrinter::Landscape;
	else orientation = QPrinter::Portrait;
	pageSize = QSize(report_item->elementAttribute("w").toInt(), report_item->elementAttribute("h").toInt());
}

//=================================================
//                              QFReportItemMetaPaintFrame
//=================================================
QFReportItemMetaPaintFrame::QFReportItemMetaPaintFrame(QFReportItemMetaPaint *_parent, QFReportProcessorItem *report_item)
	: QFReportItemMetaPaint(_parent, report_item)
{
	qfTrash() << QF_FUNC_NAME << reportElement.tagName();
	QFString s = report_item->elementAttribute("fill");
	if(!!s) fill = context.styleCache().brush(s);
	s = report_item->elementAttribute("brd");
	if(!!s) {
		lbrd = rbrd = tbrd = bbrd = context.styleCache().pen(s);
	}
	s = report_item->elementAttribute("lbrd");
	if(!!s) lbrd = context.styleCache().pen(s);
	s = report_item->elementAttribute("rbrd");
	if(!!s) rbrd = context.styleCache().pen(s);
	s = report_item->elementAttribute("tbrd");
	if(!!s) tbrd = context.styleCache().pen(s);
	s = report_item->elementAttribute("bbrd");
	if(!!s) bbrd = context.styleCache().pen(s);
	qfTrash() << "\tRETURN";
}

void QFReportItemMetaPaintFrame::paint(QFReportPainter *painter, unsigned mode)
{
	qfTrash() << QF_FUNC_NAME << reportElement.tagName();
	QF_ASSERT(painter, "painter is NULL");
	qfTrash() << "\trenderedRect:" << renderedRect.toString();
	bool selected = (painter->selectedItem() && painter->selectedItem() == this);
	if(mode & PaintFill) fillItem(painter, selected);
	QFReportItemMetaPaint::paint(painter, mode);
	//if(selected) qfTrash() << "\tBINGO";
	if(mode & PaintBorder) frameItem(painter, selected);
}

void QFReportItemMetaPaintFrame::fillItem(QPainter *painter, bool selected)
{
	Rect r = QFGraphics::mm2device(renderedRect, painter->device());
	qfTrash().color(QFLog::Yellow) << QF_FUNC_NAME << reportElement.tagName();
	//qfInfo() << "\t logicalDpiX:" << painter->device()->logicalDpiX();
	//qfInfo() << "\t logicalDpiY:" << painter->device()->logicalDpiY();
	//qfInfo() << "\t rendered rect:" << renderedRect.toString();
	//qfInfo() << "\t br:" << r.toString();
	//qfTrash() << "\tbrush color:"
	if(selected) {
		painter->fillRect(r, QColor("#FFEEEE"));
	}
	else {
		if(fill.style() != Qt::NoBrush) painter->fillRect(r, fill);
		//QFString s = element.attribute("fill");
		//if(!!s) painter->fillRect(r, context.brushFromString(s));
	}
	//painter->fillRect(r, QColor("orange"));
}

void QFReportItemMetaPaintFrame::frameItem(QPainter *painter, bool selected)
{
	Rect r = renderedRect;
	QFString s;
	if(selected) {
		s = "color: magenta; style: solid; size:2";
		painter->setPen(context.styleCache().pen(s));
		painter->setBrush(QBrush());
		painter->drawRect(QFGraphics::mm2device(renderedRect, painter->device()));
	}
	else {
		/*
		s = element.attribute("brd");
		qfTrash() << QF_FUNC_NAME << s;
		if(!!s) {
			tbrd = lbrd = bbrd = rbrd = element.attribute("brd");
		}
		*/
		drawLine(painter, LBrd, lbrd);
		drawLine(painter, TBrd, tbrd);
		drawLine(painter, RBrd, rbrd);
		drawLine(painter, BBrd, bbrd);
	}
}

void QFReportItemMetaPaintFrame::drawLine(QPainter *painter, LinePos where, const QPen &_pen)
{
	if(_pen.widthF() == 0) return;
	QPen pen = _pen;
	/// preved tiskarske body na body vystupniho zarizeni
	qreal w = pen.widthF() * 25.4 / 72;
	/// ted je w v milimetrech
	bool horizontal = (where == TBrd || where == BBrd);
	if(horizontal) pen.setWidthF(QFGraphics::y2device(w, painter->device()));
	else pen.setWidthF(QFGraphics::x2device(w, painter->device()));
	Point p1, p2;
	Rect r = QFGraphics::mm2device(renderedRect, painter->device());
	if(where == TBrd) { p1 = r.topLeft(); p2 = r.topRight(); }
	else if(where == LBrd) { p1 = r.topLeft(); p2 = r.bottomLeft(); }
	else if(where == BBrd) { p1 = r.bottomLeft(); p2 = r.bottomRight(); }
	else if(where == RBrd) { p1 = r.topRight(); p2 = r.bottomRight(); }
	if(!(p1 == p2)) {
		painter->setPen(pen);
		painter->drawLine(p1, p2);
	}
}
//=================================================
//                              QFReportItemMetaPaintPage
//=================================================
/*
QFReportItemMetaPaintPage::QFReportItemMetaPaintPage(QFReportItemMetaPaint *parent, const QFDomElement &el, const QFReportProcessor::Context &context)
	: QFReportItemMetaPaintFrame(parent, el, context)
{
}

void QFReportItemMetaPaintPage::paint(QFReportPainter *painter)
{
	qfTrash() << QF_FUNC_NAME << reportElement.tagName();
	qfTrash() << "\trenderedRect:" << renderedRect.toString();
	//qfTrash() << "\tchildren cnt:" << children.count();
	//painter->fillRect(renderedRect, context.brushFromString("color: white"));
	QFReportItemMetaPaintFrame::paint(painter);
}
	*/

//=================================================
//                              QFReportItemMetaPaintText
//=================================================
void QFReportItemMetaPaintText::paint(QFReportPainter *painter, unsigned mode)
{
	qfTrash() << QF_FUNC_NAME << reportElement.tagName();
	QF_ASSERT(painter, "painter is NULL");
	if(mode != PaintFill) return;
	
	QFontMetricsF font_metrics = QFontMetricsF(painter->font(), painter->device());

	//QFReportItemMetaPaintFrame::paint(painter);
	//qfTrash() << "\trenderedRect:" << renderedRect.toString();
	//painter->setBrush(brush);
	painter->setPen(pen);
	painter->setFont(font);
	QString s = text;
	if(text.indexOf('@') >= 0) {
		//s = s.replace(currentPageReportSubstitution, QString::number(painter->currentPage + 1));
		s = s.replace(pageCountReportSubstitution, QString::number(painter->pageCount));
	}
	Rect br = QFGraphics::mm2device(renderedRect, painter->device());
	br.adjust(0, 0, 1, 1); /// nekdy se stane, kvuji nepresnostem prepocitavani jednotek, ze se to vyrendruje pri tisku jinak, nez pri kompilaci, tohle trochu pomaha:)
	//r.setHeight(500);
	//painter->fillRect(r, QColor("#DDDDDD"));
	//qfWarning().noSpace() << "'" << s << "' flags: " << flags;
#if 0
	painter->drawText(br, flags, s);
#else
	/// to samy jako v #if 0, jen se to tiskne stejnym zpusobem, jako se to kompilovalo, coz muze ukazat, proc to vypada jinak, nez cekam
	qreal leading = font_metrics.leading();
	qreal height = 0;
	//qreal width = 0;
	QTextLayout textLayout;
	Qt::Alignment alignment = (~Qt::Alignment()) & flags;
	QTextOption opt(alignment);
	opt.setWrapMode(QTextOption::WrapAtWordBoundaryOrAnywhere);
	textLayout.setTextOption(opt);
	textLayout.setFont(painter->font());
	textLayout.setText(s);
	textLayout.beginLayout();
	while (1) {
		QTextLine line = textLayout.createLine();
		if(!line.isValid()) {
			break;
		}
		line.setLineWidth(br.width());
		if(height > 0) height += leading;
		line.setPosition(QPointF(0., height));
		height += line.height();
		//width = qMax(width, line.naturalTextWidth());
	}
	textLayout.endLayout();
	textLayout.draw(painter, br.topLeft());
#endif
}

QString QFReportItemMetaPaintText::dump(int indent)
{
	QString indent_str;
	indent_str.fill(' ', indent);
	QString ret = QString("%1[%2] 0x%3 '%4'").arg(indent_str).arg(typeid(*this).name()).arg((qulonglong)this, 0, 16).arg(reportElement.tagName());
	ret += QString(" '%1'\n").arg(text);
	foreach(QFTreeItemBase *_it, children()) {
		QFReportItemMetaPaint *it = static_cast<QFReportItemMetaPaint*>(_it);
		ret += it->dump(indent + 2);
	}
	return ret;
}

//=================================================
//                              QFReportItemMetaPaintCheck
//=================================================
void QFReportItemMetaPaintCheck::paint(QFReportPainter * painter, unsigned mode)
{
	qfTrash() << QF_FUNC_NAME << reportElement.tagName();
	QF_ASSERT(painter, "painter is NULL");
	if(mode != PaintFill) return;
	
	QFontMetricsF font_metrics = QFontMetricsF(painter->font(), painter->device());

	QRegExp rx = QFReportItemMetaPaint::checkReportSubstitutionRegExp;
	if(rx.exactMatch(text)) {
		bool check_on = rx.capturedTexts().value(1) == "1";
		/// V tabulkach by jako check OFF slo netisknout vubec nic,
		/// ale na ostatnich mistech repotu je to zavadejici .
			
		/// BOX
		static QString s_box = "color: black; style: solid; size:1";
		painter->setPen(context.styleCache().pen(s_box));
		painter->setBrush(QBrush());
		QFReportProcessorItem::Rect r = renderedRect;
		r.translate(0, -font_metrics.leading());
		qreal w = renderedRect.width();
		if(flags & Qt::AlignHCenter) r.translate((w - r.width())/2., 0);
		else if(flags & Qt::AlignRight) r.translate(w - r.width(), 0);
			//r.setWidth(2* r.width() / 3.);
			//r.setHeight(2 * r.height() / 3.);
		painter->drawRect(QFGraphics::mm2device(r, painter->device()));
		
		if(check_on) {
			/// CHECK
			static QString s_check = "color: teal; style: solid; size:2";
			painter->setPen(context.styleCache().pen(s_check));
			r = QFGraphics::mm2device(r, painter->device());
			QPointF p1(r.left(), r.top() + r.height() / 2);
			QPointF p2(r.left() + r.width() / 2, r.bottom());
			painter->drawLine(p1, p2);
			p1 = QPointF(r.right() + 0.2 * r.width(), r.top());
			painter->drawLine(p2, p1);
		}
		return;
	}
}

//=================================================
//                              QFReportItemMetaPaintImage
//=================================================
void QFReportItemMetaPaintImage::paint(QFReportPainter *painter, unsigned mode)
{
	qfTrash().color(QFLog::Green) << QF_FUNC_NAME << reportElement.tagName() << "mode:" << mode;
	QF_ASSERT(painter, "painter is NULL");
	QPrinter *printer = dynamic_cast<QPrinter*>(painter->device());
	//qfInfo() << "printer:" << printer;
	if(printer && printer->outputFormat() == QPrinter::NativeFormat) {
		if(f_layoutSettings && f_layoutSettings->suppressPrintOut) {
			//qfInfo() << "print out suppressed";
			return;
		}
	}
	if(mode != PaintFill) return;

	Rect br = QFGraphics::mm2device(renderedRect, painter->device());
	//br.adjust(0, 0, 1, 1); /// nekdy se stane, kvuji nepresnostem prepocitavani jednotek, ze se to vyrendruje pri tisku jinak, nez pri kompilaci, tohle trochu pomaha:)
	//r.setHeight(500);
	//painter->fillRect(r, QColor("#DDDDDD"));
	//qfWarning().noSpace() << "'" << s << "' flags: " << flags;
	//qfInfo() << "\t image is pixmap:" << image.isPixmap();
	if(image.isImage()) {
		painter->save();
		QSize sz = image.image.size();
		painter->translate(br.topLeft());
		painter->scale(br.width() / sz.width(), br.height() / sz.height());
		painter->drawImage(QPoint(0, 0), image.image);
		painter->restore();
		//painter->drawImage(br.topLeft(), image.image.scaled(br.size().toSize(), aspectRatioMode, Qt::SmoothTransformation));
	}
	else if(image.isPicture()) {
		painter->save();
		QSize sz = image.picture.boundingRect().size();
		painter->translate(br.topLeft());
		painter->scale(br.width() / sz.width(), br.height() / sz.height());
		painter->drawPicture(QPoint(0, 0), image.picture);
		painter->restore();
	}
	//if(image.isImage()) painter->drawImage(br.topLeft(), image.image.scaled(br.size().toSize(), Qt::KeepAspectRatioByExpanding));
}

QString QFReportItemMetaPaintImage::dump(int indent)
{
	QString indent_str;
	indent_str.fill(' ', indent);
	QString ret = QString("%1[%2] 0x%3 '%4'").arg(indent_str).arg(typeid(*this).name()).arg((qulonglong)this, 0, 16).arg(reportElement.tagName());
	ret += QString(" '%1'\n").arg("image");
	return ret; 
}

//=================================================
//                              QFReportPainter
//=================================================
QFReportPainter::QFReportPainter(QPaintDevice *device)
	: QPainter(device)
{
	//currentPage = 0;
	pageCount = 0;
	f_selectedItem = NULL;
}

QFReportPainter::~QFReportPainter()
{
}

void QFReportPainter::drawMetaPaint(QFReportItemMetaPaint *item)
{
	if(item) {
		item->paint(this, QFReportItemMetaPaint::PaintFill);
		item->paint(this, QFReportItemMetaPaint::PaintBorder);
	}
}




