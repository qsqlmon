
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfreportitem.h"
#include "qfreportprocessor.h"
#include "qfreportpainter.h"

#include <qffileutils.h>

#include <QScriptEngine>
#include <QSet>

#include <qflogcust.h>

//===================================================
//                       QFReportProcessorScriptDriver
//===================================================
QScriptValue QFReportProcessorScriptDriver::call(QFReportProcessorItem *item, const QString & func_name, const QVariantList & arg_list) 
{
	qfTrash() << QF_FUNC_NAME << "fn:" << func_name;
	currentCallContextItem = item;
	QScriptValue sv = scriptEngine()->globalObject().property(func_name);
	if(sv.isFunction()) {
		QScriptValueList args;
		foreach(QVariant v, arg_list) args << scriptEngine()->toScriptValue(v);
		sv =  sv.call(QScriptValue(), args);
	}
	else {
		qfWarning() << tr("JavaScript funkce '%1' neni definovana.").arg(func_name);
	}
	return sv;
}

QScriptValue QFReportProcessorScriptDriver::value(const QString & data_src, const QString & domain, const QVariantList &params, const QVariant &default_value, bool sql_match) 
{
	qfLogFuncFrame() << "data_src:" << data_src << "domain:" << domain << "params cnt:" << params.count() << "sql_match" << sql_match;
	//qfInfo() << QF_FUNC_NAME;
	QVariant v;
	qfTrash() << "\t currentCallContextItem:" << currentCallContextItem;
	if(currentCallContextItem) v = currentCallContextItem->value(data_src, domain, params, default_value, sql_match);
	return variantToScriptValue(v);	
}
/*
void QFReportProcessor::Document::resolveIncludesHook(QDomElement &parent_include_element, QDomNode &inserted_node, const QFDomDocument &inserted_document)
{
	Q_UNUSED(parent_include_element);
	//Q_UNUSED(inserted_node);
	//Q_UNUSED(inserted_document);
	QFDomElement el = inserted_node.toElement();
	if(!!el) el.setAttribute("included", inserted_document.fileName());
}
*/
//===================================================
//                                QFReportProcessor::Context
//===================================================
QFReportProcessor::Context::Context()
{
	d = new Data();
}

//===================================================
//                                QFReportProcessor
//===================================================
QFSearchDirs QFReportProcessor::f_searchDirs;

QFReportProcessor::QFReportProcessor(QPaintDevice *paint_device, QObject *parent)
	: QObject(parent), fProcessedItemsRoot(NULL), f_processorOutput(NULL), f_scriptDriver(NULL)
{
	fPaintDevice = paint_device;
	fProcessedPageNo = 0;
	setDesignMode(false);
}

QFReportProcessor::~QFReportProcessor()
{
	SAFE_DELETE(fProcessedItemsRoot);
}

QFReportProcessorScriptDriver* QFReportProcessor::scriptDriver()
{
	if(f_scriptDriver == NULL) f_scriptDriver = new QFReportProcessorScriptDriver(this);
	return f_scriptDriver;
}

void QFReportProcessor::reset()
{
	qfLogFuncFrame();
	makeContext();
	SAFE_DELETE(fProcessedItemsRoot);
	SAFE_DELETE(f_processorOutput);
}

void QFReportProcessor::setReport(const QFDomDocument &doc)
{
	qfLogFuncFrame();
	fReport = doc;
	reset();
}

void QFReportProcessor::setReport(const QString &rep_file_name) throw(QFException)
{
	//QString s = resolveFileName(rep_file_name);
	//QFile f(s);
	//fReport.setContent(f);
	//fReport.resolveIncludes(searchDirs());
	QFDomDocument doc;
	doc.setContentAndResolveIncludes(rep_file_name, searchDirs(), Qf::ThrowExc);
	//qfWarning() << fReport.toString();
	setReport(doc);
}

void QFReportProcessor::setData(const QFXmlTable &_data)
{
	//qfInfo() << "QFReportProcessor _data:" << _data.toString().mid(0, 100);
	fData = _data;
	//qfInfo() << "QFReportProcessor data:" << fData.toString().mid(0, 100);
}

void QFReportProcessor::makeContext()
{
	qfLogFuncFrame();
	contextRef() = Context();
	QFDomElement el = fReport.cd("/", !Qf::ThrowExc);
	qfTrash() << "\tfound <report>:" << !el.isNull();
	for(el=el.firstChildElement("stylesheet"); !!el; el=el.nextSiblingElement("stylesheet")) {
		//qfTrash() << "\tfound <stylesheet>";
		contextRef().styleCacheRef().readStyleSheet(el);
	}
	//qfTrash() << context().styleCache().toString();
}

QFReportProcessorItem *QFReportProcessor::processedItemsRoot()
{
	if(!fProcessedItemsRoot) {
		if(report().isEmpty()) {
			fReport.setContent("<report/>");
		}
		fProcessedItemsRoot = createProcessibleItem(report().firstChildElement(), NULL);
	}
	return fProcessedItemsRoot;
}

void QFReportProcessor::process(QFReportProcessor::ProcessorMode mode)
{
	qfLogFuncFrame() << "mode:" << mode;
	if(mode == FirstPage || mode == AllPages) {
		fProcessedPageNo = 0;
		SAFE_DELETE(f_processorOutput);
		f_processorOutput = new QFReportItemMetaPaintReport(processedItemsRoot());
		singlePageProcessResult = QFReportProcessorItem::PrintResult(QFReportProcessorItem::PrintNotFit);
	}
	QFReportItemMetaPaint mpit;
	//context().dump();
	while(singlePageProcessResult.value == QFReportProcessorItem::PrintNotFit
			 && !(singlePageProcessResult.flags & QFReportProcessorItem::FlagPrintNeverFit)) {
		singlePageProcessResult = processPage(&mpit);
		qfTrash() << "singlePageProcessResult:" << singlePageProcessResult.toString();
		//qfTrash().color(QFLog::Yellow) << context().styleCache().toString();
		//mpit.dump();
		QFReportItemMetaPaint *it = mpit.childAt(0);
		it->setParent(f_processorOutput);
		if(mode == FirstPage || mode == SinglePage) {
			emit pageProcessed();
			if(singlePageProcessResult.value == QFReportProcessorItem::PrintNotFit) {
				fProcessedPageNo++;
			}
			//qfInfo() << "pageProcessed:" << fProcessedPageNo;
			break;
		}
		else {
			if(singlePageProcessResult.value == QFReportProcessorItem::PrintNotFit) {
				fProcessedPageNo++;
			}
			else {
				break;
			}
		}
	}
	/*
	if(processorThread && processorThread->isRunning()) {
		qfWarning() << "processor thread is still running";
		return;
	}
	SAFE_DELETE(processorThread);
	processorThread = new QFReportProcessorThread(this);
	processorThread->start();
	*/
}

QFReportProcessorItem::PrintResult QFReportProcessor::processPage(QFReportItemMetaPaint *out)
{
	qfTrash() << QF_FUNC_NAME;
	QFReportProcessorItem::PrintResult res = processedItemsRoot()->printMetaPaint(out, QFReportProcessorItem::Rect());
	qfTrash() << "\tres:" << res.toString();
	return res;
}

bool QFReportProcessor::isProcessible(const QFDomElement &el)
{
	static QSet<QString> set;
	if(set.isEmpty()) {
		set << "report" << "body" << "frame" << "row" << "cell"
				<< "para" << "table" << "band" << "detail"
				<< "image" << "graph"
				<< "space" << "break";
				//<< "script";
	}
	bool ret = set.contains(el.tagName());
	//if(!ret) { qfError() << "Element '" + el.tagName() + "' is not processible."; }
	return ret;
}

QFReportProcessorItem* QFReportProcessor::createProcessibleItem(const QFDomElement &_el, QFReportProcessorItem *parent) throw(QFException)
{
	qfLogFuncFrame() << "parent:" << ((parent)?parent->element.tagName(): "NULL") << "to create:" << _el.tagName();
	QFReportProcessorItem *it = NULL;
	QFDomElement el(_el);
	if(!isProcessible(el)) {
	//QF_ASSERT(isProcessible(_el), "Element '" + _el.tagName() + "' is not processible.");
		qfWarning() << tr("Element '%1' is not processible and it will be ignored.").arg(el.tagName());
	}
	else {
		{
			QString s = el.attribute("copyAttributesFromId");
			if(!s.isEmpty()) {
				QStringList sl = s.split(':');
				int level = 0;
				if(sl.count() > 1) {
					s = sl[0];
					level = sl[1].toInt();
				}
				report().copyAttributesFromId(el, s, level, true/*only_new*/);
			}
		}
		if(el.tagName() == "report") {
			if(el.attribute("headeronbreak").isEmpty()) el.setAttribute("headeronbreak", "1");
		}
		else if(el.tagName() == "row") {
			el.setAttribute("layout", "horizontal");
			if(el.attribute("keepall").isEmpty()) el.setAttribute("keepall", "1");
		}
		else if(el.tagName() == "cell") {
			if(el.attribute("keepall").isEmpty()) el.setAttribute("keepall", "1");
		}
		else if(el.tagName() == "detail") {
			if(el.attribute("layout").isEmpty()) el.setAttribute("layout", "horizontal"); /// pro detail je defaultni layout horizontal.
			/// detail nemuze mit keepall, protoze pokud je detail vnorena tabulka a nevejde se na stranku, neni vytistena
			//if(el.attribute("keepall").isEmpty()) el.setAttribute("keepall", "1");
		}
		it = createItem(parent, el);
		//QF_ASSERT(it != NULL, el.tagName() + " element can not be created");
	}

	//qfTrash() << "\tcreated:" << it;
	//qfTrash() << "\tcreated child name:" << it->element.tagName();
	return it;
}

QFReportProcessorItem * QFReportProcessor::createItem(QFReportProcessorItem * parent, const QFDomElement & el) throw( QFException )
{
	QFReportProcessorItem *it = NULL;
	if(el.tagName() == "report") {
		it = new QFReportItemReport(this, el);
	}
	else if(el.tagName() == "body") {
		it = new QFReportItemBody(this, parent, el);
	}
	else if(el.tagName() == "break") {
		it = new QFReportItemBreak(this, parent, el);
	}
	else if(el.tagName() == "frame") {
		it = new QFReportItemFrame(this, parent, el);
	}
	else if(el.tagName() == "image") {
		it = new QFReportItemImage(this, parent, el);
	}
	else if(el.tagName() == "graph") {
		it = new QFReportItemGraph(this, parent, el);
	}
	else if(el.tagName() == "space") {
		it = new QFReportItemFrame(this, parent, el);
	}
	else if(el.tagName() == "row") {
		it = new QFReportItemFrame(this, parent, el);
	}
	else if(el.tagName() == "cell") {
		it = new QFReportItemFrame(this, parent, el);
	}
	else if(el.tagName() == "para") {
		it = new QFReportItemPara(this, parent, el);
	}
	else if(el.tagName() == "band") {
		it = new QFReportItemBand(this, parent, el);
	}
	else if(el.tagName() == "detail") {
		it = new QFReportItemDetail(this, parent, el);
	}
	else if(el.tagName() == "table") {
		it = new QFReportItemTable(this, parent, el);
	}
	QF_ASSERT(it != NULL, el.tagName() + " element can not be created");
	return it;
}

QFontMetricsF QFReportProcessor::fontMetrics(const QFont &font)
{
	return QFontMetricsF(font, paintDevice());
}
/*
QString QFReportProcessor::resolveFileName(const QString &f_name) throw(QFException)
{
	qfLogFuncFrame();
	QString ret = f_searchDirs.findFile(f_name);
	if(ret.isEmpty()) QF_EXCEPTION(tr("File '%1' can not be resolved trying %2.").arg(f_name).arg(f_searchDirs.dirs().join(", ")));
	return ret;
}
*/
/*
QString QFReportProcessor::resolveFN(const QString &f_name) throw(QFException)
{
	return resolveFileName(f_name);
}
*/

void QFReportProcessor::processHtml(QDomElement & el_body) throw( QFException )
{
	processedItemsRoot()->resetIndexToPrintRecursively(QFReportProcessorItem::IncludingParaTexts);
	processedItemsRoot()->printHtml(el_body);
	fixTableTags(el_body);
	removeRedundantDivs(el_body);
	/// tak a z divu s horizontalnim layoutem udelej tabulky
	fixLayoutHtml(el_body);
}

void QFReportProcessor::fixTableTags(QDomElement & _el)
{
	QFDomElement el(_el);
	bool is_table_row = false;
	bool is_table_header_row = false;
	QString attr = el.attribute("__table");
	if(!attr.isEmpty()) {
		attr = attr.mid(QString("__fakeBand").length());
		if(attr == "Table") {
			el.setTagName("table");
			el.setAttribute("border", 1);
		}
		else if(attr == "Detail") {el.setTagName("tr"); is_table_row = true;}
		else if(attr == "HeaderRow") {el.setTagName("tr"); is_table_header_row = true;}
		else if(attr == "FooterRow") {el.setTagName("tr"); is_table_header_row = true;}
	}
	for(QDomElement el1 = el.firstChildElement(); !el1.isNull(); el1 = el1.nextSiblingElement()) {
		fixTableTags(el1);
		if(is_table_row) el1.setTagName("td");
		if(is_table_header_row) el1.setTagName("th");
	}
}

QDomElement QFReportProcessor::removeRedundantDivs(QDomElement & _el)
{
	qfLogFuncFrame() << _el.tagName() << "children cnt:" << _el.childNodes().count();
	QFDomElement el(_el);
	qfTrash() << "\t path:" << el.path();
	/// pokud ma div prave jedno dite, je na prd
	while(el.tagName() == "div" && el.childNodes().count() == 1) {
		QDomNode parent_nd = el.parentNode();
		QDomElement el_child = el.childNodes().at(0).toElement();
		if(el_child.isNull()) break;
		if(!el_child.isNull() && !parent_nd.isNull()) {
			qfTrash() << "\t child el:" << el_child.tagName() << "children cnt:" << el_child.childNodes().count();
			parent_nd.replaceChild(el_child, el).toElement();
			el = el_child;
			qfTrash() << "\t new el:" << el.tagName() << "children cnt:" << el.childNodes().count();
		}
	}
	qfTrash() << "\t checking children of el:" << el.tagName() << "children cnt:" << el.childNodes().count();
	for(QDomElement el1 = el.firstChildElement(); !el1.isNull(); el1 = el1.nextSiblingElement()) {
		el1 = removeRedundantDivs(el1);
	}
	return el;
}

QDomElement QFReportProcessor::fixLayoutHtml(QDomElement & _el)
{
	qfLogFuncFrame() << _el.tagName() << "children cnt:" << _el.childNodes().count();
	QFDomElement el(_el);
	if(el.tagName() == "div") {
		QString attr = el.attribute("layout");
		if(attr == "horizontal") {
			QDomNode parent_nd = el.parentNode();
			if(!parent_nd.isNull()) {
				QDomElement el_table = el.ownerDocument().createElement("table").toElement();
				QDomNode old_el = parent_nd.replaceChild(el_table, el);
				QDomNode el_tr = el.ownerDocument().createElement("tr");
				el_table.appendChild(el_tr);
				while(true) {
					QDomElement el1 = old_el.firstChildElement();
					if(el1.isNull()) break;
					QDomNode el_td = el.ownerDocument().createElement("td");
					el_tr.appendChild(el_td);
					el_td.appendChild(el1);
				}
				el = el_table;
			}
		}
	}
	for(QDomElement el1 = el.firstChildElement(); !el1.isNull(); el1 = el1.nextSiblingElement()) {
		el1 = fixLayoutHtml(el1);
	}
	return el;
}

void QFReportProcessor::dump()
{
	if(f_processorOutput) f_processorOutput->dump();
}

void QFReportProcessor::print(QPrinter &printer, const QVariantMap &options) throw(QFException)
{
	qfLogFuncFrame();
	
	QFReportPainter painter(&printer);
	
	typedef QFReportProcessorItem::Rect Rect;
	typedef QFReportProcessorItem::Size Size;
	
	int pg_no = options.value("fromPage", 1).toInt() - 1;
	int to_page = options.value("toPage", pageCount()).toInt();
	qfTrash() << "pg_no:" << pg_no << "to_page:" << to_page;
	QFReportItemMetaPaintFrame *frm = getPage(pg_no);
	if(frm) {
		Rect r = frm->renderedRect;
		bool landscape = r.width() > r.height();
		if(landscape) printer.setOrientation(QPrinter::Landscape);
		Rect printer_pg_rect = QRectF(printer.pageRect());
		//qfWarning() << "\tprinter page rect:" << printer_pg_rect.toString();
		//qfWarning() << "\tresolution:" << printer.resolution() << Size(printer_pg_rect.size()/printer.resolution()).toString(); /// resolution je v DPI
		//qreal magnify = printer_pg_rect.width() / r.width();
		//painter.scale(magnify, magnify);
		painter.pageCount = pageCount();
		while(frm) {
			//painter.currentPage = pg_no;
			painter.drawMetaPaint(frm);
			pg_no++;
			frm = getPage(pg_no);
			if(!frm) break;
			if(pg_no >= to_page) break;
			printer.newPage();
		}
	}
}

QFReportItemMetaPaintFrame* QFReportProcessor::getPage(int n)
{
	if(!processorOutput()) return NULL;
	if(n < 0 || n >= processorOutput()->childrenCount()) return NULL;
	QFReportItemMetaPaint *it = processorOutput()->childAt(n);
	QFReportItemMetaPaintFrame *frm	= dynamic_cast<QFReportItemMetaPaintFrame*>(it);
	qfTrash() << "\treturn:" << frm;
	return frm;
}

int QFReportProcessor::pageCount()
{
	qfLogFuncFrame();
	int ret = 0;
	if(processorOutput()) {
		ret = processorOutput()->childrenCount();
	}
	return ret;
}

