
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFREPORTPAINTER_H
#define QFREPORTPAINTER_H

#include <qfreportprocessor.h>
#include <qfreportitem.h>

#include <qfguiglobal.h>
#include <qfexception.h>
#include <qftreeitembase.h>

#include <QObject>
#include <QPrinter>

class QFReportPainter;

//! Base trida objektu, ktere vzniknou prekladem reportu.
class QFReportItemMetaPaint : public QFTreeItemBase
{
	public:
		enum PaintMode {PaintBorder=1, PaintFill=2, PaintAll=3};
		//! string v reportu, ktery se vymeni za celkovy pocet stranek v reportu.
		static const QString pageCountReportSubstitution;
		static const QRegExp checkReportSubstitutionRegExp;
		static const QString checkReportSubstitution;
		//static const QString checkOffReportSubstitution;
		typedef QFGraphics::Rect Rect;
		typedef QFGraphics::Size Size;
		typedef QFGraphics::Point Point;
		struct LayoutSetting {
			qreal hinset, vinset;
			QFGraphics::Layout layout;
			Qt::Alignment alignment;
			bool suppressPrintOut; ///<  frame a jeho deti se objevi pouze v nahledu

			LayoutSetting() : hinset(0), vinset(0), layout(QFGraphics::LayoutVertical), alignment(Qt::AlignLeft | Qt::AlignTop), suppressPrintOut(false) {}
		};
	public:
		QFReportProcessorItem::Rect renderedRect; ///< rozmery v mm
		QFDomElement reportElement; ///< for designer, to know which of elements was clicked, jinak se nepouziva vubec na nic.
		//QString elementTagName; ///< for debugging only
		QFReportProcessor::Context context;
		LayoutSetting *f_layoutSettings;
	public:
		LayoutSetting *layoutSettings();
		void setInset(qreal horizontal, qreal vertical);
		qreal insetHorizontal() {return (f_layoutSettings)? f_layoutSettings->hinset: 0;}
		qreal insetVertical() {return (f_layoutSettings)? f_layoutSettings->vinset: 0;}
		QFGraphics::Layout layout() const {return (f_layoutSettings)? f_layoutSettings->layout: QFGraphics::LayoutVertical;}
		void setLayout(QFGraphics::Layout ly) {if(layout() != ly) layoutSettings()->layout = ly;}
		Qt::Alignment alignment() const {return (f_layoutSettings)? f_layoutSettings->alignment: Qt::AlignLeft | Qt::AlignTop;}
		void setAlignment(Qt::Alignment al) {if(alignment() != al) layoutSettings()->alignment = al;}

		static QFGraphics::Layout orthogonalLayout(QFGraphics::Layout l) {
			if(l == QFGraphics::LayoutHorizontal) return QFGraphics::LayoutVertical;
			if(l == QFGraphics::LayoutVertical) return QFGraphics::LayoutHorizontal;
			return QFGraphics::LayoutInvalid;
		}
		QFGraphics::Layout orthogonalLayout() const {return orthogonalLayout(layout());}
	public:
		void setRenderedRectRect(const QRectF &new_size) {renderedRect = new_size;}
		
		virtual QFReportItemMetaPaint* parent() const {return static_cast<QFReportItemMetaPaint*>(f_parent);}
		virtual QFReportItemMetaPaint* childAt(int ix) const {return static_cast<QFReportItemMetaPaint*>(children()[ix]);}
		virtual QFReportItemMetaPaint* firstChild() const {
			if(!children().isEmpty()) return static_cast<QFReportItemMetaPaint*>(children().first());
			return NULL;
		}
		virtual QFReportItemMetaPaint* lastChild() const {
			if(! children().isEmpty()) return static_cast<QFReportItemMetaPaint*>(children().last());
			return NULL;
		}

		/*
		static qreal x2dev(qreal x, QPaintDevice *dev) {Q_UNUSED(dev); return x;}
		static qreal y2dev(qreal y, QPaintDevice *dev) {Q_UNUSED(dev); return y;}
		static Rect mm2dev(const Rect &r, QPaintDevice *dev) {Q_UNUSED(dev); return r;}
		static Point mm2dev(const Point &p, QPaintDevice *dev) {Q_UNUSED(dev); return p;}
		*/
		virtual void paint(QFReportPainter *painter, unsigned mode);
		void shift(const QFReportProcessorItem::Point offset)
		{
			renderedRect.translate(offset);
			shiftChildren(offset);
		}
		void shiftChildren(const QFReportProcessorItem::Point offset);

		void alignChildren();

		/// popis funkce popsan u atributu expandChildrenFrames v qfreport.rnc
		void expandChildrenFramesRecursively();

		virtual bool isPointInside(const QPointF &p) {
			return (renderedRect.left() <= p.x() && renderedRect.right() >= p.x()
					&& renderedRect.top() <= p.y() && renderedRect.bottom() >= p.y());
		}

		virtual bool isExpandable() const {return true;}

		virtual QString dump(int indent = 0);
	public:
		QFReportItemMetaPaint();
		//! parametr \a processor v konstruktoru slouzi jenom kvuli scriptovanym atributum elementu, pouzije se jen v konstruktoru, ukazatel na nej se nikde neuklada.
		QFReportItemMetaPaint(QFReportItemMetaPaint *parent, QFReportProcessorItem *report_item);
		virtual ~QFReportItemMetaPaint();
};

//! TODO documentation
class QFReportItemMetaPaintReport : public QFReportItemMetaPaint
{
	public:
		QPrinter::Orientation orientation;
		QSize pageSize;
	public:
		QFReportItemMetaPaintReport(QFReportProcessorItem *report_item);
};

//! TODO documentation
class QFReportItemMetaPaintFrame : public QFReportItemMetaPaint
{
	public:
		enum LinePos {LBrd = 1, RBrd, TBrd, BBrd};
	public:
		QBrush fill;
		QPen lbrd, rbrd, tbrd, bbrd;
	protected:
		virtual void fillItem(QPainter *painter, bool selected = false);
		virtual void frameItem(QPainter *painter, bool selected = false);
		void drawLine(QPainter *painter, LinePos where, const QPen &pen);
	public:
		virtual void paint(QFReportPainter *painter, unsigned mode = PaintAll);
	public:
		QFReportItemMetaPaintFrame(QFReportItemMetaPaint *parent, QFReportProcessorItem *report_item);
		virtual ~QFReportItemMetaPaintFrame() {}
};
/*
//! TODO documentation
class QFReportItemMetaPaintParaFrame : public QFReportItemMetaPaintFrame
{
	public:
		QFReportItemMetaPaintParaFrame(QFReportItemMetaPaint *parent, const QFDomElement &el, const QFReportProcessor::Context &context);
		virtual ~QFReportItemMetaPaintParaFrame() {}
};
*/
//! TODO documentation
class QFReportItemMetaPaintText : public QFReportItemMetaPaint
{
	public:
		QString text;
		QFont font;
		QPen pen; ///< barva vyplne pismen
		//bool renderCheck;
		//QBrush brush;
		int flags;
	public:
		virtual void paint(QFReportPainter *painter, unsigned mode = PaintAll);
		virtual bool isPointInside(const QPointF &p) {Q_UNUSED(p); return false;}

		virtual QString dump(int indent = 0);
	public:
		QFReportItemMetaPaintText(QFReportItemMetaPaint *parent, QFReportProcessorItem *report_item)
	: QFReportItemMetaPaint(parent, report_item), flags(0) {}
		virtual ~QFReportItemMetaPaintText() {}
};

//! TODO documentation
class QFReportItemMetaPaintCheck : public QFReportItemMetaPaintText
{
	public:
		virtual void paint(QFReportPainter *painter, unsigned mode = PaintAll);
		virtual bool isExpandable() const {return false;}
	public:
		QFReportItemMetaPaintCheck(QFReportItemMetaPaint *parent, QFReportProcessorItem *report_item)
			: QFReportItemMetaPaintText(parent, report_item) {}
		//virtual ~QFReportItemMetaPaintCheck() {}
};

//! TODO documentation
class QFReportItemMetaPaintImage : public QFReportItemMetaPaint
{
	public:
		QFReportProcessorItem::Image image;
		Qt::AspectRatioMode aspectRatioMode;
	public:
		virtual void paint(QFReportPainter *painter, unsigned mode = PaintAll);
		virtual bool isPointInside(const QPointF &p) {Q_UNUSED(p); return false;}

		virtual QString dump(int indent = 0);
	public:
		QFReportItemMetaPaintImage(QFReportItemMetaPaint *parent, QFReportProcessorItem *report_item)
			: QFReportItemMetaPaint(parent, report_item), aspectRatioMode(Qt::IgnoreAspectRatio) {}
		//virtual QFReportItemMetaPaintImage() {}
};

//! TODO: write class documentation.
class  QFReportPainter : public QPainter
{
	public:
		QFReportItemMetaPaint *f_selectedItem;
		//QFDomElement selectedElement;
		/// field umoznujici zobrazit pocet stranek reportu, jinak to asi nejde, behem kompilace nevim, kolik jich nakonec bude.
		int pageCount;
	public:
		/*
		virtual QPaintDevice* paintDevice() throw(QFException)
		{
			if(!device()) QF_EXCEPTION("Paint device is NULL");
			return device();
		}
		*/
		QFReportItemMetaPaint* selectedItem() const {return f_selectedItem;}
		void setSelectedItem(QFReportItemMetaPaint *it) {f_selectedItem = it;}
		virtual void drawMetaPaint(QFReportItemMetaPaint *item);
		//virtual void paintPage();
	public:
		QFReportPainter(QPaintDevice *device);
		virtual ~QFReportPainter();
};

#endif // QFREPORTPAINTER_H

