//
// C++ Implementation: qfreportitem_html
//
// Description: 
//
//
// Author: Fanda Vacek <fanda@JedovaChyse>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "qfreportitem.h"
#include "qfreportprocessor.h"
#include "qfreportpainter.h"

//#include <qffileutils.h>
//#include <qfsql.h>
//#include <qfgraph.h>

//#include <QDate>

#include <qflogcust.h>

//===================================================================
//                           QFReportItemFrame
//===================================================================
// QFReportProcessorItem::PrintResult QFReportItemFrame::printHtml(QDomElement & out)
// {
// 	qfLogFuncFrame() << element.tagName() << "id:" << element.attribute("id");
// 	PrintResult res = PrintOk;
// 	if(out.isNull()) return res;
//
// 	qfTrash() << "\tparent html element:" << out.tagName();
// 	qfTrash() << "\tlayout:" << ((layout == LayoutHorizontal)? "horizontal": (layout == LayoutVertical)? "vertical" : "nevim");
// 	//qfTrash() << "\tmetaPaintLayoutLength:" << metaPaintLayoutLength << "metaPaintOrthogonalLayoutLength:" << metaPaintOrthogonalLayoutLength;
// 	updateChildren();
// 	if(children().count() > 0) {
// 		if(children().count() == 1) {
// 			/// jedno dite vyres tak, ze se vubec nevytiskne rodicovsky frame
// 			QFReportProcessorItem *it = childAt(0);
// 			res = it->printHtml(out);
// 		}
// 		else {
// 			QFDomElement el_table = out.ownerDocument().createElement("table");
// 			QFDomElement el_tr;
// 			if(layout == LayoutHorizontal) {
// 				el_tr = out.ownerDocument().createElement("tr");
// 				el_table.appendChild(el_tr);
// 			}
// 			for(int i=0; i<children().count(); i++) {
// 				if(layout == LayoutVertical) {
// 					el_tr = out.ownerDocument().createElement("tr");
// 					el_table.appendChild(el_tr);
// 				}
// 				QFDomElement el_td = out.ownerDocument().createElement("td");
// 				el_tr.appendChild(el_td);
// 				QFReportProcessorItem *it = childAt(i);
// 				PrintResult ch_res;
// 				//int cnt = 0;
// 				do {
// 					//if(cnt++) qfInfo() << "\t opakovacka";
// 					ch_res = it->printHtml(el_td);
// 					qfInfo() << "\t again2:" << (ch_res .flags & FlagPrintAgain);
// 				} while(ch_res.flags & FlagPrintAgain);
// 				res = ch_res;
// 			}
// 			out.appendChild(el_table);
// 		}
// 	}
// 	return res;
// }
QFReportProcessorItem::PrintResult QFReportItemFrame::printHtml(QDomElement & out)
{
	qfLogFuncFrame() << element.tagName() << "id:" << element.attribute("id");
	PrintResult res = PrintOk;
	if(out.isNull()) return res;
	
	qfTrash() << "\tparent html element:" << out.tagName();
	qfTrash() << "\tlayout:" << ((layout == QFGraphics::LayoutHorizontal)? "horizontal": (layout == QFGraphics::LayoutVertical)? "vertical" : "nevim");
	//qfTrash() << "\tmetaPaintLayoutLength:" << metaPaintLayoutLength << "metaPaintOrthogonalLayoutLength:" << metaPaintOrthogonalLayoutLength;
	updateChildren();
	if(children().count() > 0) {
		if(children().count() == 1) {
			/// jedno dite vyres tak, ze se vubec nevytiskne rodicovsky frame
			QFReportProcessorItem *it = childAt(0);
			res = it->printHtml(out);
		}
		else {
			QFDomElement el_div = out.ownerDocument().createElement("div");;
			if(layout == QFGraphics::LayoutHorizontal) {
				el_div.setAttribute("layout", "horizontal");
			}
			for(int i=0; i<children().count(); i++) {
				QFReportProcessorItem *it = childAt(i);
				PrintResult ch_res;
				//int cnt = 0;
				do {
					//if(cnt) qfInfo() << "\t opakovacka:" << cnt;
					ch_res = it->printHtml(el_div);
					//if(cnt) qfInfo() << "\t again2:" << (ch_res .flags & FlagPrintAgain);
					//cnt++;
				} while(ch_res.flags & FlagPrintAgain);
				res = ch_res;
			}
			out.appendChild(el_div);
		}
		QFDomElement el = out.lastChild().toElement();
		if(!!el) {
			QFReportItemTable *tbl_it = dynamic_cast<QFReportItemTable*>(this);
			if(tbl_it) {
				el.setAttribute("__table", "__fakeBandTable");
			}
			else {
				static QStringList sl = QStringList() << "__fakeBandDetail" << "__fakeBandHeaderRow" << "__fakeBandFooterRow";
				foreach(QString s, sl) {
					if(element.attribute(s).toInt() > 0) el.setAttribute("__table", s);
				}
			}
		}
	}
	return res;
}

//===================================================================
//                           QFReportItemDetail
//===================================================================
QFReportProcessorItem::PrintResult QFReportItemDetail::printHtml(QDomElement & out)
{
	qfLogFuncFrame() << element.tagName() << "id:" << element.attribute("id");
	//qfTrash().color(QFLog::Yellow) << "\treturn:" << res.toString();
	//qfInfo() << "design mode:" << design_mode;
	bool design_mode = processor->isDesignMode();
	QFReportItemBand *b = parentBand();
	if(b) {
		qfTrash() << "band:" << b << "\ttable is null:" << b->dataTable().isNull();
		if(!b->dataTable().isNull()) {
			//design_view = false;
			if(f_dataRow.isNull()) {
				/// kdyz neni f_dataRow, vezmi prvni radek dat
				f_dataRow = b->dataTable().firstRow();
				qfTrash() << "\tfirst row is null:" << f_dataRow.isNull();
				f_currentRowNo = 0;
				//qfInfo() << "vezmi prvni radek dat element id:" << element.attribute("id") << "f_currentRowNo:" << f_currentRowNo;
			}
		}
	}
	PrintResult res;
	if(!design_mode && f_dataRow.isNull()) {
		/// prazdnej detail vubec netiskni
		res.value = PrintOk;
		return res;
	}
	res = QFReportItemFrame::printHtml(out);
	if(res.value == PrintOk) {
		if(b) {
			/// vezmi dalsi radek dat
			//qfInfo() << "vezmi dalsi radek dat" << element.attribute("id");
			f_dataRow = b->dataTable().nextRow(f_dataRow);
			f_currentRowNo++;
			//qfInfo() << "vezmi dalsi radek dat element id:" << element.attribute("id") << "f_currentRowNo:" << f_currentRowNo;
			if(!f_dataRow.isNull()) {
				resetIndexToPrintRecursively(QFReportProcessorItem::IncludingParaTexts);
				res.flags |= FlagPrintAgain;
			}
			//else qfInfo() << "\t IS NULL";
		}
	}
	//res = checkPrintResult(res);
	//qfInfo() << "\t again:" << (res .flags & FlagPrintAgain);
	qfTrash().color(QFLog::Yellow) << "\treturn:" << res.toString();
	return res;
}

//===================================================================
//                           QFReportItemPara
//===================================================================
QFReportProcessorItem::PrintResult QFReportItemPara::printHtml(QDomElement & out)
{
	qfLogFuncFrame() << element.tagName() << "id:" << element.attribute("id");
	PrintResult res = PrintOk;
	if(out.isNull()) return res;
	
	QFDomElement el_div = out.ownerDocument().createElement("div");
	QFDomElement el_p = out.ownerDocument().createElement("p");
	QString text = paraText();
	QRegExp rx = QFReportItemMetaPaint::checkReportSubstitutionRegExp;
	if(rx.exactMatch(text)) {
		bool check_on = rx.capturedTexts().value(1) == "1";
		text = (check_on)? "X": QString();
	}
	el_p.setText(text);
	out.appendChild(el_div);
	el_div.appendChild(el_p);
	return res;
}
