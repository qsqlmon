#INCLUDEPATH += $$PWD

HEADERS +=                           \
	$$PWD/qfreportpainter.h      \
	$$PWD/qfreportprocessor.h   \
	$$PWD/qfreportitem.h  \

SOURCES +=                           \
	$$PWD/qfreportpainter.cpp      \
	$$PWD/qfreportprocessor.cpp   \
	$$PWD/qfreportitem.cpp  \
	$$PWD/qfreportitem_html.cpp  \


