
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfreportpainter.h"
#include "qfreportprocessor.h"
#include "qfreportitem.h"

#include <qffileutils.h>
#include <qfsql.h>
#include <qfgraph.h>
#include <qfsqlquerytable.h>

#include <QDate>
#include <QCryptographicHash>
#include <QSvgRenderer>

#include <qflogcust.h>

//==========================================================
//                                    QFReportItem
//==========================================================
/*
QFReportItem::QFReportItem(QFReportItem *_parent, const QFDomElement &el)
	: parent(_parent), element(el)
{
}

QFReportItem::~QFReportItem()
{
	//clearChildren();
}
*/
/*
void QFReportItem::clearChildren()
{
	foreach(QFReportItem *it, children) SAFE_DELETE(it);
}
*/
//==========================================================
//                                    QFReportProcessorItem
//==========================================================
const double QFReportProcessorItem::Epsilon = 1e-10;
const QString QFReportProcessorItem::INFO_IF_NOT_FOUND_DEFAULT_VALUE = "$INFO";

QFReportProcessorItem::QFReportProcessorItem(QFReportProcessor *proc, QFReportProcessorItem *_parent, const QFDomElement &el)
	: QFTreeItemBase(_parent), processor(proc), element(el)//, recentPrintResult(PrintNotPrintedYet)
{
	QF_ASSERT(processor, "Processor can not be NULL.");
	recentlyPrintNotFit = false;
	keepAll = element.attribute("keepall").toBool();
	//if(keepAll) { qfInfo() << "KEEP ALL is true" << element.attribute("keepall"); }
}

QFReportProcessorItem::~QFReportProcessorItem()
{
	qfTrash() << QF_FUNC_NAME << "##################" << element.tagName();
}

bool QFReportProcessorItem::childrenSynced()
{
	qfTrash() << QF_FUNC_NAME<< element.tagName() << "children count:" << children().count();
	bool synced = true;
	int i = 0;
	for(QFDomElement el = element.firstChildElement(); !!el; el = el.nextSiblingElement()) {
		qfTrash() << "\t checking:" << el.tagName() << "i:" << i;
		if(!QFReportProcessor::isProcessible(el)) continue; /// nezname elementy ignoruj
		qfTrash() << "\t processible";
		if(i >= children().count()) {
		 	/// vic znamych elementu nez deti => neco pribylo
			synced = false;
			qfTrash() << "\t more elements";
			break;
		}
		if(el == childAt(i)->element) {
			/// stejny element na stejne posici
			i++;
			continue;
		}
		/// doslo k nejaky zmene nebo deti nejsou dosud vytvoreny
		qfTrash() << "\t other element";
		synced = false;
		break;
	}
	if(i != children().count()) {
		qfTrash() << "\t divny";
		synced = false;
	}
	qfTrash() << "\treturn:" << synced;
	return synced;
}

void QFReportProcessorItem::deleteChildren()
{
	clearChildren();
	/*
	foreach(QObject *o, this->children()) {
		//qfTrash() << "\twant delete object:" << o;
		//qfTrash() << "\tdeleting object:" << o;
		//objectMap.take(o);
		SAFE_DELETE(o);
	}
	*/
}

void QFReportProcessorItem::syncChildren()
{
	qfTrash() << QF_FUNC_NAME << element.tagName() << "id:" << element.attribute("id");
#if 0
	QList<QFTreeItemBase*> lst;
	for(QFDomElement el = element.firstChildElement(); !!el; el = el.nextSiblingElement()) {
		if(!QFReportProcessor::isProcessible(el)) continue;
		int i;
		for(i=0; i<children.count(); i++) {
			if(childAt(i)->element == el) break;
		}
		if(i < children.count()) {
			/// presun vytvoreny item do noveho listu
			lst << childAt(i);
			//qfTrash() << "\tmoved:" << (lst.last()->element.isNull()? "NULL": lst.last()->element.tagName());
			children.removeAt(i);
		}
		else {
			/// vytvor chybejici item
			QFReportProcessorItem *it = processor->createItem(el, NULL);
			it->f_parent = this;
			lst << it;
			//qfTrash() << "\tcreated:" << (lst.last()->element.isNull()? "NULL": lst.last()->element.tagName());
		}
	}
	/// vymaz neaktualni itemy
	clearChildren();
	children = lst;
#else
	deleteChildren();
	for(QFDomElement el = element.firstChildElement(); !!el; el = el.nextSiblingElement()) {
		if(el.tagName() == "script") {
			QString code;
			for(QDomNode nd = el.firstChild(); !nd.isNull(); nd = nd.nextSibling()) {
				if(nd.isCDATASection()) {
					code = nd.toCDATASection().data();
				}
			}
			if(!code.isEmpty()) processor->scriptDriver()->evaluate(code);
			continue;
		}
		if(!QFReportProcessor::isProcessible(el)) continue;
		/// vytvor chybejici item
		processor->createProcessibleItem(el, this);
	}
#endif
	//qfTrash() << QF_FUNC_NAME << "<<<<<<<<<<<<<<<< OUT";
}

QString QFReportProcessorItem::elementAttribute(const QString & attr_name, const QString &default_val)
{
	QFString ret = element.attribute(attr_name, default_val);
	static QRegExp rx("script:([A-Za-z]\\S*)\\((.*)\\)");
	if(rx.exactMatch(ret)) {
		QF_ASSERT(processor, "Processor is NULL.");
		QString fn = rx.cap(1);
		ret = rx.cap(2);
		QStringList sl = ret.splitAndTrim(',', '\'');
		QVariantList vl;
		foreach(QString s, sl) vl << s;
		QScriptValue sv = processor->scriptDriver()->call(this, fn, vl);
		ret = sv.toString();
	}
	return ret;
}

QFReportItemBand* QFReportProcessorItem::parentBand()
{
	QFReportProcessorItem *it = this->parent();
	while(it) {
		if(it->toBand()) return it->toBand();
		it = it->parent();
	}
	return NULL;
}

QFReportItemDetail* QFReportProcessorItem::currentDetail()
{
	QFReportProcessorItem *it = const_cast<QFReportProcessorItem *>(this);
	while(it) {
		if(it->toDetail()) return it->toDetail();
		it = it->parent();
	}
	return NULL;
}

QFXmlTable QFReportProcessorItem::findDataTable(const QString &name) 
{
	qfLogFuncFrame();
	QFXmlTable ret;
	const QFReportItemDetail *d = currentDetail();
	qfTrash() << "\tparent:" << parent() << "parent detail:" << d;
	if(d) {
		qfTrash() << "\tdata row is null:" << d->dataRow().isNull();
		if(d->dataRow().isNull() && !processor->isDesignMode()) qfWarning().noSpace() << "'" << name << "' parent detail datarow is NULL";
		ret = d->dataRow().firstChildTable(name);
		/// pokud ji nenajde a name neni specifikovano, vezmi 1. tabulku
		if(ret.isNull() && name.isEmpty()) ret = d->dataRow().firstChildTable();
		qfTrash() << "\ttable name:" << name << "is null:" << ret.isNull();
		//qfInfo() << name << ret.element().toString();
	}
	return ret;
}

QFReportProcessorItem::PrintResult QFReportProcessorItem::checkPrintResult(QFReportProcessorItem::PrintResult res)
{
	PrintResult ret = res;
	//if(res.value == PrintNotFit) {
	//qfWarning().noSpace() << "PrintNotFit element: '" << element.tagName() << "' id: '" << element.attribute("id") << "' recentlyPrintNotFit: " << recentlyPrintNotFit << " keepall: " << keepAll;
	//}
	if(keepAll && recentlyPrintNotFit && res.value == PrintNotFit) {
		//qfWarning().noSpace() << "PrintNeverFit element: '" << element.tagName() << "' id: '" << element.attribute("id") << "'";
		ret.flags |= FlagPrintNeverFit;
	}
	recentlyPrintNotFit = (ret.value == PrintNotFit);
	return ret;
}

QVariant QFReportProcessorItem::concatenateNodeChildrenValues(const QDomNode & nd) 
{
	QVariant ret;
	QDomElement el = nd.toElement();
	for(QFDomNode nd1 = el.firstChild(); !!nd1; nd1 = nd1.nextSibling()) {
		QVariant v1 = nodeValue(nd1);
		if(0) {
			QFDomElement eel = nd1.toElement();
			qfInfo() << "node value:" << eel.toString() << v1.toString();
		}
		if(!ret.isValid()) ret = v1;
		else ret = ret.toString() + v1.toString();
	}
	return ret;
}

QString QFReportProcessorItem::nodeText(const QDomNode &nd) 
{
	QVariant v = nodeValue(nd);
	if(v.canConvert<QFXmlTableDocument>()) {
		/// jedna se o XML tabulku, ktera je vysledkem SQL dotazu, vezmi z ni pouze 1. hodnotu na 1. radku
		QFXmlTableDocument doc = v.value<QFXmlTableDocument>();
		QFXmlTable xt(doc, QString());
		v = xt.firstRow().value(0);
	}
	QString ret;
	if(nd.isElement()) {
		do {
			QFDomElement el = nd.toElement();
			bool hide_null = el.attribute("hidenull", "true").toBool();
			if(!v.isValid() && !hide_null) {
				ret = "{null}";
				break;
			}
			bool hide_zero = el.attribute("hidezero").toBool();
			if(hide_zero) {
				if(v.type() == QVariant::Int && v.toInt() == 0) {
					break;
				}
				else if(v.type() == QVariant::Double && v.toDouble() == 0) {
					break;
				}
			}
			QString format = el.attribute("format");
			if(v.type() == QVariant::Date) {
				//qfInfo() << "Date format:" << format;
				if(format.isNull()) ret = v.toDate().toString(Qf::defaultDateFormat());
				else ret = v.toDate().toString(format);
			}
			else if(v.type() == QVariant::Time) {
				if(format.isNull()) ret = v.toString();
				else ret = v.toTime().toString(format);
			}
			else if(v.type() == QVariant::DateTime) {
				//qfInfo() << v.toString();
				if(format.isNull()) ret = v.toDateTime().toString(Qf::defaultDateTimeFormat());
				else ret = v.toTime().toString(format);
			}
			else if(v.type() == QVariant::Double) {
				if(!format.isNull()) ret = QFString::number(v.toDouble(), format);
				else ret = v.toString();
			}
			else if(v.type() == QVariant::Int) {
				if(!format.isNull()) ret = QFString::number(v.toInt(), format);
				else ret = v.toString();
			}
			else {
				if(format == "check") {
					bool b = v.toBool();
					QString s = QFReportItemMetaPaint::checkReportSubstitution;
					s.replace("${STATE}", (b)? "1": "0");
					ret = s;
				}
				else ret = v.toString();
			}
		} while(false);
	}
	else ret = v.toString();
	return ret;
}

QVariant QFReportProcessorItem::nodeValue(const QDomNode &nd) 
{
	qfTrash().color(QFLog::Cyan) << QF_FUNC_NAME;
	static const QString S_ATTR_DOMAIN = "domain";
	static const QString S_EL_DATA = "data";
	QVariant ret;
	qfTrash() << "\tnode type:" << nd.nodeType();
	if(nd.isText()) {
		QString s = nd.toText().data();
		qfTrash().noSpace() << "\t\ttext: '" << s << "'";
		ret = s;
	}
	else if(nd.isElement()) {
		QFDomElement el = nd.toElement();
		if(el.tagName() == S_EL_DATA) {
			bool sql_match = el.attribute("sqlmatch", "true").toBool();
			QString default_value = el.attribute("defaultValue", QFReportProcessorItem::INFO_IF_NOT_FOUND_DEFAULT_VALUE);
			QString cast = el.attribute("cast");
			QString data_src = el.attribute("src").trimmed();
			qfTrash().noSpace() << "\t\tdata: '" << data_src << "'";
			QString domain = el.attribute(S_ATTR_DOMAIN, "row");
			QVariantList params;
			if(domain == "script") {
				for(QFDomElement el_param = el.firstChildElement("param"); !!el_param; el_param = el_param.nextSiblingElement("param")) {
					//qfInfo() << concatenateNodeChildrenValues(el_param).toString();
					params << concatenateNodeChildrenValues(el_param);
				}
			}
			else if(domain == "sql" || domain == "scriptcode") {
				QString code;
				for(QDomNode nd = el.firstChildElement("code").firstChild(); !nd.isNull(); nd = nd.nextSibling()) {
					if(nd.isCDATASection()) {
						code = nd.toCDATASection().data();
						break;
					}
				}
				if(!code.isEmpty()) {
					QVariantMap params;
					for(QFDomElement el_param = el.firstChildElement("param"); !!el_param; el_param = el_param.nextSiblingElement("param")) {
						QString param_name = el_param.attribute("name");
						if(!param_name.isEmpty()) {
							params[param_name] = concatenateNodeChildrenValues(el_param);
						}
					}
					QMapIterator<QString, QVariant> i(params);
					while(i.hasNext()) {
						i.next();
						code.replace("${" + i.key() + '}', i.value().toString());
					}
					data_src = code;
				}
			}
			QVariant data_value = value(data_src, domain, params, default_value, sql_match);
			if(!cast.isEmpty()) {
				if(cast == "double") {
					data_value = Qf::retypeVariant(data_value, QVariant::Double);
				}
				else if(cast == "int") {
					data_value = Qf::retypeVariant(data_value, QVariant::Int);
				}
				else if(cast == "date") {
					//qfInfo() << "casting date" << data_value.toString();
					data_value = Qf::retypeVariant(data_value, QVariant::Date);
					//qfInfo() << "to" << data_value.toString();
				}
				else if(cast == "time") {
					data_value = Qf::retypeVariant(data_value, QVariant::Time);
				}
			}
			ret = data_value;
		}
		else {
			qfWarning() << "unprocessible element:" << el.tagName();
		}
	}
	qfTrash().color(QFLog::Cyan) << "\treturn:" << ret.toString() << QVariant::typeToName(ret.type());
	return ret;
}

QVariant QFReportProcessorItem::value(const QString &data_src, const QString & domain, const QVariantList &params, const QVariant &default_value, bool sql_match) 
{
	//qfInfo() << "data_src:" << data_src << "domain:" << domain;
	qfTrash() << QF_FUNC_NAME << "data_src:" << data_src << "domain:" << domain << "sql_match:" << sql_match;
	static const QString S_DOMAIN_SYSTEM = "system";
	static const QString S_DOMAIN_REPORT = "report";
	static const QString S_DOMAIN_ROW = "row";
	static const QString S_DOMAIN_TABLE = "table";
	static const QString S_DOMAIN_SCRIPT = "script";
	static const QString S_DOMAIN_SCRIPT_CODE = "scriptcode";
	static const QString S_DOMAIN_SQL = "sql";
	static const QString S_SYSTEM_DATE = "date";
	static const QString S_SYSTEM_TIME = "time";
	static const QString S_TABLE_ROWNO = "ROW_NO()";
	QVariant data_value = default_value;
	bool info_if_not_found = (default_value == QFReportProcessorItem::INFO_IF_NOT_FOUND_DEFAULT_VALUE);
	if(domain == S_DOMAIN_SYSTEM) {
		if(data_src == S_SYSTEM_DATE) {
			data_value = QDate::currentDate();//.toString(date_format);
		}
		else if(data_src == S_SYSTEM_TIME) {
			data_value = QTime::currentTime();
		}
		else if(data_src == "page") {
			data_value = QString::number(processor->processedPageNo() + 1);
		}
		else if(data_src == "pageCount") {
			data_value = QFReportItemMetaPaint::pageCountReportSubstitution; /// takovyhle blby zkratky mam proto, aby to zabralo zhruba stejne mista jako cislo, za ktery se to vymeni
		}
	}
	else if(domain == S_DOMAIN_SCRIPT) {
		try {
			data_value = QFScriptDriver::scriptValueToVariant(processor->scriptDriver()->call(this, data_src, params));
		}
		catch(QFException &e) {
			qfError() << "Report table data load error:" << e.msg();
		}
	}
	else if(domain == S_DOMAIN_SCRIPT_CODE) {
		try {
			data_value = QFScriptDriver::scriptValueToVariant(processor->scriptDriver()->evaluate(data_src));
		}
		catch(QFException &e) {
			qfError() << "Report table data load error:" << e.msg();
		}
	}
	else if(domain == S_DOMAIN_SQL) {
		//qfInfo() << "element:" << element.toString();
		if(!processor->isDesignMode()) {
			QString qs = data_src;
			//qfInfo() << "qs:" << qs;
			if(!qs.isEmpty()) try {
				QFSqlQueryTable t;
				t.reload(qs);
				QFXmlTableDocument doc;
				QFXmlTable xt = t.toXmlTable(doc);
				doc.appendChild(xt.element());
				//qfInfo() << doc.toString();
				data_value.setValue(doc);
			}
			catch(QFException &e) {
				qfError() << "Report table data load error:" << e.msg();
			}
		}
	}
	else if(domain == S_DOMAIN_REPORT) {
		QFString path = QFFileUtils::path(data_src);
		QString key = QFFileUtils::file(data_src);
		//qfTrash().noSpace() << "\t\tpath: '" << path << "'" << "\t\tname: '" << data << "'";
		QFDomElement el = element.cd(path + "keyvals", !Qf::ThrowExc);
		if(!!el) {
			QFXmlKeyVals kv(el);
			data_value = kv.value(key);
		}
		else {
			qfWarning() << QString("Report path '%1' does not exist. Domain: %2 Element path: '%3'").arg(path + "keyvals").arg(domain).arg(element.path());
		}
	}
	else if(domain == S_DOMAIN_TABLE) {
		QFReportItemBand *band = parentBand();
		if(!band) qfWarning().noSpace() << "'" << data_src << "' band is null";
		if(band) {
			QFXmlTable t = band->dataTable();
			if(t.isNull()) {
				if(info_if_not_found) data_value = '{' + data_src + '}';//qfWarning().noSpace() << "'" << data_src << "' table is null";
			}
			else data_value = t.value(data_src, (info_if_not_found)? "$" + data_src: default_value, sql_match);
		}
	}
	else if(domain == S_DOMAIN_ROW) {
		QFReportItemDetail *det = currentDetail();
		if(det) {
			QFXmlTableRow r = det->dataRow();
			qfTrash() << "\t\tdata row is null:" << r.isNull();
			if(r.isNull()) {
				if(info_if_not_found) data_value = '[' + data_src + ']';
			}
			else {
				if(data_src == S_TABLE_ROWNO) {
					data_value = det->currentRowNo() + 1;
				}
				else {
					data_value = r.value(data_src, (info_if_not_found)? "$" + data_src: default_value);
				}
			}
		}
		else {
			if(info_if_not_found) data_value = "$" + data_src + " no detail";
		}
	}
	qfTrash() << "\treturn:" << data_value.toString() << QVariant::typeToName(data_value.type());
	//qfInfo() << "\treturn:" << data_value.toString() << QVariant::typeToName(data_value.type());
	return data_value;
}

QFReportItemMetaPaint * QFReportProcessorItem::createMetaPaintItem(QFReportItemMetaPaint * parent)
{
	QFReportItemMetaPaint *ret = NULL;
	ret = new QFReportItemMetaPaintFrame(parent, this);
	return ret;
}

QString QFReportProcessorItem::toString(int indent, int indent_offset)
{
	QString ret;
	QString indent_str;
	indent_str.fill(' ', indent_offset);
	ret += indent_str + element.tagName();
	for(int i=0; i<children().count(); i++) {
		ret += '\n';
		QFReportProcessorItem *it = childAt(i);
		ret += it->toString(indent, indent_offset += indent);
	}
	return ret;
}

//==========================================================
//                                    QFReportItemBreak
//==========================================================
QFReportItemBreak::QFReportItemBreak(QFReportProcessor *proc, QFReportProcessorItem *parent, const QFDomElement &_el)
	: QFReportProcessorItem(proc, parent, _el)
{
	QF_ASSERT(proc, "processor is NULL");
	designedRect.verticalUnit = Rect::UnitInvalid;
	breaking = false;
}

QFReportProcessorItem::PrintResult QFReportItemBreak::printMetaPaint(QFReportItemMetaPaint *out, const QFReportProcessorItem::Rect &bounding_rect )
{
	qfTrash() << QF_FUNC_NAME << element.tagName();
	Q_UNUSED(bounding_rect);
	Q_UNUSED(out);
	PrintResult res = PrintOk;
	if(!breaking) res = PrintNotFit;
	breaking = !breaking;
	return res;
}

//==========================================================
//                                    QFReportItemFrame
//==========================================================
QFReportItemFrame::QFReportItemFrame(QFReportProcessor *proc, QFReportProcessorItem *parent, const QFDomElement &el)
	: QFReportProcessorItem(proc, parent, el)
{
	indexToPrint = 0;
	qfTrash() << QF_FUNC_NAME << "*******************" << el.tagName() << el.attribute("id");
	QFString s;
	Point p;
	s = element.attribute("x1");
	if(!!s) {
		designedRect.flags |= Rect::LeftFixed;
		p.rx() = s.toDouble();
	}
	s = element.attribute("y1");
	if(!!s) {
		designedRect.flags |= Rect::TopFixed;
		p.ry() = s.toDouble();
	}
	designedRect.setTopLeft(p);
	s = element.attribute("x2");
	if(!!s) {
		designedRect.flags |= Rect::RightFixed;
		p.rx() = s.toDouble();
	}
	s = element.attribute("y2");
	if(!!s) {
		designedRect.flags |= Rect::BottomFixed;
		p.ry() = s.toDouble();
	}
	designedRect.setBottomRight(p);
	//qfTrash() << "\t" << __LINE__ << "designedRect:" << designedRect.toString();
	//static const QString S_PERCENT = "%";
	s = element.attribute("w").trimmed();
	if(s == "%%" && parentLayout() == QFGraphics::LayoutHorizontal) { element.setAttribute("filllayout", "1"); }
	else {
		if(s[-1] == '%') {
			s = s.slice(0, -1);
		//if(!s) s = 100;
			designedRect.horizontalUnit = Rect::UnitPercent;
		}
		qreal d = s.toDouble();
		if(d > 0) {
			if(designedRect.flags & Rect::RightFixed) {
				qreal r = designedRect.right();
				designedRect.setWidth(d);
				designedRect.moveRight(r);
			}
			else designedRect.setWidth(d);
		}
	}

	s = element.attribute("h").trimmed();
	if(s == "%%" && parentLayout() == QFGraphics::LayoutVertical) { element.setAttribute("filllayout", "1"); }
	else {
		if(s[-1] == '%') {
			s = s.slice(0, -1);
			designedRect.verticalUnit = Rect::UnitPercent;
		}
		qreal d = s.toDouble();
		if(d > 0) {
			if(designedRect.flags & Rect::BottomFixed) {
				qreal b = designedRect.bottom();
				designedRect.setWidth(d);
				designedRect.moveBottom(b);
			}
			else designedRect.setHeight(d);
		}
	}

	s = element.attribute("filllayout").trimmed();
	if(s.toBool()) {
		designedRect.flags |= Rect::FillLayout;
		if(parentLayout() == QFGraphics::LayoutHorizontal) {designedRect.horizontalUnit = Rect::UnitMM; designedRect.setWidth(0);}
		else if(parentLayout() == QFGraphics::LayoutVertical) {designedRect.verticalUnit = Rect::UnitMM; designedRect.setHeight(0);}
	}

	s = element.attribute("expandChildrenFrames").trimmed();
	if(s.toBool()) {
		//qfInfo() << "element:" << element.tagName();
		designedRect.flags |= Rect::ExpandChildrenFrames;
	}

	layout = (element.attribute("layout", "vertical") == "horizontal")? QFGraphics::LayoutHorizontal: QFGraphics::LayoutVertical;
	if(layout == QFGraphics::LayoutHorizontal) designedRect.flags |= Rect::LayoutHorizontalFlag;
	else designedRect.flags |= Rect::LayoutVerticalFlag;

	hinset = vinset = 0;
	s = element.attribute("inset");
	if(!!s) hinset = vinset = s.toDouble();
	s = element.attribute("hinset");
	if(!!s) hinset = s.toDouble();
	s = element.attribute("vinset");
	if(!!s) vinset = s.toDouble();

	alignment = 0;
	s = element.attribute("halign", "left");
	if(s == "left") alignment |= Qt::AlignLeft;
	else if(s == "center") alignment |= Qt::AlignHCenter;
	else if(s == "right") alignment |= Qt::AlignRight;
	s = element.attribute("valign", "top");
	if(s == "top") alignment |= Qt::AlignTop;
	else if(s == "center") alignment |= Qt::AlignVCenter;
	else if(s == "bottom") alignment |= Qt::AlignBottom;

	//multipage = element.attribute("multipage", "0").toBool();

	qfTrash() << "\tdesignedRect:" << designedRect.toString();
	//if(!isLeftTopFloating()) designedRect.moveTopLeft(p1);
	//if(!isRightBottomFloating()) designedRect.moveBottomRight(p2);
	//qfTrash() << "\tdesignedRect:" << designedRect.toString();
}

QFReportProcessorItem::ChildSize QFReportItemFrame::childSize(QFGraphics::Layout parent_layout)
{
	if(parent_layout == QFGraphics::LayoutHorizontal) return ChildSize(designedRect.width(), designedRect.horizontalUnit);
	return ChildSize(designedRect.height(), designedRect.verticalUnit);
}

QFReportProcessorItem::PrintResult QFReportItemFrame::printMetaPaintChildren(QFReportItemMetaPaint *out, const QFReportProcessorItem::Rect &bounding_rect)
{
	qfTrash().color(QFLog::Green) << QF_FUNC_NAME << element.tagName() << "id:" << element.attribute("id") << "childrencount:" << children().count() << "indexToPrint:" << indexToPrint;
	qfTrash() << "\tbounding_rect:" << bounding_rect.toString();
	PrintResult res = PrintOk;
	//dirtySize = Size();
	Rect bbr = bounding_rect;
	//Size children_dirty_size;
	//Size children_bounding_size = bbr.size();
	QList<ChildSize> sizes;
	QList<ChildSize> orthogonal_sizes;
	/// horizontalni layout se tiskne bud stejne, jako vertikalni (v pripade, ze lze tisknout deti v poradi, jak jsou v reportu)
	/// nebo prehazene, pokud se nedaji procenta rozpocitat predem.
	/// tato informace je ulozena v promenne can_print_children_in_natural_order

	/// nastav detem mm rozmer ve smeru layoutu
	//bool only_last_child_has_0_percent = false;
	int cnt_percent = 0;
	int cnt_0_percent = 0;
	int cnt_0_mm = 0;
	for(int i=indexToPrint; i<childrenCount(); i++) {
		QFReportProcessorItem *it = childAt(i);
		sizes << it->childSize(layout);
		/// break funguje tak, ze pri 1., 3., 5. atd. tisku vraci PrintNotFit a pri sudych PrintOk
		/// prvni break na strance znamena, ze jsem tu uz po zalomeni, takze se tiskne to za break.
		/// v horizontalnim layoutu break ignoruj
		if(it->isBreak() && i > indexToPrint && layout == QFGraphics::LayoutVertical) break;
	}
	int last_rubber_ix = -1;
	int first_percent_ix = -1;
	for(int i=0; i<sizes.count(); i++) {
		const ChildSize &sz = sizes[i];
		//only_last_child_has_0_percent = false;
		if(sz.unit == Rect::UnitPercent) {
			if(first_percent_ix < 0) first_percent_ix = i;
			cnt_percent++;
			if(sz.size == 0) {
				cnt_0_percent++;
				//only_last_child_has_0_percent = true;
			}
		}
		else if(sz.unit == Rect::UnitMM) {
			if(sz.size == 0) {
				last_rubber_ix = i;
				cnt_0_mm++;
			}
		}
	}
	//only_last_child_has_0_percent = (only_last_child_has_0_percent && cnt_percent == 1);

	/// can_print_children_in_natural_order je true v pripade, ze procenta nejsou nebo 1. procenta jsou za poslednim rubber frame.
	bool can_print_children_in_natural_order = first_percent_ix < 0 || last_rubber_ix < 0 || last_rubber_ix < first_percent_ix;

	if(layout == QFGraphics::LayoutVertical) {
		/// ve vertikalnim layoutu nejdou michat % a rubber children frames, kvuli preteceni stranky (v takovem pripade se musi tisknout v prehazenem poradi) => rubbers predelej na %
		/// predelej vsechny rubber children za prvnim vyskytem % na %
			// pokud je parent frame rubber zpusobi % jeho raztazeni na cely bounding rect
		if(!can_print_children_in_natural_order) {
			for(int i=first_percent_ix; i<sizes.count(); i++) {
				ChildSize &sz = sizes[i];
				if(sz.size == 0 && sz.unit == Rect::UnitMM) {
					if(layout == QFGraphics::LayoutVertical) childAt(indexToPrint + i)->designedRect.verticalUnit = Rect::UnitPercent;
					else if(layout == QFGraphics::LayoutHorizontal) childAt(indexToPrint + i)->designedRect.horizontalUnit = Rect::UnitPercent;
					sz.unit = Rect::UnitPercent;
				}
			}
		}
	}

	if(layout == QFGraphics::LayoutHorizontal && !can_print_children_in_natural_order) {
		/// pri tisku 2. metodou se tiskne vzdu od zacatku
		indexToPrint = 0;
	}
	/// zbyva vypocitat jeste ortogonalni rozmer
	/// je to bud absolutni hodnota nebo % z bbr
	for(int i=indexToPrint; i<children().count(); i++) {
		QFReportProcessorItem *it = childAt(i);
		QFGraphics::Layout ol = orthogonalLayout();
		ChildSize sz = it->childSize(ol);
		if(sz.unit == Rect::UnitPercent) {
			if(sz.size == 0) sz.size = bbr.sizeInLayout(ol);
			else sz.size = sz.size / 100 * bbr.sizeInLayout(ol);
		}
		orthogonal_sizes << sz;
			//it->metaPaintOrthogonalLayoutLength = sz.size;
		qfTrash() << "\tsetting orthogonal length:" << sz.size;
		if(it->isBreak() && i > indexToPrint && layout == QFGraphics::LayoutVertical) break; /// v horizontalnim layoutu break ignoruj
	}

	if(layout == QFGraphics::LayoutHorizontal && !can_print_children_in_natural_order) {
		/// tisk 2. metodou

		/// to se muze stat jen v horizontalnim layoutu, kdyz nejde sirky napocitat dopredu.
		/// v horizontalnim layoutu vytiskni nejdriv fixed itemy, pak rubber, ze zbytku rozpocitej % a vytiskni je taky
		/// vytiskly itemy pak rozsoupej do spravnyho poradi

		QList<int> poradi_tisku;
		qreal sum_mm = 0;
		/// vytiskni rubber a fixed
		for(int i=0; i<children().count(); i++) {
			QFReportProcessorItem *it = childAt(i);
			ChildSize sz = it->childSize(layout);
			//qfInfo() << "child:" << i << "size:" << sz.size << "unit:" << Rect::unitToString(sz.unit); 
			if(sz.unit == Rect::UnitMM) {
				Rect ch_bbr = bbr;
				//qfInfo() << "\t i:" << i << " fixed or rubber bbr";
				if(sz.size > 0) ch_bbr.setWidth(sz.size);
				else ch_bbr.setWidth(ch_bbr.width() - sum_mm);
				if(orthogonal_sizes[i].size > 0) {
					ch_bbr.setSizeInLayout(orthogonal_sizes[i].size, orthogonalLayout());
				}
				//qfInfo() << "\t tisknu fixed:" << it->designedRect.toString();
				PrintResult ch_res = it->printMetaPaint(out, ch_bbr);
				if(ch_res.value == PrintOk) {
					//qfInfo() << "\t OK";
					if(out->children().count() > 0) {
						poradi_tisku << i;
						sum_mm += out->lastChild()->renderedRect.width();
						//qfInfo() << "\t poradi tisku <<" << i;
						//qfInfo() << "\t renderedRect:" << out->lastChild()->renderedRect.width();
						//qfInfo() << "\t sum_mm:" << sum_mm;
					}
					else { qfWarning() << "jak to, ze se dite nevytisklo v horizontalnim layoutu? ze by h=% v parentu, ktery nema h specifikovano?"; }
				}
				else {
					qfInfo() << "\t NOT OK";
					res = ch_res;
					break;
				}
			}
		}
		qreal rest_mm = bounding_rect.width() - sum_mm;

		if(res.value == PrintOk) {
			/// rozpocitej procenta
			qreal sum_percent = 0;
			int cnt_0_percent = 0;
			bool has_percent = false;
			for(int i=0; i<children().count(); i++) {
				QFReportProcessorItem *it = childAt(i);
				ChildSize sz = it->childSize(layout);
				if(sz.unit == Rect::UnitPercent) {
					has_percent = true;
					if(sz.size == 0) cnt_0_percent++;
					else sum_percent += sz.size;
				}
			}
			if(has_percent) {
				if(rest_mm <= 0) {
					qfWarning() << "Percent exist but rest_mm is" << rest_mm << ". Ignoring rest of frames";
				}
				else {
					/// vytiskni procenta
					qreal percent_0 = 0;
					if(cnt_0_percent > 0) percent_0 = (100 - sum_percent) / cnt_0_percent;
					for(int i=0; i<children().count(); i++) {
						QFReportProcessorItem *it = childAt(i);
						ChildSize sz = it->childSize(layout);
						if(sz.unit == Rect::UnitPercent) {
							qreal d;
							if(sz.size == 0) d = rest_mm * percent_0 / 100;
							else d = rest_mm * sz.size / 100;
							Rect ch_bbr = bbr;
							ch_bbr.setWidth(d);
							if(orthogonal_sizes[i].size > 0) {
								ch_bbr.setSizeInLayout(orthogonal_sizes[i].size, orthogonalLayout());
							}
							//qfInfo() << "tisknu percent" << it->designedRect.toString();
							PrintResult ch_res = it->printMetaPaint(out, ch_bbr);
							if(ch_res.value == PrintOk) {
								poradi_tisku << i;
							}
							else { res = ch_res; break; }
						}
					}

					/// posprehazej vytisknuty deti
					if(poradi_tisku.count() == out->children().count()) {
						//QF_ASSERT(poradi_tisku.count() == out->children().count(), "nevytiskly se vsechny deti v horizontalnim layoutu");
						QVector<QFTreeItemBase*> old_children(poradi_tisku.count());
						/// zkopiruj ukazatele na deti
						for(int i=0; i<poradi_tisku.count(); i++) old_children[i] = out->children()[i];
						/// dej je do spravnyho poradi
						for(int i=0; i<poradi_tisku.count(); i++) out->childrenRef()[poradi_tisku[i]] = old_children[i];
						/// nastav jim spravne offsety
						qreal offset_x = 0;
						for(int i=0; i<poradi_tisku.count(); i++) {
							QFReportItemMetaPaint *it = out->childAt(i);
							/// tady je to potreba posunout vcetne deti :(
							it->shift(Point(offset_x, 0));
							offset_x += it->renderedRect.width();
						}
					}
				}
			}
		}
		if(res.value != PrintOk) {
			resetIndexToPrintRecursively(!QFReportProcessorItem::IncludingParaTexts);
		}
	}
	else {
		/// can_print_children_in_natural_order

		/// rozpocitej procenta
		qreal sum_percent = 0;
		cnt_0_percent = 0;
		qreal sum_mm = 0;
		for(int i=0; i<sizes.count(); i++) {
			ChildSize &sz = sizes[i];
			if(sz.unit == Rect::UnitMM) sum_mm += sz.size;
			else if (sz.unit == Rect::UnitPercent) {
				if(sz.size == 0) cnt_0_percent++;
				else sum_percent += sz.size;
			}
		}
		qreal rest_percent = 100 - sum_percent;
		if(rest_percent < 0) rest_percent = 0;
		qreal percent_0 = 0;
		if(cnt_0_percent > 0) percent_0 = rest_percent / cnt_0_percent;
		/// tiskni
		qreal length_mm = bbr.sizeInLayout(layout);
		int index_to_print_0 = indexToPrint;
		for(; indexToPrint<children().count(); indexToPrint++) {
			QFReportProcessorItem *it = childAt(indexToPrint);
			Rect ch_bbr = bbr;
			bool item_is_rubber_in_layout = false;
			qfTrash() << "\tch_bbr v1:" << ch_bbr.toString();

			/// vymysli rozmer ve smeru layoutu
			qreal d = ch_bbr.sizeInLayout(layout);
			//qfInfo() << "indexToPrint:" << indexToPrint << "index_to_print_0:" << index_to_print_0 << "sizes.count():" << sizes.count();
			ChildSize &sz = sizes[indexToPrint - index_to_print_0];
			if(sz.unit == Rect::UnitMM) {
				if(sz.size > 0) d = sz.size;
				else item_is_rubber_in_layout = true;
			}
			else if(sz.unit == Rect::UnitPercent) {
				qreal p = sz.size;
				if(p == 0) p = percent_0;
				qreal rest_mm = length_mm - sum_mm;
				if(rest_mm < 0) rest_mm = 0;
				d = p * rest_mm / 100.;
			}
			d = qMin(ch_bbr.sizeInLayout(layout), d);
			ch_bbr.setSizeInLayout(d, layout);
			if(orthogonal_sizes[indexToPrint - index_to_print_0].size > 0) {
				ch_bbr.setSizeInLayout(orthogonal_sizes[indexToPrint - index_to_print_0].size, orthogonalLayout());
			}

			qfTrash() << "\tch_bbr v2:" << ch_bbr.toString();
			PrintResult ch_res = it->printMetaPaint(out, ch_bbr);
			if(ch_res.value == PrintOk) {
				//qfTrash() << "\t" << __LINE__ << "children_dirty_size:" << children_dirty_size.toString();
				//dirtyRect = dirtyRect.unite(it->dirtyRect);
				//qfTrash() << "\t" << __LINE__ << "children_dirty_size:" << children_dirty_size.toString();
				/// muze se stat, ze se dite nevytiskne, napriklad band nema zadna data
				//QF_ASSERT(out->children().count() > 0, "jak to, ze se dite nevytisklo?");
				if(out->children().count() > 0) {
					const Rect &r = out->lastChild()->renderedRect;
					qfTrash() << "\tr:" << r.toString() << "ch_res:" << ch_res.toString();
					bbr.cutSizeInLayout(r, layout);
					if(item_is_rubber_in_layout) {
						/// pricti vyrendrovany rozmer k sum_mm, az se zacnou rozpocitavat % bude to potreba.
						/// je zaruceno, ze prvni % prijdou az po poslednim rubber frame.
						sum_mm += r.sizeInLayout(layout);
					}
					if(ch_res.flags & FlagPrintAgain) {
						indexToPrint--; /// vytiskni ho znovu
					}
				}
				//bbr.cutSizeInLayout(it->dirtyRect, layout);
				//qfTrash() << "\t" << __LINE__ << "children_dirty_size:" << children_dirty_size.toString();
			}
			else {
				/// pokud je vertikalni layout, a dite se nevejde vrat PrintNotFit
				if(layout == QFGraphics::LayoutHorizontal) {
					/// v horizontalnim, zadne pretikani neni
					/// vytiskni to znovu s doteklymi texty
					resetIndexToPrintRecursively(!QFReportProcessorItem::IncludingParaTexts);
				}
				/*
				else {
					if(ch_res.flags & FlagNotFirstDetailNotFit) {
						qfInfo() << "nevesel se detail";
						/// nevesel se detail, ale nebyl to prvni detail, takze je to OK
						indexToPrint--; /// vytiskni ho znovu
						ch_res.flags |= FlagPrintAgain;
						//ch_res.value = PrintOk;
					}
				}
				*/
				res = ch_res;
				break;
			}
			if(it->isBreak() && indexToPrint > index_to_print_0 && layout == QFGraphics::LayoutVertical) break;
		}
	}
	//res = checkPrintResult(res);
	qfTrash().color(QFLog::Green) << "\t<<< CHILDREN return:" << res.toString();
	return res;
}

QFReportProcessorItem::PrintResult QFReportItemFrame::printMetaPaint(QFReportItemMetaPaint *out, const QFReportProcessorItem::Rect &bounding_rect)
{
	qfTrash().color(QFLog::Cyan) << QF_FUNC_NAME << element.tagName() << "id:" << element.attribute("id");
	//qfInfo() << element.tagName() << "id:" << element.attribute("id") << "designedRect:" << designedRect.toString(); 
	qfTrash() << "\tbounding_rect:" << bounding_rect.toString();
	qfTrash() << "\tdesignedRect:" << designedRect.toString();// << "isLeftTopFloating:" << isLeftTopFloating() << "isRightBottomFloating:" << isRightBottomFloating();
	if(parentFrame()) qfTrash() << "\tparent layout:" << ((parentFrame()->layout == QFGraphics::LayoutHorizontal)? "horizontal": "vertical");
	qfTrash() << "\tlayout:" << ((layout == QFGraphics::LayoutHorizontal)? "horizontal": "vertical") << ", is rubber:" << isRubber(layout);
	//qfTrash() << "\tmetaPaintLayoutLength:" << metaPaintLayoutLength << "metaPaintOrthogonalLayoutLength:" << metaPaintOrthogonalLayoutLength;
	PrintResult res = PrintOk;
	updateChildren();
	Rect bbr = bounding_rect;
	qfTrash() << "\tbbr 0:" << bbr.toString();
	if(designedRect.isAnchored()) {
		/// pokud je designedRect anchored neni treba ho nekam cpat
		/// pokud frame neni floating, vyprdni se na bounding_rect
		bbr = designedRect;
		if(bbr.isRubber(QFGraphics::LayoutHorizontal)) bbr.setRight(bounding_rect.right());
		if(bbr.isRubber(QFGraphics::LayoutVertical)) bbr.setBottom(bounding_rect.bottom());
	}
	else {
		if(designedRect.horizontalUnit == Rect::UnitMM && designedRect.width() - Epsilon > bounding_rect.width()) {
			qfTrash() << "\t<<<< FRAME NOT FIT WIDTH";
			return checkPrintResult(PrintNotFit);
		}
		if(designedRect.verticalUnit == Rect::UnitMM && designedRect.height() - Epsilon > bounding_rect.height()) {
			qfTrash() << "\t<<<< FRAME NOT FIT HEIGHT";
			//qfInfo() << "\tbounding_rect:" << bounding_rect.toString() << "height:" << bounding_rect.height();
			//qfInfo() << "\tdesignedRect:" << designedRect.toString() << "height:" << designedRect.height();
			//qfInfo() << "\tbounding_rect.height() < designedRect.height() (" << bounding_rect.height() << "<" << designedRect.height() << "):" << (bounding_rect.height() > designedRect.height());
			return checkPrintResult(PrintNotFit);
		}
	}

	Rect init_bbr = bbr;
	bbr.adjust(hinset, vinset, -hinset, -vinset);
	qfTrash() << "\tbbr 1:" << bbr.toString();

	QFReportItemMetaPaintFrame *mp = dynamic_cast<QFReportItemMetaPaintFrame*>(createMetaPaintItem(NULL));
	QF_ASSERT(mp, "Meta paint item for element " + element.tagName() + " not created.");
	mp->setInset(hinset, vinset);
	mp->setLayout(layout);
	mp->setAlignment(alignment);
	res = printMetaPaintChildren(mp, bbr);
	//qfTrash() << "\tbbr_init:" << bbr_init.toString();

	if(res.value == PrintNotFit) {
		/// pokud je result neverfit, nech ho tam, at aspon vidime, co se nikdy nevejde
		//qfInfo() << "keepAll:" << keepAll;
		if(keepAll && !(res.flags & FlagPrintNeverFit)) {
			resetIndexToPrintRecursively(QFReportProcessorItem::IncludingParaTexts);
			//qfInfo() << "keepAll && !(res.flags & FlagPrintNeverFit)";
			SAFE_DELETE(mp);
			return checkPrintResult(res);
		}
	}
	mp->setParent(out);

	if(!isRubber(layout)) {
		/// pokud ma nektere dite flag filllayout, roztahni ho tak aby s ostatnimi detmi vyplnili layout
		int filllayout_child_ix = -1;
		qreal sum_mm = 0;
		for(int i=0; i<mp->childrenCount(); i++) {
			QFReportItemMetaPaint *it = mp->childAt(i);
			sum_mm += it->renderedRect.sizeInLayout(layout);
			if(it->renderedRect.flags & Rect::FillLayout) filllayout_child_ix = i;
		}
		if(filllayout_child_ix >= 0) {
			qreal offset = bbr.sizeInLayout(layout) - sum_mm;
			if(offset > 0) {
				QFReportItemMetaPaint *it = mp->childAt(filllayout_child_ix);
				it->renderedRect.setSizeInLayout(it->renderedRect.sizeInLayout(layout) + offset, layout);
				it->alignChildren();
				Point p;
				if(layout == QFGraphics::LayoutHorizontal) p.setX(offset);
				else if(layout == QFGraphics::LayoutVertical) p.setY(offset);
				for(int i=filllayout_child_ix + 1; i<mp->childrenCount(); i++) {
					it = mp->childAt(i);
					it->shift(p);
				}
			}
		}
	}
#if 0
	//if(element.tagName() == "row") qfInfo() << "expandchildren:" << element.attribute("expandchildren");
	if(isRubber(parentLayout()) && layout != orthogonalLayout(layout) && element.attribute("expandchildren").toBool()) {
		/// pokud ma item flag expandchildren, roztahni deti tak, aby mely vsechny rozmer ve smeru ortogonalniho layoutu
		/// jako to nejvetsi ve smeru ortogonalniho layoutu
		//qfInfo() << "expanding children elementid:" << element.attribute("id");
		//if(element.attribute("id") == "123") {
		//	qfInfo() << "\t" << element.toString();
		//	mp->dump();
		//}
		qreal max_mm = 0;
		for(int i=0; i<mp->children.count(); i++) {
			QFReportItemMetaPaint *it = mp->childAt(i);
			//qfInfo() << "\t " << it->reportElement.toString();
			//it->dump();
			max_mm = qMax(it->renderedRect.sizeInLayout(orthogonalLayout()), max_mm);
		}
		for(int i=0; i<mp->children.count(); i++) {
			QFReportItemMetaPaint *it = mp->childAt(i);
			it->renderedRect.setSizeInLayout(max_mm, orthogonalLayout());
		}
	}
#endif
	/// tak kolik jsem toho pokreslil?
	Rect dirty_rect;//, rendered_rect = designedRect;
	dirty_rect.flags = designedRect.flags;
	if(isRubber(layout) || isRubber(orthogonalLayout())) {
		/// pokud je v nejakym smeru natahovaci, musim to proste secist
		for(int i=0; i<mp->childrenCount(); i++) {
			QFReportItemMetaPaint *it = mp->childAt(i);
			//qfInfo() << "child" << i << "rendered rect:" << it->renderedRect.toString() << "is null:" << it->renderedRect.isNull();
			//qfInfo() << "\t 1 rubber dirty_rect:" << dirty_rect.toString();
			if(dirty_rect.isNull()) dirty_rect = it->renderedRect;
			else dirty_rect = dirty_rect.united(it->renderedRect);
			//dirty_rect.flags |= it->renderedRect.flags;
			//qfInfo() << "\t 2 rubber dirty_rect:" << dirty_rect.toString();
		}
		qfTrash() << "\trubber dirty_rect:" << dirty_rect.toString();
	}
	qfTrash() << "\tdirty_rect 1:" << dirty_rect.toString();
	/// pokud je v nekterem smeru definovany, je jedno, kolik se toho potisklo a nastav ten rozmer
	if(designedRect.horizontalUnit == Rect::UnitPercent) dirty_rect.setWidth(bbr.width());
	else if(designedRect.horizontalUnit == Rect::UnitMM && designedRect.width() > 0) dirty_rect.setWidth(designedRect.width() - 2*hinset);
	qfTrash() << "\tdirty_rect 2:" << dirty_rect.toString();
	if(designedRect.verticalUnit == Rect::UnitPercent) dirty_rect.setHeight(bbr.height());
	else if(designedRect.verticalUnit == Rect::UnitMM && designedRect.height() > 0) dirty_rect.setHeight(designedRect.height() - 2*vinset);
	qfTrash() << "\tdirty_rect 3:" << dirty_rect.toString();
	/// pri rendrovani se muze stat, ze dirtyRect nezacina na bbr, to ale alignment zase spravi
	dirty_rect.moveTopLeft(bbr.topLeft());
	qfTrash() << "\tdirty_rect:" << dirty_rect.toString();
	//qfTrash() << "\tlayout:" << ((layout == LayoutHorizontal)? "horizontal": "vertical");
	//qfTrash() << "\tortho layout:" << ((orthogonalLayout() == LayoutHorizontal)? "horizontal": "vertical");
	//qfTrash() << "\trenderedRect:" << r.toString();
	//qfTrash() << "\trenderedRect:" << r.toString();

	/// alignment
	qfTrash() << "\tALIGN:" << QString::number((int)alignment, 16);
	//alignChildren(mp, dirty_rect);
	//if(0)
	dirty_rect.adjust(-hinset, -vinset, hinset, vinset);
	mp->renderedRect = dirty_rect;
	mp->alignChildren();
	//qfInfo() << "\t designedRect flags:" << designedRect.toString();
	mp->renderedRect.flags = designedRect.flags;

	if(designedRect.flags & QFReportProcessorItem::Rect::ExpandChildrenFrames) {
		mp->expandChildrenFramesRecursively();
	}
	
	//dirtyRect = r;//.adjusted(-hinset, -vinset, hinset, vinset);;
	qfTrash() << "\trenderedRect:" << mp->renderedRect.toString();
	res = checkPrintResult(res);
	qfTrash().color(QFLog::Cyan) << "\t<<<< FRAME return:" << res.toString() << element.tagName() << "id:" << element.attribute("id");
	return res;
}
/*
void QFReportItemFrame::alignChildren(QFReportItemMetaPaintFrame *mp,  const QFReportProcessorItem::Rect & dirty_rect)
{
	if(!dirty_rect.isNull()) {
		if(alignment & ~(Qt::AlignLeft | Qt::AlignTop)) {
			Point offset;
			/// ve smeru layoutu posun cely blok
			{
				Rect r1;
				/// vypocitej velikost potisknuteho bloku
				for(int i=0; i<mp->childrenCount(); i++) {
					QFReportItemMetaPaint *it = mp->childAt(i);
					r1 = r1.united(it->renderedRect);
				}
				qreal al = 0, d;
				if(layout == QFGraphics::LayoutHorizontal) {
					if(alignment & Qt::AlignHCenter) al = 0.5;
					else if(alignment & Qt::AlignRight) al = 1;
					d = dirty_rect.width() - r1.width();
					if(al > 0 && d > 0)  {
						offset.rx() = d * al - (r1.left() - dirty_rect.left());
					}
				}
				else if(layout == QFGraphics::LayoutVertical) {
					if(alignment & Qt::AlignVCenter) al = 0.5;
					else if(alignment & Qt::AlignBottom) al = 1;
					d = dirty_rect.height() - r1.height();
					if(al > 0 && d > 0)  {
						offset.ry() = d * al - (r1.top() - dirty_rect.top());
					}
				}
			}

			/// v orthogonalnim smeru kazdy item
			for(int i=0; i<mp->childrenCount(); i++) {
				QFReportItemMetaPaint *it = mp->childAt(i);
				const Rect &r1 = it->renderedRect;
				qfTrash() << "\t\titem renderedRect:" << r1.toString();
				qreal al = 0, d;

				if(orthogonalLayout() == QFGraphics::LayoutHorizontal) {
					offset.rx() = 0;
					if(alignment & Qt::AlignHCenter) al = 0.5;
					else if(alignment & Qt::AlignRight) al = 1;
					d = dirty_rect.width() - r1.width();
					if(al > 0 && d > 0)  {
						qfTrash() << "\t\thorizontal alignment:" << al;
						offset.rx() = d * al - (r1.left() - dirty_rect.left());
					}
				}
				else if(orthogonalLayout() == QFGraphics::LayoutVertical) {
					offset.ry() = 0;
					al = 0;
					if(alignment & Qt::AlignVCenter) al = 0.5;
					else if(alignment & Qt::AlignBottom) al = 1;
					d = dirty_rect.height() - r1.height();
					if(al > 0 && d > 0)  {
						qfTrash() << "\t\tvertical alignment:" << al;
						offset.ry() = d * al - (r1.top() - dirty_rect.top());
					}
				}
				qfTrash() << "\t\talign offset:" << offset.toString();
				if(!offset.isNull()) it->shift(offset);
			}
		}
	}
}
*/
/*
QFReportItemFrame::Layout QFReportItemFrame::parentLayout() const
{
	QFReportItemFrame *frm = parentFrame();
	if(!frm) return LayoutInvalid;
	return frm->layout;
}
*/
void QFReportItemFrame::resetIndexToPrintRecursively(bool including_para_texts)
{
	//qfInfo() << "resetIndexToPrintRecursively()";
	indexToPrint = 0;
	for(int i=0; i<childrenCount(); i++) {
		QFReportProcessorItem *it = childAt(i);
		it->resetIndexToPrintRecursively(including_para_texts);
	}
	/*
	foreach(QObject *o, children()) {
		QFReportProcessorItem *it = qobject_cast<QFReportProcessorItem*>(o);
		it->resetIndexToPrintRecursively();
	}
	*/
}

//==========================================================
//                                    QFReportItemReport
//==========================================================
QFReportItemReport::QFReportItemReport(QFReportProcessor *proc, const QFDomElement &_el)
	: QFReportItemBand(proc, NULL, _el)
{
	QF_ASSERT(proc, "processor is NULL");
	//Rect r = designedRect;
	//QFDomElement el = element.cloneNode(false).toElement();
	//qfTrash() << "\toriginal:" << element.tagName() << "is null:" << element.isNull() << "has children:" << element.hasChildNodes() << "parent node is null:" << element.parentNode().isNull();
	//qfTrash() << "\tclone:" << el.tagName() << "is null:" << el.isNull() << "has children:" << el.hasChildNodes() << "parent node is null:" << el.parentNode().isNull();
	if(element.attribute("orientation") == "landscape") {
		Size sz = designedRect.size();
		sz.transpose();
		designedRect.setSize(sz);
	}
	designedRect.flags = (Rect::LeftFixed | Rect::TopFixed | Rect::RightFixed | Rect::BottomFixed);
	//element.setAttribute("brd", "color: teal");
	//element.setAttribute("fill", "color: white");
	f_dataTable = proc->data();
	//qfInfo() << f_dataTable.toString();
	dataTableLoaded = true;
}

QFReportProcessorItem::PrintResult QFReportItemReport::printMetaPaint(QFReportItemMetaPaint *out, const QFReportProcessorItem::Rect &bounding_rect )
{
	qfTrash() << QF_FUNC_NAME << "\x1B[1;31;40m***ROOT***ROOT***ROOT***ROOT***\x1B[0;37;40m" << element.tagName();
	Q_UNUSED(bounding_rect);
	PrintResult res = PrintOk;
	//updateChildren();
	//QFReportItemMetaPaintPage *pg = new QFReportItemMetaPaintPage(out, element, processor->context());
	//pg->renderedRect = designedRect;
	//indexToPrint = 0; /// vzdy vytiskni header a footer. (footer je absolutni header, umisteny pred detailem)
	res = QFReportItemBand::printMetaPaint(out, designedRect);
	//res = printMetaPaintChildren(pg, pg->renderedRect);
	qfTrash() << "\t\x1B[1;31;40m<<< ***ROOT***ROOT***ROOT***ROOT***\x1B[0;37;40m";
	//res = checkPrintResult(res);
	return res;
}

QFXmlTable& QFReportItemReport::dataTable()
{
	return f_dataTable;
}

//==========================================================
//                                    QFReportItemBody
//==========================================================
/*
QFReportProcessorItem::PrintResult QFReportItemBody::printMetaPaint(QFReportItemMetaPaint *out, const QFReportProcessorItem::Rect &bounding_rect )
{
	qfTrash() << QF_FUNC_NAME;
	PrintResult res = QFReportItemDetail::printMetaPaint(out, bounding_rect);
	/// body jediny ma tu vysadu, ze se muze vickrat za sebou nevytisknout a neznamena to print forever.
	if(res == PrintNeverFit) res = PrintNotFit;
	return res;
}
*/
#if 0
//==========================================================
//                                    QFReportItemHeaderFrame
//==========================================================
QFReportProcessorItem::PrintResult QFReportItemHeaderFrame::printMetaPaint(QFReportItemMetaPaint *out, const QFReportProcessorItem::Rect &bounding_rect )
{
	qfTrash() << QF_FUNC_NAME;
	return QFReportItemFrame::printMetaPaint(out, bounding_rect);
}

//==========================================================
//                                    QFReportItemRow
//==========================================================
QFReportProcessorItem::PrintResult QFReportItemRow::printMetaPaint(QFReportItemMetaPaint *out, const Rect &bounding_rect)
{
	qfTrash() << QF_FUNC_NAME;
	return QFReportItemFrame::printMetaPaint(out, bounding_rect);
}

//==========================================================
//                                    QFReportItemCell
//==========================================================
QFReportProcessorItem::PrintResult QFReportItemCell::printMetaPaint(QFReportItemMetaPaint *out, const Rect &bounding_rect)
{
	qfTrash() << QF_FUNC_NAME;
	return QFReportItemFrame::printMetaPaint(out, bounding_rect);
}
#endif
//==========================================================
//                                    QFReportItemPara
//==========================================================
QFReportItemPara::QFReportItemPara(QFReportProcessor * proc, QFReportProcessorItem * parent, const QFDomElement & el)
	: QFReportItemFrame(proc, parent, el)
{
	qfLogFuncFrame();
	//qfInfo() << el.text();
}

void QFReportItemPara::resetIndexToPrintRecursively(bool including_para_texts)
{
	if(including_para_texts) indexToPrint = 0;
}

QFReportProcessorItem::PrintResult QFReportItemPara::printMetaPaint(QFReportItemMetaPaint *out, const Rect &bounding_rect)
{
	qfLogFuncFrame();
	return QFReportItemFrame::printMetaPaint(out, bounding_rect);
}

QFReportProcessorItem::PrintResult QFReportItemPara::printMetaPaintChildren(QFReportItemMetaPaint *out, const QFReportProcessorItem::Rect &bounding_rect)
{
	qfTrash().color(QFLog::Yellow) << QF_FUNC_NAME << element.tagName() << "id:" << element.attribute("id");
	PrintResult res = PrintOk;
	if(indexToPrint == 0) {
		printedText = paraText();
	}
	QString text = printedText.mid(indexToPrint);
	
	QFGraphicsStyleCache::Style style = processor->context().styleCache().style(elementAttribute("style"));
	{
		QFString s;
		s = elementAttribute("font");
		if(!!s) style.font = processor->context().styleCache().font(s);
		s = elementAttribute("pen");
		if(!!s) style.pen = processor->context().styleCache().pen(s);
			//QBrush brush = processor->context().brushFromString(element.attribute("brush"));
		qfTrash() << "\tfont:" << style.font.toString();
			//qfTrash() << "\tpen color:" << pen.color().name();
			//qfTrash() << "\tbrush color:" << brush.color().name();
	}
	QFontMetricsF font_metrics = processor->fontMetrics(style.font);
	int flags = 0;
	{
		if(QFString(elementAttribute("wrap", "1")).toBool()) flags |= Qt::TextWordWrap;
		QFString s;
		s = elementAttribute("halign", "left");
		if(s == "center") flags |= Qt::AlignHCenter;
		else if(s == "right") flags |= Qt::AlignRight;
		else if(s == "justify") flags |= Qt::AlignJustify; /// ma smysl jen pro para
		s = element.attribute("valign", "top");
		if(s == "center") flags |= Qt::AlignVCenter;
		else if(s == "bottom") flags |= Qt::AlignBottom;
	}

	Rect br;
	/// velikost boundingRect je v mm, tak to prepocitej na body vystupniho zarizeni
	br = QFGraphics::mm2device(bounding_rect, processor->paintDevice());

	bool render_check_mark = false;
	bool text_item_should_be_created = true;
	QRegExp rx = QFReportItemMetaPaint::checkReportSubstitutionRegExp;
	if(rx.exactMatch(text)) {
		//bool check_on = rx.capturedTexts().value(1) == "1";
		br = font_metrics.boundingRect('X');
		render_check_mark = true;
	}
	else {
	
		//text.replace(QFReportItemMetaPaint::checkOnReportSubstitution, "X");
		//text.replace(QFReportItemMetaPaint::checkOffReportSubstitution, "X");
		//qfInfo().noSpace().color(QFLog::Green) << "index to print: " << indexToPrint << " text: '" << text << "'";
		//qfInfo() << "bounding rect:" << bounding_rect.toString();
		//qfWarning() << "device physical DPI:" << processor->paintDevice()->physicalDpiX() << processor->paintDevice()->physicalDpiY();
		//qfWarning().noSpace() << "'" << text << "' font metrics: " << br.toString();

		//QString text = element.text().simplified().replace("\\n", "\n");
		//qfInfo() << "br:" << br.toString();
		//Rect br_debug = br;
		//bool splitted = false;
		/// do layout
		{
			qreal leading = font_metrics.leading();
			qreal height = 0;
			qreal width = 0;
			textLayout.setFont(style.font);
			Qt::Alignment alignment = (~Qt::Alignment()) & flags;
			QTextOption opt(alignment);
			opt.setWrapMode(QTextOption::WrapAtWordBoundaryOrAnywhere);
			textLayout.setTextOption(opt);
			textLayout.setText(text);
			textLayout.beginLayout();
			//bool rubber_frame = designedRect.isRubber(LayoutVertical);
			//int old_pos = 0;
			//QString tx1 = text;
			//qfInfo() << "text to layout:" << text;
			while (1) {
				QTextLine line = textLayout.createLine();
				if(!line.isValid()) {
					//qfInfo() << "veslo se VSECHNO";
					indexToPrint = printedText.length();
					break;
				}
				//old_pos = line.textStart();
				line.setLineWidth(br.width()); /// setWidth() nastavi spravne line.height(), proto musi byt pred merenim popsane vysky.
				qreal interline_space = (height > 0)? leading: 0;
				if(height + interline_space + line.height() > br.height()) {
					//qfInfo() << "NEEEEEEEE veslo se";
					res = PrintNotFit;
					if(height == 0) {
						/// nevejde se ani jeden radek
						text_item_should_be_created = false;
						break;
					}
					else {
						/// neco se preci jenom veslo
						//splitted = true;
						//qfInfo() << "\tbounding_rect rect:" << bounding_rect.toString();
						//qfInfo() << "\tbr:" << br.toString();
						//qfInfo() << "\theight:" << height;
						int pos = line.textStart();
						//qfInfo() << "POS:" << pos << text.mid(pos).left(50);
						indexToPrint += pos;
						text = text.left(pos);
						break;
					}
					//line.setLineWidth(123456789); /// vytiskni to az do konce
				}
				height += interline_space;
				//qfInfo() << "LINE ##:" << line.textStart() << text.mid(line.textStart(), line.textLength());
				line.setPosition(QPointF(0., height));
				height += line.height();
				width = qMax(width, line.naturalTextWidth());
			}
			textLayout.endLayout();
			br.setWidth(width);
			br.setHeight(height);
			// musim to takhle premerit, jina
			//br = font_metrics.boundingRect(br, flags, text);
			//br = font_metrics.boundingRect(br_debug, 0, text);
			//qfInfo() << "\tbr2:" << br.toString();
		}
	}
	/*
	int x_dpi = processor->paintDevice()->logicalDpiX();
	int y_dpi = processor->paintDevice()->logicalDpiY();
	br.setWidth(br.width() * 25.4 / x_dpi);
	br.setHeight(br.height() * 25.4 / y_dpi);
	*/
	/// velikost boundingRect je v bodech vystupniho zarizeni, tak to prepocitej na mm
	br = QFGraphics::device2mm(br, processor->paintDevice());
	//if(splitted) qfInfo() << "\tbr [mm]:" << br.toString();
	//qfWarning().noSpace() << "'" << text << "' font metrics: " << br.toString();
	/// posun to na zacatek, alignment ramecku to zase vrati
	br.moveTopLeft(bounding_rect.topLeft());
	// odecti mezeru mezi radky za poslednim radkem
	//br.setHeight(br.height() - processor->fontMetrics(style.font).leading());
	//qfInfo().noSpace() << "text: '" << text << "'";
	//qfInfo() << "\tbr:" << br.toString() << "text_item_should_be_created:" << text_item_should_be_created;
	if(br.width() == 0) {
		/// tiskne se prazdny text
		if(QFString(elementAttribute("omitEmptyString")).toBool()) text_item_should_be_created = false;
	}
	if(text_item_should_be_created ) {
		QFReportItemMetaPaintText *mt;
		if(render_check_mark ) mt = new QFReportItemMetaPaintCheck(out, this);
		else mt = new QFReportItemMetaPaintText(out, this);
		mt->pen = style.pen;
		//mt->brush = brush;
		mt->font = style.font;
		mt->text = text;
		mt->flags = flags;
		//mt->renderCheck = render_check;
		//if(flags & (Qt::AlignHCenter | Qt::AlignRight)) br.setLeft(bounding_rect.left());
		//if(flags & (Qt::AlignVCenter | Qt::AlignBottom)) br.setWidth(bounding_rect.height());
		mt->renderedRect = br;
		mt->renderedRect.flags = designedRect.flags;
	}
	//qfTrash().color(QFLog::Green, QFLog::Red) << "\tleading:" << processor->fontMetrics(style.font).leading() << "\theight:" << processor->fontMetrics(style.font).height();
	qfTrash() << "\tchild rendered rect:" << br.toString();
	qfTrash() << "\t<<< CHILDREN paraText return:" << res.toString();
	//res = checkPrintResult(res);
	return res;
}

QString QFReportItemPara::paraText()
{
	qfTrash().color(QFLog::Cyan) << QF_FUNC_NAME;
	QString ret;
	QString to_localize;
	QStringList data_texts;
	int data_cnt = 0;
	for(QFDomNode nd = element.firstChild(); !!nd; nd = nd.nextSibling()) {
		if(nd.isText()) {
			to_localize += nodeText(nd);
		}
		else {
			to_localize += '%'  + QString::number(++data_cnt);
			data_texts << nodeText(nd);
		}
	}
	{
		QByteArray ba = to_localize.toUtf8();
		ret = QCoreApplication::translate("report", ba.constData(), 0, QCoreApplication::UnicodeUTF8);
		//ret = QCoreApplication::trUtf8(ba.constData(), "report");
		//qfInfo() << to_localize << "->" << ret;
		for(int i=0; i<data_texts.count(); i++) ret = ret.arg(data_texts.value(i));
	}
//qfTrash().color(QFLog::Cyan) << "\treturn:" << ret;
	{
		static QString new_line;
		if(new_line.isEmpty()) new_line += QChar::LineSeparator;
		ret.replace("\\n", new_line);
		ret.replace("\n", new_line);
		
		/// jinak nedokazu zadat mezeru mezi dvema <data> elementy nez <data>\s<data>
		ret.replace("\\s", " ");
	}
	//qfInfo().noSpace() << "'" << ret << "'";
	return ret;
}


//==========================================================
//                                    QFReportItemBand
//==========================================================
QFReportItemBand::QFReportItemBand(QFReportProcessor *proc, QFReportProcessorItem *parent, const QFDomElement &_el)
	: QFReportItemFrame(proc, parent, _el), dataTableLoaded(false)
{
}

void QFReportItemBand::resetIndexToPrintRecursively(bool including_para_texts)
{
	QFReportItemFrame::resetIndexToPrintRecursively(including_para_texts);
	//qfInfo() << "dataTableLoaded = false" << element.attribute("id");
	dataTableLoaded = false;
}

QFXmlTable& QFReportItemBand::dataTable()
{
	//qfLogFuncFrame() << "dataTableLoaded:" << dataTableLoaded;
	if(!dataTableLoaded) {
		QFDomElement el_data_src = element.firstChildElement("datasrc");
		QFDomElement el_data = el_data_src.firstChildElement("data");
		QString data_src_name = el_data_src.attribute("name");
		//QString data_domain = el_data_src.attribute("domain", "table");
		if(data_src_name.isEmpty()) {
			/// drive nebyly podporovany domeny pro data, zkus starsi zpusob
			/// v kazdem pripade, pokud neni definovano datasrc, je datadomain vzdy "table"
			data_src_name = element.attribute("datatablename");
			//data_domain = "table";
		}
		//qfInfo() << "\t data_src_name:" << data_src_name;
		if(el_data.isNull()) {
			//qfInfo() << "loading datatablename:" << data_src_name;
			f_dataTable = findDataTable(data_src_name);
		}
		else {
			//qfInfo() << "\t loading data to f_dataTable";
			QVariant v = nodeValue(el_data);
			f_dataTableOwnerDocument = v.value<QFXmlTableDocument>();
			//qfInfo() << "\t" << f_dataTableOwnerDocument.toString();
			f_dataTable = f_dataTableOwnerDocument.toTable();
		}
		dataTableLoaded = true;
	}
	return f_dataTable;
}

QFReportProcessorItem::PrintResult QFReportItemBand::printMetaPaint(QFReportItemMetaPaint *out, const Rect &bounding_rect)
{
	//qfInfo() << __LINE__;
	qfTrash().color(QFLog::White) << QF_FUNC_NAME;
	//qfInfo() << "src:" << element.attribute("datatablename") << "table is null:" << dataTable().isNull();
	//qfInfo() << dataTable().toString();
	if(dataTable().isNull() && !processor->isDesignMode()) { /// pokud neni table (treba bez radku), band se vubec netiskne
		PrintResult res;
		res.value = PrintOk;
		return res;
	}
	if(QFString(element.attribute("headeronbreak")).toBool()) {
		/// vsechno krome detailu se bude tisknout znovu
		for(int i=0; i<children().count(); i++) {
			QFReportProcessorItem *it = childAt(i);
			if(it->toDetail() == NULL) it->resetIndexToPrintRecursively(QFReportProcessorItem::IncludingParaTexts);
		}
		indexToPrint = 0;
	}
	PrintResult res = QFReportItemFrame::printMetaPaint(out, bounding_rect);
	//res = checkPrintResult(res);
	qfTrash().color(QFLog::Green) << "\tRETURN:" << res.toString();
	return res;
}

//==========================================================
//                                    QFReportItemDetail
//==========================================================
QFReportItemDetail::QFReportItemDetail(QFReportProcessor *proc, QFReportProcessorItem *parent, const QFDomElement &_el)
	: QFReportItemFrame(proc, parent, _el)
{
	//qfInfo() << QF_FUNC_NAME;
	f_currentRowNo = 0;
}

QFReportProcessorItem::PrintResult QFReportItemDetail::printMetaPaint(QFReportItemMetaPaint *out, const Rect &bounding_rect)
{
	qfTrash().color(QFLog::Blue) << QF_FUNC_NAME << element.tagName() << "id:" << element.attribute("id");
	//qfInfo() << QF_FUNC_NAME;
	bool design_mode = processor->isDesignMode(); /// true znamena, zobraz prvni radek, i kdyz tam nejsou data.
	//qfInfo() << "design mode:" << design_mode;
	QFReportItemBand *b = parentBand();
	if(b) {
		qfTrash() << "\t band:" << b << "table is null:" << b->dataTable().isNull();
		if(!b->dataTable().isNull()) {
			//design_view = false;
			if(f_dataRow.isNull()) {
				/// kdyz neni f_dataRow, vezmi prvni radek dat
				f_dataRow = b->dataTable().firstRow();
				//qfInfo() << "\ttaking first row, is null:" << f_dataRow.isNull();
				f_currentRowNo = 0;
			}
		}
	}
	PrintResult res;
	if(!design_mode && f_dataRow.isNull()) {
		/// prazdnej detail vubec netiskni
		res.value = PrintOk;
		return res;
	}
	res = QFReportItemFrame::printMetaPaint(out, bounding_rect);
	if(res.value == PrintOk) {
		if(b) {
			/// vezmi dalsi radek dat
			f_dataRow = b->dataTable().nextRow(f_dataRow);
			f_currentRowNo++;
			//qfInfo() << "\ttaking next row" << f_currentRowNo << "is null:" << f_dataRow.isNull();
			if(!f_dataRow.isNull()) {
				resetIndexToPrintRecursively(QFReportProcessorItem::IncludingParaTexts);
				res.flags |= FlagPrintAgain;
			}
		}
	}
	//res = checkPrintResult(res);
	qfTrash().color(QFLog::Blue) << "\treturn:" << res.toString();
	return res;
}

//==========================================================
//                                    QFReportItemTable
//==========================================================
QFReportItemTable::QFReportItemTable(QFReportProcessor *proc, QFReportProcessorItem *_parent, const QFDomElement &_el)
	: QFReportItemBand(proc, _parent, _el)
{
	//qfTrash() << QF_FUNC_NAME << "parent:" << parent();
	//QF_ASSERT(!!_el, "element is null.");
}

void QFReportItemTable::syncChildren()
{
	qfTrash() << QF_FUNC_NAME << element.tagName() << "id:" << element.attribute("id");
	deleteChildren();
	for(QFDomElement el = fakeBand.firstChildElement(); !!el; el = el.nextSiblingElement()) {
		if(!QFReportProcessor::isProcessible(el)) continue;
		/// vytvor chybejici item
		processor->createProcessibleItem(el, this);
	}
	qfTrash() << QF_FUNC_NAME << "<<<<<<<<<<<<<<<< OUT";
}

void QFReportItemTable::createFakeBand()
{
	//return;
	if(!!fakeBand) return;

	bool from_data = element.attribute("createfromdata").toBool();
	QString decoration = element.attribute("decoration");
	bool grid = (decoration == "grid");
	bool horizontal_lines = (decoration == "horizontallines");
	bool line = !decoration.isEmpty();
	QString line_pen = (grid)? "black1": "black1";
	bool shadow = element.attribute("shadow", "1").toBool();

	fakeBand = fakeBandDocument.createElement("band");
	fakeBandDocument.appendChild(fakeBand);
	fakeBand.setAttribute("__fake", 1); /// napomaha pri selekci v report editoru

	QFDomElement el_header, el_detail, el_footer;
	{
		QFDomElement el, el1;

		el1 = fakeBand.ownerDocument().createElement("row");
		el = element.firstChildElement("headerframe");
		el1.copyAttributesFrom(el);
		/// v tabulce ma smysl mit deti zarovnany
		if(grid && el1.attribute("expandChildrenFrames").isEmpty()) el1.setAttribute("expandChildrenFrames", "1");
		if(line && el1.attribute("bbrd").isEmpty()) el1.setAttribute("bbrd", line_pen);
		else if(horizontal_lines) el1.setAttribute("bbrd", line_pen);
		if(shadow && el1.attribute("fill").isEmpty()) el1.setAttribute("fill", "tblshadow");
		el_header = el1;
		el_header.setAttribute("__fakeBandHeaderRow", 1); /// napomaha exportu do HTML

		el1 = fakeBand.ownerDocument().createElement("detail");
		el = element.firstChildElement("detailframe");
		el1.copyAttributesFrom(el);
		if(grid && el1.attribute("expandChildrenFrames").isEmpty()) el1.setAttribute("expandChildrenFrames", "1");
		if(el1.attribute("keepall").isEmpty()) el1.setAttribute("keepall", "1");
		if(horizontal_lines) el1.setAttribute("bbrd", line_pen);
		el_detail = el1;
		el_detail.setAttribute("__fakeBandDetail", 1); /// napomaha exportu do HTML

		el1 = fakeBand.ownerDocument().createElement("row");
		el = element.firstChildElement("footerframe");
		el1.copyAttributesFrom(el);
		if(grid && el1.attribute("expandChildrenFrames").isEmpty()) el1.setAttribute("expandChildrenFrames", "1");
		if(line) el1.setAttribute("tbrd", line_pen);
		else if(horizontal_lines) el1.setAttribute("bbrd", line_pen);
		if(shadow) el1.setAttribute("fill", "tblshadow");
		el_footer = el1;
		el_footer.setAttribute("__fakeBandFooterRow", 1); /// napomaha exportu do HTML
	}

	bool has_footer = false;
	if(from_data) {
		qfTrash() << "\tcreating from data";
		QFXmlTable t = dataTable();
		//foreach(const QFXmlTableColumnDef cd, t.columns()) if(!t.columnFooter(cd.name).isEmpty()) {has_footer = true; break;}
		QFXmlTableColumnList cols = t.columns();
		foreach(const QFXmlTableColumnDef cd, t.columns()) {
			{
				QFDomElement el = fakeBand.ownerDocument().createElement("para");
				el.setAttribute("w", "%");
				el.setAttribute("hinset", "1");
				el.setAttribute("style", "tblheading");
				el.setAttribute("halign", "center");
				if(grid) el.setAttribute("brd", "black1");
				QDomText txt = fakeBand.ownerDocument().createTextNode(t.columnHeader(cd.name));
				el.appendChild(txt);
				el_header.appendChild(el);
			}
			{
				QFDomElement el = fakeBand.ownerDocument().createElement("para");
				el.setAttribute("w", "%");
				el.setAttribute("hinset", "1");
				el.setAttribute("style", "tbltext");
				el.setAttribute("halign", t.columnHAlignment(cd.name));
				if(grid) el.setAttribute("brd", "black1");
				QFDomElement el1 = fakeBand.ownerDocument().createElement("data");
				el1.setAttribute("src", cd.name);
				el.appendChild(el1);
				el_detail.appendChild(el);
			}
			{
				QString footer = t.columnFooter(cd.name);
				if(!footer.isEmpty()) has_footer = true;
				QFDomElement el = fakeBand.ownerDocument().createElement("para");
				el.setAttribute("w", "%");
				el.setAttribute("hinset", "1");
				el.setAttribute("style", "tbltextB");
				el.setAttribute("halign", t.columnHAlignment(cd.name));
				if(grid) el.setAttribute("brd", "black1");
				QDomText txt = fakeBand.ownerDocument().createTextNode(footer);
				el.appendChild(txt);
				el_footer.appendChild(el);
			}
		}
	}
	else {
		QFDomElement el = element.firstChildElement("cols");
		for(el=el.firstChildElement("col"); !!el; el=el.nextSiblingElement("col")) {
			//QString w = el.attribute("w");
			QFDomElement el1;
			el1 = el.firstChildElement("colheader");
			if(!!el1) {
				el1 = el1.cloneNode().toElement();
				el1.setTagName("cell");
			}
			else el1 = fakeBand.ownerDocument().createElement("cell");
			el1.copyAttributesFrom(el);
			if(grid) el1.setAttribute("brd", "black1");
			el_header.appendChild(el1);
			//if(!w.isEmpty()) el1.setAttribute("w", w);

			el1 = el.firstChildElement("coldetail");
			if(!!el1) {
				el1 = el1.cloneNode().toElement();
				el1.setTagName("cell");
				if(grid) el1.setAttribute("brd", "black1");
			}
			else el1 = fakeBand.ownerDocument().createElement("cell");
			el1.copyAttributesFrom(el);
			el_detail.appendChild(el1);
			//if(!w.isEmpty()) el1.setAttribute("w", w);

			el1 = el.firstChildElement("colfooter");
			if(!!el1) {
				has_footer = true;
				el1 = el1.cloneNode().toElement();
				el1.setTagName("cell");
				if(grid) el1.setAttribute("brd", "black1");
			}
			else el1 = fakeBand.ownerDocument().createElement("cell");
			el1.copyAttributesFrom(el);
			el_footer.appendChild(el1);
			//if(!w.isEmpty()) el1.setAttribute("w", w);
		}
	}

	fakeBand.appendChild(el_header);
	fakeBand.appendChild(el_detail);
	if(has_footer) fakeBand.appendChild(el_footer);
	//qfInfo() << fakeBand.toString();
}

QFReportProcessorItem::PrintResult QFReportItemTable::printMetaPaint(QFReportItemMetaPaint *out, const QFReportProcessorItem::Rect &bounding_rect)
{
	createFakeBand();
	return QFReportItemBand::printMetaPaint(out, bounding_rect);
}
/*
//===============================================================
//                                               QFReportItemIf
//===============================================================
QFReportProcessorItem::PrintResult QFReportItemIf::printMetaPaintChildren(QFReportItemMetaPaint *out, const QFReportProcessorItem::Rect &bounding_rect)
{
	qfTrash().color(QFLog::Magenta) << QF_FUNC_NAME << element.tagName() << "id:" << element.attribute("id");
	qfTrash() << "\tbounding_rect:" << bounding_rect.toString();
	PrintResult res = PrintOk;
	Rect bbr = bounding_rect;

	QFDomElement el = element.firstChildElement();
	if(!!el) {
		bool bool_res = nodeValue(el).toBool();
		QFReportProcessorItem *it_res = NULL;
		for(int i=indexToPrint; i<children().count(); i++) {
			QFReportProcessorItem *it = childAt(i);
			if(bool_res) {
				if(dynamic_cast<QFReportItemIfTrue*>(it)) {
					it_res = it;
					break;
				}
			}
			else {
				if(dynamic_cast<QFReportItemIfFalse*>(it)) {
					it_res = it;
					break;
				}
			}
		}
		if(it_res) {
			res = it_res->printMetaPaint(out, bbr);
		}
	}
	return res;
}
*/
//===============================================================
//                                               QFReportItemImage
//===============================================================
bool QFReportItemImage::childrenSynced()
{
	return childrenSyncedFlag;
}

void QFReportItemImage::syncChildren()
{
	qfTrash() << QF_FUNC_NAME << element.tagName() << "id:" << element.attribute("id");
	QString orig_src = element.attribute("src");
	QString processor_img_key;
	src = orig_src;
	QFReportProcessorItem::Image im;
	if(orig_src.startsWith("key:/")) {
		/// obrazek je ocekavan v processor->images(), takze neni treba delat nic
		src = QString();
	}
	else if(orig_src.isEmpty()) {
		/// obrazek bude v datech
		src = QString();
	}
	if(!src.isEmpty()) {
		src = QFReportProcessor::searchDirs().findFile(src);
		if(src.isEmpty()) {
			qfWarning().noSpace() << "file '" << orig_src << "' does not exists in " << QFReportProcessor::searchDirs().dirs().join(", ");
			/// pridej fake para element, aby se jmeno chybejiciho souboru zobrazilo v reportu
			if(fakeLoadErrorPara.isNull()) {
				//qfInfo() << "creating fakeLoadErrorPara:" << orig_src;
				fakeLoadErrorPara = fakeLoadErrorParaDocument.createElement("para");
				fakeLoadErrorParaDocument.appendChild(fakeLoadErrorPara);
				fakeLoadErrorPara.setAttribute("__fake", 1);
				fakeLoadErrorPara.appendChild(fakeLoadErrorPara.ownerDocument().createTextNode(orig_src));
				processor->createProcessibleItem(fakeLoadErrorPara, this);
				//qfInfo() << "children cnt:" << this->children().count();
				//qfInfo() << "this:" << this << "childrencount" << children().count() << "\n" << toString();
			}
		}
	}

	qfTrash() << "orig_src:" << orig_src;
	qfTrash() << "src:" << src;
	if(!src.isEmpty()) {
		if(src.endsWith(".svg", Qt::CaseInsensitive)) {
			QSvgRenderer ren;
			if(!ren.load(src)) qfWarning() << "SVG data read error src:" << src;
			else {
				//qfInfo() << "default size::" << ren.defaultSize().width() << ren.defaultSize().height();
				QPicture pic;
				QPainter painter(&pic);
				ren.render(&painter);
				painter.end();
				im.picture = pic;
				//qfInfo() << "bounding rect:" << Rect(pic.boundingRect()).toString();
			}
		}
		else {
			im.image = QImage(src);
		}
		if(!im.isNull()) processor_img_key = orig_src;
	}
	if(im.isNull() && !orig_src.isEmpty()) {
		/// pridej fake para element, aby se jmeno chybejiciho souboru zobrazilo v reportu
		qfWarning().noSpace() << "QImage('" << src << "') constructor error.";
		if(fakeLoadErrorPara.isNull()) {
			fakeLoadErrorPara = fakeLoadErrorParaDocument.createElement("para");
			fakeLoadErrorParaDocument.appendChild(fakeLoadErrorPara);
			fakeLoadErrorPara.setAttribute("__fake", 1);
			fakeLoadErrorPara.appendChild(fakeLoadErrorPara.ownerDocument().createTextNode("QImage('" + src + "') constructor error."));
			processor->createProcessibleItem(fakeLoadErrorPara, this);
		}
		src = QString();
	}
	else {
		processor->addImage(processor_img_key, QFReportProcessorItem::Image(im));
		src = processor_img_key;
	}
	qfTrash() << "src:" << src;
	//QFReportProcessorItem::syncChildren();
	childrenSyncedFlag = true;
}

QFReportItemImage::PrintResult QFReportItemImage::printMetaPaintChildren(QFReportItemMetaPaint *out, const QFReportProcessorItem::Rect &bounding_rect)
{
	qfTrash().color(QFLog::Magenta) << QF_FUNC_NAME << element.tagName() << "id:" << element.attribute("id");
	qfTrash() << "\tbounding_rect:" << bounding_rect.toString();
	PrintResult res = PrintOk;
	Rect br = bounding_rect;
//QFLog::setDomainTresholds(QStringList() << "qfreportitem");
	if(!fakeLoadErrorPara.isNull()) {
		/// src nebyl nalezen, child je para, kde je uvedeno, jak se jmenoval nenalezeny obrazek
		//qfInfo() << "this:" << this << "childrencount" << children().count() << "\n" << toString();
		QFReportItemFrame::printMetaPaintChildren(out, bounding_rect);
		//QString orig_src = element.attribute("src");
		//QFReportItemMetaPaintText *txt = new QFReportItemMetaPaintText(out, this);
	}
	else {
		QFReportProcessorItem::Image im = processor->images().value(src);
		if(src.isEmpty()) {
			/// muze byt jeste v datech, zkus ho nahrat pro aktualni radek
			QFDomElement el = element.firstChildElement("data");
			QString data_s = nodeText(el);
			//qfError() << data_s;
			QByteArray data;
			QString format = element.attribute("dataformat");
			{
				QString encoding = element.attribute("dataencoding");
				if(encoding == "base64") {
					data = QByteArray::fromBase64(data_s.toAscii());
				}
				else if(encoding == "hex") {
					data = QByteArray::fromHex(data_s.toAscii());
				}
			}
			{
				QString s = element.attribute("datacompression");
				if(s == "qCompress") {
					data = qUncompress(data);
				}
			}
			if(!data.isEmpty()) {
				if(format == "svg") {
					QSvgRenderer ren;
					if(!ren.load(data)) qfWarning() << "SVG data read error, format:" << format;
					else {
						QPicture pic;
						QPainter painter(&pic);
						ren.render(&painter);
						painter.end();
						im.picture = pic;
					}
				}
				else {
					if(!im.image.load(data, format.toAscii())) qfWarning() << "Image data read error, format:" << format;
				}
			}
		}
		if(im.isNull()) qfWarning() << "Printing an empty image";
		QFReportItemMetaPaintImage *img = new QFReportItemMetaPaintImage(out, this);
		if(element.attribute("suppressPrintOut").toBool()) {
			//qfInfo() << "\t suppressPrintOut";
			img->layoutSettings()->suppressPrintOut = true;
		}
		//qfInfo() << "\t src:" << src;
		//qfInfo() << "\t processor->images().contains(" << src << "):" << processor->images().contains(src);
		QString aspect = element.attribute("aspectratio", "ignored");
		if(aspect == "keep") {
			img->aspectRatioMode = Qt::KeepAspectRatio;
		}
		else if(aspect == "keepexpanding") {
			img->aspectRatioMode = Qt::KeepAspectRatioByExpanding;
		}
		if(designedRect.width() == 0 && designedRect.horizontalUnit == Rect::UnitMM && designedRect.height() == 0 && designedRect.verticalUnit == Rect::UnitMM) {
			double w = 0;
			double h = 0;
			if(im.isImage()) {
				w = im.image.width() / (im.image.dotsPerMeterX() / 1000.);
				h = im.image.height() / (im.image.dotsPerMeterY() / 1000.);
				if(w > 0 && w < br.width()) br.setWidth(w);
				if(h > 0 && h < br.height()) br.setHeight(w);
			}
			else if(im.isPicture()) {
				w  =im.picture.boundingRect().width();
				h = im.picture.boundingRect().height();
			}
		}
		else if(designedRect.width() == 0 && designedRect.horizontalUnit == Rect::UnitMM) {
			/// rubber ve smeru x
			Size sz = im.size();
			double d = br.height() * sz.width() / sz.height();
			br.setWidth(d);
		}
		else if(designedRect.height() == 0 && designedRect.verticalUnit == Rect::UnitMM) {
			/// rubber ve smeru y
			//qfInfo() << "br:" << br.toString();
			Size sz = im.size();
			double d = br.width() * sz.height() / sz.width();
			br.setHeight(d);
		}
		else {
			/// oba smery zadany
			if(img->aspectRatioMode == Qt::KeepAspectRatioByExpanding) {
				Size sz = im.size();
				/// pretece to designedrect ve smeru x?
				double d = br.height() * sz.width() / sz.height();
				if(d > designedRect.width()) {
					br.setWidth(d);
				}
				else {
					/// pretece to designedrect ve smeru y?
					d = br.width() * sz.height() / sz.width();
					if(d > designedRect.height()) {
						br.setHeight(d);
					}
				}
			}
		}
		img->image = im;
		img->renderedRect = br;
		img->renderedRect.flags = designedRect.flags;
	}
//QFLog::setDomainTresholds(QStringList());

	return res;
}

//===============================================================
//                                               QFReportItemGraph
//===============================================================
void QFReportItemGraph::syncChildren()
{
	qfTrash() << QF_FUNC_NAME << element.tagName() << "id:" << element.attribute("id");
	static int graph_no = 0;
	src = QString("key:/graph-%1").arg(++graph_no);

	childrenSyncedFlag = true;
	QFReportProcessorItem::syncChildren();
}

QFReportItemImage::PrintResult QFReportItemGraph::printMetaPaintChildren(QFReportItemMetaPaint *out, const QFReportProcessorItem::Rect &bounding_rect)
{
	qfTrash().color(QFLog::Magenta) << QF_FUNC_NAME << element.tagName() << "id:" << element.attribute("id");
	PrintResult res = PrintOk;
	Rect br = bounding_rect;

	/// vykresli graf a pridej ho do processor->images()
	QPicture pict;
	QFGraph *graph = QFGraph::createGraph(element, findDataTable(element.attribute("datatablename")));
	if(graph) {
		graph->setStyleCache(processor->context().styleCache());
		QPainter painter;
		painter.begin(&pict);
		graph->draw(&painter, br.size());
		painter.end();
		processor->addImage(src, QFReportProcessorItem::Image(pict));
		delete graph;
	}
	//qfInfo() << "physicalDpiX:" << pict.physicalDpiX();
	//qfInfo() << "logicalDpiX:" << pict.logicalDpiX();
	//qfInfo() << "pict bounding_rect:" << Rect(pict.boundingRect()).toString();

	res = QFReportItemImage::printMetaPaintChildren(out, br);
	return res;
}









