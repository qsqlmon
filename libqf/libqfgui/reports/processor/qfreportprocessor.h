
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFREPORTPROCESSOR_H
#define QFREPORTPROCESSOR_H

#include <qfguiglobal.h>
#include <qfreportitem.h>
#include <qfxmltable.h>
#include <qfclassfield.h>
#include <qfgraphicsstylecache.h>
#include <qfscriptdriver.h>
#include <qfsearchdirs.h>
#include <qfassert.h>

#include <QObject>
#include <QPen>
#include <QBrush>
#include <QPainter>

class QFReportProcessorItem;
class QFReportItemMetaPaint;
class QFReportItemMetaPaintReport;

class QFGUI_DECL_EXPORT QFReportProcessorScriptDriver : public QFSqlScriptDriver
{
	Q_OBJECT;
	protected:
		QFReportProcessorItem *currentCallContextItem; 
	public slots:
		QScriptValue value(const QString &data_src, const QString &domain = "row", const QVariantList &params = QVariantList(), const QVariant &default_value = QFReportProcessorItem::INFO_IF_NOT_FOUND_DEFAULT_VALUE, bool sql_match = true);
	public:
		QScriptValue call(QFReportProcessorItem *item, const QString &func_name, const QVariantList &arg_list);
	public:
		QFReportProcessorScriptDriver(QObject *parent = NULL) : QFSqlScriptDriver(parent), currentCallContextItem(NULL) {}
};

//! TODO: write class documentation.
class  QFGUI_DECL_EXPORT QFReportProcessor : public QObject
{
	Q_OBJECT;
	//friend class QFReportProcessorThread;
	//! v design view je detail zobrazen, i kdyz nejsou data, aby bylo neco videt.
	QF_FIELD_RW(bool, is, set, DesignMode);
	public:
		typedef QFDomDocument Document;
		/*
		class Document : public QFDomDocument
		{
			virtual void resolveIncludesHook(QDomElement &parent_include_element, QDomNode &inserted_node, const QFDomDocument &inserted_document);
		};
		*/
		enum ProcessorMode {SinglePage = 1, FirstPage, AllPages};
	public:
		class Context
		{
			private:
				struct Data : public QSharedData
				{
					QFGraphicsStyleCache styleCache;
					QVariantMap options;
					//KeyVals keyVals;
				};
				QSharedDataPointer<Data> d;
			public:
				const QFGraphicsStyleCache& styleCache() const {return d->styleCache;}
				QFGraphicsStyleCache& styleCacheRef() {return d->styleCache;}
			public:
				Context();
		};

		typedef QMap<QString, QFReportProcessorItem::Image> ImageMap;
	protected:
		Context fContext;
		Context& contextRef() {return fContext;}

		ImageMap fImageMap;
		//ImageMap& imagesRef() {return fImageMap;}

		static QFSearchDirs f_searchDirs;

		Document fReport;
		QFXmlTable fData;
		QPaintDevice *fPaintDevice;
		//! pri prekladu xml reporu vznika strom odpovidajicich QFReportProcessorItem objektu a toto je jejich root.
		QFReportProcessorItem *fProcessedItemsRoot;
		//! pri QFReportProcessorItem objekty generuji pomoci metody \a printMetaPaint() objekty QFReportItemMetaPaint a toto je jejich root.
		QFReportItemMetaPaintReport *f_processorOutput;

		int fProcessedPageNo;
		//QThread *processorThread;
		QFReportProcessorItem::PrintResult singlePageProcessResult;
	protected:
		QFReportProcessorScriptDriver *f_scriptDriver;
	public:
		QFReportProcessorScriptDriver* scriptDriver();
	protected:
		void makeContext();
		//void readStyleSheet(const QFDomElement &el_stylesheet);
	public:
		const Context& context() const {return fContext;}
	public:
		/// vymaze vsechna data vznikla predchozimi kompilacemi
		void reset();
		//virtual void paintReport(const QString &rep_file_name, const QString &dat_file_name, Options options = Options()) throw(QFException);
		void setReport(const QFDomDocument &doc);
		void setReport(const QString &rep_file_name) throw(QFException);
		QFDomDocument report() {return fReport;}
		QFDomDocument& reportRef() {return fReport;}
		void setData(const QFXmlTable &_data);
		const QFXmlTable& data() const {return fData;}

		void addImage(const QString key, const QFReportProcessorItem::Image &img) {fImageMap[key] = img;}
		const ImageMap& images() const {return fImageMap;}
	public:
		void setPaintDevice(QPaintDevice *pd) {fPaintDevice = pd;}
		QPaintDevice* paintDevice() {
			QF_ASSERT(fPaintDevice, "paintDevice cannot be null");
			return fPaintDevice;
		}
		//! Vrati QFontMetricsF pro \a font a \a paintDevice() .
		//! Pokud je paintDevice NULL, vrati fontMetrics pro screen.
		QFontMetricsF fontMetrics(const QFont &font);
		/*
		QColor color(const QString &name_or_def);
		QPen pen(const QString &name_or_def);
		QBrush brush(const QString &name_or_def);
		QFont font(const QString &name_or_def);
		Style style(const QString &name_or_def);
		*/
		static bool isProcessible(const QFDomElement &el);
		//! vytvori item pro element a nastavi nektere deefaultni hodnoty atributu, postara se taky o atribut copyAttributesFrom.
		//! Pro vytvoreni kontkretniho itemu pak vola funkci createItem()
		QFReportProcessorItem* createProcessibleItem(const QFDomElement &el, QFReportProcessorItem *parent) throw(QFException);
		virtual QFReportProcessorItem* createItem(QFReportProcessorItem *parent, const QFDomElement &el) throw(QFException);

		//! cislo stranky, ktera se zrovna zpracovava, pocitaji se od 0.
		int processedPageNo() const {return fProcessedPageNo;}

		static void appendSearchDir(const QString &path) {f_searchDirs.appendDir(path);}
		static void setSearchDirs(const QStringList &sl) {f_searchDirs.setDirs(sl);}
		/// oddelovac je kvuli windows "::"
		static void setSearchDirs(const QString &s) {f_searchDirs.setDirs(s);}
		static const QFSearchDirs &searchDirs() {return f_searchDirs;}
	protected:
		QFReportProcessorItem *processedItemsRoot();

		virtual QFReportProcessorItem::PrintResult processPage(QFReportItemMetaPaint *out);
		/// return NULL if such a page does not exist.
		QFReportItemMetaPaintFrame *getPage(int page_no);
	public:
		virtual void process(ProcessorMode mode = AllPages);
		void print(QPrinter &printer, const QVariantMap &options) throw(QFException);

		int pageCount();
		
		QFReportItemMetaPaintReport* processorOutput() {return f_processorOutput;}
		//QString resolveFileName(const QString &f_name) throw(QFException);
	public:
		/// vlozi do el_body report ve formatu HTML
		virtual void processHtml(QDomElement &el_body) throw(QFException);

		void dump();
	protected:
		void fixTableTags(QDomElement &el);
		QDomElement removeRedundantDivs(QDomElement &el);
		QDomElement fixLayoutHtml(QDomElement &el);
	signals:
		//! emitovan vzdy, kdyz procesor dokonci dalsi stranku.
		void pageProcessed();
	public slots:
		//! prelozi dalsi stranku reportu (takhle delam multithreading, protoze QFont musi bezet v GUI threadu)
		void processSinglePage() {process(SinglePage);}
	public:
		QFReportProcessor(QPaintDevice *paint_device, QObject *parent = NULL);
		virtual ~QFReportProcessor();
};

/*
QFont: It is not safe to use text and fonts outside the gui thread
		takze SORRY

class QFReportProcessorThread : public QThread
{
	protected:
		QFReportProcessor *processor;
		bool terminatedByUser;
	public:
		QFReportProcessorThread(QFReportProcessor *proc) : processor(proc), terminatedByUser(false) {}
		void run();
};
//=============================================================
//                                    QFReportProcessorThread
//=============================================================
void QFReportProcessorThread::run()
{
}
*/
#endif // QFREPORTPROCESSOR_H

