
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfreportviewwidget.h"

#include <qfreportpainter.h>
#include <qfaction.h>
#include <qftoolbar.h>
#include <qfpixmapcache.h>
#include <qfstatusbar.h>
#include <qfsmtpclient.h>
#include <qffileutils.h>
#include <qfhtmlutils.h>
#include <qfhtmlviewutils.h>
#include <qfdlghtmlview.h>
#include <qfdlgexception.h>
#include <qfapplication.h>
#include <qfmessage.h>

#include <QtGui>
#include <typeinfo>

#include <qflogcust.h>

//====================================================
//                                 QFReportViewWidget::PainterWidget
//====================================================
QFReportViewWidget::PainterWidget::PainterWidget(QWidget *parent)
	: QWidget(parent)
{
	//screenDPMm = QPainter(this).device()->physicalDpiX() / 25.4;
	//setAutoFillBackground(true);
	//resize(210*2+20, 297*2+20);
}

QFReportViewWidget* QFReportViewWidget::PainterWidget::reportViewWidget()
{
	return qfFindParent<QFReportViewWidget*>(this);
}

void QFReportViewWidget::PainterWidget::paintEvent(QPaintEvent *ev)
{
	qfTrash() << QF_FUNC_NAME;
	QWidget::paintEvent(ev);
	QFReportPainter painter(this);

	/// nakresli ramecek a stranku
	//painter.setBrush(Qt::yellow);
	QRect r1 = rect();
	painter.fillRect(r1, QBrush(QColor("#CCFF99")));
	//int d = (int)(QFReportViewWidget::PageBorder * reportViewWidget()->scale());
	//r.adjust(d, d, -d, -d);
	/*
	QPen p(Qt::blue);
	p.setBrush(QColor("red"));
	//p.setWidth(2);
	painter.setPen(p);
	painter.drawRect(rect().adjusted(0, 0, -1, -1));
	painter.setBrush(Qt::green);
	QFont f("autobus", 30);
	f.setStyleHint(QFont::Times);
	painter.setFont(f);
	//painter.drawText(r, Qt::AlignCenter | Qt::TextWordWrap, "<qt>Qt <b>kjutyn</b> <br>indian</qt>");
	*/
	reportViewWidget()->setupPainter(&painter);
	QFReportItemMetaPaintFrame *frm = reportViewWidget()->currentPage();
	if(!frm) return;
	QFGraphics::Rect r = QFGraphics::mm2device(frm->renderedRect, painter.device());
	painter.fillRect(r, QColor("white"));
	painter.setPen(QColor("teal"));
	painter.setBrush(QBrush());
	painter.drawRect(r);

	painter.drawMetaPaint(reportViewWidget()->currentPage());
	//painter.setPen(p);
	//QRect r(0, 0, 210, 297);
	//painter.drawText(r, Qt::AlignCenter | Qt::TextWordWrap, "<qt>Qt <b>kjutyn</b> <br>indian</qt>");
}

void QFReportViewWidget::PainterWidget::mousePressEvent(QMouseEvent *e)
{
	QFReportItemMetaPaint::Point p = QPointF(e->pos());
	p = reportViewWidget()->painterInverseMatrix.map(p);
	p = QFGraphics::device2mm(p, this);
	qfTrash() << QF_FUNC_NAME << QFReportProcessorItem::Point(p).toString();
	reportViewWidget()->selectItem(p);
	QFReportItemMetaPaint *selected_item = reportViewWidget()->selectedItem();
	if(selected_item) {
		if(e->button() == Qt::RightButton) {
			qfTrash() << "\t item type:" << typeid(selected_item).name();
			QFReportItemMetaPaintText *it = dynamic_cast<QFReportItemMetaPaintText*>(selected_item->firstChild());
			if(it) {
				QMenu menu(this);
				menu.setTitle(tr("Item menu"));
				QAction *act_edit = menu.addAction(tr("Editovat text"));
				QAction *a = menu.exec(mapToGlobal(e->pos()));
				if(a == act_edit) {
					bool ok;
					QString text = QInputDialog::getText(this, tr("Editovat text"), tr("Novy text:"), QLineEdit::Normal, it->text, &ok);
					if(ok && !text.isEmpty()) {
						it->text = text;
						/// roztahni text na ohranicujici ramecek, aby se tam delsi text vesel
						it->setRenderedRectRect(selected_item->renderedRect);
						update();
					}
				}
			}
		}
	}
}

//====================================================
//                                 QFReportViewWidget
//====================================================
QFReportViewWidget::QFReportViewWidget(QWidget *parent)
	: QFDialogWidget(parent), scrollArea(NULL), edCurrentPage(NULL), f_statusBar(NULL)
{
	f_reportProcessor = NULL;
	whenRenderingSetCurrentPageTo = -1;
	
	f_uiBuilder = new QFUiBuilder(this, ":/libqfgui/qfreportviewwidget.ui.xml");
	f_actionList += f_uiBuilder->actions();

	action("file.export.email")->setVisible(false);
	action("report.edit")->setVisible(false);

	fCurrentPageNo = -1;
	f_selectedItem = NULL;

	scrollArea = new ScrollArea(NULL);
	/// zajimavy, odkomentuju tenhle radek a nemuzu nastavit pozadi zadnyho widgetu na scrollArea.
	//scrollArea->setBackgroundRole(QPalette::Dark);
	painterWidget = new PainterWidget(scrollArea);
	scrollArea->setWidget(painterWidget);
	QBoxLayout *ly = new QVBoxLayout(centralWidget());
	ly->setSpacing(0);
	ly->setMargin(0);
	ly->addWidget(scrollArea);
	ly->addWidget(statusBar());

	connect(scrollArea, SIGNAL(showNextPage()), this, SLOT(scrollToNextPage()));
	connect(scrollArea, SIGNAL(showPreviousPage()), this, SLOT(scrollToPrevPage()));

	QFUiBuilder::connectActions(f_actionList, this);
}

QFReportViewWidget::~QFReportViewWidget()
{
}

QFReportProcessor * QFReportViewWidget::reportProcessor()
{
	if(f_reportProcessor == NULL) {
		setReportProcessor(new QFReportProcessor(painterWidget, this));
	}
	return f_reportProcessor;
}

void QFReportViewWidget::setReportProcessor(QFReportProcessor * proc)
{
	f_reportProcessor = proc;
	connect(f_reportProcessor, SIGNAL(pageProcessed()), this, SLOT(pageProcessed()));
}

QFStatusBar* QFReportViewWidget::statusBar()
{
	if(!f_statusBar) {
		f_statusBar = new QFStatusBar(NULL);
		zoomStatusSpinBox = new QSpinBox();
		zoomStatusSpinBox->setSingleStep(10);
		zoomStatusSpinBox->setMinimum(10);
		zoomStatusSpinBox->setMaximum(1000000);
		zoomStatusSpinBox->setPrefix("zoom: ");
		zoomStatusSpinBox->setSuffix("%");
		zoomStatusSpinBox->setAlignment(Qt::AlignRight);
		f_statusBar->addWidget(zoomStatusSpinBox);
		connect(zoomStatusSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setScaleProc(int)));
	}
	return f_statusBar;
}

QFPart::ToolBarList QFReportViewWidget::createToolBars()
{
	qfTrash() << QF_FUNC_NAME;
	QFPart::ToolBarList tool_bars;
	//if(!f_toolBars.isEmpty()) return f_toolBars;

	tool_bars = f_uiBuilder->createToolBars();

	/// je to dost divoky, najdu toolbar jmenem "main" a v nem widget majici akci s id == "view.nextPage"
	/// a pred ni prdnu lineEdit, pukud pridam do toolbaru widget, musim na konec pridat i Stretch :(
	foreach(QFToolBar *tb, tool_bars) {
		//qfTrash() << "\ttoolbar object name:" << tb->objectName();
		if(tb->objectName() == "main") {
			QLayout *ly = tb->layout();
			//QF_ASSERT(ly, "bad layout cast");
			for(int i = 0; i < ly->count(); ++i) {
				QWidget *w = ly->itemAt(i)->widget();
				//qfTrash() << "\twidget:" << w;
				if(w) {
					QList<QAction*> alst = w->actions();
					if(!alst.isEmpty()) {
						QFAction *a = qobject_cast<QFAction*>(alst[0]);
						if(a) {
							//qfTrash() << "\taction id:" << a->id();
							if(a->id() == "view.nextPage") {
								edCurrentPage = new QLineEdit(NULL);
								edCurrentPage->setAlignment(Qt::AlignRight);
								edCurrentPage->setMaximumWidth(60);
								connect(edCurrentPage, SIGNAL(editingFinished()), this, SLOT(edCurrentPageEdited()));
								tb->insertWidget(a, edCurrentPage);
								QLabel *space = new QLabel(QString(), NULL);
								space->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
								tb->addWidget(space); 
								//QSpacerItem *spacer = new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum);
								//ly->addItem(spacer);
/*
								ly->insertWidget(i, edCurrentPage);
								ly->addStretch();
								*/
								break;
							}
						}
					}
				}
			}
			break;
		}
	}

	return tool_bars;
}

void QFReportViewWidget::view_zoomIn()
{
	qfTrash() << QF_FUNC_NAME;
	if(scale() > 1) setScale(scale() + 1);
	else setScale(scale() * 1.33);
}

void QFReportViewWidget::view_zoomOut()
{
	qfTrash() << QF_FUNC_NAME;
	if(scale() > 2) setScale(scale() - 1);
	else setScale(scale() / 1.33);
}

void QFReportViewWidget::view_zoomToFitWidth()
{
	qfTrash() << QF_FUNC_NAME;
	QFReportItemMetaPaintFrame *frm = currentPage();
	if(!frm) return;
	QFReportItemMetaPaintFrame::Rect r = frm->renderedRect;
	double report_px = (r.width() + 2*PageBorder) * painterWidget->logicalDpiX() / 25.4;
	double widget_px = scrollArea->width();
	//QScrollBar *sb = scrollArea->verticalScrollBar();
	//if(sb) widget_px -= sb->width();
	double sc = widget_px / report_px * 0.98;
	setScale(sc);
}

void QFReportViewWidget::view_zoomToFitHeight()
{
	QFReportItemMetaPaintFrame *frm = currentPage();
	if(!frm) return;
	QFReportItemMetaPaintFrame::Rect r = frm->renderedRect;
	double report_px = (r.height() + 2*PageBorder) * painterWidget->logicalDpiY() / 25.4;
	double widget_px = scrollArea->height();
	double sc = widget_px / report_px * 0.98;
	setScale(sc);
}

void QFReportViewWidget::setScale(qreal _scale)
{
	qfTrash() << QF_FUNC_NAME << currentPageNo();
	QFReportItemMetaPaintFrame *frm = currentPage();
	if(!frm) return;

	f_scale = _scale;
	setupPainterWidgetSize();
	painterWidget->update();
	refreshWidget();
}

void QFReportViewWidget::setupPainterWidgetSize()
{
	qfTrash() << QF_FUNC_NAME;
	QFReportItemMetaPaintFrame *frm = currentPage();
	if(!frm) return;
	QFGraphics::Rect r1 = frm->renderedRect.adjusted(-PageBorder, -PageBorder, PageBorder, PageBorder);
	QFGraphics::Rect r2 = QFGraphics::mm2device(r1, painterWidget);
	//qfTrash() << "\tframe rect:" << r.toString();
	QSizeF s = r2.size();
	s *= scale();
	//painterScale = QSizeF(s.width() / r1.width(), s.height() / r1.height());
	painterWidget->resize(s.toSize());
}

void QFReportViewWidget::setupPainter(QFReportPainter *p)
{
	QF_ASSERT(p, "painter is NULL");
	//qfInfo() << QF_FUNC_NAME;
	//qfInfo() << "\t painterScale:" << QFReportProcessorItem::Size(painterScale).toString();
	//p->currentPage = currentPageNo();
	p->pageCount = pageCount();
	//QFDomElement el;
	//if(f_selectedItem) el = f_selectedItem->reportElement;
	p->setSelectedItem(f_selectedItem);
	p->scale(scale(), scale());
	//p->scale(painterScale.width(), painterScale.height());
	//qfInfo() << "\t painter world matrix m11:" << p->worldMatrix().m11() << "m12:" << p->worldMatrix().m12();
	//qfInfo() << "\t painter world matrix m21:" << p->worldMatrix().m21() << "m22:" << p->worldMatrix().m22();
	p->translate(QFGraphics::mm2device(QFGraphics::Point(PageBorder, PageBorder), p->device()));
	painterInverseMatrix = p->matrix().inverted();
}
/*
void QFReportViewWidget::setDocument(QFReportItemMetaPaint* doc)
{
	qfTrash() << QF_FUNC_NAME;
	fDocument = doc;
	if(!doc) return;
	//doc->dump();
}
	*/
void QFReportViewWidget::setReport(const QString &file_name)
{
	qfLogFuncFrame() << "file_name:" << file_name;
	//qfTrash() << "\tdata:" << fData.toString();
	reportProcessor()->setReport(file_name);
	reportProcessor()->setData(fData.firstChildElement());
	//out.dump();
}

void QFReportViewWidget::setReport(const QFDomDocument & doc)
{
	reportProcessor()->setReport(doc);
	reportProcessor()->setData(fData.firstChildElement());
}

void QFReportViewWidget::pageProcessed()
{
	qfTrash() << QF_FUNC_NAME;
	if(whenRenderingSetCurrentPageTo >= 0) {
		if(pageCount() - 1 == whenRenderingSetCurrentPageTo) {
			setCurrentPageNo(whenRenderingSetCurrentPageTo);
			whenRenderingSetCurrentPageTo = -1;
		}
	}
	else {
		if(pageCount() == 1) setCurrentPageNo(0);
	}
	//QApplication::processEvents();
	refreshWidget();
	//setCurrentPageNo(0);
	QTimer::singleShot(10, reportProcessor(), SLOT(processSinglePage())); /// 10 je kompromis mezi rychlosti prekladu a sviznosti GUI
}

QFReportItemMetaPaintReport* QFReportViewWidget::document(bool throw_exc) throw(QFException)
{
	QFReportItemMetaPaintReport *doc = reportProcessor()->processorOutput();
	if(!doc && throw_exc) QF_EXCEPTION("document is NULL");
	return doc;
}

int QFReportViewWidget::pageCount()
{
	qfLogFuncFrame();
	int ret = 0;
	if(document(!Qf::ThrowExc)) {
		ret = document()->childrenCount();
	}
	else {
		qfTrash() << "\tdocument is null";
	}
	qfTrash() << "\treturn:" << ret;
	return ret;
}

void QFReportViewWidget::setCurrentPageNo(int pg_no)
{
	if(pg_no >= pageCount() || pg_no < 0) pg_no = 0;
	fCurrentPageNo = pg_no;
	setupPainterWidgetSize();
	painterWidget->update();
	refreshWidget();
}

QFReportItemMetaPaintFrame* QFReportViewWidget::getPage(int n)
{
	qfTrash() << QF_FUNC_NAME << currentPageNo();
	if(!document(!Qf::ThrowExc)) return NULL;
	if(n < 0 || n >= document()->childrenCount()) return NULL;
	QFReportItemMetaPaint *it = document()->childAt(n);
	QFReportItemMetaPaintFrame *frm	= dynamic_cast<QFReportItemMetaPaintFrame*>(it);
	qfTrash() << "\treturn:" << frm;
	return frm;
}

QFReportItemMetaPaintFrame* QFReportViewWidget::currentPage()
{
	QFReportItemMetaPaintFrame *frm = getPage(currentPageNo());
	//qfTrash() << QF_FUNC_NAME << currentPageNo();
	if(!frm) return NULL;
	return frm;
}

void QFReportViewWidget::selectElement_helper(QFReportItemMetaPaint *it, const QFDomElement &el)
{
	if(it->reportElement == el) {
		//qfInfo() << "BINGO";
		f_selectedItem = it;
		painterWidget->update();
		qfTrash() << "\t EMIT:" << f_selectedItem->reportElement.tagName();
		emit elementSelected(f_selectedItem->reportElement);
	}
	else {
		foreach(QFTreeItemBase *_it, it->children()) {
			QFReportItemMetaPaint *it1 = static_cast<QFReportItemMetaPaint*>(_it);
			selectElement_helper(it1, el);
		}
	}
}

void QFReportViewWidget::selectElement(const QFDomElement &el)
{
	qfLogFuncFrame() << el.tagName();
	if(!el) return;
	//qfInfo() << __LINE__;
	if(f_selectedItem && f_selectedItem->reportElement == el) {
		qfTrash() << "\t allready selected";
		return;
	}
	f_selectedItem = NULL;
	QFReportItemMetaPaintFrame *frm = currentPage();
	if(!frm) return;
	selectElement_helper(frm, el);
	if(!f_selectedItem) {
		//qfInfo() << "vubec ho nenasel";
		/// vubec ho nenasel
		f_selectedItem = NULL;
		//fSelectedElement = QFDomElement();
		update();
		//emit elementSelected(fSelectedElement);
	}
}

static bool is_fake_element(const QFDomElement &_el)
{
	qfLogFuncFrame();
	bool ret = false;
	QFDomElement el = _el;
	while(!!el) {
		if(0) {
			QString s;
			QDomNamedNodeMap attrs = el.attributes();
			for(int i=0; i<attrs.count(); i++) {
				QDomNode n = attrs.item(i);
				if(i > 0) s += " ";
				s += n.nodeName() + "=\"" + n.nodeValue() + "\"";
			}
			qfTrash() << "\t el:" << el.tagName() << s;
		}
		if(el.hasAttribute("__fake")) {
			ret = true;
			break;
		}
		el = el.parentNode().toElement();
	}
	qfTrash() << "\t return:" << ret;
	return ret;
}

bool QFReportViewWidget::selectItem_helper(QFReportItemMetaPaint *it, const QPointF &p)
{
	if(it->isPointInside(p)) {
		qfTrash() << QF_FUNC_NAME << "point inside:" << it->renderedRect.toString();
		//qfInfo() << it->dump();
		bool in_child = false;
		foreach(QFTreeItemBase *_it, it->children()) {
			QFReportItemMetaPaint *it1 = static_cast<QFReportItemMetaPaint*>(_it);
			if(selectItem_helper(it1, p)) {in_child = true; break;}
		}
		if(!in_child) {
			bool selectable_element_found = false;
			if(f_selectedItem != it) {
				/// muze se stat, ze item nema element, pak hledej u rodicu
				while(it) {
					QFDomElement el = it->reportElement;
					if(!!el) {
						/// nalezeny element nesmi pochazet z fakeBand, pozna se to tak, ze element nebo nektery z jeho predku ma atribut "__fake"
						if(!is_fake_element(el)) {
							if(el.attribute("unselectable").toBool()) { break; }
							selectable_element_found = true;
							f_selectedItem = it;
							//qfTrash() << "element:" << fSelectedElement.toString();
							painterWidget->update();
							qfTrash() << "\t EMIT:" << el.tagName();
							emit elementSelected(el);
							break;
						}
					}
					it = it->parent();
				}
			}
			return selectable_element_found;
		}
		return true;
	}
	return false;
}

void QFReportViewWidget::selectItem(const QPointF &p)
{
	qfLogFuncFrame();
	QFReportItemMetaPaintFrame *frm = currentPage();
	QFReportItemMetaPaint *old_selected_item = f_selectedItem;
	//QFDomElement old_el = fSelectedElement;
	f_selectedItem = NULL;
	if(frm) selectItem_helper(frm, p);
	if(!f_selectedItem && old_selected_item) {
		/// odznac puvodni selekci
		painterWidget->update();
	}
}

void QFReportViewWidget::setVisible(bool visible)
{
	qfTrash() << QF_FUNC_NAME;
	//setCurrentPageNo(0);
	QFDialogWidget::setVisible(visible);
	//setCurrentPageNo(0);
	//QTimer::singleShot(0, this, SLOT(processReport()));
	if(visible) processReport();
}

void QFReportViewWidget::processReport()
{
	qfLogFuncFrame();
	if(!reportProcessor()->processorOutput()) {
		reportProcessor()->process(QFReportProcessor::FirstPage);
	}
	setCurrentPageNo(0);
	setScale(1);
	//fDocument = reportProcessor()->processorOutput();
}

void QFReportViewWidget::render()
{
	qfLogFuncFrame();
	whenRenderingSetCurrentPageTo = currentPageNo();
	reportProcessor()->reset();
	if(!reportProcessor()->processorOutput()) {
		//qfInfo() << "process report";
		reportProcessor()->process(QFReportProcessor::FirstPage);
	}
	//qfInfo() << "setCurrentPageNo:" << cur_page_no;
	//setCurrentPageNo(cur_page_no);
}

void QFReportViewWidget::refreshWidget()
{
	statusBar();
	if(edCurrentPage) edCurrentPage->setText(QString::number(currentPageNo()+1) + "/" + QString::number(pageCount()));
	refreshActions();
	zoomStatusSpinBox->setValue((int)(scale() * 100));
	//statusBar()->setText("zoom: " + QString::number((int)(scale() * 100)) + "%");
}

void QFReportViewWidget::refreshActions()
{
	int pgno = currentPageNo();
	int pgcnt = pageCount();
	action("view.firstPage")->setEnabled(pgno > 0 && pgcnt > 0);
	action("view.prevPage")->setEnabled(pgno > 0 && pgcnt > 0);
	action("view.nextPage")->setEnabled(pgno < pgcnt - 1);
	action("view.lastPage")->setEnabled(pgno < pgcnt - 1);
}

void QFReportViewWidget::view_nextPage(PageScrollPosition scroll_pos)
{
	qfTrash() << QF_FUNC_NAME;
	if(currentPageNo() < pageCount() - 1) {
		setCurrentPageNo(currentPageNo() + 1);
		if(scroll_pos == ScrollToPageTop) scrollArea->verticalScrollBar()->setValue(scrollArea->verticalScrollBar()->minimum());
		else if(scroll_pos == ScrollToPageEnd) scrollArea->verticalScrollBar()->setValue(scrollArea->verticalScrollBar()->maximum());
	}
}

void QFReportViewWidget::view_prevPage(PageScrollPosition scroll_pos)
{
	qfTrash() << QF_FUNC_NAME;
	if(currentPageNo() > 0) {
		setCurrentPageNo(currentPageNo() - 1);
		if(scroll_pos == ScrollToPageTop) scrollArea->verticalScrollBar()->setValue(scrollArea->verticalScrollBar()->minimum());
		else if(scroll_pos == ScrollToPageEnd) scrollArea->verticalScrollBar()->setValue(scrollArea->verticalScrollBar()->maximum());
	}
}

void QFReportViewWidget::view_firstPage()
{
	qfTrash() << QF_FUNC_NAME;
	int pgno = 0;
	if(pgno < pageCount()) setCurrentPageNo(pgno);
}

void QFReportViewWidget::view_lastPage()
{
	qfTrash() << QF_FUNC_NAME;
	int pgno = pageCount() - 1;
	if(pgno >= 0) setCurrentPageNo(pgno);
}

void QFReportViewWidget::edCurrentPageEdited()
{
	qfTrash() << QF_FUNC_NAME;
	QStringList sl = edCurrentPage->text().split("/");
	if(sl.count() > 0) {
		int pg = sl[0].toInt() - 1;
		setCurrentPageNo(pg);
	}
}
/*
void QFReportViewWidget::printOnDefaultPrinter() throw(QFException)
{
}
*/
void QFReportViewWidget::print(QPrinter &printer, const QVariantMap &options) throw(QFException)
{
	qfLogFuncFrame();

	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

	QFReportPainter painter(&printer);

	typedef QFReportProcessorItem::Rect Rect;
	typedef QFReportProcessorItem::Size Size;

	int pg_no = options.value("fromPage", 1).toInt() - 1;
	int to_page = options.value("toPage", pageCount()).toInt();
	qfTrash() << "pg_no:" << pg_no << "to_page:" << to_page;
	QFReportItemMetaPaintFrame *frm = getPage(pg_no);
	if(frm) {
		Rect r = frm->renderedRect;
		bool landscape = r.width() > r.height();
		if(landscape) printer.setOrientation(QPrinter::Landscape);
		Rect printer_pg_rect = QRectF(printer.pageRect());
		//qfWarning() << "\tprinter page rect:" << printer_pg_rect.toString();
		//qfWarning() << "\tresolution:" << printer.resolution() << Size(printer_pg_rect.size()/printer.resolution()).toString(); /// resolution je v DPI
		//qreal magnify = printer_pg_rect.width() / r.width();
		//painter.scale(magnify, magnify);
		painter.pageCount = pageCount();
		while(frm) {
			//painter.currentPage = pg_no;
			painter.drawMetaPaint(frm);
			pg_no++;
			frm = getPage(pg_no);
			if(!frm) break;
			if(pg_no >= to_page) break;
			printer.newPage();
		}
	}

	QApplication::restoreOverrideCursor();
	//emit reportPrinted();
}

void QFReportViewWidget::print() throw(QFException)
{
	qfLogFuncFrame();

	QPrinter printer;
	printer.setOutputFormat(QPrinter::NativeFormat);
	//printer.setOutputFileName(fn);
	printer.setFullPage(true);
	printer.setPageSize(QPrinter::A4);
	printer.setOrientation(document()->orientation);

	QPrintDialog dlg(&printer, this);
	if(dlg.exec() != QDialog::Accepted) return;

	qfTrash() << "options:" << dlg.options();
	QVariantMap opts;
	if(dlg.testOption(QAbstractPrintDialog::PrintPageRange)) { /// tohle je nastaveny vzdycky :(
		int from_page = dlg.fromPage();
		int to_page = dlg.toPage();
		qfTrash() << "fromPage:" << dlg.fromPage() << "toPage:" << dlg.toPage();
		if(from_page > 0) opts["fromPage"] = dlg.fromPage();
		if(to_page > 0) opts["toPage"] = dlg.toPage();
	}

	print(printer, opts);
}

void QFReportViewWidget::exportPdf(const QString &file_name) throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	QFString fn = file_name;
	if(!fn) QF_EXCEPTION(tr("empty file name"));
	if(!fn.toLower().endsWith(".pdf")) fn += ".pdf";

	QPrinter printer;
	printer.setOutputFormat(QPrinter::PdfFormat);
	printer.setOutputFileName(fn);
	printer.setFullPage(true);
	printer.setPageSize(QPrinter::A4);
	printer.setOrientation(document()->orientation);
	/// zatim bez dialogu
	//QPrintDialog printDialog(&printer, parentWidget());
	//if(printDialog.exec() != QDialog::Accepted) return;

	print(printer);
}

QString QFReportViewWidget::exportHtml() throw( QFException )
{
	qfLogFuncFrame();
	QFDomDocument doc = QFHtmlUtils::bodyToHtmlDocument(QString());
	QFDomElement el_body = doc.cd("/body");
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	reportProcessor()->processHtml(el_body);
	QApplication::restoreOverrideCursor();
	return doc.toString();
}

void QFReportViewWidget::file_export_pdf()
{
	qfTrash() << QF_FUNC_NAME;
	//reportProcessor()->dump();
	QFString fn = qfApp()->getSaveFileName (this, tr("Save as PDF"), QString(), "*.pdf");
	if(!fn) return;
	exportPdf(fn);
}

void QFReportViewWidget::file_export_html()
{
	qfLogFuncFrame();
	QFString fn = "report.html";
	QString s = exportHtml(); 
	QFHtmlViewUtils hut(!QFHtmlViewUtils::UseWebKit);
	hut.showOrSaveHtml(s, fn, "showOrSaveHtml");
}

void QFReportViewWidget::file_export_email()
{
	/// uloz pdf do tmp file report.pdf
	QString fn = QFFileUtils::appTempDir() + "/report.pdf";
	exportPdf(fn);
}

void QFReportViewWidget::data_showHtml()
{
	qfTrash() << QF_FUNC_NAME;
	QString s = reportProcessor()->data().toHtml();
	s = QFHtmlUtils::addHtmlEnvelope(s);
	QString file_name = "data.html";

	QFHtmlViewUtils hu(!QFHtmlViewUtils::UseWebKit);
	hu.showOrSaveHtml(s, file_name);
}

void QFReportViewWidget::file_print()
{
	qfTrash() << QF_FUNC_NAME;
	print();
}

//====================================================
//                                 QFReportViewWidget::ScrollArea
//====================================================
QFReportViewWidget::ScrollArea::ScrollArea(QWidget * parent)
	: QScrollArea(parent)
{
	connect(verticalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(verticalScrollBarValueChanged(int)));
}

void QFReportViewWidget::ScrollArea::wheelEvent(QWheelEvent * e)
{
	if(e->orientation() == Qt::Vertical) {
		QScrollBar *sb = verticalScrollBar();
		if(!sb || !sb->isVisible()) {
			/// pokud neni scroll bar, nemuzu se spolehnout na funkci verticalScrollBarValueChanged(), protoze value je pro oba smery == 0
			//qfInfo() << e->delta();
			if(e->delta() < 0) {
				emit showNextPage();
			}
			else {
				emit showPreviousPage();
			}
			e->accept();
			return;
		}
	}
	QScrollArea::wheelEvent(e);
}

void QFReportViewWidget::ScrollArea::verticalScrollBarValueChanged(int value)
{
	qfLogFuncFrame() << "value:" << value;
	//qfInfo() << value;
	static int old_val = -1;
	QScrollBar *sb = verticalScrollBar();
	if(value == old_val) {
		if(value == sb->maximum()) {
			emit showNextPage();
		}
		else if(value == sb->minimum()) {
			emit showPreviousPage();
		}
	}
	old_val = value;
}






