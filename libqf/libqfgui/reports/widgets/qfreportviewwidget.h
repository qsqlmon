//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFREPORTVIEWWIDGET_H
#define QFREPORTVIEWWIDGET_H

#include <qf.h>
#include <qfdom.h>
#include <qfguiglobal.h>
#include <qfexception.h>
#include <qfdialogwidget.h>
#include <qfuibuilder.h>

#include <QScrollArea>
#include <QFrame>
#include <QMatrix>

class QFAction;
class QFReportItemMetaPaint;
class QFReportItemMetaPaintReport;
class QFReportItemMetaPaintFrame;
class QFReportPainter;
class QFReportProcessor;
class QFStatusBar;
class QLineEdit;
class QSpinBox;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT  QFReportViewWidget : public QFDialogWidget
{
	Q_OBJECT
	protected:
		class ScrollArea;
		class PainterWidget;
		PainterWidget *painterWidget;
		ScrollArea *scrollArea;

		QFPart::ActionList f_actionList;
		QFUiBuilder *f_uiBuilder;

		//QFReportItemMetaPaint *fDocument;
		QFDomDocument fData;
		int fCurrentPageNo;
		qreal f_scale;

		int whenRenderingSetCurrentPageTo;

		QLineEdit *edCurrentPage;
		//QFPart::ToolBarList f_toolBars;
		//qreal mm2pxFactor;
		//QMap<QString, QFAction*> actionMap;
		//QList<QFToolBar*> fToolBarList;

		QFReportItemMetaPaint *f_selectedItem; 
		//QFDomElement fSelectedElement;
		QMatrix painterInverseMatrix;
		//QSizeF painterScale;

		QFReportProcessor *f_reportProcessor;

		static const int PageBorder = 5;

		QFStatusBar *f_statusBar; 
	private:
		void selectElement_helper(QFReportItemMetaPaint *it, const QFDomElement &el);
		void selectItem(const QPointF &p);
		bool selectItem_helper(QFReportItemMetaPaint *it, const QPointF &p);
	protected:
		QFStatusBar *statusBar();
		QSpinBox *zoomStatusSpinBox;

		/// nastavi painteru scale a offset
		void setupPainter(QFReportPainter *p);
		//! nastavi velikost widgetu podle rozmeru aktualni stranky a aktualniho zvetseni.
		void setupPainterWidgetSize();

		void refreshWidget();
		void refreshActions(); 

		void print() throw(QFException);
		void exportPdf(const QString &file_name) throw(QFException);
		QString exportHtml() throw(QFException);
	signals:
		//! pokud je report vytisknut nepo exportovan do PDF
		//void reportPrinted();
		void elementSelected(const QFDomElement &el);
	protected slots:
		void edCurrentPageEdited();
		void pageProcessed();
		void scrollToPrevPage() {view_prevPage(ScrollToPageEnd);}
		void scrollToNextPage() {view_nextPage(ScrollToPageTop);}
		void setScaleProc(int proc) {setScale(proc * 0.01);}
	public:
		enum PageScrollPosition {ScrollToPageTop, ScrollToPageEnd};
	public slots:
		/// prerendruje report
		void render();
		//! Zacne prekladat report a jak pribyvaji stranky, zobrazuji se ve view, nemuzu pro to pouzit specialni thread,
		//! protoze QFont musi byt pouzivan v GUI threadu, tak prekladam stranku po strance pomoci QTimer::singleShot()
		void processReport();
		void selectElement(const QFDomElement &el);

		//void printOnDefaultPrinter() throw(QFException);
		
		void file_print();
		void file_export_pdf();
		void file_export_html();
		virtual void file_export_email();
		virtual void report_edit() {}

		void data_showHtml();

		void view_nextPage(PageScrollPosition scroll_pos = ScrollToPageTop);
		void view_prevPage(PageScrollPosition scroll_pos = ScrollToPageTop);
		void view_firstPage();
		void view_lastPage();
		void view_zoomIn();
		void view_zoomOut();
		void view_zoomToFitWidth();
		void view_zoomToFitHeight();
	public:
		virtual QFReportProcessor* reportProcessor();
		//! does not take ownership of \a proc
		//! connect necessarry signals and slots
		void setReportProcessor(QFReportProcessor *proc);

		virtual void setVisible(bool visible);

		///=================== INTERFACE ================
		virtual QFPart::ToolBarList createToolBars();
		//virtual bool hasMenubar() {return true;}
		virtual void updateMenuOrBar(QWidget *menu_or_menubar) {f_uiBuilder->updateMenuOrBar(menu_or_menubar);}
		///===============================================

		QFReportItemMetaPaintReport* document(bool throw_exc = Qf::ThrowExc) throw(QFException);
		// widget does not take the ownership of the document \a doc .
		//void setDocument(QFReportItemMetaPaint* doc);
		const QFDomDocument& data() const;
		void setData(const QFDomDocument &_data) {fData = _data;}
		//! Volani teto funkce zpusobi prelozeni reportu, vlozeni pripadnych dat a jeho zobrazeni.
		void setReport(const QString &file_name);
		void setReport(const QFDomDocument &doc);

		/// stranky se pocitaji od 0
		int currentPageNo() const {return fCurrentPageNo;}
		/// stranky se pocitaji od 0
		void setCurrentPageNo(int pg_no);
		int pageCount();
		QFReportItemMetaPaintFrame *currentPage();
		/// return NULL if such a page does not exist.
		QFReportItemMetaPaintFrame *getPage(int page_no);

		qreal scale() const {return f_scale;}
		void setScale(qreal _scale);

		QFAction* action(const QString &name, bool throw_exc = Qf::ThrowExc) throw(QFException)
		{
			return QFUiBuilder::findAction(f_actionList, name, throw_exc);
		}

		QFReportItemMetaPaint* selectedItem() const {return f_selectedItem;}
		//const QList<QFToolBar*>& toolBars() {return fToolBarList;}
		void print(QPrinter &printer, const QVariantMap &options = QVariantMap()) throw(QFException);
	public:
		QFReportViewWidget(QWidget *parent = NULL);
		virtual ~QFReportViewWidget();
};

class QFReportViewWidget::PainterWidget : public QWidget
{
	Q_OBJECT
	protected:
		virtual void mousePressEvent(QMouseEvent *e);
		virtual void paintEvent(QPaintEvent *event);
		QFReportViewWidget* reportViewWidget();
		/// screen dots per mm
	public:
		PainterWidget(QWidget *parent);
		virtual ~PainterWidget() {}

		//double screenDPMm;
};

class QFReportViewWidget::ScrollArea : public QScrollArea
{
	Q_OBJECT;
	signals:
		void showNextPage();
		void showPreviousPage();
	protected slots:
		void verticalScrollBarValueChanged(int value);
	protected:
		virtual void wheelEvent(QWheelEvent *e);
	public:
		ScrollArea(QWidget *parent);
};



#endif // QFREPORTVIEWWIDGET_H

