
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFTABLEPRINTDIALOGWIDGET_H
#define QFTABLEPRINTDIALOGWIDGET_H

#include <qfguiglobal.h>
#include <qfdialogwidget.h>

namespace Ui {class QFTablePrintDialogWidget;};
class QFTableModel;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFTablePrintDialogWidget : public QFDialogWidget
{
	Q_OBJECT;
	private:
		Ui::QFTablePrintDialogWidget *ui;
	protected:
		QVariantMap f_allSettings;
	protected:
		QVariant settings() const;
		void setSettings(const QVariant &_settings);
	protected slots:
		void on_btSaveSettings_clicked();
		void on_btDeleteSetting_clicked();
		void on_lstSettings_activated(const QString &sett_name);

		void on_btColumnsAll_clicked();
		void on_btColumnsNone_clicked();
		void on_btColumnsInvert_clicked();
	public:
		QStringList modelColumnNames() const;
		QStringList tableFieldNames() const;
		QString reportFileName() const;
		QString reportTitle() const;
		bool isSelectedRowsOnly() const;

		void setXmlConfigPersistentId(const QString &id, bool load_persistent_data = true);
		virtual void savePersistentData();
		virtual void loadPersistentData();
		
	public:
		QFTablePrintDialogWidget(QFTableModel *model, QWidget *parent = NULL);
		virtual ~QFTablePrintDialogWidget();
};

#endif // QFTABLEPRINTDIALOGWIDGET_H

