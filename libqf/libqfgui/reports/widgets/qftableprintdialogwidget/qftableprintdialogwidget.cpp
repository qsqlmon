
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#include "ui_qftableprintdialogwidget.h"
#include "qftableprintdialogwidget.h"

#include <qffileutils.h>
#include <qftablemodel.h>
#include <qfjson.h>

#include <QDir>

#include <qflogcust.h>

//=================================================
//                                    QFTablePrintDialogWidget
//=================================================
QFTablePrintDialogWidget::QFTablePrintDialogWidget(QFTableModel *model, QWidget *parent)
	: QFDialogWidget(parent)
{
	qfLogFuncFrame() << model;
	ui = new Ui::QFTablePrintDialogWidget;
	ui->setupUi(centralWidget());
	Qf::connectSlotsByName(centralWidget(), this);
	ui->grpColumns->setVisible(model);
	if(model) {
		{
			QListWidget *w = ui->lstModelColumns;
			foreach(const QFTableModel::ColumnDefinition &cd, model->columns()) {
				QListWidgetItem *it = new QListWidgetItem(cd.caption());
				it->setData(Qt::UserRole, cd.fieldName());
				it->setCheckState(Qt::Unchecked);
				w->addItem(it);
			}
		}
		QFBasicTable *tbl = model->table();
		if(tbl) {
			QListWidget *w = ui->lstTableColumns;
			foreach(const QFSqlField &fld, tbl->fields()) {
				/// pokud je field soucasti modelu
				QListWidgetItem *it = new QListWidgetItem(fld.fullName());
				it->setCheckState(Qt::Unchecked);
				w->addItem(it);
			}
		}
	}
	{
		QComboBox *lst = ui->lstReportFileName;
		QDir dir(":/libqfgui/reports/");
		QStringList filters;
		filters << "*.rep";
		foreach(QString fn, dir.entryList(filters, QDir::Files, QDir::Name | QDir::Reversed)) {
			lst->addItem(QFFileUtils::baseName(fn), dir.absoluteFilePath(fn));
		}
		lst->setCurrentIndex(0);
	}
	ui->frmSettings->setVisible(false);
}

QFTablePrintDialogWidget::~QFTablePrintDialogWidget()
{
	qfLogFuncFrame();
	savePersistentData();
	delete ui;
}

void QFTablePrintDialogWidget::on_btSaveSettings_clicked()
{
	qfLogFuncFrame();
	QString setting_name = ui->lstSettings->currentText();
	if(!setting_name.isEmpty()) {
		f_allSettings[setting_name] = settings();
		if(ui->lstSettings->findText(setting_name) < 0) ui->lstSettings->addItem(setting_name);
		qfTrash() << "\t all settings:" << QFJson::variantToString(f_allSettings);
	}
}

void QFTablePrintDialogWidget::on_btDeleteSetting_clicked()
{
	qfLogFuncFrame();
	QString setting_name = ui->lstSettings->currentText();
	if(!setting_name.isEmpty()) {
		f_allSettings.remove(setting_name);
		int ix = ui->lstSettings->findText(setting_name);
		if(ix >= 0) ui->lstSettings->removeItem(ix);
	}
}

void QFTablePrintDialogWidget::on_lstSettings_activated(const QString &sett_name)
{
	QVariant v = f_allSettings.value(sett_name);
	if(v.isValid()) setSettings(v);
}

void QFTablePrintDialogWidget::on_btColumnsAll_clicked()
{
	QListWidget *w = ui->lstTableColumns;
	if(ui->columnsTabWidget->currentWidget() == ui->tabModel) w = ui->lstModelColumns;
	for(int i=0; i<w->count(); i++) w->item(i)->setCheckState(Qt::Checked);
}

void QFTablePrintDialogWidget::on_btColumnsNone_clicked()
{
	QListWidget *w = ui->lstTableColumns;
	if(ui->columnsTabWidget->currentWidget() == ui->tabModel) w = ui->lstModelColumns;
	for(int i=0; i<w->count(); i++) w->item(i)->setCheckState(Qt::Unchecked);
}

void QFTablePrintDialogWidget::on_btColumnsInvert_clicked()
{
	QListWidget *w = ui->lstTableColumns;
	if(ui->columnsTabWidget->currentWidget() == ui->tabModel) w = ui->lstModelColumns;
	for(int i=0; i<w->count(); i++) {
		QListWidgetItem *it = w->item(i);
		Qt::CheckState s = it->checkState();
		s = (s == Qt::Checked)? Qt::Unchecked: Qt::Checked;
		it->setCheckState(s);
	}
}

QStringList QFTablePrintDialogWidget::modelColumnNames() const
{
	QStringList ret;
	QListWidget *w = ui->lstModelColumns;
	for(int i=0; i<w->count(); i++) {
		QListWidgetItem *it = w->item(i);
		Qt::CheckState s = it->checkState();
		if(s == Qt::Checked) {
			ret << it->data(Qt::UserRole).toString();
		}
	}
	return ret;
}

QStringList QFTablePrintDialogWidget::tableFieldNames() const
{
	QStringList ret;
	QListWidget *w = ui->lstTableColumns;
	for(int i=0; i<w->count(); i++) {
		QListWidgetItem *it = w->item(i);
		Qt::CheckState s = it->checkState();
		if(s == Qt::Checked) {
			ret << it->text();
		}
	}
	return ret;
}

QString QFTablePrintDialogWidget::reportFileName() const
{
	QComboBox *lst = ui->lstReportFileName;
	QString curr_text = lst->currentText();
	int ix = lst->findText(curr_text);
	if(ix >= 0) return lst->itemData(lst->currentIndex()).toString();
	return curr_text;
}

bool QFTablePrintDialogWidget::isSelectedRowsOnly() const
{
	return ui->chkSelectedRowsOnly->isChecked();
}

QString QFTablePrintDialogWidget::reportTitle() const
{
	return ui->edReportTitle->text();
}

void QFTablePrintDialogWidget::setXmlConfigPersistentId(const QString &id, bool load_persistent_data)
{
	qfLogFuncFrame() << id;
	QFDialogWidget::setXmlConfigPersistentId(id, load_persistent_data);
	ui->frmSettings->setVisible(!xmlConfigPersistentId().isEmpty());
}

void QFTablePrintDialogWidget::savePersistentData()
{
	qfLogFuncFrame();
	if(xmlConfigPersistentId().isEmpty()) return;
	//QFDialogWidget::savePersistentData();
	qfTrash() << "\t setting persistent value:" << QFJson::variantToString(f_allSettings);
	setPersistentValue("settings", f_allSettings);
}

void QFTablePrintDialogWidget::loadPersistentData()
{
	qfLogFuncFrame() << xmlConfigPersistentId();
	if(xmlConfigPersistentId().isEmpty()) return;
	QFDialogWidget::loadPersistentData();
	f_allSettings = persistentValue("settings").toMap();
	qfTrash() << "\t all settings:" << QFJson::variantToString(f_allSettings);
	QString sett_name;
	QMapIterator<QString, QVariant> it(f_allSettings);
	while(it.hasNext()) {
		it.next();
		if(sett_name.isEmpty()) sett_name = it.key();
		ui->lstSettings->addItem(it.key());
	}
	if(!sett_name.isEmpty()) on_lstSettings_activated(sett_name);
}

QVariant QFTablePrintDialogWidget::settings() const
{
	QVariantMap ret;
	{
		QVariantMap m1;
		m1["title"] = reportTitle();
		m1["fileName"] = reportFileName();
		ret["report"] = m1;
	}
	{
		QVariantMap m1;
		m1["selectedRowsOnly"] = isSelectedRowsOnly();
		ret["options"] = m1;
	}
	ret["modelColumnNames"] = modelColumnNames();
	ret["tableFieldNames"] = tableFieldNames();
	return ret;
}

void QFTablePrintDialogWidget::setSettings(const QVariant &_settings)
{
	QVariantMap m = _settings.toMap();
	ui->edReportTitle->setText(m.value("report").toMap().value("title").toString());
	ui->lstReportFileName->setEditText(m.value("report").toMap().value("fileName").toString());
	ui->chkSelectedRowsOnly->setChecked(m.value("options").toMap().value("selectedRowsOnly").toBool());
	{
		QSet<QString> model_col_names = QSet<QString>::fromList(m.value("modelColumnNames").toStringList());
		QListWidget *w = ui->lstModelColumns;
		for(int i=0; i<w->count(); i++) {
			QListWidgetItem *it = w->item(i);
			QString col_name = it->data(Qt::UserRole).toString();
			it->setCheckState(model_col_names.contains(col_name)? Qt::Checked: Qt::Unchecked);
		}
	}
	{
		QSet<QString> table_field_names = QSet<QString>::fromList(m.value("tableFieldNames").toStringList());
		QListWidget *w = ui->lstTableColumns;
		for(int i=0; i<w->count(); i++) {
			QListWidgetItem *it = w->item(i);
			QString fld_name = it->text();
			it->setCheckState(table_field_names.contains(fld_name)? Qt::Checked: Qt::Unchecked);
		}
	}
}
