#include <QSqlError>
#include <QColor>
#include <QIcon>

#include <qfapplication.h>
#include <qfstring.h>
#include <qfsqlquery.h>

#include "qfsqlquerymodel.h"

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

//=======================================================
//                   QFSqlQueryModel
//=======================================================

QFSqlQueryModel::QFSqlQueryModel(const QFSqlConnection &_connection)
	: QFTableModel(parent), _d(), d(&_d)
{
	//d = new Data(_connection);
}
			
/*
int QFSqlQueryModel::columnCount(const QModelIndex & parent) const
{
	Q_UNUSED(parent);
	return columnIndex().count();
	//return query().colCount();
}

int QFSqlQueryModel::rowCount(const QModelIndex & parent) const
{
	Q_UNUSED(parent);
	int ret =  query().rowCount();
	//qfTrash() << "QFSqlQueryModel::rowCount():" << ret;
	return ret;
}
*/

/*! \reimp
 */
Qt::ItemFlags QFSqlQueryModel::flags(const QModelIndex &index) const
{
	/*
	Q_D(const QSqlTableModel);
	if (index.internalPointer() || index.column() < 0 || index.column() >= d->rec.count()
		   || index.row() < 0)
		return 0;
	if (d->rec.field(index.column()).isReadOnly())
		return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
	*/
	//Q_UNUSED(index);
	//qfTrash() << "QFSqlQueryModel::flags()";
	return QFTableModel::flags(index);
}
	
/*! \reimp */
QVariant QFSqlQueryModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	static QIcon icoDirty(QPixmap(":/libqf/images/tuzka.png"));

	//qfTrash() << QF_FUNC_NAME << "icon is null:" << icoDirty.serialNumber();
	if(orientation == Qt::Vertical) {
		Row r = row(section, !Qf::ThrowExc);
		if(!r.isNull()) {
			if(r.isDirty()) {
				if(role == Qt::DecorationRole) {
					return qVariantFromValue(icoDirty);
				}
				else if(role == Qt::DisplayRole) {
					return QString();
				}
			}
		}
	}
	/*
	else if(orientation == Qt::Horizontal) {
		//int col = columnIndex()[section];
		//if(col >= q.colCount()) return QVariant();
		if(role == Qt::DisplayRole) {
			QString s = column(section, !Qf::ThrowExc).caption();
			return s;
		}
	}
	*/
	return QFTableModel::headerData(section, orientation, role);
}

/*! \reimp */
QVariant QFSqlQueryModel::data(const QModelIndex & item, int role) const
{
	QVariant ret;
	ret = QFTableModel::data(item, role);
#if 0
	const QFSqlQuery &q = query();
	int col = columnIndex()[item.column()];
	if(col >= q.colCount()) return ret;
	if(role == Qt::BackgroundColorRole) {
		//if(q.row(item.row(), false).isDirty()) ret = QColor(Qt::yellow);
		ret = QColor(Qt::white);
		if(q.mode() == QFSqlQuery::ModeReadWrite) {
			if(item.row() == q.at()) {
				QFSqlField f = q.field(item.column());
				ret = (f.isReadOnly())? QColor(Qt::lightGray): QColor(Qt::yellow);
			}
		}
	}
	else if(role == Qt::TextColorRole) {
		/*
		const QFSqlQuery &q = queryqf();
		if(q.mode() == QFSqlQuery::ModeReadWrite) {
			QFSqlField f = q.field(item.column());
			ret = (f.isReadOnly())? QColor(Qt::blue): QColor(Qt::black);
		}
		else {
			ret = QColor(Qt::black);
		}
		*/
	}
	else if(role == Qt::EditRole || role == Qt::DisplayRole) {
		try {
			ret = q.data(col, item.row(), role);
		}
		catch(QFException &e) {
			ret = "PROBLEM";
		}
	}
	else {
		ret = QSqlQueryModel::data(item, role);
	}
#endif
	return ret;
}

/*! \reimp */
bool QFSqlQueryModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	return QFTableModel::setData(index, value, role);
}

bool QFSqlQueryModel::insertRow(int before_row, const QModelIndex &parent) throw(QFSqlException)
{
	qfTrash() << QF_FUNC_NAME;
	return QFTableModel::insertRow(before_row, parent);
}

bool QFSqlQueryModel::removeRow(int ri, const QModelIndex &parent) throw(QFSqlException)
{
	qfTrash() << QF_FUNC_NAME;
	Q_UNUSED(parent);
	bool ret = false;
	Row r = row(ri);
	ret = deleteRowInDatabase(r);
	if(ret) ret = QFTableModel::removeRow(ri);
	return ret;
}

/*
QFSqlQueryModel::ColumnIndex& QFSqlQueryModel::columnIndex()
{
	if(f_columnIndex.isEmpty()) {
		// fill index with information from query
		QFSqlQuery &q = queryRef();
		int i = 0;
		foreach(QFSqlField f, q.fields()) {
			f_columnIndex.append(i++);
		}
	}
	return f_columnIndex;
}

QFSqlField& QFSqlQueryModel::appendField(const QString &field_name, const QString &field_caption, int width)
{
	QFSqlQuery &q = queryRef();
	QFSqlQuery::FieldList &flds = q.fieldsRef();
	QFSqlField f(field_name);
	f.setCaption(field_caption).setDisplayWidth(width);
	int i = flds.count();
	f_columnIndex.append(i);
	flds.append(f);
	return flds[i];
}
*/
#if 0
bool QFTableModel::reload(bool throw_exc) throw(QFSqlException)
{
	Q_UNUSED(throw_exc);
	return true;
	/*
	//int n = at();
	bool ret = exec(lastSelect(), Qf::ThrowExc, true);
	//setAt(n);
	return ret;
	*/
}

QFSqlField& QFSqlQuery::fieldRef(int col) throw(QFSqlException)
{
	if(!isValidCol(col, false))
		QF_SQL_EXCEPTION(TR("col %1 is not valid column number (count: %2)").arg(col).arg(colCount()));
	return d->fields[col];
}

QFSqlField QFSqlQuery::field(int col, bool throw_exc) const throw(QFSqlException)
{
	//qfTrash() << QF_FUNC_NAME;
	//qfTrash() << "\tcol:" << col << "colCount():" << colCount() << "d:" << d;
	if(isValidCol(col, throw_exc)) return d->fields[col];
	return QFSqlField::sharedNull();
}
/*		
void QFSqlQuery::edit() throw(QFSqlException)
{
	qfTrash() << QF_FUNC_NAME;
	post();
	Row &r = rowRef(at(), Qf::ThrowExc);
	r.setFields(q.fields());
	
}
*/
void QFSqlQuery::insert() throw(QFSqlException)
{
	qfTrash() << QF_FUNC_NAME;
	post();
	if(colCount() <= 0) QF_SQL_EXCEPTION(QObject::tr("Table has no columns, row can not be inserted."));
	
	QFSqlQuery::Row empty_row(fields());
	empty_row.setInsert();
	for(int i=0; i<colCount(); i++) {
		QFSqlField &f = empty_row.fields()[i];
		f.setDirty(false);
		// can have an autogenerated fields
		qfTrash() << "\t" << __LINE__ << f.fullTableName() << f.name();
		qfTrash() << "\t" << f.toString();
		qfTrash() << "\t" << fields()[i].toString();
		QFSqlFieldInfo fi = connection().catalog().table(f.fullTableName()).field(f.name());
		qfTrash() << "\t" << __LINE__;
		QVariant v = fi.seqNextVal();
		if(!v.isValid()) {
			// get default values
			qfTrash() << "default val:" << fi.defaultValue().toString();
			v = fi.defaultValue();
}
		if(v.isValid()) empty_row.setValue(i, v);
}
	
	int row = (eof())? rowCount(): at()+1;
	d->rows.append(empty_row);
	d->index.insert(row, d->rows.size()-1);
	setAt(row);
	
	//setRowMode(Insert);
}
		
void QFSqlQuery::remove() throw(QFSqlException)
{
	qfTrash() << QF_FUNC_NAME;
	int ri = at();
	if(!isValidRow(ri, false)) {
		qfWarning() << QF_FUNC_NAME << "removing invalid row" << ri;
		return;
}
	Row r = row(ri);
	int ix = d->index[ri];
	d->index.remove(ri);
	d->rows.removeAt(ix);
	for(int i=0; i<d->index.size(); i++) {
		int &v = d->index[i];
		if(v > ix) v--;
}
	if(r.isInsert()) return;
	if(r.isDirty()) r.restoreValues();
	foreach(QString tableid, d->tableids) {
		QFSqlTableInfo ti = connection().catalog().table(tableid);
		QString table = ti.fullName().mid(1);
		QString s;
		QSqlRecord rec;
		qfTrash() << "deleting in table" << table;
		if(!resultHasAllPriKeys(tableid)) {
			qfWarning() << QF_FUNC_NAME << TR("The resultset doesn't have all pri keys for delete in table") << table;
}
		else {
			QSqlDriver *drv = connection().driver();
			s += drv->sqlStatement(QSqlDriver::DeleteStatement, table, rec, false);
			s += " ";
			const QSqlIndex &pri_ix = ti.primaryIndex();
			rec.clear();
			for(int i=0; i<pri_ix.count(); i++) {
				QFSqlField f = pri_ix.field(i);
				// WHERE condition should use old values
				f.setValue(r.value(i));
				//qfTrash() << "\tpri index" << f.name() << ":" << f.value().toString();
				rec.append(f);
}
			if(rec.isEmpty()) QF_SQL_EXCEPTION(TR("There is not a primary key to remove the row."));
			s += drv->sqlStatement(QSqlDriver::WhereStatement, table, rec, false);
			qfTrash() << "\t" << s;
			command(s);
			//if(numRowsAffected() != 1) QF_INTERNAL_ERROR_EXCEPTION(QString("numRowsAffected() = %1, sholuld be 1\n%2").arg(numRowsAffected()).arg(s));
}
		break; // only the first table is deleted
}
	if(ri >= rowCount()) {
		//qfTrash() << "\tlast row removed";
		setAt(rowCount() - 1);
}
}

	/*
void QFSqlQuery::edit() throw(QFSqlException)
{
	qfWarning() << QF_FUNC_NAME << "NOT used anymore!";
	Row &r = row(at(), false);
	if(r.isEmpty()) {
		qfWarning() << QF_FUNC_NAME << "Editing invalid row" << at();
		return;
}
	for(int i=0; i<colCount(); i++) {
		QFSqlField &f = r.fields()[i];
		f.setDirty(false);
		// cash ONLY old pri keys values
		//const QFSqlCatalogField &cf = connection().field(f.tableName(), f.name());
		//if(cf.isPriKey()) f.setValue(row(at()).value(i));
		// save old values
		f.setValue(r.value(i));
}
	r.setEdit();
	//setRowMode(Edit);
	//qfTrash() << "\tEXIT";
}
	*/

void QFSqlQuery::post() throw(QFSqlException)
{
	if(mode() != QFSqlQuery::ModeReadWrite) return;
	Row &r = rowRef(at(), false);
	if(!r.isDirty()) return;
	qfTrash() << QF_FUNC_NAME;
	if(r.isInsert()) {
		foreach(QString tableid, d->tableids) {
			QFSqlTableInfo ti = connection().catalog().table(tableid);
			QString table = ti.fullName().mid(1);
			QString s;
			QSqlDriver *drv = connection().driver();
{
				QSqlRecord rec;
				int i = -1;
				//int pri_key_ix = -1;
				foreach(QFSqlField f, r.fields()) {
					i++;
					if(f.fullTableName() != tableid) continue;
					//if(f.isPriKey()) pri_key_ix = i;
					if(!f.isDirty()) continue;
					//qfTrash() << "\tdirty field:" << f.name();
					f.setValue(value(i));
					f.setName(f.name()); // tohle neni omyl, f.name() jinak vraci, to, co tam zapise driver a to je jmeno vcetne tabulky
					rec.append(f);
}
				if(!rec.isEmpty()) {
					qfTrash() << "updating table inserts" << table;
					s += drv->sqlStatement(QSqlDriver::InsertStatement, table, rec, false);
					qfTrash() << "\texecuting:" << s;
					command(s);
					if(numRowsAffected() != 1) QF_INTERNAL_ERROR_EXCEPTION(QString("numRowsAffected() = %1, sholuld be 1\n%2").arg(numRowsAffected()).arg(s));
#if 0
					if(pri_key_ix >= 0) {
						// set last inserted ID
						if(connection().driverType() == QFSqlConnection::SQLITE) {
							r.setValue(pri_key_ix, lastInsertId());
}
}
#endif
}
}
}
}
	else if(r.isDirty()) {
		foreach(QString tableid, d->tableids) {
			QFSqlTableInfo ti = connection().catalog().table(tableid);
			QString table = ti.fullName().mid(1);
			QString s;
			QSqlDriver *drv = connection().driver();
{
				QSqlRecord rec;
				int i = -1;
				foreach(QFSqlField f, r.fields()) {
					i++;
					//qfTrash() << "\ttableid:" << tableid << "fullTableName:" << f.fullTableName();
					if(f.fullTableName() != tableid) continue;
					if(!f.isDirty()) continue;
					qfTrash() << "\tdirty field" << f.name() << ":" << value(i).toString();
					f.setValue(value(i));
					f.setName(f.name()); // tohle neni omyl, f.name() jinak vraci, to, co tam zapise driver a to je jmeno vcetne tabulky
					rec.append(f);
}
				if(!rec.isEmpty()) {
					qfTrash() << "updating table edits" << table << "(" << tableid << ")";
					if(!resultHasAllPriKeys(tableid)) {
						qfWarning() << QF_FUNC_NAME << TR("The result doesn't have all pri keys for updates in table") << table;
						continue;
}
					s += drv->sqlStatement(QSqlDriver::UpdateStatement, table, rec, false);
					s += " ";
					const QSqlIndex &pri_ix = ti.primaryIndex();
					rec.clear();
					for(int i=0; i<pri_ix.count(); i++) {
						QFSqlField f = pri_ix.field(i);
						// WHERE condition should use old values
						f.setValue(r.fields()[i].value());
						qfTrash() << "\tpri index" << f.name() << ":" << f.value().toString();
						rec.append(f);
}
					if(rec.isEmpty()) QF_INTERNAL_ERROR_EXCEPTION("pri keys values not generated");
					s += drv->sqlStatement(QSqlDriver::WhereStatement, table, rec, false);
					qfTrash() << "\t" << s;
					command(s);
					if(numRowsAffected() != 1) QF_INTERNAL_ERROR_EXCEPTION(QString("numRowsAffected() = %1, sholuld be 1\n%2").arg(numRowsAffected()).arg(s));
}
}
}
}
	r.clearDirty();
	r.fields() = fields();
}

void QFSqlQuery::revert() throw(QFSqlException)
{
	qfTrash() << QF_FUNC_NAME;
	Row &r = rowRef(at(), false);
	if(!r.isDirty()) return;
	if(r.isInsert()) {
		qfError() << QF_FUNC_NAME << "Revert of Insert NIY";
		return;
}
	r.restoreValues();
}
/*
void QFSqlQuery::setValue(int index, const QVariant& val) throw(QFSqlException)
{
	if(rowMode() != Edit && rowMode() != Insert) QF_SQL_EXCEPTION("row mode is not edit or insert");
	qfTrash() << QF_FUNC_NAME << "\n\tcolumn:" << index << "value:" << val.toString();
	field(index).setDirty();
	data(at(), index, Qt::EditRole) = val;
}

void QFSqlQuery::setValue(const QString& col_name, const QVariant& val) throw(QFSqlException)
{
	qfTrash() << QF_FUNC_NAME << "\n\tcolname:" << col_name << "value:" << val.toString();
	setValue(fieldNameToCol(col_name), val);
}
*/
QFSqlQuery::Row& QFSqlQuery::rowRef(int i, bool throw_exc) throw(QFSqlException)
{
	//qfTrash() << QF_FUNC_NAME;
	//qfTrash() << "\trow:" << i << "at():" << at();
	if(i < 0) i = at();
	if(!isValidRow(i, throw_exc)) return Row::nullRow();
	int ix = d->index[i];
	if(ix < 0 || ix >= d->rows.size()) {
		if(throw_exc) QF_INTERNAL_ERROR_EXCEPTION(QString("row: %1 -> index: %2 is out of range of rows (%3)").arg(i).arg(ix).arg(d->rows.size()));
		return Row::nullRow();
}
	//qfTrash() << "\tOK";
	return d->rows[ix];
}
	
QFSqlQuery::Row QFSqlQuery::row(int i, bool throw_exc) const throw(QFSqlException)
{
	return ((QFSqlQuery*)this)->rowRef(i, throw_exc);
}
	
QVariant QFSqlQuery::value(int index) const throw(QFSqlException)
{
	return row(at()).value(index);
}
		
QFSqlQuery::Modes QFSqlQuery::setMode(Modes m)
{
	Modes om = mode();
	if(m == ModeReadWrite && !d->fieldsPreparedForUpdates) {
		d->fieldsPreparedForUpdates = true;
		qfTrash() << QF_FUNC_NAME;
		//qfTrash() << "\tsetting mode RW first time";
		// tak tady se stane hodne veci,
		// vezme se select, vyparsuji se z neho tabulky a pokusi se sparovat s fieldy a tabulky
		// select * from t1 left join t4 on t1.id=t4.id left join t2 on t4.id=t2.id;

		// vyparsuj tabulky ze selectu
		QStringList sl, sl1;
		QStringList &tables = d->tableids;
		QString s = lastSelect().simplified(), s1;
		//qfTrash() << "\tparsing:" << s;
		//qfTrash() << "\td:" << d.constData();
		int ix = s.indexOf("FROM ", 0, Qt::CaseInsensitive);
		if(ix < 0) return om;
		s = s.mid(ix + 5).trimmed();
		// hledej carky (select * from t1,t2,t4)
		while(true) {
			ix = s.indexOf(',');
			if(ix > 0) {
				s1 = s.mid(0, ix);
				s = s.mid(ix + 1).trimmed();
				tables.append(connection().normalizeTableName(s1));
				//qfTrash() << "FROM:" << s1;
}
			else break;
}
		ix = s.indexOf(' ');
		if(ix > 0) {
			s1 = s.mid(0, ix);
			s = s.mid(ix + 1).trimmed();
}
		else 	s1 = s;
		tables.append(connection().normalizeTableName(s1));
		//qfTrash() << "FROM:" << s1;
		sl = s.split("JOIN ", QString::SkipEmptyParts, Qt::CaseInsensitive);
		int i = 0;
		foreach(s, sl) {
			//qfTrash() << "\t" << s;
			if(i++ == 0) continue; // skip first
			ix = s.indexOf(' ');
			//qfTrash() << "\t" << ix;
			if(ix < 0) return om;
			s = s.mid(0, ix);
			tables.append(connection().normalizeTableName(s));
			//qfTrash() << "JOIN:" << s;
}
		qfTrash() << "\ttables:" << tables.join(",");
		
		// projdi jmena fieldu a zkus najit v cachi odpovidajici zaznam
		for(int i=0; i<fields().size(); i++) {
			QFSqlField &f = fieldRef(i);
			QString reported_name = f.driverReportedName();
			QString db, tbl, nm;
			QFSql::parseFullName(reported_name, &nm, &tbl, &db);
			// najdi prvni tabulku, ktera konci na ftn a obsahuje tento field
			QString ftn = QFSql::composeFullName(tbl, db);
			qfTrash().noSpace() << "\tlooking in table '" << ftn << "' for field:" << nm;
			foreach(QString t, tables) {
				if(!t.endsWith(ftn)) continue;
				qfTrash() << "\t\ttrying:" << t;
				QFSqlFieldInfo fi = connection().catalog().table(t).field(nm);
				if(fi.isValid()) {
					qfTrash() << "\t\tfound:" << fi.toString();
					f = fi;
					f.setCanUpdate();
					break;
}
				qfTrash() << "\t\t\tNEGATIVE";
}
}
		
		// zkontroluj prikeys, jestli pujde pole updatovat
		foreach(QString t, tables) {
			if(!resultHasAllPriKeys(t)) {
				// pole, ktera je nemaji jsou RO
				for(int i=0; i<fields().size(); i++) {
					QFSqlField &f = fieldRef(i);
					if(f.fullTableName() == t) f.setCanUpdate(false);
					//qfTrash() << "\t\t\tt:" << t << "f:" << f.fullTableName();
}
}
}

		// change fields in all rows to precized values
		for(int i=0; i<rowCount(); i++) {
			Row &r = rowRef(i);
			r.setFields(fields());
}
}
	d->mode = m;
	return om;
}
		
#endif
