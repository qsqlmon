#ifndef QF_SQL_QUERY_MODEL_H
#define QF_SQL_QUERY_MODEL_H

#include <qfguiglobal.h>

#include <qfexception.h>
#include <qfsqlconnection.h>
#include <qfsqlquery.h>
#include <qftablemodel.h>

//=======================================================
//                   QFSqlQueryModel
//=======================================================
/**
 model for QFSqlQuery.
 @deprecated
*/
class QFGUI_DECL_EXPORT QFSqlQueryModel : public QFTableModel
{
	ERROR
	Q_OBJECT
		//enum Modes {ModeReadOnly, ModeReadWrite};
		//typedef QList<int> ColumnIndex;
	protected:
	public:
		QFSqlQueryModel(QObject *parent = NULL, const QFSqlConnection &_connection = QFSqlConnection());
		virtual ~QFSqlQueryModel() {}
		
		virtual QVariant data(const QModelIndex & item, int role = Qt::DisplayRole) const;
		virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
		virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
		
		void reset() {QFTableModel::reset();}
		
	public:
		// reimplemented from QFTableModel
		/**
	      inserts row after current row. If current row is invalid, prepends row.
	      @return true if row is successfully inserted
		 */
		virtual bool insertRow(int before_row, const QModelIndex & parent = QModelIndex()) throw(QFSqlException);
		//! If select is JOIN one, only row from first table is removed on the server.
		virtual bool removeRow(int ri, const QModelIndex & parent = QModelIndex()) throw(QFSqlException);
		virtual Qt::ItemFlags flags(const QModelIndex &index) const;
		
};

#endif // QF_SQL_QUERY_MODEL_H
