
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqlquerytablemodel.h"

#include <qflogcust.h>

QFSqlQueryTableModel::QFSqlQueryTableModel(QObject *parent)
	: QFTableModel(parent)
{
}

QFSqlQueryTableModel::QFSqlQueryTableModel(const QFSqlConnection & conn, QObject * parent)
	: QFTableModel(parent)
{
	QFSqlQueryTable *t = new QFSqlQueryTable(conn);
	qfTrash() << QF_FUNC_NAME << "created new sql table:" << t;
	setTable(t);
}

QFSqlQueryTableModel::~QFSqlQueryTableModel()
{
}

QFSqlQueryTable* QFSqlQueryTableModel::table() const
{
	QFSqlQueryTable *t = NULL;
	if(!tableIsNull()) {
		t = dynamic_cast<QFSqlQueryTable*>(QFTableModel::table());
	}
	if(!t) {
		t = new QFSqlQueryTable();
		qfTrash() << QF_FUNC_NAME << "created new sql table:" << t;
		/// setTable() deletes any existing old table whatever it is
		const_cast<QFSqlQueryTableModel*>(this)->setTable(t);
	}
	return t;
}

bool QFSqlQueryTableModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant & val, int role)
{
	bool ret = true;
	if(orientation == Qt::Horizontal) {
		if(role == Qf::AddColumnToBenListRole) {
			QFSqlQueryBuilder qb = table()->queryBuilder();
			qb.setColumnBenned(column(section, !Qf::ThrowExc).fieldName(), val.toBool());
			table()->setQueryBuilder(qb);
		}
	}
	return ret;
}

int QFSqlQueryTableModel::reload(const QString &query_str) throw(QFException)
{
	int ret = table()->reload(query_str);
	//QFTableModel::reload();
	//qfInfo() << "reset";
	reset();
	return ret;
}


