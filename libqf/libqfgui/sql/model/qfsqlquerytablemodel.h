
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLQUERYTABLEMODEL_H
#define QFSQLQUERYTABLEMODEL_H

#include <qfguiglobal.h>


#include <qftablemodel.h>
#include <qfsqlquerytable.h>

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlQueryTableModel : public QFTableModel
{
	Q_OBJECT;
	public:
		virtual bool setHeaderData(int section, Qt::Orientation orientation, const QVariant & value, int role = Qt::EditRole);
		/*
		QFSqlQueryTable& tableRef() throw(QFException) {
			QFSqlQueryTable *t = qobject_cast<QFSqlQueryTable *>(QFTableModel::table());
			if(!t) QF_EXCEPTION("Table is NULL or not a kind of QFSqlQueryTable.");
			return *t;
		}
		*/
		//! Returns the model's table.
		/*!
		If no table was previously set calling \a QFTableModel::setTable() a new one is created.
		\sa QFTableModel::table().
		 */
		virtual QFSqlQueryTable* table() const;

		/// QFSqlQueryTableView si potrebuje query schovavat do modelu, jinak se narusi model->view filosofie,
		/// takze QFSqlQueryTableView::setQuery() se nakonec propadne sem, coz je vyhodne, protoze dokument muze reloadnout model, aniz by query tahal z view
		QString sqlTableViewQuery() const {return property("qf_sqlTableViewQuery").toString();}
		void setSqlTableViewQuery(const QString &qs) {setProperty("qf_sqlTableViewQuery", qs);}
		
		virtual void reload() throw(QFException) {QFTableModel::reload();}
		virtual int reload(const QString &query_str) throw(QFException);
	public:
		QFSqlQueryTableModel(QObject *parent = NULL);
		QFSqlQueryTableModel(const QFSqlConnection &conn, QObject *parent = NULL);
		virtual ~QFSqlQueryTableModel();
};
   
#endif // QFSQLQUERYTABLEMODEL_H

