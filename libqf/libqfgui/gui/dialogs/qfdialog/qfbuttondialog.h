
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFBUTTONDIALOG_H
#define QFBUTTONDIALOG_H

#include <qfguiglobal.h>
#include <qfdialog.h>

#include <QDialogButtonBox>

namespace Ui {class QFButtonDialog;};

//! TODO: write class documentation. 
class QFGUI_DECL_EXPORT QFButtonDialog : public QFDialog
{
	Q_OBJECT
	public:
		enum Button {ButtonOk=QDialogButtonBox::Ok, ButtonCancel=QDialogButtonBox::Cancel,
			ButtonNone=QDialogButtonBox::RestoreDefaults << 1,
			ButtonUnused=ButtonNone << 1};
	private:
		Ui::QFButtonDialog *ui;
		QPushButton *btNone;
	private slots:
		void btNone_clicked();
	public:
		void setButtonsVisible(bool b);
		bool isButtonsVisible() const;
		void setButtonsVisible(int bts, bool b);
		/// for convinience
		void setButtonNoneVisible(bool b = true);
		//void addWidgetToButtonBar(QWidget *w);
		QDialogButtonBox* buttonBox() const;

		QPushButton* button(Button bt);
	public:
		QFButtonDialog(QWidget *parent = NULL, Qt::WFlags f = 0);
		virtual ~QFButtonDialog();
};
   
#endif // QFBUTTONDIALOG_H

