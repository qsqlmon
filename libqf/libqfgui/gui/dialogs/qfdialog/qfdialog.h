
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDIALOG_H
#define QFDIALOG_H

#include <qfguiglobal.h>
#include <qfxmlconfigpersistenter.h>

#include <QDialog>

class QFDialogWidget;

//! @todo write class documentation.
class QFGUI_DECL_EXPORT QFDialog : public QDialog, public QFXmlConfigPersistenter
{
	Q_OBJECT
	protected:
		QWidget *f_centralWidget;
		//QObject *f_doneSender;
	public:
		/// tyto hodnoty vraci funkce exec()
		enum ModalResult {Rejected = QDialog::Rejected, Accepted = QDialog::Accepted, NoneOfThem, Dropped};
	protected slots:
		virtual void closeDialogWidget(QFDialogWidget *w, int result);
	protected:
		virtual void keyPressEvent(QKeyEvent *e);
	public:
		virtual void done(int result);
		//virtual void accept();
		//virtual void reject();
		
		virtual QWidget* centralWidget() {return f_centralWidget;}
		/*!
		Z widgetu si vytahne informace o akcich, toolbarech a menu a ty zobrazi.
		Function calls setWidget(), so it takes ownership of \a w .
		\code
			QFButtonDialog dlg;
			InsertPartsWidget *w = new InsertPartsWidget();
			dlg.setDialogWidget(w);
			int res = dlg.exec();
			if(res == QFDialog::Accepted) {
				QFMessage::information(w->partType());
			}
		\endcode
		*/
		virtual void setDialogWidget(QFDialogWidget *w) throw(QFException);
		/// zobrazi widget, jako svuj centralWidget, takes ownership of \a w .
		//! vraci false, kdyz se to nepovede
		virtual bool setWidget(QWidget *w);

		//QObject *doneSender() const {return f_doneSender;}

		virtual void loadPersistentData();
		virtual void savePersistentData();
	public:
		QFDialog(QWidget * parent = 0, Qt::WFlags f = 0);
		virtual ~QFDialog();
};

#endif // QFDIALOG_H

