
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "ui_qfbuttondialog.h"
#include "qfbuttondialog.h"

#include <QPushButton>

#include <qflogcust.h>

QFButtonDialog::QFButtonDialog(QWidget *parent, Qt::WFlags f)
	: QFDialog(parent, f)
{
	QBoxLayout *ly = qobject_cast<QBoxLayout*>(layout());
	if(ly) {
		QWidget *w = new QWidget(this);
		ly->addWidget(w);
		ui = new Ui::QFButtonDialog;
		ui->setupUi(w);
		btNone = buttonBox()->addButton(tr("None"), QDialogButtonBox::ActionRole);
		buttonBox()->button(QDialogButtonBox::Ok)->setDefault(true);
		//btNone->setEnabled(false);
		/// connectSlotsByName v tomto pripade nefunguje, protoze w tyto sloty nema
		connect(buttonBox(), SIGNAL(accepted()), this, SLOT(accept()));
		connect(buttonBox(), SIGNAL(rejected()), this, SLOT(reject()));
		connect(btNone, SIGNAL(clicked()), this, SLOT(btNone_clicked()));
	}
	/// tady nema cenu ho skryvat, protoze pridanim dalsiho buttonu do buttonBox() zpusobi zviditelneni vsech buttonu (alespon ve 4.2.0)
	/// schovabat buttony ma cenu az pote, co byl pridan posledni
	setButtonsVisible(ButtonNone, false);
	//qfInfo() << "btNone visible:" << btNone->isVisible();
}

QFButtonDialog::~QFButtonDialog()
{
	delete ui;
}
		
void QFButtonDialog::btNone_clicked()
{
	qfInfo() << "btNone visible:" << btNone->isVisible();
	done(NoneOfThem);
}
		
QDialogButtonBox* QFButtonDialog::buttonBox() const
{
	return ui->buttonBox;
}
		
void QFButtonDialog::setButtonsVisible(bool b)
{
	qfTrash() << QF_FUNC_NAME << b;
	ui->buttonBox->setVisible(b);
}
		
bool QFButtonDialog::isButtonsVisible() const
{
	return ui->buttonBox->isVisible();
}

void QFButtonDialog::setButtonsVisible(int bts, bool b)
{
	qfTrash() << QF_FUNC_NAME << "buttons:" << QString::number(bts, 16) << b << QString::number(bts & ButtonNone, 16);
	if(bts & ButtonOk) ui->buttonBox->button((QDialogButtonBox::StandardButton)ButtonOk)->setVisible(b);
	if(bts & ButtonCancel) ui->buttonBox->button((QDialogButtonBox::StandardButton)ButtonCancel)->setVisible(b);
	if(bts & ButtonNone) {
		qfTrash() << "\tset btNone visible:" << b;
		btNone->setVisible(b);
	}
}

void QFButtonDialog::setButtonNoneVisible(bool b)
{
	btNone->setVisible(b);
}
		
QPushButton* QFButtonDialog::button(Button bt)
{
	if(bt == ButtonOk) return ui->buttonBox->button((QDialogButtonBox::StandardButton)ButtonOk);
	if(bt == ButtonCancel) return ui->buttonBox->button((QDialogButtonBox::StandardButton)ButtonCancel);
	if(bt == ButtonNone) return btNone;
	QF_EXCEPTION(tr("Button %1 does not exist.").arg(bt));
	return NULL;
}
/*		
void QFButtonDialog::addWidgetToButtonBar(QWidget *w)
{
	QBoxLayout *ly = qobject_cast<QBoxLayout*>(ui->frmButtons->layout());
	QF_ASSERT(ly, "bad layout");
	ly->insertWidget(1, w); 
}
*/


