
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfinputdialog.h"

#include <qfdateedit.h>

#include <QVBoxLayout>
#include <QLabel>
#include <QDialogButtonBox>
#include <QPushButton>

#include <qflogcust.h>

QFInputDialog::QFInputDialog(const QString & title, const QString & label, QWidget * parent, QWidget * input, Qt::WindowFlags f)
	: QDialog(parent, f)
{
	setWindowTitle(title);
	QVBoxLayout *vbox = new QVBoxLayout(this);

	QLabel *lbl = new QLabel(label, this);
	vbox->addWidget(lbl);
	vbox->addStretch(1);

	input->setParent(this);
	vbox->addWidget(input);
	vbox->addStretch(1);

	lbl->setBuddy(input);

	QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Cancel, Qt::Horizontal, this);
	buttonBox->setObjectName(QLatin1String("qf_inputdlg_buttonbox"));
	QPushButton *okButton = static_cast<QPushButton *>(buttonBox->addButton(QDialogButtonBox::Ok));
	okButton->setDefault(true);
	vbox->addWidget(buttonBox);

	QObject::connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
	QObject::connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

	setSizeGripEnabled(true);
}

QFInputDialog::~QFInputDialog()
{
}

QDate QFInputDialog::getDate(QWidget * parent, const QString & title, const QString & label, const QDate & _d, bool none_visible, bool * ok, Qt::WindowFlags f)
{
	qfLogFuncFrame() << "date:" << _d.toString(Qt::ISODate);
	QFDateEdit *ed = new QFDateEdit();
	ed->setButtonNoneVisible(none_visible);
	QDate d = _d;
	if(!d.isValid()) d = QDate::currentDate();
	ed->setDate(d);
	QFInputDialog dlg(title, label, parent, ed, f);
	bool accepted = (dlg.exec() == QDialog::Accepted);
	if(ok) *ok = accepted;
	if(!ok) return QDate();
	return ed->date();
}
