
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdialog.h"

#include <qfdialogwidget.h>
#include <qfuibuilder.h>
#include <qftoolbar.h>

#include <QVBoxLayout>
#include <QMenuBar>

#include <qflogcust.h>

QFDialog::QFDialog(QWidget *parent, Qt::WFlags f)
	: QDialog(parent, f)//, f_doneSender(NULL)
{
	if(f == 0) {
		/// defaultne se mi ve Win32 u dialogu nezograzuje Maximize button, takze ho tam dodam takhle.
		setWindowFlags(windowFlags() | Qt::WindowMaximizeButtonHint);
	}
	QBoxLayout *ly = new QVBoxLayout(this);
	ly->setMargin(0);
	QFrame *frm = new QFrame(this);
	//frm->setFrameShape(QFrame::Box);
	f_centralWidget = frm;
	ly->addWidget(f_centralWidget);
}

QFDialog::~QFDialog()
{
	savePersistentData();
}

void  QFDialog::savePersistentData()
{
	if(!xmlConfigPersistentId().isEmpty()) {
		qfTrash() << QF_FUNC_NAME << xmlConfigPersistentId();
		//QFXmlConfigElement el = createPersistentPath();
		/// save window size
		setPersistentValue("window/pos", pos());
		setPersistentValue("window/size", size());
	}
	//qfWarning() << __LINE__;
}

void  QFDialog::loadPersistentData()
{
	if(!xmlConfigPersistentId().isEmpty()) {
		qfTrash() << QF_FUNC_NAME << xmlConfigPersistentId();
		//QFXmlConfigElement el = persistentPath();
		/// load window size
		QVariant v = persistentValue("window/pos");
		if(v.type() == QVariant::Point) {
			move(v.toPoint());
		}
		v = persistentValue("window/size");
		if(v.type() == QVariant::Size) {
			resize(v.toSize());
		}
	}
}

bool QFDialog::setWidget(QWidget *w)
{
	qfLogFuncFrame() << w;
	if(!w) return false;
	QBoxLayout *ly = qobject_cast<QBoxLayout*>(layout());
	if(!ly) return false;
	QWidget *wc = centralWidget();
	qfTrash() << "\t centralWidget:" << wc;
	qDeleteAll(wc->findChildren<QWidget*>());
	qDeleteAll(wc->findChildren<QLayout*>());
	qfTrash() << "\t create new layout for centralWidget";
	QHBoxLayout *ly1 = new QHBoxLayout(wc);
	ly1->setMargin(0);
	qfTrash() << "\t adding widget to centralWidget layout";
	ly1->addWidget(w);
	w->setFocus();
	return true;
}

void QFDialog::setDialogWidget(QFDialogWidget *wd) throw(QFException)
{
	qfLogFuncFrame() << wd;
	if(!setWidget(wd)) return;

	if(xmlConfigPersistentId().isEmpty() && !wd->xmlConfigPersistentId().isEmpty()) {
		setXmlConfigPersistentId(wd->xmlConfigPersistentId() + "/qfdialog");
	}

	QBoxLayout *ly = qobject_cast<QBoxLayout*>(layout());

	QFPart::ToolBarList tool_bars = wd->createToolBars();
	if(tool_bars.count() == 1) {
		QFToolBar *tb = tool_bars[0];
		tb->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
		ly->insertWidget(0, tb);
	}
	else if(!tool_bars.isEmpty()) {
		/// toolbary dej vedle sebe
		QHBoxLayout *ly1 = new QHBoxLayout(NULL);
		foreach(QFToolBar *tb, tool_bars) {
			tb->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
			ly1->addWidget(tb);
		}
		ly1->addStretch();
		ly->insertLayout(0, ly1);
	}

	int menu_widget_index = 0;
	/*
	if(wd->hasCaption()) {
		///caption
		QWidget *w = wd->createCaption();
		if(w) {
			ly->insertWidget(0, w);
			menu_widget_index = 1;
		}
	}
	*/
	{
		QMenuBar *mb = new QMenuBar(this);
		//mb->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
		wd->updateMenuOrBar(mb);
		if(mb->actions().isEmpty()) { delete mb; }
		else { ly->insertWidget(menu_widget_index, mb); }
	}
	{
		QFDialogWidgetCaptionFrame *cf = wd->captionFrame();
		if(cf) {
			ly->insertWidget(0, cf);
			//ly->addWidget(cf);
		}
	}

	connect(wd, SIGNAL(wantClose(QFDialogWidget*, int)), this, SLOT(closeDialogWidget(QFDialogWidget*, int)));
}

void QFDialog::closeDialogWidget(QFDialogWidget *w, int result)
{
	qfTrash() << QF_FUNC_NAME << "result:" << result;
	Q_UNUSED(w);
	done(result);
	/*
	if(result == QDialog::Accepted) {
		//qfInfo() << "ACCEPT";
		accept();
	}
	else {
		reject();
	}
	*/
}
/*
void QFDialog::accept()
{
	qfInfo() << QF_FUNC_NAME;
	QDialog::accept();
}

void QFDialog::reject()
{
	qfInfo() << QF_FUNC_NAME;
	QDialog::reject();
}
*/
void QFDialog::done(int result)
{
	qfLogFuncFrame() << "result:" << result;
	//qfInfo() << QFLog::stackTrace();
	bool call_base = true;
	QList<QFDialogWidget*> lst = findChildren<QFDialogWidget*>();
	foreach(QFDialogWidget *w, lst) {
		if(!w->canBeClosed(result)) {call_base = false; break;}
	}
	if(call_base) QDialog::done(result);
}

void QFDialog::keyPressEvent(QKeyEvent * e)
{
	//qfInfo() << e << "keypress:" << e->key() << "accepted:" << e->isAccepted();
	/// i kdyz je accepted == true, volaji mi QT defaultni button, nevi, kde je chyba, takhle to odstranuju
	//if((e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter) && e->isAccepted()) return;
	QDialog::keyPressEvent(e);
}
