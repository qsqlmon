
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFINPUTDIALOG_H
#define QFINPUTDIALOG_H

#include <qfguiglobal.h>

#include <QDialog>
#include <QDate>

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFInputDialog : public QDialog
{
	public:
		static const bool ButtonNoneVisible = true;
	public:
		static QDate getDate(QWidget * parent, const QString & title, const QString & label, const QDate &d = QDate(), bool none_visible = !ButtonNoneVisible, bool * ok = 0, Qt::WindowFlags f = 0 );
	public:
		QFInputDialog( const QString &title, const QString &label, QWidget *parent, QWidget *input, Qt::WindowFlags f = 0);
		virtual ~QFInputDialog();
};

#endif // QFINPUTDIALOG_H

