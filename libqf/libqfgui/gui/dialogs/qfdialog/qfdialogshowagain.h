
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDIALOGSHOWAGAIN_H
#define QFDIALOGSHOWAGAIN_H

#include <qfguiglobal.h>

#include <qfdialog.h>


namespace Ui {class QFDialogShowAgain;};
class QDialogButtonBox;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFDialogShowAgain : public QFDialog
{
	Q_OBJECT
	private:
		Ui::QFDialogShowAgain *ui;
	protected:
		QString messageId;
	protected:
		QDialogButtonBox* buttonBox() const;
	public:
		int exec(bool show_anyway = false);
	public:
		QFDialogShowAgain(const QString &msg_id, QWidget *parent = NULL, Qt::WFlags f = 0);
		virtual ~QFDialogShowAgain();
};

#endif // QFDIALOGSHOWAGAIN_H

