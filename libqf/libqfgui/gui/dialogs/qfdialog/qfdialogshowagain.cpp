
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "ui_qfdialogshowagain.h"
#include "qfdialogshowagain.h"

#include <qfapplication.h>

#include <qflogcust.h>

QFDialogShowAgain::QFDialogShowAgain(const QString &msg_id, QWidget *parent, Qt::WFlags f)
	: QFDialog(parent, f), messageId(msg_id)
{
	QBoxLayout *ly = qobject_cast<QBoxLayout*>(layout());
	if(ly) {
		QWidget *w = new QWidget(this);
		ly->addWidget(w);
		ui = new Ui::QFDialogShowAgain;
		ui->setupUi(w);
		connect(buttonBox(), SIGNAL(accepted()), this, SLOT(accept()));
		connect(buttonBox(), SIGNAL(rejected()), this, SLOT(reject()));
	}
	ui->chkAgain->setChecked(!qfApp()->messagesIdNotToShow().contains(messageId));
}

QFDialogShowAgain::~QFDialogShowAgain()
{
	delete ui;
}

QDialogButtonBox* QFDialogShowAgain::buttonBox() const
{
	return ui->buttonBox;
}

int QFDialogShowAgain::exec(bool show_anyway)
{
	qfLogFuncFrame();
	int ret = QFDialog::Rejected;
	bool was_checked = ui->chkAgain->isChecked();
	qfTrash() << "was_checked:" << was_checked << "show_anyway:" << show_anyway;
	if(was_checked || show_anyway) {
		ret = QFDialog::exec();
		qfTrash() << "is checked:" << ui->chkAgain->isChecked(); 
		if(!ui->chkAgain->isChecked() && was_checked) {
			qfApp()->addMessageIdNotToShow(messageId);
			qfApp()->saveMessagesIdNotToShow();
		}
	}
	return ret;
}
