
FORMS +=       \
	$$PWD/qfbuttondialog.ui     \
	$$PWD/qfdialogshowagain.ui   \

HEADERS +=        \
	$$PWD/qfdialog.h      \
	$$PWD/qfbuttondialog.h     \
	$$PWD/qfdialogshowagain.h   \
	$$PWD/qfinputdialog.h  \

SOURCES +=        \
	$$PWD/qfdialog.cpp      \
	$$PWD/qfbuttondialog.cpp     \
	$$PWD/qfdialogshowagain.cpp   \
	$$PWD/qfinputdialog.cpp  \


