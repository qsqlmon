
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDATAFORMDIALOG_H
#define QFDATAFORMDIALOG_H

#include <qfclassfield.h>
#include <qfguiglobal.h>
#include <qfbasictable.h>
#include <qfdataformdocument.h>
#include <qfdataformwidget.h>
#include <qfdialog.h>

class QFStatusBar;
class QFDataFormDialogWidget;

//! Vlastne ho mam jen kvuli tomu, aby na ENTER zavolal save() a kvuli funkcim *Exec().
class QFGUI_DECL_EXPORT QFDataFormDialog : public QFDialog
{
	Q_OBJECT;
	//		QF_FIELD_RW(bool, s, setS, ignalsSlotsConnected);
	protected:
		virtual void keyPressEvent(QKeyEvent *e);
		//virtual void closeEvent(QCloseEvent *e);

		void save();

		QFDataFormDialogWidget *fDataFormDialogWidget;
	protected slots:
		//virtual void accept();
		//virtual void reject();
	public:
		QFDataFormDialogWidget* dataFormDialogWidget() const throw(QFException);
		void setDataFormDialogWidget(QFDataFormDialogWidget *w);
	signals:
		void execParams(const QVariant &, QFDataFormDocument::Mode);
	public slots:
		int exec() {return QFDialog::exec();}
		int exec(const QVariant &row_id, QFDataFormDocument::Mode mode);
		int viewExec(const QVariant &row_id) {
			return exec(row_id, QFDataFormDocument::ModeView);
		}
		int editExec(const QVariant &row_id) {
			return exec(row_id, QFDataFormDocument::ModeEdit);
		}
		int insertExec() {
			return exec(0, QFDataFormDocument::ModeInsert);
		}
		int copyExec(const QVariant &row_id) {
			return exec(row_id, QFDataFormDocument::ModeCopy);
		}
	public:
		QFDataFormDialog(QWidget *parent, Qt::WFlags f = 0);
		virtual ~QFDataFormDialog();
};

#endif // QFDATAFORMDIALOG_H

