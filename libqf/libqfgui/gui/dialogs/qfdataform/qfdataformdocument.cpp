
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdataformdocument.h"

#include <qfdbapplication.h>
#include <qfsql.h>
#include <qfsqlquery.h>
#include <qfxmltable.h>

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

//==========================================
//                                   QFDataFormDocument
//==========================================
QFDataFormDocument::QFDataFormDocument(QObject *parent)
	: QObject(parent)
{
	qfLogFuncFrame();
	setMode(ModeView);
	setDataId(0);
	connect(this, SIGNAL(valuesReloaded(QFDataFormDocument*)), this, SIGNAL(valueSet()));
	connect(this, SIGNAL(valueEdited(QString,QVariant,QVariant)), this, SIGNAL(valueSet(QString)));
}

QFDataFormDocument::~QFDataFormDocument()
{
	//qfInfo() << "~QFDataFormDocument()";
	releaseRowLock();
}

bool QFDataFormDocument::isEmpty() const
{
	return false;
}

QVariant QFDataFormDocument::dataId() const
{
	//if(mode() == ModeInsert) return 0;
	//return value(idColumnName());
	return fDataId;
}

void QFDataFormDocument::setDataDirty_helper(bool dirty, bool *refresh_widget_actions_emited)
{
	//qfInfo() << "setDataDirty" << dirty;
	qfLogFuncFrame() << "dirty:" << dirty;
	QVariant old_dirty = f_dataDirty;
	f_dataDirty = dirty;
	if(!old_dirty.isValid() || f_dataDirty.toBool() != old_dirty.toBool()) {
		emit dataDirtyChanged(dirty);
		qfTrash() << "\t emitting refreshWidgetActions()";
		emit refreshWidgetActions();
		if(refresh_widget_actions_emited) *refresh_widget_actions_emited = true;
	}
	else {
		if(refresh_widget_actions_emited) *refresh_widget_actions_emited = false;
	}
}

void QFDataFormDocument::load(const QVariant &id, QFDataFormDocument::Mode _mode) throw(QFException)
{
	qfLogFuncFrame() << id.toString() << _mode << "isNull:" << id.isNull();
	//if(currentModelRowIndex() < 0) setCurrentModelRowIndex(0);
	setDataId(id);
	Mode m = _mode;
	if((id.isNull() || (id.type() == QVariant::Int && id.toInt() == 0)) && m == ModeEdit) m = ModeInsert;
	//bool copy = false;
	if(m == ModeCopy && !id.isNull()) {
		//setMode(ModeView);
		setMode(m);
		loadData();
		copy();
		emit valuesReloaded(this); /// widgety nastavi RO pro ModeInsert, ktery nastavi copy()
		emit refreshWidgetActions();
	}
	else {
		setMode(m);
		//reload();
		loadData();
		getRowLock();
		if(!isEmpty()) {
			emit valuesReloaded(this);
			//qfInfo() << __LINE__;
			bool refresh_widget_actions_emited;
			setDataDirty_helper(false, &refresh_widget_actions_emited);
			if(!refresh_widget_actions_emited) {
				qfTrash() << "\t reload() emitting refreshWidgetActions()";
				emit refreshWidgetActions();
			}
		}
	}
}

void QFDataFormDocument::reload() throw(QFException)
{
	qfLogFuncFrame();
	loadData();
	qfTrash() << "\tis empty:" << isEmpty();
	if(!isEmpty()) {
		emit valuesReloaded(this);
		//qfInfo() << __LINE__;
		bool refresh_widget_actions_emited;
		setDataDirty_helper(false, &refresh_widget_actions_emited);
		if(!refresh_widget_actions_emited) {
			qfTrash() << "\t reload() emitting refreshWidgetActions()";
			emit refreshWidgetActions();
		}
	}
}

void QFDataFormDocument::save() throw(QFException)
{
	qfLogFuncFrame();
	//qfInfo() << __LINE__;
	if(mode() != ModeView) {
		Mode m = mode();
		emit aboutToSave(this);
		if(saveData()) {
			emit saved(this, m);
			//qfInfo() << __LINE__;
			bool refresh_widget_actions_emited;
			setDataDirty_helper(false, &refresh_widget_actions_emited);
			if(!refresh_widget_actions_emited) emit refreshWidgetActions();
		}
	}
}

void QFDataFormDocument::drop() throw(QFException)
{
	qfLogFuncFrame();
	emit aboutToDrop(this);
	//int ix = currentModelRowIndex();
	QVariant id = dataId();
	if(dropData()) {
		//emit refreshActions();
		/// na poradi zalezi, protoze v reakci na signal dropped se muze stat mnoho veci
		afterDropData_hook();
		emit dropped(id);
	}
	//qfInfo() << QFLog::stackTrace();
}

void QFDataFormDocument::copy()  throw(QFException)
{
	qfLogFuncFrame();
	if(mode() != ModeInsert) {
		copyData();
		setMode(ModeInsert);
		setDataDirty_helper(isDataDirty());
		//setDataDirty_helper(isModelDataDirty());
	}
}

QVariant QFDataFormDocument::origValue(const QString & sql_id, const QVariant & default_value) const
{
	QVariant ret = default_value;
	if(isCalculatedField(sql_id)) {
		//QF_EXCEPTION("Orig values not supported for calculatedFields.");
	}
	else {
		if(isValidFieldName(sql_id)) ret = origValue(sql_id);
	}
	return ret;
}

QString QFDataFormDocument::modeToString(int mode)
{
	QString ret = "UNKNOWN";
	switch(mode) {
		case QFDataFormDocument::ModeView: ret = "VIEW"; break;
		case QFDataFormDocument::ModeEdit: ret = "EDIT"; break;
		case QFDataFormDocument::ModeInsert: ret = "INSERT"; break;
		case QFDataFormDocument::ModeCopy: ret = "COPY"; break;
		case QFDataFormDocument::ModeDelete: ret = "DELETE"; break;
	}
	return ret;
}

//==========================================
//                                   QFTableDataFormDocument
//==========================================
QFTableDataFormDocument::QFTableDataFormDocument(QObject *parent)
	: QFDataFormDocument(parent)
{
	qfLogFuncFrame();
	setCurrentModelRowIndex(-1);
	setIdColumnName("id");
}

QFTableDataFormDocument::~QFTableDataFormDocument()
{
	qfLogFuncFrame();// << QFLog::stackTrace();
}

QFTableModel* QFTableDataFormDocument::model()
{
	if(f_model.isNull()) {
		f_model = new QFTableModel(this);
	}
	return f_model;
}

bool QFTableDataFormDocument::isEmpty() const
{
	//qfInfo() << QF_FUNC_NAME << "currentModelRowIndex:" << currentModelRowIndex() << "model row count:" << model()->rowCount();
	if(!f_model) return true;
	int r = currentModelRowIndex();
	bool ret = !(r >= 0 && r < model()->rowCount());
	//qfInfo() << "\tr:" << r << "row count:" << model()->rowCount() << "ret:" << ret;
	return ret;
}
/*
void QFTableDataFormDocument::load(const QVariant &id, QFDataFormDocument::Mode _mode) throw(QFException)
{
	qfLogFuncFrame() << id.toString() << _mode;
	//if(currentModelRowIndex() < 0) setCurrentModelRowIndex(0);
	setDataId(id);
	Mode m = _mode;
	if(id.isNull() && m == ModeEdit) m = ModeInsert;
	//bool copy = false;
	if(m == ModeCopy && !id.isNull()) {
		setMode(ModeView);
		reload();
		copy();
		emit valuesReloaded(this); /// widgety nastavi RO pro ModeInsert, ktery nastavi copy()
		emit refreshWidgetActions();
	}
	else {
		setMode(m);
		getRowLock();
		reload();
	}
}
*/
bool QFTableDataFormDocument::loadData()
{
	qfLogFuncFrame();
	if(mode() == ModeInsert) {//} && isEmpty()) {
		int ri = currentModelRowIndex();
		if(ri < 0 || ri >= model()->rowCount()) {
			model()->appendRow();
			setCurrentModelRowIndex(model()->rowCount() - 1);
		}
		else {
			/// mrkni, jestli uz nereloaduju insertnutej radek
			QFBasicTable::Row r = model()->table()->row(ri);
			if(!r.isInsert()) {
				ri++;
				model()->insertRow(ri);
				setCurrentModelRowIndex(ri);
			}
		}
	}
	else {
		if(currentModelRowIndex() < 0) setCurrentModelRowIndex(0);
	}
	//qfTrash() << "\tEXIT" << QF_FUNC_NAME;
	return true;
}

void QFTableDataFormDocument::reload() throw(QFException)
{
	qfLogFuncFrame();
	QFDataFormDocument::reload();
	/*
	loadData();
	qfTrash() << "\tis empty:" << isEmpty();
	if(!isEmpty()) {
		emit valuesReloaded(this);
		//qfInfo() << __LINE__;
		bool refresh_widget_actions_emited;
		setDataDirty_helper(false, &refresh_widget_actions_emited);
		if(!refresh_widget_actions_emited) emit refreshWidgetActions();
	}
	*/
}
/*
void QFTableDataFormDocument::save() throw(QFException)
{
	qfLogFuncFrame();
	//qfInfo() << __LINE__;
	if(mode() != ModeView) {
		Mode m = mode();
		emit aboutToSave(this);
		if(saveData()) {
			emit saved(this, m);
			//qfInfo() << __LINE__;
			bool refresh_widget_actions_emited;
			setDataDirty_helper(false, &refresh_widget_actions_emited);
			if(!refresh_widget_actions_emited) emit refreshWidgetActions();
		}
	}
}

void QFTableDataFormDocument::drop() throw(QFException)
{
	qfLogFuncFrame();
	emit aboutToDrop(this);
	//int ix = currentModelRowIndex();
	QVariant id = dataId();
	if(dropData()) {
		/// na poradi zalezi, protoze v reakci na signal dropped se muze stat mnoho veci
		setCurrentModelRowIndex(-1); /// v pripade, ze ma model vic radku, by jinak rowindex ukazoval na jiny platny radek
		//emit refreshActions();
		emit dropped(id);
	}
	//qfInfo() << QFLog::stackTrace();
}

void QFTableDataFormDocument::copy()  throw(QFException)
{
	qfLogFuncFrame();
	if(mode() != ModeInsert) {
		copyData();
		setMode(ModeInsert);
		setDataDirty_helper(isModelDataDirty());
	}
}
*/
bool QFTableDataFormDocument::saveData()
{
	qfLogFuncFrame();
	if(mode() == ModeInsert) {
		setMode(ModeEdit);
	}
	return true;
}

void QFTableDataFormDocument::afterDropData_hook()
{
	/// na poradi zalezi, protoze v reakci na signal dropped se muze stat mnoho veci
	setCurrentModelRowIndex(-1); /// v pripade, ze ma model vic radku, by jinak rowindex ukazoval na jiny platny radek
}

bool QFTableDataFormDocument::copyData()
{
	qfLogFuncFrame();
	if(isEmpty()) return false;
	int ix = currentModelRowIndex();
	QFBasicTable::Row &r = model()->table()->rowRef(ix);
	r.prepareForCopy();
	return true;
}
/*
void QFDataFormDocument::widgetValueUpdated(const QString &sql_id, const QVariant &val)
{
	qfLogFuncFrame();
	qfTrash() << "\t sql id:" << sql_id << "value:" << val.toString();
	setValue(sql_id, val);
}
*/
/*
QFBasicTable::Row& QFDataFormDocument::rowRef() throw(QFException)
{
	qfLogFuncFrame();
	if(table()->isEmpty()) QF_ABORT_EXCEPTION("Table is empty");
	return table()->rowRef(0);
}

QVariant QFDataFormDocument::idColumnValue() const
{
	qfLogFuncFrame();// << QFLog::stackTrace();
	qfTrash() << "\tidColumnName:" << idColumnName();
	return value(idColumnName());
}
*/
bool QFTableDataFormDocument::isValidFieldName(const QString &sql_id) const
{
	//qfLogFuncFrame() << sql_id;
	if(isCalculatedField(sql_id)) return true;
	const QFTableModel *m = model();
	return m->fieldIndex(sql_id, !Qf::ThrowExc) >= 0;
}

bool QFTableDataFormDocument::isDirty(const QString &sql_id) const
{
	if(!isValidFieldName(sql_id)) return false;
	//if(isCalculatedField(sql_id)) return false;
	int r = currentModelRowIndex();
	return model()->isDirty(r, sql_id);
}

void QFTableDataFormDocument::clearDataDirty()  throw(QFException)
{
	int r = currentModelRowIndex();
	model()->clearDirty(r);
}

bool QFTableDataFormDocument::isModelDataDirty() const
{
	const QFTableModel *m = model();
	if(m) {
		int ri = currentModelRowIndex();
		QFBasicTable::Row r = m->table()->row(ri);
		return r.isDirty();
	}
	return false;
	/*
	foreach(QFTableModel::ColumnDefinition cd, m->columns()) {
		//qfTrash() << "\ttrying:" << cd.fieldName();
		if(isDirty(cd.fieldName())) return true;
	}
	return false;
	*/
}

QVariant::Type QFTableDataFormDocument::fieldType(const QString &sql_id) const  throw(QFException)
{
	if(isCalculatedField(sql_id)) {
		return calculatedFields.value(sql_id).type();
	}
	QFBasicTable::FieldList flds = tableFields();
	int ix = flds.fieldNameToIndex(sql_id);
	return flds[ix].type();
}

QVariant QFTableDataFormDocument::origValue(const QString & sql_id) const throw( QFException )
{
	QVariant ret;
	if(isCalculatedField(sql_id)) {
		QF_EXCEPTION("Orig values not supported for calculatedFields.");
	}
	else {
		const QFTableModel *m = model();
		int r = currentModelRowIndex();
		ret = m->origValue(r, sql_id);
		//qfTrash() << "\treturn:" << ret.toString() << "type:" << QVariant::typeToName(ret.type());
	}
	return ret;
}

QVariant QFTableDataFormDocument::value(const QString &sql_id) const  throw(QFException)
{
	//qfLogFuncFrame() << sql_id;
	QVariant ret;
	if(isCalculatedField(sql_id)) {
		ret = calculatedFields[sql_id];
	}
	else {
		const QFTableModel *m = model();
		int r = currentModelRowIndex();
		ret = m->value(r, sql_id);
		//qfTrash() << "\treturn:" << ret.toString() << "type:" << QVariant::typeToName(ret.type());
	}
	return ret;
}

void QFTableDataFormDocument::setValue(const QString &sql_id, const QVariant &val)  throw(QFException)
{
	qfTrash().color(QFLog::Yellow) << QF_FUNC_NAME << "sql_id:" << sql_id << "val:" << val.toString();
	if(isEmpty()) {
		qfWarning() << "sql_id:" << sql_id << "val:" << val.toString();
		qfWarning() << "QFTableDataFormDocument::setValue() empty document";
	}
	QVariant old_val;
	if(isCalculatedField(sql_id)) {
		old_val = calculatedFields[sql_id];
		calculatedFields[sql_id] = val;
	}
	else {
		QFTableModel *m = model();
		int r = currentModelRowIndex();
		//int c = m->columnIndex(sql_id);
		old_val = m->value(r, sql_id);
		m->setValue(r, sql_id, val);
		//qfInfo() << m->value(r, sql_id).toString();
	}
	if(old_val != val) {
		emit valueEdited(sql_id, old_val, val);
		setDataDirty_helper(true);
	}
}

QFBasicTable::FieldList QFTableDataFormDocument::tableFields() const
{
	if(f_model.isNull()) { return QFBasicTable::FieldList(); }
	if(model()->tableIsNull()) { return QFBasicTable::FieldList(); }
	return model()->table()->fields();
}

QFTableModel::ColumnList QFTableDataFormDocument::modelColumns() const
{
	if(f_model.isNull()) { return QFTableModel::ColumnList(); }
	return model()->columns();
}

QFXmlTable QFTableDataFormDocument::toXmlTable(QDomDocument & owner_doc, const QString & table_name) const throw( QFException )
{
	QFXmlTable t(owner_doc, table_name);
	/// export columns
	for(int i=0; i<model()->table()->fields().count(); i++) {
		QFSqlField f = model()->table()->field(i);
		t.appendColumn(f.fullName(), f.type());
	}
	/// calculatedFields
	{
		QHashIterator<QString, QVariant> i(calculatedFields);
		while (i.hasNext()) {
			i.next();
			t.appendColumn(i.key(), fieldType(i.key()));
		}
	}
	/// export data
	QFBasicTable::Row r = model()->table()->row(currentModelRowIndex());
	QFXmlTableRow t_r = t.appendRow();
	for(int i=0; i<tableFields().count(); i++) t_r.setValue(i, r.value(i));
	/// calculatedFields
	{
		QHashIterator<QString, QVariant> i(calculatedFields);
		while (i.hasNext()) {
			i.next();
			t_r.setValue(i.key(), value(i.key()));
		}
	}
	return t;
}

//==========================================
//                                   QFSqlFormDocument
//==========================================
QFSqlFormDocument::QFSqlFormDocument(QObject *parent, const QFSqlConnection &con)
	: QFTableDataFormDocument(parent), f_connection(con)
{
	f_rowLock = NULL;
}

QFSqlFormDocument::~ QFSqlFormDocument()
{
	SAFE_DELETE(f_rowLock);
}

void QFSqlFormDocument::createRowLock()
{
	f_rowLock = new QFSqlFormDocumentRowLock(this);
	//f_rowLock->setIdFieldName(idColumnName());
}

QFSqlFormDocumentRowLock * QFSqlFormDocument::rowLock() const
{
	if(!f_rowLock) {
		const_cast<QFSqlFormDocument*>(this)->createRowLock();
		//connect(f_rowLock, SIGNAL(rowLockingInfo(int, bool, const QString &)), this, SLOT(rowLockingInfo(int, bool, const QString &)));
	}
	return f_rowLock;
}

QFSqlQueryTableModel* QFSqlFormDocument::model()
{
	//qfLogFuncFrame();
	if(f_model.isNull()) {
		qfTrash() << "\t creating new QFSqlQueryTableModel";
		f_model = new QFSqlQueryTableModel(this);
	}
	QFSqlQueryTableModel *ret = qobject_cast<QFSqlQueryTableModel *>(f_model);
	QF_ASSERT(ret, "Model is not a kind of QFSqlQueryTableModel.");
	return ret;
}

QFSqlConnection QFSqlFormDocument::connection() throw(QFException)
{
	if(!f_connection.isValid()) {
		qfLogFuncFrame();
		f_connection = qfDbApp()->connection();
		qfTrash() << "\t using application wide connection:" << f_connection.signature();
		if(!f_connection.isValid()) QF_EXCEPTION("Connection is not valid.");
	}
	return f_connection;
}

bool QFSqlFormDocument::loadData()
{
	qfLogFuncFrame();
	QString qs = query();
	if(!dataId().isValid()) {
		//if(mode() == ModeInsert) setDataId(-1);
		/// dohle dobre funguje pro int autoicrement ID, pro jiny typy to muze delat potize :(
		setDataId(-1);
	}
	//qfInfo() << "\tdata id:" << dataId().toString() << "valid:" << dataId().isValid() << "mode:" << mode();
	//qfInfo() << "\tqs:" << qs << "empty:" << qs.isEmpty();
	if(!qs.isEmpty() && dataId().isValid()) {
		//qfInfo() << "\treplace";
		qs = qs.replace("${ID}", QFSql::formatValue(dataId()));
		qfTrash().color(QFLog::Yellow) << "\t dataId:" << dataId().toString();
		qfTrash().color(QFLog::Yellow) << "\t query:" << qs;
		model()->table()->reload(qs);
		//qfInfo() << "model row count:" << model()->rowCount() << "table row count:" << model()->table()->rowCount();
		//QF_ASSERT(table()->rowCount() == 1, "Query problem row count is "IARG(table()->rowCount())" and should be 1.");
	}
	else {
		/// pokud dokument nema query, muze to byt pohled na radek tabulky nebo ma model query reseno jinak.
		/*
		/// V takovem pripade je treba reloadnout tento radek
		int ix = currentModelRowIndex();
		if(ix >= 0) {
			qfTrash().color(QFLog::Yellow) << "\t reloading row:" << ix;
			if(!isEmpty()) model()->reloadRow(ix);
		}
		*/
	}
	QFTableDataFormDocument::loadData();
	return true;
}

bool QFSqlFormDocument::saveData()
{
	qfLogFuncFrame();
	if(mode() != ModeView) {
		//qfTrash() << "\ttable type:" << model()->table()->metaObject()->className();
		model()->table()->postRow(currentModelRowIndex());
		if(mode() == ModeInsert) {
			QVariant id = value(idColumnName());
			qfTrash() << "\tid column name:" << idColumnName() << "inserted ID:" << id.toString();
			setDataId(id);
			setValue(idColumnName(), id); /// aby se obnovily i widgety s sqlId() == "id"
		}
	}
	return QFTableDataFormDocument::saveData();
}

bool QFSqlFormDocument::dropData()
{
	qfLogFuncFrame();
	//qfInfo() << QFLog::stackTrace();
	if(mode() != ModeView) {
		//qfTrash() << "\ttable type:" << model()->table()->metaObject()->className();
		model()->table()->removeRow(currentModelRowIndex());
	}
	QFTableDataFormDocument::dropData();
	return true;
}

void QFSqlFormDocument::getRowLock()
{
	qfLogFuncFrame();
	rowLock()->unlock();
	if(mode() == ModeEdit) {
		//int locking_status;
		//rowLock()->setDocumentRowId(dataId());
		bool aquired = rowLock()->lock();
		if(!aquired) {
			qfTrash() << "\t NOT AQUIRED dataId:" << dataId().toString();
			setMode(ModeView);
		}
		else {
			qfTrash() << "\t AQUIRED dataId:" << dataId().toString();
		}
		emit rowLockingInfo(rowLock()->documentLockId(), aquired, rowLock()->lockingStatus());
		//qfInfo() << "LOCK INFO" << rowLock()->isLockedByMe();
	}
	else {
		//rowLock()->setRowId(QVariant());
		//qfInfo() << "LOCK INFO" << rowLock()->isLockedByMe();
	}
}

void QFSqlFormDocument::releaseRowLock()
{
	//qfInfo() << "releaseRowLock() row lock:" << f_rowLock;
	if(f_rowLock) f_rowLock->unlock();
}

bool QFSqlFormDocument::hasWriteLock() const
{
	qfLogFuncFrame();
	if(mode() == ModeEdit) {
		qfTrash() << "\t mode:EDIT doc lock id:" << rowLock()->documentLockId() << "current connection id:" << rowLock()->currentConnectionId();
		if(!f_rowLock || rowLock()->currentConnectionId() == 0) return true;
		return rowLock()->isLockedByMe();
	}
	return true;
}

void QFSqlFormDocument::addForeignKey(const QString & master_key_name, const QString & detail_key_name)
{
	model()->table()->addForeignKey(master_key_name, detail_key_name);
}

//==========================================
//                   QFSqlFormDocumentRowLock
//==========================================

QFSqlFormDocument * QFSqlFormDocumentRowLock::document() throw(QFException)
{
	if(!f_document) QF_EXCEPTION("row lock should have pointer to QFSqlFormDocument");
	return f_document;
}

QString QFSqlFormDocumentRowLock::idFieldName()
{
	QString ret;
	ret = document()->idColumnName();
	return ret;
}

void QFSqlFormDocumentRowLock::filterConcurentConnectionIds(QSet< int > & active_connections_ids)
{
	Q_UNUSED(active_connections_ids);
}

bool QFSqlFormDocumentRowLock::lock()
{
	qfLogFuncFrame();// << "lock relation:" << lockedRelation();
	f_lockingStatus = LockIgnored;
	bool ret = false;
	/*
	struct LockDisposer
	{
		~LockDisposer() {
			QFSqlQuery q(qfDbApp()->connection());
			qfTrash() << "unlocking tables";
			q.exec("UNLOCK TABLES");
			qfInfo() << "unlock OK";
		}
	};
	LockDisposer lockDisposer;
	Q_UNUSED(lockDisposer);
	*/
	do {
		if(lockedTableIdFieldName().isEmpty()) {ret = true; break;}
		if(document()->isEmpty()) QF_EXCEPTION("document is empty");
		
		//unlock();
		int row_id = document()->value(lockedTableIdFieldName()).toInt();
		if(row_id <= 0) QF_EXCEPTION("locked table row id should be int greater than 0, locked table field name: " + lockedTableIdFieldName());

		QFSqlQuery q(qfDbApp()->connection());
		/// zjisti svoje Connection ID
		q.exec("SELECT CONNECTION_ID()");
		f_currentConnectionId = 0;
		if(q.next()) f_currentConnectionId = q.value(0).toInt();
		QF_ASSERT(f_currentConnectionId > 0, "can not get my connection id");
		qfTrash() << "\tmy connection id:" << f_currentConnectionId;

		QSet<int> active_connections_ids;
		q.exec("SELECT ID FROM information_schema.PROCESSLIST");
		while(q.next()) {
			active_connections_ids << q.value("ID").toInt();
		}

		filterConcurentConnectionIds(active_connections_ids);

		QString table_name = lockedTableIdFieldName().section('.', 0, 0);
		{
			QStringList sl;
			foreach(int id, active_connections_ids) sl << QString::number(id);
			qfTrash() << "\t concurent connection ids:" << sl.join(",");
			QString qs = "UPDATE " + table_name + " SET rowlock = " + QString::number(currentConnectionId()) +
					" WHERE id = "IARG(row_id);
			if(!sl.isEmpty()) qs += " AND rowlock NOT IN (" + sl.join(",") + ")";
			qfTrash() << qs;
			q.exec(qs);
		}
		/// precti si, jestli jsi ho dostal
		{
			QString qs = "SELECT rowlock FROM " + table_name + " WHERE id = "IARG(row_id);
			qfTrash() << qs;
			q.exec(qs);
			f_documentLockId = 0;
			if(q.next()) f_documentLockId = q.value(0).toInt();
			else QF_EXCEPTION(tr("Row cannot be locked, because row id does not exists. Row id: %1, table: '%2'").arg(row_id).arg(table_name));
			qfTrash() << "\tlock documentLockId:" << documentLockId() << "currentConnectionId:" << currentConnectionId();
		}

		if(documentLockId() == currentConnectionId()) {
			f_lockingStatus = LockAquired;
			ret = true;
		}
		else {
			f_lockingStatus = LockFailed;
		}
	} while(false);
	return ret;
}

void QFSqlFormDocumentRowLock::unlock()
{
	qfLogFuncFrame();// << f_tableName << this;
	if(isLockedByMe() && !document()->isEmpty()) { /// pri vymazani dokumentu se zamozrejme vymaze i rowlock, takze ho ani nelze explicitne uvolnit
		QFSqlQuery q(qfDbApp()->connection());
		QString table_name = lockedTableIdFieldName().section('.', 0, 0);
		int row_id = document()->value(lockedTableIdFieldName()).toInt();
		if(row_id <= 0) {
			QF_EXCEPTION("locked table row id should be int greater than 0, locked table field name: " + lockedTableIdFieldName());
		}
		q.exec("UPDATE " + table_name + " SET rowlock = -1"
				" WHERE ID = " IARG(row_id));
		qfTrash() << "\t" << table_name << "LOCK" << row_id << "RELEASED";
	}
	f_documentLockId = 0;
}

bool QFSqlFormDocumentRowLock::isLockedByMe() const
{
	//qfInfo() << QF_FUNC_NAME;
	//qfInfo() << "documentLockId():" << documentLockId() << "currentConnectionId():" << currentConnectionId() << "return:" << (documentLockId() == currentConnectionId() && documentLockId() > 0);
	//qfInfo() << QFLog::stackTrace();
	return (documentLockId() == currentConnectionId() && documentLockId() > 0);
}
