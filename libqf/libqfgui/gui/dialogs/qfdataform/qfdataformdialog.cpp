
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdataformdialog.h"

#include <qfmessage.h>
#include <qfpixmapcache.h>
#include <qfaction.h>
#include <qfapplication.h>
#include <qfdataformdocument.h>
#include <qfdataformwidget.h>
#include <qfdataformdialogwidget.h>

#include <QKeyEvent>

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

//==========================================
//                                   QFDataFormDialog
//==========================================
QFDataFormDialog::QFDataFormDialog(QWidget * parent, Qt::WFlags f)
	: QFDialog(parent, f), fDataFormDialogWidget(NULL)
{
	qfTrash() << QF_FUNC_NAME;
}

QFDataFormDialog::~QFDataFormDialog()
{
}

void QFDataFormDialog::setDataFormDialogWidget(QFDataFormDialogWidget *w)
{
	if(!w) return;
	if(fDataFormDialogWidget) QF_EXCEPTION("Dialog has allready its DataFormDialogWidget");
	setDialogWidget(w);
	fDataFormDialogWidget = w;
	//QHBoxLayout *ly = new QHBoxLayout(centralWidget());
	//ly->setMargin(0);
	//ly->addWidget(w);
	connect(this, SIGNAL(execParams(const QVariant &, QFDataFormDocument::Mode)), w, SLOT(load(const QVariant &, QFDataFormDocument::Mode)));
	/*
	connect(w, SIGNAL(wantClose(QFDialogWidget*)), this, SLOT(reject()));
	setXmlConfigPersistentId(w->xmlConfigPersistentId());
	*/
}

QFDataFormDialogWidget* QFDataFormDialog::dataFormDialogWidget() const throw(QFException)
{
	if(!fDataFormDialogWidget) QF_EXCEPTION("DataFormDialogWidget is NULL.");
	return fDataFormDialogWidget;
}

/*
void QFDataFormDialog::accept()
{
	qfTrash() << QF_FUNC_NAME;
	centralWidget()->setFocus(); /// make the focusOut event
	QFPartDialog::accept();
}

void QFDataFormDialog::reject()
{
	qfTrash() << QF_FUNC_NAME;
}
*/

int QFDataFormDialog::exec(const QVariant &row_id, QFDataFormDocument::Mode mode)
{
	qfLogFuncFrame();
	qfTrash() << "\t emitting execParams()";
	emit execParams(row_id, mode);
	int ret = QFDialog::exec();
	qfTrash() << "\treturning:" << ret;
	return ret;
}

void QFDataFormDialog::keyPressEvent(QKeyEvent *e)
{
	qfTrash() << QF_FUNC_NAME;
	bool modified = (e->modifiers() == Qt::ControlModifier)
			|| (e->modifiers() == Qt::AltModifier)
			|| (e->modifiers() == Qt::MetaModifier);
	if(!modified && (e->key() == Qt::Key_Enter || e->key() == Qt::Key_Return)) {
		save();
		accept();
		return;
	}
	QDialog::keyPressEvent(e);
}
/*
void QFDataFormDialog::closeEvent(QCloseEvent *e)
{
	save();
	QDialog::closeEvent(e);
}
*/
/*
void QFDataFormDialog::accept()
{
	qfTrash() << QF_FUNC_NAME;
	save();
	QDialog::accept();
}

void QFDataFormDialog::reject()
{
	qfTrash() << QF_FUNC_NAME;
	QDialog::reject();
}
*/
void QFDataFormDialog::save()
{
	qfTrash() << QF_FUNC_NAME;
	dataFormDialogWidget()->save();
}

