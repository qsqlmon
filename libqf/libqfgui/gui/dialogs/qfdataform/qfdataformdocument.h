
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDATAFORMDOCUMENT_H
#define QFDATAFORMDOCUMENT_H

#include <qfguiglobal.h>
#include <qfsqlquerytablemodel.h>
#include <qfclassfield.h>

#include <QObject>
//#include <QScriptValue>

class QFSqlQuery;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFDataFormDocument : public QObject
{
	Q_OBJECT;
	friend class QFDataFormWidget;
	public:
		typedef QHash<QString, QVariant> CalculatedFields;
		enum Mode {ModeView = 0, ModeEdit, ModeInsert, ModeCopy, ModeDelete};
		QF_FIELD_RW(Mode, m, setM, ode);
		/// closed dokument nejde editovat widgety DocumentWidgetu, pokud nemaji atribut ignoreClosedForEdits,
		/// napriklad vystaveny nebo stornovany dokumnet je closed.
		virtual bool isClosedForEdits() const {return false;}
	protected:
		QVariant f_dataDirty;
		/// ID radku v SQL databazi
		QVariant fDataId;
		CalculatedFields calculatedFields;
	public:
		//! pokud tato funkce vrati true, jsou vsechny widgetu majici property externalData == true nastaveny readonly
		virtual bool isExternalDataReadOnly() const {return false;}
		/// base implementace nedela nic, protoze v qflib neni zadnej user (ikdyz by granty mozna stalo zato presunout z wlib do qflib).
		virtual bool currentUserHasGrant(const QString &grant) const {Q_UNUSED(grant); return true;}
		void setDataId(const QVariant &id) {fDataId = id;}
		virtual QVariant dataId() const;
		void refreshFieldControls(const QString &sql_id = QString()) {emit valueSet(sql_id);}
	signals:
		///emituje se po valueEdited nebo valuesReloaded, pokud je to po valuesReloaded() sql_id == QString()
		void valueSet(const QString &sql_id = QString());
		/// emited after each slot setValue(const QString &sql_id, const QVariant &val) invocation.
		void valueEdited(const QString &sql_id, const QVariant &old_val, const QVariant &new_val);
		//! Signal for QFDataFormWidget.
		void valuesReloaded(QFDataFormDocument *doc);
		/// emituje se pred ulozenim dokumentu
		void aboutToSave(QFDataFormDocument *doc);
		/// emituje se pred smazanim dokumentu
		void aboutToDrop(QFDataFormDocument *doc);
		void saved(QFDataFormDocument *doc, int mode);
		//! ri je cislo radku v modelu dokumentu
		void dropped(const QVariant &id);
		//void inserted(QFDataFormDocument *doc); neni potreba, staci saved, ze slo o insert se pozna z mode()
		/// emituje se v pripade, ze se neco stalo, save, drop, ....
		/// vhodny napriklad pro slot refreshActions() vidgetu, ktere dokument ovladaji
		void refreshWidgetActions();
		void dataDirtyChanged(bool dirty);
	protected:
		void setDataDirty_helper(bool dirty, bool *refresh_widget_actions_emited = NULL);
		virtual void afterDropData_hook() {}

		bool isCalculatedField(const QString &sql_id) const {return calculatedFields.contains(sql_id);}
	protected:
		///! funkce, ktera naplni radek daty pri reloadu.
		virtual bool loadData() {return true;}
		///! funkce, backupuje data pri save().
		//! vraci, jestli se to podarilo nebo ne
		virtual bool saveData()  {return true;}
		///! funkce, ktera smaze data na serveru pri drop().
		virtual bool dropData() {return true;}
		//! zavola prepareForCopy() pro odpovidajici radek v tabulce
		virtual bool copyData()  {return true;}
	public slots:
		//virtual void widgetValueUpdated(const QString &sql_id, const QVariant &val);
		//! Calls saveData() and emit signals, in most cases is better to overload saveData().
		/// emituje \a aboutToSave() a vola \a saveData().
		virtual void save() throw(QFException);
		//! Base class implementation emits \a fieldValuesChanged(table()->row(0)) .
		//! If model is empty and mode() is ModeInsert it also calls \a insert() function.
		virtual void load(const QVariant &id = QVariant(), QFDataFormDocument::Mode mode = ModeEdit) throw(QFException);
		//! this is a convinience function, @see load() .
		void loadForInsert() throw(QFException) {load(QVariant(), ModeInsert);}
		
		//! Calls loadData() and emit signals, in most cases is better to overload loadData().
		virtual void reload() throw(QFException);
		//! Calls dropData() and emit signals, in most cases is better to overload dropData().
		virtual void drop() throw(QFException);
		/// Uvede se do ModeInsert a do takoveho stavu, ze po zavolani funkce save() vznikne klon puvodniho dokumentu.
		/// V ModeInsert neudela nic.
		virtual void copy() throw(QFException);

		void setCalculatedField(const QString &field_name, const QVariant &value) {calculatedFields[field_name] = value;}
		/// prepocitej calculatedFields
		virtual void recalculate() {}
	public:
		virtual bool isEmpty() const;
	public:
		virtual void getRowLock() {}
		virtual void releaseRowLock() {}
		virtual bool hasWriteLock() const {return true;}
		//virtual QString lockInfo() const {return QString();}
	public:
		virtual void clearDataDirty() {f_dataDirty = false;}
		virtual bool isDataDirty() const {return f_dataDirty.toBool();}
		//! zjisti jestli v modelu existuje field s \a sql_id .
		virtual bool isValidFieldName(const QString &sql_id) const = 0;
		//! vraci true, pokud je hodnoda \a sql_id dirty, pro calculatedFields vraci vzdy false.
		virtual bool isDirty(const QString &sql_id) const = 0;
		//! vraci typ fieldu \a sql_id, calculated field ma typ toho, co do nej bylo ulozeno.
		QVariant::Type fieldType(const QString &sql_id) const  throw(QFException);
		Q_INVOKABLE virtual QVariant value(const QString &sql_id) const  throw(QFException) = 0;
		Q_INVOKABLE virtual QVariant value(const QString &sql_id, const QVariant &default_value) const {
			if(isValidFieldName(sql_id) && !isEmpty()) return value(sql_id);
			return default_value;
		}
		virtual QVariant origValue(const QString &sql_id) const  throw(QFException) = 0;
		QVariant origValue(const QString &sql_id, const QVariant &default_value) const;

		static QString modeToString(int mode);
	public slots:
		virtual void setValue(const QString &sql_id, const QVariant &val)  throw(QFException) = 0;
	public:
		QFDataFormDocument(QObject *parent = NULL);
		virtual ~QFDataFormDocument();
};

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFTableDataFormDocument : public QFDataFormDocument
{
	Q_OBJECT;
	QF_FIELD_RW(QString, i, setI, dColumnName);
	QF_FIELD_RW(int, c, setC, urrentModelRowIndex);
	protected:
		QPointer<QFTableModel> f_model;
		///! funkce, ktera naplni radek daty pri reloadu.
		virtual bool loadData();
		///! funkce, backupuje data pri save().
		//! vraci, jestli se to podarilo nebo ne
		virtual bool saveData();
		///! funkce, ktera smaze data na serveru pri drop().
		//virtual bool dropData();
		virtual void afterDropData_hook();
		//! zavola prepareForCopy() pro odpovidajici radek v tabulce
		virtual bool copyData();
	public slots:
		//virtual void widgetValueUpdated(const QString &sql_id, const QVariant &val);
		//! Calls saveData() and emit signals, in most cases is better to overload saveData().
		/// emituje \a aboutToSave() a vola \a saveData().
		//virtual void save() throw(QFException);
		//! Base class implementation emits \a fieldValuesChanged(table()->row(0)) .
		//! If model is empty and mode() is ModeInsert it also calls \a insert() function.
		//virtual void load(const QVariant &id = QVariant(), QFDataFormDocument::Mode mode = ModeEdit) throw(QFException);
		
		//! Calls loadData() and emit signals, in most cases is better to overload loadData().
		virtual void reload() throw(QFException);
		//! Calls dropData() and emit signals, in most cases is better to overload dropData().
		//virtual void drop() throw(QFException);
		/// Uvede se do ModeInsert a do takoveho stavu, ze po zavolani funkce save() vznikne klon puvodniho dokumentu.
		/// V ModeInsert neudela nic.
		//virtual void copy() throw(QFException);
	public:
		//! Return the document model set by calling \a setModel() or create a new one if no one was set.
		/**
		Never returns NULL.
		The document has no model, before someone calls this function.
		 */
		virtual QFTableModel* model();
		const QFTableModel* model() const {return const_cast<QFTableDataFormDocument*>(this)->model();}
		void setModel(QFTableModel* m) {f_model = m;}
		/*
		*       First call of this function creates new table of type QFBasicTable.
		*/
		//virtual QFBasicTable* table();
		/// currentModelRowIndex() ukazuje na neplatny radek
		virtual bool isEmpty() const;
		bool isFieldsLoaded() const {return !tableFields().isEmpty();}
		/// vrati seznam sloupcu tabulky pod modelem
		QFBasicTable::FieldList tableFields() const;
		/// vrati seznam sloupcu modelu
		QFTableModel::ColumnList modelColumns() const;
		QFXmlTable toXmlTable(QDomDocument &owner_doc, const QString &table_name = QString()) const throw(QFException);
	public:
		virtual void clearDataDirty() throw(QFException);
		virtual bool isDataDirty() const {return f_dataDirty.toBool() && isModelDataDirty();}
		//! zjisti jestli v modelu existuje field s \a sql_id .
		virtual bool isValidFieldName(const QString &sql_id) const;
		//! vraci true, pokud je hodnoda \a sql_id dirty, pro calculatedFields vraci vzdy false.
		virtual bool isDirty(const QString &sql_id) const;
		//! vraci true, pokud je hodnoda \a sql_id dirty pro nektere z ne calculatedFields true.
		bool isModelDataDirty() const;
		//! vraci typ fieldu \a sql_id, calculated field ma typ toho, co do nej bylo ulozeno.
		QVariant::Type fieldType(const QString &sql_id) const  throw(QFException);
		virtual QVariant value(const QString &sql_id) const  throw(QFException);
		virtual QVariant value(const QString &sql_id, const QVariant &default_value) const {
			return QFDataFormDocument::value(sql_id, default_value);
		}
		virtual QVariant origValue(const QString &sql_id) const  throw(QFException);
		QVariant origValue(const QString &sql_id, const QVariant &default_value) const {
			return QFDataFormDocument::origValue(sql_id, default_value);
		}
	public slots:
		virtual void setValue(const QString &sql_id, const QVariant &val)  throw(QFException);
	public:
		QFTableDataFormDocument(QObject *parent = NULL);
		virtual ~QFTableDataFormDocument();
};

class QFSqlFormDocument;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlFormDocumentRowLock// : public QObject
{
	//Q_OBJECT;
	Q_DECLARE_TR_FUNCTIONS(QFSqlFormDocumentRowLock);
	public:
		enum LockingStatus {LockFailed = 0, LockIgnored, LockAquired};
	protected:
		QFSqlFormDocument *f_document;
		QString f_lockedTableIdFieldName; ///< id fieldname v tabulce, ktera obsahuje field rowlock
		//int f_lockedRowId; ///< id zamykaneho radku, musim si ho pamatovat, protoze pokud je dokument vymazan, vola se v destruktoru unlock 
		QVariant f_lockedTableRowId; ///< id radku v tabulce, ktera obsahuje field rowlock
		//QString f_lockedTableName;
		int f_currentConnectionId; ///< id meho threadu
		int f_documentLockId; ///< id threadu, ktery vlastni lock dokumentu
		int f_lockingStatus;
	protected:
		virtual void filterConcurentConnectionIds(QSet<int> &active_connections_ids);
		//void saveLock(bool aquired);
		//virtual bool furtherLockChecks(bool aquired) {return aquired;}
		QFSqlFormDocument* document() throw(QFException);
		QString idFieldName();
		/// vraci par id_field_name, id_value
		QPair<QString, int> findLockedTableId() const;
	public:
		//void setIdFieldName(const QString &fn) {f_idFieldName = fn;}
		void setLockedTableIdFieldName(const QString &fn) {f_lockedTableIdFieldName = fn;}
		QString lockedTableIdFieldName() const {return f_lockedTableIdFieldName;}
		//void setRowId(const QVariant &_id) {f_rowId = _id;}

		/// nastavuje lockingStatus() vraci LockingStatus
		virtual bool lock();
		void unlock();
		bool isLockedByMe() const;
		bool isLockedByOther() const {return f_documentLockId != f_currentConnectionId && f_documentLockId > 0;}
		int documentLockId() const {return f_documentLockId;}
		int currentConnectionId() const {return f_currentConnectionId;}
		int lockingStatus() const {return f_lockingStatus;}
	//signals:
		//void rowLockingInfo(int server_thread_id, bool lock_aquired, const QString &message);
	public:
		QFSqlFormDocumentRowLock(QFSqlFormDocument *doc) : f_document(doc), f_currentConnectionId(0), f_documentLockId(0) {}
		virtual ~QFSqlFormDocumentRowLock() {unlock();}
};

class QFGUI_DECL_EXPORT QFSqlFormDocument : public QFTableDataFormDocument
{
	Q_OBJECT
			//QF_FIELD_RW(QString, l, setL, ockedTableName);
	protected:
		QFSqlFormDocumentRowLock *f_rowLock;
		QFSqlConnection f_connection;
		QString f_query;
	protected:
		//! return SQL query in form 'SELECT .... WHERE .... ${ID}', ${ID} will be replaced by dataId() in reload() function.
		virtual QString query() {return f_query;}

		virtual bool saveData();
		virtual bool loadData();
		virtual bool dropData();
		//virtual bool cloneData();
		virtual void createRowLock();
		QFSqlFormDocumentRowLock* rowLock() const;
	public:
		void setQuery(const QString &qs) {f_query = qs;}
		//void load(const QVariant &id, QFDataFormDocument::Mode mode) {QFDataFormDocument::reload(id, mode);}

		/**
		 *       First call of this function creates new model of type QFSqlQueryTableModel.
		 @sa QFDataFormDocument::model().
		 */
		virtual QFSqlQueryTableModel* model();
		//! If no connection is set in constructor, take QFQpplication::connection()
		//! Throws exception if connection is not valid.
		QFSqlConnection connection() throw(QFException);

		///@deprecated
		void setLockedTableName(const QString &table_name) {rowLock()->setLockedTableIdFieldName(table_name + ".id");}
		/// muze mit celkam 2 formaty
		/// table_name - field je potom id
		/// table_name.id_field_name 
		void setLockedTableIdFieldName(const QString &fn) {rowLock()->setLockedTableIdFieldName(fn);}
		virtual bool hasWriteLock() const;
		//virtual QString lockInfo() const {return rowLock()->info();}
		/// pokud zde je neco jako setForeignKeyInfo('objednavky.Id', 'objparapet.objednavkaId'), insertne se i do joined tabulek
		/// keys jsou ve forme tablename.fieldname
		void addForeignKey(const QString &master_key_name, const QString &detail_key_name);
	protected:
		virtual void getRowLock();
		virtual void releaseRowLock();
	signals:
		void rowLockingInfo(int server_thread_id, bool lock_aquired, int locking_status);
	public:
		QFSqlFormDocument(QObject *parent = NULL, const QFSqlConnection &con = QFSqlConnection());
		virtual ~QFSqlFormDocument();
};


#endif // QFDATAFORMDOCUMENT_H

