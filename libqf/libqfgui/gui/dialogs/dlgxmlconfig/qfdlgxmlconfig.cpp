//#include <QDomNodeList>

#include <qfdlgexception.h>
#include <qfdlgxmlconfig.h>
#include <qfxmlconfigwidget.h>
#include <qfxmltreemodel.h>
#include <qfaction.h>

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

//==================================================
//                                 QFDlgXmlConfig
//==================================================
QFDlgXmlConfig::QFDlgXmlConfig(QWidget *parent) :
		QFButtonDialog(parent)
{
	configWidget = new QFXmlConfigWidget();
	configWidget->setToolBarVisible(false);
	setDialogWidget(configWidget);
	QToolButton *btDefault = new QToolButton();
	btDefault->setDefaultAction(configWidget->action("defaults"));
	buttonBox()->addButton(btDefault, QDialogButtonBox::ResetRole);
	setButtonsVisible(ButtonNone, false);
}

QFDlgXmlConfig::~QFDlgXmlConfig()
{
}

void QFDlgXmlConfig::setConfig(QFXmlConfig *doc)
{
	configWidget->setConfig(doc);
}

void QFDlgXmlConfig::accept()
{
	qfTrash() << QF_FUNC_NAME;
	configWidget->saveCurrentlyEdited();
	//qfTrash() << ui->configWidget->content()->toString();
	QFDialog::accept();
}

