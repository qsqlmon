#ifndef QFDLGXMLCONFIG_H
#define QFDLGXMLCONFIG_H

#include <qfguiglobal.h>
#include <qfbuttondialog.h>

class QFXmlConfig;
class QFXmlConfigWidget;

/// Dokumentace chybi
class QFGUI_DECL_EXPORT QFDlgXmlConfig : public QFButtonDialog
{
	Q_OBJECT
	protected:
		QFXmlConfigWidget *configWidget;
	public:
		QFDlgXmlConfig(QWidget *parent = 0);
		virtual ~QFDlgXmlConfig();

		virtual void accept();

		void setConfig(QFXmlConfig *doc);
		//const QFXmlConfigDocument& content();
};

#endif // QFDLGXMLCONFIG_H
