
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdlgprogress.h"
#include "ui_qfdlgprogress.h"

QFDlgProgress::QFDlgProgress(QWidget *parent) :
	QDialog(parent)
{
	ui = new Ui::QFDlgProgress;
	ui->setupUi(this);
	setModal(false);
}

QFDlgProgress::~QFDlgProgress()
{
	delete ui;
}

