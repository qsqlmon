
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDLGPROGRESS_H
#define QFDLGPROGRESS_H

#include <qfguiglobal.h>

#include <QDialog>

namespace Ui {
	class QFDlgProgress;
}
 
//! @deprecated use QProgressdialog instead.
class QFGUI_DECL_EXPORT QFDlgProgress : public QDialog
{
	Q_OBJECT
	protected:
		Ui::QFDlgProgress *ui;
	public:
		QFDlgProgress(QWidget *parent = NULL);
		virtual ~QFDlgProgress();
};
   
#endif // QFDLGPROGRESS_H

