MY_MODULE_NAME = qfdlgprogress

INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/$${MY_MODULE_NAME}.h \

SOURCES += \
    $$PWD/$${MY_MODULE_NAME}.cpp \

FORMS += \
	$$PWD/$${MY_MODULE_NAME}.ui \

#RESOURCES += \
#    $$PWD/dlgexception.qrc 