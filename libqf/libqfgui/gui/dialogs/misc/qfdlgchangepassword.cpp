
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#include "ui_qfdlgchangepassword.h"
#include "qfdlgchangepassword.h"

#include <qflogcust.h>

//=================================================
//             QFDlgChangePassword
//=================================================
QFDlgChangePassword::QFDlgChangePassword(QWidget *parent) 
	: QDialog(parent)
{
	ui = new Ui::QFDlgChangePassword;
	ui->setupUi(this);
}

QFDlgChangePassword::~QFDlgChangePassword()
{
	delete ui;
}

QString QFDlgChangePassword::oldPassword()
{
	return ui->edOldPassword->text().trimmed();
}

QString QFDlgChangePassword::newPassword()
{
	return ui->edNewPassword->text().trimmed();
}

QString QFDlgChangePassword::newPasswordRetyped()
{
	return ui->edNewPasswordAgain->text().trimmed();
}
