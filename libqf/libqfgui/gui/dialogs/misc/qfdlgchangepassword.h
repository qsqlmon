
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDLGCHANGEPASSWORD_H
#define QFDLGCHANGEPASSWORD_H

#include <qfguiglobal.h>

#include <QDialog>


namespace Ui {class QFDlgChangePassword;};

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFDlgChangePassword : public QDialog
{
	Q_OBJECT;
	private:
		Ui::QFDlgChangePassword *ui;
	public:
		QString oldPassword();
		QString newPassword();
		QString newPasswordRetyped();
	public:
		QFDlgChangePassword(QWidget *parent = NULL);
		virtual ~QFDlgChangePassword();
};

#endif // DLGCHANGEPASSWORD_H

