#include "qfmsgshowagain.h"
#include "ui_qfmsgshowagaindialogwidget.h"

#include <qfapplication.h>

#include <qflogcust.h>

//===================================================
//                           QFMsgShowAgainDialogWidget
//===================================================
QFMsgShowAgainDialogWidget::QFMsgShowAgainDialogWidget(QWidget *parent)
	: QFDialogWidget(parent)
{
	ui = new Ui::QFMsgShowAgainDialogWidget;
	ui->setupUi(centralWidget());
	Qf::connectSlotsByName(centralWidget(), this);
}

QFMsgShowAgainDialogWidget::~QFMsgShowAgainDialogWidget()
{
	delete ui;
}

void QFMsgShowAgainDialogWidget::setPlainText(const QString& txt)
{
	ui->lblMessage->setText(txt);
}

//===================================================
//                           QFMsgShowAgain
//===================================================
QFMsgShowAgain::QFMsgShowAgain(const QString &msg_id, QWidget *parent)
	: QFDialogShowAgain(msg_id, parent)
{
	textWidget = new QFMsgShowAgainDialogWidget();
	setDialogWidget(textWidget);
}

QFMsgShowAgain::~QFMsgShowAgain()
{
}

void QFMsgShowAgain::setText(const QString& txt)
{
	textWidget->setPlainText(txt);
}

int QFMsgShowAgain::show(QWidget *parent, const QString& txt, const QString &_id)
{
	QString id = _id;
	if(id.isEmpty()) id = txt;
	if(qfApp()->messagesIdNotToShow().contains(id)) return NotDisplayed;
	
	QFMsgShowAgain d(id, parent);
	d.setText(txt);
	int ret = d.exec();
	return ret;
}
/*
void QFMsgShowAgain::savePersistentData()
{
	qfTrash() << QF_FUNC_NAME;
	//QFXmlConfigPersistenter ps("msgShowAgain");
	QString s;
	QStringList sl;
	StringSet &ss = messagesNotToShow();
	foreach(s, ss) sl << s;
	QFXmlConfigElement el = qfApp()->config()->createPersistentPath("msgShowAgain");
	el.setValue("msgbenlist", sl.join("{sep}"));
}

void QFMsgShowAgain::loadPersistentData()
{
	//QFXmlConfigPersistenter ps("msgShowAgain");
	QFXmlConfigElement el = qfApp()->config()->persistentPath("msgShowAgain");
	if(el) {
		QString s = el.value("msgbenlist", QVariant()).toString();
		StringSet &ss = messagesNotToShow();
		foreach(s, s.split("{sep}")) ss << s;
	}
}
*/
