#ifndef QFMSGSHOWAGAIN_H
#define QFMSGSHOWAGAIN_H

#include <qfguiglobal.h>
#include <qfdialogwidget.h>
#include <qfdialogshowagain.h>
#include <qfexception.h>

#include <QDialog>
#include <QSet>

namespace Ui {class QFMsgShowAgainDialogWidget;};

class QFMsgShowAgainDialogWidget : public QFDialogWidget
{
	Q_OBJECT
	private:
		Ui::QFMsgShowAgainDialogWidget *ui;
	public:
		void setPlainText(const QString& txt);
	public:
		QFMsgShowAgainDialogWidget(QWidget *parent = 0);
		virtual ~QFMsgShowAgainDialogWidget();
};

//! A dialog for displaying message which can be displayed only once.
//! Displayed message benlist is stored in a application config file.
class QFGUI_DECL_EXPORT QFMsgShowAgain : public QFDialogShowAgain
{
	Q_OBJECT;
	public:
		enum {NotDisplayed = QDialog::Rejected - 1};
	protected:
		QFMsgShowAgainDialogWidget *textWidget;
	public:
		//! pokud se nezada id, vezme se jako id text;
		//! @return QDialog::Accepted, QDialog::Rejected, QFMsgShowAgain::NotDisplayed
		static int show(QWidget *parent, const QString &text, const QString &id = QString());
		int exec() {return QFDialogShowAgain::exec();}
		void setText(const QString& txt);
	public:
		QFMsgShowAgain(const QString &msg_id, QWidget *parent = 0);
		virtual ~QFMsgShowAgain();
};

#endif // QFMSGSHOWAGAIN_H
