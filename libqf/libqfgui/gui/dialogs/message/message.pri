# Input

INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/qfmessage.h \
    $$PWD/qfmsgshowagain.h \

SOURCES += \
    $$PWD/qfmessage.cpp \
    $$PWD/qfmsgshowagain.cpp \

FORMS += \
   $$PWD/qfmsgshowagaindialogwidget.ui \
