#ifndef QFMESSAGE_H__INCLUDED
#define QFMESSAGE_H__INCLUDED

#include <qfguiglobal.h>

#include <QMessageBox>
#include <QInputDialog>
#include <QApplication>

class QFGUI_DECL_EXPORT QFMessage
{
	Q_DECLARE_TR_FUNCTIONS(QFMessage);
	public:
		QFMessage();
		virtual ~QFMessage();

		/// pokusi se object prevest na widget a pokud to nejde, hleda v rodicich, dokud nenajde widget
		/// a kdyz stejne nic nenajde vrati NULL.
		static QWidget* objectToWidget(QObject *o);
	public:
		static int information(QObject * parent, const QString & msg)
		{
			return QMessageBox::information(objectToWidget(parent), QMessageBox::tr("informace"), msg);
		}
		static int information(const QString & msg)
		{
			return QMessageBox::information(qApp->activeWindow(), QMessageBox::tr("informace"), msg);
		}
		static int error(QObject * parent, const QString & msg)
		{
			return QMessageBox::critical(objectToWidget(parent), QMessageBox::tr("error"), msg);
		}
		static int error(const QString & msg)
		{
			return QMessageBox::critical(NULL, QMessageBox::tr("error"), msg);
		}
		static bool askYesNo(QObject *parent, const QString & msg, bool default_ret = true)
		{
			int i_def = (default_ret)? 0: 1;
			int i = QMessageBox::question(objectToWidget(parent), tr("Question"), msg,
										tr("&Yes"), tr("&No"), QString(),
										i_def, 1);
			return i == 0;
		}
		static bool askYesNo(const QString & msg, bool default_ret = true)
		{
			return askYesNo(NULL, msg, default_ret);
		}

		static bool getText(QObject *parent, const QString & msg, QString& text)
		{
			bool ok;
			text = QInputDialog::getText(objectToWidget(parent), tr("Enter the text"), msg, QLineEdit::Normal, text, &ok);
			return ok;
		}
};

#endif // !defined(QFMESSAGE_H__INCLUDED)
