include($$PWD/dlgexception/dlgexception.pri)
include($$PWD/dlghtmlview/dlghtmlview.pri)
include($$PWD/dlgtextview/dlgtextview.pri)
include($$PWD/dlgxmlconfig/dlgxmlconfig.pri)
include($$PWD/dlgprogress/dlgprogress.pri)
include($$PWD/dlgdatatable/qfdlgdatatable.pri)

include($$PWD/message/message.pri)

include($$PWD/qfdataform/qfdataform.pri)

include($$PWD/qfdialog/qfdialog.pri)

include($$PWD/dlgopenurl/dlgopenurl.pri)

include($$PWD/misc/misc.pri)

