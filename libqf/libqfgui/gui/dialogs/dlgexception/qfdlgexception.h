#ifndef QFDLGEXCEPTION_H
#define QFDLGEXCEPTION_H

#include <qfguiglobal.h>
#include <qfexception.h>
#include <QDialog>

namespace Ui {
	class DlgException;
}

/// A dialog for FQExeption displaying
/**
  This dialog can show all details about caught QFException.
 */
class QFGUI_DECL_EXPORT QFDlgException : public QDialog
{
		Q_OBJECT
	protected:
		Ui::DlgException *ui;
		QFException exception;
	protected:
		virtual void sendReport() {}
		QPushButton* btSendReport();
	protected slots:
		void on_btSend_clicked() {sendReport();}
	public:
		/*
		int exec(const QFException &e) {
			//setWindowTitle(tr("Exception")); nefunguje
			setText(e);
			return QDialog::exec();
		}
		*/

		int exec() {return QDialog::exec();}

		static int exec(QObject *parent, const QFException &e);
		static int exec(const QFException &e);

		void setText(const QString& msg, const QString& where, const QString& stack_trace = QString(), const QString &catch_location = QString());
		void setException(const QFException &e) {exception = e; setText(e.msg(), e.where(), e.stackTrace(), e.catchLocation());} 
	public:
		QFDlgException(QWidget *parent = 0);
		virtual ~QFDlgException();
};

#endif // QFDLGEXCEPTION_H
