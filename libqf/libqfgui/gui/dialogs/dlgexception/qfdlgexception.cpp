#include <qfmessage.h>

#include "qfdlgexception.h"
#include "ui_dlgexception.h"

#include <qflogcust.h>

QFDlgException::QFDlgException(QWidget *parent) :
	QDialog(parent)
{
	ui = new Ui::DlgException;
	ui->setupUi(this);
	btSendReport()->hide();
}

QFDlgException::~QFDlgException()
{
	delete ui;
}

QPushButton* QFDlgException::btSendReport()
{
	return ui->btSend;
}

void QFDlgException::setText(const QString& msg, const QString& _where, const QString& stack_trace, const QString &catch_location)
{
	ui->txtException->setPlainText(msg);
	QString where = _where;
	if(!catch_location.isEmpty()) where += "\n\nCatch location: " + catch_location;
	ui->txtWhere->setPlainText(where);
	QFString s = stack_trace;
	QStringList sl = s.splitAndTrim('\n');
	ui->tblStack->setRowCount(sl.count());
	int i = 0;
	foreach(s, sl) {
		QStringList sl1 = s.split(';');
		for(int j=0; j<sl1.count(); j++) {
			s = sl1[j];
			if(!!s) {
				QTableWidgetItem *it;
				it = new QTableWidgetItem(s);
				ui->tblStack->setItem(i, j, it);
			}
		}
		i++;
	}
	ui->tblStack->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
	ui->tblStack->verticalHeader()->resizeSections(QHeaderView::ResizeToContents);
}

int QFDlgException::exec(QObject *parent, const QFException &e)
{
	QFDlgException d(QFMessage::objectToWidget(parent));
	d.setException(e);
	return d.exec();
}

int QFDlgException::exec(const QFException &e)
{
	QFDlgException d(qApp->activeWindow());
	d.setException(e);
	return d.exec();
}

