
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

//#include "ui_qfdlgdatatable.h"
#include "qfdlgdatatable.h"

#include <qfsqlquerytablemodel.h>
#include <qftableviewwidget.h>

#include <QKeyEvent>

#include <qflogcust.h>

//====================================================================
//                                                          QFDlgDataTable
//====================================================================
QFDlgDataTable::QFDlgDataTable(QFDlgDataTable::TableViewType type, const VisibleParts &visible_parts, QWidget *parent)
	: QFButtonDialog(parent), f_model(NULL)
{
	qfLogFuncFrame();
	//ui = new Ui::QFDlgDataTable;
	//ui->setupUi(centralWidget());

	//QVBoxLayout *ly = new QVBoxLayout(ui->frmTable);
	if(type == Sql) {
		f_tableView = new QFSqlTableViewWidget();
	}
	else {
		f_tableView = new QFTableViewWidget();
	}
	qfTrash() << "\t created table view:" << f_tableView;
	setDialogWidget(f_tableView);
	//ly->addWidget(f_tableView);
	setButtonsVisible(visible_parts & ShowButtons);
	f_tableView->setToolBarVisible(visible_parts & ShowToolBar);
	f_tableView->setStatusLineVisible(visible_parts & ShowStatusBar);
	setCaption();
}

QFDlgDataTable::QFDlgDataTable(const QFTableViewWidget::TableViewFactory &table_view_factory, const VisibleParts &visible_parts, QWidget *parent)
	: QFButtonDialog(parent), f_model(NULL)
{
	qfLogFuncFrame();
	f_tableView = new QFTableViewWidget(NULL, table_view_factory);
	qfTrash() << "\t created table view:" << f_tableView;
	setDialogWidget(f_tableView);
	setButtonsVisible(visible_parts & ShowButtons);
	f_tableView->setToolBarVisible(visible_parts & ShowToolBar);
	f_tableView->setStatusLineVisible(visible_parts & ShowStatusBar);
	setCaption();
}

QFDlgDataTable::~QFDlgDataTable()
{
	//delete ui;
}

QFTableModel* QFDlgDataTable::model()
{
	if(!f_model) {
		f_model = new QFTableModel(this);
		qfTrash() << QF_FUNC_NAME << "model created:" << f_model;
		tableView()->setModel(f_model);
	}
	return f_model;
}

void QFDlgDataTable::setModel(QFTableModel *m)
{
	qfTrash() << QF_FUNC_NAME;
	f_model = m;
	tableView()->setModel(f_model);
}

QFBasicTable* QFDlgDataTable::table()
{
	return model()->table();
}
/*
QFSqlQueryTable* QFDlgDataTable::sqlTable() throw(QFException)
{
	qfTrash() << QF_FUNC_NAME << table();
	QFSqlQueryTable *ret = qobject_cast<QFSqlQueryTable*>(table());
	if(!ret) QF_EXCEPTION("Table is not a kind of QFSqlQueryTable");
	return ret;
}
		*/
QFTableView* QFDlgDataTable::view()
{
	return tableView()->tableView();
}

void QFDlgDataTable::keyPressEvent(QKeyEvent *e)
{
	qfTrash() << QF_FUNC_NAME;
	bool modified = (e->modifiers() == Qt::ControlModifier)
			|| (e->modifiers() == Qt::AltModifier)
			|| (e->modifiers() == Qt::MetaModifier);
	if(!modified && e->key() == Qt::Key_Escape) {reject(); return;}
	QDialog::keyPressEvent(e);
}

int QFDlgDataTable::exec()
{
	loadPersistentData();
	view()->setFocus();
	int ret = QDialog::exec();
	savePersistentData();
	setExecResult(ret);
	return ret;
//tableView()->tableView()->resizeColumnsToContents();
}
/*
void  QFDlgDataTable::savePersistentData()
{
	if(!xmlConfigPersistentId().isEmpty()) {
		QFXmlConfigElement el = makePersistentPath();
		// save window size
		el.setValue("window/pos", pos());
		el.setValue("window/size", size());
	}
}

void  QFDlgDataTable::loadPersistentData()
{
	if(!xmlConfigPersistentId().isEmpty()) {
		QFXmlConfigElement el = persistentPath();
		// load window size
		QVariant v = el.value("window/pos", QVariant());
		if(v.type() == QVariant::Point) {
			move(v.toPoint());
		}
		v = el.value("window/size", QVariant());
		if(v.type() == QVariant::Size) {
			resize(v.toSize());
		}
	}
}
*/

void QFDlgDataTable::setXmlConfigPersistentId(const QString &id, bool load_persistent_data)
{
	qfTrash() << QF_FUNC_NAME << id << load_persistent_data;
	if(id.isEmpty()) {
		view()->setXmlConfigPersistentId(QString());
	}
	else {
		view()->setXmlConfigPersistentId(id + "/tableView", load_persistent_data);
	}
	QFButtonDialog::setXmlConfigPersistentId(id, load_persistent_data);
}

void QFDlgDataTable::setExternalRowEditorFactory(QFDataFormDialogWidgetFactory *fact)
{
	tableView()->tableView()->setExternalRowEditorFactory(fact);
}

void QFDlgDataTable::setCaption(const QString &cap)
{
	//QF_ASSERT(tableView())
	if(!tableView()) return;
	qfTrash() << QF_FUNC_NAME << "tableView:" << tableView();
	tableView()->setCaptionText(cap);
	tableView()->setCaptionCloseButtonVisible(false);
}

QFBasicTable::Row QFDlgDataTable::chosenRow()
{
	QFBasicTable::Row r;
	if(execResult() == QFDialog::Accepted) {
		r = view()->selectedRow();
	}
	return r;
}

//====================================================================
//                                                          QFDlgBasicDataTable
//====================================================================
/*
QFTableViewWidget* QFDlgBasicDataTable::tableView() const
{
	return f_tableView;
}
*/
QFTableModel* QFDlgBasicDataTable::model()
{
	if(!f_model) {
		f_model = new QFTableModel(this);
		qfTrash() << QF_FUNC_NAME << "model created:" << f_model;
		tableView()->setModel(f_model);
	}
	return f_model;
}

//====================================================================
//                                                          QFDlgSqlDataTable
//====================================================================
QFSqlTableViewWidget* QFDlgSqlDataTable::tableView() const
{
	return qobject_cast<QFSqlTableViewWidget*>(f_tableView);
}

QFSqlQueryTableModel* QFDlgSqlDataTable::model()
{
	if(!f_model) {
		f_model = new QFSqlQueryTableModel(this);
		qfTrash() << QF_FUNC_NAME << "model created:" << f_model;
		tableView()->setModel(f_model);
	}
	return qobject_cast<QFSqlQueryTableModel*>(f_model);
}

QFSqlTableView* QFDlgSqlDataTable::sqlView()
{
	return qobject_cast<QFSqlTableView*>(QFDlgDataTable::view());
}
//====================================================================
//                                                          QFDlgDbXmlDataTable
//====================================================================
#if 0
QFDbXmlTableViewWidget* QFDlgDbXmlDataTable::tableView() const
{
	return qobject_cast<QFDbXmlTableViewWidget*>(f_tableView);
}
#endif
