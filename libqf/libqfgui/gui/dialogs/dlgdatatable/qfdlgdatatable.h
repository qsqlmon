
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDLGDATATABLE_H
#define QFDLGDATATABLE_H

#include <qfguiglobal.h>
#include <qfbuttondialog.h>
#include <qfsqlquerytable.h>
#include <qfsqltableviewwidget.h>
#include <qfsqlquerytablemodel.h>
#include <qfsqltableview.h>

//namespace Ui {class QFDlgDataTable;};
//class QFTableModel;
//class QFBasicTable;
//class QFSqlQueryTable;
class QFDataFormDialog;
class QFDataFormDialogWidgetFactory;
//class QFTableView;
//class QFTableViewWidget;
//class QFSqlTableView;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFDlgDataTable : public QFButtonDialog
{
	Q_OBJECT;
	/// result of last exec() call.
	QF_FIELD_R(int, e, setE, xecResult);
	public:
		enum TableViewType {Basic, Sql};
		enum VisiblePart {ShowToolBar = 1, ShowStatusBar = 2, ShowButtons = 4 };
		Q_DECLARE_FLAGS(VisibleParts, VisiblePart);
	private:
		//Ui::QFDlgDataTable *ui;
	protected:
		QFTableModel *f_model;
	protected:
		QFTableViewWidget *f_tableView;

	protected:
		virtual void keyPressEvent(QKeyEvent *e);
	public:
		/// pokud neni prirazen predem vytvori novou instanci QFSqlTableModel
		QFTableModel* model();
		void setModel(QFTableModel *m);
		//QFSqlQueryTable* sqlTable() throw(QFException);
		QFBasicTable* table();
		QFTableView* view();
		virtual QFTableViewWidget* tableView() const {return f_tableView;}

		void setXmlConfigPersistentId(const QString &id, bool load_persistent_data = true);

		void setExternalRowEditorFactory(QFDataFormDialogWidgetFactory *fact);
	public slots:
		int exec();
		void reload() throw(QFSqlException)
		{
			model()->reload();
		}
		void setCaption(const QString &cap = QString());
		//virtual void insertRow();
		//virtual void editRow();
	public:
		/// selectedRow() pokud je execResult==Accepted nebo null row jinak
		QFBasicTable::Row chosenRow();
	public:
		//! @param type typ TableViewWidget, ktery se vytvori
		QFDlgDataTable(TableViewType type, const VisibleParts &visible_parts, QWidget *parent = NULL);
		QFDlgDataTable(const QFTableViewWidget::TableViewFactory &table_view_factory, const VisibleParts &visible_parts, QWidget *parent = NULL);
		virtual ~QFDlgDataTable();
};
Q_DECLARE_OPERATORS_FOR_FLAGS(QFDlgDataTable::VisibleParts);

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFDlgSqlDataTable : public QFDlgDataTable
{
	Q_OBJECT;
	protected:
	public:
		/// pokud neni prirazen predem vytvori novou instanci QFSqlQueryTableModel
		QFSqlQueryTableModel* model();
		void reload(const QString query_str = QString()) throw(QFSqlException)
		{
			model()->reload(query_str);
		}
		QFSqlTableView* sqlView();
		virtual QFSqlTableViewWidget* tableView() const;
	public:
		QFDlgSqlDataTable(QWidget *parent = NULL, const VisibleParts &visibility_flags = (ShowToolBar | ShowButtons))
			: QFDlgDataTable(Sql, visibility_flags, parent) {}
		QFDlgSqlDataTable(const QFTableViewWidget::TableViewFactory &table_view_factory, QWidget *parent = NULL, const VisibleParts &visibility_flags = (ShowToolBar | ShowButtons))
			: QFDlgDataTable(table_view_factory, visibility_flags, parent) {}
		virtual ~QFDlgSqlDataTable() {}
};

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFDlgBasicDataTable : public QFDlgDataTable
{
	Q_OBJECT;
	public:
		/// pokud neni prirazen predem vytvori novou instanci QFSqlQueryTableModel
		QFTableModel* model();
	public:
		QFDlgBasicDataTable(QWidget *parent = NULL, const VisibleParts &visibility_flags = (ShowToolBar | ShowButtons))
			: QFDlgDataTable(Basic, visibility_flags, parent) {}
		QFDlgBasicDataTable(const QFTableViewWidget::TableViewFactory &table_view_factory, QWidget *parent = NULL, const VisibleParts &visibility_flags = (ShowToolBar | ShowButtons))
			: QFDlgDataTable(table_view_factory, visibility_flags, parent) {}
		virtual ~QFDlgBasicDataTable() {}
};
/*
//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFDlgDbXmlDataTable : public QFDlgDataTable
{
	Q_OBJECT;
	protected:
		virtual QFDbXmlTableViewWidget* tableView() const;
	public:
		QFDlgDbXmlDataTable(QWidget *parent = NULL);
		virtual ~QFDlgDbXmlDataTable() {}
};
*/
#endif // QFDLGDATATABLE_H

