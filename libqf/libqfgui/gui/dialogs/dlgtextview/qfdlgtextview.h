#ifndef QFDLGTEXTVIEW_H
#define QFDLGTEXTVIEW_H

#include <qfguiglobal.h>
#include <qfexception.h>
#include <qfdialog.h>

//namespace Ui {
//	class QFDlgTextView;
//}

class QFTextViewWidget;
class QTextEdit;

/// A dialog for FQExeption displaying
/**
  This dialog can show all details about caught QFException.
 */
class QFGUI_DECL_EXPORT QFDlgTextView : public QFDialog
{
	Q_OBJECT;
	protected:
		//Ui::QFDlgTextView *ui;
		QFTextViewWidget *f_editor;
	public:
		QString text();
		void setText(const QString& content, const QString &suggested_file_name = QString(), const QString &codec_name = QString());
		void setFile(const QString& file_name, const QString &codec_name = QString());
		//void setSuggestedFileName();
		void setUrl(QFile &url, const QString &codec_name = QString());
		
		int exec() {return QDialog::exec();}
		int exec(const QString &content, const QString &suggested_file_name = QString(), const QString &persistent_data_id = QString(), const QString &codec_name = QString());

		QTextEdit* editor();
		QFTextViewWidget* textViewWidget() {return f_editor;}

		static int exec(QWidget *parent, const QString &content, const QString &suggested_file_name = QString(), const QString &persistent_data_id = QString(), const QString &codec_name = QString());
		static int exec(QWidget *parent, QFile &url, const QString &persistent_data_id = QString(), const QString &codec_name = QString());
	public:
		QFDlgTextView(QWidget *parent = 0);
		virtual ~QFDlgTextView();
};

#endif // QFDLGTEXTVIEW_H
