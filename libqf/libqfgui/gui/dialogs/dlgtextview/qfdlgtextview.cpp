//#include <QToolButton>

#include <qftextviewwidget.h>

#include "qfdlgexception.h"
#include "qfdlgtextview.h"

//#include <QTimer>
#include <QTextEdit>

#include <qflogcust.h>

QFDlgTextView::QFDlgTextView(QWidget *parent) :
	QFDialog(parent)
{
	//ui = new Ui::QFDlgTextView;
	//ui->setupUi(centralWidget());
	f_editor = new QFTextViewWidget();
	setDialogWidget(f_editor);
	//QTimer::singleShot(0, f_editor, SLOT(setFocus()));
	connect(f_editor, SIGNAL(acceptRequest()), this, SLOT(accept()));
	editor()->setFocus();
}

QFDlgTextView::~QFDlgTextView()
{
	//delete ui;
}

void QFDlgTextView::setFile(const QString& file_name, const QString &codec_name)
{
	f_editor->setFile(file_name, codec_name);
}
		
void QFDlgTextView::setUrl(QFile &url, const QString &codec_name)
{
	f_editor->setUrl(url, codec_name);
}

void QFDlgTextView::setText(const QString& content, const QString &suggested_file_name, const QString &codec_name)
{
	f_editor->setText(content, suggested_file_name, codec_name);
}

QString QFDlgTextView::text()
{
	return f_editor->text();
}
/*
void  QFDlgTextView::savePersistentData()
{
	if(!xmlConfigPersistentId().isEmpty()) {
		QFXmlConfigElement el = makePersistentPath();
		// save window size
		QString s;
		s = variantToString(pos());
		el.setValue("window/pos", s);
		s = variantToString(size());
		el.setValue("window/size", s);
	}
}

void  QFDlgTextView::loadPersistentData()
{
	if(!xmlConfigPersistentId().isEmpty()) {
		QFXmlConfigElement el = persistentPath();
		// load window size
		QString s = el.value("window/pos", QVariant()).toString();
		if(!s.isEmpty()) {
			QVariant v = stringToVariant(s);
			move(v.toPoint());
		}
		s = el.value("window/size", QVariant()).toString();
		if(!s.isEmpty()) {
			QVariant v = stringToVariant(s);
			resize(v.toSize());
		}
	}
}
*/

int QFDlgTextView::exec(const QString &content, const QString &suggested_file_name, const QString &persistent_data_id, const QString &codec_name)
{
	setXmlConfigPersistentId(persistent_data_id);
	//loadPersistentData();
	setText(content, suggested_file_name, codec_name);
	//qfInfo() << "QFDlgTextView::exec:" << text();
	int ret = exec();
	//qfInfo() << "QFDlgTextView::exec ret:" << text();
	//savePersistentData();
	return ret;
}

int QFDlgTextView::exec(QWidget *parent, const QString &content, const QString &suggested_file_name, const QString &persistent_data_id, const QString &codec_name)
{
	QFDlgTextView d(parent);
	return d.exec(content, suggested_file_name, persistent_data_id, codec_name);
}

int QFDlgTextView::exec(QWidget *parent, QFile &url, const QString &persistent_data_id, const QString &codec_name)
{
	QFDlgTextView d(parent);
	d.setXmlConfigPersistentId(persistent_data_id);
	d.loadPersistentData();
	d.setUrl(url, codec_name);
	int ret = d.exec();
	d.savePersistentData();
	return ret;
}

QTextEdit* QFDlgTextView::editor()
{
	return f_editor->editor();
}

