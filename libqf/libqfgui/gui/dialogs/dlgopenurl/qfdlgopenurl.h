
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDLGOPENURL_H
#define QFDLGOPENURL_H

#include <qfguiglobal.h>

#include <qfdialog.h>


namespace Ui {class QFDlgOpenUrl;}; 

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFDlgOpenUrl : public QFDialog
{
	Q_OBJECT;
	public:
		enum {SaveUrl = 100, OpenUrl};
	private:
		Ui::QFDlgOpenUrl *ui;
	protected:
		static QString recentSavePath;
	protected slots:
		void on_btCancel_clicked();
		void on_btSave_clicked();
		void on_btOpen_clicked();
	public:
		void setUrl(const QUrl &url);
		QUrl url() const;

		virtual void loadPersistentData();
		virtual void savePersistentData();

		static void openUrl(const QUrl &url);
	public:
		QFDlgOpenUrl(QWidget * parent = 0, Qt::WFlags f = 0);
		virtual ~QFDlgOpenUrl();
};
   
#endif // QFDLGOPENURL_H

