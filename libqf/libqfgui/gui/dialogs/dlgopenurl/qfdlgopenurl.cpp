
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "ui_qfdlgopenurl.h"
#include "qfdlgopenurl.h"

#include <qflogcust.h>
#include <qffileutils.h>
#include <qfmessage.h>

#include <QUrl>
#include <QFileDialog>
#include <QDesktopServices>

QString QFDlgOpenUrl::recentSavePath;

QFDlgOpenUrl::QFDlgOpenUrl(QWidget * parent, Qt::WFlags f)
	: QFDialog(parent, f)
{
	ui = new Ui::QFDlgOpenUrl;
	ui->setupUi(centralWidget());
	connect(ui->btSave, SIGNAL(clicked()), this, SLOT(on_btSave_clicked()));
	connect(ui->btOpen, SIGNAL(clicked()), this, SLOT(on_btOpen_clicked()));
	connect(ui->btCancel, SIGNAL(clicked()), this, SLOT(on_btCancel_clicked()));
	
	ui->btSave->setEnabled(false);
}

QFDlgOpenUrl::~QFDlgOpenUrl()
{
	savePersistentData();
	delete ui;
}

void QFDlgOpenUrl::on_btCancel_clicked()
{
	reject();
}

void QFDlgOpenUrl::on_btSave_clicked()
{
	done(SaveUrl);
}

void QFDlgOpenUrl::on_btOpen_clicked()
{
	done(OpenUrl);
}

QUrl QFDlgOpenUrl::url() const
{
	QUrl ret = QUrl(ui->edUrl->text());
	return ret;
}

void QFDlgOpenUrl::setUrl(const QUrl & url)
{
	ui->edUrl->setText(url.toString());
	ui->btSave->setEnabled(url.scheme() == "file");
}

void QFDlgOpenUrl::openUrl(const QUrl & url)
{
	qfLogFuncFrame();
	QFDlgOpenUrl dlg(QApplication::activeWindow());
	dlg.setXmlConfigPersistentId("libqf/QFDlgOpenUrl");
	dlg.setUrl(url);
	int ret = dlg.exec();
	if(ret == SaveUrl) {
		QString fn = QFFileUtils::joinPath(recentSavePath, QFFileUtils::file(url.path()));
		QString ext = "." + QFFileUtils::extension(fn);
		fn = QFileDialog::getSaveFileName(QApplication::activeWindow(), tr("Save File"), fn, "(*" + ext + ")", 0, QFileDialog::DontUseNativeDialog);
		if(!fn.isEmpty()) {
			if(!fn.toLower().endsWith(ext)) fn += ext;
			qfTrash() << "\t save as:" << fn;
			recentSavePath = QFFileUtils::path(fn);
			if(QFile::exists(fn)) QFile::remove(fn);
			if(!QFile::copy(url.path(), fn)) {
				QFMessage::error(tr("Chyba pri ukladani souboru ''%1'").arg(fn));
			}
		}
	}
	else if(ret == OpenUrl) {
		QDesktopServices::openUrl(url);
	}
}

void  QFDlgOpenUrl::savePersistentData()
{
	//QFDialog::savePersistentData();
	if(!xmlConfigPersistentId().isEmpty()) {
		qfTrash() << QF_FUNC_NAME << xmlConfigPersistentId();
		//QFXmlConfigElement el = createPersistentPath();
		setPersistentValue("recentSavePath", recentSavePath);
	}
	//qfWarning() << __LINE__;
}

void  QFDlgOpenUrl::loadPersistentData() 
{
	//QFDialog::loadPersistentData();
	if(!xmlConfigPersistentId().isEmpty()) {
		qfTrash() << QF_FUNC_NAME << xmlConfigPersistentId();
		//QFXmlConfigElement el = persistentPath();
		QVariant v = persistentValue("recentSavePath", QFFileUtils::homeDir());
		recentSavePath = v.toString();
	}
}



