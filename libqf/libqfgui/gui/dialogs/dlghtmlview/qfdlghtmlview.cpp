#include <QToolButton>

#include <qfhtmlviewwidget.h>

#include "qfdlgexception.h"
#include "qfdlghtmlview.h"
#include "ui_qfdlghtmlview.h"

QFDlgHtmlView::QFDlgHtmlView(QWidget *parent) :
	QFDialog(parent)
{
	ui = new Ui::QFDlgHtmlView;
	ui->setupUi(centralWidget());
}

QFDlgHtmlView::~QFDlgHtmlView()
{
	delete ui;
}

void QFDlgHtmlView::setHtmlText(const QString& content, const QString &suggested_file_name)
{
	ui->browser->setHtmlText(content, suggested_file_name);
}

void QFDlgHtmlView::setUrl(QFile& url)
{
	ui->browser->setUrl(url);
}

int QFDlgHtmlView::exec(QWidget *parent, const QString &content, const QString &suggested_file_name, const QString &persistent_data_id)
{
	QFDlgHtmlView d(parent);
	d.setXmlConfigPersistentId(persistent_data_id);
	//d.loadPersistentData();
	d.setHtmlText(content, suggested_file_name);
	int ret = d.exec();
	d.savePersistentData();
	return ret;
}

int QFDlgHtmlView::exec(QWidget *parent, QFile &url, const QString &persistent_data_id)
{
	QFDlgHtmlView d(parent);
	d.setXmlConfigPersistentId(persistent_data_id);
	//d.loadPersistentData();
	d.setUrl(url);
	int ret = d.exec();
	d.savePersistentData();
	return ret;
}
/*
void  QFDlgHtmlView::savePersistentData()
{
	if(!xmlConfigPersistentId().isEmpty()) {
		QFXmlConfigElement el = makePersistentPath();
		// save window size
		QString s;
		s = variantToString(pos());
		el.setValue("window/pos", s);
		s = variantToString(size());
		el.setValue("window/size", s);
	}
}

void  QFDlgHtmlView::loadPersistentData()
{
	if(!xmlConfigPersistentId().isEmpty()) {
		QFXmlConfigElement el = persistentPath();
		// load window size
		QString s = el.value("window/pos", QVariant()).toString();
		if(!s.isEmpty()) {
			QVariant v = stringToVariant(s);
			move(v.toPoint());
		}
		s = el.value("window/size", QVariant()).toString();
		if(!s.isEmpty()) {
			QVariant v = stringToVariant(s);
			resize(v.toSize());
		}
	}
}
*/
