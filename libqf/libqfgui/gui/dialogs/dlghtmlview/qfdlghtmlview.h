#ifndef QFDLGHTMLVIEW_H
#define QFDLGHTMLVIEW_H

#include <qfguiglobal.h>
#include <qfexception.h>
#include <qfdialog.h>

class QFile;

namespace Ui {
	class QFDlgHtmlView;
}

class QFGUI_DECL_EXPORT QFDlgHtmlView : public QFDialog
{
	Q_OBJECT
	protected:
		Ui::QFDlgHtmlView *ui;
	public:
		QFDlgHtmlView(QWidget *parent = 0);
		virtual ~QFDlgHtmlView();

		void setHtmlText(const QString& content, const QString &suggested_file_name = QString());
		void setUrl(QFile &url);

		int exec() {return QDialog::exec();}
		
		static int exec(QWidget *parent, const QString &content, const QString &suggested_file_name = QString(), const QString &persistent_data_id = QString());
		static int exec(QWidget *parent, QFile &url, const QString &persistent_data_id = QString());
};

#endif // QFDLGHTMLVIEW_H
