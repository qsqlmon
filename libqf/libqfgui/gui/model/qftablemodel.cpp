#include <QtAlgorithms>

#include "qftablemodel.h"
#include "qftablemodeldelegate.h"

#include <qfsql.h>
#include <qfassert.h>
#include <qftableview.h>
#include <qfdatetime.h>
#include <qfdlgdatatable.h>

#include <qflogcust.h>

//=========================================
//                          QFTableModel::ColumnDefinition
//=========================================
const QFTableModel::ColumnDefinition & QFTableModel::ColumnDefinition::sharedNull()
{
	static ColumnDefinition n(1);
	return n;
}

QFDlgDataTable* QFTableModel::ColumnDefinition::chooser() const
{
	return d->chooser;
}

QFTableModel::ColumnDefinition& QFTableModel::ColumnDefinition::setChooser(QFDlgDataTable *_chooser)
{
	d->chooser = _chooser; return *this;
}
/*
ColumnDefinition & QFTableModel::ColumnDefinition::setFormat(const QString & s)
{
	d->format = s;
	QRegExp rx("^\\((\\w*)\\)");
	//QRegExp rx = rx_retype;
	if(rx.indexIn(s == 0)) {
		QString s = rx.capturedTexts().value(1);
		d->castType = QVariant::nameToType(s.toAscii().constData());
	}
	return *this;
}
*/
//=========================================
//                          QFTableModel
//=========================================
QFTableModel::QFTableModel(QObject *parent)
	: QAbstractTableModel(parent)
{
	d = &_d;
	setElideDisplayedTextAt(100);
	setNullReportedAsString(true);
	//f_currentlyPaintedRow = f_currentlyPaintedColumn = -1;
	connect(this, SIGNAL(rowPosted(int)), this, SIGNAL(rowsPostedOrRemoved()));
	connect(this, SIGNAL(rowsRemoved(const QModelIndex &, int, int)), this, SIGNAL(rowsPostedOrRemoved()));
}

QFTableModel::~QFTableModel()
{
	SAFE_DELETE(d->table);
}

QFBasicTable* QFTableModel::table() const
{
	if(!d->table) {
		QFBasicTable *t = new QFBasicTable();
		const_cast<QFTableModel*>(this)->setTable(t);
		//const_cast<QFTableModel*>(this)->setTableCreatedByModel(t);
	}
	return d->table;
}

void QFTableModel::setTable(QFBasicTable *new_t)
{
	qfTrash() << QF_FUNC_NAME << new_t;
	//if(new_t != d->tableCreatedByModel) SAFE_DELETE(d->tableCreatedByModel);
	SAFE_DELETE(d->table);
	if(new_t) {
		//connect(new_t, SIGNAL(reloaded()), this, SLOT(reset()));
		//qfTrash() << "\tisInnsertRowsAllowed:" << new_t->isInsertRowsAllowed();
	}
	d->table = new_t;
	reset();
	//emit allDataChanged();
}

int QFTableModel::rowCount(const QModelIndex & parent) const
{
	Q_UNUSED(parent);
	if(d->table) return d->table->rowCount();
	return 0;
}

int QFTableModel::columnCount(const QModelIndex & parent) const
{
	Q_UNUSED(parent);
	return columns().size();
}

bool QFTableModel::isValid(int row, int col, int role) const  //throw(QFException)
{
	Q_UNUSED(role);
	const QFBasicTable *t = table();
	return isValidCol(col) && t->isValidRow(row);
}

bool QFTableModel::isValidCol(int col) const //throw(QFException)
{
	bool ret = (col >= 0 && col < columnCount());
	//if(!ret && throw_exc) QF_EXCEPTION(QString("col: %1 is not a valid column. ColCount: %2").arg(col).arg(columnCount()));
	return ret;
}

int QFTableModel::insertRow(int before_row, const QModelIndex & parent) throw(QFException)
{
	Q_UNUSED(parent);
	qfLogFuncFrame() << "before_row:" << before_row << "row cnt:" << rowCount();
	QFBasicTable *t = table();
	int r = (t->isValidRow(before_row))? before_row: rowCount();
	beginInsertRows(QModelIndex(), r, r);
	t->insertRow(r);
	endInsertRows();
	qfTrash() << "\t row cnt:" << rowCount();
	return r;
}

void QFTableModel::insertBasicRow(int before_row) throw(QFException)
{
	//qfTrash() << QF_FUNC_NAME;
	QFBasicTable *t = table();
	int r = (t->isValidRow(before_row))? before_row: rowCount();
	beginInsertRows(QModelIndex(), r, r);
	t->insertRowNoFillDefaultAndAutoValues(r);
	endInsertRows();
}

void QFTableModel::removeBasicRow(int r) throw(QFException)
{
	//qfTrash() << QF_FUNC_NAME;
	QFBasicTable *t = table();
	if(!t->isValidRow(r)) return;
	beginRemoveRows(QModelIndex(), r, r);
	t->removeRowNoDrop(r);
	endRemoveRows();
}

bool QFTableModel::removeRows(int row, int count, const QModelIndex & parent)  throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	Q_UNUSED(parent);
	QFBasicTable *t = table();
	beginRemoveRows(QModelIndex(), row, row + count - 1);
	/// zajimavy je, ze kdyz dam beginInsertRows() az za tuto smycku, zustanou mi v tabulce prazdne nedostupne radky
	/// nadruhou stranu je pruser, ze beginRemoveRows() vola currentChanged() v pripojenych TableViews.
	for(int i=0; i<count; i++) {
		if(!t->isValidRow(row)) {
			return false;
		}
		//qfWarning() << "\tremoving row" << row << "count:" << i;
		if(!t->removeRow(row)) return false;
	}
	endRemoveRows();
	return true;
}

void QFTableModel::reload() throw(QFException)
{
	qfTrash() << QF_FUNC_NAME << this;
	table()->reload();
	reset();
}

bool QFTableModel::reloadRow(int ri) throw(QFException)
{
	qfLogFuncFrame() << "row:" << ri << "row count:" << rowCount();
	QFBasicTable *t = table();
	QFBasicTable::Row &r = t->rowRef(ri);
	int i = t->reloadRow(r);
	qfTrash() << "\trows reloaded:" << i;
	if(i == 0) {
		/// radek je v tabulce, ale uz neni v databazi, nekdo jinej ho mezi tim vymazal
		t->QFBasicTable::removeRowNoDrop(ri);
		//qfInfo() << "\t beginRemoveRows:" << ri;
		/// kdyz volam beginRemoveRows pred t->QFBasicTable::removeRow(ri) vola mi to currentChanged() a to mi zavola znovu reloadRow()
		beginRemoveRows(QModelIndex(), ri, ri);
		//qfInfo() << "\t endRemoveRows:" << ri;
		endRemoveRows();
	}
	qfTrash() << "\t row count:" << rowCount();
	return i;
}

bool QFTableModel::postRow(int ri) throw(QFException)
{
	qfLogFuncFrame() << "row:" << ri;
	QFBasicTable *t = table();
	bool ret = t->postRow(ri);
	if(ret) emit rowPosted(ri);
	return ret;
}

/*
QFTableView* QFTableModel::currentView() const
{
	if(!f_currentViewAddr) return NULL;
	QAbstractItemView *w1 = reinterpret_cast<QAbstractItemView*>(f_currentViewAddr);
	QFTableView *w = qobject_cast<QFTableView*>(w1);
	return w;
}
*/
QVariant QFTableModel::data(const QModelIndex &ix, int role) const
{
	QVariant ret;
	if(!ix.isValid()) return ret;
	if(role == Qf::RawValueRole) {
		ret = value(ix, !Qf::ThrowExc);
	}
	if(role == Qt::DisplayRole) {
		//qfTrash() << QF_FUNC_NAME;
		//qfTrash() << "\trow:" << ix.row() << "col:" << ix.column() << "role:" << role;
		if(data(ix, Qf::ValueIsNullRole).toBool()) {
			if(isNullReportedAsString()) return QFSql::nullString();
			return QVariant();
		}
		ret = data(ix, Qf::RawValueRole);
		QVariant::Type type = headerData(ix.column(), Qt::Horizontal, Qf::FieldTypeRole).type();
		if(type == QVariant::ByteArray) {
			const static QString blob_string = "{blob %1%2}";
			int size = ret.toByteArray().size();
			if(size < 1024) ret = blob_string.arg(size).arg(" B");
			else ret = blob_string.arg(size/1024).arg("kB");
		}
		else if(type == QVariant::Bool) {
			/// zobrazuje se jako check
			ret = QString();
		}
		else if(type == QVariant::String) {
			int elide_at = elideDisplayedTextAt();
			if(elide_at > 0) {
				//qfInfo() << "elide_at:" << elide_at;
				QString s = ret.toString();
				if(s.length() > elide_at) {
					s = s.mid(0, elide_at);
					s += " ...";
				}
				ret = s;
			}
		}
		/*
		else if(type == QVariant::Double) {
			QString s = QLocale().toString(ret.toDouble());
			QChar sep = QLocale().groupSeparator();
			if(!sep.isNull() && sep != ' ') {
				/// vymen carky za mezery
				s.replace(',', ' ');
			}
			ret = s;
		}
		*/
		const ColumnDefinition &cd = column(ix.column());
		QString format = cd.format();
		/*
		if(format.startsWith("(double)", Qt::CaseInsensitive)) {
			ret = QFSql::retypeVariant(ret, QVariant::Double);
			format = format.mid(8);
			type = QVariant::Double;
		}
		else if(format.startsWith("(bool)", Qt::CaseInsensitive)) {
			ret = QFSql::retypeVariant(ret, QVariant::Bool);
			format = format.mid(6);
			type = QVariant::Bool;
		}
		*/
		if(format.isEmpty()) {
			if(type == QVariant::Date) {
				format = Qf::defaultDateFormat();
			}
			else if(type == QVariant::Time) {
				format = Qf::defaultTimeFormat();
				//qfInfo() << "format" << format;
			}
			else if(type == QVariant::DateTime) {
				format = Qf::defaultDateTimeFormat();
				//qfInfo() << "format" << format;
			}
			else if(type == QVariant::Int) {
				format = intFormat();
			}
			else if(type == QVariant::Double) {
				format = doubleFormat();
			}
		}
		if(!format.isEmpty()) {
			if(type == QVariant::Time) {
				QTime t = ret.toTime();
				ret = t.toString(format);
			}
			else if(type == QVariant::Date) {
				QDate d = ret.toDate();
				//qfInfo() << cd.fieldName() << "format:" << format;
				ret = d.toString(format);
			}
			else if(type == QVariant::DateTime) {
				QDateTime dt = ret.toDateTime();
				ret = dt.toString(format);
			}
			else if(type == QVariant::Int) {
				ret = QFString::number(ret.toInt(), format);
			}
			else if(type == QVariant::Double) {
				ret = QFString::number(ret.toDouble(), format);
			}
		}
	}
	if(role == Qt::EditRole) {
		ret = data(ix, Qf::RawValueRole);
		#if 0
		QVariant::Type type = headerData(ix.column(), Qt::Horizontal, Qf::FieldTypeRole).type();
		const ColumnDefinition &cd = column(ix.column());
		QString format = cd.format();
		if(!format.isEmpty()) {
			if(type == QVariant::Time) {
				QTime t = ret.toTime();
				ret = t.toString(format);
			}
			else if(type == QVariant::Date) {
				QDate d = ret.toDate();
				//qfInfo() << cd.fieldName() << "format:" << format;
				ret = d.toString(format);
			}
			else if(type == QVariant::DateTime) {
				QDateTime dt = ret.toDateTime();
				ret = dt.toString(format);
			}
			else if(type == QVariant::Int) {
				ret = QFString::number(ret.toInt(), format);
			}
			else if(type == QVariant::Double) {
				ret = QFString::number(ret.toDouble(), format);
			}
		}
		#endif
	}
	else if (role == Qf::ValueIsNullRole) {
		ret = data(ix, Qf::RawValueRole);
		return !ret.isValid();
	}
	else if (role == Qt::TextAlignmentRole) {
		const ColumnDefinition &cd = column(ix.column());
		Qt::Alignment al = cd.alignment();
		if(!!al) ret = (int)al;
		else {
			if(!data(ix, Qf::ValueIsNullRole).toBool()) {
				ret = data(ix, Qf::RawValueRole);
				if(ret.type() > QVariant::Bool && ret.type() <= QVariant::Double) ret = Qt::AlignRight;
				else ret = Qt::AlignLeft;
			}
		}
	}
	else if (role == Qt::TextColorRole) {
		//QFFlags<Qt::ItemFlag>f(flags(ix));
		//if(!f.contains(Qt::ItemIsEditable)) return QColor(Qt::red);
		QVariant::Type type = headerData(ix.column(), Qt::Horizontal, Qf::FieldTypeRole).type();
		if(type == QVariant::ByteArray) return QColor(Qt::blue);
		if(data(ix, Qf::ValueIsNullRole).toBool()) {
			//qfInfo() << ret.toString();
			//qfInfo() << "is null:" << ret.isNull();
			//qfInfo() << "==" << QFSql::nullString() << ":" << (ret.toString() == QFSql::nullString());
			return QColor(Qt::blue);
		}
		ret = QVariant();
	}
	else if (role == Qt::BackgroundColorRole) {
		QFFlags<Qt::ItemFlag>f(flags(ix));
		if(!f.contains(Qt::ItemIsEditable)) return QColor("#eeeeff");
	}
	else if (role == Qt::CheckStateRole) {
		QVariant::Type type = headerData(ix.column(), Qt::Horizontal, Qf::FieldTypeRole).type();
		if(type == QVariant::Bool) {
			//qfInfo() << "BOOL";
			return (data(ix, Qt::EditRole).toBool()? Qt::Checked: Qt::Unchecked);
			//static QString true_str = "x";
			//return (data(ix, Qt::EditRole).toBool()? true_str: QString());
		}
	}
	else if (role == Qt::ToolTipRole) {
		//QString s = data(ix, Qf::RawValueRole).toString();
		//ret = s;
		ret = data(ix, Qt::DisplayRole);
	}
	return ret;
}

//! @reimp
Qt::ItemFlags QFTableModel::flags(const QModelIndex &ix) const
{
	QFFlags<Qt::ItemFlag>flags(QAbstractTableModel::flags(ix));
	//qfWarning() << QF_FUNC_NAME << "Qt::ItemIsEditable" << flags.contains(Qt::ItemIsEditable);
	if(ix.isValid()) {
		bool can_edit = isEditRowsAllowed();
		//qfWarning() << "\t" << __LINE__ << can_edit;
		ColumnDefinition c = column(ix.column(), !Qf::ThrowExc);
		can_edit = can_edit && !c.isReadOnly();
		//qfWarning() << "\t" << __LINE__ << can_edit;
		int field_ix = c.fieldIndex();
		if(field_ix >= 0) {
			can_edit = can_edit && table()->field(field_ix, !Qf::ThrowExc).isUpdateable();
			/// BLOB fields cann't be edited in grid.
			QVariant::Type type = headerData(ix.column(), Qt::Horizontal, Qf::FieldTypeRole).type();
			can_edit = can_edit && (type != QVariant::ByteArray);
			if(type == QVariant::Bool) {
				//QVariant v = data(ix, Qt::EditRole);
				//if(!v.isNull()) {
					flags << Qt::ItemIsUserCheckable;// << Qt::ItemIsEnabled;
					//flags >> Qt::ItemIsEditable;
				//}
			}
			//qfWarning() << "\t" << __LINE__ << can_edit;
		}
		//qfWarning() << "\t" << __LINE__ << can_edit;
		if(can_edit) flags << Qt::ItemIsEditable;
		else flags >> Qt::ItemIsEditable;
	}
	return flags;
}

bool QFTableModel::setData(const QModelIndex & ix, const QVariant & value, int role)
{
	qfLogFuncFrame() << value.toString();
	bool ret = false;
	if(!ix.isValid()) return ret;
	QFBasicTable *t = table();
	//if(!t) return ret; table() vzdycky neco vrati
	int field_ix = column(ix.column(), !Qf::ThrowExc).fieldIndex();
	if(role == Qt::EditRole) {
		if(t->isValidRow(ix.row()) && t->isValidField(field_ix)) {
			QVariant v = value;
			if(isNullReportedAsString() && v.toString() == QFSql::nullString()) v = QVariant();
			// primitivni ochrana, abych si blob neprepsel tim, jak se zobrazuje
			//if(!v.toString().startsWith("{blob")) {
			setValue(ix.row(), field_ix, v);
			ret = true;
		}
	}
	else if(role == Qt::CheckStateRole) {
		qfTrash() << "\t Qt::CheckStateRole:" << value.toString();
		if(t->isValidRow(ix.row()) && t->isValidField(field_ix)) {
			ColumnDefinition cd = column(ix.column());
			qfTrash() << "\t Qt::CheckStateRole column RO:" << cd.isReadOnly();
			if(!cd.isReadOnly()) {
				int i = (value.toInt() == Qt::Unchecked)? 0: 1;
				setValue(ix.row(), field_ix, i);
				ret = true;
			}
		}
	}
	qfTrash() << "\treturn:" << ret;
	return ret;
}

QVariant QFTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	static QIcon icoDirty(QPixmap(":/libqfgui/images/tuzka.png"));
	QVariant ret;
	if(orientation == Qt::Horizontal) {
		if(role == Qt::DisplayRole || role == Qt::EditRole) {
			ColumnDefinition c = column(section, !Qf::ThrowExc);
			//if(c.fieldIndex() < 0) ret = c.fieldName();// + " [not linked]";
			ret = c.caption();
			//ret = c.fieldName();
			//qfTrash() << "\tindex:" << c.fieldIndex();
		}
		else if(role == Qt::ToolTipRole) {
			//qfTrash() << "TOOL TIP VOLE !!!";
			ret = column(section, !Qf::ThrowExc).toolTip();
		}
		else if(role == Qf::HeaderSectionInitialSizeRole) {
			ColumnDefinition c = column(section, !Qf::ThrowExc);
			ret = c.initialSize();
			//ret = (int)(QApplication::fontMetrics().lineSpacing() * 1.3);
			//ret = QAbstractTableModel::headerData(section, orientation, role);
			//qfTrash() << "size hint:" << ret.toString();
		}
		else if (role == Qf::FieldTypeRole) {
			ColumnDefinition cd = column(section, !Qf::ThrowExc);
			QVariant::Type type = cd.castType();
			if(type == QVariant::Invalid) {
				int field_ix = cd.fieldIndex();
				if(field_ix >= 0) {
					const QFBasicTable *t = table();
					type = t->field(field_ix).type();
				}
			}
			return QVariant(type);
		}
		else if (role == Qf::FieldIsNullableRole) {
			int field_ix = column(section, !Qf::ThrowExc).fieldIndex();
			if(field_ix < 0) return QVariant();
			const QFBasicTable *t = table();
			/// drivery nastavuji requiredStatus() jako !nullable
			return QVariant(t->field(field_ix).isNullable());
		}
		else if (role == Qf::FieldNativeTypeRole) {
			int field_ix = column(section, !Qf::ThrowExc).fieldIndex();
			if(field_ix < 0) return QVariant();
			const QFBasicTable *t = table();
			return QVariant(t->field(field_ix).typeID());
		}
		else if(role == Qf::ColumnSqlIdRole) {
			ret = column(section, !Qf::ThrowExc).fieldName();
		}
	}
	else if(orientation == Qt::Vertical) {
		if(role == Qt::DisplayRole || role == Qt::EditRole) {
			QFBasicTable::Row r = table()->row(section, !Qf::ThrowExc);
			if(!r.isNull() && r.isDirty()) {
				ret = QString();
			}
			else ret = QVariant(section + 1);
		}
		else if(role == Qt::DecorationRole) {
			QFBasicTable::Row r = table()->row(section, !Qf::ThrowExc);
			if(!r.isNull() && r.isDirty()) {
				qfTrash() << QF_FUNC_NAME << "role:" << role << "orientation:" << orientation;
				return qVariantFromValue(icoDirty);
			}
		}
	}
	else {
		ret = QAbstractTableModel::headerData(section, orientation, role);
	}
	return ret;
}

void QFTableModel::clearColumns()
{
	qfTrash() << QF_FUNC_NAME;
	columnsRef().clear();
}

QFTableModel::ColumnList& QFTableModel::columnsRef()
{
	if(d->columns.isEmpty()) {
		qfTrash() << QF_FUNC_NAME << "EMPTY";
		QFBasicTable *t = table();
		int ix = 0;
		if(t) {
			if(t->fields().isEmpty()) {
				qfTrash() << "\ttable EMPTY too";
			}
			else foreach(QFSqlField f, t->fields()) {
				qfTrash() << "\tfield:" << f.fullName() << f.fieldName() << f.driverReportedName();
				ColumnDefinition &cd = addColumn(f.fullName(), f.fieldName());
				cd.setFieldIndex(ix++);
				cd.setToolTip(f.fullName());
			}
		}
	}
	return d->columns;
}

QFTableModel::ColumnDefinition& QFTableModel::insertColumn(int before_ix, const QString &field_name, const QString &_caption, int init_size)
{
	qfLogFuncFrame() << field_name << "before_ix:" << before_ix;
	if(before_ix < 0 || before_ix > d->columns.count()) before_ix = d->columns.count();
	d->columns.insert(before_ix, ColumnDefinition(field_name));
	ColumnDefinition &c = d->columns[before_ix];
	c.setCaption(_caption).setInitialSize(init_size);
	return c;
}

QFTableModel::ColumnDefinition QFTableModel::removeColumn(int ix)
{
	ColumnDefinition cd;
	if(ix >= 0 && ix < d->columns.count()) cd = d->columns.takeAt(ix);
	return cd;
}

QFTableModel::ColumnDefinition& QFTableModel::columnRef(int col) throw(QFException)
{
	if(col < 0 || col >= columns().size())
		QF_EXCEPTION(tr("Column %1 out of range (%2)").arg(col).arg(columns().size()));
	return columnsRef()[col];
}

QFTableModel::ColumnDefinition QFTableModel::column(int col, bool throw_exc) const throw(QFException)
{
	if(col < 0 || col >= columns().size()) {
		if(throw_exc) QF_EXCEPTION(tr("Column %1 out of range (%2)").arg(col).arg(columns().size()));
		return ColumnDefinition();
	}
	return columns()[col];
}

int QFTableModel::columnIndex(int field_ix, bool throw_exc) const throw(QFException)
{
	int ret = -1, i = 0;
	foreach(ColumnDefinition cd, columns()) {
		if(cd.fieldIndex() == field_ix) {ret = i; break;}
		i++;
	}
	if(throw_exc && ret < 0) QF_EXCEPTION(tr("Field index %1 not found in column list.").arg(field_ix));
	return ret;
}

int QFTableModel::fieldIndex(int column_ix, bool throw_exc) const throw(QFException)
{
	int ret = column(column_ix, throw_exc).fieldIndex();
	if(throw_exc && ret < 0) QF_EXCEPTION(tr("Field index %1 is less tha 0.").arg(column_ix));
	return ret;
}

int QFTableModel::fieldIndex(const QString &column_name, bool throw_exc) const throw(QFException)
{
	int ix = table()->fields().fieldNameToIndex(column_name, throw_exc);
	return ix;
}

int QFTableModel::columnIndex(const QString &column_name, bool throw_exc) const throw(QFException)
{
	//qfTrash() << QF_FUNC_NAME << column_name;
	int ret = -1, i = 0;
	foreach(ColumnDefinition cd, columns()) {
		//qfTrash() << "\ttrying:" << cd.fieldName();
		if(QFSql::endsWith(cd.fieldName(), column_name)) {ret = i; break;}
		i++;
	}
	if(throw_exc && ret < 0) {
		QStringList sl;
		foreach(ColumnDefinition cd, columns()) sl << cd.fieldName();
		QString s = sl.join(", ");
		QF_EXCEPTION(tr("Column named '%1' not found in column list. Existing columns: [%2]").arg(column_name).arg(s));
	}
	return ret;
}

void QFTableModel::fillColumnIndexes()
{
	qfTrash() << QF_FUNC_NAME;
	QFBasicTable *t = table();
	if(t) for(int i=0; i<columns().size(); i++) {
		ColumnDefinition &c = columnsRef()[i];
		int ix = t->fields().fieldNameToIndex(c.fieldName(), !Qf::ThrowExc);
		if(ix < 0) qfTrash() << "\tcolumn" << c.fieldName() << "not found in fields."; 
		c.setFieldIndex(ix);
	}
}

void QFTableModel::reset()
{
	qfTrash() << QF_FUNC_NAME;
	//if(column_policy == ClearColumns) clearColumns();
	fillColumnIndexes();
	QAbstractTableModel::reset();
	emit layoutChanged();
	emit allDataChanged();
}

void QFTableModel::sort(int column, Qt::SortOrder order, bool cs, bool asci7bit)
{
	QFBasicTable *t = table();
	if(t) {
		QFBasicTable::SortDefList sdl;
		bool asc = (order == Qt::AscendingOrder);
		int fld_ix = fieldIndex(column, !Qf::ThrowExc);
		if(fld_ix >= 0) sdl << QFBasicTable::SortDef(fld_ix, asc, cs, asci7bit);
		t->sort(sdl);
	}
	emit layoutChanged();
}

void QFTableModel::sort(const QFBasicTable::SortDefList &sdl)
{
	sortTable(sdl, 0, rowCount());
}

void QFTableModel::sortTable(const QFBasicTable::SortDefList & sdl, int start_row_index, int row_count)
{
	QFBasicTable *t = table();
	if(t) {
		t->sort(sdl, start_row_index, row_count);
	}
	emit layoutChanged();
}

bool QFTableModel::isEditRowsAllowed() const
{
	bool ret = false;
	QFBasicTable *t = table();
	if(t) {
		ret = t->isEditRowsAllowed();
	}
	ret = ret && d->flags.contains(EditRowsAllowed);
	return ret;
}
bool QFTableModel::isInsertRowsAllowed() const
{
	bool ret = false;
	QFBasicTable *t = table();
	if(t) {
		ret = t->isInsertRowsAllowed();
	}
	ret = ret && d->flags.contains(InsertRowsAllowed);
	return ret;
}

bool QFTableModel::isDeleteRowsAllowed() const
{
	bool ret = false;
	QFBasicTable *t = table();
	if(t) {
		ret = t->isDeleteRowsAllowed();
	}
	ret = ret && d->flags.contains(DeleteRowsAllowed);
	return ret;
}

void QFTableModel::setReadOnly(bool ro)
{
	setEditRowsAllowed(!ro);
	setInsertRowsAllowed(!ro);
	setDeleteRowsAllowed(!ro);
}

bool QFTableModel::isReadOnly() const
{
	return !isEditRowsAllowed() && !isInsertRowsAllowed() && !isDeleteRowsAllowed();
}

bool QFTableModel::isDirty(int row, const QString& col_name) const throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	int field_ix = fieldIndex(col_name);
	qfTrash() << "col_name:" << col_name << "field_ix:" << field_ix;
	return isDirty(row, field_ix);
}

void QFTableModel::clearDirty(int row_ix) throw(QFException)
{
	table()->rowRef(row_ix).clearEditFlags();
}

bool QFTableModel::isDirty(int row, int field_index_in_table) const throw(QFException)
{
	QFBasicTable::Row r = table()->row(row);
	return r.isDirty(field_index_in_table);
}

QVariant QFTableModel::value(int row, int field_index_in_table, bool throw_exc) const throw(QFException)
{
	QFBasicTable::Row r = table()->row(row);
	if(r.isNull() && throw_exc) {
		QF_EXCEPTION(tr("Row index %1 out of range (%2).").arg(row).arg(table()->rowCount()));
	}
	return r.value(field_index_in_table, throw_exc);
}

QVariant QFTableModel::value(int row, const QString& col_name, bool throw_exc) const throw(QFException)
{
	int field_index_in_table = fieldIndex(col_name, throw_exc);
	return value(row, field_index_in_table);
}

QVariant QFTableModel::origValue(int row, const QString & col_name, bool throw_exc) const throw( QFException )
{
	int field_index_in_table = fieldIndex(col_name, throw_exc);
	return value(row, field_index_in_table);
}

QVariant QFTableModel::origValue(int row, int field_index_in_table, bool throw_exc) const throw( QFException )
{
	QFBasicTable::Row r = table()->row(row);
	if(r.isNull() && throw_exc) {
		QF_EXCEPTION(tr("Row index %1 out of range (%2).").arg(row).arg(table()->rowCount()));
	}
	return r.origValue(field_index_in_table, throw_exc);
}

void QFTableModel::setValue(int row, int field_index_in_table, const QVariant &val) throw(QFException)
{
	QFBasicTable::Row &r = table()->rowRef(row);
	r.setValue(field_index_in_table, val);
}

void QFTableModel::setValue(int row, const QString& col_name, const QVariant &val) throw(QFException)
{
	qfLogFuncFrame() << "row no:" << row << "rowCount:" << rowCount() << "col_name:" << col_name << "value:" << val.toString();
	int field_ix = fieldIndex(col_name);
	if(field_ix < 0) QF_EXCEPTION(tr("Model column named '%1' not found.").arg(col_name));
	setValue(row, field_ix, val);
}

QVariant QFTableModel::value(const QModelIndex &ix, bool throw_exc) const  throw(QFException)
{
		//qfTrash() << QF_FUNC_NAME;
		//qfTrash() << "\trow:" << ix.row() << "col:" << ix.column() << "role:" << role;
	int field_ix = fieldIndex(ix.column(), !Qf::ThrowExc);
	if(field_ix < 0) {
		QString s = tr("invalid column: %1").arg(ix.column());
		if(throw_exc) QF_EXCEPTION(s);
		return s;
	}
	const QFBasicTable *t = table();
	if(!t->isValidRow(ix.row())) {
		QString s = tr("invalid row: %1").arg(ix.row());
		if(throw_exc) QF_EXCEPTION(s);
		return s;
	}
	if(!t->isValidField(field_ix)) {
		QString s = tr("invalid field index: %1").arg(field_ix);
		if(throw_exc) QF_EXCEPTION(s);
		return s;
	}
	QVariant ret = value(ix.row(), field_ix);
		//qfInfo() << "raw value:" << ret.toString() << "is valid:" << ret.isValid() << "is null:" << ret.isNull();
	return ret;
}

void QFTableModel::setValue(const QModelIndex &ix, const QVariant &val) throw(QFException)
{
	int field_ix = fieldIndex(ix.column(), !Qf::ThrowExc);
	setValue(ix.row(), field_ix, val);
}

void QFTableModel::setDataFromChooser(const QModelIndex & ix, QFDlgDataTable * chooser)
{
	if(!ix.isValid()) return;
	QString column_name = column(ix.column()).fieldName();
	if(true) {
		//int row = ix.row();
		QFBasicTable::Row r = chooser->chosenRow();
		//QModelIndex ix2 = ix.sibling(row, columnIndex("monterId"));
		QFTableView *v = chooser->view();
		setData(ix, r.value(v->idColumnName(), !Qf::ThrowExc));
	}
}
