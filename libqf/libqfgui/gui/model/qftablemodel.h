#ifndef QFTABLEMODEL_H
#define QFTABLEMODEL_H

#include <qfguiglobal.h>
#include <qfsql.h>
#include <qfclassfield.h>
#include <qfsqlfield.h>
#include <qf.h>
#include <qfbasictable.h>

#include <QAbstractTableModel>
#include <QVector>
#include <QVariant>
#include <QPointer>
#include <QSharedData>

class QFTableView;
class QFDlgDataTable;

//! Tady chybi dokumentace.
class QFGUI_DECL_EXPORT QFTableModel : public QAbstractTableModel
{
	Q_OBJECT;
	public:
		enum Flags {EditRowsAllowed = 1, InsertRowsAllowed = 2, DeleteRowsAllowed = 4};
	public:
		void setReadOnly(bool ro = true);
		bool isReadOnly() const;
	public:
		class QFGUI_DECL_EXPORT ColumnDefinition
		{
			protected:
				class Data : public QSharedData
				{
					public:
						QString fieldName; //!< ID to pair ColumnDefinitions with fields
						int fieldIndex;
						QString caption;
						QString toolTip;
						int initialSize; //!< initial width of column
						bool readOnly;
						Qt::Alignment alignment;
						QPointer<QFDlgDataTable> chooser;
						QString format; //!< format for date, time, ... types
						QVariant::Type castType; /// pokud je formatem nastaveno pretypovani je to tento typ, jinak QVariant::Invalid

						Data(const QString &fldname = QString()) : fieldName(fldname), fieldIndex(-1), readOnly(false), castType(QVariant::Invalid) {}
				};
			private:
				QSharedDataPointer<Data> d;
				ColumnDefinition(int) {
					d = new Data();
				}
			public:
				static const ColumnDefinition& sharedNull();
				bool isNull() const {return d == sharedNull().d;}
			public:
				ColumnDefinition() {
					*this = sharedNull();
				}
				ColumnDefinition(const QString &fldname) {
					d = new Data(fldname);
				}

				QString fieldName() const {return d->fieldName;}
				ColumnDefinition& setFieldName(const QString &s) {d->fieldName = s; return *this;}
				int fieldIndex() const {return d->fieldIndex;}
				ColumnDefinition& setFieldIndex(int i) {d->fieldIndex = i; return *this;}
				QString caption() const {return d->caption;}
				ColumnDefinition& setCaption(const QString &s) {d->caption = s; return *this;}
				QString toolTip() const {return d->toolTip;}
				ColumnDefinition& setToolTip(const QString &s) {d->toolTip = s; return *this;}
				int initialSize() const {return d->initialSize;}
				ColumnDefinition& setInitialSize(int i) {d->initialSize = i; return *this;}
				bool isReadOnly() const {return d->readOnly;}
				ColumnDefinition& setReadOnly(bool b = true) {d->readOnly = b; return *this;}
				Qt::Alignment alignment() const {return d->alignment;}
				ColumnDefinition& setAlignment(const Qt::Alignment &al) {d->alignment = al; return *this;}
				QFDlgDataTable* chooser() const; 
				ColumnDefinition& setChooser(QFDlgDataTable *_chooser);
				QString format() const {return d->format;}
				/// pro double viz. QFString::number(...)
				/// pro QTime viz. QTime::toString(...)
				/// pro QDate viz. QDate::toString(...)
				ColumnDefinition& setFormat(const QString &s) {d->format = s; return *this;}
				
				ColumnDefinition& setCastType(QVariant::Type t) {d->castType = t; return *this;}
				QVariant::Type castType() const {return d->castType;}
		};
		typedef QList<ColumnDefinition> ColumnList;
	public:
		//! prida field \a field_name do seznamu zobrazovanych fieldu
		ColumnDefinition& addColumn(const QString &field_name, const QString &_caption = QString(), int init_size = -1) {
			return insertColumn(-1, field_name, _caption, init_size);
		}
		ColumnDefinition& insertColumn(int before_ix, const QString &field_name, const QString &_caption = QString(), int init_size = -1);
		ColumnDefinition removeColumn(int ix);
	protected:
		void fillColumnIndexes();
	public:
		void clearColumns();
	protected:
		class Data //: public QSharedData
		{
			public:
				ColumnList columns;
				QFBasicTable *table;
				QString doubleFormat;
				QString intFormat;
				bool nullReportedAsString;
				int elideDisplayedTextAt;
				QFFlags<Flags> flags;
			public:
				Data() : table(NULL) {flags << EditRowsAllowed << InsertRowsAllowed << DeleteRowsAllowed;}
				~Data() { }
		};
	private:
		Data _d, *d;
	public:
		bool isEditRowsAllowed() const;
		void setEditRowsAllowed(bool b = true) {if(b) d->flags << EditRowsAllowed; else d->flags >> EditRowsAllowed;}
		bool isInsertRowsAllowed() const;
		void setInsertRowsAllowed(bool b = true) {if(b) d->flags << InsertRowsAllowed; else d->flags >> InsertRowsAllowed;}
		bool isDeleteRowsAllowed() const;
		void setDeleteRowsAllowed(bool b = true) {if(b) d->flags << DeleteRowsAllowed; else d->flags >> DeleteRowsAllowed;}

		//! viz. QFString::number(double d, const QString &format);
		QString doubleFormat() const {return d->doubleFormat;}
		void setDoubleFormat(const QString &fmt) {d->doubleFormat = fmt;}
		QString intFormat() const {return d->intFormat;}
		void setIntFormat(const QString &fmt) {d->intFormat = fmt;}

		const ColumnList& columns() const {return const_cast<QFTableModel*>(this)->columnsRef();}
		//! Model returns NULL value as string '{null}', not as a invalid QVariant();
		bool isNullReportedAsString() const {return d->nullReportedAsString;}
		void setNullReportedAsString(bool b = true) {d->nullReportedAsString = b;}
		int elideDisplayedTextAt() const {return d->elideDisplayedTextAt;}
		void setElideDisplayedTextAt(int n) {d->elideDisplayedTextAt = n;}
	protected:
		ColumnList& columnsRef();
		// clears all rows, if \a fields_options tells what else will be cleared.
		//void cleanupData(CleanupDataOption fields_options);
		//QFBasicTable* tableCreatedByModel() const {return d->tableCreatedByModel;}
		//void setTableCreatedByModel(QFBasicTable *t) {d->tableCreatedByModel = t;}
	public:
		/**
		 *  checks if item row and column exists in the query result.
	 	 * @param role not used for now
		 */
		bool isValid(int row, int col, int role) const;
		bool isValidCol(int col) const;
		bool isEmpty() const {return rowCount() <= 0;}
		bool tableIsNull() const {return d->table == NULL;}
	public:
		/*
		QFBasicTable& tableRef() throw(QFException) {
			QFBasicTable *t = table();
			if(!t) QF_EXCEPTION("Table is NULL.");
			return *t;
		}
		*/
		//! Return the model table set by calling \a setTable() or create a new one if no one was set.
		/**
		 Never returns NULL.
		 The model has no table, before some one calls this function.
		 */
		virtual QFBasicTable* table() const;

		//! Set new table to model, model TAKES OWNERSHIP of the table.
		virtual void setTable(QFBasicTable *t);
	public:
		virtual Qt::ItemFlags flags(const QModelIndex &index) const;
		virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
		virtual bool setData(const QModelIndex & ix, const QVariant & value, int role = Qt::EditRole);
		virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

		/// Vola QFTableModelDelegate, pokud se bunka editovala pomoci chooseru.
		virtual void setDataFromChooser(const QModelIndex & ix, QFDlgDataTable *chooser);
	public:
		ColumnDefinition& columnRef(int col) throw(QFException);
		ColumnDefinition& columnRef(const QString &col_name) throw(QFException) { return columnRef(columnIndex(col_name)); }
		ColumnDefinition column(int col, bool throw_exc = true) const throw(QFException);
		ColumnDefinition column(const QString &col_name, bool throw_exc = true) const throw(QFException) { return column(columnIndex(col_name), throw_exc); }
		//! Returns index of column \a column_name in underlaying \a table() .
		int fieldIndex(const QString &column_name, bool throw_exc = true) const throw(QFException);
		int fieldIndex(int column_ix, bool throw_exc) const throw(QFException);
		//! Returns index of column \a column_name in model .
		int columnIndex(const QString &column_name, bool throw_exc = true) const throw(QFException);
		//! Converts \a table() field index to column index in model columns.
		int columnIndex(int field_ix, bool throw_exc = true) const throw(QFException);

		/// data z tabulky
		void setValue(int row, const QString& col_name, const QVariant &val) throw(QFException);
		/// data z tabulky
		QVariant value(int row, const QString& col_name, bool throw_exc = true) const throw(QFException);
		QVariant origValue(int row, const QString& col_name, bool throw_exc = true) const throw(QFException);
		bool isDirty(int row, const QString& col_name) const throw(QFException);
		void clearDirty(int row) throw(QFException); 
	
		virtual void setValue(int row, int field_index_in_table, const QVariant &val) throw(QFException);
		QVariant value(int row, int field_index_in_table, bool throw_exc = true) const throw(QFException);
		QVariant origValue(int row, int field_index_in_table, bool throw_exc = true) const throw(QFException);
		bool isDirty(int row, int field_index_in_table) const throw(QFException);

		QVariant value(const QModelIndex &ix, bool throw_exc = Qf::ThrowExc) const  throw(QFException);
		void setValue(const QModelIndex &ix, const QVariant &val) throw(QFException);
	public slots:
		virtual void reload() throw(QFException);
		virtual bool reloadRow(int ri) throw(QFException);
		virtual bool postRow(int ri) throw(QFException);
	public:
		// reimplemented from QAbstractTableModel
		virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;
		virtual int columnCount(const QModelIndex & parent = QModelIndex()) const;
		/// vraci index insertnutyho radku nebo -1
		virtual int insertRow(int before_row, const QModelIndex & parent = QModelIndex()) throw(QFException);
		int appendRow() throw(QFException) {return insertRow(-1);}
		virtual bool removeRows(int row, int count, const QModelIndex & parent = QModelIndex())  throw(QFException);
		//virtual bool removeRow(int ri, const QModelIndex & parent = QModelIndex()) throw(QFException);
		/**
		 *       Insert row in BasicTable only.
		 */
		void insertBasicRow(int before_row) throw(QFException);
		//!        Remove row in BasicTable only.
		void removeBasicRow(int _row) throw(QFException);
	public slots:
		//virtual void reload(ColumnPolicy column_policy = KeepColumns) throw(QFException);
		virtual void reset();
	signals:
	    //! Emited when new data are loaded into model.
		void allDataChanged();
		/// emituje se pokud doslo k uspesnemu postnuti radku.
		void rowPosted(int row_no);
		/// doslo k pohybu v modelu, nekdo uspesne postnul nebo vymazal radeky, takze to postihuje i insert.
		void rowsPostedOrRemoved();
	protected:
		/// setridi tabulku pod modelem a emituje layoutChanged()
		virtual void sortTable(const QFBasicTable::SortDefList &sdl, int start_row_index, int row_count);
	public:
		// sort / search / seek
		//! Reimplemented from \a QAbstractItemModel .
		//! Pokud je \a column neplatny, nastavi se puvodni poradi ktere bylo po reload().
		virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder) {sort(column, order, false, true);}
		virtual void sort(int column, Qt::SortOrder order, bool case_sensitive, bool asci7bit);
		virtual void sort(const QFBasicTable::SortDefList &sdl);
	public:
		QFTableModel(QObject *parent = NULL);
		virtual ~QFTableModel();
};

#endif // QFTABLEMODEL_H

