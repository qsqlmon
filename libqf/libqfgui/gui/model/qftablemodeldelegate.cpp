
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qftablemodeldelegate.h"

#include <qfdlgdatatable.h>
#include <qftableview.h>
#include <qfsqlquerytablemodel.h>
#include <qfsql.h>

#include <QItemDelegate>

#include <qflogcust.h>

//===============================================
//                                 QFTableModelDelegateEditWithButton
//===============================================
QFTableModelDelegateEditWithButton::QFTableModelDelegateEditWithButton(QFDlgDataTable *chooser, QWidget *parent)
	: QFEditWithButton(parent), f_chooser(chooser)
{
	QF_CONNECT(this, SIGNAL(buttonClicked()), this, SLOT(slotButtonClicked()));
}

QFDlgDataTable* QFTableModelDelegateEditWithButton::chooser()
{
	if(!f_chooser) QF_EXCEPTION(tr("Chooser is NULL."));
	return f_chooser;
}

void QFTableModelDelegateEditWithButton::slotButtonClicked()
{
	QFDlgDataTable *ch = chooser();
	if(ch->model()->isEmpty()) ch->reload();
	ch->loadPersistentData();
	int ret = ch->exec();
	if(ret == QFDialog::Accepted) {
		QFBasicTable::Row r = ch->view()->selectedRow();
		if(!r.isNull()) {
			QString id_col = ch->view()->idColumnName();
			QString cap_col = ch->view()->captionColumnName();
			if(cap_col.isEmpty()) cap_col = id_col;
			if(cap_col.isEmpty()) setText("NO_CAP_COL");
			else setText(r.value(cap_col).toString());
		}
		emit commitAndCloseEditor();
	}
	else if(ret == QFDialog::NoneOfThem) {
		setText(QString());
		emit commitAndCloseEditor();
	}
	else {
		emit rejectAndCloseEditor();
	}
}

//===============================================
//                                 QFTableModelDelegate
//===============================================
void QFTableModelDelegate::commitAndCloseEditor()
{
	qfTrash() << QF_FUNC_NAME;// << currentEditor;//QFLog::stackTrace();
	QWidget *editor = qobject_cast<QWidget*>(sender());
	QF_ASSERT(editor, "divnej editor");
	qfTrash() << "\temitting";
	emit commitData(editor);
	emit closeEditor(editor);
}

void QFTableModelDelegate::rejectAndCloseEditor()
{
	qfTrash() << QF_FUNC_NAME;// << currentEditor;//QFLog::stackTrace();
	QWidget *editor = qobject_cast<QWidget*>(sender());
	QF_ASSERT(editor, "divnej editor");
	emit closeEditor(editor);
}

QWidget* QFTableModelDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
	qfLogFuncFrame();
	const QFTableModel *m = qobject_cast<const QFTableModel*>(index.model());
	qfTrash() << "\tmodel:" << m;
	QWidget *ret = NULL;
	if(m) {
		//QF_ASSERT(m, "model is NULL");
		//QString column_name = m->column(index.column()).fieldName();
		QFDlgDataTable *chooser = m->column(index.column()).chooser();
		if(chooser) {
			QFTableModelDelegateEditWithButton *ed = new QFTableModelDelegateEditWithButton(chooser, parent);
			ed->setReadOnly();
			//ed->setButtonVisible();
			/// QueuedConnection je dulezite, bez toho nefunguje index v setModelData(...)
			connect(ed, SIGNAL(commitAndCloseEditor()), this, SLOT(commitAndCloseEditor()), Qt::QueuedConnection);
			connect(ed, SIGNAL(rejectAndCloseEditor()), this, SLOT(rejectAndCloseEditor()), Qt::QueuedConnection);
			ed->slotButtonClicked();
			//QF_CONNECT(this, SIGNAL(execChooser()), ed, SLOT(slotButtonClicked()));
			//ed->lineEdit->setReadOnly(true);
			//currentEditor = ed;
			ret = ed;
		}
	}
	if(ret) {
		//currentEditor = ret;
		return ret;
	}
	return QFItemDelegateBase::createEditor(parent, option, index);
}

void QFTableModelDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
	qfLogFuncFrame();
	QFTableModel *m = qobject_cast<QFTableModel*>(model);
	if(!m) return;
	qfTrash() << "column index:" << index.column();
	if(!index.isValid()) return;
	//QString column_name = m->column(index.column()).fieldName();
	if(QFTableModelDelegateEditWithButton *ed = qobject_cast<QFTableModelDelegateEditWithButton*>(editor)) {
		//int result = ed->chooser()->execResult();
		m->setDataFromChooser(index, ed->chooser());
		/*
		if(result == QFDialog::NoneOfThem) {
			m->setDataFromChooser(index, QFBasicTable::Row());
		}
		else {
			QFTableView *v = qobject_cast<QFTableView*>(ed->chooser()->view());
			if(v) {
				QFBasicTable::Row r = v->selectedRow();
				if(!r.isNull()) {
					m->setDataFromChooser(index, r);
				}
			}
		}
		*/
	}
	else QItemDelegate::setModelData(editor, model, index);
}

QFTableView * QFTableModelDelegate::view() const
{
	QFTableView *view = qobject_cast<QFTableView*>(parent()); 
	return view;
}
/*
void QFTableModelDelegate::drawBackground(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const
{
	qfLogFuncFrame();
	QFTableView *v = view();
	QModelIndex ix = v->currentIndex();
	QStyleOptionViewItem opt = option;
	if(v && index.row() == ix.row() && index != ix) {
		painter->fillRect(option.rect, Qt::yellow);
	}
	else QFItemDelegateBase::drawBackground(painter, opt, index);
}
*/
void QFTableModelDelegate::paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const
{
	/**
	Protoze z nepochopitelnyho duvodu neni funkce drawBackground() virtualni, musim patchovat QItemDelegate::drawBackground() v QT, kdyz chci podsvitit aktivni radek
	qtitemdelegate.cpp:823
	
	#if 1/// QF_PATCH patch pro podsviceni radku se selekci
		QVariant value = index.data(Qt::BackgroundRole);
		if(!value.isValid()) value = property("qfSelectedRowHighlightColor");
	#else
		QVariant value = index.data(Qt::BackgroundRole);
	#endif

	*/
	QFTableView *v = view();
	if(v) {
		QModelIndex ix = v->currentIndex();
		if(index.row() == ix.row() && index != ix) {
			QStyleOptionViewItem opt = option;
			const_cast<QFTableModelDelegate*>(this)->setProperty("qfSelectedRowHighlightColor", QColor(245, 245, 184));//QColor("lemonchiffon"));
		}
		else {
			const_cast<QFTableModelDelegate*>(this)->setProperty("qfSelectedRowHighlightColor", QVariant());
		}
	}
	QFItemDelegateBase::paint(painter, option, index);
}

