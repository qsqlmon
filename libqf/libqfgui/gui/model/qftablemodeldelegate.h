
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFTABLEMODELDELEGATE_H
#define QFTABLEMODELDELEGATE_H

#include <qfguiglobal.h>
#include <qfclassfield.h>
#include <qfeditwithbutton.h>
#include <qfitemdelegatebase.h>

#include <QPointer>

class QFDlgDataTable;
class QFTableView;

//! TODO: write class documentation.
class  QFGUI_DECL_EXPORT QFTableModelDelegateEditWithButton : public QFEditWithButton
{
	Q_OBJECT;
	protected:
		QPointer<QFDlgDataTable> f_chooser;
	signals:
		void commitAndCloseEditor();
		void rejectAndCloseEditor();
	public slots:
		void slotButtonClicked();
	public:
		QFDlgDataTable* chooser();
	public:
		QFTableModelDelegateEditWithButton(QFDlgDataTable *_chooser, QWidget *parent = NULL);
};

class QFGUI_DECL_EXPORT QFTableModelDelegate : public QFItemDelegateBase
{
	Q_OBJECT;
	protected:
		/// pokud je parent() QFTableView vraci jej, jinak NULL
		QFTableView* view() const;
		//virtual void drawBackground(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
	public:
		virtual void paint ( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const;
	protected slots:
		void commitAndCloseEditor();
		void rejectAndCloseEditor();
	public:
		virtual QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const;
		virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
	public:
		QFTableModelDelegate(QObject *parent = NULL) : QFItemDelegateBase(parent) {}
};


#endif // QFTABLEMODELDELEGATE_H

