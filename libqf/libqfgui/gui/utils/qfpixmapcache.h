//
// C++ Interface: qfpixmapcache
//
// Description: 
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef QFPIXMAPCACHE_H
#define QFPIXMAPCACHE_H

#include <QPixmapCache>
#include <QIcon>

#include <qfguiglobal.h>
#include <qfsearchdirs.h>

/**
@author Fanda Vacek
*/
class QFGUI_DECL_EXPORT QFPixmapCache : public QPixmapCache
{
	protected:
		static QFSearchDirs f_searchDirs;
		//static QString searchFile(const QString &file_name);
		static QFSearchDirs& searchDirsRef();
		static const QFSearchDirs& searchDirs() {return searchDirsRef();}
	public:
		static QPixmap insert(const QString &key, const QString &pixmap_file_name);
		//! @param pixmap_file_name If \a key does not exist in the cache, pixmap is loaded from \a pixmap_file_name
		//! and cache is updated.
		static QPixmap pixmap(const QString &key, const QString &pixmap_file_name = QString());
		//! @param pixmap_file_name If \a key does not exist in the cache, icon is loaded from \a pixmap_file_name
		//! and cache is updated. If \a pixmap_file_name is empty, cache tries to load file "key.png"
		static QIcon icon(const QString &key, const QString &pixmap_file_name = QString());
		/// append neni tak sikovny, protoze by neslo predbehnout :/libqfgui/images
		static void prependSearchDir(const QString &path) {searchDirsRef().prependDir(path);}
};

#endif
