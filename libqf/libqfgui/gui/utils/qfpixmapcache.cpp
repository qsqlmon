//
// C++ Implementation: qfpixmapcache
//
// Description: 
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "qfpixmapcache.h"

#include <qffileutils.h>

#include <qflogcust.h>
		
QFSearchDirs QFPixmapCache::f_searchDirs;

QFSearchDirs & QFPixmapCache::searchDirsRef()
{
	if(f_searchDirs.dirs().isEmpty()) {
		f_searchDirs.appendDir(":/libqfgui/images");
	}
	return f_searchDirs;
}

QPixmap QFPixmapCache::insert(const QString &key, const QString &pixmap_file_name)
{
	static QPixmap null_pm;
	QPixmap pm;
	if(key.isEmpty()) return null_pm;
	if(!QPixmapCache::find(key, pm)) {
		QString fn = pixmap_file_name.trimmed();
		if(fn.isEmpty()) fn = key;
		if(!(fn.endsWith(".png") || fn.endsWith(".svg"))) fn = fn.section('.', -1, -1) + ".png";
		QString abs_fn = searchDirs().findFile(fn);
		if(abs_fn.isEmpty()) {
			qfWarning() << "Cann't open pixmap file" << fn << "key:" << key << "in dirs:" << searchDirs().dirs().join(", ");
		}
		else {
			if(pm.load(abs_fn)) {
				if(pm.width() > 128) {
					pm = pm.scaled(128, pm.height(), Qt::KeepAspectRatio);
				}
				if(pm.width() > 128) {
					pm = pm.scaled(pm.width(), 128, Qt::KeepAspectRatio);
				}
				QPixmapCache::insert(key, pm);
			}
		}
	}
	return pm;
}

QPixmap QFPixmapCache::pixmap(const QString &key, const QString &pixmap_file_name)
{
	return insert(key, pixmap_file_name);
}

QIcon QFPixmapCache::icon(const QString &key, const QString &pixmap_file_name)
{
	return QIcon(pixmap(key, pixmap_file_name));
}
/*
QString QFPixmapCache::searchFile(const QString &file_name)
{
	qfLogFuncFrame() << file_name;
	QString ret;
	if(QFile::exists(file_name)) ret = file_name;
	else foreach(QString s, searchPath()) {
		if(QDir::isRelativePath(s)) s = QFFileUtils::joinPath(QFFileUtils::appDir(), s);
		s = QFFileUtils::joinPath(s, file_name);
		qfTrash() << "\ttrying:" << s;
		if(QFile::exists(s)) {
			ret = s;
			break;
		}
	}
	qfTrash() << "\tfound:" << ret;
	return ret;
}
*/
/*/
void QFPixmapCache::appendSearchPath(const QString &path)
{
	int ix = searchPath().indexOf(path);
	if(ix < 0) searchPath() << path;
}
*/

