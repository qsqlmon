/****************************************************************************
**
** Copyright (C) 2005-2005 Trolltech AS. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.trolltech.com/products/qt/opensource.html
**
** If you are unsure which license is appropriate for your use, please
** review the following information:
** http://www.trolltech.com/products/qt/licensing.html or contact the
** sales department at sales@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef QFTEXTVIEWWIDGET_H
#define QFTEXTVIEWWIDGET_H

#include <qfdialogwidget.h>
#include <qfpart.h>
//#include <qfaction.h>

namespace Ui {
class QFTextViewWidget;
}
class QTextEdit;

class QFGUI_DECL_EXPORT QFTextViewWidget : public QFDialogWidget
{
	Q_OBJECT;
	private:
		Ui::QFTextViewWidget *ui;
	protected:
		QFPart::ActionList toolBarActions;
		QFPart::ActionList f_actions;
		QString suggestedFileName;
		QString f_codecName;
	public:
		QString codecName() const {return (f_codecName.isEmpty())? "system": f_codecName;}
		void setCodecName(const QString &codec_name) {f_codecName = codec_name;}
		QString text();
		void setText(const QString &doc, const QString &suggested_file_name = QString(), const QString &codec_name = QString());
		void setUrl(QFile &url, const QString &codec_name = QString());
		void setFile(const QString &file_name, const QString &codec_name = QString());
		QString fileName() {return suggestedFileName;}

		QTextEdit* editor();
	protected:
		void save_helper(const QString &file_name);
	public slots:
		void save();
		void saveAs();
		void print();
		void wordWrap(const QString &action_id, bool checked);
	signals:
		void acceptRequest();
	private:
		void createToolBar();
		void createActions();
		QFPart::ActionList& actionListRef() {return f_actions;}
		QFPart::ActionList actionList() const {
			return ((QFTextViewWidget*)this)->actionListRef();
		}
	public:
		virtual QFPart::ToolBarList createToolBars();
	public:
		QFTextViewWidget(QWidget *parent = 0);
		~QFTextViewWidget();
};

#endif     // QFTEXRVIEWWIDGET_H
