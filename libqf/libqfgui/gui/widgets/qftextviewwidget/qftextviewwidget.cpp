#include "qftextviewwidget.h"
#include "ui_qftextviewwidget.h"

#include <qfpart.h>
#include <qfapplication.h>
#include <qfuibuilder.h>
#include <qfaction.h>
#include <qfpixmapcache.h>
#include <qfmessage.h>
#include <qffileutils.h>
#include <qftoolbar.h>

#include <QtGui>
#include <QString>
#include <QTextCodec>

#include <qflogcust.h>

QFTextViewWidget::QFTextViewWidget(QWidget *parent)
	: QFDialogWidget(parent)
{
	setCodecName("System");

	QFPixmapCache::insert("icon.libqf.print", ":/libqfgui/images/printer.png");
	QFPixmapCache::insert("icon.libqf.reload", ":/libqfgui/images/reload.png");
	QFPixmapCache::insert("icon.libqf.save", ":/libqfgui/images/save.png");

	ui = new Ui::QFTextViewWidget;
	ui->setupUi(centralWidget());
	Qf::connectSlotsByName(centralWidget(), this);

	createActions();
	//createToolBar();
	QFUiBuilder::connectActions(actionList(), this);
}

QFTextViewWidget::~QFTextViewWidget()
{
	//qfTrash() << QF_FUNC_NAME;
	SAFE_DELETE(ui);
}

void QFTextViewWidget::setText(const QString &doc, const QString &suggested_file_name, const QString &codec_name)
{
	setCodecName(codec_name);
	suggestedFileName = suggested_file_name;
	ui->editor->setPlainText(doc);
	//qfInfo() << "set:" << ui->editor->toPlainText();
}

QString QFTextViewWidget::text()
{
	QString ret = ui->editor->toPlainText();
	//qfInfo() << "get:" << ret;
	return ret;
}

void QFTextViewWidget::setFile(const QString &file_name, const QString &codec_name)
{
	suggestedFileName = file_name;
	QFile f(fileName());
	setUrl(f, codec_name);
}

void QFTextViewWidget::setUrl(QFile &url, const QString &codec_name)
{
	setCodecName(codec_name);
	QFile &f = url;
	suggestedFileName = f.fileName();
	if(!f.open(QIODevice::ReadOnly)) {
		QFMessage::information(this, tr("Soubor %1 nelze otevrit pro cteni.").arg(f.fileName()));
		return;
	}
	QTextCodec *tc = QTextCodec::codecForName(codecName().toAscii());
	if(!tc) {
		QFMessage::error(this, trUtf8("Nelze nahrát kodek pro kódování %1.").arg(codecName()));
		return;
	}
	QTextStream ts(&f);
	ts.setCodec(tc);
	QString s = ts.readAll();
	ui->editor->setPlainText(s);
}

void QFTextViewWidget::createActions()
{
	QFPart::ActionList &actlst = actionListRef();
	QFAction *a;
	QString s;
/*
	s = "reload";
	a = new QFAction(QFPixmapCache::icon("icon.libqf." + s), tr("Obnovit"), this);
	a->setId(s);
	a->setToolTip(tr("Obnovit"));
	actlst << a;
	//toolBarActions.append(a);
*/
	s = "accept";
	a = new QFAction(QFPixmapCache::icon("icon.libqf." + s, ":/libqfgui/images/ok.png"), tr("Ok"), this);
	a->setId(s);
	a->setToolTip(tr("Ok"));
	a->setShortcut(QKeySequence("Ctrl+S"));
	actlst << a;
	toolBarActions.append(a);
	connect(a, SIGNAL(triggered()), this, SIGNAL(acceptRequest()));

	a = new QFAction(this);
	a->setSeparator(true);
	toolBarActions.append(a);

	s = "print";
	a = new QFAction(QFPixmapCache::icon("icon.libqf." + s), tr("Tisk"), this);
	a->setId(s);
	a->setToolTip(tr("Tisk"));
	actlst << a;
	toolBarActions.append(a);

	s = "save";
	a = new QFAction(QFPixmapCache::icon("icon.libqf." + s), trUtf8("Uložit"), this);
	a->setId(s);
	a->setToolTip(tr("Ulozit dokument"));
	actlst << a;
	toolBarActions.append(a);

	s = "saveAs";
	a = new QFAction(QFPixmapCache::icon("icon.libqf.saveas"), trUtf8("Uložit jako"), this);
	a->setId(s);
	a->setToolTip(tr("Ulozit dokument pod jinym jmenem"));
	actlst << a;
	toolBarActions.append(a);
	
	s = "wordWrap";
	a = new QFAction(QFPixmapCache::icon("icon.libqf." + s, ":/libqfgui/images/wordwrap.png"), tr("Zalomit radky"), this);
	a->setCheckable(true);
	a->setId(s);
	a->setToolTip(tr("Zalomit radky"));
	actlst << a;
	toolBarActions.append(a);

}

void QFTextViewWidget::createToolBar()
{
	QToolBar *tb = new QToolBar(NULL);
	tb->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	QBoxLayout *l = qobject_cast<QBoxLayout*>(layout());
	l->setMargin(0);
	if(l) l->insertWidget(0, tb);
	foreach(QFAction *a, toolBarActions) tb->addAction(a);
}

void QFTextViewWidget::save()
{
	save_helper(fileName());
}

void QFTextViewWidget::saveAs()
{
	QFString fn = QFFileUtils::file(fileName());
	QString filter = tr("textove soubory (*.txt)");
	QString ext = QFFileUtils::extension(fn);
	if(!ext.isEmpty() && ext != "txt") {
		filter = "(*.%1);;" + filter;
		filter = filter.arg(ext);
	}
	else if(ext.isEmpty()) {
		fn += ".txt";
	}
	fn = qfApp()->getSaveFileName(this, trUtf8("Uložit jako ..."), fn, filter);
	if(!fn) return;
	save_helper(fn);
}

void QFTextViewWidget::save_helper(const QString &file_name)
{
	qfLogFuncFrame() << file_name;
	QFString fn = file_name;
	qfTrash() << "saving to file" << fn;
	QFile f(fn);
	if(!f.open(QIODevice::WriteOnly)) {
		QString s = tr("Nelze otevrit soubor %1 pro zapis.").arg(fn);
		QFMessage::information(this, s);
		return;
	}
	//QString codec_name = codecName();
	//if(codec_name.isEmpty()) codec_name = "utf8";
	qfTrash() << "\tcodecName:" << codecName();
	QTextCodec *tc = QTextCodec::codecForName(codecName().toAscii());
	if(!tc) {
		QFMessage::error(this, trUtf8("Nelze nahrát kodek pro kódování %1.").arg(codecName()));
		return;
	}
	QTextStream ts(&f);
	ts.setCodec(tc);
	ts << text();
	//f.write(htmlTextOriginal.toUtf8());
	//f.write(text().toUtf8());
}

void QFTextViewWidget::print()
{
	QPrinter printer(QPrinter::HighResolution);
	printer.setFullPage(true);

	QPrintDialog *dlg = new QPrintDialog(&printer, this);
	if (dlg->exec() == QDialog::Accepted) {
		ui->editor->document()->print(&printer);
	}
	delete dlg;
}

QTextEdit* QFTextViewWidget::editor()
{
	return ui->editor;
}

void QFTextViewWidget::wordWrap(const QString &action_id, bool checked)
{
	Q_UNUSED(action_id);
	if(checked) ui->editor->setLineWrapMode(QTextEdit::WidgetWidth);
	else ui->editor->setLineWrapMode(QTextEdit::NoWrap);
	qfTrash() << QF_FUNC_NAME << "checked" << checked << "new mode" << ui->editor->lineWrapMode();
}

QFPart::ToolBarList QFTextViewWidget::createToolBars()
{
	QFPart::ToolBarList ret;
	QFToolBar *t = new QFToolBar(NULL);
	t->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	foreach(QFAction *a, toolBarActions) t->addAction(a);
	ret << t;
	return ret;
}

