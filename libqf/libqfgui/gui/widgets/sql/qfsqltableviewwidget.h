
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLTABLEVIEWWIDGET_H
#define QFSQLTABLEVIEWWIDGET_H

#include "qfsqlcontrol.h"

#include <qfguiglobal.h>
#include <qfclassfield.h>

#include <qftableviewwidget.h>

//! @todo write doc.
class QFGUI_DECL_EXPORT QFSqlTableViewWidget : public QFTableViewWidget, public QFSqlControl
{
	Q_OBJECT
	Q_PROPERTY(QString sqlId READ sqlId WRITE setSqlId);
	Q_PROPERTY(bool dataReadOnly READ isDataReadOnly WRITE setDataReadOnly);
	Q_PROPERTY(QString editGrant READ editGrant WRITE setEditGrant);
	Q_PROPERTY(bool ignoreClosedForEdits READ ignoreClosedForEdits WRITE setIgnoreClosedForEdits);
	/// SELECT * FROM smlparpol WHERE smlouvaID=${ID}, v tomto pripade je sqlIdPlaceHolder == "${ID}",
	/// coz je take defaultni hodnota.
	Q_PROPERTY(QString query READ query WRITE setQuery);
	//QF_FIELD_RW(QString, q, setQ,  uery);
	Q_PROPERTY(QString sqlIdPlaceHolder READ sqlIdPlaceHolder WRITE setSqlIdPlaceHolder);
	protected:
		virtual void setWidgetReadOnly(bool b);
	public slots:
		virtual void loadValue(const QString &sql_id = QString());
		virtual void flushValue() {}
	signals:
		void valueUpdated(const QString &sql_id, const QVariant &val);
	public:
		virtual QString sqlId() const;
		virtual void setSqlId(const QString &sql_id);
		QString sqlIdPlaceHolder() const;
		void setSqlIdPlaceHolder(const QString &s);
		QString query() const;
		void setQuery(const QString &s);
	public:
		QFSqlTableViewWidget(QWidget *parent = NULL, const TableViewFactory &factory = TableViewFactory());
		virtual ~QFSqlTableViewWidget();
};

#endif // QFSQLTABLEVIEWWIDGET_H

