
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqltableviewwidget.h"

#include <qfsqlquerybuilder.h>
#include <qfsqltableview.h>
#include <qfdataformdocument.h>

#include <qflogcust.h>
/*
QFSqlTableViewWidget::QFSqlTableViewWidget(const TableViewFactory &factory, QWidget *parent)
	: QFTableViewWidget(factory, parent), QFSqlControl(this)
{
	setSqlIdPlaceHolder("${ID}");
}
*/
QFSqlTableViewWidget::QFSqlTableViewWidget(QWidget * parent, const TableViewFactory & factory)
	: QFTableViewWidget(parent, factory), QFSqlControl(this)
{
}

QFSqlTableViewWidget::~QFSqlTableViewWidget()
{
}

void QFSqlTableViewWidget::loadValue(const QString &sql_id)
{
	QFSqlTableView *view = qobject_cast<QFSqlTableView*>(tableView());
	if(view) view->loadValue(sql_id);
/*
	if(!sql_id.isEmpty() && !sqlIdCmp(sql_id)) return;
	QFDataFormDocument *doc = document();
	if(!doc) return;
	qfTrash() << QF_FUNC_NAME << "sql id:" << sqlId() << "object name:" << objectName();
	QFSqlQueryTableModel *m = qobject_cast<QFSqlQueryTableModel*>(tableView()->model(!Qf::ThrowExc));
	if(!m) {
		m = new QFSqlQueryTableModel(this);
		tableView()->setModel(m);
	}
	QFSqlQueryTable *t = dynamic_cast<QFSqlQueryTable*>(tableView()->model()->table());
	QF_ASSERT(t, "table is not a QFSqlQueryTable");
	QVariant v = doc->value(sqlId());
	/// pravdepodobne jsem v insertu a id jeste nema hodnotu
	if(v.type() == QVariant::Invalid) v = -1;
	//qfTrash() << "\tvalue type:" << v.type();
	QString qs = query();
	if(qs.isEmpty()) {
		/// pokud neni query(), vezmi ji z tabulky
		qs = t->queryBuilder().toString();
		setQuery(qs);
	}
	qs.replace(sqlIdPlaceHolder(), v.toString());
	t->setQuery(qs);
	QFSqlControl::loadValue(sql_id);
	
	tableView()->reload();
	*/
}

void QFSqlTableViewWidget::setWidgetReadOnly(bool b)
{
	qfTrash() << QF_FUNC_NAME << sqlId() << b;
	QFSqlTableView *view = qobject_cast<QFSqlTableView*>(tableView());
	if(view) view->setDataReadOnly(b);
	//setEnabled(!b);
}

QString QFSqlTableViewWidget::sqlIdPlaceHolder() const
{
	QFSqlTableView *view = qobject_cast<QFSqlTableView*>(tableView());
	if(view) return view->sqlIdPlaceHolder();
	return QString();
}

void QFSqlTableViewWidget::setSqlIdPlaceHolder(const QString & s)
{
	QFSqlTableView *view = qobject_cast<QFSqlTableView*>(tableView());
	if(view) view->setSqlIdPlaceHolder(s);
}

QString QFSqlTableViewWidget::sqlId() const
{
	QFSqlTableView *view = qobject_cast<QFSqlTableView*>(tableView());
	if(view) return view->sqlId();
	return QString();
}

void QFSqlTableViewWidget::setSqlId(const QString & sql_id)
{
	QFSqlTableView *view = qobject_cast<QFSqlTableView*>(tableView());
	if(view) view->setSqlId(sql_id);
}

QString QFSqlTableViewWidget::query() const
{
	QFSqlTableView *view = qobject_cast<QFSqlTableView*>(tableView());
	if(view) return view->query();
	return QString();
}

void QFSqlTableViewWidget::setQuery(const QString & s)
{
	QFSqlTableView *view = qobject_cast<QFSqlTableView*>(tableView());
	if(view) view->setQuery(s);
}

