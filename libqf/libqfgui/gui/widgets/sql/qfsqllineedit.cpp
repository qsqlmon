
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqllineedit.h"

#include <qfdbapplication.h>
#include <qfsql.h>
#include <qfsqlquery.h>
#include <qfdataformdocument.h>
#include <qfdataformwidget.h>

#include <qflogcust.h>

//===================================================
//                  QFSqlLineEdit
//===================================================
QFSqlLineEdit::QFSqlLineEdit(QWidget *parent)
	: QLineEdit(parent), QFSqlControl(this)
{
	//designedStyleSheet = "UNASSIGNED";
	connect(this, SIGNAL(editingFinished()), this, SLOT(slotEditingFinished()));
	connect(this, SIGNAL(textChanged(const QString&)), this, SLOT(updateStyleSheet()));
}

QFSqlLineEdit::~QFSqlLineEdit()
{
}

void QFSqlLineEdit::loadValue(const QString &sql_id)
{
	if(!checkSqlId(sql_id)) return;
	qfLogFuncFrame();
	QFDataFormDocument *doc = document();
	if(!doc) return;
	loadingState = true;
	QString s = doc->value(sqlId()).toString();
	setText(s);
	updateStyleSheet();
	loadingState = false;
	QFSqlControl::loadValue(sql_id);
}

void QFSqlLineEdit::setWidgetReadOnly(bool b)
{
	qfLogFuncFrame() << sqlId() << b;
	qfTrash().color(QFLog::Green) << sqlId();
	//QFile f("/home/fanda/t/err.log");
	//f.open(QFile::Append);
	//f.write(QFLog::stackTrace().toAscii());
	setReadOnly(b);
	updateStyleSheet();
}

void QFSqlLineEdit::updateStyleSheet()
{
	qfLogFuncFrame() << sqlId() << "isMandatory:" << isMandatory() << "isReadOnly:" << isReadOnly() << "isEmpty:" << text().isEmpty();
	if(!designedStyleSheet.isValid()) {
		QString ss = styleSheet().simplified();
		qfTrash() << "\t setting designedStyleSheet to" << ss;
		//qfInfo() << "DEFINE:" << ss;
		designedStyleSheet = ss;
	}
	QString new_ss = designedStyleSheet.toString();
	//qfInfo() << "old_ss:" << new_ss;
	if(isMandatory()) {
		//qfInfo() << sqlId() << "isMandatory:" << isMandatory() << "isReadOnly:" << isReadOnly() << "isEmpty:" << text().isEmpty();
		if(!isReadOnly() && text().isEmpty()) {
			new_ss = designedStyleSheet.toString() + ";background:" + QFDataFormWidget::MandatoryWidgetBackgroundColor;
		}
	}
	//qfInfo() << "new_ss:" << new_ss;
	setStyleSheet(new_ss);
}

void QFSqlLineEdit::flushValue()
{
	qfTrash() << QF_FUNC_NAME;
	if(!loadingState) {
		QFDataFormDocument *doc = document();
		if(!doc || doc->isEmpty()) return;
		emit valueUpdated(sqlId(), QVariant(text()));
	}
}

void QFSqlLineEdit::slotEditingFinished()
{
	qfTrash() << QF_FUNC_NAME;
	updateStyleSheet();
	if(!isReadOnly()) flushValue();
}

//===================================================
//                  QFSqlForeignKeyLineEdit
//===================================================
QFSqlForeignKeyLineEdit::QFSqlForeignKeyLineEdit(QWidget * parent)
	: QFSqlLineEdit(parent)
{
}

const QFStringMap & QFSqlForeignKeyLineEdit::captionMap()
{
	QFDbApplication *db_app = qobject_cast<QFDbApplication*>(QApplication::instance());
	if(db_app && f_captionMap.isEmpty()) {
		qfTrash() << QF_FUNC_NAME << objectName();
		QFString tblname, fldname, capname;
		QFSql::parseFullName(referencedField(), &fldname, &tblname);
		capname = captionField();
		if(!capname) capname = fldname;
		qfTrash() << "\t capname:" << capname;
		QFString query_str = query();
		if(!query_str) {
			QFSqlQueryBuilder b;
			b.select(fldname);
			if(capname != fldname) b.select(capname);
			b.from(tblname);
			b.orderBy(capname);
			query_str = b.toString();
		}
		/*
		else {
			query_str = query_str.replace(referencedFieldPlaceHolder, fldname);
			query_str = query_str.replace(referencedTablePlaceHolder, tblname);
			query_str = query_str.replace(captionFieldPlaceHolder, capname);
		}
		*/
		QFSqlQuery q(db_app->connection());
		//checkLoadItemsQuery(b);
		qfTrash() << "\t" << query_str;
		qfTrash() << "\t capname:" << capname;
		q.exec(query_str);
		while(q.next()) {
			QString caption = q.value(capname).toString();
			QString id = q.value(fldname).toString();
			qfTrash() << "\t adding :" << caption << id;
			f_captionMap[id] = caption;
		}
	}
	return f_captionMap; 
}

QString QFSqlForeignKeyLineEdit::idToCaption(const QVariant & fk_id)
{
	QString key = fk_id.toString();
	if(captionMap().contains(key)) return captionMap().value(key);
	return unknownIdCaption();
}

void QFSqlForeignKeyLineEdit::clearCache()
{
	f_captionMap.clear();
}

void QFSqlForeignKeyLineEdit::loadValue(const QString & sql_id)
{
	if(!sql_id.isEmpty() && !sqlIdCmp(sql_id)) return;
	qfLogFuncFrame();
	QFDataFormDocument *doc = document();
	if(!doc) return;
	loadingState = true;
	setText(idToCaption(doc->value(sqlId())));
	loadingState = false;
	QFSqlControl::loadValue(sql_id);
}

void QFSqlForeignKeyLineEdit::flushValue()
{
	qfTrash() << QF_FUNC_NAME;
	//if(!loadingState) emit valueUpdated(sqlId(), QVariant(text()));
}





