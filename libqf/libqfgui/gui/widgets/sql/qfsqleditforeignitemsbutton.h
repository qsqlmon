
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLEDITFOREIGNITEMSBUTTON_H
#define QFSQLEDITFOREIGNITEMSBUTTON_H

#include <qfguiglobal.h>

#include <QToolButton>
#include <QPointer>

class QFSqlForeignKeyCombo;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlEditForeignItemsButton : public QToolButton
{
	Q_OBJECT;
	protected:
		//QPointer<QFSqlForeignKeyCombo> f_editItemsButton;
	public:
		QFSqlEditForeignItemsButton(QWidget *parent = NULL);
		//virtual ~QFSqlEditForeignItemsButton();
};

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlClearValueButton : public QToolButton
{
	Q_OBJECT;
	protected:
		//QPointer<QFSqlForeignKeyCombo> f_editItemsButton;
	public:
		QFSqlClearValueButton(QWidget *parent = NULL);
		//virtual ~QFSqlEditClearValueButton();
};

#endif // QFSQLEDITFOREIGNITEMSBUTTON_H

