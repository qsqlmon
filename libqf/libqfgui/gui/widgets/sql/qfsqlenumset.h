
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLENUMSET_H
#define QFSQLENUMSET_H

#include "qfsqlcontrol.h"
#include "qfsqlforeignkeycontrolitemseditor.h"

#include <qfguiglobal.h>

#include <QListWidget>

class QFSqlQueryBuilder;
//class QFSqlForeignKeyControlItemsEditor;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlForeignSetBase : public QListWidget, public QFSqlControl
{
	Q_OBJECT;
	Q_PROPERTY(QString sqlId READ sqlId WRITE setSqlId);
	/// ve tvaru "tablename.fieldname"
	Q_PROPERTY(QString referencedField READ referencedField WRITE setReferencedField);
	QF_FIELD_RW(QString, r, setR, eferencedField);
	/// pokud je vyplneno, je to jmeno fieldu z foreign table, ktery se zobracuje
	Q_PROPERTY(QString captionField READ captionField WRITE setCaptionField);
	QF_FIELD_RW(QString, c, setC, aptionField);
	Q_PROPERTY(bool readOnly READ isReadOnly WRITE setReadOnly);
	Q_PROPERTY(bool dataReadOnly READ isDataReadOnly WRITE setDataReadOnly);
	Q_PROPERTY(QString editGrant READ editGrant WRITE setEditGrant);
	Q_PROPERTY(bool ignoreClosedForEdits READ ignoreClosedForEdits WRITE setIgnoreClosedForEdits);
	Q_PROPERTY(bool valuesRestrictedToItems READ isValuesRestrictedToItems WRITE setValuesRestrictedToItems);
	QF_FIELD_RW(bool, is, set, ValuesRestrictedToItems);
	Q_PROPERTY(QString editItemsGrant READ editItemsGrant WRITE setEditItemsGrant);
	QF_FIELD_RW(QString, e, setE, ditItemsGrant);
	//! urcuje, jak se sklada caption ze sloupcu v tabulce enumz, placeholder je ${column_name}
	Q_PROPERTY(QString itemCaptionFormat READ itemCaptionFormat WRITE setItemCaptionFormat);
	QF_FIELD_RW(QString, i, setI, temCaptionFormat);
	Q_PROPERTY(QString nonItemValueCaption READ nonItemValueCaption WRITE setNonItemValueCaption);
	QF_FIELD_RW(QString, n, setN, onItemValueCaption);
	public:
		static const QString referencedFieldPlaceHolder, captionFieldPlaceHolder, referencedTablePlaceHolder;
	public:
		class Item;
	protected slots:
		void on_itemChanged(QListWidgetItem *item);
		void contextMenuRequest(const QPoint &point);
		virtual void showItemsEditor() = 0;
		QVariant currentValue();
		void setCurrentValue(const QVariant &v);
	public slots:
		virtual void loadValue(const QString &sql_id = QString());
		virtual void flushValue();
	signals:
		void valueUpdated(const QString &sql_id, const QVariant &val);
	protected:
		bool f_readOnly;
		bool itemsLoaded;
	protected:
		// aby se mi z flushValue hned nevolalo loadValue()
		//bool ignoreLoadValue;

		virtual void setWidgetReadOnly(bool b);
		virtual void loadItems() = 0;
		virtual void reloadItems();
		// potomek muze jeste opravit, co ne nahraje do items.
		//virtual void checkLoadItemsQuery(QFSqlQueryBuilder &b) {Q_UNUSED(b);}
		QString encodeNonItemText(const QString &s);
		QString decodeNonItemText(const QString &s);
	public:
		Item* item(int ix);

		bool isReadOnly() const {return f_readOnly;}
		void setReadOnly(bool ro);
	public:
		QFSqlForeignSetBase(QWidget *parent = NULL);
		virtual ~QFSqlForeignSetBase();
};

class QFGUI_DECL_EXPORT QFSqlForeignSetBase::Item : public QListWidgetItem
{
	//QF_FIELD_RW(QVariant, u, setU, serData);
	public:
		void setUserData(const QVariant &user_data) { setData(Qt::UserRole, user_data); }
		QVariant userData() const { return data(Qt::UserRole); }
	public:
		Item(const QString &text, const QVariant &user_data, QListWidget * parent = 0)
	: QListWidgetItem(text, parent)//, f_userData(user_data)
		{
			setUserData(user_data);
		}
		virtual ~Item() {}
};

//! List view, kde se vytvori radek pro kazdou polozku z \a enumGroup v tabulce enums .
class QFGUI_DECL_EXPORT QFSqlEnumSet : public QFSqlForeignSetBase
{
	Q_OBJECT;
	Q_PROPERTY(QString enumGroup READ enumGroup WRITE setEnumGroup);
	QF_FIELD_RW(QString, e, setE, numGroup);
	protected:
		virtual void loadItems();
	public:
		virtual void showItemsEditor();
	public:
		QFSqlEnumSet(QWidget *parent = NULL);
		virtual ~QFSqlEnumSet() {}
};

//! List view, kde se vytvori radek pro kazdou polozku z \a referencedField .
class QFGUI_DECL_EXPORT QFSqlForeignKeySet : public QFSqlForeignSetBase
{
	Q_OBJECT;
	/// pokud je prazdne, vytvori se automaticky,
	/// jinak pouziva placeholders napr. SELECT ${referencedField}, ${captionField} FROM ${referencedTable} WHERE isUserGroup != 0
	Q_PROPERTY(QString query READ query WRITE setQuery);
	QF_FIELD_RW(QString, q, setQ, uery);
	/// pokud je key prazdny, nic se nekesuje
	Q_PROPERTY(QString cachedItemsKey READ cachedItemsKey WRITE setCachedItemsKey);
	QF_FIELD_RW(QString, c, setC, achedItemsKey);
	protected:
		struct CachedItem {
			QString caption;
			QVariant id;

			CachedItem(const QString &cap = QString(), const QVariant & _id = QVariant()) : caption(cap), id(_id) {}
		};
		typedef QList<CachedItem> CachedItemList;
		static QMap<QString, CachedItemList> itemCache;
		
		QPointer<QFSqlForeignKeyControlItemsEditor> f_itemsEditor;
	public:
		virtual void loadItems();
		virtual void showItemsEditor();

		static void clearCachedItems(const QString &key) {itemCache.remove(key);}
		
		virtual QFSqlForeignKeyControlItemsEditor* itemsEditor() {return f_itemsEditor;}
		void setItemsEditor(QFSqlForeignKeyControlItemsEditor *dlg) { f_itemsEditor = dlg; }
	public:
		QFSqlForeignKeySet(QWidget *parent = NULL);
		virtual ~QFSqlForeignKeySet() {}
};

#endif // QFSQLENUMSET_H

