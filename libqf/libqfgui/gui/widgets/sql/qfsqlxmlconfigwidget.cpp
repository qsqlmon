
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqlxmlconfigwidget.h"

#include <qfapplication.h>
#include <qfaction.h>
#include <qfdataformdocument.h>
#include <qfdbxmlconfig.h>

#include <typeinfo>

#include <qflogcust.h>

QFSqlXmlConfigWidget::QFSqlXmlConfigWidget(QWidget *parent)
	: QFXmlConfigWidget(parent), QFSqlControl(this), f_createdDbConfig(NULL)
{
	qfLogFuncFrame();
	setSqlId("dbxml");
	setIgnoreNotFoundDbxmlKeys(false);

	QFUiBuilder::ActionList alst;
	QFAction *a;

	a = new QFAction(tr("Defaults"), this);
	a->setId("defaults");
	alst << a;

	QFUiBuilder::connectActions(alst, this);
	f_actions += alst;
}

QFSqlXmlConfigWidget::~QFSqlXmlConfigWidget()
{
	qfLogFuncFrame();
	//flushValue(); tady nemuze bej flush value, protoze je to virtualni funkce a nezavola se ta nejvyssi
	SAFE_DELETE(f_createdDbConfig);
}

QFXmlConfig* QFSqlXmlConfigWidget::dbConfig()
{
	qfLogFuncFrame() << "created:" << f_createdDbConfig << "f_config:" << f_config;// << dynamic_cast<QFDbXmlConfig*>(f_config) << &(typeid(*f_config)) << typeid(*f_config).name();
	if(!f_config) {
		/// tohle mi z nejakyho duvodu nefunguje s gcc 4.4.1, i kdyz je f_config typu QFDbXmlConfig* vrati to NULL
		/// nevim, kde je chyba, snad tim, ze f_config byl vytvoren v jinym modulu, nez kde se castuje, v zivote jsem se s tim nesetkal, ma duvera v C++ je otresena
		///QFDbXmlConfig *db_config = dynamic_cast<QFDbXmlConfig*>(f_config);
		//qfTrash() << "\t dynamic_cast<QFDbXmlConfig*>(f_config):" << db_config << typeid(*f_config).name() << "==" << typeid(QFDbXmlConfig).name();
		{
			//SAFE_DELETE(f_createdDbConfig);
			qfTrash() << "\tEMPTY";
			f_createdDbConfig = new QFDbXmlConfig();
			f_createdDbConfig->setCrypter(f_crypter);
			f_config = f_createdDbConfig;
			//return f_createdDbConfig;
		}
	}
	if(f_createdDbConfig && f_createdDbConfig->templateDocument().isEmpty()) {
		f_createdDbConfig->loadTemplateDocument(dbxmlKey(), !isIgnoreNotFoundDbxmlKeys());
	}

	//QFDbXmlDocument *doc = dynamic_cast<QFDbXmlDocument*>(&fContent);
	//if(!doc) QF_THROW("document is not a QFDbXmlDocument");
	return f_config;
}
/*
QFDbXmlDocument* content() const throw(QFException)
{
	QFDbXmlDocument *doc = dynamic_cast<QFDbXmlDocument*>(QFXmlConfigDocument::content());
	if(!doc) QF_EXCEPTION("document is NULL or not a QFDbXmlDocument");
	return doc;
}
	*/
void QFSqlXmlConfigWidget::loadValue(const QString &sql_id)
{
	if(!checkSqlId(sql_id)) return;
	qfLogFuncFrame() << "sql_id:" << sql_id;
	qfTrash().color(QFLog::Blue) << "sqlId:" << sqlId();
	QFSqlControl::loadValue(sql_id); /// tohle musi bejt prvni, jinak mi prestane fungovat setDataReadOnly()
	QFDataFormDocument *doc = document();
	if(!doc) return;
	QString s = doc->value(sqlId()).toString();
	/// QFSqlXmlConfigWidget loading state nepouziva, protoze je vudci nekonecne smycce setValue->loadValue zrovna imunni
	//loadingState = true;
	loadValueString(s);
	//loadingState = false;
	/// je to dulezity, jinak se v modu insert neulozi dbxml, pokud ho nikdo zrovna nepoedituje
	/// protoze je ve flushValue kontrola na data dirty, nezacykli se to
	/// dirty flag nastavi funkce saveCurrentlyEdited spravne, i v pripade, kdy uzivatel nic nepoklikal
	flushValue();
}

void QFSqlXmlConfigWidget::loadValueString(const QString &xml_str)
{
	qfLogFuncFrame() << "\t xml_str:" << xml_str;
	//deleteCurrentConfigWidgets();
	if(dbConfig() && !dbConfig()->isTemplateEmpty()) {
		//qfInfo() << dbConfig()->dataDocument().toString();
		qfTrash() << "\t SHOW";
	    show();
		//QString current_xml_str = dbConfig()->dataDocument().toString();
		//qfTrash() << "\t current_xml_str:" << current_xml_str;
		if(!f_currentlyFlushedXmlData.isEmpty() && xml_str == f_currentlyFlushedXmlData && !dbConfig()->dataDocument().isEmpty()) {
			/// pokud jsou data a ta nahravana jsou stejna jako ta predesle nahravana
			/// nedelej nic, widget se snazi reloadnout hodnotu, kterou prave flushnul
		}
		else {
			//f_currentXmlData = xml_str;
			dbConfig()->setDataDocument(xml_str);
			QFDataFormDocument *doc = document();
			if(doc) {
				QFString s = rootPathSqlId();
				qfTrash() << "\t rootPathSqlId:" << rootPathSqlId();
				qfTrash() << "\t rootPath:" << s;
				if(!!s) s = doc->value(s).toString();
				if(!!s) {
					setRootPath(s);
				}
			}
			/// zobraz nahrana data
			resetTree();
		}
	}
	else {
		qfTrash() << "\t HIDE";
		hide();
	}
}

void QFSqlXmlConfigWidget::flushValue()
{
	qfLogFuncFrame();
	if(!dbConfig()) return;
	QFDataFormDocument *doc = document();
	if(!doc) return;
	if(doc->isEmpty()) return;
	/// uloz soucasnou editaci, pokud nejaka je
	saveCurrentlyEdited();
	//qfInfo() << "FLUSH VALUE";
	qfTrash() << "\t data dirty:" << dbConfig()->isDataDirty() << "loadingState:" << loadingState;
	if(dbConfig()->isDataDirty()) {
		dbConfig()->dataLint();
		QString s = dbConfig()->dataDocument().toString();
		qfTrash().color(QFLog::Yellow) << "\tsaving" << s;
		//if(!loadingState) {
			//qfInfo() << "\tEMIT valueUpdated sqlId:" << sqlId() << "data:" << s;
			f_currentlyFlushedXmlData = s;
			emit valueUpdated(sqlId(), s);
		//}
		dbConfig()->setDataDirty(false);
	}
}

void QFSqlXmlConfigWidget::setRootPath(const QString &s)
{
	qfLogFuncFrame() << s;
	/// load definition
	if(!dbConfig()) return;
	QFXmlConfigWidget::setRootPath(s);
}

void QFSqlXmlConfigWidget::setDbxmlKey(const QString &key)
{
	f_dbxmlKey = key;
	if(f_config) f_config->clearTemplate();
}

