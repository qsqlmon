
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#include "ui_qfsqlenumitemseditor.h"
#include "qfsqlenumitemseditor.h"
#include "qfsqlenumitemseditoritemeditor.h"

#include <qfdbapplication.h>
#include <qfdlgexception.h>

#include <qflogcust.h>

//=================================================
//                                    QFSqlEnumItemsEditor
//=================================================
QFSqlEnumItemsEditor::QFSqlEnumItemsEditor(QString group_name, QWidget *parent)
	: QFDialogWidget(parent), groupName(group_name)
{
	ui = new Ui::QFSqlEnumItemsEditor;
	ui->setupUi(centralWidget());
	Qf::connectSlotsByName(centralWidget(), this);

	connect(ui->tblItems, SIGNAL(doubleClicked(const QModelIndex &)), this, SLOT(on_btEditItem_clicked()));

	model = new Model(this);
	model->setHorizontalHeaderLabels(QStringList() << tr("id") << tr("groupId") << trUtf8("název"));
	ui->tblItems->setModel(model);
}

QFSqlEnumItemsEditor::~QFSqlEnumItemsEditor()
{
	delete ui;
}

void QFSqlEnumItemsEditor::load()
{
	//if(!controlWidget) return;
	//QString group_name = controlWidget->property("groupName");
	int row = 0;
	foreach(const QFDbEnum &de, qfDbApp()->dbEnumsForGroup(groupName)) {
		QStandardItem *it;
		it = new QStandardItem(de.id());
		model->setItem(row, 0, it);
		it = new QStandardItem(de.groupId());
		model->setItem(row, 1, it);
		it = new QStandardItem(de.caption());
		it->setData(de.userText(), UserTextRole);
		model->setItem(row, 2, it);
		row++;
	}
	ui->tblItems->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
}

void QFSqlEnumItemsEditor::save()
{
	QString qs = "INSERT INTO enumz (groupName, groupId, caption, pos, userText) VALUES ('%1', '%2', '${caption}', ${pos}, '${user_text}') "
			"ON DUPLICATE KEY UPDATE caption='${caption}', pos=${pos}, userText='${user_text}'";
	try {
		QFSqlQuery q(qfDbApp()->connection());
		for(int i=0; i<model->rowCount(); i++) {
			QStandardItem *it;
			it = model->item(i, 1);
			QString group_id;
			if(it) group_id = it->text();
			it = model->item(i, 2);
			QString caption;
			QString user_text;
			if(it) {
				caption = it->text();
				user_text = it->data(UserTextRole).toString();
			}
			QString s = qs.arg(groupName, group_id).replace("${caption}", caption).replace("${pos}", QString::number(i)).replace("${user_text}", user_text);
			//qfInfo() << s;
			q.exec(s);
		}
	}
	catch(QFException &e) {
		QFDlgException::exec(this, e);
	}
}

int QFSqlEnumItemsEditor::selectedRow()
{
	int ret = -1;
	QModelIndexList lst = ui->tblItems->selectionModel()->selectedRows();
	if(lst.count()) ret = lst.value(0).row();
	return ret;
}

QFDbEnum QFSqlEnumItemsEditor::selectedEnum()
{
	QFDbEnum ret;
	int row = selectedRow();
	QStandardItem *it;
	it = model->item(row, 0);
	if(it) ret.setId(it->text().toInt());
	it = model->item(row, 1);
	if(it) ret.setGroupId(it->text());
	it = model->item(row, 2);
	if(it) {
		ret.setCaption(it->text());
		ret.setUserText(it->data(UserTextRole).toString());
	}
	ret.setGroupName(groupName);
	return ret;
}

QStringList QFSqlEnumItemsEditor::currentGroupIds()
{
	QStringList ret;
	for(int i=0; i<model->rowCount(); i++) {
		QStandardItem *it = model->item(i, 1);
		if(it) ret << it->text();
	}
	return ret;
}

void QFSqlEnumItemsEditor::on_btEditItem_clicked()
{
	QFDbEnum de = selectedEnum();
	if(de.isValid()) {
		QFSqlEnumItemsEditorItemEditor dlg(de, currentGroupIds(), this);
		if(dlg.exec()) {
			QFDbEnum de = dlg.dbEnum();
			int row = selectedRow();
			QStandardItem *it;
			it = model->item(row, 1);
			if(it) it->setText(de.groupId());
			it = model->item(row, 2);
			if(it) {
				it->setText(de.caption());
				it->setData(de.userText(), UserTextRole);
			}
		}
	}
}

void QFSqlEnumItemsEditor::on_btAddItem_clicked()
{
	QFDbEnum de;// = selectedEnum();
	de.setGroupName(groupName);
	if(de.isValid()) {
		QFSqlEnumItemsEditorItemEditor dlg(de, currentGroupIds(), this);
		if(dlg.exec()) {
			QFDbEnum de = dlg.dbEnum();
			int row = selectedRow();
			if(row < 0) row = model->rowCount();
			QList<QStandardItem*> lst;
			lst << new QStandardItem("0");
			lst << new QStandardItem(de.groupId());
			QStandardItem *it = new QStandardItem(de.caption());
			it->setData(de.userText(), UserTextRole);
			lst << it;
			model->insertRow(row, lst);
		}
	}
}

void QFSqlEnumItemsEditor::on_btMoveUpItem_clicked()
{
	int row = selectedRow();
	if(row < 1) return;
	QList<QStandardItem*> lst = model->takeRow(row);
	row--;
	model->insertRow(row, lst);
	ui->tblItems->selectRow(row);
}

void QFSqlEnumItemsEditor::on_btMoveDownItem_clicked()
{
	int row = selectedRow();
	if(row < 0 || row >= model->rowCount() - 1) return;
	QList<QStandardItem*> lst = model->takeRow(row);
	row++;
	model->insertRow(row, lst);
	ui->tblItems->selectRow(row);
}

