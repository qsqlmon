
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLFOREIGNKEYCOMBO_H
#define QFSQLFOREIGNKEYCOMBO_H 

#include "qfsqlforeignkeycontrolitemseditor.h"

#include <qfsqlcombobox.h>
#include <qfexception.h>
#include <qfbuttondialog.h>

#include <QPointer>

class QSortFilterProxyModel;
class QStandardItemModel;
class QFSqlEditForeignItemsButton;
class QFSqlClearValueButton;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlEnumComboStandardItemsEditor : public QFSqlForeignKeyControlItemsEditor
{
	Q_OBJECT;
	protected:
		QFTableView *f_view;
	protected:
		virtual void keyPressEvent(QKeyEvent *e);
	public:
		virtual void reload();
		virtual QFTableModel* tableModel();
		virtual QFTableView* tableView() {return f_view;}
		void setTableView(QFTableView *view);

		void setButtonsVisible(bool visible);
		QDialogButtonBox* dialogButtons();
	public:
		QFSqlEnumComboStandardItemsEditor(QWidget *parent = NULL, Qt::WFlags f = 0);
};
		
//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlForeignKeyComboBase : public QFSqlListBox
{
	Q_OBJECT;
	/// ve tvaru "tablename.fieldname"
	Q_PROPERTY(QString referencedField READ referencedField WRITE setReferencedField);
	QF_FIELD_RW(QString, r, setR, eferencedField);
	/// pokud je vyplneno, je to jmeno fieldu z foreign table, ktery se zobrazuje
	Q_PROPERTY(QString captionField READ captionField WRITE setCaptionField);
	//QF_FIELD_RW(QString, c, setC, aptionField);
	//! urcuje, jak se sklada caption ze sloupcu, placeholder je ${column_name}, property captionField ma prednost
	Q_PROPERTY(QString itemCaptionFormat READ itemCaptionFormat WRITE setItemCaptionFormat);
	QF_FIELD_RW(QString, i, setI, temCaptionFormat);
	Q_PROPERTY(QString editItemsGrant READ editItemsGrant WRITE setEditItemsGrant);
	QF_FIELD_RW(QString, e, setE, ditItemsGrant);
	public:
		static const QString referencedFieldPlaceHolder, captionFieldPlaceHolder, referencedTablePlaceHolder;
	protected:
		QPointer<QFSqlEditForeignItemsButton> f_editItemsButton;
		QPointer<QFSqlClearValueButton> f_clearValueButton;
		/// caption.toLower()->row_no v f_itemModel
		QMap<QString, int> itemCaptions;
		//QPointer<QFTableView> f_itemsEditorView;
		//QPointer<QFTableModel> f_itemsEditorModel;
		QSortFilterProxyModel *f_itemFilterProxyModel;
		QStandardItemModel *f_itemModel;
		QString f_captionField;
		bool itemsLoaded;
		bool textEdited;
		bool ignoreFocusOut; /// kdyz se zobrazuje popup neni treba flushnout widget na focusOutEvent
	protected slots:
		void onEditTextChanged();
		virtual void setEditItemsButton();
	signals:
		//void setEditItemsButtonRequest();
	protected:
		virtual void focusOutEvent(QFocusEvent *event);

		virtual void showPopup();
		virtual void hidePopup();
		QStandardItemModel *itemModel();
		QSortFilterProxyModel *itemFilterProxyModel();
	public:
		void setItemModel(QStandardItemModel *m);
		void setItemFilterProxyModel(QSortFilterProxyModel *m);
		//virtual QFDialog* itemsEditor();
		//virtual QFTableView* itemsEditorView();
		//void setItemsEditorView(QFTableView *view);
		//virtual QFTableModel* itemsEditorModel();
		void setCaptionField(const QString &s) {f_captionField = s;}
		QString captionField() const {return (f_captionField.isEmpty())? referencedField(): f_captionField;}
		
		virtual void removeItems() {clear();}
		virtual void loadItems() = 0;
		virtual void reloadItems();
		virtual void flushValue();
		virtual void setCurrentValue(const QVariant &val);
		virtual QVariant currentValue();

		virtual void setReadOnly(bool ro);
	public slots:
		virtual void showItemsEditor() = 0;
		virtual void clearValue();
	public:
		QFSqlForeignKeyComboBase(QWidget *parent = NULL);
		virtual ~QFSqlForeignKeyComboBase();
};
		
//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlForeignKeyCombo : public QFSqlForeignKeyComboBase
{
	Q_OBJECT;
	/// pokud je prazdne, vytvori se automaticky,
	/// jinak pouziva placeholders napr. SELECT ${referencedField}, ${captionField} FROM ${referencedTable} WHERE isUserGroup != 0
	Q_PROPERTY(QString query READ query WRITE setQuery);
	QF_FIELD_RW(QString, q, setQ, uery);
	/// pokud je key prazdny, nic se nekesuje
	Q_PROPERTY(QString cachedItemsKey READ cachedItemsKey WRITE setCachedItemsKey);
	QF_FIELD_RW(QString, c, setC, achedItemsKey);
	protected:
		struct CachedItem {
			QString caption;
			QVariant id;

			CachedItem(const QString &cap = QString(), const QVariant & _id = QVariant()) : caption(cap), id(_id) {}
		};
		typedef QList<CachedItem> CachedItemList;
		static QMap<QString, CachedItemList> itemCache;
		QPointer<QFSqlForeignKeyControlItemsEditor> f_itemsEditor;
	public:
		virtual void removeItems();
		virtual void loadItems();
		
		virtual QFSqlForeignKeyControlItemsEditor* itemsEditor() {return f_itemsEditor;}
		void setItemsEditor(QFSqlForeignKeyControlItemsEditor *dlg) {f_itemsEditor = dlg;}
		virtual void showItemsEditor();

		static void clearCachedItems(const QString &key) {itemCache.remove(key);}
	public:
		QFSqlForeignKeyCombo(QWidget *parent = NULL);
		//virtual ~QFSqlForeignKeyCombo();
};

class QFGUI_DECL_EXPORT QFSqlForeignKeyList : public QFSqlForeignKeyCombo
{
	Q_OBJECT;
	public:
		QFSqlForeignKeyList(QWidget *parent = NULL) : QFSqlForeignKeyCombo(parent) {setEditable(false);}
		//virtual ~QFSqlForeignKeyCombo();
};

#endif // QFSQLFOREIGNKEYCOMBO_H

