
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLXMLCONFIGWIDGET_H
#define QFSQLXMLCONFIGWIDGET_H

#include "qfsqlcontrol.h"

#include <qfguiglobal.h>
#include <qfxmlconfigwidget.h>

class QFDbXmlConfig;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlXmlConfigWidget : public QFXmlConfigWidget, public QFSqlControl
{
	Q_OBJECT;

	Q_PROPERTY(QString sqlId READ sqlId WRITE setSqlId);
	Q_PROPERTY(QString editGrant READ editGrant WRITE setEditGrant);
	Q_PROPERTY(bool ignoreClosedForEdits READ ignoreClosedForEdits WRITE setIgnoreClosedForEdits);

	Q_PROPERTY(QString rootPathSqlId READ rootPathSqlId WRITE setRootPathSqlId);
	/// nazev xml definice struktury v tabulce dbxml.
	Q_PROPERTY(QString dbxmlKey READ dbxmlKey WRITE setDbxmlKey);
	/// pokud je false, je vrzena vyjjimka pokud se nepodari najit dbxmlKey
	Q_PROPERTY(bool ignoreNotFoundDbxmlKeys READ isIgnoreNotFoundDbxmlKeys WRITE setIgnoreNotFoundDbxmlKeys);
	Q_PROPERTY(bool dataReadOnly READ isDataReadOnly WRITE setDataReadOnly);
	QF_FIELD_RW(QString, r, setR, ootPathSqlId);
	QF_FIELD_RW(bool, isI, setI, gnoreNotFoundDbxmlKeys);
	protected:
		// skryj tuto funkci, protoze config se nahrava z databaze
		//void setConfig(QFXmlConfig *doc);
		QString f_dbxmlKey;
		QString f_currentlyFlushedXmlData; 
	protected:
		QFCrypt f_crypter;
		QFDbXmlConfig *f_createdDbConfig;
	public:
		void setDbxmlKey(const QString &key);
		QString dbxmlKey() const {return f_dbxmlKey;}
		void setCrypter(const QFCrypt &crypter) {f_crypter = crypter;}
		virtual void setRootPath(const QString &s);

		void loadValueString(const QString &xml_str);
	public slots:
		virtual void loadValue(const QString &sql_id = QString());
		/// force widget to save its state to document.
		virtual void flushValue();
		//virtual void reset() {f_currentXmlData = QString(); loadValue();}
	signals:
		void valueUpdated(const QString &sql_id, const QVariant &val);
	protected:
		virtual void setWidgetReadOnly(bool b) {QFXmlConfigWidget::setReadOnly(b);}
	public:
		virtual QFXmlConfig* dbConfig();
		//const QFDbXmlDocument& sqlDoc() const;
		//void set(const QFDbXmlDocument &doc);
	public:
		QFSqlXmlConfigWidget(QWidget *parent = NULL);
		virtual ~QFSqlXmlConfigWidget();
};

#endif // QFSQLXMLCONFIGWIDGET_H

