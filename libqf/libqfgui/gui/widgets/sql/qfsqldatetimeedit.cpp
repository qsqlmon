
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqldatetimeedit.h"

#include <qfsql.h>
#include <qfdataformdocument.h>

#include <qflogcust.h>

QFSqlDateTimeEdit::QFSqlDateTimeEdit(QWidget *parent)
	: QDateTimeEdit(parent), QFSqlControl(this)
{
	setAlignment(Qt::AlignRight);
	setDisplayFormat(Qf::defaultDateTimeFormat());
	connect(this, SIGNAL(dateChanged(const QDate &)), this, SLOT(flushValue()));
}

QFSqlDateTimeEdit::~QFSqlDateTimeEdit()
{
}

void QFSqlDateTimeEdit::loadValue(const QString &sql_id)
{
	if(!checkSqlId(sql_id)) return;
	qfLogFuncFrame() << sqlId();
	QFDataFormDocument *doc = document();
	if(!doc) return;
	QVariant v = doc->value(sqlId());
	QDateTime dt = v.toDateTime();
	qfTrash() << "\t" << sqlId() << "new value:" << dt.toString();
	loadingState = true;
	setDateTime(dt);
	loadingState = false;
	QFSqlControl::loadValue(sql_id);
}

void QFSqlDateTimeEdit::flushValue()
{
	qfTrash() << QF_FUNC_NAME;
	if(!loadingState) {
		QFDataFormDocument *doc = document();
		if(!doc || doc->isEmpty()) return;
		emit valueUpdated(sqlId(), QVariant(dateTime()));
	}
}

void QFSqlDateTimeEdit::setWidgetReadOnly(bool b)
{
	setReadOnly(b);
}

void QFSqlDateTimeEdit::setDateTime(const QDateTime &dt)
{
	if(dt.isNull()) setNull();
	else QDateTimeEdit::setDateTime(dt);
}

QDateTime QFSqlDateTimeEdit::dateTime() const
{
	if(isNull()) return QDateTime();
	return QDateTimeEdit::dateTime();
}

QString QFSqlDateTimeEdit::textFromDateTime(const QDateTime & dt) const
{
	qfLogFuncFrame() << dt.toString();
	QString ret;
	if(!isNull()) {
		ret = QDateTimeEdit::textFromDateTime(dt);
	}
	qfTrash() << "\t return:" << ret;
	return ret;
}
