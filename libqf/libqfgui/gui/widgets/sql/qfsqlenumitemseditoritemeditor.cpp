
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#include "ui_qfsqlenumitemseditoritemeditor.h"
#include "qfsqlenumitemseditoritemeditor.h"

#include <qfsqlcontrol.h>

#include <qflogcust.h>

//=================================================
//                                    QFSqlEnumItemsEditorItemEditor
//=================================================
QFSqlEnumItemsEditorItemEditor::QFSqlEnumItemsEditorItemEditor(const QFDbEnum &de, const QStringList &current_group_ids, QWidget *parent) 
	: QDialog(parent), editedEnum(de)
{
	f_currentGroupIds = current_group_ids;
	f_maxGroupIdLength = 16;
	ui = new Ui::QFSqlEnumItemsEditorItemEditor;
	ui->setupUi(this);

	ui->edCaption->setText(editedEnum.caption());
	ui->edGroupId->setText(editedEnum.groupId());
	ui->edUserText->setPlainText(editedEnum.userText());
}

QFSqlEnumItemsEditorItemEditor::~QFSqlEnumItemsEditorItemEditor()
{
	delete ui;
}

QFDbEnum QFSqlEnumItemsEditorItemEditor::dbEnum()
{
	QFDbEnum ret = editedEnum;
	ret.setGroupId(ui->edGroupId->text());
	ret.setCaption(ui->edCaption->text());
	ret.setUserText(ui->edUserText->toPlainText());
	return ret;
}

void QFSqlEnumItemsEditorItemEditor::on_edCaption_textChanged()
{
	if(editedEnum.groupId().isEmpty()) {
		QString s = QFSqlControl::generateUniqueGroupIdFromCaption(ui->edCaption->text(), f_currentGroupIds, f_maxGroupIdLength);
		ui->edGroupId->setText(s);
	}
}

