
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqleditwithbutton.h"

#include <qfsql.h>
#include <qfdataformdocument.h>
#include <qfdlgdatatable.h>
#include <qfdbapplication.h>

#include <qflogcust.h>

QFSqlEditWithButton::QFSqlEditWithButton(QWidget *parent)
	: QFEditWithButton(parent), QFSqlControl(this), f_chooser(NULL)
{
	f_captionCachePolicy = LoadCacheIfEmpty;
	f_unknownIdCaption = tr("id ${ID} nenalezeno");
	connect(this, SIGNAL(editingFinished()), this, SLOT(slotEditingFinished()));
}

QFSqlEditWithButton::~QFSqlEditWithButton()
{
}

void QFSqlEditWithButton::loadValue(const QString &sql_id)
{
	if(!checkSqlId(sql_id)) return;
	QFDataFormDocument *doc = document();
	if(!doc) return;
	f_id = doc->value(sqlId());
	loadingState = true;
	lineEdit->setText(idToCaption(f_id));
	loadingState = false;
//setReadOnly(doc->mode() == QFDataFormDocument::ModeView);
	QFSqlControl::loadValue(sql_id);
}

void QFSqlEditWithButton::flushValue()
{
	qfTrash() << QF_FUNC_NAME;
	if(!loadingState) {
		QFDataFormDocument *doc = document();
		if(!doc || doc->isEmpty()) return;
		emit valueUpdated(sqlId(), f_id);
	}
}
/*
void QFSqlEditWithButton::emitValueUpdated(const QVariant &val)
{
	qfTrash() << QF_FUNC_NAME;
	emit valueUpdated(sqlId(), val);
}
*/
void QFSqlEditWithButton::slotEditingFinished()
{
	qfTrash() << QF_FUNC_NAME;
	if(isEditable()) flushValue();
}

void QFSqlEditWithButton::setText(const QString &_text)
{
	qfTrash() << QF_FUNC_NAME << _text;
	QString old_text = text();
	if(old_text != _text) {
		QFEditWithButton::setText(_text);
		//emit valueUpdated(sqlId(), _text);
	}
}

void QFSqlEditWithButton::slotButtonClicked()
{
	qfTrash() << QF_FUNC_NAME;
	QFDlgSqlDataTable *ch = chooser();
	if(ch) {
		if(ch->model() && ch->model()->isEmpty()) ch->reload();
		if(ch->sqlView()) {
			QFDataFormDocument *doc = document();
			if(doc) ch->sqlView()->selectId(doc->value(sqlId()));
		}
		int ret = ch->exec();
		if(ret == QFDialog::Accepted) {
			f_id = ch->view()->selectedRow().value(ch->view()->idColumnName());
			//setText(idToCaption(val).toString());
			flushValue();
			emit rowChoosen(ch->view()->selectedRow());
		}
		else if(ret == QFDialog::NoneOfThem) {
			f_id = QVariant();
			flushValue();
			emit rowChoosen(QFBasicTable::Row());
		}
	}
	else emit buttonClicked();
}

void QFSqlEditWithButton::setChooser(QFDlgSqlDataTable *chooser)
{
	f_chooser = chooser;
}

QFDlgSqlDataTable* QFSqlEditWithButton::chooser()
{
	return f_chooser;
}

const QFStringMap & QFSqlEditWithButton::captionMap()
{
	QFDbApplication *db_app = qobject_cast<QFDbApplication*>(QApplication::instance());
	if(db_app) {
		if(f_captionMap.isEmpty()) {
			qfTrash() << QF_FUNC_NAME << objectName();
			QFString tblname, fldname, capname;
			QFSql::parseFullName(referencedField(), &fldname, &tblname);
			capname = captionField();
			if(!capname) capname = fldname;
			qfTrash() << "\t capname:" << capname;
			QFString query_str = query();
			qfTrash() << "\t query:" << query_str;
			if(!query_str) {
				QFSqlQueryBuilder qb;
				qb.select(fldname);
				if(capname != fldname) qb.select(capname);
				qb.from(tblname);
				qb.orderBy(capname);
				if(captionCachePolicy() == OneValueCache) {
					QFDataFormDocument *doc = document();
					if(doc) {
						f_id = doc->value(sqlId());
						qb.where(fldname + "=" + QFSql::formatValue(f_id));
					}
				}
				query_str = qb.toString();
			}
			QFSqlQuery q(db_app->connection());
			qfTrash() << "\t query_str" << query_str;
			qfTrash() << "\t capname:" << capname;
			q.exec(query_str);
			while(q.next()) {
				QString caption = q.value(capname).toString();
				QString id = q.value(fldname).toString();
				qfTrash() << "\t adding :" << caption << id;
				f_captionMap[id] = caption;
			}
		}
	}
	return f_captionMap;
}

QString QFSqlEditWithButton::idToCaption(const QVariant & fk_id)
{
	QString key = fk_id.toString();
	QString ret;
	if(captionCachePolicy() == ReloadCacheIfNotFound || captionCachePolicy() == OneValueCache) {
		if(!captionMap().contains(key)) {
			clearCache();
		}
	}
	if(captionMap().contains(key)) {
		ret = captionMap().value(key);
	}
	else {
		ret = unknownIdCaption();
		ret.replace("${ID}", fk_id.toString());
	}
	return ret;
}

void QFSqlEditWithButton::clearCache()
{
	f_captionMap.clear();
}

