
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqlenumcombo.h"
#include "qfsqlenumitemseditor.h"

#include <qfsqleditforeignitemsbutton.h>
#include <qfdbapplication.h>
#include <qftableview.h>
#include <qfsql.h>
#include <qfsqlcatalog.h>

#include <qflogcust.h>

//=============================================================
//                       EnumzTableModel
//=============================================================
class EnumzTableModel : public QFSqlQueryTableModel
{
	protected:
		QFSqlEnumCombo *f_combo;
	public:
		//virtual bool setData(const QModelIndex & ix, const QVariant & value, int role = Qt::EditRole);
		virtual int insertRow(int before_row, const QModelIndex & parent = QModelIndex()) throw(QFException);
		virtual bool postRow(int ri) throw(QFException);
	public:
		EnumzTableModel(QObject *parent, QFSqlEnumCombo *combo) : QFSqlQueryTableModel(parent), f_combo(combo) {}
};

bool EnumzTableModel::postRow(int ri) throw(QFException)
{
	if(table()->row(ri).isDirty()) {
		if(QFSql::sqlIdCmp("groupId", f_combo->referencedField())) {
			QString group_id = value(ri, "groupId").toString();
			if(group_id.isEmpty()) {
				/// vygeneruj automaticky groupId
				static int group_id_len = 0;
				if(group_id_len == 0) {
					group_id_len = qfDbApp()->connection().catalog().table("enumz").field("groupId").length();
				}
				QFString caption = value(ri, "caption").toString();
				QStringList current_group_ids;
				foreach(const QFBasicTable::Row &r, table()->rows()) current_group_ids << r.value("groupId").toString();
				group_id = QFSqlControl::generateUniqueGroupIdFromCaption(caption, current_group_ids, group_id_len);
				setValue(ri, "groupId", group_id);
			}
		}
	}
	return QFSqlQueryTableModel::postRow(ri);
}

int EnumzTableModel::insertRow(int before_row, const QModelIndex & parent) throw( QFException )
{
	int ret = QFSqlQueryTableModel::insertRow(before_row, parent);
	if(ret >= 0 && f_combo) {
		setValue(ret, "groupName", f_combo->enumGroup());
	}
	return ret;
}

//=============================================================
//                       QFSqlEnumCombo
//=============================================================
QFSqlEnumCombo::QFSqlEnumCombo(QWidget *parent)
	: QFSqlForeignKeyComboBase(parent)
{
	setReferencedField("enumz.groupId");
	setCaptionField("enumz.caption");
	setEditItemsGrant("editForeignKeyItems");
}
/*
QFSqlForeignKeyControlItemsEditor * QFSqlEnumCombo::itemsEditor()
{
	if(!f_itemsEditor) {
		QFSqlEnumComboStandardItemsEditor *ed = new QFSqlEnumComboStandardItemsEditor(this);
		QFTableView *tv = new QFTableView();
		tv->setCopyRowActionVisible(true);
		tv->setXmlConfigPersistentId("QFSqlEnumCombo/itemsEditor/tableView");
		
		QFTableModel *m  = new EnumzTableModel(this, this);
		m->addColumn("pos", trUtf8("pos"), 50);
		m->addColumn("groupId", trUtf8("klíč"), 200);
		m->addColumn("caption", trUtf8("název"), 300);
		QFSqlQueryTable *t = dynamic_cast<QFSqlQueryTable*>(m->table());
		if(t) t->setQuery("SELECT id, groupName, groupId, pos, caption FROM enumz WHERE groupName="SARG(enumGroup())" ORDER BY pos");
		tv->setModel(m);

		ed->setTableView(tv);
		f_itemsEditor = ed;
	}
	return f_itemsEditor;
}
*/
void QFSqlEnumCombo::loadItems()
{
	if(!itemsLoaded) {
		qfTrash() << QF_FUNC_NAME << objectName();
		loadingState = true;
		clear();  /// clear je kvuli reloadItems()
		itemCaptions.clear();
		QFDbEnumCache::EnumList lst = qfDbApp()->dbEnumsForGroup(enumGroup());
		int row_no = 0;
		bool id_from_caption = QFSql::sqlIdCmp(referencedField(), "caption");
		foreach(QFDbEnum e, lst) {
			QString caption = e.caption();
			QString fmt = itemCaptionFormat();
			if(!fmt.isEmpty()) {
				caption = fmt;
				caption.replace("${groupName}", e.groupName());
				caption.replace("${groupId}", e.groupId());
				caption.replace("${pos}", QString::number(e.pos()));
				caption.replace("${abbreviation}", e.abbreviation());
				caption.replace("${value}", e.value().toString());
				caption.replace("${caption}", e.caption());
			}
			QVariant id = (id_from_caption)? caption: e.groupId();
			QStandardItem *it = new QStandardItem(caption);
			itemModel()->setItem(row_no, it);
			QModelIndex ix = itemModel()->index(row_no, 0);
			//qfTrash() << "row_no:" << row_no << "caption:" << caption << "data:" << id.toString();
			if (ix.isValid()) { itemModel()->setData(ix, id, Qt::UserRole); }
			itemCaptions[caption.toLower()] = row_no;
			row_no++;
		}
		loadingState = false;
		itemsLoaded = true;
	}
}
/*
void QFSqlEnumCombo::reloadItems()
{
	QFSqlForeignKeyCombo::reloadItems();
}
*/
void QFSqlEnumCombo::showItemsEditor()
{
	qfLogFuncFrame();
	QFSqlEnumItemsEditor *w = new QFSqlEnumItemsEditor(enumGroup());
	w->load();
	QFButtonDialog dlg(this);
	dlg.setXmlConfigPersistentId("QFSqlEnumItemsEditor");
	dlg.setDialogWidget(w);
	if(dlg.exec()) {
		w->save();
		QFDbEnumCache *cache = qfDbApp()->enumCache();
		if(cache) cache->reload(enumGroup());
		reloadItems();
	}
}

void QFSqlEnumCombo::setEditItemsButton()
{
	QFSqlForeignKeyComboBase::setEditItemsButton();
	if(f_editItemsButton) {
		if(qobject_cast<QFApplication*>(QApplication::instance())) { /// kvuli designeru, tam qfApp neexistuje
			f_editItemsButton->setVisible(qfApp()->currentUserHasGrant(editItemsGrant()));
		}
	}
}

QFDbEnum QFSqlEnumCombo::currentDbEnum()
{
	QFDbEnum ret;
	QString group_id;
	int ix = currentIndex();
	qfTrash() << "\t currentIndex:" << ix;
	if(isEditable() && isValueRestrictedToItems()) {
		/// muze se stat, ze spravny item doplnil completer v editboxu a current item je jinej
		QString s = lineEdit()->text().toLower();
		int ix2 = itemCaptions.value(s, -1);
		qfTrash() << "\t editor text:" << s << "row_no:" << ix2;
		if(ix2 >= 0) {
			group_id = itemModel()->data(itemModel()->index(ix2, 0), Qt::UserRole).toString();
		}
	}
	if(group_id.isEmpty()) {
		if(ix < 0) {
			/// neni vybrana zadna polozka
		}
		else {
			group_id = itemData(ix).toString();
		}
	}
	if(!group_id.isEmpty()) {
		ret = qfDbApp()->dbEnum(enumGroup(), group_id);
	}
	return ret;
}
