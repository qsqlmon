
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqleditforeignitemsbutton.h"

#include <qfpixmapcache.h>

#include <qflogcust.h>

QFSqlEditForeignItemsButton::QFSqlEditForeignItemsButton(QWidget *parent)
	: QToolButton(parent)
{
	setText("E..");
	setIcon(QFPixmapCache::icon("tuzka"));
	setToolTip(tr("Edit items"));
	setAutoRaise(true);
	setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
}

QFSqlClearValueButton::QFSqlClearValueButton(QWidget *parent)
: QToolButton(parent)
{
	setText("NC");
	setIcon(QFPixmapCache::icon("cancel"));
	setToolTip(tr("Clear current value"));
	setAutoRaise(true);
	setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
}
