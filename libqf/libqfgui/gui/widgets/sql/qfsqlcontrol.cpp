
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqlcontrol.h"
 
#include <qfgui.h>
#include <qfdataformdocument.h>
#include <qfdataformwidget.h>

#include <QTextCodec>

#include <qflogcust.h>

QFSqlControl::QFSqlControl(QWidget *controlled_widget)
	: controlledWidget(controlled_widget)
{
#ifdef QFSQCONTROL_WITH_IGNORED
	setIgnored(false);
#endif
	setDataReadOnly(false);
	setExternalData(false);
	setIgnoreClosedForEdits(false);
	setMandatory(false);
	loadingState = false;
}

QFSqlControl::~QFSqlControl()
{
}

QFDataFormDocument* QFSqlControl::document()
{
	if(!controlledWidget) return NULL;
	QFDataFormWidget *w = qfFindParent<QFDataFormWidget*>(controlledWidget, !Qf::ThrowExc);
	if(!w) return NULL;
	return w->document(!Qf::ThrowExc);
}

bool QFSqlControl::sqlIdCmp(const QString &sql_id) const
{
	return QFSql::sqlIdCmp(sqlId(), sql_id);
}

void QFSqlControl::loadValue(const QString &sql_id)
{
	if(!checkSqlId(sql_id)) return;
	updateWidgetAppearence();
}
void QFSqlControl::setDataReadOnly(bool ro)
{
	f_dataReadOnly = ro;
	//qfInfo() << QFLog::stackTrace();
	setWidgetReadOnly(ro);
}

void QFSqlControl::setMandatory(bool b)
{
	f_mandatory = b;
	updateWidgetAppearence();
}

void QFSqlControl::updateWidgetAppearence()
{
	QFDataFormDocument *doc = document();
	qfLogFuncFrame() << sqlId() 
			<< "isDataReadOnly:" << isDataReadOnly()
			<< "isExternalData:" << isExternalData();
	bool ro = isDataReadOnly();
	if(doc) {
		qfTrash() << "doc mode view:" << (doc->mode() == QFDataFormDocument::ModeView)
				<< "isExternalDataReadOnly (doc):" << doc->isExternalDataReadOnly();
		if(doc->mode() == QFDataFormDocument::ModeView) {
			ro = true;
			qfTrash() << "\t RO because of ModeView";
		}
		else if(doc->mode() == QFDataFormDocument::ModeEdit) {
			if(doc->hasWriteLock()) {
				qfTrash() << "\t HAS write LOCK";
				ro =  isDataReadOnly();
				qfTrash() << "\t" << __LINE__ << "ro:" << ro;
				if(!ro) ro = ro || (doc->isClosedForEdits() && !ignoreClosedForEdits());
				qfTrash() << "\t" << __LINE__ << "ro:" << ro;
				if(!ro) ro = ro || !doc->currentUserHasGrant(editGrant());
				qfTrash() << "\t" << __LINE__ << "ro:" << ro;
				if(!ro) ro = ro || (doc->isExternalDataReadOnly() && isExternalData());
				qfTrash() << "\t" << __LINE__ << "ro:" << ro;
			}
			else {
				qfTrash() << "\t CANN'T GET write LOCK";
				ro = true;
			}
		}
		else {
			/// v insertu to necham podle nastaveni widgetu
		}
	}
	//qfInfo() << "doc->hasWriteLock():" << doc->hasWriteLock();
	//qfInfo() << (controlledWidget? controlledWidget->objectName(): "NULL") << sqlId() << ro;
	qfTrash() << "\t RO:" << ro;
	setWidgetReadOnly(ro);
}

void QFSqlControl::setEditGrantToChildWidgets(QWidget *parent, const QString &grant)
{
	QFGui::setPropertyInChildWidgets(parent, "editGrant", grant);
}

bool QFSqlControl::checkSqlId(const QString &sql_id) const
{
#ifdef QFSQCONTROL_WITH_IGNORED
	if(isIgnored()) return false;
#endif
	if(sqlId().isEmpty()) return false;
	if(!sql_id.isEmpty() && !sqlIdCmp(sql_id)) return false;
	return true;
}

QString QFSqlControl::generateUniqueGroupIdFromCaption(const QString &_caption, const QStringList & current_group_ids, int group_id_len)
{
	QString ret;
	QTextCodec *tc_ascii7 = QTextCodec::codecForName("ASCII7");
	if(!tc_ascii7) QF_EXCEPTION("Text codec ASCII7 load error.");
	QFString caption = _caption;
	QRegExp rx("[\\.\\,\\=\\;\\+\\-\\%\\#\\@\\$\\^\\&\\*\\(\\)\\<\\>\\:\\\"\\'\\!\\~\\`\\?\\/\\\\\\{\\}\\[\\]]");
	caption.replace(rx, QString());
	QStringList sl = caption.splitAndTrim(' ');
	QStringList sl2;
	foreach(QFString s, sl) {
		s = QString(tc_ascii7->fromUnicode(s));
		if(sl2.count()) {
			/// prvni slovo nech, jak je, ostatni camel case
			s = QString(tc_ascii7->fromUnicode(s)).toLower();
			s[0] = s[0].toUpper();
		}
		//ret.replace(rx, QString());
		sl2 << s;
	}
	{
		/// pokud jsou slova moc dlouha, pozkracuj je
		int len = 0;
		foreach(QString s, sl2) len += s.length();
		if(len > group_id_len) {
			int optimal_len = group_id_len / sl2.count();
			int extra_len = 0;
			for(int i=0; i<sl2.count(); i++) {
				QString s = sl2[i];
				if(s.length() > optimal_len) {
					sl2[i] = s.mid(0, optimal_len + extra_len);
				}
				else {
					extra_len += optimal_len - s.length();
				}
			}
		}
	}
	ret = sl2.join(QString());
	ret = ret.mid(0, group_id_len);
	for(int i=0; i<1000000; i++) {
		bool duplicate = false;
		foreach(const QString current_group_id, current_group_ids) {
			if(current_group_id == ret) {
				duplicate = true;
				break;
			}
		}
		if(duplicate) {
			QString sno = QString::number(i+1);
			ret = ret.mid(0, group_id_len - sno.length()) + sno;
		}
		else break;
	}
	return ret;
}

