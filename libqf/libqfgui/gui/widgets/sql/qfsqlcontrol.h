
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLCONTROL_H
#define QFSQLCONTROL_H

#include <qfguiglobal.h>
#include <qfexception.h>
#include <qfclassfield.h>

#include <QVariant>

//#define QFSQCONTROL_WITH_IGNORED

class QFDataFormDocument;

class QFGUI_DECL_EXPORT QFMandatoryEmptyFieldException : public QFException
{
	public:
		QFMandatoryEmptyFieldException(const QString& msg) : QFException() { init(QString(), msg, QString()); }
};

//! Definice funkci jsou takhle divoky, protoze se nemuze vicenasobne dedit od tridy \a QObject .
class QFGUI_DECL_EXPORT QFSqlControl
{
	//QF_FIELD_RW(QString, s, setS, qlId);
	//! external data jsou treba data z Excaliburu,
	/// pokud ma widget tuto property, muze byt nastaven jako RO, pokud se welloffice pouziva s Excaliburem a RW pokud ne.
	QF_FIELD_RW(bool, is, set, ExternalData);
	//! pokud je dokument closedForEdits a tento flag je nastaven, widget se neprepne na read only, jinak jo.
	QF_FIELD_RW(bool, i, setI, gnoreClosedForEdits);
	//! pokud neni empty, pouze uzivatel s timto grantem muze editovat sql control.
	QF_FIELD_RW(QString, e, setE, ditGrant);
	/// zprava, ktera se objevi, pokud neni field vyplnen
	QF_FIELD_RW(QString, m, setM, andatoryErrorMessage);
#ifdef QFSQCONTROL_WITH_IGNORED
	/// pokud je true QFSqlControl nepracuje, pouziva se to, kdyz chci nektere widgety docasne vyradit z funkce na formulari
	QF_FIELD_RW(bool, isI, setI, gnored);
#endif
	protected:
		/// jmeno fieldu v SQL dotazu, s kterym se control provaze
		QString f_sqlId;
		/// Pokud je true, nelze widget editovat
		/// nektery widgety uz maji readOnly property, proto tenhle nazev.
		bool f_dataReadOnly;
		bool f_mandatory;
		//QString f_mandatoryErrorMessage;
		/// behem loadValue neni treba emitovat signal valueUpdated, tato promenna mi to umozni zaridit
		bool loadingState;
	public:
		virtual QString sqlId() const {return f_sqlId;}
		virtual void setSqlId(const QString &sql_id) {f_sqlId = sql_id;}
		
		void setDataReadOnly(bool ro = true);
		bool isDataReadOnly() const {return f_dataReadOnly;}

		//! pokud je true, musi byt field vyplnen a kdyz neni podbarvi se cervene
		void setMandatory(bool b);
		bool isMandatory() const {return f_mandatory;}

	protected:
		/// vraci true, kdyz si mysli, ze sql_id je nazev stejnyho fieldu, jako sqlId()
		bool sqlIdCmp(const QString &sql_id) const;
	protected:
		virtual void updateWidgetAppearence();

		// property v designeru, ktera rika, jestli widget pujde editovat nebo ne
		//QVariant fEditable;

		/// tato funkce se vola, kdyz chci widgetu povolit nebo zakazat editaci
		virtual void setWidgetReadOnly(bool b) {Q_UNUSED(b);}
	public: /// slots:
		/// reloadne widget z dokumentu z fieldu \a sqlId
		virtual void loadValue(const QString &sql_id = QString());
		/// nahraje obsah controlu do dokumentu tim, ze emituje valueUpdated.
		virtual void flushValue() = 0;
	///signals:
		/// kdyz chce widget synchronizovat svuj obsah s dokumentem, vysle tento signal
	///	void valueUpdated(const QString &sql_id, const QVariant &val);

		//! vrati dokument svazany s timto kontrolem nebo NULL.
		QFDataFormDocument* document();
	protected:
		QWidget *controlledWidget;
	public:
		bool checkSqlId(const QString &sql_id) const;
		static void setEditGrantToChildWidgets(QWidget *parent, const QString &grant);

		static QString generateUniqueGroupIdFromCaption(const QString &caption, const QStringList &current_group_ids, int max_id_len = 16);
	public:
		QFSqlControl(QWidget *controlled_widget);
		virtual ~QFSqlControl();
};

#endif // QFSQLCONTROL_H

