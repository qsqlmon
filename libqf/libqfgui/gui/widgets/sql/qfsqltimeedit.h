
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLTIMEEDIT_H
#define QFSQLTIMEEDIT_H

#include "qfsqlcontrol.h"

#include <qfguiglobal.h>

#include <QTimeEdit>


//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlTimeEdit : public QTimeEdit, public QFSqlControl
{
	Q_OBJECT;
	Q_PROPERTY(QString sqlId READ sqlId WRITE setSqlId);
	Q_PROPERTY(bool dataReadOnly READ isDataReadOnly WRITE setDataReadOnly);
	Q_PROPERTY(QString editGrant READ editGrant WRITE setEditGrant);
	Q_PROPERTY(bool ignoreClosedForEdits READ ignoreClosedForEdits WRITE setIgnoreClosedForEdits);
	public slots:
		void loadValue(const QString &sql_id = QString());
		virtual void flushValue();
	signals:
		void valueUpdated(const QString &sql_id, const QVariant &val);
	protected:
		virtual void setWidgetReadOnly(bool b);
	public:
		QFSqlTimeEdit(QWidget *parent = NULL);
		virtual ~QFSqlTimeEdit();
};

#endif // QFSQLTIMEEDIT_H

