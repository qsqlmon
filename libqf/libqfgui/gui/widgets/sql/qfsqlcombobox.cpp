
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqlcombobox.h"

#include <qfdataformdocument.h>
#include <qfdataformwidget.h>

#include <QWheelEvent>

#include <qflogcust.h>

QFSqlListBox::QFSqlListBox(QWidget * parent)
	: QComboBox(parent), QFSqlControl(this)
{
	setEditable(false);
	setReadOnly(false);
	connect(this, SIGNAL(activated(int)), this, SLOT(flushValue()));
}

void QFSqlListBox::setReadOnly(bool ro)
{
	qfLogFuncFrame() << sqlId() << "ro:" << ro;
	//qfTrash().color(QFLog::Yellow) << QFLog::stackTrace();
	f_readOnly = ro;
	QLineEdit *ed = this->lineEdit();
	if(ed) {
		ed->setReadOnly(ro);
		/// musim to delat takhle, protoze lineEdit neni ditetem listboxu asi
		QString style_sheet = "QLineEdit[readOnly=\"true\"] {background-color: " + QFDataFormWidget::ReadOnlyWidgetBackgroundColor + "}";
		//qfTrash() << "\t setting DataFormWidget styleSheet:" << style_sheet;
		ed->setStyleSheet(style_sheet);
	}
	//setEditable(!ro);
}

void QFSqlListBox::showPopup()
{
	if(isReadOnly()) return;
	QComboBox::showPopup();
}

void QFSqlListBox::wheelEvent(QWheelEvent * event)
{
	if(isReadOnly()) {
		event->ignore();
	}
	else {
		QComboBox::wheelEvent(event);
	}
}
/*
void QFSqlComboBox::onActivated(int index)
{
	Q_UNUSED(index);
	flushValue();
}
*/
void QFSqlListBox::flushValue()
{
	qfLogFuncFrame();
	if(!loadingState) {
		QFDataFormDocument *doc = document();
		if(!doc || doc->isEmpty()) return;
		QVariant v;
		if(isEditable()) {
			//qfInfo() << v.toString();
			v = currentText();
		}
		else {
			v = itemData(currentIndex()); /// data pouzivam i v pripade listboxu kvuli lokalizacim
			if(!v.isValid()) v = currentIndex(); /// kdyz nejsou data, uklada se index
		}
		//qfInfo() << v.toString();
		emit valueUpdated(sqlId(), v);
	}
}

void QFSqlListBox::loadValue(const QString &sql_id)
{
	qfLogFuncFrame() << "my sqlid:" << sqlId() << "src sqlid:" << sql_id;
	if(!checkSqlId(sql_id)) return;
	QFDataFormDocument *doc = document();
	if(!doc) return;
	QVariant id = doc->value(sqlId());
	loadingState = true;
	//qfInfo() << __LINE__;
	setCurrentValue(id);
	loadingState = false;
	QFSqlControl::loadValue(sql_id);
}

void QFSqlListBox::setCurrentValue(const QVariant &val)
{
	qfLogFuncFrame() << "sqlId:" << sqlId() << "val:" << val.toString() << "isEditable()" << isEditable();
	loadItems();
	//setCurrentIndex(-1);
	int ix = -1;
	for(int i=0; i<count(); i++) {
		QVariant v = itemData(i);
		if(!v.isValid() && val.type() == QVariant::Int) v = i;/// value je index
		qfTrash() << "\t checking:" << v.toString();
		if(v == val) {
			setCurrentIndex(i);
			ix = i;
			break;
		}
	}
	if(ix < 0 && isEditable() && !isValueRestrictedToItems()) {
		QLineEdit *ed = lineEdit();
		if(ed) ed->setText(val.toString());
	}
	else {
		setCurrentIndex(ix);
	}
}

//=======================================================
//                                           QFSqlComboBox
//=======================================================

QFSqlComboBox::QFSqlComboBox(QWidget *parent)
	: QFSqlListBox(parent)
{
	setEditable(true);
	if(lineEdit()) connect(lineEdit(), SIGNAL(editingFinished()), this, SLOT(flushValue()));
}

void QFSqlComboBox::setReadOnly(bool ro)
{
	f_readOnly = ro;
	QLineEdit *ed = this->lineEdit();
	if(ed) ed->setReadOnly(ro);
}






