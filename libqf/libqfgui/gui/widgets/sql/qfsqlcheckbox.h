
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLCHECKBOX_H
#define QFSQLCHECKBOX_H

#include "qfsqlcontrol.h"

#include <qfguiglobal.h>

#include <QCheckBox>


//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlCheckBox : public QCheckBox, public QFSqlControl
{
	Q_OBJECT
	Q_PROPERTY(QString sqlId READ sqlId WRITE setSqlId)
	Q_PROPERTY(bool dataReadOnly READ isDataReadOnly WRITE setDataReadOnly);
	Q_PROPERTY(bool externalData READ isExternalData WRITE setExternalData);
	Q_PROPERTY(QString editGrant READ editGrant WRITE setEditGrant);
	Q_PROPERTY(bool ignoreClosedForEdits READ ignoreClosedForEdits WRITE setIgnoreClosedForEdits);
	QF_FIELD_RW(bool, is, set, ReadOnly);
	protected:
		virtual void setWidgetReadOnly(bool b);
		virtual bool hitButton(const QPoint &pos) const;
	protected slots:
		//void checkStateChanged(int new_state);
	public slots:
		virtual void loadValue(const QString &sql_id = QString());
		virtual void flushValue();
	signals:
		void valueUpdated(const QString &sql_id, const QVariant &val);
	public:
		QFSqlCheckBox(QWidget *parent = NULL);
		virtual ~QFSqlCheckBox();
};

#endif // QFSQLCHECKBOX_H

