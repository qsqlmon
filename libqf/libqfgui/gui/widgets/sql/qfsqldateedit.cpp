
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqldateedit.h"

#include <qfsql.h>
#include <qfdataformdocument.h>
#include <qfdataformwidget.h>

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

QFSqlDateEdit::QFSqlDateEdit(QWidget *parent)
	: QFDateEdit(parent), QFSqlControl(this)
{
	connect(this, SIGNAL(dateChanged(const QDate &)), this, SLOT(flushValue()));
}

QFSqlDateEdit::~QFSqlDateEdit()
{
}

void QFSqlDateEdit::loadValue(const QString &sql_id)
{
	if(!checkSqlId(sql_id)) return;
	qfTrash() << QF_FUNC_NAME << sqlId();
	QFDataFormDocument *doc = document();
	if(!doc) return;
	QVariant v = doc->value(sqlId());
	QDate d = v.toDate();
	qfTrash() << "\t" << sqlId() << "new value:" << d.toString();
	loadingState = true;
	setDate(d);
	updateStyleSheet();
	loadingState = false;
	QFSqlControl::loadValue(sql_id);
}

void QFSqlDateEdit::flushValue()
{
	qfLogFuncFrame();
	updateStyleSheet();
	if(!loadingState) emit valueUpdated(sqlId(), QVariant(date()));
}

void QFSqlDateEdit::setWidgetReadOnly(bool b)
{
	qfTrash() << QF_FUNC_NAME << sqlId();
	setReadOnly(b);
}

void QFSqlDateEdit::updateStyleSheet()
{
	qfLogFuncFrame() << sqlId() << "isMandatory:" << isMandatory() << "isReadOnly:" << isReadOnly() << "isValid:" << date().isValid();
	if(!designedStyleSheet.isValid()) {
		QString ss = styleSheet().simplified();
		designedStyleSheet = ss;
	}
	QString new_ss = designedStyleSheet.toString();
	//qfInfo() << "old_ss:" << new_ss;
	if(isMandatory()) {
		//qfInfo() << sqlId() << "isMandatory:" << isMandatory() << "isReadOnly:" << isReadOnly() << "isValid:" << date().isValid();
		if(!isReadOnly() && !date().isValid()) {
			new_ss = designedStyleSheet.toString() + "\nQDateEdit {background-color:" + QFDataFormWidget::MandatoryWidgetBackgroundColor + "}";
			//qfInfo() << "new_ss:" << new_ss;
		}
	}
	//qfInfo() << "new_ss:" << new_ss;
	setStyleSheet(new_ss);
}

