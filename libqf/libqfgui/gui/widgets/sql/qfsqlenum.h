
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLENUM_H
#define QFSQLENUM_H

#include "qfsqlcontrol.h"

#include <qfguiglobal.h>
#include <qfsqlcombobox.h>

#include <QComboBox>

//! TODO: write class documentation.
/// @deprecated use QFSqlEnumCombo instead
class QFGUI_DECL_EXPORT QFSqlEnumList : public QFSqlListBox
{
	Q_OBJECT;

	Q_PROPERTY(QString enumGroup READ enumGroup WRITE setEnumGroup);
	//! urcuje, jak se sklada caption ze sloupcu v tabulce enumz, placeholder je ${column_name}
	Q_PROPERTY(QString itemCaptionFormat READ itemCaptionFormat WRITE setItemCaptionFormat);
	QF_FIELD_RW(QString, e, setE, numGroup);
	QF_FIELD_RW(QString, i, setI, temCaptionFormat);
	public:
		virtual void loadItems();
	public:
		QFSqlEnumList(QWidget *parent = NULL);
		virtual ~QFSqlEnumList() {}
};
/*
//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlForeignKeyList : public QFSqlListBox
{
	Q_OBJECT;

	/// ve tvaru "tablename.fieldname"
	Q_PROPERTY(QString referencedField READ referencedField WRITE setReferencedField);
	QF_FIELD_RW(QString, r, setR, eferencedField);
	/// pokud je vyplneno, je to jmeno fieldu z foreign table, ktery se zobracuje
	Q_PROPERTY(QString captionField READ captionField WRITE setCaptionField);
	QF_FIELD_RW(QString, c, setC, aptionField);
	/// pokud je prazdne, vytvori se automaticky,
	/// jinak pouziva placeholders napr. SELECT ${referencedField}, ${captionField} FROM ${referencedTable} WHERE isUserGroup != 0
	Q_PROPERTY(QString query READ query WRITE setQuery);
	QF_FIELD_RW(QString, q, setQ, uery);
	public:
		static const QString referencedFieldPlaceHolder, captionFieldPlaceHolder, referencedTablePlaceHolder;
	public:
		virtual void loadItems();
	public:
		QFSqlForeignKeyList(QWidget *parent = NULL);
		virtual ~QFSqlForeignKeyList() {}
};
*/
#endif // QFSQLENUM_H

