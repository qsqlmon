
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqldoublespinbox.h"

#include <qfsqlquery.h>
#include <qfapplication.h>
#include <qfdataformdocument.h>

#include <QKeyEvent>

#include <qflogcust.h>

QFSqlDoubleSpinBox::QFSqlDoubleSpinBox(QWidget *parent)
	: QDoubleSpinBox(parent), QFSqlControl(this)
{
	f_insideKeyPressEvent = false;
	setMaximum(1000000000);
	setAlignment(Qt::AlignRight);
	QF_CONNECT(this, SIGNAL(valueChanged(double)), this, SLOT(_valueChanged(double)));
}

QFSqlDoubleSpinBox::~QFSqlDoubleSpinBox()
{
}

void QFSqlDoubleSpinBox::loadValue(const QString &sql_id)
{
	qfLogFuncFrame() << "sqlId():" << sqlId() << "sql_id:" << sql_id;
	if(!checkSqlId(sql_id)) return;
	QFDataFormDocument *doc = document();
	if(!doc) return;
	QVariant v = doc->value(sqlId());
	double d = v.toDouble();
	/// zakaz signal value updated
	loadingState = true;
	setValue(d);
	loadingState = false;
	//qfTrash() << "\t" << sqlId() << "new value:" << d.toString();
	QFSqlControl::loadValue(sql_id);
}

void QFSqlDoubleSpinBox::flushValue()
{
	qfLogFuncFrame();
	QFDataFormDocument *doc = document();
	if(!doc || doc->isEmpty()) return;
	double d = value();
	_valueChanged(d);
}

void QFSqlDoubleSpinBox::_valueChanged(double val)
{
	qfLogFuncFrame() << "val:" << val;
	if(!loadingState) emit valueUpdated(sqlId(), val);
}

void QFSqlDoubleSpinBox::setWidgetReadOnly(bool b)
{
	setReadOnly(b);
}

QAbstractSpinBox::StepEnabled QFSqlDoubleSpinBox::stepEnabled() const
{
	//qfTrash() << QF_FUNC_NAME;
	QAbstractSpinBox::StepEnabled ret = QAbstractSpinBox::StepNone;
	bool ro = isReadOnly();
	if(!ro) {
		ret = QAbstractSpinBox::StepUpEnabled | QAbstractSpinBox::StepDownEnabled;
	}
	//qfTrash() << "\treturn:" << ret;
	return ret;
}

void QFSqlDoubleSpinBox::keyPressEvent(QKeyEvent *ev)
{
	f_insideKeyPressEvent = true;
	QDoubleSpinBox::keyPressEvent(ev);
	f_insideKeyPressEvent = false;
	/*
	int key = ev->key();
	if(key == '.' || key == ',') {
		/// vyber desetiny, jinak se totiz deje to, ze nejde poradne napsat des. cislo pokud property keyboardTracking == true
		/// zmacknu des tecku, naskoci 00 a pokud je chci prepsat musim je vymazat
		/// pokud je ale dam do selekce, pujdou prepsat rovnou
	}
	*/
}

QString QFSqlDoubleSpinBox::textFromValue(double value) const
{
	QString s = QDoubleSpinBox::textFromValue(value);
	if(f_insideKeyPressEvent) {
		/// pokud prave pisu, oddelej vsechny nevyznamny nuly, nedaji se kvuli nim totiz rozumne psat desetiny, furt to tam doplnuje nuly na konci
		if(decimals() > 0) {
			qfTrash() << "original:" << s;
			int ix;
			for(ix = s.length() - 1; ix > 0; ix--) {
				if(s[ix] != '0') break;
			}
			s = s.mid(0, ix + 1);
			qfTrash() << "new:" << s;
		}
	}
	return s;
}
