
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqlforeignkeycontrolitemseditor.h"

#include <qftableview.h>

#include <qflogcust.h>

//=========================================================================
//                             QFSqlForeignKeyControlItemsEditor
//=========================================================================
void QFSqlForeignKeyControlItemsEditor::savePersistentData()
{
	QFDialog::savePersistentData();
	if(tableView()) tableView()->savePersistentData();
}


