
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLDATEEDIT_H
#define QFSQLDATEEDIT_H

#include "qfsqlcontrol.h"

#include <qfguiglobal.h>
#include <qfclassfield.h>
#include <qfdateedit.h>

class QFDataFormDocument;

//! @todo write class documentation.
class QFGUI_DECL_EXPORT QFSqlDateEdit : public QFDateEdit, public QFSqlControl
{
	Q_OBJECT;

		Q_PROPERTY(QString sqlId READ sqlId WRITE setSqlId)
		Q_PROPERTY(bool dataReadOnly READ isDataReadOnly WRITE setDataReadOnly);
		Q_PROPERTY(QString editGrant READ editGrant WRITE setEditGrant);
		Q_PROPERTY(bool ignoreClosedForEdits READ ignoreClosedForEdits WRITE setIgnoreClosedForEdits);
		Q_PROPERTY(bool mandatory READ isMandatory WRITE setMandatory);
		Q_PROPERTY(QString mandatoryErrorMessage READ mandatoryErrorMessage WRITE setMandatoryErrorMessage);
	protected:
		QVariant designedStyleSheet;
	protected slots:
		void updateStyleSheet();
	public slots:
		void loadValue(const QString &sql_id = QString());
		virtual void flushValue();
	signals:
		void valueUpdated(const QString &sql_id, const QVariant &val);
	protected:
		virtual void setWidgetReadOnly(bool b);
	public:
		QFSqlDateEdit(QWidget *parent = NULL);
		virtual ~QFSqlDateEdit();
};

#endif // QFSQLDATEEDIT_H
