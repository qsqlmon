
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLSPINBOX_H
#define QFSQLSPINBOX_H

#include "qfsqlcontrol.h"

#include <qfguiglobal.h>

#include <QSpinBox>


//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlSpinBox : public QSpinBox, public QFSqlControl
{
	Q_OBJECT;

	Q_PROPERTY(QString sqlId READ sqlId WRITE setSqlId);
	Q_PROPERTY(bool externalData READ isExternalData WRITE setExternalData);
	Q_PROPERTY(bool dataReadOnly READ isDataReadOnly WRITE setDataReadOnly);;
	Q_PROPERTY(QString editGrant READ editGrant WRITE setEditGrant);
	Q_PROPERTY(bool ignoreClosedForEdits READ ignoreClosedForEdits WRITE setIgnoreClosedForEdits);
	protected slots:
		void _valueChanged(int val);
	public slots:
		virtual void loadValue(const QString &sql_id = QString());
		virtual void flushValue();
	signals:
		void valueUpdated(const QString &sql_id, const QVariant &val);
	protected:
		virtual void setWidgetReadOnly(bool b);
		virtual StepEnabled stepEnabled() const;
	public:
		QFSqlSpinBox(QWidget *parent = NULL);
		virtual ~QFSqlSpinBox();
};

#endif // QFSQLSPINBOX_H

