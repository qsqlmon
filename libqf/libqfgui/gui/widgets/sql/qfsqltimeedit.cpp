
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqltimeedit.h"

#include <qfsql.h>
#include <qfdataformdocument.h>

#include <qflogcust.h>

QFSqlTimeEdit::QFSqlTimeEdit(QWidget *parent)
	: QTimeEdit(parent), QFSqlControl(this)
{
	setAlignment(Qt::AlignRight);
	setDisplayFormat(Qf::defaultTimeFormat());
	connect(this, SIGNAL(timeChanged(const QTime &)), this, SLOT(flushValue()));
}

QFSqlTimeEdit::~QFSqlTimeEdit()
{
}

void QFSqlTimeEdit::loadValue(const QString &sql_id)
{
	if(!checkSqlId(sql_id)) return;
	qfTrash() << QF_FUNC_NAME << sqlId();
	QFDataFormDocument *doc = document();
	if(!doc) return;
	QVariant v = doc->value(sqlId());
	QTime t = v.toTime();
	qfTrash() << "\t" << sqlId() << "new value:" << t.toString();
	loadingState = true;
	setTime(t);
	loadingState = false;
	QFSqlControl::loadValue(sql_id);
}

void QFSqlTimeEdit::flushValue()
{
	qfTrash() << QF_FUNC_NAME;
	if(!loadingState) {
		QFDataFormDocument *doc = document();
		if(!doc || doc->isEmpty()) return;
		emit valueUpdated(sqlId(), QVariant(time()));
	}
}

void QFSqlTimeEdit::setWidgetReadOnly(bool b)
{
	setReadOnly(b);
}

