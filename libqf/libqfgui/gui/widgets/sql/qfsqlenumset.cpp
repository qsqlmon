
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqlenumset.h"
#include "qfsqlenumitemseditor.h"
#include "qfsqlforeignkeycontrolitemseditor.h" 

#include <qftableview.h>
#include <qfbuttondialog.h>
#include <qfdlgexception.h>
#include <qfaction.h>
#include <qfsqlquery.h>
#include <qfdbapplication.h>
#include <qfdataformdocument.h>

#include <QInputDialog>
#include <QMenu>
#include <QTextCodec>

#include <qflogcust.h>

//=======================================================
//                                           QFSqlForeignSetBase
//=======================================================
const QString QFSqlForeignSetBase::referencedFieldPlaceHolder = "${referencedField}";
const QString QFSqlForeignSetBase::referencedTablePlaceHolder = "${referencedTable}";
const QString QFSqlForeignSetBase::captionFieldPlaceHolder = "${captionField}";

QFSqlForeignSetBase::QFSqlForeignSetBase(QWidget *parent)
	: QListWidget(parent), QFSqlControl(this)
{
	itemsLoaded = false;
	setValuesRestrictedToItems(true);
	setNonItemValueCaption(trUtf8("jiná možnost"));
	setContextMenuPolicy(Qt::CustomContextMenu);
	QF_CONNECT(this, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(on_itemChanged(QListWidgetItem*)));
	QF_CONNECT(this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(contextMenuRequest(const QPoint&)));
}

QFSqlForeignSetBase::~QFSqlForeignSetBase()
{
}

void QFSqlForeignSetBase::reloadItems()
{
	QVariant v = currentValue();
	itemsLoaded = false;
	setCurrentValue(v);
}

QFSqlForeignSetBase::Item* QFSqlForeignSetBase::item(int ix)
{
	Item *it = dynamic_cast<Item*>(QListWidget::item(ix));
	QF_ASSERT(it, "item is NULL or not a QFSqlForeignSetBase::Item");
	return it;
}

void QFSqlForeignSetBase::loadValue(const QString &sql_id)
{
	if(!checkSqlId(sql_id)) return;
	QFDataFormDocument *doc = document();
	if(!doc) return;
	qfLogFuncFrame() << "sql_id:" << sqlId();
	QVariant v = doc->value(sqlId());
	setCurrentValue(v);
	QFSqlControl::loadValue(sql_id);
}

void QFSqlForeignSetBase::flushValue()
{
	if(loadingState) return;
	qfLogFuncFrame() << sqlId() << "isValuesRestrictedToItems:" << isValuesRestrictedToItems() << "count:" << count();
	//qfInfo() << QFLog::stackTrace();
	QFDataFormDocument *doc = document();
	if(!doc) return;
	if(doc->isEmpty()) return;
	QVariant v = doc->value(sqlId());
	QVariant curr_v = currentValue();
	if(curr_v != v) {
		qfTrash() << "\t emitting valueUpdated() sql_id:" << sqlId() << "value:" << curr_v.toString();
		if(!v.isValid()) v = ""; /// pokud je sloupec, kam se to uklada NOT NULL, invalidni variant spusobi ulozeni NULL hodnoty a mysql se posere, kupodivu UPDATE t SET cislo='' WHERE id=4 ji nevadi a da tam 0
		emit valueUpdated(sqlId(), curr_v);
	}
}

QVariant QFSqlForeignSetBase::currentValue()
{
	QVariant ret;
	QFDataFormDocument *doc = document();
	if(!doc) return ret;
	QVariant v = doc->value(sqlId());
	if(v.type() == QVariant::Int || v.type() == QVariant::UInt) {
		unsigned u = 0;
		for(int i=0; i<count(); i++) {
			Item *it = item(i);
			if(it->checkState() == Qt::Checked) u = u | it->userData().toUInt();
		}
		ret = u;
	}
	else if(v.type() == QVariant::String) {
		QStringList sl;
		int cnt = count();
		if(!isValuesRestrictedToItems()) cnt--;
		for(int i=0; i<cnt; i++) {
			Item *it = item(i);
			if(it->checkState() == Qt::Checked) sl << it->userData().toString();
		}
		if(!isValuesRestrictedToItems() && count()) {
			Item *it = item(count() - 1);
			if(it->checkState() == Qt::Checked) sl << encodeNonItemText(it->userData().toString());
		}
		QString s = sl.join(",");
		ret = s;
	}
	else {
		qfWarning() << "unsupported variant type:" << v.typeName() << "sql id:" << sqlId();
	}
	return ret;
}

void QFSqlForeignSetBase::setCurrentValue(const QVariant & v)
{
	qfLogFuncFrame() << "new value:" << v.toString();
	loadItems();
	QVariant::Type type = v.type();
	/// podle typu fieldy se to ulozi jako bitfield nebo stringy oddelene carkou
	//qfInfo() << "loaded:" << v.toString();
	loadingState = true;
	if(type == QVariant::Int || type == QVariant::UInt) {
		unsigned u =v.toUInt();
		for(int i=0; i<count(); i++) {
			Item *it = item(i);
			unsigned u2 = it->userData().toUInt();
			if(u & u2) it->setCheckState(Qt::Checked);
			else it->setCheckState(Qt::Unchecked);
		}
	}
	else if(type == QVariant::String) {
		QStringList sl;
		sl = QFString(v.toString()).splitAndTrim(',');
		QString non_item_text;
		if(sl.count() && sl.last().startsWith('@')) {
			non_item_text = decodeNonItemText(sl.last());
			sl.removeLast();
		}
		QSet<QString> ss = QSet<QString>::fromList(sl);
		//qfTrash() << "\t ss.count();" << ss.count();
		for(int i=0; i<count(); i++) {
			Item *it = item(i);
			QString s = it->userData().toString();
			if(ss.contains(s)) it->setCheckState(Qt::Checked);
			else it->setCheckState(Qt::Unchecked);
		}
		if(!isValuesRestrictedToItems() && count()) {
			Item *it = item(count() - 1);
			if(non_item_text.isEmpty()) {
				it->setText(nonItemValueCaption());
				it->setCheckState(Qt::Unchecked);
				/// tady se nesmi nastavovat user data, prestalo by fungovat to, ze se po odskrtnuti a zaskrtnuti objevi drivejsi non item text
			}
			else {
				it->setText(non_item_text);
				it->setCheckState(Qt::Checked);
				it->setUserData(non_item_text);
			}
		}
	}
	else {
		qfWarning() << "unsupported variant type:" << v.type() << v.typeName() << "sql id:" << sqlId();
	}
	loadingState = false;
}

void QFSqlForeignSetBase::setWidgetReadOnly(bool b)
{
	qfLogFuncFrame() << "objectName:" << objectName() << "ro:" << b;
	setReadOnly(b);
}

void QFSqlForeignSetBase::setReadOnly(bool ro)
{
	loadingState = true;
	for(int i=0; i<count(); i++) {
		Item *it = item(i);
		Qt::ItemFlags f = it->flags();
		f &= ~Qt::ItemIsEnabled;
		if(!ro) f |= Qt::ItemIsEnabled;
		it->setFlags(f); /// pozor, tohle vysle signal itemChanged()
	}
	f_readOnly = ro;
	loadingState = false;
}

static QString comma_replacement = "=2C";

QString QFSqlForeignSetBase::encodeNonItemText(const QString & s)
{
	qfLogFuncFrame() << s;
	QString ret = s;
	ret.replace(',', comma_replacement);
	ret = '@' + ret;
	qfTrash() << "\t return:" << ret;
	return ret;
}

QString QFSqlForeignSetBase::decodeNonItemText(const QString & s)
{
	QFString ret = s;
	if(ret.value(0) != '@') qfWarning() << "Bad non item text format." << s;
	else ret = ret.slice(1);
	ret.replace(comma_replacement, ",");
	return ret;
}

void QFSqlForeignSetBase::on_itemChanged(QListWidgetItem * item)
{
	if(loadingState) return;
	qfLogFuncFrame();// << "\t 0 userData:" << it->userData().toString();
	if(!isValuesRestrictedToItems()) {
		Item *it = dynamic_cast<Item*>(item);
		if(it) {
			int r = row(it);
			if(r == count() - 1) {
				loadingState = true; /// blokuj on_itemChanged() pro nasledujici operace
				qfTrash() << "\t 1 userData:" << it->userData().toString();
				if(it->checkState() == Qt::Checked) {
					bool ok;
					QString text = QInputDialog::getText(this, tr("Vlozte text"), tr("Vlozte text:"), QLineEdit::Normal, it->userData().toString(), &ok);
					if(ok) {
						it->setText(text);
						it->setUserData(text);
					}
					else {
						it->setText(nonItemValueCaption());
						it->setCheckState(Qt::Unchecked);
					}
				}
				else {
					it->setText(nonItemValueCaption());
				}
				qfTrash() << "\t 2 userData:" << it->userData().toString();
				loadingState = false;
			}
		}
	}
	flushValue();
}

void QFSqlForeignSetBase::contextMenuRequest(const QPoint &point)
{
	qfLogFuncFrame();
	QMenu menu(this);
	menu.setTitle(tr("menu"));
	QFAction *a;
	a = new QFAction(trUtf8("Editovat položky"), &menu); a->setId("editItems"); menu.addAction(a);
	a->setEnabled(qfApp()->currentUserHasGrant(editItemsGrant()));
	a = qobject_cast<QFAction*>(menu.exec(this->mapToGlobal(point)));
	if(a) {
		try {
			if(a->id() == "editItems") {
				showItemsEditor();
			}
		}
		catch(QFException &e) {
			QFDlgException::exec(this, e);
		}
	}
}

//=======================================================
//                                           QFSqlEnumSet
//=======================================================
QFSqlEnumSet::QFSqlEnumSet(QWidget * parent)
	: QFSqlForeignSetBase(parent)
{
	setReferencedField("enumz.groupId");
	setCaptionField("enumz.caption");
	setEditItemsGrant("editForeignKeyItems");
}

void QFSqlEnumSet::loadItems()
{
	if(!itemsLoaded) {
		qfTrash() << QF_FUNC_NAME << objectName();
		loadingState = true;
		clear(); 
		//itemCaptions.clear();
		QFDbEnumCache::EnumList lst = qfDbApp()->dbEnumsForGroup(enumGroup());
		//int row_no = 0;
		bool id_from_caption = QFSql::sqlIdCmp(referencedField(), "caption");
		foreach(QFDbEnum e, lst) {
			QString caption = e.caption();
			QString fmt = itemCaptionFormat();
			if(!fmt.isEmpty()) {
				caption = fmt;
				caption.replace("${groupName}", e.groupName());
				caption.replace("${groupId}", e.groupId());
				caption.replace("${pos}", QString::number(e.pos()));
				caption.replace("${abbreviation}", e.abbreviation());
				caption.replace("${value}", e.value().toString());
				caption.replace("${caption}", e.caption());
			}
			QVariant id = (id_from_caption)? caption: e.groupId();
			Item *it = new Item(caption, id);
			addItem(it);
		}
		if(!isValuesRestrictedToItems()) {
			Item *it = new Item(nonItemValueCaption(), QString());
			addItem(it);
		}
		loadingState = false;
		itemsLoaded = true;
	}
}

void QFSqlEnumSet::showItemsEditor()
{
	qfLogFuncFrame();
	QFSqlEnumItemsEditor *w = new QFSqlEnumItemsEditor(enumGroup());
	w->load();
	QFButtonDialog dlg(this);
	dlg.setXmlConfigPersistentId("QFSqlEnumItemsEditor");
	dlg.setDialogWidget(w);
	if(dlg.exec()) {
		w->save();
		QFDbEnumCache *cache = qfDbApp()->enumCache();
		if(cache) cache->reload(enumGroup());
		reloadItems();
	}
}

//=======================================================
//                                           QFSqlForeignKeySet
//=======================================================
QMap<QString, QFSqlForeignKeySet::CachedItemList> QFSqlForeignKeySet::itemCache;

QFSqlForeignKeySet::QFSqlForeignKeySet(QWidget * parent)
	: QFSqlForeignSetBase(parent)
{
	setEditItemsGrant("editForeignKeyItems");
}

void QFSqlForeignKeySet::loadItems()
{
	QFDataFormDocument *doc = document();
	if(!doc) return;
	if(!itemsLoaded) {
		qfLogFuncFrame() << objectName();
		loadingState = true;
		clear();  /// clear je k(vuli reloadItems()
		//itemCaptions.clear();
		CachedItemList cached_items = itemCache.value(cachedItemsKey());
		if(cached_items.isEmpty()) {
			QFString tblname, fldname, capname;
			QFSql::parseFullName(referencedField(), &fldname, &tblname);
			capname = captionField();
			if(!capname) capname = fldname;
			qfTrash() << "\t capname:" << capname;
			QFString query_str = query();
			if(!query_str) {
				QFSqlQueryBuilder b;
				b.select(fldname);
				if(capname != fldname) b.select(capname);
				b.from(tblname);
				b.orderBy(capname);
				query_str = b.toString();
			}
			else {
				query_str = query_str.replace(referencedFieldPlaceHolder, fldname);
				query_str = query_str.replace(referencedTablePlaceHolder, tblname);
				query_str = query_str.replace(captionFieldPlaceHolder, capname);
			}
			query_str = query_str.trimmed();
			if(!query_str.isEmpty()) {
				QFSqlQuery q(qfDbApp()->connection());
				//checkLoadItemsQuery(b);
				qfTrash() << "\t" << query_str;
				qfTrash() << "\t capname:" << capname;
				q.exec(query_str);
				while(q.next()) {
					QString caption = q.value(capname).toString();
					QVariant id = q.value(fldname);
					cached_items << CachedItem(caption, id);
					//qfTrash() << "\t adding item:" << caption << id.toString();
				}
			}
		}
		foreach(const CachedItem &cit, cached_items) {
			Item *it = new Item(cit.caption, cit.id);
			addItem(it);
		}
		if(!isValuesRestrictedToItems()) {
			Item *it = new Item(nonItemValueCaption(), "@");
			addItem(it);
		}
		if(!cachedItemsKey().isEmpty()) itemCache[cachedItemsKey()] = cached_items;
		loadingState = false;
		itemsLoaded = true;
		qfTrash() << "\t item count:" << count();
	}
}

void QFSqlForeignKeySet::showItemsEditor()
{
	qfLogFuncFrame();
	QFSqlForeignKeyControlItemsEditor *dlg = itemsEditor();
	if(dlg) {
		QFTableView *view = dlg->tableView();
		if(view) {
			//if(!view->model(!Qf::ThrowExc)) view->setModel(itemsEditorModel());
			//qfInfo() << editItemsGrant() << qfApp()->currentUserHasGrant(editItemsGrant());
			view->setReadOnly(!qfApp()->currentUserHasGrant(editItemsGrant()));
		}
		//QFTableModel *m = itemsEditorModel();
		dlg->reload();
		dlg->exec();
		//bool accepted = (dlg->exec() == QDialog::Accepted);
		/// persistentni data musim ukladat rucne, protoze automaticky se ukladaji az v destruktoru dialogu
		dlg->savePersistentData();
		//if(view) view->savePersistentData();
		reloadItems();
	}
}


