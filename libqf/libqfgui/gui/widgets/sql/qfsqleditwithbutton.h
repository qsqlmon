
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLEDITWITHBUTTON_H
#define QFSQLEDITWITHBUTTON_H

#include "qfsqlcontrol.h"

#include <qfguiglobal.h>
#include <qfclassfield.h>
#include <qfbasictable.h>

#include <qfeditwithbutton.h>

#include <QPointer>

class QFDataFormDocument;
class QFDlgSqlDataTable;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlEditWithButton : public QFEditWithButton, public QFSqlControl
{
	Q_OBJECT;

	Q_PROPERTY(QString sqlId READ sqlId WRITE setSqlId);
	Q_PROPERTY(bool dataReadOnly READ isDataReadOnly WRITE setDataReadOnly);
	Q_PROPERTY(bool externalData READ isExternalData WRITE setExternalData);
	Q_PROPERTY(QString editGrant READ editGrant WRITE setEditGrant);
	Q_PROPERTY(bool ignoreClosedForEdits READ ignoreClosedForEdits WRITE setIgnoreClosedForEdits);
	
	Q_PROPERTY(QFSqlEditWithButton::CaptionCachePolicy captionCachePolicy READ captionCachePolicy WRITE setCaptionCachePolicy);
	Q_ENUMS(CaptionCachePolicy);
	/// ve tvaru "tablename.fieldname"
	Q_PROPERTY(QString referencedField READ referencedField WRITE setReferencedField);
	QF_FIELD_RW(QString, r, setR, eferencedField);
	/// pokud je vyplneno, je to jmeno fieldu z foreign table, ktery se zobracuje
	Q_PROPERTY(QString captionField READ captionField WRITE setCaptionField);
	QF_FIELD_RW(QString, c, setC, aptionField);
	/// pokud je prazdne, vytvori se automaticky,
	/// jinak pouziva placeholders napr. SELECT ${referencedField}, ${captionField} FROM ${referencedTable} WHERE isUserGroup != 0
	Q_PROPERTY(QString query READ query WRITE setQuery);
	/// caption, ktery se objevi, pokud id neni nalezeno, pouziva se v reimplementacich funkce idToCaption()
	Q_PROPERTY(QString unknownIdCaption READ unknownIdCaption WRITE setUnknownIdCaption);
	QF_FIELD_RW(QString, u, setU, nknownIdCaption);
	/*
		/// ktery column obsahuje id zaznamu v tabulce
	Q_PROPERTY(QString idColumnName READ idColumnName WRITE setIdColumnName);
		/// ktery column obsahuje caption zaznamu v tabulce
	Q_PROPERTY(QString captionColumnName READ captionColumnName WRITE setCaptionColumnName);
	QF_FIELD_RW(QString, i, setI, dColumnName);
	QF_FIELD_RW(QString, c, setC, aptionColumnName);
	*/
	public:
		enum CaptionCachePolicy {LoadCacheIfEmpty, ReloadCacheIfNotFound, OneValueCache};

		CaptionCachePolicy captionCachePolicy() const {return f_captionCachePolicy;}
		void setCaptionCachePolicy(CaptionCachePolicy p) {f_captionCachePolicy = p;}

		virtual QString query() {return f_query;}
		void setQuery(const QString &qs) {f_query = qs;}
	protected:
		QFStringMap f_captionMap;
		CaptionCachePolicy f_captionCachePolicy;
		QString f_query;
	protected:
		const QFStringMap& captionMap();
		virtual QString idToCaption(const QVariant& fk_id);
	protected:
		QPointer<QFDlgSqlDataTable> f_chooser;
		QVariant f_id;
	private slots:
		void slotEditingFinished();
	public slots:
		virtual void setText(const QString &_text);
		virtual void loadValue(const QString &sql_id = QString());
		virtual void flushValue();
		void clearCache();
	signals:
		void valueUpdated(const QString &sql_id, const QVariant &val);
		void rowChoosen(const QFBasicTable::Row &row);
	protected:
		virtual void slotButtonClicked();
		//virtual void emitValueUpdated(const QVariant &val);
		virtual void setWidgetReadOnly(bool b) {QFEditWithButton::setWidgetReadOnly(b);}
	public:
		/// Widget does not take ownership of a chooser.
		void setChooser(QFDlgSqlDataTable *chooser);
		virtual QFDlgSqlDataTable* chooser();
	public:
		QFSqlEditWithButton(QWidget *parent = NULL);
		virtual ~QFSqlEditWithButton();
};

#endif // QFSQLEDITWITHBUTTON_H

