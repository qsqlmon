
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqlspinbox.h"

#include <qfsqlquery.h>
#include <qfapplication.h>
#include <qfdataformdocument.h>

#include <qflogcust.h>

QFSqlSpinBox::QFSqlSpinBox(QWidget *parent)
	: QSpinBox(parent), QFSqlControl(this)
{
	setMaximum(1000000000);
	setAlignment(Qt::AlignRight);
	QF_CONNECT(this, SIGNAL(valueChanged(int)), this, SLOT(_valueChanged(int)));
}

QFSqlSpinBox::~QFSqlSpinBox()
{
}

void QFSqlSpinBox::loadValue(const QString &sql_id)
{
	qfLogFuncFrame() << "sqlId():" << sqlId() << "sql_id:" << sql_id;
	if(!checkSqlId(sql_id)) return;
	QFDataFormDocument *doc = document();
	if(!doc) return;
	QVariant v = doc->value(sqlId());
	int i = v.toInt();
	qfTrash() << "\t" << sqlId() << "loaded value:" << i;
	loadingState = true;
	setValue(i);
	loadingState = false;
	QFSqlControl::loadValue(sql_id);
}

void QFSqlSpinBox::flushValue()
{
	qfTrash() << QF_FUNC_NAME;
	QFDataFormDocument *doc = document();
	if(!doc || doc->isEmpty()) return;
	int i = value();
	_valueChanged(i);
}

void QFSqlSpinBox::_valueChanged(int val)
{
	qfLogFuncFrame();
	if(!loadingState) {
		qfTrash() << "\t emmitting valueUpdated()" << sqlId() << val;
		emit valueUpdated(sqlId(), val);
	}
}

void QFSqlSpinBox::setWidgetReadOnly(bool b)
{
	setReadOnly(b);
}

QAbstractSpinBox::StepEnabled QFSqlSpinBox::stepEnabled() const
{
	//qfTrash() << QF_FUNC_NAME;
	QAbstractSpinBox::StepEnabled ret = QAbstractSpinBox::StepNone;
	bool ro = isReadOnly();
	if(!ro) {
		ret = QAbstractSpinBox::StepUpEnabled | QAbstractSpinBox::StepDownEnabled;
	}
	//qfTrash() << "\treturn:" << ret;
	return ret;
}

