
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLENUMCOMBO_H 
#define QFSQLENUMCOMBO_H

#include <qfsqlforeignkeycombo.h> 

class QFDbEnum;

//! TODO: write class documentation.
class  QFGUI_DECL_EXPORT QFSqlEnumCombo : public QFSqlForeignKeyComboBase  
{
	Q_OBJECT;
	Q_PROPERTY(QString enumGroup READ enumGroup WRITE setEnumGroup);
	QF_FIELD_RW(QString, e, setE, numGroup);
	protected:
		//virtual void reloadItems();
		virtual void setEditItemsButton();
	public:
		virtual void loadItems();
		//virtual QFSqlForeignKeyComboItemsEditor* itemsEditor();
		virtual void showItemsEditor();
		QFDbEnum currentDbEnum();
	signals:
		void itemsEdited(const QString &enum_group);
	public:
		QFSqlEnumCombo(QWidget *parent = NULL);
		virtual ~QFSqlEnumCombo() {}
};

#endif // QFSQLENUMCOMBO_H

