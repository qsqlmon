
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLLINEEDIT_H
#define QFSQLLINEEDIT_H

#include "qfsqlcontrol.h"

#include <qfguiglobal.h>
#include <qf.h>
#include <qfclassfield.h>

#include <QLineEdit>

class QFDataFormDocument;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlLineEdit : public QLineEdit, public QFSqlControl
{
	Q_OBJECT;
	Q_PROPERTY(QString sqlId READ sqlId WRITE setSqlId);
	Q_PROPERTY(bool externalData READ isExternalData WRITE setExternalData);
	Q_PROPERTY(bool dataReadOnly READ isDataReadOnly WRITE setDataReadOnly);
	Q_PROPERTY(QString editGrant READ editGrant WRITE setEditGrant);
	Q_PROPERTY(bool ignoreClosedForEdits READ ignoreClosedForEdits WRITE setIgnoreClosedForEdits);
	Q_PROPERTY(bool mandatory READ isMandatory WRITE setMandatory);
	Q_PROPERTY(QString mandatoryErrorMessage READ mandatoryErrorMessage WRITE setMandatoryErrorMessage);
	protected:
		QVariant designedStyleSheet;
	protected:
		virtual void setWidgetReadOnly(bool b);
	protected slots:
		void updateStyleSheet();
	public slots:
		virtual void loadValue(const QString &sql_id = QString());
		virtual void flushValue();
	signals:
		void valueUpdated(const QString &sql_id, const QVariant &val);
		///------------------------------ tady konci nejnutnejsi pro QFSqlControl
	protected slots:
		void slotEditingFinished();
	protected slots:
	public:
		QFSqlLineEdit(QWidget *parent = NULL);
		virtual ~QFSqlLineEdit();
};

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlForeignKeyLineEdit : public QFSqlLineEdit
{
	Q_OBJECT;
	/// ve tvaru "tablename.fieldname"
	Q_PROPERTY(QString referencedField READ referencedField WRITE setReferencedField);
	QF_FIELD_RW(QString, r, setR, eferencedField);
	/// pokud je vyplneno, je to jmeno fieldu z foreign table, ktery se zobracuje
	Q_PROPERTY(QString captionField READ captionField WRITE setCaptionField);
	QF_FIELD_RW(QString, c, setC, aptionField);
	/// pokud je prazdne, vytvori se automaticky,
	Q_PROPERTY(QString query READ query WRITE setQuery);
	QF_FIELD_RW(QString, q, setQ, uery);
	Q_PROPERTY(QString unknownIdCaption READ unknownIdCaption WRITE setUnknownIdCaption);
	QF_FIELD_RW(QString, u, setU, nknownIdCaption);
	protected:
		QFStringMap f_captionMap;
	protected:
		const QFStringMap& captionMap();
		QString idToCaption(const QVariant& fk_id);
	public:
		virtual void loadValue(const QString &sql_id = QString());
		virtual void flushValue();
		
		void clearCache();
	public:
		QFSqlForeignKeyLineEdit(QWidget *parent = NULL);
		//virtual ~QFSqlLineEdit();
};

#endif // QFSQLLINEEDIT_H

