
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLENUMITEMSEDITORITEMEDITOR_H
#define QFSQLENUMITEMSEDITORITEMEDITOR_H


#include <qfdbapplication.h>

#include <QDialog>

namespace Ui {class QFSqlEnumItemsEditorItemEditor;};

//! TODO: write class documentation.
class  QFSqlEnumItemsEditorItemEditor : public QDialog
{
	Q_OBJECT;
	private:
		Ui::QFSqlEnumItemsEditorItemEditor *ui;
	protected:
		QFDbEnum editedEnum;
		int f_maxGroupIdLength;
		QStringList f_currentGroupIds;
	protected slots:
		void on_edCaption_textChanged();
	public:
		QFDbEnum dbEnum();
	public:
		QFSqlEnumItemsEditorItemEditor(const QFDbEnum &de, const QStringList &current_group_ids, QWidget *parent = NULL);
		virtual ~QFSqlEnumItemsEditorItemEditor();
};

#endif // QFSQLENUMITEMSEDITORITEMEDITOR_H

