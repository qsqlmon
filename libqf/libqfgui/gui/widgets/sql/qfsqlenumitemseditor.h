
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLENUMITEMSEDITOR_H
#define QFSQLENUMITEMSEDITOR_H

#include <qfdialogwidget.h>

#include <QStandardItemModel>

class QFDbEnum;

namespace Ui {class QFSqlEnumItemsEditor;};

//! TODO: write class documentation.
class  QFSqlEnumItemsEditor : public QFDialogWidget
{
	Q_OBJECT;
	private:
		Ui::QFSqlEnumItemsEditor *ui;
	protected:
		enum {UserTextRole = Qt::UserRole+1};
		class Model;
		Model *model;
		//QPointer<QWidget> controlWidget;
		QString groupName;
	protected:
		int selectedRow();
		QFDbEnum selectedEnum();
		QStringList currentGroupIds();
	protected slots:
		void on_btAddItem_clicked();
		void on_btEditItem_clicked();
		void on_btMoveUpItem_clicked();
		void on_btMoveDownItem_clicked();
	public:
		void load();
		void save();
	public:
		QFSqlEnumItemsEditor(QString group_name, QWidget *parent = NULL);
		virtual ~QFSqlEnumItemsEditor();
};

class  QFSqlEnumItemsEditor::Model : public QStandardItemModel
{
	Q_OBJECT;
	public:
		Model(QObject *parent = NULL) : QStandardItemModel(parent) {}
		//virtual ~QFSqlEnumItemsEditor();
};

#endif // QFSQLENUMITEMSEDITOR_H

