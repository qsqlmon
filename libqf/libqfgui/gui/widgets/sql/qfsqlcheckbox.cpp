
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqlcheckbox.h"

#include <qfdataformdocument.h>

#include <qflogcust.h>

QFSqlCheckBox::QFSqlCheckBox(QWidget *parent)
	: QCheckBox(parent), QFSqlControl(this)
{
	setReadOnly(false);
	connect(this, SIGNAL(stateChanged(int)), this, SLOT(flushValue()));
}

QFSqlCheckBox::~QFSqlCheckBox()
{
}

bool QFSqlCheckBox::hitButton(const QPoint &pos) const
{
	if(isReadOnly()) return false;
	return QCheckBox::hitButton(pos);
}

void QFSqlCheckBox::loadValue(const QString &sql_id)
{
	if(!checkSqlId(sql_id)) return;
	QFDataFormDocument *doc = document();
	if(!doc) return;
	qfTrash() << QF_FUNC_NAME << sql_id << doc->value(sqlId()).toString();
	bool val = doc->value(sqlId()).toBool();
	//qfInfo() << "loaded value" << doc->value(sqlId()).toString();
	//qfInfo() << "loaded value" << val;
	Qt::CheckState state = (val)? Qt::Checked: Qt::Unchecked;
	loadingState = true;
	setCheckState(state);
	loadingState = false;
	QFSqlControl::loadValue(sql_id);
}

void QFSqlCheckBox::setWidgetReadOnly(bool b)
{
	qfTrash() << QF_FUNC_NAME << sqlId() << b;
	setReadOnly(b);
	//setEnabled(!b);
}

void QFSqlCheckBox::flushValue()
{
	qfTrash() << QF_FUNC_NAME;
	bool val = (checkState() == Qt::Unchecked)? false: true;
	//qfInfo() << "flush value" << val;
	if(!loadingState) {
		QFDataFormDocument *doc = document();
		if(!doc || doc->isEmpty()) return;
		emit valueUpdated(sqlId(), val);
	}
	//qfInfo() << "doc value" << document()->value(sqlId()).toString();
}
/*
void QFSqlCheckBox::checkStateChanged(int new_state)
{
	qfTrash() << QF_FUNC_NAME << new_state;
	emit valueUpdated(sqlId(), (new_state == Qt::Unchecked)? false: true);
}
*/

