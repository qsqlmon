message(including module 'sqlwidgets')

#	$$PWD/qfsqlwidgetinterface.h

FORMS +=      \
	$$PWD/qfsqlenumitemseditor.ui    \
	$$PWD/qfsqlenumitemseditoritemeditor.ui   \

HEADERS +=                           \
	$$PWD/qfsqllineedit.h                        \
	$$PWD/qfsqldateedit.h                      \
	$$PWD/qfsqleditwithbutton.h                     \
	$$PWD/qfsqlcontrol.h                    \
	$$PWD/qfsqlenum.h                   \
	$$PWD/qfsqlxmlconfigwidget.h                  \
	$$PWD/qfsqltableviewwidget.h                 \
	$$PWD/qfsqlenumset.h                \
	$$PWD/qfsqldoublespinbox.h               \
	$$PWD/qfsqlspinbox.h              \
	$$PWD/qfsqltextedit.h             \
	$$PWD/qfsqltableview.h            \
	$$PWD/qfsqlcheckbox.h           \
	$$PWD/qfsqlcombobox.h          \
	$$PWD/qfsqldatetimeedit.h         \
	$$PWD/qfsqltimeedit.h        \
	$$PWD/qfsqlforeignkeycombo.h       \
	$$PWD/qfsqlenumcombo.h      \
	$$PWD/qfsqleditforeignitemsbutton.h     \
	$$PWD/qfsqlenumitemseditor.h    \
	$$PWD/qfsqlenumitemseditoritemeditor.h   \
	$$PWD/qfsqlforeignkeycontrolitemseditor.h  \

SOURCES +=                           \
	$$PWD/qfsqllineedit.cpp                        \
	$$PWD/qfsqldateedit.cpp                      \
	$$PWD/qfsqleditwithbutton.cpp                     \
	$$PWD/qfsqlcontrol.cpp                    \
	$$PWD/qfsqlenum.cpp                   \
	$$PWD/qfsqlxmlconfigwidget.cpp                  \
	$$PWD/qfsqltableviewwidget.cpp                 \
	$$PWD/qfsqlenumset.cpp                \
	$$PWD/qfsqldoublespinbox.cpp               \
	$$PWD/qfsqlspinbox.cpp              \
	$$PWD/qfsqltextedit.cpp             \
	$$PWD/qfsqltableview.cpp            \
	$$PWD/qfsqlcheckbox.cpp           \
	$$PWD/qfsqlcombobox.cpp          \
	$$PWD/qfsqldatetimeedit.cpp         \
	$$PWD/qfsqltimeedit.cpp        \
	$$PWD/qfsqlforeignkeycombo.cpp       \
	$$PWD/qfsqlenumcombo.cpp      \
	$$PWD/qfsqleditforeignitemsbutton.cpp     \
	$$PWD/qfsqlenumitemseditor.cpp    \
	$$PWD/qfsqlenumitemseditoritemeditor.cpp   \
	$$PWD/qfsqlforeignkeycontrolitemseditor.cpp  \


