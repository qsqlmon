
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqlenum.h"

#include <qfsqlquery.h>
#include <qfdbapplication.h>
#include <qfdataformdocument.h>

#include <qflogcust.h>

//=======================================================
//                                           QFSqlEnumList
//=======================================================
QFSqlEnumList::QFSqlEnumList(QWidget * parent)
	: QFSqlListBox(parent)
{
	//setEditable(false);
}

void QFSqlEnumList::loadItems()
{
	if(count() == 0) {
		qfTrash() << QF_FUNC_NAME;
		loadingState = true;
		/*
		QFSqlQuery q(qfDbApp()->connection());
		QString s = "SELECT groupId, caption FROM enumz WHERE groupName='%1' ORDER BY pos";
		s = s.arg(enumGroup());
		qfTrash() << "\t" << s;
		q.exec(s);
		while(q.next()) {
			s = q.value("groupId").toString();
			addItem(q.value("caption").toString(), s);
		}
		*/
		QFDbEnumCache::EnumList lst = qfDbApp()->dbEnumsForGroup(enumGroup());
		foreach(QFDbEnum e, lst) {
			QString s = e.groupId();
			QString caption = e.caption();
			QString fmt = itemCaptionFormat();
			if(!fmt.isEmpty()) {
				caption = fmt;
				caption.replace("${groupName}", e.groupName());
				caption.replace("${groupId}", e.groupId());
				caption.replace("${pos}", QString::number(e.pos()));
				caption.replace("${abbreviation}", e.abbreviation());
				caption.replace("${value}", e.value().toString());
				caption.replace("${caption}", e.caption());
			}
			addItem(caption, s);
		}
		loadingState = false;
	}
}
/*
		QString groupName() const {return values.value(0).toString();}
		QString groupId() const {return values.value(1).toString();}
		int pos() const {return values.value(2).toInt();}
		QString abbreviation() const {return values.value(3).toString();}
		QVariant value() const {return values.value(4);}
		QString caption() const {return values.value(5).toString();}
*/
//=======================================================
//                                           QFSqlForeignKeyList
//=======================================================
#if 0
const QString QFSqlForeignKeyList::referencedFieldPlaceHolder = "${referencedField}";
const QString QFSqlForeignKeyList::referencedTablePlaceHolder = "${referencedTable}";
const QString QFSqlForeignKeyList::captionFieldPlaceHolder = "${captionField}";

QFSqlForeignKeyList::QFSqlForeignKeyList(QWidget * parent)
	: QFSqlListBox(parent)
{
	setEditable(false);
}

void QFSqlForeignKeyList::loadItems()
{
	if(count() == 0) {
		qfTrash() << QF_FUNC_NAME << objectName();
		loadingState = true;
		QFString tblname, fldname, capname;
		QFSql::parseFullName(referencedField(), &fldname, &tblname);
		capname = captionField();
		if(!capname) capname = fldname;
		qfTrash() << "\t capname:" << capname;
		QFString query_str = query();
		if(!query_str) {
			QFSqlQueryBuilder b;
			b.select(fldname);
			if(capname != fldname) b.select(capname);
			b.from(tblname);
			b.orderBy(capname);
			query_str = b.toString();
		}
		else {
			query_str = query_str.replace(referencedFieldPlaceHolder, fldname);
			query_str = query_str.replace(referencedTablePlaceHolder, tblname);
			query_str = query_str.replace(captionFieldPlaceHolder, capname);
		}
		query_str = query_str.trimmed();
		if(!query_str.isEmpty()) {
			QFSqlQuery q(qfDbApp()->connection());
			//checkLoadItemsQuery(b);
			qfTrash() << "\t" << query_str;
			qfTrash() << "\t capname:" << capname;
			q.exec(query_str);
			while(q.next()) {
				QString caption = q.value(capname).toString();
				QString id = q.value(fldname).toString();
				qfTrash() << "\t adding item:" << caption << id;
				addItem(caption, id);
			}
		}
		loadingState = false;
	}
}
#endif


