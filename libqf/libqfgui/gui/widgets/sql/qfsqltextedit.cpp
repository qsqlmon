
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqltextedit.h"

#include <qflogcust.h>
#include <qfdataformdocument.h>
#include <qfdataformwidget.h>

QFSqlTextEdit::QFSqlTextEdit(QWidget *parent)
	: QTextEdit(parent), QFSqlControl(this)
{
	setAcceptRichText(false);
	connect(this, SIGNAL(textChanged()), this, SLOT(updateStyleSheet()));
}

QFSqlTextEdit::~QFSqlTextEdit()
{
}

void QFSqlTextEdit::loadValue(const QString &sql_id)
{
	if(!checkSqlId(sql_id)) return;
	QFDataFormDocument *doc = QFSqlControl::document();
	if(!doc) return;
	loadingState = true;
	setPlainText(doc->value(sqlId()).toString());
	updateStyleSheet();
	loadingState = false;
	QFSqlControl::loadValue(sql_id);
}

void QFSqlTextEdit::setWidgetReadOnly(bool b)
{
	qfTrash() << QF_FUNC_NAME << sqlId() << b;
	setReadOnly(b);
	//setEnabled(!b);
}

void QFSqlTextEdit::flushValue()
{
	updateStyleSheet();
	if(!loadingState) emit valueUpdated(sqlId(), toPlainText());
}

void QFSqlTextEdit::focusOutEvent(QFocusEvent *ev)
{
	//qfInfo() << QF_FUNC_NAME;
	if(!isReadOnly()) flushValue();
	QTextEdit::focusOutEvent(ev);
}

void QFSqlTextEdit::updateStyleSheet()
{
	qfLogFuncFrame() << sqlId() << "isMandatory:" << isMandatory() << "isReadOnly:" << isReadOnly();// << "characterCount():" << QTextEdit::document()->characterCount();
	if(!designedStyleSheet.isValid()) {
		QString ss = styleSheet().simplified();
		designedStyleSheet = ss;
	}
	QString new_ss = designedStyleSheet.toString();
	if(isMandatory()) {
		if(!isReadOnly() && QTextEdit::document()->characterCount() <= 1) { /// z nejakyho duvodu ma prazdnej textedit 1 znak
			new_ss = designedStyleSheet.toString() + "\nQTextEdit {background-color:" + QFDataFormWidget::MandatoryWidgetBackgroundColor + "}";
		}
	}
	setStyleSheet(new_ss);
}
