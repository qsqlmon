
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqltableview.h"

#include <qfdataformdocument.h>
#include <qfsqlquerytablemodel.h>

#include <qflogcust.h>

QFSqlTableView::QFSqlTableView(QWidget *parent)
	: QFTableView(parent), QFSqlControl(this)
{
	setSqlIdPlaceHolder("${ID}");
}

QFSqlTableView::~QFSqlTableView()
{
}
/*
void QFSqlTableView::setSqlIdUsed(bool b)
{
	if(!b) {
		setSqlId("sqlid:ignored");
	}
	f_sqlIdUsed = b;
}
*/
void QFSqlTableView::loadValue(const QString &sql_id)
{
	qfLogFuncFrame() << "sql_id:" << sql_id << "sqlId():" << sqlId() << "object name:" << objectName();
	if(!checkSqlId(sql_id)) return;
	qfTrash() << "\t passed check";
	QFDataFormDocument *doc = document();
	if(!doc) return;
	QFSqlQueryTableModel *m = qobject_cast<QFSqlQueryTableModel*>(model(!Qf::ThrowExc));
	if(!m) {
		m = new QFSqlQueryTableModel(this);
		setModel(m);
	}
	QFSqlQueryTable *t = dynamic_cast<QFSqlQueryTable*>(model()->table());
	QF_ASSERT(t, "table is not a QFSqlQueryTable");
	QVariant v = doc->value(sqlId());
	/// pravdepodobne jsem v insertu a id jeste nema hodnotu
	if(v.type() == QVariant::Invalid) v = -1;
	//qfTrash() << "\tvalue type:" << v.type();
	QString qs = query();
	if(qs.isEmpty()) {
		/// pokud neni query(), vezmi ji z modelu
		qs = m->sqlTableViewQuery();
		if(qs.isEmpty()) {
			/// pokud neni query(), vezmi ji z tabulky
			qs = t->queryBuilder().toString();
		}
		setQuery(qs); /// takhle muzu dat query s placeholderem do table, pri prvnim nacteni se query ulozi a place holder se muze pouzivat dal
	}
	qfTrash() << "\tqs:" << qs;
	qs.replace(sqlIdPlaceHolder(), v.toString());
	qfTrash() << "\tquery:" << qs;
	t->setQuery(qs);
	QFSqlControl::loadValue(sql_id);

	reload();

}

void QFSqlTableView::selectId(const QVariant &id)
{
	qfTrash() << QF_FUNC_NAME << "id:" << id.toString() << "idColumnName:" << idColumnName();
	if(!id.isValid()) return;
	if(idColumnName().isEmpty()) return;
	QFTableModel *m = model(!Qf::ThrowExc);
	if(!m) return;
	for(int i=0; i<m->rowCount(); i++) {
		//qfTrash() << "\ttrying:" << m->value(i, idColumnName()).toString();
		if(m->value(i, idColumnName()) == id) {
			qfTrash() << "\tOK";
			QModelIndex ix = m->index(i, 0, QModelIndex());
			qfTrash() << "\tOK index row:" << ix.row() << "col:" << ix.column();
			setCurrentIndex(ix);
			break;
		}
	}
}

QFSqlQueryTableModel* QFSqlTableView::model(bool throw_exc) const throw(QFException)
{
	QFSqlQueryTableModel* ret = qobject_cast<QFSqlQueryTableModel*>(QFTableView::model(throw_exc));
	if(!ret && throw_exc) QF_EXCEPTION("Model is NULL or not a type of QFSqlQueryTableModel.");
	return ret;
}

void QFSqlTableView::setWidgetReadOnly(bool b)
{
	//qfLogFuncFrame() << "objectname:" << objectName()  << "newval:" << b << "f_readOnly:" << f_readOnly << "f_dataReadOnly:" << f_dataReadOnly;
	setReadOnly(b);
	refreshActions();
}

bool QFSqlTableView::isReadOnly() const
{
	//qfLogFuncFrame() << "objectname:" << objectName() << "f_readOnly:" << f_readOnly;
	return QFTableView::isReadOnly() || isDataReadOnly();
}
