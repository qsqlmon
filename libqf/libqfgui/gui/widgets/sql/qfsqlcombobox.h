
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLCOMBOBOX_H
#define QFSQLCOMBOBOX_H

#include "qfsqlcontrol.h"

#include <qfguiglobal.h>

#include <QComboBox>
#include <QLineEdit>

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlListBox : public QComboBox, public QFSqlControl
{
	Q_OBJECT;

	Q_PROPERTY(QString sqlId READ sqlId WRITE setSqlId);
	Q_PROPERTY(bool dataReadOnly READ isDataReadOnly WRITE setDataReadOnly);
	Q_PROPERTY(QString editGrant READ editGrant WRITE setEditGrant);
	Q_PROPERTY(bool ignoreClosedForEdits READ ignoreClosedForEdits WRITE setIgnoreClosedForEdits);
	Q_PROPERTY(bool externalData READ isExternalData WRITE setExternalData);
	Q_PROPERTY(bool valueRestrictedToItems READ isValueRestrictedToItems WRITE setValueRestrictedToItems);
	QF_FIELD_RW(bool, is, set, ValueRestrictedToItems);
	
	protected:
		bool f_readOnly;
	public:
		bool isReadOnly() const {return f_readOnly;}
		virtual void setReadOnly(bool ro);	
	private slots:
		//void onActivated(int index);
	public slots:
		virtual void loadValue(const QString &sql_id = QString());
		virtual void flushValue();

		/// pokusi se najit val a nastavit na ni current index.
		virtual void setCurrentValue(const QVariant &val);
	signals:
		void valueUpdated(const QString &sql_id, const QVariant &val);
	protected:
		virtual void setWidgetReadOnly(bool b) {setReadOnly(b);}
	protected:
		virtual void showPopup();
		virtual void wheelEvent ( QWheelEvent * event ); 
	public:
		//! defaultni implemantace nedela nic, kazdopadne pretizena funkce musi u vsech item krome textu nastavit i data, ta jsou emitovana ve funkci flushValue()
		virtual void loadItems() {}
	public:
		QFSqlListBox(QWidget *parent = NULL);
		//virtual ~QFSqlListBox();
};

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlComboBox : public QFSqlListBox
{
	Q_OBJECT;
	public:
		virtual void setReadOnly(bool ro);
	public:
		QFSqlComboBox(QWidget *parent = NULL);
};

#endif // QFSQLCOMBOBOX_H

