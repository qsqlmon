
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfsqlforeignkeycombo.h"
#include "qfsqlforeignkeycontrolitemseditor.h"

#include <qfsqleditforeignitemsbutton.h>
#include <qfsql.h>
#include <qfsqlquerybuilder.h>
#include <qfsqlquery.h>
#include <qfdbapplication.h>
#include <qfgui.h>
#include <qftableview.h>
#include <qftablemodel.h>
#include <qfbuttondialog.h>
#include <qftableviewtoolbar.h>
#include <qfdataformwidget.h>

#include <QVBoxLayout>
#include <QKeyEvent>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <QTimer>

#include <qflogcust.h>

//=========================================================================
//                             QFSqlEnumComboStandardItemsEditor
//=========================================================================
QFSqlEnumComboStandardItemsEditor::QFSqlEnumComboStandardItemsEditor(QWidget * parent, Qt::WFlags f)
	: QFSqlForeignKeyControlItemsEditor(parent, f)
{
	f_view = NULL;
	setXmlConfigPersistentId("QFSqlForeignKeyCombo/itemsEditor");
	QVBoxLayout *ly = new QVBoxLayout();
	ly->setSpacing(0);
	centralWidget()->setLayout(ly);
	qfTrash() << "\t new layout set";
	QFTableViewToolBar *tb = new QFTableViewToolBar();
	ly->addWidget(tb);
/*
	QDialogButtonBox *bx = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	bx->setObjectName("StandardDialogButtons");
	ly->addWidget(bx);
	connect(bx, SIGNAL(accepted()), this, SLOT(accept()));
	connect(bx, SIGNAL(rejected()), this, SLOT(reject()));
	QPushButton *bt = new QFSqlForeignKeyComboItemsEditorButton(tr("Vybrat"));
		//bt->setDefault(false);
		//bt->setAutoDefault(false);
	ly->addWidget(bt);
	connect(bt, SIGNAL(clicked()), f_itemsEditor, SLOT(accept()));
		*/
}

void QFSqlEnumComboStandardItemsEditor::reload()
{
	if(tableModel()) tableModel()->reload();
}

QFTableModel * QFSqlEnumComboStandardItemsEditor::tableModel()
{
	if(tableView()) return qobject_cast<QFTableModel*>(tableView()->model());
	return NULL;
}

void QFSqlEnumComboStandardItemsEditor::setTableView(QFTableView * view)
{
	SAFE_DELETE(f_view);
	if(view) {
		QBoxLayout *ly = qobject_cast<QBoxLayout*>(centralWidget()->layout());
		if(ly) {
			ly->insertWidget(1, view);
			QFTableViewToolBar *tb = centralWidget()->findChild<QFTableViewToolBar*>();
			if(tb) connect(view, SIGNAL(connectRequest(QFTableView*)), tb, SLOT(connectTableView(QFTableView*)));
		}
	}
	f_view = view;
}

void QFSqlEnumComboStandardItemsEditor::keyPressEvent(QKeyEvent * e)
{
	/// pokud je v tabulce otevreny editor a ja dam enter, zavre mi to dialog.
	/// je to asi tim, ze editor widget, ktery vrati item delegate pri svem uzavreni prepise accepted flag eventu na True. 
	/// hral jsem si s tim cely dopoledne a nepodarilo se mi to vyresit pomoci uprav QFTableView::keyPressEvent()
	/// i kdyz tato funkce nastavi e->setAccepted(), vola se stejne tato funkce pro dialog
	/// pokud se to same udela v pripade, ze neni otevren editor, nevola se nic 
	if(e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter) return;
	QFSqlForeignKeyControlItemsEditor::keyPressEvent(e);
}
/*
QDialogButtonBox * QFSqlEnumComboStandardItemsEditor::dialogButtons()
{
	QDialogButtonBox *bx = centralWidget()->findChild<QDialogButtonBox*>("StandardDialogButtons");
	return bx;
}

void QFSqlEnumComboStandardItemsEditor::setButtonsVisible(bool visible)
{
	QDialogButtonBox *bx = dialogButtons();
	if(bx) bx->setVisible(visible);
}
*/
//=========================================================================
//                             QFSqlForeignKeyComboBase
//=========================================================================
const QString QFSqlForeignKeyComboBase::referencedFieldPlaceHolder = "${referencedField}";
const QString QFSqlForeignKeyComboBase::referencedTablePlaceHolder = "${referencedTable}";
const QString QFSqlForeignKeyComboBase::captionFieldPlaceHolder = "${captionField}";
		
QFSqlForeignKeyComboBase::QFSqlForeignKeyComboBase(QWidget *parent)
	: QFSqlListBox(parent)
{
	f_itemModel = NULL;
	f_itemFilterProxyModel = NULL;
	ignoreFocusOut = false;
	itemsLoaded = false;
	textEdited = false;
	setValueRestrictedToItems(true);
	//setModel(f_itemModel);

	setInsertPolicy(QComboBox::NoInsert);

	connect(this, SIGNAL(editTextChanged( const QString& )), this, SLOT(onEditTextChanged()));
			
	/// tohle tu musi bejt, protoze jinak se combo snazi najit button, kdyz jeste neni vytvoreny UI
	//connect(this, SIGNAL(setEditItemsButtonRequest()), this, SLOT(setEditItemsButton()));
	QTimer::singleShot(0, this, SLOT(setEditItemsButton()));
}

QFSqlForeignKeyComboBase::~QFSqlForeignKeyComboBase()
{
}

QStandardItemModel *QFSqlForeignKeyComboBase::itemModel()
{
	if(!f_itemModel) {
		setItemModel(new QStandardItemModel(0, 1, this));
	}
	return f_itemModel;
}

QSortFilterProxyModel *QFSqlForeignKeyComboBase::itemFilterProxyModel()
{
	if(!f_itemFilterProxyModel) {
		setItemFilterProxyModel(new QSortFilterProxyModel(this));
	}
	return f_itemFilterProxyModel;
}

void QFSqlForeignKeyComboBase::setItemModel(QStandardItemModel *m)
{
	f_itemModel = m;
	f_itemModel->setObjectName("itemModel");
}

void QFSqlForeignKeyComboBase::setItemFilterProxyModel(QSortFilterProxyModel *m)
{
	f_itemFilterProxyModel = m;
	f_itemFilterProxyModel->setSourceModel(itemModel());
	setModel(f_itemFilterProxyModel);
}

void QFSqlForeignKeyComboBase::flushValue()
{
	qfLogFuncFrame() << sqlId();
	if(!loadingState) {
		QFDataFormDocument *doc = document();
		if(doc && !doc->isEmpty()) {
			QVariant v = currentValue();
			if(!v.isValid()) v = ""; /// pokud je sloupec, kam se to uklada NOT NULL, invalidni variant spusobi ulozeni NULL hodnoty a mysql se posere, kupodivu UPDATE t SET cislo='' WHERE id=4 ji nevadi a da tam 0
			qfTrash() << "\t emiting valueUpdated()" << sqlId() << v.toString();
			emit valueUpdated(sqlId(), v);
		}
	}
	textEdited = false;
}

QVariant QFSqlForeignKeyComboBase::currentValue()
{
	int ix = currentIndex();
	qfTrash() << "\t currentIndex:" << ix;
	QVariant v;
	if(isEditable()) {
		/// muze se stat, ze spravny item doplnil completer v editboxu a current item je jinej
		QString s = lineEdit()->text().toLower();
		int ix2 = itemCaptions.value(s, -1);
		qfTrash() << "\t editor text:" << s << "row_no:" << ix2;
		if(ix2 < 0) {
			if(isValueRestrictedToItems()) {
				v = itemModel()->data(itemModel()->index(0, 0), Qt::UserRole); ///vyber prvni item
			}
			else {
				/// je to text, ktery nepochazi z ciselniku
				v = lineEdit()->text();
			}
			
		}
		else {
			///ix2 je radek itemModel()u
			v = itemModel()->data(itemModel()->index(ix2, 0), Qt::UserRole);
		}
	}
	else {
		if(ix < 0) {
			/// neni vybrana zadna polozka
		}
		else {
			v = itemData(ix);
			qfTrash() << "\t row:" << ix << "item data:" << v.toString();
		}
	}
	return v;
}

void QFSqlForeignKeyComboBase::setCurrentValue(const QVariant & val)
{
	qfLogFuncFrame() << "sqlId:" << sqlId() << "val:" << val.toString() << "isEditable()" << isEditable();
	loadItems();
	textEdited = false;
	itemFilterProxyModel()->setFilterRegExp(QRegExp());
	int ix = -1;
	qfTrash() << "\t items count:" << count();
	for(int i=0; i<count(); i++) {
		QVariant v = itemData(i);
		qfTrash() << "\t checking:" << v.toString();
		if(v == val) {
			ix = i;
			break;
		}
	}
	if(ix < 0 && isEditable() && !isValueRestrictedToItems()) {
		qfTrash() << "\t setting current text to:" << val.toString();
		lineEdit()->setText(val.toString());
	}
	else {
		qfTrash() << "\t setting current index to:" << ix;
		setCurrentIndex(ix);
	}
	textEdited = false;
}

void QFSqlForeignKeyComboBase::clearValue()
{
	qfLogFuncFrame();
	setCurrentValue(QVariant());
	flushValue();
}

void QFSqlForeignKeyComboBase::showPopup()
{
	qfLogFuncFrame();
	//qfInfo() << "showPopup";
	qfTrash() << "\t item count:" << count();
	ignoreFocusOut = true;
	itemFilterProxyModel()->setFilterRegExp(QRegExp());
	/// filtruj jen kdyz nekdo poeditoval text, jinak se stane, ze se neukazi moznosti pokud nekdo je klikne na sipku a v okenku bude text nasteny z DB
	if(isEditable()) {
		qfTrash() << "\t isModified():" << lineEdit()->isModified();
		if(textEdited) {
			QString s = lineEdit()->text();
		/// oddelej text pridany completerem, je oznacen
			s = s.mid(0, s.length() - lineEdit()->selectedText().length());
			lineEdit()->setText(s);
			s = '*' + s + '*';
		//qfInfo() << s;
			QRegExp rx(s, Qt::CaseInsensitive, QRegExp::Wildcard);
			itemFilterProxyModel()->setFilterRegExp(rx);
		}
	}
	QFSqlListBox::showPopup();
}

void QFSqlForeignKeyComboBase::hidePopup()
{
	ignoreFocusOut = false;
	QFSqlListBox::hidePopup();
}

void QFSqlForeignKeyComboBase::setEditItemsButton()
{
	qfLogFuncFrame();
	QLayout *parent_layout = QFGui::findWidgetLayout(this);
	if(parent_layout) {
		for(int i=0; i<parent_layout->count(); i++) {
			QLayoutItem *lit = parent_layout->itemAt(i);
			if(QWidget *w = lit->widget()) {
				f_editItemsButton = qobject_cast<QFSqlEditForeignItemsButton*>(w);
				f_clearValueButton = qobject_cast<QFSqlClearValueButton*>(w);
				if(f_editItemsButton && f_clearValueButton) break;
			}
		}
	}
	qfTrash() << "\t f_editItemsButton:" << f_editItemsButton;
	if(f_editItemsButton) {
		f_editItemsButton->setText("...");
		connect(f_editItemsButton, SIGNAL(clicked()), this, SLOT(showItemsEditor()));
	}
	if(f_clearValueButton) {
		f_clearValueButton->setText(QString());
		connect(f_clearValueButton, SIGNAL(clicked()), this, SLOT(clearValue()));
	}
	if(f_editItemsButton || f_clearValueButton) {
		parent_layout->setSpacing(0);
	}
	//qfInfo() << "must be first" << f_editItemsButton;
	//disconnect(this, SIGNAL(setEditItemsButtonRequest()), this, SLOT(setEditItemsButton()));
	/// slot se vola casto az po te, co je widget nastaven RO v zavislosti na datech, oprav viditelnost buttonu
	setReadOnly(isReadOnly());
}

void QFSqlForeignKeyComboBase::reloadItems()
{
	QVariant v = currentValue();
	itemsLoaded = false;
	loadItems();
	setCurrentValue(v);
}

void QFSqlForeignKeyComboBase::setReadOnly(bool ro)
{
	qfLogFuncFrame() << ro;
	//qfInfo() << f_editItemsButton;
	QFSqlListBox::setReadOnly(ro);
	if(f_editItemsButton) {
		f_editItemsButton->setVisible(!ro);
	}
	if(f_clearValueButton) {
		f_clearValueButton->setVisible(!ro);
	}
}

#if 0
QFSqlForeignKeyComboBaseItemsEditor * QFSqlForeignKeyComboBase::itemsEditor()
{
	qfLogFuncFrame();
	if(!f_itemsEditor) {
		f_itemsEditor = new QFSqlForeignKeyComboBaseItemsEditorDialog(this);
	}
	return f_itemsEditor;
}
void QFSqlForeignKeyComboBase::setItemsEditor(QFSqlForeignKeyComboItemsEditor * dlg)
{
	f_itemsEditor = dlg;
}
#endif

/*
void QFSqlForeignKeyComboBase::showEvent(QShowEvent * ev)
{
	qfInfo() << "show";	
	QFSqlListBox::showEvent(ev);
	emit setEditItemsButtonRequest();
}
*/

void QFSqlForeignKeyComboBase::onEditTextChanged()
{
	textEdited = true;
	if(isValueRestrictedToItems()) {
		QString s = lineEdit()->text().toLower();
		bool found = itemCaptions.contains(s);
		//qfInfo() << s << found;
		//if(!found) foreach(QString cap, itemCaptions.keys()) qfInfo() << cap;
		QString ss = (found)? QString(): "background:" + QFDataFormWidget::MandatoryWidgetBackgroundColor;
		lineEdit()->setStyleSheet(ss);
	}
}

void QFSqlForeignKeyComboBase::focusOutEvent(QFocusEvent * e)
{
	/// tak tuhle funkci nemuzu pouzit, protoze mi kazi popup model
	if(!ignoreFocusOut) {
		//qfInfo() << "focus out";
		Q_UNUSED(e);
		flushValue();
	}
}

//=========================================================================
//                             QFSqlForeignKeyCombo
//=========================================================================
QMap<QString, QFSqlForeignKeyCombo::CachedItemList> QFSqlForeignKeyCombo::itemCache;

QFSqlForeignKeyCombo::QFSqlForeignKeyCombo(QWidget * parent)
	: QFSqlForeignKeyComboBase(parent)
{
	setEditItemsGrant("editForeignKeyItems");
}

void QFSqlForeignKeyCombo::removeItems()
{
	QFSqlForeignKeyComboBase::removeItems();
	itemCaptions.clear();
	itemsLoaded = false;
}

void QFSqlForeignKeyCombo::loadItems()
{
	if(!itemsLoaded) do {
		qfLogFuncFrame() << objectName();
		{
			QFDbApplication *db_app = qobject_cast<QFDbApplication*>(QCoreApplication::instance());
			if(!db_app) break;
			if(!db_app->connection().isOpen()) break;
		}
		loadingState = true;
		removeItems();
		CachedItemList cached_items = itemCache.value(cachedItemsKey());
		if(cached_items.isEmpty()) { 
			QFString tblname, fldname, capname;
			QFSql::parseFullName(referencedField(), &fldname, &tblname);
			capname = captionField();
			//if(!capname) capname = fldname;
			qfTrash() << "\t capname:" << capname;
			QFString query_str = query();
			if(!query_str) {
				QFSqlQueryBuilder b;
				b.select(fldname);
				if(capname != fldname) b.select(capname);
				b.from(tblname);
				b.orderBy(capname);
				query_str = b.toString();
			}
			else {
				query_str = query_str.replace(referencedFieldPlaceHolder, fldname);
				query_str = query_str.replace(referencedTablePlaceHolder, tblname);
				query_str = query_str.replace(captionFieldPlaceHolder, capname);
			}
			query_str = query_str.trimmed();
			if(!query_str.isEmpty()) {
				QFSqlQuery q(qfDbApp()->connection());
				qfTrash() << "\t query_str:" << query_str;
				QStringList caption_fields;
				capname = captionField();
				if(!itemCaptionFormat().isEmpty()) {
					capname = itemCaptionFormat();
					if(!capname.isEmpty()) {
						qfTrash() << "\t itemCaptionFormat:" << capname;
						QRegExp rx;
						rx.setPattern("\\$\\{([A-Za-z][A-Za-z0-9]*(\\.[A-Za-z][A-Za-z0-9]*)*)\\}");
						rx.setPatternSyntax(QRegExp::RegExp);
						int ix = 0;
						while((ix = rx.indexIn(capname, ix)) != -1) {
							qfTrash() << "\t caption:" << rx.cap(0) << rx.cap(1);
							caption_fields << rx.cap(1);
							ix += rx.matchedLength();
						}
					}
				}
				qfTrash() << "\t capname:" << capname;
				q.exec(query_str);
				while(q.next()) {
					QString caption = itemCaptionFormat();
					if(caption.isEmpty()) caption = q.value(captionField()).toString();
					else foreach(QString fld, caption_fields) {
						qfTrash() << "\t replacing caption field:" << fld;
						caption.replace("${" + fld + "}", q.value(fld).toString());
					}
					QVariant id = q.value(fldname);
					cached_items << CachedItem(caption, id);
					qfTrash() << "\t adding item:" << caption << id.toString();
				}
			}
		}
		int row_no = 0;
		foreach(const CachedItem &cit, cached_items) {
			//qfInfo() << "itemCaptions << " << cit.caption.toLower();
			itemCaptions[cit.caption.toLower()] = row_no;
			QStandardItem *it = new QStandardItem(cit.caption);
			itemModel()->setItem(row_no, it);
			QModelIndex ix = itemModel()->index(row_no, 0);
			if (ix.isValid()) { itemModel()->setData(ix, cit.id, Qt::UserRole); }
			//qfTrash() << "\t item model data:" << itemModel()->data(ix, Qt::UserRole).toString();
			//qfTrash() << "\t combo item data:" << itemData(row_no).toString();
			row_no++;
		}
		if(!cachedItemsKey().isEmpty()) itemCache[cachedItemsKey()] = cached_items;
		loadingState = false;
		itemsLoaded = true;
		qfTrash() << "\t item count:" << count();
	} while(false);
}

void QFSqlForeignKeyCombo::showItemsEditor()
{
	qfLogFuncFrame();
	QFSqlForeignKeyControlItemsEditor *dlg = itemsEditor();
	if(dlg) {
		QFTableView *view = dlg->tableView();
		if(view) {
			//if(!view->model(!Qf::ThrowExc)) view->setModel(itemsEditorModel());
			//qfInfo() << editItemsGrant() << qfApp()->currentUserHasGrant(editItemsGrant());
			view->setReadOnly(!qfApp()->currentUserHasGrant(editItemsGrant()));
		}
		//QFTableModel *m = itemsEditorModel();
		dlg->reload();
		bool accepted = (dlg->exec() == QDialog::Accepted);
		/// persistentni data musim ukladat rucne, protoze automaticky se ukladaji az v destruktoru dialogu
		dlg->savePersistentData();
		//if(view) view->savePersistentData();
		reloadItems();
		if(accepted && view) {
			QFBasicTable::Row r = view->selectedRow();
			if(!r.isNull()) {
				QVariant v = r.value(referencedField());
				//qfInfo() << v.toString();
				setCurrentValue(v);
			}
		}
		flushValue();
	}
}


