
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLFOREIGNKEYCONTROLITEMSEDITOR_H
#define QFSQLFOREIGNKEYCONTROLITEMSEDITOR_H

#include<qfguiglobal.h>
#include<qfbuttondialog.h>

class QFTableView;
class QFTableModel;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlForeignKeyControlItemsEditor : public QFButtonDialog
{
	Q_OBJECT;
	public:
		virtual void reload() = 0;
		virtual QFTableModel* tableModel() = 0;
		virtual QFTableView* tableView() = 0;

		virtual void savePersistentData();
	public:
		QFSqlForeignKeyControlItemsEditor(QWidget *parent = NULL, Qt::WFlags f = 0) : QFButtonDialog(parent, f) {}
};

#endif // QFSQLFOREIGNKEYCONTROLITEMSEDITOR_H

