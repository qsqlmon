
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLTABLEVIEW_H
#define QFSQLTABLEVIEW_H

#include "qfsqlcontrol.h"

#include <qfguiglobal.h>
#include <qftableview.h>

class QFSqlQueryTableModel;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlTableView : public QFTableView, public QFSqlControl
{
	Q_OBJECT

		Q_PROPERTY(QString sqlId READ sqlId WRITE setSqlId);
		Q_PROPERTY(bool dataReadOnly READ isDataReadOnly);
		Q_PROPERTY(QString editGrant READ editGrant WRITE setEditGrant);
		Q_PROPERTY(bool ignoreClosedForEdits READ ignoreClosedForEdits WRITE setIgnoreClosedForEdits);
		/// QFSqlTableView nemusi vzdy pouzivat sqlId, napr. kdyz je ciselnik, pokud mu ho ale nevyplnim, nepropoji se jeho signaly a sloty.
		/// Tato property rika vim o tom, ze sqlId neni vyplneno a je to v poradku, propoj tento widget 
		Q_PROPERTY(bool sqlIdNotUsed READ isSqlIdNotUsed WRITE setSqlIdNotUsed);
		/// Bohuzel to nemuzu nacpat do query() modelu, protoze mi nahradi vsechny placeholders pri prvnim reloadu.
		/// Sem se da napsat neco, jako select * from table where groupid=${ID}.
		Q_PROPERTY(QString query READ query WRITE setQuery);
		Q_PROPERTY(QString sqlIdPlaceHolder READ sqlIdPlaceHolder WRITE setSqlIdPlaceHolder);
		QF_FIELD_RW(QString, s, setS,  qlIdPlaceHolder);
		
		QF_FIELD_RW(bool, is, set, SqlIdNotUsed);
		// nekdy potrebuju pouze zamykani/odemykani widgetu
		//Q_PROPERTY(bool sqlIdUsed READ isSqlIdUsed WRITE setSqlIdUsed);
	protected:
		//bool f_sqlIdUsed;
		QString f_query;
	public:
		virtual QString query() const {return f_query;}
		void setQuery(const QString &q) {f_query = q.trimmed();}
		//bool isSqlIdUsed() const {return f_sqlIdUsed;}
		//void setSqlIdUsed(bool b);
		virtual bool isReadOnly() const;
	public slots:
		void loadValue(const QString &sql_id = QString());
		virtual void flushValue() {}
		void selectId(const QVariant &id);
	signals:
		void valueUpdated(const QString &sql_id, const QVariant &val);
	protected:
		virtual void setWidgetReadOnly(bool b);
	public:
		QFSqlQueryTableModel* model(bool throw_exc = Qf::ThrowExc) const throw(QFException);
	public:
		QFSqlTableView(QWidget *parent = NULL);
		virtual ~QFSqlTableView();
};

#endif // QFSQLTABLEVIEW_H

