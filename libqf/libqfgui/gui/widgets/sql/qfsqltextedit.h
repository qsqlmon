
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLTEXTEDIT_H
#define QFSQLTEXTEDIT_H

#include "qfsqlcontrol.h"

#include <qfguiglobal.h>

#include <QTextEdit>

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlTextEdit : public QTextEdit, public QFSqlControl
{
	Q_OBJECT;
	Q_PROPERTY(QString sqlId READ sqlId WRITE setSqlId);
	Q_PROPERTY(bool dataReadOnly READ isDataReadOnly WRITE setDataReadOnly);
	Q_PROPERTY(QString editGrant READ editGrant WRITE setEditGrant);
	Q_PROPERTY(bool ignoreClosedForEdits READ ignoreClosedForEdits WRITE setIgnoreClosedForEdits);
	Q_PROPERTY(bool mandatory READ isMandatory WRITE setMandatory);
	Q_PROPERTY(QString mandatoryErrorMessage READ mandatoryErrorMessage WRITE setMandatoryErrorMessage);
	protected:
		QVariant designedStyleSheet;
	protected slots:
		void updateStyleSheet();
	protected:
		virtual void setWidgetReadOnly(bool b);
	public slots:
		virtual void loadValue(const QString &sql_id = QString());
		virtual void flushValue();
	signals:
		void valueUpdated(const QString &sql_id, const QVariant &val);
	protected:
		virtual void focusOutEvent(QFocusEvent *event);
	public:
		QFSqlTextEdit(QWidget *parent = NULL);
		virtual ~QFSqlTextEdit();
};

#endif // QFSQLTEXTEDIT_H

