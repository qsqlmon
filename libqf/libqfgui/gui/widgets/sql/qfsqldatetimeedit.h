
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSQLDATETIMEEDIT_H
#define QFSQLDATETIMEEDIT_H

#include "qfsqlcontrol.h"

#include <qfguiglobal.h>

#include <QDateTimeEdit>

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlDateTimeEdit : public QDateTimeEdit, public QFSqlControl
{
	Q_OBJECT;
	Q_PROPERTY(QString sqlId READ sqlId WRITE setSqlId);
	Q_PROPERTY(bool dataReadOnly READ isDataReadOnly WRITE setDataReadOnly);
	Q_PROPERTY(QString editGrant READ editGrant WRITE setEditGrant);
	Q_PROPERTY(bool ignoreClosedForEdits READ ignoreClosedForEdits WRITE setIgnoreClosedForEdits);
	public slots:
		void loadValue(const QString &sql_id = QString());
		virtual void flushValue();
	signals:
		void valueUpdated(const QString &sql_id, const QVariant &val);
	protected:
		virtual void setWidgetReadOnly(bool b);
		virtual QString textFromDateTime(const QDateTime &dateTime) const;
	public:
		void setDateTime(const QDateTime &dt);
		QDateTime dateTime() const;
		bool isNull() const {return date() == minimumDate();}
		void setNull() {QDateTimeEdit::setDate(minimumDate());}
	public:
		QFSqlDateTimeEdit(QWidget *parent = NULL);
		virtual ~QFSqlDateTimeEdit();
};

#endif // QFSQLDATETIMEEDIT_H

