
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdialogwidgetframe.h"

#include <qfdialogwidget.h>
#include <qftoolbar.h>

#include <qflogcust.h>

QFDialogWidgetFrame::QFDialogWidgetFrame(QWidget *parent)
	: QFDialogWidgetStack(parent)
{
}

QFDialogWidgetFrame::~QFDialogWidgetFrame()
{
}
/*	
QBoxLayout* QFDialogWidgetFrame::boxLayout()
{
	qfTrash() << QF_FUNC_NAME << count();
	if(count() == 0) {
		insertWidget(0, new QWidget(this));
	}
	QWidget *w = currentWidget();
	qfTrash() << "\t w:" << w << "w ly:" << w->layout();
	QBoxLayout *ly = qobject_cast<QBoxLayout*>(w->layout());
	if(!ly) {
		ly = new QVBoxLayout(w);
		ly->setMargin(0);
		ly->setSpacing(1);
	}
	return ly;
}
*/
/*			
void QFDialogWidgetFrame::closeDialogWidget()
{
	QWidget *w = currentWidget();
	if(w) {
		w->hide();
		//removeWidget(w);
		//w->setParent(0);
		//qfTrash() << "\t current w:" << currentWidget() << "count:" << count();
		/// takhle mi to pada, protoze mazu objekt ve vlastnim signalu. deleteLater() nepomaha.
		//SAFE_DELETE(w);
		//killList << w;
	}
}
*/
void QFDialogWidgetFrame::setDialogWidget(QFDialogWidget *dw)
{
	qfTrash() << QF_FUNC_NAME;// << "caption:" << caption << "icon serial:" << icon.serialNumber();
	QFDialogWidget *tdw = topLevelDialogWidget();
	if(tdw) tdw->accept();
	//closeTopLevelDialogWidget();
	pushDialogWidget(dw);
}

