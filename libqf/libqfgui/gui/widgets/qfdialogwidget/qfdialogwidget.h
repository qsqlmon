
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDIALOGWIDGET_H
#define QFDIALOGWIDGET_H

#include <qfguiglobal.h>
#include <qfdialog.h>
#include <qfuibuilder.h>

#include <QWidget>
#include <QLabel>
#include <QToolButton>

class QPushButton;
class QToolBar;

class QFGUI_DECL_EXPORT QFDialogWidgetInterface
{
	public:
		//! volajici funkce je zodpovedna za dealokaci toolbaru (maji parent NULL).
		virtual QFPart::ToolBarList createToolBars() {return QFPart::ToolBarList();}
		virtual bool hasMenuOnToolBar() {return false;}
		//virtual bool hasMenuBar() {return false;}
		// pokud vrati true vola se createCaption(), pri vkladani widgetu do dialogu
		//virtual bool hasCaption() {return false;}
		// zahlavi widgetu, pokud je, zobrazi se mezi menu a toolbary.
		//virtual QWidget* createCaption() {return NULL;}
		virtual void setCaptionText(const QString &s) {Q_UNUSED(s);}
		virtual void setCaptionIcon(const QIcon &ico) {Q_UNUSED(ico);}
		//! @param menu_or_menubar je typu QWidget, protoze je to prvni spolecny predek QMenu a QMenuBar
		virtual void updateMenuOrBar(QWidget *menu_or_menubar) {Q_UNUSED(menu_or_menubar);}
		/// vyrobi na toolbaru \a tb button pro menu a naplni ho pomoci funkce \a updateMenuOrBar() .
		/// @return pointer na vytvoreny button.
		virtual QPushButton* createToolBarMenuButton(QWidget *parent = NULL);
	public:
		virtual ~QFDialogWidgetInterface() {}
};

class QFGUI_DECL_EXPORT QFDialogWidgetCaptionFrame : public QFrame
{
	Q_OBJECT;
	protected:
		QLabel *captionIconLabel;
		QLabel *captionLabel;
		QToolButton *closeButton;
		//QFDialog::ModalResult closeButtonResult;
	signals:
		void closeButtonClicked();
	public:
		void setText(const QString &s);
		QString text() const;
		void setIcon(const QIcon &ico);
		void setCloseButtonVisible(bool b = true) {
			closeButton->setVisible(b);
		}
		/*
		void setCloseButtonResult(QFDialog::ModalResult result) {
		closeButtonResult = result;
}
		*/
	public:
		QFDialogWidgetCaptionFrame(QWidget *parent = NULL);
};

/*!
Pokud takovy widget priradim QFDialog-u a funkce \a uiBuilder() nevrati NULL, vyrobi si z neho menu a toolbar.
    \code
	QFButtonDialog dlg;
	InsertPartsWidget *w = new InsertPartsWidget();
	dlg.setDialogWidget(w);
	int res = dlg.exec();
	if(res == QFDialog::Accepted) {
		QFMessage::information(w->partType());
	}
    \endcode
*/
class QFGUI_DECL_EXPORT QFDialogWidget : public QWidget, public QFDialogWidgetInterface, public QFXmlConfigPersistenter
{
	Q_OBJECT
	private:
		QFUiBuilder f_uiBuilder;
	protected:
		QFUiBuilder::ActionList f_actions;
		QPointer<QFDialogWidgetCaptionFrame> f_captionFrame;
		QWidget *f_centralWidget;
	protected:
		QString defaultCaptionText();
		QIcon defaultCaptionIcon();
	signals:
		//! @param result neco z QFDialog::ModalResult.
		void wantClose(QFDialogWidget* w, int result = -1);
	protected slots:
		void captionFrameCloseButtonClicked() {emit wantClose(this, QFDialog::Rejected);}
	public:
		virtual QFUiBuilder* uiBuilder() {return &f_uiBuilder;}
		QWidget *centralWidget() {return f_centralWidget;}

		//! pokud widget nema mit captionFrame staci pretizit tuto funkci, aby vracela NULL
		virtual QFDialogWidgetCaptionFrame* captionFrame();

		QFUiBuilder::ActionList actionList() const {return f_actions;}
		QFAction* action(const QString &name, bool throw_exc = Qf::ThrowExc) const throw(QFException)
		{
			return QFUiBuilder::findAction(actionList(), name, throw_exc);
		}

		//virtual QFUiBuilder* uiBuilder() {return NULL;}
		//=================== INTERFACE ================
		virtual QFPart::ToolBarList createToolBars();
		//virtual bool hasMenubar() {return false;}
		virtual void updateMenuOrBar(QWidget *menu_or_menubar);
		//virtual bool hasCaption() {return captionFrame != NULL;}
		//===============================================
	public slots:
		virtual void accept();
		virtual void reject();

		virtual void refreshActions();
		virtual void setCaptionText(const QString &s);
		QString captionText();
		virtual void setCaptionIcon(const QIcon &ico);
		void setCaptionCloseButtonVisible(bool b = true);
	public:
		//! Called by QFDialog::done()
		virtual bool canBeClosed(int done_result) {Q_UNUSED(done_result); return true;}
	public:
		QFDialogWidget(QWidget *parent = NULL);
		virtual ~QFDialogWidget();
};


#endif // QFDIALOGWIDGET_H

