
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDIALOGWIDGETSTACK_H
#define QFDIALOGWIDGETSTACK_H

#include <qfguiglobal.h>
#include <qfexception.h>

#include <QStackedWidget>

class QFDialogWidget;

//! dela stack z dialog widgetu, takze nahrazuje modalni okna, ma to vyhodu, ze toplevel widget nemusi byt modalni.
class QFGUI_DECL_EXPORT QFDialogWidgetStack : public QStackedWidget
{
	Q_OBJECT
	public:
		//const static bool ConnectWantClose = true;
	signals:
		//void topLevelDialogWidgetClosed(int result);
	public slots:
		// zavre toplevel widget
		//void closeDialogWidget() {closeDialogWidget(-1);}
		void closeDialogWidget(QFDialogWidget* w, int result);
		/// pokud widget ma slot save(), bude pred zavrenim zavolan.
		//virtual void saveAndCloseDialogWidget() throw(QFException);
		virtual void closeAllDialogWidgetsButCount(int leaved_widget_cnt, bool save_data) throw(QFException);
		void closeAllDialogWidgets(bool save_data) throw(QFException) {closeAllDialogWidgetsButCount(0, save_data);}
	public:
		/// naposledy pushnuty dialog widget nebo NULL.
		QFDialogWidget *topLevelDialogWidget();
		virtual void pushDialogWidget(QFDialogWidget *w);//, bool connect_want_close = !ConnectWantClose);
		/// zavre top widget, result se v teto implementaci nepouziva
		virtual void closeTopLevelDialogWidget(int result);
	public:
		QFDialogWidgetStack(QWidget *parent = NULL);
		virtual ~QFDialogWidgetStack();
};

#endif // QFDIALOGWIDGETSTACK_H

