
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDIALOGWIDGETFRAME_H
#define QFDIALOGWIDGETFRAME_H

#include <qfguiglobal.h>
#include <qfdialogwidgetstack.h>

class QBoxLayout;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFDialogWidgetFrame : public QFDialogWidgetStack
{
	Q_OBJECT
	protected:
		//QBoxLayout* boxLayout();
	public slots:
		//virtual void closeDialogWidget();
	public:
		virtual void setDialogWidget(QFDialogWidget *w);
	public:
		QFDialogWidgetFrame(QWidget *parent = NULL);
		virtual ~QFDialogWidgetFrame();
};
   
#endif // QFDIALOGWIDGETFRAME_H

