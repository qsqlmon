
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdialogwidget.h"

#include <qfpixmapcache.h>
#include <qfapplication.h>
#include <qfaction.h>

#include <QPushButton>
#include <QMenu>
#include <QVBoxLayout>

#include <qflogcust.h>

//================================================
//                     QFDialogWidgetInterface
//================================================
QPushButton* QFDialogWidgetInterface::createToolBarMenuButton(QWidget *parent)
{
	QPushButton *bt = NULL;
	if(hasMenuOnToolBar()) {
		qfTrash() << QF_FUNC_NAME;
		//QFrame *frm = new QFrame();
		//frm->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
		//frm->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
		//frm->setFrameStyle(QFrame::Box);
		//tb->addWidget(frm);
		bt = new QPushButton(QObject::tr("&Menu", "toolbar menu button"), parent);
		QCursor c = bt->cursor();
		c.setShape(Qt::PointingHandCursor);
		bt->setCursor(c);
		bt->setFlat(true);
		//tb->addWidget(bt);

		QMenu *m = new QMenu(bt);
		updateMenuOrBar(m);
		bt->setMenu(m);
	}
	return bt;
}

//================================================
//                     QFDialogWidget
//================================================
QFDialogWidget::QFDialogWidget(QWidget *parent)
	: QWidget(parent)
{
	f_uiBuilder.setCreatedActionsParent(this);
	f_captionFrame = NULL;
	f_centralWidget = new QWidget();
	QBoxLayout *ly = new QVBoxLayout(this);
	ly->setMargin(0);
	ly->setSpacing(1);
	ly->addWidget(f_centralWidget);
}

QFDialogWidget::~QFDialogWidget()
{
}

QFDialogWidgetCaptionFrame * QFDialogWidget::captionFrame()
{
	if(f_captionFrame == NULL) {
		qfLogFuncFrame() << "creating caption frame";
		f_captionFrame = new QFDialogWidgetCaptionFrame();
		f_captionFrame->setCloseButtonVisible(true);
		//connect(captionFrame, SIGNAL(closeButtonClicked()), this, SLOT(captionFrameCloseButtonClicked()));
		QBoxLayout *ly = qobject_cast<QBoxLayout*>(this->layout());
		ly->insertWidget(0, f_captionFrame);
		connect(f_captionFrame, SIGNAL(closeButtonClicked()), this, SLOT(captionFrameCloseButtonClicked()), Qt::QueuedConnection); /// abych nevolal delete na dialog widget v event handleru
		f_captionFrame->hide();
		
		setCaptionText(defaultCaptionText());
		setCaptionIcon(defaultCaptionIcon());
	}
	return f_captionFrame;
}

/*
QFPart::ToolBarList QFDialogWidget::createToolBars()
{
	QFToolBar *tool_bar = new QFToolBar(NULL);
	foreach(QFAction *a, my_actions) tool_bar->addAction(a);
	QFPart::ToolBarList ret;
	ret << tool_bar;
	return ret;
}
*/

QFPart::ToolBarList QFDialogWidget::createToolBars()
{
	qfLogFuncFrame();
	QFPart::ToolBarList tblst;
	QFUiBuilder *uib = uiBuilder();
	while(uib) {
		qfTrash() << "\t ui builder id:" << uib->uiId();
		QFPart::ToolBarList lst = uib->createToolBars();
		tblst = lst + tblst;
		uib = uib->prevUiBuilder();
	}
	return tblst;
}

void QFDialogWidget::updateMenuOrBar(QWidget * menu_or_menubar)
{
	qfLogFuncFrame();
	QList<QFUiBuilder*> uib_lst;
	QFUiBuilder *uib = uiBuilder();
	while(uib) {
		uib_lst.prepend(uib);
		uib = uib->prevUiBuilder();
	}
	foreach(QFUiBuilder *ui, uib_lst) {
		ui->updateMenuOrBar(menu_or_menubar);
	}
}
 
QString QFDialogWidget::defaultCaptionText()
{
	qfLogFuncFrame();
	QString ret = uiBuilder()->caption();
	qfTrash() << "\t return:" << ret;
	return ret;
}

QIcon QFDialogWidget::defaultCaptionIcon()
{
	return uiBuilder()->icon();
}

void QFDialogWidget::setCaptionText(const QString & s)
{
	if(!s.isEmpty()) {
		qfLogFuncFrame() << "captionFrame:" << f_captionFrame;
		if(captionFrame()) captionFrame()->setText(s);
	}
}

QString QFDialogWidget::captionText()
{
	QString ret;
	if(f_captionFrame) ret = captionFrame()->text();
	return ret;
}

void QFDialogWidget::setCaptionIcon(const QIcon & ico)
{
	if(!ico.isNull()) {
		if(captionFrame()) captionFrame()->setIcon(ico);
	}
}

void QFDialogWidget::setCaptionCloseButtonVisible(bool b)
{
	if(captionFrame()) captionFrame()->setCloseButtonVisible(b);
}

void QFDialogWidget::refreshActions()
{
	qfLogFuncFrame() << objectName();
	foreach(QFAction *a, actionList()) {
		//qfInfo() << a->id() << 
		bool enabled = qfApp()->currentUserHasGrant(a->grant());
		a->setEnabled(enabled);
	}
}

//======================================================
//                        QFDialogWidgetCaptionFrame
//======================================================
QFDialogWidgetCaptionFrame::QFDialogWidgetCaptionFrame(QWidget *parent)
	: QFrame(parent)
{
	qfLogFuncFrame();
	QPalette p = palette();
	p.setColor(QPalette::Window, QColor(245, 245, 184));
	setPalette(p);
	setAutoFillBackground(true);
	setFrameShape(QFrame::StyledPanel);
	setFrameShadow(QFrame::Raised);
	QBoxLayout *ly = new QHBoxLayout(this);
	ly->setMargin(0);
	//ly->setContentsMargins(5, 1, 5, 1);
	ly->setSpacing(6);
	captionIconLabel = new QLabel();
		//captionLabel->setPixmap(icon.pixmap(32));
	ly->addWidget(captionIconLabel);

	captionLabel = new QLabel();
	qfTrash() << "\t label:" << captionLabel;
	QFont f = captionLabel->font();
	f.setBold(true);
	f.setPointSize(14);
	captionLabel->setFont(f);
	ly->addWidget(captionLabel);
	ly->addStretch();
	closeButton = new QToolButton();
	closeButton->setIcon(QFPixmapCache::icon("close_window"));
	connect(closeButton, SIGNAL(clicked()), this, SIGNAL(closeButtonClicked()));
	closeButton->setAutoRaise(true);
	ly->addWidget(closeButton);
	//closeButton->setVisible(true);
	setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
	qfTrash() << "\t label:" << captionLabel;
}

void QFDialogWidgetCaptionFrame::setText(const QString & s)
{
	qfLogFuncFrame() << "text:" << s;
	qfTrash() << "\t label:" << captionLabel;
	captionLabel->setText(s);
	setVisible(!(captionLabel->text().isEmpty() && captionIconLabel->pixmap() == NULL));
}

QString QFDialogWidgetCaptionFrame::text() const
{
	return captionLabel->text();
}

void QFDialogWidgetCaptionFrame::setIcon(const QIcon & ico)
{
	captionIconLabel->setPixmap(ico.pixmap(32));
	setVisible(!(captionLabel->text().isEmpty() && captionIconLabel->pixmap() == NULL));
}

void QFDialogWidget::accept()
{
	qfLogFuncFrame();
	qfTrash() << "\t emitting wantClose";
	emit wantClose(this, QDialog::Accepted);
}

void QFDialogWidget::reject()
{
	qfLogFuncFrame();
	emit wantClose(this, QDialog::Rejected);
}
