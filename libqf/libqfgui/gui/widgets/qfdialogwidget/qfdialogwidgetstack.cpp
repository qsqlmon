
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdialogwidgetstack.h"

#include <qfdialogwidget.h>
#include <qftoolbar.h>

#include <QVBoxLayout>
#include <QMenuBar>
#include <QPushButton>

#include <qflogcust.h>

QFDialogWidgetStack::QFDialogWidgetStack(QWidget *parent)
	: QStackedWidget(parent)
{
}

QFDialogWidgetStack::~QFDialogWidgetStack()
{
}

void QFDialogWidgetStack::pushDialogWidget(QFDialogWidget *dw)//, bool connect_want_close)
{
	qfLogFuncFrame();// << "caption:" << caption << "icon serial:" << icon.serialNumber();
	if(dw) {
		QWidget *w = new QWidget();
		QBoxLayout *ly = new QVBoxLayout(w);
		ly->setMargin(0);
		ly->setSpacing(1);
		{
			QFDialogWidgetCaptionFrame *cf = dw->captionFrame();
			if(cf) {
				ly->addWidget(cf);
			}
		}
		/*
		if(dw->hasCaption()) {
			///caption
			QWidget *w = dw->createCaption();
			if(w) ly->addWidget(w);
		}
		*/
		{
			QMenuBar *mb = new QMenuBar(NULL);
			dw->updateMenuOrBar(mb);
			if(mb->actions().isEmpty()) { delete mb; }
			else { ly->addWidget(mb); }
		}

		QFPart::ToolBarList tool_bars = dw->createToolBars();
		int toolbars_cnt = tool_bars.count() + ((dw->hasMenuOnToolBar())? 1: 0);
		qfTrash() << "\ttoolbars cnt:" << toolbars_cnt;

		if(toolbars_cnt == 1) {
			if(!tool_bars.isEmpty()) {
				QFToolBar *tb = tool_bars[0];
				tb->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
				ly->addWidget(tb);
			}
			else if(dw->hasMenuOnToolBar()) {
				QFToolBar *tb = new QFToolBar();
				tb->addWidget(dw->createToolBarMenuButton());
				ly->addWidget(tb);
			}
		}
		else if(toolbars_cnt > 1) {
			/// toolbary dej vedle sebe
			//ly->setObjectName("boxLayout");
			QFrame *frm = new QFrame();
			QBoxLayout *ly1 = new QHBoxLayout(frm);
			ly1->setMargin(0);
			ly1->setObjectName("ly1");
			if(!tool_bars.isEmpty()) {
				foreach(QFToolBar *tb, tool_bars) {
					tb->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
					ly1->addWidget(tb);
				}
			}
			if(dw->hasMenuOnToolBar()) {
				//QFToolBar *tb = new QFToolBar();
				//tb->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
				QPushButton *bt = dw->createToolBarMenuButton();
				qfTrash() << "\ttool bar button:" << bt;
				//tb->addWidget(bt);
				ly1->addWidget(bt);
			}
			//ly1->addStretch();
			//ly->addLayout(ly1);
			ly->addWidget(frm);
			//ly->dumpObjectTree();
			//dumpObjectTree();
		}

		ly->addWidget(dw);
		addWidget(w);
		setCurrentWidget(w);
		/*
		if(connect_want_close) {
			connect(dw, SIGNAL(wantClose(QFDialogWidget*, int)), this, SLOT(closeDialogWidget(QFDialogWidget*, int)));
		}
		*/
		dw->refreshActions();
		/// QueuedConnection tu nemuze bejt, nefungovala by funkce closeAllDialogWidgetsButCount
		connect(dw, SIGNAL(wantClose(QFDialogWidget*, int)), this, SLOT(closeDialogWidget(QFDialogWidget*, int)));//, Qt::QueuedConnection);
	}
}

void QFDialogWidgetStack::closeTopLevelDialogWidget(int result)
{
	qfLogFuncFrame();
	//emit aboutToCloseTopLevelDialogWidget(result);
	//qfInfo() << QFLog::stackTrace();
	Q_UNUSED(result);
	//Q_UNUSED(_w);
	QWidget *w = currentWidget();
	//QFDialogWidget *dw = topDialogWidget();
	if(w) {
		//qfTrash() << "\t removing dialog widget name:" << dw->objectName() << "type:" << dw->metaObject()->className();
		/// dialog widget je umisten spolu s toolbary, menu a ostatnimi kramy ve Widgetu
		//QWidget *w = dw->parentWidget();
		removeWidget(w);
		delete w;
	}
}
/*
void QFDialogWidgetStack::saveAndCloseDialogWidget() throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	QWidget *w = currentWidget();
	if(w) {
		QFDialogWidget *dw = w->findChild<QFDialogWidget*>();
		if(dw) {
			//qfTrash() << "\tclosing object of type:" << dw->metaObject()->className();
			//qfTrash() << "\tindex of slot save():" << dw->metaObject()->indexOfMethod("save()");
			//qfTrash() << "\tinvoking slot save():" << ;
			QMetaObject::invokeMethod(dw, "save", Qt::DirectConnection);
		}
		removeWidget(w);
		delete w;
	}
}
*/
QFDialogWidget * QFDialogWidgetStack::topLevelDialogWidget()
{
	QWidget *w = currentWidget();
	if(!w) return NULL;
	QFDialogWidget *dw = w->findChild<QFDialogWidget*>();
	return dw;
}

void QFDialogWidgetStack::closeAllDialogWidgetsButCount(int leaved_widget_cnt, bool save_data) throw( QFException )
{
	qfLogFuncFrame() << "widget count:" << count();
	while(count() > leaved_widget_cnt) {
		int old_cnt = count();
		QFDialogWidget *dw = topLevelDialogWidget();
		if(dw) {
			if(save_data) dw->accept();
			else dw->reject();
		}
		if(count() == old_cnt) {
			/// accept() nebo reject() nezavreli, coz je chyba
			//QF_EXCEPTION
			qfWarning() << "accept() or reject() did not close widget:" << dw;
			break;
		}
	}
}

void QFDialogWidgetStack::closeDialogWidget(QFDialogWidget * w, int result)
{
	if(!w->canBeClosed(result)) return;
	closeTopLevelDialogWidget(result);
}
