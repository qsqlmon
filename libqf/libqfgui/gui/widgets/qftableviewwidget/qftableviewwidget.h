
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFTABLEVIEWWIDGET_H
#define QFTABLEVIEWWIDGET_H

#include <qfguiglobal.h>
#include <qftoolbar.h>
#include <qftableview.h>
#include <qfdialogwidget.h>

namespace Ui {
class QFTableViewWidget;
}

class QFTableModel;
class QFTableView;
class QFDataFormDialog;
class QToolBar;
class QFrame;

//! @todo write doc.
class QFGUI_DECL_EXPORT QFTableViewWidget : public QFDialogWidget
{
	Q_OBJECT
			Q_PROPERTY(bool statusLineVisible READ isStatusLineVisible WRITE setStatusLineVisible)
			Q_PROPERTY(bool toolBarVisible READ isToolBarVisible WRITE setToolBarVisible)
			Q_PROPERTY(bool readOnly READ isReadOnly WRITE setReadOnly)
			Q_PROPERTY(QFTableView::RowEditorMode rowEditorMode READ rowEditorMode WRITE setRowEditorMode)
	protected:
		QFTableView *f_tableView;
	public:
		bool isStatusLineVisible() const;
		void setStatusLineVisible(bool b = true);

		bool isToolBarVisible() const;
		void setToolBarVisible(bool b = true);

		bool isReadOnly() const {return tableView()->isReadOnly();}
		void setReadOnly(bool b) {tableView()->setReadOnly(b);}
	public:
		/// bez factory to fakt nejde, virtualni funkce nefunguji z konstruktoru
		class QFGUI_DECL_EXPORT TableViewFactory
		{
			public:
				virtual ~TableViewFactory() {}
				virtual QFTableView* createView() const;
		};
	protected:
		void createToolBar();
	public:
		virtual void setModel(QFTableModel *m);
		QFTableModel* model(bool throw_exc = Qf::ThrowExc) const;
		//void setExternalEditor(QFDataFormDialog *frm);
		//QFDataFormDialog* externalEditor() const;
		void setRowEditorMode(QFTableView::RowEditorMode _mode);
		QFTableView::RowEditorMode rowEditorMode() const;

		virtual QFTableView* tableView() const;

		virtual QFPart::ToolBarList createToolBars();
		//virtual QWidget* createCaption();
		//virtual bool hasCaption() {return true;}
		void setExternalRowEditorFactory(QFDataFormDialogWidgetFactory *fact);
		
		void setXmlConfigPersistentId(const QString &id, bool load_persistent_data = true);
	protected slots:
		void updateStatus();
		void on_btMenu_clicked();
	public slots:
		void setInfo(const QString &info);
		//void toggleMode();
	signals:
		void statusTextAction(const QString &status_text);
	private:
		Ui::QFTableViewWidget *ui;

		QFToolBar *toolBar;
		QFrame *captionFrame;
		//void createToolBar();
	public:
		// takhle muze mit table view widget jako tableView libovolneho potomka QFTableView.
		//QFTableViewWidget(const TableViewFactory &factory, QWidget *parent = 0);
		QFTableViewWidget(QWidget *parent = 0, const TableViewFactory &factory = TableViewFactory());
		virtual ~QFTableViewWidget();
};

#endif // QFTABLEVIEWWIDGET_H

