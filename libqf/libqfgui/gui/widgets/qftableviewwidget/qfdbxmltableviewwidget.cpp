
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdbxmltableviewwidget.h"

#include <qfsqlxmlconfigwidget.h>
#include <qfdataformwidget.h>

#include <qflogcust.h>
/*
class Factory : public QFTableViewWidget::TableViewFactory
{
	public:
		virtual ~Factory() {}
		virtual QFDbXmlTableViewWidget_DbXmlView* createView() const
		{
			return new QFDbXmlTableViewWidget_DbXmlView();
		}
};
*/
//==============================================================
//                                        QFDbXmlTableViewWidget
//==============================================================
QFDbXmlTableViewWidget::QFDbXmlTableViewWidget(QWidget *parent)
	: QFSqlTableViewWidget(parent)
{
	qfTrash() << QF_FUNC_NAME;
	//currentTableRow = -1;
	
	QBoxLayout *ly = qobject_cast<QBoxLayout*>(layout());
	QF_ASSERT(ly, "bad layout");
	dataFormWidget = new QFDataFormWidget();
	ly->addWidget(dataFormWidget);
	
	ly = new QVBoxLayout(dataFormWidget);
	ly->setMargin(0);
	f_dbxmlWidget = new QFSqlXmlConfigWidget();
	ly->addWidget(f_dbxmlWidget);
	//dbxmlWidget->setSqlId("dbxml");
	//dbxmlWidget()->hide();
	dataFormWidget->connectAsRowBrowser(tableView());
}

QFDbXmlTableViewWidget::~QFDbXmlTableViewWidget()
{
}
				
void QFDbXmlTableViewWidget::setModel(QFTableModel *m)
{
	QFSqlTableViewWidget::setModel(m);
	QFDataFormDocument *doc = new QFDataFormDocument(dataFormWidget);
	doc->setModel(tableView()->model());
	dataFormWidget->setDocument(doc);
}

QString QFDbXmlTableViewWidget::rootPathFieldName() const
{
	return dbxmlWidget()->rootPathSqlId();
}

void QFDbXmlTableViewWidget::setRootPathFieldName(const QString &fn)
{
	dbxmlWidget()->setRootPathSqlId(fn);
}

