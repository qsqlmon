
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDBXMLTABLEVIEWWIDGET_H
#define QFDBXMLTABLEVIEWWIDGET_H

#include <qfguiglobal.h>

#include <qfsqltableviewwidget.h>

ERROR

class QFSqlXmlConfigWidget;
class QFDataFormWidget;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFDbXmlTableViewWidget : public QFSqlTableViewWidget
{
	Q_OBJECT;
	protected:
		QFSqlXmlConfigWidget *f_dbxmlWidget;
		QFDataFormWidget *dataFormWidget;
	public:
		QFDbXmlTableViewWidget(QWidget *parent = NULL);
		virtual ~QFDbXmlTableViewWidget();

		void setRootPathFieldName(const QString &fn);
		QString rootPathFieldName() const;
		
		virtual void setModel(QFTableModel *m);
		//virtual QFDbXmlTableViewWidget_DbXmlView* tableView() const;
		QFSqlXmlConfigWidget *dbxmlWidget() const {return f_dbxmlWidget;}
};


   
#endif // QFDBXMLTABLEVIEWWIDGET_H

