
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#include <QtGui>

#include <qftableview.h>
#include <qftablemodel.h>
#include <qfaction.h>
#include <qfsqltableview.h>

#include "qftableviewwidget.h"
#include "ui_qftableviewwidget.h"

#include <qflogcust.h>

QFTableView * QFTableViewWidget::TableViewFactory::createView() const
{
	return new QFSqlTableView();
}
/*
QFTableViewWidget::QFTableViewWidget(const TableViewFactory &factory, QWidget *parent)
	: QFDialogWidget(parent), toolBar(NULL), captionFrame(NULL)
{
	ui = new Ui::QFTableViewWidget;
	ui->setupUi(centralWidget());
	Qf::connectSlotsByName(centralWidget(), this);

	f_tableView = factory.createView();
	QBoxLayout *ly = qobject_cast<QBoxLayout*>(layout());
	QF_ASSERT(ly, "bad layout");
	ly->insertWidget(0, f_tableView);
	//createToolBar();
	setRowEditorMode(QFTableView::RowEditorInline);
}
*/
QFTableViewWidget::QFTableViewWidget(QWidget * parent, const TableViewFactory & factory)
	: QFDialogWidget(parent), toolBar(NULL), captionFrame(NULL)
{
	qfLogFuncFrame();
	ui = new Ui::QFTableViewWidget;
	ui->setupUi(centralWidget());
	Qf::connectSlotsByName(centralWidget(), this);

	f_tableView = factory.createView();
	QBoxLayout *ly = qobject_cast<QBoxLayout*>(layout());
	QF_ASSERT(ly, "bad layout");
	ly->insertWidget(0, f_tableView);
	//createToolBar();
	setRowEditorMode(QFTableView::RowEditorInline);
}

QFTableViewWidget::~QFTableViewWidget()
{
	qfTrash() << QF_FUNC_NAME << objectName();
	//savePersistentData();
	delete ui;
}

void QFTableViewWidget::createToolBar()
{
	if(!toolBar) {
		toolBar = new QFToolBar(NULL);
		toolBar->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
		QBoxLayout *l = qobject_cast<QBoxLayout*>(layout());
		l->setMargin(0);
		if(l) l->insertWidget(0, toolBar);
		foreach(QFAction *a, tableView()->toolBarActions()) toolBar->addAction(a);
	}
}

QFPart::ToolBarList QFTableViewWidget::createToolBars()
{
	QFPart::ToolBarList ret;
	if(isToolBarVisible()) ret << toolBar;
	return ret;
}
/*
QWidget* QFTableViewWidget::createCaption()
{
	qfTrash() << QF_FUNC_NAME << "captionFrame:" << captionFrame;
	if(!captionFrame) {
		captionFrame = new QFrame(NULL);
		//captionFrame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
		QPalette p = captionFrame->palette();
		p.setColor(QPalette::Window, QColor(245, 245, 184));
		captionFrame->setPalette(p);
		captionFrame->setAutoFillBackground(true);
		captionFrame->setFrameShape(QFrame::StyledPanel);
		captionFrame->setFrameShadow(QFrame::Raised);
		QBoxLayout *ly = new QHBoxLayout(captionFrame);
		ly->setMargin(4);
		ly->setSpacing(6);
		QLabel *lbl = new QLabel();
		lbl->setObjectName("lblCaptionText");
		ly->addWidget(lbl);
		ly->addStretch();
		qfTrash() << "\tcreated:" << captionFrame;
	}
	return captionFrame;
}
*/
void QFTableViewWidget::setModel(QFTableModel *m)
{
	tableView()->setModel(m);
	connect(m, SIGNAL(allDataChanged()), this, SLOT(updateStatus()));
}

QFTableModel* QFTableViewWidget::model(bool throw_exc) const
{
	return tableView()->model(throw_exc);
}

void QFTableViewWidget::updateStatus()
{
	QFTableModel *m = tableView()->model(!Qf::ThrowExc);
	if(m) {
		ui->lblRowCnt->setText(QString("%1 rows").arg(m->rowCount()));
	}
	tableView()->refreshActions();
}

void QFTableViewWidget::setInfo(const QString &info)
{
	ui->edInfo->setText(info);
}

/*
void QFTableView::toggleMode()
{
	QFSqlQuery &q = queryRef();
	if(mode() == QFSqlQuery::ModeReadOnly) {
		q.setAt(currentIndex().row());
		setMode(QFSqlQuery::ModeReadWrite);
}
	else {
		postRow();
		setMode(QFSqlQuery::ModeReadOnly);
}
	updateRow(q.at());
}
*/

QFTableView* QFTableViewWidget::tableView() const
{
	return f_tableView;
}
/*
QFDataFormDialog* QFTableViewWidget::externalEditor() const
{
	qfTrash() << QF_FUNC_NAME;
	QFDataFormDialog *ret = tableView()->externalEditor();
	qfTrash() << "\treturn:" << ret;
	return ret;
}
*/
void QFTableViewWidget::setRowEditorMode(QFTableView::RowEditorMode _mode)
{
	qfTrash() << QF_FUNC_NAME;
	tableView()->setRowEditorMode(_mode);
}

QFTableView::RowEditorMode QFTableViewWidget::rowEditorMode() const
{
	return tableView()->rowEditorMode();
}

bool QFTableViewWidget::isStatusLineVisible() const
{
	return ui->frameStatus->isVisible();
}

void QFTableViewWidget::setStatusLineVisible(bool b)
{
	return ui->frameStatus->setVisible(b);
}

bool QFTableViewWidget::isToolBarVisible() const
{
	if(toolBar) return toolBar->isVisible();
	return false;
}

void QFTableViewWidget::setToolBarVisible(bool b)
{
	if(!toolBar && b == true) {
		createToolBar();
		QBoxLayout *ly = qobject_cast<QBoxLayout*>(layout());
		QF_ASSERT(ly, "bad layout");
		ly->insertWidget(0, toolBar);
	}
	if(toolBar) toolBar->setVisible(b);
}

void QFTableViewWidget::on_btMenu_clicked()
{
	emit statusTextAction(ui->edInfo->text());
}

void QFTableViewWidget::setXmlConfigPersistentId(const QString & id, bool load_persistent_data)
{
	qfTrash() << QF_FUNC_NAME << id << load_persistent_data << objectName();
	if(id.isEmpty()) {
		tableView()->setXmlConfigPersistentId(QString());
	}
	else {
		tableView()->setXmlConfigPersistentId(id + "/tableView", load_persistent_data);
	}
}

void QFTableViewWidget::setExternalRowEditorFactory(QFDataFormDialogWidgetFactory * fact)
{
	tableView()->setExternalRowEditorFactory(fact);
}


