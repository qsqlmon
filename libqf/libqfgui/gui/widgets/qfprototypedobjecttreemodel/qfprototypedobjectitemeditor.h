
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFPROTOTYPEDOBJECTITEMEDITOR_H
#define QFPROTOTYPEDOBJECTITEMEDITOR_H

#include<qfguiglobal.h>

#include <QVariant>
#include <QWidget>
#include <QPointer>

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFPrototypedObjectItemEditor : public QWidget
{
	Q_OBJECT;
	Q_PROPERTY(QVariant value READ value WRITE setValue USER true);
	protected:
		//QString f_type;
		bool f_valueInvalid;
		QPointer<QWidget> f_editor;
	protected slots:
		void btClearValue_clicked();
	signals:
		void clearOwnValue(QWidget *editor);
	public:
		virtual QVariant value() const;
		virtual void setValue(const QVariant &val) const;
	public:
		void setEditorWidget(QWidget *ed);
		void setValueInvalid(bool b) {f_valueInvalid = b;}
		bool isValueInvalid() const {return f_valueInvalid;}
	public:
		QFPrototypedObjectItemEditor(QWidget *parent = NULL);
		virtual ~QFPrototypedObjectItemEditor();
};

#endif // QFPROTOTYPEDOBJECTITEMEDITOR_H

