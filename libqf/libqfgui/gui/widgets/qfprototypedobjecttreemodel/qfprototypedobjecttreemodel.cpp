
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfprototypedobjecttreemodel.h"

#include <QApplication>

#include <qflogcust.h>

//=================================================
//             QFPrototypedObjectTreeModel
//=================================================
QFPrototypedObjectTreeModel::QFPrototypedObjectTreeModel(QObject *parent) 
	: QStandardItemModel(parent)
{
}

QFPrototypedObjectTreeModel::~QFPrototypedObjectTreeModel()
{
}

void QFPrototypedObjectTreeModel::setRoot(const QFPrototypedObject &o)
{
	f_rootObject = o;
	reload();
	reset();
}

void QFPrototypedObjectTreeModel::reload_helper(const QStringList &path, QStandardItem *p_it)
{
	static QStringList meta_keys;
	if(meta_keys.isEmpty()) meta_keys
		<< QFPrototypedObject::HELP_KEY
		<< QFPrototypedObject::OPTIONS_KEY
		<< QFPrototypedObject::UNIT_KEY
		<< QFPrototypedObject::TYPE_KEY
		<< QFPrototypedObject::DELEGATE_KEY
		<< QFPrototypedObject::CAPTION_KEY;
	foreach(QString key, f_rootObject.keysOnPath(path)) {
		Item *it = new Item();
		QStringList path2 = path;
		path2 << key;
		it->path = path2;
		foreach(QString k, meta_keys) {
			it->meta[k] = f_rootObject.metaValueOnPath(path2, k);
		}
		QString caption = it->meta.value(QFPrototypedObject::CAPTION_KEY, key).toString();
		if(caption.isEmpty()) caption = key;
		it->setText(caption);
		p_it->appendRow(it);
		reload_helper(path2, it);
	}
}

void QFPrototypedObjectTreeModel::reload()
{
	clear();
	reload_helper(QStringList(), invisibleRootItem());
}

QFPrototypedObjectTreeModel::Item* QFPrototypedObjectTreeModel::itemFromIndex(const QModelIndex &ix) const
{
	Item *it = dynamic_cast<Item*>(QStandardItemModel::itemFromIndex(ix.sibling(ix.row(), 0)));
	return it;
}

QVariant QFPrototypedObjectTreeModel::itemValue(const QModelIndex &ix, int *p_status) const
{
	QVariant ret;
	int status = QFPrototypedObject::ValueNotFound;
	Item *it = itemFromIndex(ix);
	if(it) {
		ret = f_rootObject.valueOnPath(it->path, QVariant(), &status);
	}
	if(p_status) *p_status = status;
	return ret;
}

int QFPrototypedObjectTreeModel::columnCount(const QModelIndex &) const
{
	return 2;
}
 
QModelIndex QFPrototypedObjectTreeModel::index(int row, int column, const QModelIndex & parent) const
{
	/// tahle funkce vyrabi generovane sloupce modelu
	QModelIndex ret = QStandardItemModel::index(row, 0, parent);
	if(column > 0) ret = createIndex(row, column, ret.internalPointer());
	return ret;
}

QVariant QFPrototypedObjectTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant ret;
	if(orientation == Qt::Horizontal) {
		if(role == Qt::DisplayRole) {
			if(section == 0) ret = trUtf8("vlastnost");
			else if(section == 1) ret = trUtf8("hodnota");
		}
	}
	else ret = QStandardItemModel::headerData(section, orientation, role);
	return ret;
}

Qt::ItemFlags QFPrototypedObjectTreeModel::flags(const QModelIndex &ix) const
{
	Qt::ItemFlags ret = QStandardItemModel::flags(ix);
	ret &= ~Qt::ItemIsEditable;
	int col = ix.column();
	if(col == 1) {
		Item *it = itemFromIndex(ix);
		if(it) {
			if(it->valueType().count()) ret |= Qt::ItemIsEditable;// | Qt::ItemIsSelectable);
		}
	}
	else {
	}
	return ret;
}

QVariant QFPrototypedObjectTreeModel::data(const QModelIndex &ix, int role) const
{
	QVariant ret = QStandardItemModel::data(ix, role);
	int col = ix.column();
	if(col == 1) {
		Item *it = itemFromIndex(ix);
		if(it) {
			if(role == Qt::DisplayRole || role == Qt::EditRole) {
				//int status;
				ret = itemValue(ix);
				/*
				if(!ret.isValid()) {
					QByteArray type_name = it->meta.value("type").toString().toAscii();
					//qfInfo() << type_name << "->" << QVariant::nameToType(type_name.constData()) << QVariant::typeToName(QVariant::String) << QVariant::typeToName(QVariant::Int);
					ret = QVariant(QVariant::nameToType(type_name.constData()));
					//qfInfo() << type_name << "->" << ret.typeName();
				}
				*/
			}
			else if(role == Qt::ForegroundRole) {
				int status;
				ret = itemValue(ix, &status);
				if(status & QFPrototypedObject::ValuePrototype) {
					ret = QColor(Qt::gray);
				}
			}
			#if 0
			else if(role == Qt::FontRole) {
				int status;
				ret = itemValue(ix, &status);
				if(status & QFPrototypedObject::ValuePrototype) {
				}
				else {
					QFont f = QApplication::font("QStyledItemDelegate");
					f.setBold(true);
					ret = f;
				}
			}
			#endif
		}
	}
	else if(col == 0) {
	}
	if(role == Qt::BackgroundRole) {
		Item *it = itemFromIndex(ix);
		if(it && it->path.count() == 1) {
			if(it->valueType().isEmpty()) ret = QColor("khaki");
		}
	}
	return ret;
}

bool QFPrototypedObjectTreeModel::setData(const QModelIndex &ix, const QVariant &val, int role)
{
	qfLogFuncFrame();
	bool ret = false;
	int col = ix.column();
	if(col == 1) {
		if(role == Qt::EditRole) {
			Item *it = itemFromIndex(ix);
			if(it) {
				qfTrash() << "\t setting data on path:" << it->path.join("/") << "to:" << val.toString();
				f_rootObject.setValueOnPath(it->path, val);
				//qfInfo() << f_rootObject.dump();
				ret = true;
				emit propertyChanged(f_designPath, it->path, val);
			}
		}
	}
	return ret;
}

