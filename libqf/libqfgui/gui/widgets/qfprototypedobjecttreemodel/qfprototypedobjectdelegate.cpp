
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfprototypedobjectdelegate.h"
#include "qfprototypedobjectitemeditorfactory.h"
#include "qfprototypedobjectitemeditor.h"
#include "qfprototypedobjecttreemodel.h"

#include <QPainter>
#include <QApplication>

#include <qflogcust.h>

//=================================================
//             QFPrototypedObjectDelegate
//=================================================

QFPrototypedObjectDelegate::QFPrototypedObjectDelegate(QObject *parent) 
	: QStyledItemDelegate(parent)
{
	//setItemEditorFactory(&fact);
}

QFPrototypedObjectDelegate::~QFPrototypedObjectDelegate()
{
}

void QFPrototypedObjectDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &ix) const
{
	bool custom_draw = false;
	if(ix.column() == 1) {
		QVariant v = ix.data();
		if(v.type() == QVariant::Bool) {
			QStyleOptionButton opts;
			opts.rect = option.rect;
			opts.state = (v.toBool())? QStyle::State_On: QStyle::State_Off;
			QApplication::style()->drawControl(QStyle::CE_CheckBox, &opts, painter);
			custom_draw = true;
		}
	}
	if(!custom_draw) QStyledItemDelegate::paint(painter, option, ix);
	QRect r = option.rect; 
	painter->save();
	painter->setPen(QPen(QColor("#9d9d9d")));
	painter->drawRect(r);
	painter->restore();
}

QWidget* QFPrototypedObjectDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	Q_UNUSED(option);
	static QFPrototypedObjectItemEditorFactory fact;
	QWidget *ret = NULL;
	const QFPrototypedObjectTreeModel *m = qobject_cast<const QFPrototypedObjectTreeModel *>(index.model());
	if(m) {
		QFPrototypedObjectTreeModel::Item *it = m->itemFromIndex(index);
		if(it) {
			ret = fact.createEditor(it->meta, parent);
			connect(ret, SIGNAL(clearOwnValue(QWidget *)), this, SLOT(clearOwnValue(QWidget *)));
		}
	}
	return ret;
}

void QFPrototypedObjectDelegate::clearOwnValue(QWidget *editor)
{
	qfLogFuncFrame();
	QFPrototypedObjectItemEditor *ed = qobject_cast<QFPrototypedObjectItemEditor*>(editor);
	if(ed) {
		qfTrash() << "\t set value to QVariant()";
		ed->setValueInvalid(true);
		qfTrash() << "\t commitData()";
		emit commitData(editor);
		qfTrash() << "\t closeEditor()";
		emit closeEditor(editor);
		qfTrash() << "\t closed";
	}
}
