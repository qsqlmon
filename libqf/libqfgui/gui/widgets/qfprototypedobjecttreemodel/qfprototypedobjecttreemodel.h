 
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFPROTOTYPEDOBJECTTREEMODEL_H
#define QFPROTOTYPEDOBJECTTREEMODEL_H

#include<qfguiglobal.h>  
#include<qfprototypedobject.h>

#include <QStandardItemModel>

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFPrototypedObjectTreeModel : public QStandardItemModel
{
	Q_OBJECT;
	public:
		class QFGUI_DECL_EXPORT Item : public QStandardItem
		{
			public:
				QStringList path;
				//QVariant value;
				QVariantMap meta;
			public:
				QString valueType() const {return meta.value("type").toString();}
				//QStringList path() const {return f_path;}
				//void setPath(const QStringList &p) {f_path = p;}
			public:
				virtual ~Item() {}
		};
	protected:
		QFPrototypedObject f_rootObject;
		QStringList f_designPath;
	protected:
		void reload_helper(const QStringList &path, QStandardItem *it);
	public slots:
		void reload();
	signals:
		void propertyChanged(const QStringList &design_path, const QStringList &object_path, const QVariant &value);
	public:
		void setRoot(const QFPrototypedObject &o);
		QVariant itemValue(const QModelIndex &ix, int *status = NULL) const;
		Item* itemFromIndex(const QModelIndex &ix) const;
	public:
		virtual int columnCount(const QModelIndex &parent_ix = QModelIndex()) const;
		virtual QModelIndex index( int row, int column, const QModelIndex & parent = QModelIndex() ) const;
		virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
		virtual QVariant data(const QModelIndex &ix, int role = Qt::DisplayRole) const;
		virtual bool setData(const QModelIndex &index, const QVariant &val, int role = Qt::EditRole);
		virtual Qt::ItemFlags flags(const QModelIndex &ix) const;
	public:
		QFPrototypedObjectTreeModel(QObject *parent = NULL);
		virtual ~QFPrototypedObjectTreeModel();
};

#endif // QFPROTOTYPEDOBJECTTREEMODEL_H

