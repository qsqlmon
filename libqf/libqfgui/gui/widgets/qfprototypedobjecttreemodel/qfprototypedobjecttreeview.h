
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFPROTOTYPEDOBJECTTREEVIEW_H
#define QFPROTOTYPEDOBJECTTREEVIEW_H

#include<qfguiglobal.h>

#include <qftreeview.h>

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFPrototypedObjectTreeView : public QFTreeView
{
	Q_OBJECT;
	protected:
		virtual void keyPressEvent(QKeyEvent *event);
	public:
		virtual void reset();
	public:
		QFPrototypedObjectTreeView(QWidget *parent = NULL);
		virtual ~QFPrototypedObjectTreeView();
};

#endif // QFPROTOTYPEDOBJECTTREEVIEW_H

