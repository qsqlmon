
HEADERS +=        \
	$$PWD/qfprototypedobjecttreemodel.h      \
	$$PWD/qfprototypedobjectdelegate.h     \
	$$PWD/qfprototypedobjecttreeview.h    \
	$$PWD/qfprototypedobjectitemeditor.h   \
	$$PWD/qfprototypedobjectitemeditorfactory.h  \

SOURCES +=       \
	$$PWD/qfprototypedobjecttreemodel.cpp      \
	$$PWD/qfprototypedobjectdelegate.cpp     \
	$$PWD/qfprototypedobjecttreeview.cpp    \
	$$PWD/qfprototypedobjectitemeditor.cpp   \
	$$PWD/qfprototypedobjectitemeditorfactory.cpp  \


