
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfprototypedobjecttreeview.h"

#include <qfprototypedobjectdelegate.h>

#include <QKeyEvent>
#include <QHeaderView>

#include <qflogcust.h>

//=================================================
//             QFPrototypedObjectTreeView
//=================================================
QFPrototypedObjectTreeView::QFPrototypedObjectTreeView(QWidget *parent)
	: QFTreeView(parent)
{
	setItemDelegate(new QFPrototypedObjectDelegate(this));
	//setStyleSheet("QTreeView { alternate-background-color: yellow; }");
	header()->setStretchLastSection(false);
}

QFPrototypedObjectTreeView::~QFPrototypedObjectTreeView()
{
}

void QFPrototypedObjectTreeView::keyPressEvent(QKeyEvent *e)
{
	qfLogFuncFrame();
	QFTreeView::keyPressEvent(e);
	if(e->key() == Qt::Key_Return || (e->key() == Qt::Key_Enter && e->modifiers() == Qt::KeypadModifier)) {
		e->accept();
	}
	qfTrash() << "\t accepted:" << e->isAccepted();
}

void QFPrototypedObjectTreeView::reset()
{
	qfLogFuncFrame();
	QFTreeView::reset();
	setExpandedRecursively(QModelIndex());
	header()->resizeSections(QHeaderView::ResizeToContents);
}
