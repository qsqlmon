
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfprototypedobjectitemeditorfactory.h"
#include "qfprototypedobjectitemeditor.h"

#include <qfprototypedobject.h>

#include <QCheckBox>
#include <QComboBox>

#include <qflogcust.h>

//=================================================
//             QFPrototypedObjectItemEditorFactory
//=================================================
QFPrototypedObjectItemEditorFactory::QFPrototypedObjectItemEditorFactory() 
	: QItemEditorFactory()
{
}

QFPrototypedObjectItemEditorFactory::~QFPrototypedObjectItemEditorFactory()
{
}

QWidget* QFPrototypedObjectItemEditorFactory::createEditor(const QVariantMap &opts, QWidget *parent) const
{
	qfLogFuncFrame();
	QWidget *w = NULL;
	QString delegate_type = opts.value(QFPrototypedObject::DELEGATE_KEY).toString();
	if(delegate_type.isEmpty()) delegate_type = opts.value(QFPrototypedObject::TYPE_KEY).toString();
	qfTrash() << "\t delegate_type:" << delegate_type;
	if(delegate_type == "list") {
		QVariantMap object_opts = opts.value(QFPrototypedObject::OPTIONS_KEY).toMap();
		QVariantList items = object_opts.value("items").toList();
		QComboBox *cbx = new QComboBox(parent);
		foreach(QVariant item, items) {
			QString caption;
			QVariant value;
			QVariantList lst = item.toList();
			if(lst.isEmpty()) {
				caption = item.toString();
				value = item.toString();
			}
			else {
				value = lst.value(0);
				caption = lst.value(1).toString();
			}
			cbx->addItem(caption, value);
		}
		cbx->setEditable(object_opts.value("editable").toBool());
		w = cbx;
	}
	else {
		QByteArray ba = delegate_type.toAscii();
		QVariant::Type type = QVariant::nameToType(ba.constData());
		if(type == QVariant::Bool) {
			w = new QCheckBox(parent);
			w->setAutoFillBackground(true);
		}
		else if(type == QVariant::Invalid) {
			/// needituj policka, ktera neznas
		}
		else {
			w = QItemEditorFactory::createEditor(type, NULL);
		}
	}
	QFPrototypedObjectItemEditor *ed = NULL;
	if(w) {
		ed = new QFPrototypedObjectItemEditor(parent);
		ed->setEditorWidget(w);
	}
	return ed;
}
