
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfprototypedobjectitemeditor.h"

#include <qfpixmapcache.h>

#include <QHBoxLayout>
#include <QToolButton>
#include <QMetaProperty>
#include <QComboBox>
#include <QLineEdit>

#include <qflogcust.h>

static QVariant cbx_value_helper(QComboBox *cbx)
{
	QVariant ret;
	if(cbx->isEditable()) {
		ret = cbx->currentText();
	}
	else {
		ret = cbx->itemData(cbx->currentIndex()); 
		if(!ret.isValid()) ret = cbx->currentIndex(); /// kdyz nejsou data, uklada se index
	}
	return ret;
}

static void cbx_setValue_helper(QComboBox *cbx, const QVariant &val)
{
	int ix = -1;
	for(int i=0; i<cbx->count(); i++) {
		QVariant v = cbx->itemData(i);
		if(v == val) {
			cbx->setCurrentIndex(i);
			ix = i;
			break;
		}
	}
	if(ix < 0 && cbx->isEditable()) {// && !isValueRestrictedToItems()) {
		QLineEdit *ed = cbx->lineEdit();
		if(ed) ed->setText(val.toString());
	}
	else {
		cbx->setCurrentIndex(ix);
	}
}

//=================================================
//             QFPrototypedObjectItemEditor
//=================================================
QFPrototypedObjectItemEditor::QFPrototypedObjectItemEditor(QWidget *parent)
	: QWidget(parent)
{
	f_editor = NULL;
	setValueInvalid(false);
	QHBoxLayout *ly = new QHBoxLayout();
	ly->setSpacing(0);
	ly->setMargin(0);
	{
		QToolButton *bt = new QToolButton(this);
		bt->setAutoRaise(true);
		QIcon ico = QFPixmapCache::icon("default_value");
		bt->setIcon(ico);
		bt->setAutoFillBackground(true);
		ly->addWidget(bt);
		connect(bt, SIGNAL(clicked()), this, SLOT(btClearValue_clicked()));
	}
	setLayout(ly);
}

QFPrototypedObjectItemEditor::~QFPrototypedObjectItemEditor()
{
}

void QFPrototypedObjectItemEditor::setEditorWidget(QWidget *ed)
{
	QBoxLayout *ly = qobject_cast<QBoxLayout*>(layout());
	if(ly) {
		ly->insertWidget(0, ed);
		f_editor = ed;
		setFocusProxy(f_editor);
	}
}

void QFPrototypedObjectItemEditor::btClearValue_clicked()
{
	emit clearOwnValue(this);
}

QVariant QFPrototypedObjectItemEditor::value() const
{
	qfLogFuncFrame();
	QVariant ret;
	if(!isValueInvalid()) {
		if(f_editor) {
			QByteArray n = f_editor->metaObject()->userProperty().name();
			// ### Qt 5: give QComboBox a USER property qstyleditemdelegate.cpp:500
			if(n.isEmpty() && f_editor->inherits("QComboBox")) {
				QComboBox *cbx = qobject_cast<QComboBox*>(f_editor);
				ret = cbx_value_helper(cbx);
			}
			else {
				ret = f_editor->property(n);
			}
			qfTrash() << "\t return property:" << n << "val:" << ret.toString() << "type:" << ret.typeName();
		}
	}
	return ret;
}

void QFPrototypedObjectItemEditor::setValue(const QVariant &val) const
{
	qfLogFuncFrame() << val.toString();
	if(f_editor) {
		QByteArray n = f_editor->metaObject()->userProperty().name();
		// ### Qt 5: give QComboBox a USER property qstyleditemdelegate.cpp:500
		qfTrash() << "\t setting property:" << n << "to:" << val.toString() << "type:" << val.typeName();
		if(n.isEmpty() && f_editor->inherits("QComboBox")) {
			QComboBox *cbx = qobject_cast<QComboBox*>(f_editor);
			cbx_setValue_helper(cbx, val);
		}
		else {
			f_editor->setProperty(n, val);
		}
	}
}
