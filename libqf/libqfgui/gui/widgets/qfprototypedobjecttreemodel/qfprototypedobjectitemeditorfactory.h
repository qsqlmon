
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFPROTOTYPEDOBJECTITEMEDITORFACTORY_H
#define QFPROTOTYPEDOBJECTITEMEDITORFACTORY_H

#include<qfguiglobal.h>

#include <QItemEditorFactory>

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFPrototypedObjectItemEditorFactory : public QItemEditorFactory
{
	public:
		virtual QWidget* createEditor(const QVariantMap &opts, QWidget *parent) const;
	public:
		QFPrototypedObjectItemEditorFactory();
		virtual ~QFPrototypedObjectItemEditorFactory();
};

#endif // QFPROTOTYPEDOBJECTITEMEDITORFACTORY_H

