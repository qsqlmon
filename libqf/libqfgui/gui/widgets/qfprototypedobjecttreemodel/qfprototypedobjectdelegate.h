
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFPROTOTYPEDOBJECTDELEGATE_H
#define QFPROTOTYPEDOBJECTDELEGATE_H

#include <qfguiglobal.h>

#include <QStyledItemDelegate>

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFPrototypedObjectDelegate : public QStyledItemDelegate
{
	Q_OBJECT;
	public slots:
		void clearOwnValue(QWidget *editor);
	public:
		virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
		virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
	public:
		QFPrototypedObjectDelegate(QObject *parent = NULL);
		virtual ~QFPrototypedObjectDelegate();
};

#endif // QFPROTOTYPEDOBJECTDELEGATE_H

