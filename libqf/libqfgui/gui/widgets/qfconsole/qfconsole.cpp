
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfconsole.h"

#include <QTextBlock>
#include <QKeyEvent>

#include <qflogcust.h>

QFConsole::QFConsole(QWidget *parent)
	: QTextEdit(parent), f_historyIndex(0)
{
	f_prompt = "> ";
	promptFormat.setForeground(QBrush(QColor(Qt::blue)));
	inputFormat.setForeground(QBrush(QColor(Qt::black)));
	outputFormat.setForeground(QBrush(QColor(Qt::darkGreen)));
	errorFormat.setForeground(QBrush(QColor(Qt::red)));
	setInput();
}

QFConsole::~QFConsole()
{
}

void QFConsole::processInput()
{
	QString s = currentInput();
	if(!s.isEmpty()) {
		if(f_history.isEmpty()) f_history << s;
		else if(f_history.last() != s) f_history << s;
	}
	f_historyIndex = f_history.count();

	writeOutput(s, EvalWriteInput);
	
	emit evaluateLine(s);

	setInput();
}

void QFConsole::setInput(const QString & str)
{
	moveCursor(QTextCursor::End);
	QTextCursor c = textCursor();
	c.movePosition(QTextCursor::StartOfBlock, QTextCursor::KeepAnchor);
	//qfInfo() << "inserting '" << prompt + str << "'";
	//c.insertHtml("<b>" + prompt + "</b><i>" + str + "</i>");
	c.insertText(f_prompt, promptFormat);
	c.movePosition(QTextCursor::EndOfLine);
	c.setCharFormat(inputFormat);
	c.insertText(str);
	//moveCursor(QTextCursor::End);
	//setCurrentCharFormat(inputFormat);
}

QString QFConsole::currentInput()
{
	qfLogFuncFrame();
	QTextBlock bl = document()->end().previous();
	QString s = bl.text();
	qfTrash() << "block:" << s;
	qfTrash() << "prompt:" << f_prompt;
	/// oddelej prompt
	s = s.mid(f_prompt.length()).trimmed();
	return s;
}

void QFConsole::keyPressEvent(QKeyEvent * ev)
{
	bool eaten = false;
	if(currentCharFormat() != inputFormat) setCurrentCharFormat(inputFormat);
	if(!ev->modifiers() || ev->modifiers() == Qt::KeypadModifier) {
		int key = ev->key();
		if(key == Qt::Key_Up) {
			inputFromHistory(-1);
			eaten = true;		
		}
		else if(key == Qt::Key_Down) {
			inputFromHistory(1);
			eaten = true;
		}
		else if(key == Qt::Key_Left) {
			QTextCursor c = textCursor();
			c.movePosition(QTextCursor::StartOfLine, QTextCursor::KeepAnchor);
			if(c.selectedText() == f_prompt) eaten = true;
		}
		else if(key == Qt::Key_Enter || key == Qt::Key_Return) {
			processInput();
			eaten = true;
		}
	}
	if(!eaten) QTextEdit::keyPressEvent(ev);
}

void QFConsole::inputFromHistory(int history_index_increment)
{
	int ix = f_historyIndex + history_index_increment;
	if(ix >= f_history.count()) return;
	if(ix < 0) return;
	if(ix != f_historyIndex) {
		f_historyIndex = ix;
		setInput(f_history[f_historyIndex]);
	}
}

void QFConsole::writeOutput(const QString & str, int status)
{
	QTextCursor c = textCursor();
	c.movePosition(QTextCursor::StartOfBlock);
	c.insertBlock();
	c.movePosition(QTextCursor::Left);
	QTextCharFormat fmt = inputFormat;
	if(status == EvalError) fmt = errorFormat;
	else if(status == EvalOk) fmt = outputFormat;
	else if(status == EvalWriteInput) {
		c.insertText(f_prompt, promptFormat);
		fmt = inputFormat;
	}
	c.insertText(str, fmt);
}

void QFConsole::loadPersistentData()
{
	if(!xmlConfigPersistentId().isEmpty()) {
		/// load window size
		QVariant v = persistentValue("history");
		f_history = v.toStringList();
		f_historyIndex = f_history.count();
	}
}

void QFConsole::savePersistentData()
{
	if(!xmlConfigPersistentId().isEmpty()) {
		/// save window size
		setPersistentValue("history", QStringList(f_history.mid(0, 20)));
	}
}
