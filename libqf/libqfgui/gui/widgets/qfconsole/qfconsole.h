
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFCONSOLE_H
#define QFCONSOLE_H

#include <qfguiglobal.h>
#include <qfxmlconfigpersistenter.h>

#include <QTextEdit>

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFConsole : public QTextEdit, public QFXmlConfigPersistenter
{
	Q_OBJECT;
	public:
		enum EvaluateStatus {EvalOk = 0, EvalError, EvalWriteInput};
	protected:
		QStringList f_history;
		int f_historyIndex;
		QString f_prompt;

		QTextCharFormat promptFormat;
		QTextCharFormat inputFormat;
		QTextCharFormat outputFormat;
		QTextCharFormat errorFormat;
	protected:
		void inputFromHistory(int history_index_increment);
		QString currentInput();

		virtual void keyPressEvent(QKeyEvent *ev);

		//virtual QString evaluateLine(const QString &line, QFConsoleEvaluator::EvaluationStatus &status);
	protected slots:
		void processInput();
	signals:
		void evaluateLine(const QString &line);
	public slots:
		void setInput(const QString &str = QString());
		void writeOutput(const QString &str, int status = EvalOk);
	public:
		virtual void loadPersistentData();
		virtual void savePersistentData();
	public:
		QFConsole(QWidget *parent = NULL);
		virtual ~QFConsole();
};
   
#endif // QFCONSOLE_H

