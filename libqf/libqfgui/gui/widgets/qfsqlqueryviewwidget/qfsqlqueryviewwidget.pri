MY_WIDGET_NAME =  qfsqlqueryviewwidget

message(including $$MY_WIDGET_NAME)

QT += sql

INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/$${MY_WIDGET_NAME}.h 

SOURCES += \
    $$PWD/$${MY_WIDGET_NAME}.cpp

FORMS += \	
    $$PWD/$${MY_WIDGET_NAME}.ui
	
