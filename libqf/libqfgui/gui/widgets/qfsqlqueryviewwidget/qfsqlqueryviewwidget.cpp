#include <QtGui>

#include <qftableview.h>
#include <qftablemodel.h>
#include <qfaction.h>

#include "qfsqlqueryviewwidget.h"
#include "ui_qfsqlqueryviewwidget.h"

#include <qflogcust.h>

QFSqlQueryViewWidget::QFSqlQueryViewWidget(QWidget *parent)
    : QWidget(parent)
{
    ui = new Ui::QFSqlQueryViewWidget;
	ui->setupUi(this);
    //setWindowTitle(tr("QFSqlQueryViewWidget"));
    //resize(200, 200);
	ui->btToggleMode->setDefaultAction(ui->queryView->action("toggleMode"));
	ui->btReload->setDefaultAction(ui->queryView->action("reload"));
	ui->btInsert->setDefaultAction(ui->queryView->action("insertRow"));
	ui->btRemove->setDefaultAction(ui->queryView->action("removeSelectedRows"));
	ui->btPost->setDefaultAction(ui->queryView->action("postRow"));
}

QFSqlQueryViewWidget::~QFSqlQueryViewWidget()
{
	//qfTrash() << QF_FUNC_NAME;
	delete ui;
}

void QFSqlQueryViewWidget::setModel(QFTableModel *m)
{
	ui->queryView->setModel(m);
	connect(m, SIGNAL(allDataChanged()), this, SLOT(updateStatus()));
}

void QFSqlQueryViewWidget::updateStatus()
{
	QFTableModel *m = ui->queryView->model();
	ui->lblRowCnt->setText(QString("%1 rows").arg(m->rowCount()));
}

/*
void QFTableView::toggleMode()
{
	QFSqlQuery &q = queryRef();
	if(mode() == QFSqlQuery::ModeReadOnly) {
		q.setAt(currentIndex().row());
		setMode(QFSqlQuery::ModeReadWrite);
}
	else {
		postRow();
		setMode(QFSqlQuery::ModeReadOnly);
}
	updateRow(q.at());
}
*/
		
QFTableView* QFSqlQueryViewWidget::queryView()
{
	return ui->queryView;
}

