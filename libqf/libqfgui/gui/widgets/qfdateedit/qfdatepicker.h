#ifndef QFDATEPICKER_H
#define QFDATEPICKER_H

#include <QFrame>

class QPushButton;
class QCalendarWidget;
class QDate;

class QFDatePicker : public QFrame
{
	Q_OBJECT;
	friend class QFDateEdit;
	protected slots:
		void none_clicked();
		void calendarClicked(const QDate &d);
	signals:
		void datePicked(const QDate &d);
	protected:
		QCalendarWidget *f_calendar;
		QPushButton *btNone;
	public:
		QFDatePicker(QWidget *parent = NULL, Qt::WFlags f = 0);
		virtual ~QFDatePicker() {}

		QCalendarWidget* calendar() {return f_calendar;}
};

#if 0
/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

 // TODO: rewrite this object to not break GPL licence
 

#include <QFrame>
#include <QToolButton>

#include "datetable.h"
ERROR
class QComboBox;
class QLabel;

/**
 * @author David Cuadrado <krawek@gmail.com>
*/

class QFDatePicker : public QFrame
{
	Q_OBJECT;
	public:
		QFDatePicker(QWidget *parent = 0, Qt::WFlags f = 0);
		~QFDatePicker();
		void setDate(const QDate &date);
		
	private:
		void fillWeeks(const QDate &date);
		
	public slots:
		void setWeek(int week);
		void setYear(int year);
		
	protected slots:
		void previousYear();
		void nextYear();
		
		void previousMounth();
		void nextMounth();
		
	private slots:
		void mounthFromAction(QAction *act);
		
	signals:
		void dateChanged(const QDate &date);
		
	private:
		QComboBox *m_week;
		DateTable *m_dateTable;
		
		class EditableButton;
		
		QToolButton *m_mounth;
		//EditableButton *m_year;
		QSpinBox *m_year;
};

/// Trollove tvrdi, ze Nested Classes Cannot Have Signals or Slots, ale tady to funguje ?
class QFDatePicker::EditableButton : public QToolButton
{
	Q_OBJECT
	public:
		EditableButton();
		~EditableButton();
		
	public slots:
		void edit();
		
	private slots:
		void emitYearSelected();
		
	signals:
		void yearSelected(int year);
		
	private:
		QLineEdit *m_editor;
};
#endif

#endif //QFDATEPICKER_H
