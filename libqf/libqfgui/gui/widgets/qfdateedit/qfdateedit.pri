HEADERS +=    \
	$$PWD/qfdateedit.h  \
	$$PWD/qfdatepicker.h  \
	$$PWD/datetable.h  \

SOURCES +=    \
	$$PWD/qfdateedit.cpp  \
	$$PWD/qfdatepicker.cpp  \
	$$PWD/datetable.cpp  \

RESOURCES += $$PWD/dateedit.qrc

