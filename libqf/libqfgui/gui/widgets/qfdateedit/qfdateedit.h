
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDATEEDIT_H
#define QFDATEEDIT_H

#include <qfguiglobal.h>
#include <qfclassfield.h>

#include <QWidget>
#include <QDateEdit>
#include <QCalendarWidget>

class QFDatePicker;
class QDateEdit;
class QPushButton;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFDateEdit : public QWidget
{
	Q_OBJECT;
	Q_PROPERTY(bool calendarPopup READ calendarPopup WRITE setCalendarPopup);
	/// pokud kliknu na sipku, prida/ubere se vzdy jeden den, jinak defaultni chovani QDateEdit
	Q_PROPERTY(bool incrementByOneDay READ isIncrementByOneDay WRITE setIncrementByOneDay);
	QF_FIELD_RW(bool, is, set, IncrementByOneDay);
	Q_PROPERTY(bool buttonNoneVisible READ isButtonNoneVisible WRITE setButtonNoneVisible);
	QF_FIELD_RW(bool, is, set, ButtonNoneVisible);
	Q_PROPERTY(QDate minimumDate READ minimumDate WRITE setMinimumDate);
	Q_PROPERTY(QDate maximumDate READ maximumDate WRITE setMaximumDate);
	public:
		bool calendarPopup() const;
		void setCalendarPopup(bool b);
	public:
		class DateEdit;
	protected:
		DateEdit *f_dateEdit;
		QPushButton *btPickDate;
		QFDatePicker *fDatePicker;
		QFDatePicker* datePicker();
	private slots:
		void showDatePicker();
	public slots:
		void setDate(const QDate &date);
	signals:
		void dateChanged(const QDate &date);
	public:
		QDate date() const;
		bool isNull() const;
		bool setReadOnly(bool ro = true);
		bool isReadOnly() const;

		QDate minimumDate() const;
		void setMinimumDate(const QDate &d);
		QDate maximumDate() const;
		void setMaximumDate(const QDate &d);
	public:
		QFDateEdit(QWidget *parent = NULL);
		virtual ~QFDateEdit();
};

/// Nested Classes Cannot Have Signals or Slots, ikdyz QFDatePicker to pouziva a zda se ze to funguje.
class QFDateEdit::DateEdit : public QDateEdit
{
	Q_OBJECT;
			//QF_FIELD_R(bool, is, set, Null);
	protected:
		virtual void stepBy(int steps);
		virtual StepEnabled stepEnabled() const;
		virtual QString textFromDateTime(const QDateTime &dateTime) const;
		virtual void mousePressEvent(QMouseEvent *event);
	public:
		void setDate(const QDate &date);

		bool isNull() const {return date() == minimumDate();}
		void setNull() {QDateEdit::setDate(minimumDate());}
	public:
		DateEdit(QWidget *parent);
};

#endif // QFDATEEDIT_H

