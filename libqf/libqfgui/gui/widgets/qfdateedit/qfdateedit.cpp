
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdateedit.h"
#include "qfdatepicker.h"

#include <qf.h>
#include <qfpixmapcache.h>

#include <QPushButton>
#include <QHBoxLayout>
#include <QApplication>
#include <QDesktopWidget>

#include <qflogcust.h>

//========================================================
//                                              QFDateEdit
//========================================================
QFDateEdit::QFDateEdit(QWidget *parent)
	: QWidget(parent), fDatePicker(NULL)
{
	setIncrementByOneDay(false);
	setButtonNoneVisible(true);

	f_dateEdit = new DateEdit(this);
	//dateEdit->setMinimumDate(QDate(1970, 1, 1));
	f_dateEdit->setDisplayFormat(Qf::defaultDateFormat());
	btPickDate = new QPushButton(this);
	btPickDate->setIcon(QFPixmapCache::icon("dateEdit.calendar", "calendar.png"));
	btPickDate->setMinimumWidth(30);
	btPickDate->setMaximumWidth(30);
	//button->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
	connect(btPickDate, SIGNAL(clicked()), this, SLOT(showDatePicker()));
	QHBoxLayout *ly = new QHBoxLayout(this);
	ly->setMargin(0);
	ly->setSpacing(1);
	ly->insertWidget(0, btPickDate);
	ly->insertWidget(0, f_dateEdit);

	connect(f_dateEdit, SIGNAL(dateChanged(const QDate&)), this, SIGNAL(dateChanged(const QDate&)));
}

QFDateEdit::~QFDateEdit()
{
}

QFDatePicker* QFDateEdit::datePicker()
{
	if(fDatePicker == NULL) {
		fDatePicker = new QFDatePicker(this, Qt::Popup);
		//fDatePicker->setAttribute(Qt::WA_WindowPropagation);
		fDatePicker->setFrameStyle(QFrame::StyledPanel | QFrame::Plain);
		fDatePicker->setLineWidth(1);
		//qfInfo() << "btNoneVisible:" << isButtonNoneVisible();
		fDatePicker->btNone->setVisible(isButtonNoneVisible());
		connect(fDatePicker, SIGNAL(datePicked(const QDate&)), this, SLOT(setDate(const QDate&)));
	}
	return fDatePicker;
}

void QFDateEdit::showDatePicker()
{
	qfTrash() << QF_FUNC_NAME;
	QFDatePicker *dp = datePicker();
	if(isNull()) dp->calendar()->setSelectedDate(QDate::currentDate());
	else dp->calendar()->setSelectedDate(f_dateEdit->date());
	QPoint p = mapToGlobal(QPoint(0, height()));
	QSize s(width(), 250);
	dp->setGeometry(QRect(p, s));
	qfTrash() << "dp width():" << dp->width();
	dp->raise();
	dp->show();
	p = mapToGlobal(dp->rect().topLeft());
	s = dp->rect().size();
	qfTrash() << "p.x:" << p.x() << "s.width():" << s.width() << "desktop width:" << qApp->desktop()->width();
	int dx = p.x() + s.width() - qApp->desktop()->width();
	int dy = p.y() + s.height() - qApp->desktop()->height();
	if(dx > 0 || dy > 0) {
		p = dp->pos();
		if(dx > 0) { p.setX(p.x() - dx); }
		if(dy > 0) { p.setY(p.y() - dy); }
		dp->move(p);
	}
}

bool QFDateEdit::isNull() const
{
	return f_dateEdit->isNull();
}

QDate QFDateEdit::date() const
{
	if(isNull()) return QDate();
	return f_dateEdit->date();
}

void QFDateEdit::setDate(const QDate &d)
{
	qfTrash() << QF_FUNC_NAME << d.toString() << "is null:" << d.isNull();
	f_dateEdit->setDate(d);
	qfTrash() << "\tdate:" << date().toString();
}

bool QFDateEdit::setReadOnly(bool ro)
{
	//qfTrash() << QF_FUNC_NAME << objectName() << "set read only" << ro;
	bool ret = f_dateEdit->isReadOnly();
	f_dateEdit->setReadOnly(ro);
	btPickDate->setVisible(!ro && !calendarPopup());
	return ret;
}

bool QFDateEdit::isReadOnly() const
{
	return f_dateEdit->isReadOnly();
}

void QFDateEdit::DateEdit::setDate(const QDate &d)
{
	//qfTrash().noSpace() << QF_FUNC_NAME << " '" << d.toString() << "' is null:" << d.isNull();
	if(d.isNull()) setNull();
	else QDateEdit::setDate(d);
	//repaint();
	//update();
}

bool QFDateEdit::calendarPopup() const
{
	return (f_dateEdit)? f_dateEdit->calendarPopup(): false;
}

void QFDateEdit::setCalendarPopup(bool b)
{
	btPickDate->setVisible(!b);
	f_dateEdit->setCalendarPopup(b);
}

QDate QFDateEdit::minimumDate() const
{
	return f_dateEdit->minimumDate();  
}

void QFDateEdit::setMinimumDate(const QDate &d)
{
	f_dateEdit->setMinimumDate(d);
}

QDate QFDateEdit::maximumDate() const
{
	return f_dateEdit->maximumDate();
}

void QFDateEdit::setMaximumDate(const QDate &d)
{
	f_dateEdit->setMaximumDate(d);
}

//========================================================
//                                              QFDateEdit::DateEdit
//========================================================
QFDateEdit::DateEdit::DateEdit(QWidget *parent)
	: QDateEdit(parent)
{
	setMinimumWidth(100);
}

void QFDateEdit::DateEdit::stepBy(int steps)
{
	qfTrash() << QF_FUNC_NAME;
	QFDateEdit *de = qobject_cast<QFDateEdit*>(parent());
	if(de && de->isIncrementByOneDay()) setDate(date().addDays(steps));
	else QDateEdit::stepBy(steps);
}


QAbstractSpinBox::StepEnabled QFDateEdit::DateEdit::stepEnabled() const
{
	//qfTrash() << QF_FUNC_NAME;
	QAbstractSpinBox::StepEnabled ret = QAbstractSpinBox::StepNone;
	bool ro = isReadOnly();
	if(!ro) {
		ret = QAbstractSpinBox::StepUpEnabled | QAbstractSpinBox::StepDownEnabled;
	}
	//qfTrash() << "\treturn:" << ret;
	return ret;
}

QString QFDateEdit::DateEdit::textFromDateTime(const QDateTime &dateTime) const
{
	qfLogFuncFrame() << dateTime.toString() << "is null:" << dateTime.isNull() << "my isNull:" << isNull();
	//qfTrash() << QFLog::stackTrace();
	if(isNull()) return QString();
	return QDateEdit::textFromDateTime(dateTime);
}

void QFDateEdit::DateEdit::mousePressEvent(QMouseEvent *e)
{
	//qfInfo() << QF_FUNC_NAME << date().toString();
	if(date().year() < 1800) setDate(QDate::currentDate());
	//qfInfo() << "\t" << date().toString();
	QDateEdit::mousePressEvent(e);
}

