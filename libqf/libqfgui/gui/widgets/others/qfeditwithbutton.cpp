
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfeditwithbutton.h"

#include <QHBoxLayout>
#include <QPushButton>
#include <QLabel>

#include <qflogcust.h>

QFEditWithButton::QFEditWithButton(QWidget *parent)
	: QWidget(parent)
{
	QHBoxLayout *ly = new QHBoxLayout(this);
	ly->setSpacing(0);
	ly->setMargin(0);
	lineEdit = new QLineEdit(this);
	//lineEdit->setText("AHOJ");
	button = new QPushButton(this);
	button->setText("...");
	button->setMinimumWidth(30);
	button->setMaximumWidth(30);
	//button->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
	ly->insertWidget(0, button);
	ly->insertWidget(0, lineEdit);
	
	setEditable(false);

	connect(button, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));
	connect(lineEdit, SIGNAL(editingFinished()), this, SIGNAL(editingFinished()));
}

QFEditWithButton::~QFEditWithButton()
{
}
		
bool QFEditWithButton::isEditable() const
{
	return f_editable;
}
		
void QFEditWithButton::setEditable(bool b)
{
	f_editable = b;
	lineEdit->setReadOnly(b);
}
		
void QFEditWithButton::setReadOnly(bool b)
{
	qfTrash() << QF_FUNC_NAME << objectName() << b;
	lineEdit->setReadOnly(b);
	if(!isEditable()) lineEdit->setReadOnly(true);
	button->setVisible(!b);
}
		
bool QFEditWithButton::isReadOnly() const
{
	return lineEdit->isReadOnly();
}
		
void QFEditWithButton::setText(const QString &_text)
{
	qfTrash() << QF_FUNC_NAME << _text;
	lineEdit->setText(_text);
}
/*
bool QFEditWithButton::isButtonVisible() const
{
	return button->isVisible();
}
		
void QFEditWithButton::setButtonVisible(bool b)
{
	qfInfo() << "set button visible:" << b;
	button->setVisible(b);
	qfInfo() << "button visible:" << button->isVisible() << isButtonVisible();
}
*/		
QString QFEditWithButton::text() const
{
	return lineEdit->text();
}
		
void QFEditWithButton::keyPressEvent(QKeyEvent *e)
{
/*	if((e->key() == Qt::Key_Enter || e->key() == Qt::Key_Return) && e->modifiers() == 0) {
		if(button->isVisible() && button->isEnabled()) {
			slotButtonClicked();
			e->accept();
			return;
		}
	}*/
	QWidget::keyPressEvent(e);
}


