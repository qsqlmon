
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "ui_qfsplashscreen.h"
#include "qfsplashscreen.h"

#include <qflogcust.h>

QFSplashScreen::QFSplashScreen(const QPixmap &_pixmap, Qt::WFlags f)
	: QSplashScreen(_pixmap, f)
{
	ui = new Ui::QFSplashScreen;
	ui->setupUi(this);
	resize(pixmap().size());
	ui->frmSplash->setWindowOpacity(0.5);
}

QFSplashScreen::~QFSplashScreen() 
{
	delete ui;
}

void QFSplashScreen::setSplashLabel(const QString &text)
{
	qfTrash() << QF_FUNC_NAME;
	ui->lblSplash->setText(text);
}
