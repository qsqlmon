
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdockwidget.h"

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

QFDockWidget::QFDockWidget(QWidget *_parent, Qt::WFlags flags)
	: QDockWidget(_parent, flags)
{
	fPreferredArea = Qt::TopDockWidgetArea;
}

QFDockWidget::~QFDockWidget()
{
}

