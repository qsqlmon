
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qftabwidget.h"

#include <qf.h>
#include <qfassert.h>

#include <qflogcust.h>

//======================================================
//                                          QFTabBar
//======================================================
QFTabBar::QFTabBar(QWidget *parent)
	: QTabBar(parent), oldIndex(-1), ignoreCurrentChanged_hook(false)
{
	qfTrash() << QF_FUNC_NAME;
	/// abych mohl volat QFTabWidget::currentIndexAboutToChange(int new_index),
	/// musim ziskat kontrolu nad emitovanim signalu currentChanged(int new_index).
	/// Je to takova jemna pracicka, musim pripojit QTabBar::currentChanged() signal na hook
	/// a pak emitovat QFTabBar::currentChanged(), pokud je to povoleno funkci currentIndexAboutToChange().
	const QMetaObject *smeta = metaObject()->superClass();
	int sigix = smeta->indexOfSignal(QMetaObject::normalizedSignature("currentChanged(int)"));
	//qfTrash() << "\tsig ix:" << sigix << "orig:" << metaObject()->indexOfSignal(QMetaObject::normalizedSignature("currentChanged(int)"));
	QF_ASSERT(sigix >= 0, ":()");
	int slotix = metaObject()->indexOfSlot(QMetaObject::normalizedSignature("currentChanged_hook(int)"));
	//qfTrash() << "\tslot ix:" << slotix;
	QF_ASSERT(slotix >= 0, ":()");
	QMetaObject::connect(this, sigix, this, slotix);
	//QF_CONNECT(this, SIGNAL(QTabBar::currentChanged(int)), this, SLOT(hook(int)));
}

void QFTabBar::currentChanged_hook(int ix)
{
	qfTrash() << QF_FUNC_NAME;
	qfTrash() << "\told:" << oldIndex << "new:" << ix;
	if(oldIndex >= 0) {
		QFTabWidget *w = tabWidget();
		if(w) {
			if(oldIndex == ix) {
				qfTrash() << "\tRETURN (old index == ix)";
				return;
			}
			/// hned to vrat a zeptej se, jestli je to mozny
			/// vratit ho musim, aby QTabWidget::currentIndex() vracel dosud neprepnutou hodnotu
			qfTrash() << "\tBACK";
			setCurrentIndex(oldIndex);
			qfTrash() << "\tASK";
			bool ret = w->currentIndexAboutToChange(ix);
			if(ret) {
				qfTrash() << "\t" << oldIndex << "TO:" << ix;
				oldIndex = ix;
				setCurrentIndex(ix);
				qfTrash() << "\temit currentChanged(ix)";
				emit currentChanged(ix);
			}
			else {
				qfTrash() << "\tIGNORE";
			}
			qfTrash() << "\tRETURN";
			return;
		}
		qfTrash() << "\temiting:" << ix;
	}
	oldIndex = ix;
	emit currentChanged(ix);
	qfTrash() << "\tRETURN (old index < 0)) emit currentChanged(ix)";
}
				
QFTabWidget* QFTabBar::tabWidget() const
{
	QFTabWidget *w = qfFindParent<QFTabWidget*>(this, !Qf::ThrowExc);
	return w;
}

//======================================================
//                                          QFTabWidget
//======================================================
QFTabWidget::QFTabWidget(QWidget *parent)
	: QTabWidget(parent)
{
	setTabBar(new QFTabBar(this));
}

QFTabWidget::~QFTabWidget()
{
}

