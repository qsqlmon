
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSTATUSBAR_H
#define QFSTATUSBAR_H

#include <qfguiglobal.h>
#include <qfexception.h>


#include <QStatusBar>

class QLabel;
class QProgressBar;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFStatusBar : public QStatusBar
{
	Q_OBJECT
			Q_PROPERTY(QString text READ text WRITE setText);
	protected:
		QProgressBar *fProgressBar;
		QLabel *fProgressBarLabel;

		QList<QLabel*> labels;
	protected:
		void addLabel();
	public slots:
		//! Set progress bar value, if \a val is out of <0, 1> \a hideProgress() is called instead.
		void setProgressValue(double val, const  QString &label_text = QString());
		void setLabelText(int label_no, const QString &text);
	public:
		//! Create status bar with \a label_cnt labels from left and the progress bar.
		//! Minimal \a label_cnt is 1.
		QFStatusBar(QWidget *parent = NULL);
		virtual ~QFStatusBar();

		//! resets and hides progress bar.
		void hideProgress();
		QProgressBar* progressBar() {return fProgressBar;}
		//! return i-th label from the left or Throw exception.
		QLabel* label(int i) throw(QFException);

		void setText(const QString &txt) {setLabelText(0, txt);}
		QString text();
};
   
#endif // QFSTATUSBAR_H

