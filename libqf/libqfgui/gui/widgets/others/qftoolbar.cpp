
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qftoolbar.h"

QFToolBar::QFToolBar(QWidget * parent)
	: QToolBar(parent)
{
	fPreferredArea = Qt::TopToolBarArea;
}

QFToolBar::~QFToolBar()
{
}

