INCLUDEPATH += $$PWD

#FORMS +=       \
#    $$PWD/$${MY_WIDGET_NAME}.ui

#QT += xml

FORMS +=      \
	$$PWD/qfsplashscreen.ui    \

HEADERS +=       \
    $$PWD/qfsplitter.h       \
    $$PWD/qfdockwidget.h       \
    $$PWD/qftoolbar.h       \
    $$PWD/qfstatusbar.h       \
	$$PWD/qfeditwithbutton.h       \
	$$PWD/qftabwidget.h      \
	$$PWD/qfsplashscreen.h    \
	$$PWD/qfwidgettoolbarbase.h  \

SOURCES +=       \
    $$PWD/qfsplitter.cpp       \
    $$PWD/qfdockwidget.cpp       \
    $$PWD/qftoolbar.cpp       \
    $$PWD/qfstatusbar.cpp       \
	$$PWD/qfeditwithbutton.cpp       \
	$$PWD/qftabwidget.cpp      \
	$$PWD/qfsplashscreen.cpp    \
	$$PWD/qfwidgettoolbarbase.cpp  \


