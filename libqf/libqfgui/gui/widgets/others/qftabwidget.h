
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFTABWIDGET_H
#define QFTABWIDGET_H

#include <qfguiglobal.h>

#include <QTabWidget>
#include <QTabBar>

class QFTabWidget;

class QFTabBar : public QTabBar
{
	Q_OBJECT
	private:
		/// Vraci rodicovsky tab widget nebo NULL.
		QFTabWidget* tabWidget() const;
		int oldIndex;
		bool ignoreCurrentChanged_hook;
	protected slots:
		void currentChanged_hook(int ix);
	public:
		QFTabBar(QWidget *parent);
		virtual ~QFTabBar() {}
	signals:
		void currentChanged(int index);
};


//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFTabWidget : public QTabWidget
{
	Q_OBJECT
			friend class QFTabBar;
	protected:
		/// pokud vrati false, k prepnuti indexu nedojde.
		virtual bool currentIndexAboutToChange(int new_index) {Q_UNUSED(new_index); return true;}
	public:
		QFTabWidget(QWidget *parent = NULL);
		virtual ~QFTabWidget();
};
   
#endif // QFTABWIDGET_H

