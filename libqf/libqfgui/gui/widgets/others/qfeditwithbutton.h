
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFEDITWITHBUTTON_H
#define QFEDITWITHBUTTON_H

#include <qfguiglobal.h>

#include <QLineEdit>

class QLineEdit;
class QPushButton;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFEditWithButton : public QWidget
{
	Q_OBJECT;
	Q_PROPERTY(QString text READ text WRITE setText);
	Q_PROPERTY(bool editable READ isEditable WRITE setEditable);
	//Q_PROPERTY(bool buttonVisible READ isButtonVisible WRITE setButtonVisible);
	public:
		bool isEditable() const;
		void setEditable(bool b = true);
	protected:
		bool f_editable;
		virtual void keyPressEvent(QKeyEvent *e);
	public:
		QLineEdit *lineEdit;
		QPushButton *button;
		
		virtual void setWidgetReadOnly(bool b) {setReadOnly(b);}
		virtual void setReadOnly(bool b = true);
		virtual bool isReadOnly() const;

		//bool isButtonVisible() const;
		//void setButtonVisible(bool b = true);
	protected slots:
		virtual void slotButtonClicked() {emit buttonClicked();}
		//void slotLineEditingFinished();
	signals:
		void buttonClicked();
		void editingFinished();
	public slots:
		virtual void setText(const QString &_text);
	public:
		QString text() const;
	public:
		QFEditWithButton(QWidget *parent = NULL);
		virtual ~QFEditWithButton();
};
   
#endif // QFEDITWITHBUTTON_H

