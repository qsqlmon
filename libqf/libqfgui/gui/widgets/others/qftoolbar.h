
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFTOOLBAR_H
#define QFTOOLBAR_H

#include <qfguiglobal.h>
#include <qfclassfield.h>

#include <QToolBar>
#include <QMainWindow>

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFToolBar : public QToolBar
{
	Q_OBJECT;
	Q_PROPERTY(Qt::ToolBarArea preferredArea READ preferredArea WRITE setPreferredArea DESIGNABLE (qobject_cast<QMainWindow *>(parentWidget()) != 0));
	QF_FIELD_RW(QString, i, setI, d);
	protected:
		Qt::ToolBarArea fPreferredArea;
	public:
		void setPreferredArea(Qt::ToolBarArea area) {fPreferredArea = area;}
		Qt::ToolBarArea preferredArea() const {return fPreferredArea;}
	public:
		QFToolBar(QWidget *parent = 0);
		virtual ~QFToolBar();
};
   
#endif // QFTOOLBAR_H

