#ifndef QFSPLITTER_H
#define QFSPLITTER_H

#include <qfguiglobal.h>
#include <qfxmlconfigpersistenter.h>


#include <QSplitter>

//! Splitter with persistent sizes.
class QFGUI_DECL_EXPORT QFSplitter : public QSplitter, public QFXmlConfigPersistenter
{
	public:
		QFSplitter(QWidget *parent = 0);
		virtual ~QFSplitter();
	public:
		void loadPersistentData();
		void savePersistentData();
};
   
#endif // QFSPLITTER_H

