#include <QVariant>

#include <qfxmlconfigelement.h>

#include "qfsplitter.h"

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

QFSplitter::QFSplitter(QWidget *parent) 
	: QSplitter(parent)
{
}

QFSplitter::~QFSplitter()
{
	savePersistentData();
}

void  QFSplitter::savePersistentData()
{
	if(!xmlConfigPersistentId().isEmpty()) {
		qfTrash() << QF_FUNC_NAME << xmlConfigPersistentId();
		//QFXmlConfigElement el = createPersistentPath();
		// save sizes
		QStringList sl;
		foreach(int i, sizes()) 	sl << QString::number(i);
		qfTrash() << "\t sizes:" << sl.join(" ");
		setPersistentValue("sizes", QVariant(sl.join(" ")));
	}
}

void  QFSplitter::loadPersistentData()
{
	if(!xmlConfigPersistentId().isEmpty()) {
		qfTrash() << QF_FUNC_NAME << xmlConfigPersistentId();
		//QFXmlConfigElement el = persistentPath();
		// load sizes
		QString s = persistentValue("sizes").toString().trimmed();
		if(!s.isEmpty()) {
			QList<int> szs;
			QStringList sl = s.split(" ");
			qfTrash() << "\t loaded sizes:" << sl.join(" ");
			bool all_zeros = true;
			foreach(s, sl) {
				int i = s.toInt();
				if(i != 0) all_zeros = false;
				szs << i;
			}
			if(!all_zeros) {
				/// all sizes zeros is a nosense
				qfTrash() << "\t set sizes";
				setSizes(szs);
			}
		}
	}
}

