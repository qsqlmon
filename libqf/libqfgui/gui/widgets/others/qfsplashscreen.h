
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSPLASHSCREEN_H
#define QFSPLASHSCREEN_H

#include <qfguiglobal.h>

#include <QSplashScreen>


namespace Ui {class QFSplashScreen;};

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSplashScreen : public QSplashScreen
{
	Q_OBJECT
	private:
		Ui::QFSplashScreen *ui;
	public:
		QFSplashScreen(const QPixmap &pixmap = QPixmap(), Qt::WFlags f = 0);
		virtual ~QFSplashScreen();
	public slots:
		void setSplashLabel(const QString &text);
};
   
#endif // QFSPLASHSCREEN_H

