
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFWIDGETTOOLBARBASE_H
#define QFWIDGETTOOLBARBASE_H

#include <qfguiglobal.h>

#include <QToolBar>

class QFAction;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFWidgetToolBarBase : public QToolBar
{
	Q_OBJECT;
	protected:
		void addActions(QList<QFAction*> actions);
		void addActions(QWidget *w);
	public:
		QFWidgetToolBarBase(QWidget *parent = NULL); 
		virtual ~QFWidgetToolBarBase();
};
   
class QFGUI_DECL_EXPORT QFWidgetToolBar : public QFWidgetToolBarBase
{
	Q_OBJECT;
	protected:
		void addActions(QList<QFAction*> actions);
	public slots:
		/// Kdyz Widget emituje signal connectRequest(), vytvori si buttony z jejich akci a odpoji se od signalu.
		virtual void connectWidget(QWidget *sender);
	public:
		QFWidgetToolBar(QWidget *parent = NULL) : QFWidgetToolBarBase(parent) {}
		virtual ~QFWidgetToolBar() {}
};
   
#endif // QFWIDGETTOOLBARBASE_H

