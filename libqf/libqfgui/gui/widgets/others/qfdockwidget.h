
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDOCKWIDGET_H
#define QFDOCKWIDGET_H

#include <QDockWidget>
#include <QMainWindow>

#include <qfguiglobal.h>

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFDockWidget : public QDockWidget
{
	Q_OBJECT
	Q_PROPERTY(Qt::DockWidgetArea preferredArea READ preferredArea WRITE setPreferredArea
			DESIGNABLE (qobject_cast<QMainWindow *>(parentWidget()) != 0))
	protected:
		Qt::DockWidgetArea fPreferredArea;
	public:
		void setPreferredArea(Qt::DockWidgetArea area) {fPreferredArea = area;}
		Qt::DockWidgetArea preferredArea() const {return fPreferredArea;}
	public:
		QFDockWidget(QWidget *parent = 0, Qt::WFlags flags = 0);
		virtual ~QFDockWidget();
};
   
#endif // QFDOCKWIDGET_H

