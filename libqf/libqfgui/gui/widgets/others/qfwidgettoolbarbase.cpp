
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfwidgettoolbarbase.h"

#include <qf.h>
#include <qfaction.h>

#include <QLabel>

#include <qflogcust.h>

//======================================================
//                                    QFWidgetToolBarBase
//======================================================
QFWidgetToolBarBase::QFWidgetToolBarBase(QWidget *parent)
	: QToolBar(parent)
{
	setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	QLabel *lbl = new QLabel("not inited");
	lbl->setObjectName("notInited");
	addWidget(lbl);
}

QFWidgetToolBarBase::~QFWidgetToolBarBase()
{
}

void QFWidgetToolBarBase::addActions(QList<QFAction*> actions)
{
	QLabel *lbl = findChild<QLabel*>("notInited");
	SAFE_DELETE(lbl);
	foreach(QFAction *a, actions) {
		//qfInfo() << "\taddidng action:" << a->id() << "visible:" << a->isVisible();
		addAction(a);
	}
}

void QFWidgetToolBarBase::addActions(QWidget *w)
{
	if(!w) return;
	QLabel *lbl = findChild<QLabel*>("notInited");
	SAFE_DELETE(lbl);
	foreach(QAction *a, w->actions()) addAction(a);
}

//======================================================
//                                    QFWidgetToolBar
//======================================================
void QFWidgetToolBar::connectWidget(QWidget *sender)
{
	QFWidgetToolBarBase::addActions(sender);
}
