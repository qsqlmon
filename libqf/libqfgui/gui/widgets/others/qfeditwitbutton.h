
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFEDITWITBUTTON_H
#define QFEDITWITBUTTON_H

#include <qfguiglobal.h>

#include <QWidget>


//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFEditWitButton : public QWidget
{
	public:
		QFEditWitButton(QWidget *parent = NULL);
		virtual ~QFEditWitButton();
};
   
#endif // QFEDITWITBUTTON_H

