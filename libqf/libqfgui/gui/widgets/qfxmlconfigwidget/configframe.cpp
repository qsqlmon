//
// C++ Implementation: configframe
//
// Description:
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "configframe.h"
#include "qfxmlconfigwidget.h"

#include <qfassert.h>
#include <qffileutils.h>
#include <qfeditwithbutton.h>
#include <qfmessage.h>

#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QFileDialog>
#include <QCheckBox>
#include <QComboBox>
#include <QGroupBox>
#include <QSpinBox>
#include <QSpacerItem>

#include <float.h> /// definice DBL_MIN a DBL_MAX

#include <qflogcust.h>

//=====================================
//                             ConfigFrame
//=====================================
ConfigFrame::ConfigFrame(QFXmlConfig *doc, const QFXmlConfigElement &el, QWidget *parent)
	: QScrollArea(parent), f_config(doc), f_element(el)
{
	setWidgetResizable(true);
	f_frame = new QGroupBox(this);
	//frame->setObjectName();
	f_frame->setTitle(f_element.caption());
	if(f_element.isCheckable()) {
		f_frame->setCheckable(true);
		f_frame->setChecked(doc->value(f_element).toBool());
	}
	QGridLayout *ly = new QGridLayout(f_frame);
	ly->setSpacing(3);
	ly->setMargin(2);
	setWidget(f_frame);
	setFrameShape(QFrame::NoFrame);
	//QLabel *lbl = new QLabel("ahoj", frame);
	//ly->addWidget(lbl, 0, 0);
	//setWidget(new QLabel("ahoj", this));
	connect(f_frame, SIGNAL(toggled(bool)), this, SLOT(groupBoxCheckToggled(bool)));
}

ConfigFrame::~ConfigFrame()
{
}

void ConfigFrame::groupBoxCheckToggled(bool on)
{
	dirty = true;
	f_config->setValue(f_element, on);
	emit valueChanged(f_element.path(), on);
}

QString ConfigFrame::needRestartMsg()
{
	return tr("Changes in option '%1' will be applied after the application restart.");
}

void ConfigFrame::save()
{
	//qfTrash() << QF_FUNC_NAME << element.tagName() << "dirty:" << dirty << "needsAppRestart" << element.option("needsAppRestart").toBool();
	if(dirty && f_element.options().attribute("needsAppRestart").toBool()) {
		QFMessage::information(this, ConfigFrame::needRestartMsg().arg(f_element.path()));
	}
	dirty = false;
}

ConfigWidget* ConfigFrame::createConfigWidget(const QFXmlConfigElement &el)
{
	//qfTrash() << QF_FUNC_NAME << "name:" << el.nodeName();
	ConfigWidget *w = NULL;
	if(!f_config) return w;
	if(el.type() == QFXmlConfigElement::Text) {
		w = new TextConfigWidget(f_config, el, f_frame);
	}
	else if(el.type() == QFXmlConfigElement::Int) {
		w = new IntConfigWidget(f_config, el, f_frame);
	}
	else if(el.type() == QFXmlConfigElement::Float) {
		w = new FloatConfigWidget(f_config, el, f_frame);
	}
	else if(el.type() == QFXmlConfigElement::Bool) {
		w = new BoolConfigWidget(f_config, el, f_frame);
	}
	else if(el.type() == QFXmlConfigElement::List) {
		w = new ListConfigWidget(f_config, el, f_frame);
	}
	else if(el.type() == QFXmlConfigElement::Dir) {
		w = new DirConfigWidget(f_config, el, f_frame);
	}
	else if(el.type() == QFXmlConfigElement::File) {
		w = new FileConfigWidget(f_config, el, f_frame);
	}
	else {
		//qfWarning() << "Unknown config type" << el.tagName() << el.type();
	}
	return w;
}

bool ConfigFrame::addConfigElement(const QFXmlConfigElement &el)
{
	qfLogFuncFrame() << "name:" << el.nodeName() << "type:" << el.type() << "is leaf:" << el.isLeaf();
	//qfTrash() << "\tConfigPathFragment" << QFXmlConfigElement::ConfigPathFragment << "(el.type() > QFXmlConfigElement::ConfigPathFragment):" << (el.type() > QFXmlConfigElement::ConfigPathFragment);
	ConfigWidget *w = createConfigWidget(el);
	if(!w) return false;
	connect(w, SIGNAL(valueChanged(QString,QVariant)), this, SIGNAL(valueChanged(QString,QVariant)));
	QFXmlConfigWidget *parent_w = qfFindParent<QFXmlConfigWidget*>(this, !Qf::ThrowExc);
	if(parent_w) {
		qfTrash() << "parent widget read only:" << parent_w->isReadOnly();
		w->setReadOnly(parent_w->isReadOnly());
		w->setDefaultButtonVisible(parent_w->isDefaultButtonsVisible());
	}
	//qfTrash() << "\tiface:" << iface;
	QLabel *lbl = NULL;
	if(w->needLabel()) {
		lbl = new QLabel(el.caption(), f_frame);
	}
	QGridLayout *ly = qobject_cast<QGridLayout*>(f_frame->layout());
	QF_ASSERT(ly, "layout is not the QGrigLayOut");
	//qfTrash() << "widget:" << iface->widget();
	QLabel *lbl_unit = NULL;
	QString unit = el.unit();
	if(!unit.isEmpty()) { lbl_unit = new QLabel(unit); }
	int r = ly->rowCount();
	if(w->labelOrientation() == Qt::Vertical) {
		if(lbl) ly->addWidget(lbl, r++, 0, 1, 3);
		if(lbl_unit) {
			ly->addWidget(w, r, 0, 1, 2);
			ly->addWidget(lbl_unit, r, 2);
		}
		else ly->addWidget(w, r, 0, 1, 3);
	}
	else {
		int c = 0;
		if(lbl) {
			ly->addWidget(lbl, r, 0);
			c++;
		}
		if(lbl_unit) {
			ly->addWidget(w, r, c);
			ly->addWidget(lbl_unit, r, 2);
		}
		else ly->addWidget(w, r, c ,1 , 3-c);
	}
	return true;
}

void ConfigFrame::addSpacer()
{
	QGridLayout *ly = qobject_cast<QGridLayout*>(f_frame->layout());
	QF_ASSERT(ly, "@@#$%^&");
	QSpacerItem *spacer = new QSpacerItem(10, 10, QSizePolicy::Minimum, QSizePolicy::Expanding);
	ly->addItem(spacer, ly->rowCount(), 0);
}

//=====================================
//                             ConfigWidget
//=====================================
ConfigWidget::ConfigWidget(QFXmlConfig *doc, const QFXmlConfigElement &el, QWidget *parent)
	: QWidget(parent), config(doc), configElement(el), dirty(false)
{
	static QIcon ico_defval = QIcon(":/libqfgui/images/default_value.png");

	qfLogFuncFrame();
	widgetLayout = new QHBoxLayout(this);
	widgetLayout->setSpacing(0);
	widgetLayout->setMargin(0);
	btDefault = new QPushButton(this);
	btDefault->setIcon(ico_defval);
	btDefault->setMaximumWidth(30);
	//bt_default->setMaximumSize(QSize(20, 20));
	btDefault->setToolTip(tr("default value"));
	//bt_default->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
	connect(btDefault, SIGNAL(clicked()), this, SLOT(setDefaultValue()));
	widgetLayout->addWidget(btDefault);
	//qfTrash() << "\tconnecting destroyed:" << connect(this, SIGNAL(destroyed(QObject*)), this, SLOT(save()));
}

ConfigWidget::~ConfigWidget()
{
	qfLogFuncFrame();
	//save();
}

Qt::Orientation ConfigWidget::labelOrientation()
{
	QFDomElement el = configElement;
	QFString s = el.attribute("orientation");
	if(s == "vertical") return Qt::Vertical;
	return Qt::Horizontal;
}

void ConfigWidget::save()
{
	qfLogFuncFrame() << configElement.tagName() << "dirty:" << dirty << "needsAppRestart" << configElement.option("needsAppRestart").toBool();
	if(dirty && configElement.options().attribute("needsAppRestart").toBool()) {
		QFMessage::information(this, ConfigFrame::needRestartMsg().arg(configElement.path()));
	}
	dirty = false;
}
/*
bool ConfigWidget::setElementDataDirty(bool b)
{
	ConfigFrame *frm = qfFindParent<ConfigFrame*>(this);
	frm->setDataDirty(b);
}
*/
QVariant ConfigWidget::value(const QVariant &default_val)
{
	if(!config) return QVariant();
	return config->value(configElement, default_val);
}

void ConfigWidget::setValue(const QVariant &val)
{
	qfLogFuncFrame();
	//qfInfo() << configElement.path() << "value:" << val.toString() << "old val:" << value(QVariant()).toString();
	if(!config) return;
	dirty = (val != value(QVariant()));
	config->setValue(configElement, val);
}

bool ConfigWidget::isDefaultButtonVisible() const
{
	return btDefault->isVisible();
}

void ConfigWidget::setDefaultButtonVisible(bool b)
{
	btDefault->setVisible(b);
}

//=====================================
//                             TextConfigWidget
//=====================================
TextConfigWidget::TextConfigWidget(QFXmlConfig *doc, const QFXmlConfigElement &_el, QWidget *parent)
	: ConfigWidget(doc, _el, parent)
{
	qfLogFuncFrame() << _el.toString();
	lineEdit = new QLineEdit(this);
	widgetLayout->insertWidget(0, lineEdit);
	QFDomElement el = configElement.options();
	QFString s = el.attribute("echo");
	if(!!s) {
		if(s == "password") lineEdit->setEchoMode(QLineEdit::Password);
		else if(s == "noecho") lineEdit->setEchoMode(QLineEdit::NoEcho);
		else if(s == "passwordechoonedit") lineEdit->setEchoMode(QLineEdit::PasswordEchoOnEdit);
	}
	s = value(configElement.defaultValue()).toString();
	qfTrash() << "\t set text:" << s;
	lineEdit->setText(s);
	lineEdit->setToolTip(configElement.help());
	connect(lineEdit, SIGNAL(editingFinished()), this, SLOT(lineEditEditingFinished()));
}

void TextConfigWidget::lineEditEditingFinished()
{
	emit valueChanged(configElement.path(), lineEdit->text());
}

void TextConfigWidget::save()
{
	qfLogFuncFrame();
	//qfInfo() << QFLog::stackTrace();
	QFString s = lineEdit->text();
	setValue(s);
	ConfigWidget::save();
}

void TextConfigWidget::setDefaultValue()
{
	lineEdit->setText(configElement.defaultValue().toString());
}

void TextConfigWidget::setReadOnly(bool ro)
{
	lineEdit->setReadOnly(ro);
}

bool TextConfigWidget::isReadOnly() const
{
	return lineEdit->isReadOnly();
}

//=====================================
//                             IntConfigWidget
//=====================================
IntConfigWidget::IntConfigWidget(QFXmlConfig *doc, const QFXmlConfigElement &_el, QWidget *parent)
	: ConfigWidget(doc, _el, parent)
{
	qfLogFuncFrame();
	spinBox = new QSpinBox(this);
	widgetLayout->insertWidget(0, spinBox);
	QFDomElement el = configElement.options().cd("range", !Qf::ThrowExc);
	spinBox->setMinimum((!!el && el.hasAttribute("min"))? el.attribute("min").toInt(): INT_MIN);
	spinBox->setMaximum((!!el && el.hasAttribute("max"))? el.attribute("max").toInt(): INT_MAX);
	spinBox->setValue(value(configElement.defaultValue()).toInt());
	spinBox->setToolTip(configElement.help());
	spinBox->setAlignment(Qt::AlignRight);
	//setMaximumWidth(100);
	dirty = false;
	connect(spinBox, SIGNAL(valueChanged(int)), this, SLOT(spinBoxValueChanged()));
}

void IntConfigWidget::spinBoxValueChanged()
{
	qfLogFuncFrame();
	save();
	emit valueChanged(configElement.path(), spinBox->value());
}

void IntConfigWidget::save()
{
	qfLogFuncFrame();
	QFString s = QString::number(spinBox->value());
	setValue(s);
	ConfigWidget::save();
}

void IntConfigWidget::setDefaultValue()
{
	qfLogFuncFrame();
	spinBox->setValue(configElement.defaultValue().toInt());
	//dirty = false;
}

void IntConfigWidget::setReadOnly(bool ro)
{
	//qfLogFuncFrame();
	spinBox->setReadOnly(ro);
}
bool IntConfigWidget::isReadOnly() const
{
	return spinBox->isReadOnly();
}

//=====================================
//                             FloatConfigWidget
//=====================================
FloatConfigWidget::FloatConfigWidget(QFXmlConfig *doc, const QFXmlConfigElement &_el, QWidget *parent)
	: ConfigWidget(doc, _el, parent)
{
	qfLogFuncFrame();
	spinBox = new QDoubleSpinBox(this);
	widgetLayout->insertWidget(0, spinBox);
	QFDomElement el = configElement.options().cd("range", !Qf::ThrowExc);
	//qfInfo() << "!!el:" << (!!el == true);
	spinBox->setMinimum((!!el && el.hasAttribute("min"))? el.attribute("min").toDouble(): -1e99); /// DBL_MIN nejak nefunguje
	//spinBox->setMinimum(-1000);
	//qfInfo() << "min:" << spinBox->minimum();
	spinBox->setMaximum((!!el && el.hasAttribute("max"))? el.attribute("max").toDouble(): 1e99); /// DBL_MAX nejak nefunguje
	spinBox->setDecimals((!!el && el.hasAttribute("decimals"))? el.attribute("decimals").toInt(): 2);
	spinBox->setValue(value(configElement.defaultValue()).toDouble());
	spinBox->setToolTip(configElement.help());
	spinBox->setAlignment(Qt::AlignRight);
	//setMaximumWidth(100);
	dirty = false;
	connect(spinBox, SIGNAL(valueChanged(double)), this, SLOT(spinBoxValueChanged()));
}

void FloatConfigWidget::spinBoxValueChanged()
{
	qfLogFuncFrame();
	save();
	emit valueChanged(configElement.path(), spinBox->value());
}

void FloatConfigWidget::save()
{
	qfLogFuncFrame();
	QFString s = QString::number(spinBox->value());
	setValue(s);
	ConfigWidget::save();
}

void FloatConfigWidget::setDefaultValue()
{
	spinBox->setValue(configElement.defaultValue().toDouble());
	//dirty = false;
}

void FloatConfigWidget::setReadOnly(bool ro)
{
	spinBox->setReadOnly(ro);
}

bool FloatConfigWidget::isReadOnly() const
{
	return spinBox->isReadOnly();
}

//=====================================
//                             BoolConfigWidget
//=====================================
BoolConfigWidget::BoolConfigWidget(QFXmlConfig *doc, const QFXmlConfigElement &_el, QWidget *parent)
	: ConfigWidget(doc, _el, parent)
{
	checkBox = new QCheckBox(this);
	widgetLayout->insertWidget(0, checkBox);
	checkBox->setText(configElement.caption());
	checkBox->setToolTip(configElement.help());
	load();
	connect(checkBox, SIGNAL(stateChanged(int)), this, SLOT(checkBoxStateChanged(int)));
}

void BoolConfigWidget::checkBoxStateChanged(int val)
{
	save();
	emit valueChanged(configElement.path(), val); 
}

void BoolConfigWidget::load()
{
	//qfLogFuncFrame() << tagName();
	QVariant v  = value(configElement.defaultValue());
	if(v.type() == QVariant::Bool) {
		checkBox->setChecked(v.toBool());
	}
	else {
		bool ok;
		int i = v.toInt(&ok);
		//qfInfo() << i << "ok:" << ok;
		if(!ok) {
			bool b = QFString(v.toString()).toBool();
			i = (b)? Qt::Checked: Qt::Unchecked;
		}
		switch(i) {
			case Qt::Unchecked: checkBox->setCheckState(Qt::Unchecked); break;
			case Qt::PartiallyChecked: checkBox->setCheckState(Qt::PartiallyChecked); break;
			default: checkBox->setCheckState(Qt::Checked); break;
		}
	}
	//qfInfo() << QF_FUNC_NAME << v.toString();
}

void BoolConfigWidget::save()
{
	//qfTrash() << QF_FUNC_NAME;
	setValue(checkBox->isChecked()? true: false);
	ConfigWidget::save();
}

void BoolConfigWidget::setDefaultValue()
{
	int v  = configElement.defaultValue().toInt();
	switch(v) {
		case 0: checkBox->setCheckState(Qt::Unchecked); break;
		case 2: checkBox->setCheckState(Qt::PartiallyChecked); break;
		default: checkBox->setCheckState(Qt::Checked); break;
	}
}

void BoolConfigWidget::setReadOnly(bool ro)
{
	//qfInfo() << QF_FUNC_NAME << ro;
	checkBox->setEnabled(!ro);
}

bool BoolConfigWidget::isReadOnly() const
{
	return checkBox->isEnabled();
}

void BoolConfigWidget::setValue(const QVariant &val)
{
	ConfigWidget::setValue(val);
	if(val.toBool()) {
		QFXmlConfigElement el = QFDomElement(configElement.parentNode().toElement());
		//qfInfo() << QF_FUNC_NAME << el.tagName();
		if(el.childrenHasExclusiveCheck()) {
			ConfigFrame *cf = qfFindParent<ConfigFrame*>(this);
			if(cf) {
				/// reloadni vsechny bool widgety
				QList<BoolConfigWidget*> lst = cf->QObject::parent()->findChildren<BoolConfigWidget*>();
				foreach(BoolConfigWidget *w, lst) {
					//qfInfo() << w;
					w->load();
				}
			}
		}
	}
}

//=====================================
//                             ListConfigWidget
//=====================================
ListConfigWidget::ListConfigWidget(QFXmlConfig *doc, const QFXmlConfigElement &_el, QWidget *parent)
	: ConfigWidget(doc, _el, parent)
{
	qfLogFuncFrame();
	comboBox = new ComboBox(this);
	widgetLayout->insertWidget(0, comboBox);
	comboBox->setEditable(false);
	comboBox->setToolTip(configElement.help());
	connect(comboBox, SIGNAL(activated(int)), this, SLOT(listItemActivated(int)));
	QFDomElement el = configElement.cd("options/items", !Qf::ThrowExc);
	QFString s = configElement.attribute("editable");
	comboBox->setEditable(s.toBool());
	s = el.text();
	QString delim = el.attribute("delimiter", ",");
	QStringList sl = s.splitAndTrim(delim[0]);
	foreach(s, sl) {
		QString key = s.section(':', 0, 0);
		QString val = s.section(':', 1);
		if(val.isEmpty()) val = key;
		comboBox->addItem(val, key);
	}
	/*
	s = configElement.cd("default", !Qf::ThrowExc).text();
	int ix = 0;
	if(!!s) {
		ix = sl.indexOf(s);
	}
	*/
	//s = value(configElement.defaultValue()).toString();
	s = value(QString()).toString();
	qfTrash() << "\tvalue: " << s;
	int ix = comboBox->findData(s);
	if(ix >= 0) comboBox->setCurrentIndex(ix);
	else {
		if(comboBox->isEditable()) comboBox->setEditText(s);
	}
}

void ListConfigWidget::listItemActivated(int ix)
{
	qfLogFuncFrame();
	save();
	QVariant val = comboBox->itemData(ix);
	emit valueChanged(configElement.path(), val);
}

void ListConfigWidget::save()
{
	qfLogFuncFrame();
	if(comboBox->isEditable()) {
		setValue(comboBox->currentText());
		//qfInfo() << comboBox->currentText();
	}
	else {
		int ix = comboBox->currentIndex();
		if(ix >= 0) setValue(comboBox->itemData(ix).toString());
	}
	ConfigWidget::save();
}

void ListConfigWidget::setDefaultValue()
{
	QString s = configElement.defaultValue().toString();
	int ix = comboBox->findData(s);
	if(ix >= 0) comboBox->setCurrentIndex(ix);
}

void ListConfigWidget::setReadOnly(bool ro)
{
	qfTrash() << QF_FUNC_NAME << ro;
	//abort();
	comboBox->setReadOnly(ro);
}

bool ListConfigWidget::isReadOnly() const
{
	return comboBox->isReadOnly();
}

ListConfigWidget::ComboBox::ComboBox(QWidget *parent)
	: QComboBox(parent)
{
	setReadOnly(false);
}

void ListConfigWidget::ComboBox::showPopup()
{
	if(isReadOnly()) return;
	QComboBox::showPopup();
}

//=====================================
//                             DirConfigWidget
//=====================================
DirConfigWidget::DirConfigWidget(QFXmlConfig *doc, const QFXmlConfigElement &_el, QWidget *parent)
	: ConfigWidget(doc, _el, parent)
{
	edit = new QFEditWithButton(this);
	edit->setEditable(true);
	//button->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
	connect(edit, SIGNAL(buttonClicked()), this, SLOT(buttonClicked()));
	widgetLayout->insertWidget(0, edit);

	edit->lineEdit->setToolTip(configElement.help());
	QString s = value(configElement.defaultValue()).toString();
	edit->setText(s);
}

void DirConfigWidget::save()
{
	qfTrash() << QF_FUNC_NAME;
	setValue(edit->text());
	ConfigWidget::save();
}

QString& DirConfigWidget::lastDir()
{
	static QString s;
	return s;
}

void DirConfigWidget::buttonClicked()
{
	QFString s = QFileDialog::getExistingDirectory( this, tr("Vyberte adresar"), lastDir(), QFileDialog::DontResolveSymlinks);
	if(!!s) {
		lastDir() = s;
		edit->setText(s);
	}
}

void DirConfigWidget::setDefaultValue()
{
	edit->setText(configElement.defaultValue().toString());
}

void DirConfigWidget::setReadOnly(bool ro)
{
	edit->setReadOnly(ro);
}

bool DirConfigWidget::isReadOnly() const
{
	return edit->isReadOnly();
}

//=====================================
//                             FileConfigWidget
//=====================================
FileConfigWidget::FileConfigWidget(QFXmlConfig *doc, const QFXmlConfigElement &_el, QWidget *parent)
	: DirConfigWidget(doc, _el, parent)
{
}

void FileConfigWidget::buttonClicked()
{
	QString mode = configElement.options().cd("access", !Qf::ThrowExc).attribute("mode", "w");
	QFString s;
	if(mode == "r") {
		s = QFileDialog::getOpenFileName( this, tr("Vyberte soubor"), lastDir(), configElement.option("filter", tr("Vsechny soubory (*)")));
	}
	else {
		s = QFileDialog::getSaveFileName( this, tr("Vyberte soubor"), lastDir(), configElement.option("filter", tr("Vsechny soubory (*)")));
	}
	if(!!s) {
		lastDir() = QFFileUtils::path(s);
		edit->setText(s);
	}
}


