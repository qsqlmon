INCLUDEPATH += $$PWD

HEADERS +=    \
	$$PWD/qfxmlconfigwidget.h  \
    $$PWD/xmlconfigwidget_p.h \
    $$PWD/configframe.h \

SOURCES +=    \
	$$PWD/qfxmlconfigwidget.cpp  \
    $$PWD/xmlconfigwidget_p.cpp \
    $$PWD/configframe.cpp \

FORMS +=    \
	$$PWD/qfxmlconfigwidget.ui  \

RESOURCES +=                      \
	$$PWD/qfxmlconfigwidget.qrc
