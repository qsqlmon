//
// C++ Interface: dlgxmlconfig_p
//
// Description: 
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include <qfxmltreemodel.h>

#ifndef XMLCONFIGWIDGET_P_H
#define XMLCONFIGWIDGET_P_H

/// missing doc.
class ConfigTreeModel : public QFXmlTreeModel
{
	Q_OBJECT;
	protected:
		virtual bool isNodeAccepted(const QDomNode& nd) const;
	public:
		int columnCount(const QModelIndex & parent_nd = QModelIndex()) const;
		QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
		QVariant headerData(int section, Qt::Orientation o, int role = Qt::DisplayRole) const;
	public:
		ConfigTreeModel(QObject *parent = NULL) : QFXmlTreeModel(parent) {}
		virtual ~ConfigTreeModel() {}
};

#endif // XMLCONFIGWIDGET_P_H
