#include <QtGui>

#include <qfxmlconfigdocument.h>

#include "xmlconfigwidget_p.h"

#include <qflogcust.h>

//=======================================
//                                    ConfigTreeModel
//=======================================
bool ConfigTreeModel::isNodeAccepted(const QDomNode& nd) const
{
	//return true;
	//bool ret = false;
	if(!nd.isElement()) return false;
	QFDomElement el = nd.toElement();
	//qfTrash() << QF_FUNC_NAME << "name:" << el.tagName();
	if(QFXmlConfigElement::isServiceElement(el)) return false;
	//qfTrash() << "\tnot service";
	QFXmlConfigElement cel = el;
	if(cel.attribute("hidden").toBool()) return false;
	//qfTrash() << "\tnot hidden";
	if(cel.isLeaf()) return false;
	//qfTrash() << "\tnot leaf BINGO";
	//qfTrash() << "\treturn:" << ret;
	return true;
}

int ConfigTreeModel::columnCount(const QModelIndex & parent_nd) const
{
	Q_UNUSED(parent_nd);
	return 1;
}


QVariant ConfigTreeModel::data(const QModelIndex& ix, int role) const
{
	//qfTrash() << QF_FUNC_NAME;
	if(ix.column() != 0) return QFXmlTreeModel::data(ix, role);
	QVariant ret;
	if(role == Qt::DisplayRole) {
		QDomNode nd = index2node(ix);
		QFXmlConfigElement el = QFDomElement(nd.toElement());
		if(el) {
			if(el.parentNode().toElement().isNull()) {
				ret = el.attribute("caption", tr("konfigurace")); /// root configu
			}
			else ret = el.caption();
		}
	}
	//qfTrash() << "\treturn:" << ret.toString();
	return ret;
}

QVariant ConfigTreeModel::headerData(int section, Qt::Orientation o, int role) const
{
	QVariant ret;
	if (o == Qt::Horizontal) {
		if(section == 1) return "";
	}
	else ret = QAbstractItemModel::headerData(section, o, role);
	return ret;
}

