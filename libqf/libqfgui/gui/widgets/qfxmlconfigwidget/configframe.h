//
// C++ Interface: configframe
//
// Description: 
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CONFIGFRAME_H
#define CONFIGFRAME_H

#include <qfguiglobal.h>
#include <qfxmlconfig.h>

#include <QScrollArea>
#include <QComboBox>

//class QLineEdit;
class QFEditWithButton;
class ConfigWidget;
class QGroupBox;
class QSpinBox;
class QDoubleSpinBox;
class QCheckBox;
class QPushButton;
class QHBoxLayout;

/**
@author Fanda Vacek
 */
class QFGUI_DECL_EXPORT ConfigFrame : public QScrollArea
{
	Q_OBJECT
	protected:
		QGroupBox *f_frame;
		QFXmlConfig *f_config;
		QFXmlConfigElement f_element;
		bool dirty;
	protected:
		ConfigWidget* createConfigWidget(const QFXmlConfigElement &el);
	public:
		bool addConfigElement(const QFXmlConfigElement &el);
		void addSpacer();

		//bool isDataDirty() const {return f_dataDirty;}
		//void setDataDirty(bool b) {f_dataDirty = b;}
		void save();

		QFXmlConfigElement configElement() {return f_element;}

		static QString needRestartMsg();
	protected slots:
		void groupBoxCheckToggled(bool on);
	signals:
		void valueChanged(const QString &path, const QVariant &value);
	public:
		ConfigFrame(QFXmlConfig *doc, const QFXmlConfigElement &el, QWidget *parent = NULL);
		virtual ~ConfigFrame();
};

class ConfigWidget : public QWidget
{
	Q_OBJECT
	protected:
		QFXmlConfig *config;
		QFXmlConfigElement configElement;
		//QWidget *fEditor;
		QPushButton *btDefault;
		bool dirty;
	protected:
		QHBoxLayout *widgetLayout;
	protected:
		//bool setElementDataDirty(bool b);
		virtual QVariant value(const QVariant &default_val);
		virtual void setValue(const QVariant &val);
	public:
		//! return true if label is not part of widget (QTextEdit).
		//! For example QCheckBox does not need label.
		virtual bool needLabel() {return true;}
		//! Tells where is the widget label. It can be above widget (Qt::Vertical)
		//! or on the left side (Qt::Horizontal).
		virtual Qt::Orientation labelOrientation();
		
		//virtual QWidget* editor() = 0;
		virtual void setReadOnly(bool ro = true) = 0;
		virtual bool isReadOnly() const = 0;

		bool isDefaultButtonVisible() const;
		void setDefaultButtonVisible(bool b = true);
	public slots:
		virtual void save();
		virtual void setDefaultValue() {}
	signals:
		void valueChanged(const QString &path, const QVariant &value);
	public:
		ConfigWidget(QFXmlConfig *doc, const QFXmlConfigElement &el, QWidget *parent);
		virtual ~ConfigWidget();
};

class TextConfigWidget : public ConfigWidget
{
	Q_OBJECT;
	protected:
		bool crypted;
		QLineEdit *lineEdit;
	protected slots:
		void lineEditEditingFinished();
	public:
		//virtual QWidget* editor() {return lineEdit;}
		virtual void save();
		virtual void setDefaultValue();
		
		virtual void setReadOnly(bool ro = true);
		virtual bool isReadOnly() const;
	public:
		TextConfigWidget(QFXmlConfig *doc, const QFXmlConfigElement &el, QWidget *parent);
		virtual ~TextConfigWidget() {}
};

class IntConfigWidget : public ConfigWidget
{
	Q_OBJECT
	protected:
		QSpinBox *spinBox;
	protected slots:
		void spinBoxValueChanged();
	public:
		//virtual QWidget* editor() {return spinBox;}
		virtual void save();
		virtual void setDefaultValue();
		
		virtual void setReadOnly(bool ro = true);
		virtual bool isReadOnly() const;
	public:
		IntConfigWidget(QFXmlConfig *doc, const QFXmlConfigElement &el, QWidget *parent);
		virtual ~IntConfigWidget() {}
};

class FloatConfigWidget : public ConfigWidget
{
	Q_OBJECT
	protected:
		QDoubleSpinBox *spinBox;
	protected slots:
		void spinBoxValueChanged();
	public:
		//virtual QWidget* editor() {return spinBox;}
		virtual void save();
		virtual void setDefaultValue();
		
		virtual void setReadOnly(bool ro = true);
		virtual bool isReadOnly() const;
	public:
		FloatConfigWidget(QFXmlConfig *doc, const QFXmlConfigElement &el, QWidget *parent);
		virtual ~FloatConfigWidget() {}
};

class BoolConfigWidget : public ConfigWidget
{
	Q_OBJECT
	protected:
		QCheckBox *checkBox;
	protected slots:
		void checkBoxStateChanged(int val);
	protected:
		virtual void setValue(const QVariant &val);
		void load();
	public:
		virtual bool needLabel() {return false;}
		virtual void save();
		virtual void setDefaultValue();
		
		virtual void setReadOnly(bool ro = true);
		virtual bool isReadOnly() const;
	public:
		BoolConfigWidget(QFXmlConfig *doc, const QFXmlConfigElement &el, QWidget *parent);
		virtual ~BoolConfigWidget() {}
};

class ListConfigWidget : public ConfigWidget
{
	Q_OBJECT
	protected:
		class ComboBox;
	protected:
		ComboBox *comboBox;
	protected slots:
		void listItemActivated(int ix);
	public:
		virtual void save();
		virtual void setDefaultValue();
		 
		virtual void setReadOnly(bool ro = true);
		virtual bool isReadOnly() const;
	public:
		ListConfigWidget(QFXmlConfig *doc, const QFXmlConfigElement &el, QWidget *parent);
		virtual ~ListConfigWidget() {}
};

class ListConfigWidget::ComboBox : public QComboBox
{
	Q_OBJECT;
	QF_FIELD_RW(bool, is, set, ReadOnly);
	public:
		virtual void showPopup();
	public:
		ComboBox(QWidget *parent = NULL);
};

class DirConfigWidget : public ConfigWidget
{
	Q_OBJECT;
	protected:
		QFEditWithButton *edit;
	protected slots:
		virtual void buttonClicked();
		QString& lastDir();
	public:
		virtual void save();
		virtual void setDefaultValue();
		virtual void setReadOnly(bool ro = true);
		virtual bool isReadOnly() const;
	public:
		DirConfigWidget(QFXmlConfig *doc, const QFXmlConfigElement &el, QWidget *parent);
		virtual ~DirConfigWidget() {}
};

class FileConfigWidget : public DirConfigWidget
{
	Q_OBJECT;
	protected:
		virtual void buttonClicked();
	public:
		FileConfigWidget(QFXmlConfig *doc, const QFXmlConfigElement &el, QWidget *parent);
		virtual ~FileConfigWidget() {}
};

#endif
