
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "ui_qfxmlconfigwidget.h"
#include "qfxmlconfigwidget.h"
#include "xmlconfigwidget_p.h"
#include "configframe.h"

#include <qftoolbar.h>
#include <qfaction.h>

#include <typeinfo>

#include <qflogcust.h>

QFXmlConfigWidget::QFXmlConfigWidget(QWidget *parent)
	: QFDialogWidget(parent)
{
	//supressSaveCurrentlyEdited = false;
	f_config = NULL;
	fRootPath = QString();
	setReadOnly(false);
	setDefaultButtonsVisible(true);
	
	model = new ConfigTreeModel(this);

	ui = new Ui::QFXmlConfigWidget;
	ui->setupUi(centralWidget());
	Qf::connectSlotsByName(centralWidget(), this);
	
	uiBuilder.setCreatedActionsParent(this);
	uiBuilder.open(":/libqfgui/qfxmlconfigwidget.ui.xml");
	uiBuilder.connectActions(this);
	f_actions += uiBuilder.actions();
	
	//QList<int> sizes;
	//sizes.append(100);
	//sizes.append(100);
	//ui->splitter->setSizes(sizes);

	ui->tree->setModel(model);
	connect(ui->tree, SIGNAL(selected(const QModelIndex&)), this, SLOT(configureElement(const QModelIndex&)));
	ui->tree->header()->hide();
	
	ui->tree->addAction(uiBuilder.action("expandTree"));
	ui->tree->addAction(uiBuilder.action("colapseTree"));
	ui->tree->setContextMenuPolicy(Qt::ActionsContextMenu);

	QFPart::ToolBarList tblst = createToolBars();
	toolBar = tblst[0];
	QBoxLayout *ly = qobject_cast<QBoxLayout*>(layout());
	QF_ASSERT(ly, "bad layout");
	ly->insertWidget(0, toolBar);
}

QFXmlConfigWidget::~QFXmlConfigWidget()
{
	qfTrash() << QF_FUNC_NAME;
	delete ui;
	/// tady ne, xmlconfig widget nema ownership dokumentu
}
				
void QFXmlConfigWidget::setRootPath(const QString &s)
{
	qfTrash() << QF_FUNC_NAME << s;
	fRootPath = s;
	//if(fRootPath.isEmpty()) fRootPath = "/";
}

void QFXmlConfigWidget::setConfig(QFXmlConfig *doc)
{
	qfLogFuncFrame();// << data.nodeName();
	//qfInfo() << typeid(*doc).name();// << dynamic_cast<QFDbXmlConfig*>(doc);
	deleteCurrentConfigWidgets();
	f_config = doc;
	//setRootPath(rootPath());
	resetTree();
}

void QFXmlConfigWidget::resetTree()
{
	qfLogFuncFrame() << "root:" << rootPath() << "f_config:" << f_config;
	if(f_config) {
		qfTrash() << "\ttemplateDocument:" << f_config->templateDocument().toString();
		//qfTrash() << "\tcontent is null:" << fContent->isNull();
		//qfTrash() << "\tcontent document element is null:" << fContent->documentElement().isNull();
		QDomNode nd;
		if(rootPath().isEmpty()) nd = f_config->templateDocument().documentElement();
		else  nd = f_config->templateDocument().cd(rootPath(), !Qf::ThrowExc);
		qfTrash() << "\t root element name" << nd.toElement().tagName();
		model->setContent(nd);
		qfTrash() << "\t model row count:" << model->rowCount(QModelIndex());
		QAbstractItemModel *m = ui->tree->model();
		qfTrash() << "\t m:" << m;
		if(m) {
			//ui->tree->setExpanded(m->index(0, 0), true);
			ui->tree->setExpandedRecursively(QModelIndex(), true);
		} 
		qfTrash() << "\t dataDocument:" << f_config->dataDocument().toString();
		ui->tree->setFocus();
	}
	else {
		model->setContent(QFDomElement());
	}
	if(model->rowCount(QModelIndex()) == 0) {
		/// kdyz nejsou vetve, neni treba zobrazovat treeview
		configureElement(QModelIndex());
		ui->tree->setVisible(false);
	}
	else {
		ui->tree->setVisible(true);
		ui->tree->setCurrentIndex(model->index(0, 0, QModelIndex()));
		//configureElement(model->index(0, 0, QModelIndex()));
	}
}
			
void QFXmlConfigWidget::saveCurrentlyEdited()
{
	qfLogFuncFrame();
	/// uloz zmeny
	QList<ConfigWidget*> config_widgets = findChildren<ConfigWidget*>();
	foreach(ConfigWidget *cw, config_widgets) cw->save();
	ConfigFrame* config_frame = findChild<ConfigFrame*>();
	if(config_frame) config_frame->save();
}

void QFXmlConfigWidget::deleteCurrentConfigWidgets()
{
	ConfigFrame *frm = findChild<ConfigFrame*>("configFrame");
	if(frm) {
		//qfInfo() << "deleteing config frame:" << frm->configElement().path();
		delete frm;
	}
}

void QFXmlConfigWidget::configureElement(const QModelIndex &ix)
{
	qfLogFuncFrame();
	saveCurrentlyEdited();

	QFDomElement el = model->index2node(ix).toElement();
	if(el.isNull()) el = model->root().toElement();
	qfTrash().color(QFLog::Yellow) << "\tel:" << el.tagName();

	deleteCurrentConfigWidgets();
	qfTrash() << "\t configuring element:" << el.path();
	ConfigFrame *frm = new ConfigFrame(f_config, el, ui->frame);
	frm->setObjectName("configFrame");
	connect(frm, SIGNAL(valueChanged(QString,QVariant)), this, SIGNAL(valueChanged(QString,QVariant)));
	QHBoxLayout *ly = qobject_cast<QHBoxLayout*>(ui->frame->layout());
	if(!ly) ly = new QHBoxLayout(ui->frame);
	ly->setMargin(5);
	ly->addWidget(frm);
	QDomNodeList nl = el.childNodes();
	bool has_config_widgets = false;
	for(int i=0; i<nl.count(); i++) {
		QFXmlConfigElement el1 = QFDomElement(nl.item(i).toElement());
		if(QFXmlConfigElement::isServiceElement(el1)) continue;
		if(el1.attribute("hidden", 0).toBool()) continue;
		if(frm->addConfigElement(el1)) has_config_widgets = true;
	}
	frm->addSpacer();
	frm->setVisible(has_config_widgets);
	#if 0
	if(el.isNull()) {
		deleteCurrentConfigWidgets();
	}
	else {
		qfTrash() << "\t configuring element:" << el.path();
		ConfigFrame *frm = findChild<ConfigFrame*>("configFrame");
		if(frm) {
			qfTrash() << "\t current frame config element:" << frm->configElement().path();
			if(frm->configElement() != el) {
				qfTrash() << "\t NOT EQUAL";
				deleteCurrentConfigWidgets();
				frm = NULL;
			}
		}
		if(!frm) {
			frm = new ConfigFrame(f_config, el, ui->frame);
			frm->setObjectName("configFrame");
			connect(frm, SIGNAL(valueChanged(QString,QVariant)), this, SIGNAL(valueChanged(QString,QVariant)));
			QHBoxLayout *ly = qobject_cast<QHBoxLayout*>(ui->frame->layout());
			if(!ly) ly = new QHBoxLayout(ui->frame);
			ly->setMargin(5);
			ly->addWidget(frm);
			QDomNodeList nl = el.childNodes();
			bool has_config_widgets = false;
			for(int i=0; i<nl.count(); i++) {
				QFXmlConfigElement el1 = QFDomElement(nl.item(i).toElement());
				if(QFXmlConfigElement::isServiceElement(el1)) continue;
				if(el1.attribute("hidden", 0).toBool()) continue;
				if(frm->addConfigElement(el1)) has_config_widgets = true;
			}
			frm->addSpacer();
			frm->setVisible(has_config_widgets);
		}
	}
	#endif
}

void QFXmlConfigWidget::expandTree()
{
// 	QFString fn  = QFileDialog::getOpenFileName(this, tr("Otevrit soubor ..."), QString(), "*.xml");
// 	if (!!fn) {
// 		QFXmlConfigDocument xdoc;
// 		QFile f(fn);
// 		xdoc.setContent(f);
// 		setContent(xdoc);
// 	}

	ui->tree->setExpandedRecursively(ui->tree->currentIndex());
}
		
void QFXmlConfigWidget::colapseTree()
{
	ui->tree->setExpandedRecursively(ui->tree->currentIndex(), false);
}
		
void QFXmlConfigWidget::closeEvent(QCloseEvent *event)
{
	Q_UNUSED(event);
	/// save currently edited node, if any
	saveCurrentlyEdited(); 
}
		
bool QFXmlConfigWidget::isToolBarVisible() const
{
	if(toolBar) return toolBar->isVisible();
	return false;
}
		
void QFXmlConfigWidget::setToolBarVisible(bool b)
{
	if(toolBar) toolBar->setVisible(b);
}
		
QFPart::ToolBarList QFXmlConfigWidget::createToolBars()
{
	qfTrash() << QF_FUNC_NAME;
	QFPart::ToolBarList tblst = uiBuilder.createToolBars();
	qfTrash() << "\ttblst cnt:" << tblst.count();
	return tblst;
}

void QFXmlConfigWidget::defaults()
{
	configureElement(QModelIndex()); /// zlikviduj aktualni config widgety
	if(f_config) f_config->restoreDefaultData();
	resetTree();
}

