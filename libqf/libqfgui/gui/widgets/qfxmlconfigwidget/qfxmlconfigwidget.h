
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFXMLCONFIGWIDGET_H 
#define QFXMLCONFIGWIDGET_H

#include <qfguiglobal.h>
#include <qfexception.h>
#include <qfclassfield.h>
#include <qfxmlconfig.h>
#include <qfdialogwidget.h>
#include <qfuibuilder.h>
#include <qfpart.h>

#include <QModelIndex>
//#include <QWidget>

namespace Ui {class QFXmlConfigWidget;};
class ConfigTreeModel;
class QFToolBar;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFXmlConfigWidget : public QFDialogWidget
{
	Q_OBJECT
	Q_PROPERTY(bool toolBarVisible READ isToolBarVisible WRITE setToolBarVisible);
	QF_FIELD_RW(bool, is, set, ReadOnly);
	Q_PROPERTY(bool defaultButtonsVisible READ isDefaultButtonsVisible WRITE setDefaultButtonsVisible);
	QF_FIELD_RW(bool, is, set, DefaultButtonsVisible);
	private:
		QFUiBuilder uiBuilder;
	protected:
		QString fRootPath;
		QFToolBar *toolBar;
		// v resetTree() musim nejak vypnout funkci saveCurrentlyEdited, protoze mi to jinak do novyho dokumentu nahraje v resetTree() data ze staryho
		//bool supressSaveCurrentlyEdited;
	protected:
		QFXmlConfig *f_config;
		ConfigTreeModel *model;
	private:
		Ui::QFXmlConfigWidget *ui;
	protected:
		virtual void closeEvent(QCloseEvent *event);
		
		virtual QFPart::ToolBarList createToolBars();

	public:
		virtual QFXmlConfig* config() const throw(QFException)
		{
			if(f_config == NULL) QF_EXCEPTION("content is NULL");
			return f_config;
		}
		//! Function does not take ownership of document.
		void setConfig(QFXmlConfig *doc);
		// puvodni data nejprve naklonuje, aby je nezmenil.
		//void setCloneContent(const QFXmlConfigDocument &data);
		//const QFXmlConfigDocument& content() {return fContent;}
		/// editace neexistujiciho indexu zpusobi zavreni soucasneho editoru a tim ulozeni jeho dat.
		void saveCurrentlyEdited();

		/// pokud chci zobrazit jen cast konfigurace, toto je cesta k ni.
		virtual void setRootPath(const QString &s);
		QString rootPath() const {return fRootPath;}
		
		bool isToolBarVisible() const;
		void setToolBarVisible(bool b = true);
	protected:
		void deleteCurrentConfigWidgets();
	protected slots:
		void configureElement(const QModelIndex &ix);
		
		void expandTree();
		void colapseTree();
		/// nahraje do konfigu defaultni hodnoty, cili vymaze data
		void defaults();
	public slots:
		//! naplni tree a vyselektuje prvni item.
		void resetTree();
		void resetTree(const QString &new_root_path) {
			setRootPath(new_root_path);
			resetTree();
		}
	signals:
		void valueChanged(const QString &path, const QVariant &val);
	public:
		QFXmlConfigWidget(QWidget *parent = NULL);
		virtual ~QFXmlConfigWidget();
};
   
#endif // QFXMLCONFIGWIDGET_H

