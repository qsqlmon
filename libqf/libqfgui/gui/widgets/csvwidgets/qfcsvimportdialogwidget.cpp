
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#include "ui_qfcsvimportdialogwidget.h"
#include "qfcsvimportdialogwidget.h"

#include <qftablemodel.h>
#include <qfapplication.h>
#include <qfheaderview.h>

#include <QTextCodec>

#include <qflogcust.h>
//===============================================
//                                 TableModel
//===============================================
class TableModel : public QFTableModel
{
	Q_OBJECT;
	protected:
		QFCSVImportDialogWidget* importerWidget() {return qobject_cast<QFCSVImportDialogWidget*>(QObject::parent());}
		const QFCSVImportDialogWidget* importerWidget() const {return qobject_cast<const QFCSVImportDialogWidget*>(QObject::parent());}
	public:
		virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
		virtual void setTable(QFBasicTable *t);
	public:
		TableModel(QObject *parent = NULL);
};

TableModel::TableModel(QObject *parent)
	: QFTableModel(parent)
{
}

void TableModel::setTable(QFBasicTable * t)
{
	QFTableModel::setTable(t);
	clearColumns();
	reset();
}

QVariant TableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant ret;
	if(orientation == Qt::Horizontal) {
		if(role == Qt::DisplayRole) {
			ret = QFTableModel::headerData(section, orientation, role);
			QString colname;
			foreach(const QFCSVImportDialogWidget::ColumnMapping mp, importerWidget()->columnMapping()) {
				if(mp.columnIndex() == section) {
					colname = '[' + mp.columnCaption() + ']';
					break;
				}
			}
			ret = QString("%1\n%2").arg(ret.toString()).arg(colname);
		}
		else {
			ret = QFTableModel::headerData(section, orientation, role);
		}
	}
	else {
		ret = QFTableModel::headerData(section, orientation, role);
	}
	return ret;
}

//===============================================
//                                 QFCSVImportDialogWidget
//===============================================
QFCSVImportDialogWidget::QFCSVImportDialogWidget(QWidget *parent)
	: QFDialogWidget(parent)
{
	ui = new Ui::QFCSVImportDialogWidget;
	ui->setupUi(centralWidget());
	Qf::connectSlotsByName(centralWidget(), this);

	ui->frmExtraOptions->hide();

	f_tableModel = new TableModel(this);
	ui->tblPreview->setModel(f_tableModel);

	foreach(QByteArray ba, QTextCodec::availableCodecs()) ui->lstCodec->addItem(ba);
}

QFCSVImportDialogWidget::~QFCSVImportDialogWidget()
{
	delete ui;
}

QFBasicTable * QFCSVImportDialogWidget::table()
{
	return f_tableModel->table();
}

void QFCSVImportDialogWidget::on_btReload_clicked()
{
	QFString fn = ui->edFileName->text().trimmed();
	if(!!fn) {
		QFile f(fn);
		if(f.open(QFile::ReadOnly)) {
			QTextStream ts(&f);
			ts.setCodec(ui->lstCodec->currentText().toAscii().constData());
			QFBasicTable *t = new QFBasicTable();
			QFBasicTable::TextImportOptions opts;
			{
				if(ui->btSeparatorTab->isChecked()) {
					opts.setFieldSeparator('\t');
				}
				else {
					QString s = ui->edSepartorOther->text().trimmed();
					if(s.isEmpty()) opts.setFieldSeparator(0);
					else opts.setFieldSeparator(s[0].toLatin1());
				}
			}
			{
				QString s = ui->edQuotes->text().trimmed();
				if(s.isEmpty()) opts.setFieldQuotes(0);
				else opts.setFieldQuotes(s[0].toLatin1());
			}
			opts.setIgnoreFirstLinesCount(ui->edIgnoreFirstLines->value());
			opts.setImportColumnNames(ui->chkHeader->isChecked());
			t->importCSV(ts, opts);
			f_tableModel->setTable(t);
		}
	}
}

void QFCSVImportDialogWidget::on_btFileName_clicked()
{
	QString fn = qfApp()->getOpenFileName(this, "Importovat ze souboru", QString(), "*.csv");
	ui->edFileName->setText(fn);
}

void QFCSVImportDialogWidget::on_tblPreview_clicked(const QModelIndex & ix)
{
	qfLogFuncFrame();
	int col = ix.column();
	QFTableModel::ColumnDefinition cd = f_tableModel->column(col, !Qf::ThrowExc);
	ui->lblMapping->setText(cd.caption());
	QString col_name;
	foreach(const ColumnMapping &cm, columnMapping()) {
		if(cm.columnIndex() == col) {
			col_name = cm.columnName();
			break;
		}
	}
	QComboBox *box = ui->lstMapping;
	box->setCurrentIndex(0);
	if(!col_name.isEmpty()) {
		for(int i=0; i<box->count(); i++) {
			if(box->itemData(i).toString() == col_name) {
				box->setCurrentIndex(i);
			}
		}
	}
}

void QFCSVImportDialogWidget::setColumnMapping(const ColumnMappingList & lst)
{
	f_columnMappingList = lst;
	QComboBox *box = ui->lstMapping;
	box->clear();
	box->addItem("----");
	foreach(const ColumnMapping &cm, columnMapping()) {
		box->addItem(cm.columnCaption(), cm.columnName());
	}
}

void QFCSVImportDialogWidget::on_lstMapping_activated(int ix)
{
	int curr_column = ui->tblPreview->currentIndex().column();
	if(curr_column >= 0) {
		QString col_name = ui->lstMapping->itemData(ix).toString();
		for(int i=0; i<f_columnMappingList.count(); i++) {
			ColumnMapping &cm = f_columnMappingList[i];
			if(cm.columnName() == col_name) {
				cm.setColumnIndex(curr_column);
			}
			else if(cm.columnIndex() == curr_column) {
				/// pripadne predchozi mapovani tohoto sloupce vymaz
				cm.setColumnIndex(-1);
			}
		}
	}
	ui->tblPreview->horizontalHeader()->headerDataChanged(Qt::Horizontal, 0, ui->tblPreview->horizontalHeader()->count() - 1);
}

QFrame * QFCSVImportDialogWidget::extraOptionsFrame()
{
	return ui->frmExtraOptions;
}


#include "qfcsvimportdialogwidget.moc"
#include "moc_qfcsvimportdialogwidget.cpp"

