
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFCSVEXPORTDIALOGWIDGET_H
#define QFCSVEXPORTDIALOGWIDGET_H 


#include <qfbasictable.h>

#include <qfguiglobal.h>
#include <qfdialogwidget.h>

namespace Ui {class QFCSVExportDialogWidget;};

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFCSVExportDialogWidget : public QFDialogWidget
{
	Q_OBJECT;
	private:
		Ui::QFCSVExportDialogWidget *ui;
	protected slots:
		void on_btColumnsAll_clicked();
		void on_btColumnsNone_clicked();
		void on_btColumnsInvert_clicked();
	public:
		void setExportOptions(const QFBasicTable::TextExportOptions &opts);
		QFBasicTable::TextExportOptions exportOptions() const;
		QStringList columnNames() const;
	public:
		QFCSVExportDialogWidget(QFBasicTable *tbl = NULL, QWidget *parent = NULL);
		virtual ~QFCSVExportDialogWidget();
};

#endif // QFCSVEXPORTDIALOGWIDGET_H

