
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//
#include "ui_qfcsvexportdialogwidget.h"

#include "qfcsvexportdialogwidget.h"

#include <QTextCodec>

#include <qflogcust.h>

QFCSVExportDialogWidget::QFCSVExportDialogWidget(QFBasicTable *tbl, QWidget *parent)
	: QFDialogWidget(parent)//, table(tbl)
{
	ui = new Ui::QFCSVExportDialogWidget;
	ui->setupUi(centralWidget());
	Qf::connectSlotsByName(centralWidget(), this);
	{
		QComboBox *w = ui->lstCodec;
		QList<QByteArray> lst = QTextCodec::availableCodecs();
		/// vyndej system
		QByteArray system = lst.takeFirst();
		qSort(lst);
		lst.insert(0, system);
		foreach(QByteArray ba, lst) w->addItem(ba);
		int i = w->findText("UTF-8", Qt::MatchExactly);
		if(i > 0) w->setCurrentIndex(i);
	}
	ui->grpColumns->setVisible(tbl);
	if(tbl) {
		QListWidget *w = ui->lstColumns;
		foreach(const QFSqlField &fld, tbl->fields()) {
			w->addItem(fld.fullName());
		}
		on_btColumnsAll_clicked();
	}
}

QFCSVExportDialogWidget::~QFCSVExportDialogWidget()
{
}

QFBasicTable::TextExportOptions QFCSVExportDialogWidget::exportOptions() const
{
	QFBasicTable::TextExportOptions ret;
	if(ui->btSeparatorTab->isChecked()) ret.setFieldSeparator('\t');
	else {
		QFString fs = ui->edSeparatorOther->text();
		ret.setFieldSeparator(fs.value(0));
	}
	{
		QFString fs = ui->edQuoteChar->text();
		ret.setFieldQuotes(fs.value(0));
	}
	if(ui->btQuotesAlways->isChecked()) ret.setFieldQuotingPolicy(QFBasicTable::TextExportOptions::Always);
	else if(ui->btQuotesNever->isChecked()) ret.setFieldQuotingPolicy(QFBasicTable::TextExportOptions::Never);
	else ret.setFieldQuotingPolicy(QFBasicTable::TextExportOptions::IfNecessary);
	ret.setExportColumnNames(ui->chkHeader->isChecked());
	ret.setCodecName(ui->lstCodec->currentText());
	ret.setFromLine(ui->edFromLine->value());
	ret.setToLine(ui->edToLine->value());
	return ret;
}

void QFCSVExportDialogWidget::setExportOptions(const QFBasicTable::TextExportOptions & opts)
{
	ui->btSeparatorTab->setChecked(opts.fieldSeparator() == '\t');
	if(!ui->btSeparatorTab->isChecked()) {
		ui->edSeparatorOther->setText(opts.fieldSeparator());
	}
	{
		ui->edQuoteChar->setText(opts.fieldQuotes());
	}
	ui->btQuotesAlways->setChecked(opts.fieldQuotingPolicy() == QFBasicTable::TextExportOptions::Always);
	ui->btQuotesNever->setChecked(opts.fieldQuotingPolicy() == QFBasicTable::TextExportOptions::Never);
	ui->btQuotesIfNecessary->setChecked(opts.fieldQuotingPolicy() == QFBasicTable::TextExportOptions::IfNecessary);
	ui->chkHeader->setChecked(opts.isExportColumnNames());
	ui->lstCodec->setCurrentIndex(ui->lstCodec->findText(opts.codecName()));
	ui->edFromLine->setValue(opts.fromLine());
	ui->edToLine->setValue(opts.toLine());
}

void QFCSVExportDialogWidget::on_btColumnsAll_clicked()
{
	QListWidget *w = ui->lstColumns;
	for(int i=0; i<w->count(); i++) w->item(i)->setCheckState(Qt::Checked);
}

void QFCSVExportDialogWidget::on_btColumnsNone_clicked()
{
	QListWidget *w = ui->lstColumns;
	for(int i=0; i<w->count(); i++) w->item(i)->setCheckState(Qt::Unchecked);
}

void QFCSVExportDialogWidget::on_btColumnsInvert_clicked()
{
	QListWidget *w = ui->lstColumns;
	for(int i=0; i<w->count(); i++) {
		QListWidgetItem *it = w->item(i);
		Qt::CheckState s = it->checkState();
		s = (s == Qt::Checked)? Qt::Unchecked: Qt::Checked;
		it->setCheckState(s);
	}
}

QStringList QFCSVExportDialogWidget::columnNames() const
{
	QStringList ret;
	QListWidget *w = ui->lstColumns;
	for(int i=0; i<w->count(); i++) {
		QListWidgetItem *it = w->item(i);
		Qt::CheckState s = it->checkState();
		if(s == Qt::Checked) {
			ret << it->text();
		}
	}
	return ret;
}

