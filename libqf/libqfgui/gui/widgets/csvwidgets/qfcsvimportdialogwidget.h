
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFCSVIMPORTDIALOGWIDGET_H
#define QFCSVIMPORTDIALOGWIDGET_H

#include <qfguiglobal.h>
#include <qfdialogwidget.h>


namespace Ui {class QFCSVImportDialogWidget;};
class QFTableModel;
class QFBasicTable;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFCSVImportDialogWidget : public QFDialogWidget
{
	Q_OBJECT;
	private:
		Ui::QFCSVImportDialogWidget *ui;
	public:
		class QFGUI_DECL_EXPORT ColumnMapping
		{
			private:
				class Data : public QSharedData
				{
					public:
						QString columnName, columnCaption;
						int columnIndex;
					public:
						Data(const QString &col_name = QString(), const QString &col_caption = QString()) : columnName(col_name), columnCaption(col_caption), columnIndex(-1) {
							if(columnCaption.isEmpty()) columnCaption = columnName; 
						}
				};
				QSharedDataPointer<Data> d;
			public:
				QString columnName() const {return d->columnName;}
				QString columnCaption() const {return d->columnCaption;}
				int columnIndex() const {return d->columnIndex;}
				void setColumnIndex(int ix) {d->columnIndex = ix;}
			public:
				ColumnMapping(const QString &col_name = QString(), const QString &col_caption = QString()) {
					d = new Data(col_name, col_caption);
				}
		};
		typedef QList<ColumnMapping> ColumnMappingList;
	protected:
		ColumnMappingList f_columnMappingList;
		QFTableModel *f_tableModel;
	protected slots:
		void on_btReload_clicked();
		void on_btFileName_clicked();
		void on_tblPreview_clicked(const QModelIndex &ix);
		void on_lstMapping_activated(int ix);
	public:
		void setColumnMapping(const ColumnMappingList &lst);
		const ColumnMappingList& columnMapping() const {return f_columnMappingList;}

		QFrame* extraOptionsFrame();

		QFBasicTable* table();
	public:
		QFCSVImportDialogWidget(QWidget *parent = NULL);
		virtual ~QFCSVImportDialogWidget();
}; 

#endif // QFCSVIMPORTDIALOGWIDGET_H

