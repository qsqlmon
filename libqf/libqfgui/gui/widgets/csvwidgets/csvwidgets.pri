message("including module $$PWD")

FORMS +=     \
	$$PWD/qfcsvimportdialogwidget.ui   \
	$$PWD/qfcsvexportdialogwidget.ui   \

HEADERS +=    \
	$$PWD/qfcsvimportdialogwidget.h   \
	$$PWD/qfcsvexportdialogwidget.h  \

SOURCES +=      \
	$$PWD/qfcsvimportdialogwidget.cpp   \
	$$PWD/qfcsvexportdialogwidget.cpp  \


