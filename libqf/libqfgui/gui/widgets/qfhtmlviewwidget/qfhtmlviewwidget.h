/****************************************************************************
**
** Copyright (C) 2005-2005 Trolltech AS. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.trolltech.com/products/qt/opensource.html
**
** If you are unsure which license is appropriate for your use, please
** review the following information:
** http://www.trolltech.com/products/qt/licensing.html or contact the
** sales department at sales@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef QFHTMLVIEWWIDGET_H
#define QFHTMLVIEWWIDGET_H

#include <QWidget>

#include <qfpart.h>

namespace Ui {
class QFHtmlViewWidget;
}
class QFile;

class QFGUI_DECL_EXPORT QFHtmlViewWidget : public QWidget
{
    Q_OBJECT
	protected:
		QFPart::ActionList f_actions;
		QFPart::ActionList toolBarActions;
		//! original of html text previously set by calling setHtmlText().
		QString htmlTextOriginal;
		QString suggestedFileName;
	public:
		QFHtmlViewWidget(QWidget *parent = 0);
		~QFHtmlViewWidget();

		void setHtmlText(const QString &doc, const QString &suggested_file_name = QString());
		void setUrl(QFile &url);
	public slots:
		void save();
		void print();
	private:
		void createToolBar();
		void createActions();
		QFPart::ActionList& actionListRef() {return f_actions;}
		QFPart::ActionList actionList() const {
			return const_cast<QFHtmlViewWidget*>(this)->actionListRef();
		}
	private:
		Ui::QFHtmlViewWidget *ui;
};

#endif     // QFHTMLVIEWWIDGET_H
