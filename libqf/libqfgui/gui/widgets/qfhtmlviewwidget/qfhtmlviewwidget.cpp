#include "qfhtmlviewwidget.h"
#include "ui_qfhtmlviewwidget.h"

#include <qfapplication.h>
#include <qfhtmlutils.h>
#include <qfuibuilder.h>
#include <qfaction.h>
#include <qfmessage.h>
#include <qfpixmapcache.h>

#include <QtGui>
#include <QString>

#include <qflogcust.h>

QFHtmlViewWidget::QFHtmlViewWidget(QWidget *parent)
    : QWidget(parent)
{
	QFPixmapCache::insert("icon.libqf.previous", ":/libqfgui/images/left.png");
	QFPixmapCache::insert("icon.libqf.next", ":/libqfgui/images/right.png");
	QFPixmapCache::insert("icon.libqf.print", ":/libqfgui/images/printer.png");
	QFPixmapCache::insert("icon.libqf.reload", ":/libqfgui/images/reload.png");
	QFPixmapCache::insert("icon.libqf.home", ":/libqfgui/images/home.png");
	QFPixmapCache::insert("icon.libqf.save", ":/libqfgui/images/save.png");

	ui = new Ui::QFHtmlViewWidget;
	ui->setupUi(this);

	createActions();
	createToolBar();
	QFUiBuilder::connectActions(actionList(), this);
}

QFHtmlViewWidget::~QFHtmlViewWidget()
{
	//qfTrash() << QF_FUNC_NAME;
	SAFE_DELETE(ui);
}

void QFHtmlViewWidget::setHtmlText(const QString &doc, const QString &suggested_file_name)
{
	htmlTextOriginal = doc;
	suggestedFileName = suggested_file_name;
	ui->browser->setHtml(doc);
}

void QFHtmlViewWidget::createActions()
{
	QFPart::ActionList &actlst = actionListRef();
	QFAction *a;
	QString s;

	s = "previous";
	a = new QFAction(QFPixmapCache::icon("icon.libqf." + s), tr("Zpet"), this);
	a->setId(s);
	a->setToolTip(tr("Zpet"));
	actlst << a;
	toolBarActions.append(a);

	s = "next";
	a = new QFAction(QFPixmapCache::icon("icon.libqf." + s), tr("Dopredu"), this);
	a->setId(s);
	a->setToolTip(tr("Dopredu"));
	actlst << a;
	toolBarActions.append(a);

	s = "home";
	a = new QFAction(QFPixmapCache::icon("icon.libqf." + s), tr("Na zacatek"), this);
	a->setId(s);
	a->setToolTip(tr("Na zacatek"));
	actlst << a;
	toolBarActions.append(a);

	s = "reload";
	a = new QFAction(QFPixmapCache::icon("icon.libqf." + s), tr("Obnovit"), this);
	a->setId(s);
	a->setToolTip(tr("Obnovit"));
	actlst << a;
	toolBarActions.append(a);

	a = new QFAction(this);
	a->setSeparator(true);
	toolBarActions.append(a);

	s = "print";
	a = new QFAction(QFPixmapCache::icon("icon.libqf." + s), tr("Tisk"), this);
	a->setId(s);
	a->setToolTip(tr("Tisk"));
	actlst << a;
	toolBarActions.append(a);

	s = "save";
	a = new QFAction(QFPixmapCache::icon("icon.libqf." + s), tr("Ulozit"), this);
	a->setId(s);
	a->setToolTip(tr("Ulozit dokument"));
	actlst << a;
	toolBarActions.append(a);
}

void QFHtmlViewWidget::createToolBar()
{
	QToolBar *tb = new QToolBar(NULL);
	tb->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	QBoxLayout *l = qobject_cast<QBoxLayout*>(layout());
	QF_ASSERT(l, "error");
	l->setMargin(0);
	l->insertWidget(0, tb);
	foreach(QFAction *a, toolBarActions) tb->addAction(a);
}

void QFHtmlViewWidget::save()
{
	QString fn = suggestedFileName;
	if(!fn.endsWith(".html")) fn += ".html";
	fn = qfApp()->getSaveFileName(this, tr("Ulozit jako ..."),
										fn, tr("soubory Html (*.html)"));
	if(fn.isEmpty()) return;
	if(fn.indexOf('.') < 0) fn += ".html";
	qfTrash() << "saving to file" << fn;
	QFHtmlUtils::saveHtmlWithResources(htmlTextOriginal, QString(), fn);
	/*
	QFile f(fn);
	if(!f.open(QIODevice::WriteOnly)) {
		QString s = tr("file_export_HTML", "file_export_HTML: ERROR open file");
		throw QFException(s);
	}
	f.write(htmlTextOriginal.toUtf8());
	*/
	//f.write(ui->browser->toHtml().toUtf8());
}

void QFHtmlViewWidget::print()
{
	QPrinter printer(QPrinter::HighResolution);
	printer.setFullPage(true);

	QPrintDialog *dlg = new QPrintDialog(&printer, this);
	if (dlg->exec() == QDialog::Accepted) {
		ui->browser->document()->print(&printer);
	}
	delete dlg;
}

void QFHtmlViewWidget::setUrl(QFile &url)
{
	QFile &f = url;
	suggestedFileName = f.fileName();
	if(!f.open(QIODevice::ReadOnly)) {
		QFMessage::information(this, tr("Soubor %1 nelze otevrit pro cteni.").arg(f.fileName()));
		return;
	}
	QString s;
	QByteArray ba = f.readAll();
	s = QString::fromUtf8(ba.data());
	ui->browser->setHtml(s);
}
