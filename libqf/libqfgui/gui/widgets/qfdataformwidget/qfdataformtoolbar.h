
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDATAFORMTOOLBAR_H
#define QFDATAFORMTOOLBAR_H

#include <qfguiglobal.h>

#include <qfwidgettoolbarbase.h> 

class QFDataFormWidget;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFDataFormToolBar : public QFWidgetToolBarBase
{
	Q_OBJECT;
	public slots:
		/// Kdyz QFTableView emituje signal connectRequest(), vytvori si buttony z akci TableView a odpoji se od signalu.
		virtual void connectDataFormWidget(QFDataFormWidget *dataform);
	public:
		QFDataFormToolBar(QWidget *parent = NULL);
		virtual ~QFDataFormToolBar();
};
   
#endif // QFDATAFORMTOOLBAR_H

