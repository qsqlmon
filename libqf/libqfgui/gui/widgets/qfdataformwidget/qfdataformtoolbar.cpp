
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdataformtoolbar.h"

#include <qfaction.h>
#include <qfdataformwidget.h>
#include <qflogcust.h>

QFDataFormToolBar::QFDataFormToolBar(QWidget *parent)
	: QFWidgetToolBarBase(parent)
{
}

QFDataFormToolBar::~QFDataFormToolBar()
{
}

void QFDataFormToolBar::connectDataFormWidget(QFDataFormWidget *dataform)
{
	if(dataform) {
		//qfInfo() << "addidng actions";
		//foreach(QFAction *a, dataform->toolBarActions()) qfInfo() << "******** action:" << a->id();
		addActions(dataform->toolBarActions());
		disconnect(dataform, 0, this, SLOT(connectDataFormWidget(QFDataFormWidget *)));
	}
}

