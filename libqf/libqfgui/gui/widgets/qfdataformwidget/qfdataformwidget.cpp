
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdataformwidget.h"

#include <qfdataformdocument.h>
#include <qfmessage.h>
#include <qftableview.h>
#include <qfaction.h>
#include <qfsqlcontrol.h>
#include <qfpixmapcache.h>

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

//=====================================================
//                                             QFDataFormWidget
//=====================================================
QString QFDataFormWidget::ReadOnlyWidgetBackgroundColor = "#eeeeff";
QString QFDataFormWidget::MandatoryWidgetBackgroundColor = "pink";

QFDataFormWidget::QFDataFormWidget(QWidget *parent)
	: QFrame(parent)
{
	setDocument(NULL);
	clearSqlWidgetsCache();
	f_mandatorySqlWidgets << NULL;
	createActions();
}

QFDataFormWidget::~QFDataFormWidget()
{
	qfTrash() << QF_FUNC_NAME;
	//if(!documentPtr.isNull()) documentPtr->releaseRowLock();
}

QFDataFormDocument* QFDataFormWidget::document(bool throw_exc) throw(QFException)
{
	if(!documentPtr.isNull()) return documentPtr;
	if(throw_exc) QF_EXCEPTION("Document is NULL.");
	return NULL;
}

void QFDataFormWidget::clearSqlWidgetsCache()
{
	f_sqlWidgets.clear();
	f_sqlWidgets << NULL;
}

static bool object_has_property(QObject *o, const char *prop_name)
{
	qfLogFuncFrame() << o->objectName() << prop_name;
	const QMetaObject *mo = o->metaObject();
	while(mo) {
		qfTrash() << "\t looking in:" << mo->className() << "index:" << mo->indexOfProperty(prop_name);
		if(mo->indexOfProperty(prop_name) >= 0) return true;
		mo = mo->superClass();
	}
	return false;
}

QList<QWidget*> QFDataFormWidget::sqlWidgets()
{
	if(f_sqlWidgets.count() == 1 && f_sqlWidgets[0] == NULL) {
		//qfLogFuncFrame();
		f_sqlWidgets.clear();
		QList<QWidget *> lst = findChildren<QWidget *>();
		foreach(QWidget *w, lst) {
			if(object_has_property(w, "sqlId")) {
				QFString s = w->property("sqlId").toString();
				//qfInfo() << "\t" << w->objectName() << ":" << s;
				if(!s) {
					bool ignore_sql_id = w->property("sqlIdNotUsed").toBool();
					if(ignore_sql_id) f_sqlWidgets << w;
					/*
					QStringList sl;
					QObject *o = w;
					while(o) {
						sl.prepend(QString("%1[%2]").arg(o->metaObject()->className()).arg(o->objectName()));
						o = o->parent();
					}
					QString s = "sql id not set: \n\t%1";
					s = s.arg(sl.join("\n\t"));
					qfWarning() << s;
					*/
				}
				else f_sqlWidgets << w;
			}
		}
	}
	return f_sqlWidgets;
}

QList< QWidget * > QFDataFormWidget::mandatorySqlWidgets()
{
	if(f_mandatorySqlWidgets.count() == 1 && f_mandatorySqlWidgets[0] == NULL) {
		//qfLogFuncFrame();
		f_mandatorySqlWidgets.clear();
		QList<QWidget *> lst = sqlWidgets();
		foreach(QWidget *w, lst) {
			if(object_has_property(w, "mandatory")) {
				bool b = w->property("mandatory").toBool();
				if(b) f_mandatorySqlWidgets << w;
			}
		}
	}
	return f_mandatorySqlWidgets;
}

void QFDataFormWidget::setDocument(QFDataFormDocument *doc)
{
	qfLogFuncFrame() << doc;
	//connect(doc, SIGNAL(fieldValuesChanged(const QFBasicTable::Row &)), this, SLOT(updateWidgets(const QFBasicTable::Row &)));
	if(doc) {
		if(doc != documentPtr) {
			if(documentPtr) {
				documentPtr->disconnect(this); /// odpoj widget od stareho dokumentu
				this->disconnect(documentPtr); /// odpoj stary dokument od widgetu
			}
			documentPtr = doc;
			//qfInfo() << "\t CONNECT: (this, SIGNAL(widgetValueUpdated(const QString &, const QVariant &)), doc, SLOT(setValue(const QString &, const QVariant &)))";
			QF_CONNECT(this, SIGNAL(widgetValueUpdated(const QString &, const QVariant &)), doc, SLOT(setValue(const QString &, const QVariant &)));

			//QF_CONNECT(doc, SIGNAL(valuesReloaded(QFDataFormDocument *)), this, SLOT(refreshActions()));
			QF_CONNECT(doc, SIGNAL(aboutToSave(QFDataFormDocument *)), this, SLOT(_documentAboutToSave()));
			QF_CONNECT(doc, SIGNAL(saved(QFDataFormDocument *, int)), this, SLOT(_documentSaved(QFDataFormDocument *, int)));
			QF_CONNECT(doc, SIGNAL(dropped(const QVariant &)), this, SLOT(_documentDropped(const QVariant &)));
			QF_CONNECT(doc, SIGNAL(refreshWidgetActions()), this, SLOT(refreshActions()));
			//QF_CONNECT(doc, SIGNAL(saved(QFDataFormDocument *, int)), this, SLOT(refreshActions()));
			//QF_CONNECT(doc, SIGNAL(dataDirtyChanged(bool)), this, SLOT(onDocumentDataDirtyChanged(bool)));

			/// tohle potrebuje connectAsExternalRowEditor() pro svou funkci
			qfTrash() << "\tconnecting master table signal/slots";
			QF_CONNECT(this, SIGNAL(_load(const QVariant &, QFDataFormDocument::Mode)), doc, SLOT(load(const QVariant &, QFDataFormDocument::Mode)));
			QF_CONNECT(this, SIGNAL(_save()), doc, SLOT(save()));
			QF_CONNECT(this, SIGNAL(_drop()), doc, SLOT(drop()));
			QF_CONNECT(this, SIGNAL(_reload()), doc, SLOT(reload()));

			connectWidgets();

			{
				/// setup external data source widgets
				bool external_data_ro = this-> property("externalDataReadOnly").toBool();
				qfTrash() << "\t isExternalDataReadOnly:" << external_data_ro;
				if(external_data_ro) {
					foreach(QWidget *w, sqlWidgets()) {
						if(w->property("externalData").toBool()) {
							qfTrash() << "\t setting" << w->objectName() << "data read only" << w->property("externalData").toString();
							w->setProperty("dataReadOnly", true);
						}
					}
				}
			}
		
			emit connectRequest(this);
			/// force widgets to set their content according to document
			if(!doc->isEmpty()) emit doc->valuesReloaded(doc);
		}
	}
	else {
		documentPtr = NULL;
	}
	refreshActions();
}

void QFDataFormWidget::connectWidgets()
{
	qfLogFuncFrame();
	foreach(QWidget *w, sqlWidgets()) {
		qfTrash() << "\t" << w->objectName() << w->property("sqlId").toString();
		QF_CONNECT(w, SIGNAL(valueUpdated(const QString &, const QVariant &)), this, SIGNAL(widgetValueUpdated(const QString &, const QVariant &)));
		//QF_CONNECT(this, SIGNAL(documentValuesReloaded(const QString&)), w, SLOT(loadValue(const QString&)));
		QF_CONNECT(document(), SIGNAL(valueSet(QString)), w, SLOT(loadValue(QString)));
		//QF_CONNECT(document(), SIGNAL(valuesReloaded(QFDataFormDocument*)), w, SLOT(loadValue()));
		//QF_CONNECT(document(), SIGNAL(valueEdited(const QString&, const QVariant &, const QVariant &)), w, SLOT(loadValue(const QString&)));
		// pokud ma widget slot documentAboutToSave zavolej ji,
		// napr QFSqlXmlConfigWidget neemituje signal valueUpdated() (dost slozite),
		// takze se musi emitovat touto cestou
		//if(w->metaObject()->indexOfSlot(QMetaObject::normalizedSignature("flushValue()")) >= 0) {
		//	QF_CONNECT(this, SIGNAL(documentAboutToSave()), w, SLOT(flushValue()));
		//}
	}
}
/*
void QFDataFormWidget::documentLockInfo(const QString &info)
{
	qfTrash() << QF_FUNC_NAME;
	if(!info.isEmpty()) {
		QString s = tr("Teto zaznam jiz edituje %1.\nBude proto zobrazen jen pro cteni.").arg(info);
		QFMessage::information(this, s);
	}
}
*/
/*
void QFDataFormWidget::sync()
{
	qfTrash() << QF_FUNC_NAME << "sender:" << sender();
	emit documentValuesReloaded(QString());
}
*/
void QFDataFormWidget::connectAsExternalRowEditor(QFTableView *view)
{
	qfTrash() << QF_FUNC_NAME;
	if(view) {
		qfTrash() << view;
		QF_CONNECT(view, SIGNAL(viewRowInExternalEditor(const QVariant &)), this, SLOT(_loadView(const QVariant &)));
		QF_CONNECT(view, SIGNAL(editRowInExternalEditor(const QVariant &)), this, SLOT(_loadEdit(const QVariant &)));
		QF_CONNECT(view, SIGNAL(insertRowInExternalEditor()), this, SLOT(_loadInsert()));

		connectExternalEditorEditResults(view);
		disconnect(view, 0, this, SLOT(connectAsExternalRowEditor(QFTableView *)));
	}
}

void QFDataFormWidget::connectAsRowBrowser(QFTableView *view)
{
	if(!view) return;
	view->setIgnoreFocusOutEvent(true);
	QF_CONNECT(view, SIGNAL(selected(const QModelIndex &)), this, SLOT(masterViewSelected(const QModelIndex &)));
	QF_CONNECT(view, SIGNAL(reloaded()), this, SLOT(reloadDocument()));
	//QF_CONNECT(view, SIGNAL(rowExternalyDropped(const QModelIndex &)), this, SLOT(masterViewSelected(const QModelIndex &)));
	//QF_CONNECT(view, SIGNAL(currentChanging(const QModelIndex &, const QModelIndex &)), this, SLOT(masterViewCurrentIndexChanging(const QModelIndex &, const QModelIndex &)));

	connectExternalEditorEditResults(view);
	disconnect(view, 0, this, SLOT(connectAsRowBrowser(QFTableView *)));
}

void QFDataFormWidget::connectExternalEditorEditResults(QFTableView * table_view)
{
	if(!table_view) return;
	QF_CONNECT(this, SIGNAL(documentSaved(QFDataFormDocument*, int)), table_view, SLOT(dataExternalySaved(QFDataFormDocument*, int)));
	QF_CONNECT(this, SIGNAL(documentDropped(const QVariant &)), table_view, SLOT(dataExternalyDropped(const QVariant &)));
}

void QFDataFormWidget::_documentAboutToSave()
{
	qfTrash() << QF_FUNC_NAME;
	/// nektere sql controly ulozi svoji hodnotu do dokumnetu az kdyz ztrati fokus
	this->setFocus();
	emit documentAboutToSave();
}

void QFDataFormWidget::_documentSaved(QFDataFormDocument *doc, int mode)
{
	qfTrash() << QF_FUNC_NAME;
	emit documentSaved(doc, mode);
}

void QFDataFormWidget::_documentDropped(const QVariant &id)
{
	//refreshActions();
	emit documentDropped(id);
}
/*
void QFDataFormWidget::onDocumentDataDirtyChanged(bool dirty)
{
	qfLogFuncFrame() << "dirty:" << dirty;
	toolBarAction("post")->setEnabled(dirty);
	//qfInfo() << "emit documentDataDirtyChanged" << dirty;
	emit documentDataDirtyChanged(dirty);
}
*/
void QFDataFormWidget::masterViewSelected(const QModelIndex &current)
{
	qfLogFuncFrame() << "current row:" << current.row();// << "document row:" << document()->currentModelRowIndex();
	if(!current.isValid()) {
		qfTrash() << "\t hiding widget because current index is not valid";
		hide();
		return;
	}
	QFTableDataFormDocument *tdoc = qobject_cast<QFTableDataFormDocument*>(document());
	if(tdoc) {
		if(current.row() == tdoc->currentModelRowIndex()) return;
		QFTableModel *m = tdoc->model();
		if(m->isEditRowsAllowed()) {
			if(tdoc->currentModelRowIndex() >= 0 && tdoc->currentModelRowIndex() < m->rowCount()) {
				qfTrash() << "\tsaving old document";
				saveDocument();
			}
		}
		int row = current.row();
	//qfTrash() << __LINE__ << " row:" << row;
		if(row >= 0) {
			tdoc->setCurrentModelRowIndex(row);
			QFDataFormDocument::Mode edit_mode = (m->isEditRowsAllowed())? QFDataFormDocument::ModeEdit: QFDataFormDocument::ModeView;
			tdoc->setMode(edit_mode); /// aby to nebylo readonly
			qfTrash() << "\treloading new document in mode" << edit_mode;
			reloadDocument();
		}
	}
}
/*
void QFDataFormWidget::masterViewCurrentIndexChanging(const QModelIndex &current, const QModelIndex &previous)
{
	qfTrash() << QF_FUNC_NAME << "current row:" << current.row() << "previous row:" << previous.row() << "document row:" << document()->currentModelRowIndex();
	if(current.row() == document()->currentModelRowIndex() && current.row() == previous.row()) return;
	int row = previous.row();
	if(document()->currentModelRowIndex() >= 0) {
		saveDocument();
	}
	row = current.row();
	qfTrash() << __LINE__ << " row:" << row;
	if(row >= 0) {
		QFTableModel *m = document()->model();
		document()->setCurrentModelRowIndex(row);
		document()->setMode((m->isEditRowsAllowed())? QFDataFormDocument::ModeEdit: QFDataFormDocument::ModeView); /// aby to nebylo readonly
		reloadDocument();
	}
}
*/
void QFDataFormWidget::createActions()
{
	QFAction *a;
	{
		a = new QFAction(QFPixmapCache::icon("icon.new", "new.png"), tr("New"), this);
		a->setId("new");
		a->setShortcut(QKeySequence(tr("Ctrl+Ins", "new")));
		a->setShortcutContext(Qt::WidgetShortcut);
		connect(a, SIGNAL(triggered()), this, SLOT(newDocument()));
		f_toolBarActions << a;
	}
	{
		a = new QFAction(QFPixmapCache::icon("icon.delete", "delete.png"), tr("Delete"), this);
		a->setId("delete");
		a->setShortcut(QKeySequence(tr("Ctrl+Del", "delete")));
		a->setShortcutContext(Qt::WidgetShortcut);
		connect(a, SIGNAL(triggered()), this, SLOT(dropDocument()));
		f_toolBarActions << a;
	}
	{
		a = new QFAction( this);
		a->setSeparator(true);
		a->setId(QString("sep_%1").arg(a->creationId()));
		f_toolBarActions << a;
	}
	{
		a = new QFAction(QFPixmapCache::icon("icon.post", "sql_post.png"), tr("Ulozit"), this);
		a->setId("post");
		a->setShortcut(QKeySequence(tr("Ctrl+Return", "post")));
		a->setShortcutContext(Qt::WidgetShortcut);
		connect(a, SIGNAL(triggered()), this, SLOT(saveDocument()));
		f_toolBarActions << a;
	}
	{
		a = new QFAction(QFPixmapCache::icon("icon.reload", "reload.png"), tr("Znovu nacist data"), this);
		a->setId("reload");
		a->setShortcut(QKeySequence(tr("Ctrl+R", "reload")));
		a->setShortcutContext(Qt::WidgetShortcut);
		connect(a, SIGNAL(triggered()), this, SLOT(reloadDocument()));
		f_toolBarActions << a;
	}
}

QFAction * QFDataFormWidget::toolBarAction(const QString & id, bool throw_exc) throw( QFException )
{
	QFAction *ret = NULL;
	foreach(QFAction *a, f_toolBarActions) if(a->id() == id) {ret = a; break;}
	if(!ret && throw_exc) QF_EXCEPTION(tr("toolbar action '%1' not found").arg(id));
	return ret;
}

void QFDataFormWidget::showEvent(QShowEvent *ev)
{
	qfTrash() << QF_FUNC_NAME;
	//emit connectRequest(this);
	QFrame::showEvent(ev);
}

void QFDataFormWidget::refreshActions()
{
	qfLogFuncFrame();// << sender();
	//qfWarning() << QFLog::stackTrace();
	QFDataFormDocument *doc = document(!Qf::ThrowExc);
	qfTrash() << "\tdocument:" << doc;
	if(doc) {
		qfTrash() << "\tdocument is empty:" << doc->isEmpty();
		if(doc->isEmpty()) {
			qfTrash() << "disable actions";
			foreach(QFAction *a, f_toolBarActions) {
				if(a->id() != "new") a->setEnabled(false);
			}
			qfTrash() << "\t hiding widget because document is empty";
			hide();
		}
		else {
			qfTrash() << "enable actions";
			bool doc_dirty = doc->isDataDirty();
			//qfInfo() << "doc is dirty:" << doc_dirty;
			foreach(QFAction *a, f_toolBarActions) {
				a->setEnabled(true);
				//qfInfo() << "enabling:" << a->id();
				if(a->id() == "post") {
					//qfInfo() << "POST set enabled" << doc_dirty;
					/// zakazovat akci save nemuzu, protoze nekdy se vola flush() az pri save, takze dirty neni smeroplatny
					if(doc_dirty) a->setIcon(QFPixmapCache::icon("saveas"));
					else a->setIcon(QFPixmapCache::icon("save"));
				}
			}
			qfTrash() << "\t showing widget because document is not empty";
			show();
		}
	}

	qfTrash() << "\t emit actionsRefreshed();";
	emit actionsRefreshed();

	/// styl nastav az naposled, protoze signal actionsRefreshed() muze jeste nastavit property nekterych widgetu v DataFormWidget
	QString style_sheet;
	style_sheet = "QWidget[readOnly=\"true\"] {background-color: " + QFDataFormWidget::ReadOnlyWidgetBackgroundColor + "}";
	qfTrash() << "\t setting DataFormWidget styleSheet:" << style_sheet;
	setStyleSheet(style_sheet);
}

void QFDataFormWidget::flushFocusedSqlWidget()
{
	QWidget *w = focusWidget();
	while(w) {
		QFSqlControl *ctrl = dynamic_cast<QFSqlControl*>(w);
		if(ctrl) {
			//qfInfo() << "flush sqlid:" << ctrl->sqlId() << "oject name:" << w->objectName();
			//qfInfo() << "flushFocusedSqlWidget sqlid:" << ctrl->sqlId();
			ctrl->flushValue();
			break;
		}
		w = w->parentWidget();
		//if(w->metaObject()->indexOfProperty("sqlId") >= 0) {
		//}
	}
}

void QFDataFormWidget::saveDocument()
{
	qfTrash() << QF_FUNC_NAME;
	flushFocusedSqlWidget();
	QString errmsg;
	if(!checkMandatoryFields(errmsg)) throw QFMandatoryEmptyFieldException(errmsg);
	emit _save();
}

bool QFDataFormWidget::checkMandatoryFields(QString &errmsg)
{
	QFDataFormDocument *doc = document(!Qf::ThrowExc);
	bool ret = true;
	if(doc && !doc->isEmpty()) {
		foreach(QWidget *w, mandatorySqlWidgets()) {
			QString sql_id = w->property("sqlId").toString();
			QString s = doc->value(sql_id).toString();
			if(s.isEmpty()) {
				errmsg = w->property("mandatoryErrorMessage").toString();
				if(errmsg.isEmpty()) errmsg = tr("Pole '%1' je povinne.").arg(sql_id);
				ret = false;
				break;
			}
		}
	}
	return ret;
}

#ifdef QFSQCONTROL_WITH_IGNORED
void QFDataFormWidget::setSqlControlsIgnored(QWidget * parent_w, bool ignore)
{
	QList<QWidget*> lst = parent_w->findChildren<QWidget*>();
	foreach(QWidget *w, lst) {
		QFSqlControl *ctrl = dynamic_cast<QFSqlControl*>(w);
		if(ctrl) ctrl->setIgnored(ignore);
	}
}
#endif
//=====================================================
//                                             QFSqlDataFormWidget
//=====================================================
QFSqlFormDocument* QFSqlDataFormWidget::document(bool throw_exc) throw(QFException)
{
	QFSqlFormDocument *ret = qobject_cast<QFSqlFormDocument *>(QFDataFormWidget::document());
	if(ret) return ret;
	if(throw_exc) QF_EXCEPTION("Document is not a kind of QFSqlFormDocument.");
	return NULL;
}

