
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDATAFORMFRAME_H
#define QFDATAFORMFRAME_H

#include <qfguiglobal.h>
#include <qfdialogwidgetframe.h>
#include <qfdataformdialogwidget.h>

//! Neco do ceho se da vlozit QFDataFormDialogWidget jako to QFDataFormDialogu.
//! Pouziva se, kdyz je potreba QFDataFormDialogWidget zobrazit ve widgetu vcetne toolbaru a menu.
class QFGUI_DECL_EXPORT QFDataFormFrame : public QFDialogWidgetFrame
{
	Q_OBJECT;
	protected slots:
		/// tady se rozhodne, jestli se widget zavre nebo co,
		/// defaultni implementace na reject widget zavre, accepted ignoruje.
		//virtual void closeDialogWidgetRequest(QFDialogWidget *w, int result);
	public slots:
		//virtual void closeDataFormDialogWidget() throw(QFException);
		//virtual void accept();
		//virtual void reject();
	public:
		bool isEmpty() const {
			return !dataFormDialogWidget();
		}
		QFDataFormDialogWidget* dataFormDialogWidget() const;
		virtual void setDataFormDialogWidget(QFDataFormDialogWidget *w);
		virtual void pushDataFormDialogWidget(QFDataFormDialogWidget *w);
	public:
		QFDataFormFrame(QWidget *parent = NULL);
		virtual ~QFDataFormFrame();
};

#endif // QFDATAFORMFRAME_H

