
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDATAFORMTABWIDGET_H
#define QFDATAFORMTABWIDGET_H

#include <qfguiglobal.h>
#include <qfdataformdialogwidget.h>
#include "qftabwidget.h"

//class QFDataFormDialogWidget;

//! Tab widget for more pages, each containig QFDataFormDialogWidget.
class QFGUI_DECL_EXPORT QFDataFormTabWidget : public QFTabWidget
{
	Q_OBJECT
	protected:
		virtual bool currentIndexAboutToChange(int new_index);
	public slots:
		virtual void closeWidget(QFDataFormDialogWidget *w);
	public:
		QFDataFormTabWidget(QWidget *parent = NULL);
		virtual ~QFDataFormTabWidget();
		
		QFDataFormDialogWidget* currentDataFormDialogWidget() const {
			return qobject_cast<QFDataFormDialogWidget*>(currentWidget());
		}

		void addDataFormDialogWidget(QFDataFormDialogWidget *w, const QString &caption, const QIcon &icon = QIcon());
		/// pokud to nejde (treba duplicitni klic v datech) vraci false
		bool closeAllTabs();
};
   
#endif // QFDATAFORMTABWIDGET_H

