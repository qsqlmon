
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDATAFORMWIDGET_H
#define QFDATAFORMWIDGET_H

#include <qf.h>
#include <qfguiglobal.h>
#include <qfexception.h>
#include <qfdataformdocument.h>

#include <QFrame>

//class QFDataFormDocument;
//class QFSqlFormDocument;
class QFTableView;
class QFAction;

//! Container for dataform controls like QFSqlLineEdit etc.
//! Zdruzuje widgety pro jeden document.
//! Jen aby se poznalo, ktery widgety patri ke kterymu dokumentu.
class QFGUI_DECL_EXPORT QFDataFormWidget : public QFrame
{
	Q_OBJECT;
	public:
		static QString ReadOnlyWidgetBackgroundColor;
		static QString MandatoryWidgetBackgroundColor;
	protected:
		QPointer<QFDataFormDocument> documentPtr;
		QList<QWidget*> f_sqlWidgets;
		QList<QWidget*> sqlWidgets();
		QList<QWidget*> f_mandatorySqlWidgets;
		QList<QWidget*> mandatorySqlWidgets();
		QList<QFAction*> f_toolBarActions;
	protected:
		virtual void showEvent(QShowEvent *ev);
		//virtual void hideEvent(QHideEvent *ev);
	protected:
		void createActions();
		QFAction* toolBarAction(const QString &id, bool throw_exc = Qf::ThrowExc) throw(QFException);
		//! Connect all widgets that have property sqlColumnName to \a document .
		void connectWidgets();
	public:
		void clearSqlWidgetsCache();
		
		virtual QList<QFAction*> toolBarActions() const {return f_toolBarActions;}

		//! assign document do this widget and connect SQLWidgets and document signal slots.
		virtual void setDocument(QFDataFormDocument *doc);
		QFDataFormDocument* document(bool throw_exc = Qf::ThrowExc) throw(QFException);
	signals:
		void connectRequest(QFDataFormWidget *sender);

		void widgetValueUpdated(const QString &sql_id, const QVariant &val);
		//! akce widgetu byly refreshnuty a mozna i aplikace by si neco povolila nebo zakazala, k tomu slouzi tento signal.
		void actionsRefreshed();
		void documentDataDirtyChanged(bool dirty);

		void documentAboutToSave();
		//void documentValuesReloaded(const QString &dummy = QString());
		void documentSaved(QFDataFormDocument *doc, int mode);
		void documentDropped(const QVariant &id);

	protected slots:
		//virtual void masterViewCurrentIndexChanging(const QModelIndex &current, const QModelIndex &previous);
		virtual void masterViewSelected(const QModelIndex &current);
		//void onDocumentDataDirtyChanged(bool dirty);
	public slots:
		/// dokument ma spoustu pocitanych poli, tak je prepocitej
		void recalculate() {if(document(!Qf::ThrowExc)) document()->recalculate();}
		
		//! Pripojim-li tableview touto funkci, dojde k tomu, ze pri zvyrazneni noveho radku v tabulce se stary ulozi a novy nahraje.
		//! TableView nemusi volat akci xxxInExternalEditor(), jako v pripade connectAsExternalRowEditor(), staci prosta selekce jineho radku.
		//! Pripadne zmeny v datech se nepropaguji zpet do tabulky, pokud TableView a DataFormWidget nesdili stejny model (coz je povoleno a
		//! k tomu je to predevsim:)
		void connectAsRowBrowser(QFTableView *view);
		//! externi editace radku v tabulce bude probihat v tomto widgetu.
		void connectAsExternalRowEditor(QFTableView *sender);
	public:
		/// pripoji signaly objektu table_view, dataExternalySaved() a dataExternalyDropped()
		void connectExternalEditorEditResults(QFTableView *table_view);
#ifdef QFSQCONTROL_WITH_IGNORED
		static void setSqlControlsIgnored(QWidget *parent_w, bool ignore);
#endif
	protected slots:
		void _documentAboutToSave();
		void _documentDropped(const QVariant &id);
		void _documentSaved(QFDataFormDocument *doc, int mode);

		void _loadView(const QVariant &id) {emit _load(id, QFDataFormDocument::ModeView);}
		void _loadEdit(const QVariant &id) {emit _load(id, QFDataFormDocument::ModeEdit);}
		void _loadInsert() {emit _load(QVariant(), QFDataFormDocument::ModeInsert);}
		virtual bool checkMandatoryFields(QString &errmsg);
		virtual void refreshActions();
	signals:
		void _load(const QVariant &id, QFDataFormDocument::Mode mode);
		//=========================================================
		void _save();
		void _drop();
		void _reload();

	public slots:
		// synchronizuje sql widgety s obsahem a stavem dokumnetu
		//virtual void sync();
		//virtual void documentLockInfo(const QString&);
		void flushFocusedSqlWidget();

		virtual void newDocument() {emit _load(QVariant(), QFDataFormDocument::ModeInsert);}
		virtual void loadDocument(const QVariant &id, QFDataFormDocument::Mode mode) {emit _load(id, mode);}
		virtual void reloadDocument() {emit _reload();}
		virtual void saveDocument();
		virtual void dropDocument() {emit _drop();}
	public:
		QFDataFormWidget(QWidget *parent = NULL);
		virtual ~QFDataFormWidget();
};

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFSqlDataFormWidget : public QFDataFormWidget
{
	Q_OBJECT
	public:
		QFSqlFormDocument* document(bool throw_exc = Qf::ThrowExc) throw(QFException);
	public:
		QFSqlDataFormWidget(QWidget *parent = 0) : QFDataFormWidget(parent) {}
		virtual ~QFSqlDataFormWidget() {}
};


#endif // QFDATAFORMWIDGET_H

