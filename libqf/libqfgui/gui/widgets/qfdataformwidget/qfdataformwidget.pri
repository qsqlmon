
HEADERS +=        \
	$$PWD/qfdataformwidget.h      \
	$$PWD/qfdataformdialogwidget.h     \
	$$PWD/qfdataformtabwidget.h    \
	$$PWD/qfdataformframe.h   \
	$$PWD/qfdataformtoolbar.h  \

SOURCES +=        \
	$$PWD/qfdataformwidget.cpp      \
	$$PWD/qfdataformdialogwidget.cpp     \
	$$PWD/qfdataformtabwidget.cpp    \
	$$PWD/qfdataformframe.cpp   \
	$$PWD/qfdataformtoolbar.cpp  \

RESOURCES +=         \
    $$PWD/qfdataformdialogwidget.qrc     \

