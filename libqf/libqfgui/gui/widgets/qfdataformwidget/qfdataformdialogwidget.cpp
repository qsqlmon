
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include <qfmessage.h>
#include <qftoolbar.h>
#include <qfaction.h>
#include <qfapplication.h>
#include <qfdlgexception.h>
#include <qfpixmapcache.h>
#include <qfdataformwidget.h>
#include <qfsqlcontrol.h>

#include "qfdataformdialogwidget.h"

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

QFDataFormDialogWidget::QFDataFormDialogWidget(QWidget *parent)
	: QFDialogWidget(parent), f_dataformWidgetsConnected(false)
{
	f_uiBuilder.setPrevUiBuilder(QFDialogWidget::uiBuilder());
	f_uiBuilder.setCreatedActionsParent(this);
	f_uiBuilder.open(":/libqfgui/gui/widgets/qfdataformwidget/qfdataformdialogwidget.ui.xml");
	f_uiBuilder.connectActions(this, QString(), Qt::QueuedConnection);/// abych nevolal delete na dialog widget v event handleru
	f_actions += f_uiBuilder.actions();
	//setAttribute(Qt::WA_DeleteOnClose, true);
	//toolBar = new QFToolBar(this);
	//toolBar->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	//QBoxLayout *ly = qobject_cast<QBoxLayout*>(layout());
	//ly->setMargin(0);
	//if(ly) ly->insertWidget(0, tb);
/*
	QFAction *a;

	a = new QFAction(QFPixmapCache::icon("icon.delete", "delete.png"), tr("Smazat"), this);
	a->setToolTip(tr("Smazat"));
	connect(a, SIGNAL(triggered()), this, SLOT(drop()));
	a->setId("delete");
	my_actions << a;
	//toolBar->addAction(a);

	a = new QFAction( this);
	a->setSeparator(true);
	my_actions << a;

	a = new QFAction(QFPixmapCache::icon("icon.cancel", "cancel.png"), tr("Storno"), this);
	a->setToolTip(tr("Zavrit dialog bez ulozeni poslednich zmen"));
	connect(a, SIGNAL(triggered()), this, SLOT(reject()));
	a->setId("reject");
	my_actions << a;

	a = new QFAction(QFPixmapCache::icon("icon.dataformDialog.accept", "ok.png"), tr("Ok"), this);
	a->setToolTip(tr("Ulozit zmeny"));
	connect(a, SIGNAL(triggered(QFAction*)), this, SLOT(accept()));
	a->setId("accept");
	my_actions << a;

	a = new QFAction(QFPixmapCache::icon("icon.reload", "reload.png"), tr("Zvovu nacist"), this);
	a->setToolTip(tr("Znovu nacist data"));
	connect(a, SIGNAL(triggered(QFAction*)), this, SLOT(reload()));
	a->setId("reload");
	my_actions << a;

	f_actions += my_actions;
	*/
}

QFDataFormDialogWidget::~QFDataFormDialogWidget()
{
	qfTrash() << QF_FUNC_NAME;
	//qfInfo() << objectName() << "~QFDataFormDialogWidget();";
}

void QFDataFormDialogWidget::accept()
{
	qfLogFuncFrame();
	if(save()) {
		qfTrash() << "\t successfully saved";
		QFDialogWidget::accept();
		//qfInfo() << "want close please!!!";
	}
}
/*
void QFDataFormDialogWidget::reject()
{
	qfTrash() << QF_FUNC_NAME;
	emit wantClose(this, QDialog::Rejected);
}
*/
/*
void QFDataFormDialogWidget::emitWantClose()
{
	qfTrash() << QF_FUNC_NAME;
	emit wantClose(this);
}
*/
/*
void QFDataFormDialogWidget::flush()
{
	qfLogFuncFrame();
	QWidget *recently_focused = QApplication::focusWidget();
	QFSqlControl *ctrl = NULL;
	while(recently_focused) {
		ctrl = dynamic_cast<QFSqlControl*>(recently_focused);
		if(ctrl) break;
		recently_focused = recently_focused->parentWidget();
	}
	if(ctrl) {
		qfTrash() << "\tflushing:" << recently_focused->objectName();
		qfInfo() << "flush sqlid:" << ctrl->sqlId() << "oject name:" << recently_focused->objectName();
		ctrl->flushValue(); /// ulozi obsah prave editovanyho controlu do dokumentu.
	}
}
*/
bool QFDataFormDialogWidget::save() throw(QFException)
{
	qfLogFuncFrame();
	//flush();
	emit saveDocumentData();
	return true;
}

void QFDataFormDialogWidget::drop()
{
	qfLogFuncFrame();
	// insertnuty neulozeny data dropni bez debat
	//if(mode() == QFDataFormDocument::ModeInsert) return true;
	//if(QFMessage::askYesNo(this, TR("Do you  realy want to delete this data?"), true)) {
	//qfInfo() << objectName() << "emit dropDocumentData();";
	emit dropDocumentData();
	//qfInfo() << objectName() << "emit wantClose(this, QFDialog::Dropped);";
	emit wantClose(this, QFDialog::Dropped);
}

void QFDataFormDialogWidget::load(const QVariant &row_id, QFDataFormDocument::Mode mode)
{
	qfTrash() << QF_FUNC_NAME;
	try {
		document(); /// pokud widget tvori dokument on demand, tak ho vytvor, kdyby jeste neexistoval
		connectDataFormWidgets();
		emit loadDocumentData(row_id, mode);
	}
	catch(QFSqlException &e) {
		QFDlgException::exec(this, e);
		//throw;
	}
}

void QFDataFormDialogWidget::reload()
{
	qfTrash() << QF_FUNC_NAME;
	try {
		connectDataFormWidgets();
		emit reloadDocumentData();
	}
	catch(QFSqlException &e) {
		QFDlgException::exec(this, e);
	}
}

void QFDataFormDialogWidget::connectDataFormWidgets()
{
	qfLogFuncFrame();
	if(!f_dataformWidgetsConnected) {
		f_dataformWidgetsConnected = true;
		QList<QFDataFormWidget *> lst = findChildren<QFDataFormWidget *>();
		//QList<QFDataFormWidget *> lst = centralWidget()->findChildren<QFDataFormWidget *>();
		foreach(QFDataFormWidget *w, lst) {
			qfTrash() << "\tconnecting" << w->objectName() << w;
			connect(this, SIGNAL(saveDocumentData()), w, SLOT(saveDocument()));
			connect(this, SIGNAL(dropDocumentData()), w, SLOT(dropDocument()));
			connect(this, SIGNAL(loadDocumentData(const QVariant &, QFDataFormDocument::Mode)), w, SLOT(loadDocument(const QVariant &, QFDataFormDocument::Mode)));
			connect(this, SIGNAL(reloadDocumentData()), w, SLOT(reloadDocument()));

			connect(w, SIGNAL(documentSaved(QFDataFormDocument*, int)), this, SIGNAL(documentSaved(QFDataFormDocument*, int)));
			connect(w, SIGNAL(documentDropped(const QVariant &)), this, SIGNAL(documentDropped(const QVariant &)));
			connect(w, SIGNAL(actionsRefreshed()), this, SLOT(refreshActions()));
			//connect(w, SIGNAL(documentDataDirtyChanged(bool)), this, SLOT(onDocumentDataDirtyChanged(bool)));
		}
	}
}

QList<QFDataFormWidget*> QFDataFormDialogWidget::dataFormWidgets() const
{
	QList<QFDataFormWidget*> ret = findChildren<QFDataFormWidget*>();
	//QList<QFDataFormWidget*> ret = centralWidget()->findChildren<QFDataFormWidget*>();
	return ret;
}

QFDataFormWidget* QFDataFormDialogWidget::dataFormWidget(const QString &widget_object_name) const
{
	QFDataFormWidget* ret = findChild<QFDataFormWidget*>(widget_object_name);
	if(ret) const_cast<QFDataFormDialogWidget*>(this)->connectDataFormWidgets();
	//QFDataFormWidget* ret = centralWidget()->findChild<QFDataFormWidget*>(widget_object_name);
	return ret;
}

void QFDataFormDialogWidget::setDocument(QFDataFormDocument *doc, const QString &dataform_widget_object_name)
{
	QFDataFormWidget *w = findChild<QFDataFormWidget*>(dataform_widget_object_name);
	if(!w) qfWarning() << ("setDocument(): no dataform widget " + dataform_widget_object_name + " found");
	else {
		connectDataFormWidgets();
		w->setDocument(doc);
	}
}

QFDataFormDocument* QFDataFormDialogWidget::document(const QString &document_object_name, bool throw_exc) 
{
	qfLogFuncFrame() << "document_object_name:" << document_object_name << "throw_exc:" << throw_exc;
	foreach(QFDataFormWidget *w, dataFormWidgets()) {
		QFDataFormDocument *d = qobject_cast<QFDataFormDocument *>(w->document(!Qf::ThrowExc));
		if(d) {
			if(!document_object_name.isEmpty()) {
				if(d->objectName() == document_object_name) return d;
			}
			else return d;
		}
	}
	if(throw_exc) QF_EXCEPTION("Document not found.");
	return NULL;
}

QFDataFormDocument* QFDataFormDialogWidget::document(bool throw_exc) throw(QFException)
{
	QFDataFormDocument *doc = document(QString(), throw_exc);
	//if(!doc) if(throw_exc) QF_EXCEPTION("Document not found.");
	return doc;
}

/*
QFPart::ToolBarList QFDataFormDialogWidget::createToolBars()
{
	QFPart::ToolBarList ret = QFDialogWidget::createToolBars();
	QFToolBar *tool_bar = new QFToolBar(NULL);
	foreach(QFAction *a, my_actions) tool_bar->addAction(a);
	ret.prepend(tool_bar);
	return ret;
}
*/
void QFDataFormDialogWidget::recalculate()
{
	foreach(QFDataFormWidget *w, dataFormWidgets()) w->recalculate();
}

void QFDataFormDialogWidget::refreshActions()
{
	qfLogFuncFrame();
	QFDialogWidget::refreshActions();
	QFDataFormDocument *doc = document(!Qf::ThrowExc);
	if(doc) {
		QFAction *a = action("save", !Qf::ThrowExc);
		if(a) {
			/// zakazovat akci save nemuzu, protoze nekdy se vola flush() az pri save, takze dirty neni smeroplatny
			/*
			bool enabled = qfApp()->currentUserHasGrant(a->grant()) && (doc->isDataDirty());
			a->setEnabled(enabled);
			*/
			if(doc->isDataDirty()) a->setIcon(QFPixmapCache::icon("saveas"));
			else a->setIcon(QFPixmapCache::icon("save"));
			if(doc->mode() == QFDataFormDocument::ModeView) a->setEnabled(false);
		}

		a = action("drop", !Qf::ThrowExc);
		if(a) {
			a->setEnabled(doc->mode() != QFDataFormDocument::ModeView);
			qfTrash() << "\t enable:" << a->isEnabled() << "doc mode:" << doc->mode();
		}
	}
}
/*
void QFDataFormDialogWidget::onDocumentDataDirtyChanged(bool dirty)
{
	//qfInfo() << "QFDataFormDialogWidget::onDocumentDataDirtyChanged" << dirty;
	QFAction *a = action("save", !Qf::ThrowExc); 
	if(a) a->setEnabled(dirty);
}
*/
/*
void QFDataFormDialogWidget::connectAsExternalRowEditor(QFTableView * sender)
{
	QFDataFormWidget *w = findChild<QFDataFormWidget*>();
	if(!w) qfWarning() << ("connectAsExternalRowEditor(): no dataform widget found");
	else {
		w->connectAsExternalRowEditor(sender);
	}
}

*/

void QFDataFormDialogWidget::connectExternalEditorEditResults(QFTableView * table_view)
{
	QFDataFormWidget *w = findChild<QFDataFormWidget*>();
	if(!w) qfWarning() << ("connectAsExternalRowEditor(): no dataform widget found");
	else {
		w->connectExternalEditorEditResults(table_view);
	}
}

void QFDataFormDialogWidget::flushFocusedSqlWidget()
{
	QFDataFormWidget *w = dataFormWidget();
	if(w) w->flushFocusedSqlWidget();
}
