
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdataformtabwidget.h"

#include <qfdlgexception.h>
//#include <qfmessage.h>

#include <QTabBar>

//#include <qfdataformdialogwidget.h>

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

QFDataFormTabWidget::QFDataFormTabWidget(QWidget *parent)
	: QFTabWidget(parent)
{
}

QFDataFormTabWidget::~QFDataFormTabWidget()
{
}

bool QFDataFormTabWidget::currentIndexAboutToChange(int new_index)
{
	qfTrash() << QF_FUNC_NAME;
	Q_UNUSED(new_index);
	bool ret = true;
	/*
	QFDataFormDialogWidget *w = currentDataFormDialogWidget();
	if(w) {
		try {
			w->save();
			//ret = QFMessage::askYesNo(this, "prepnout ?");
		}
		catch(QFException &e) {
			ret = false;
			QFDlgException::exec(this, e);
		}
	}
	*/
	return ret;
}

void QFDataFormTabWidget::closeWidget(QFDataFormDialogWidget *w)
{
	qfTrash() << QF_FUNC_NAME << ((w)? w: 0);
	if(w) {
		int i = indexOf(w);
		if(i >= 0) {
			removeTab(i);
			delete w;
		}
	}
}
		
bool QFDataFormTabWidget::closeAllTabs()
{
	qfTrash() << QF_FUNC_NAME;
	QList<QWidget*> widgets;
	for(int i=0; i<count(); i++) widgets << widget(i);
	foreach(QWidget *w, widgets) {
		QFDataFormDialogWidget *ww = qobject_cast<QFDataFormDialogWidget*>(w);
		if(ww) {
			ww->accept();
		}
		else {
			delete w;
		}
	}
	return (count() == 0);
	/*
	QWidget *w = NULL;
	while((w = currentWidget()) != NULL) {
		QFDataFormDialogWidget *ww = qobject_cast<QFDataFormDialogWidget*>(w);
		if(ww) {
			ww->accept();
		}
		else {
			//QF_ASSERT(ww, "tab child widget is not type of QFDataFormDialogWidget");
			qfWarning() << "tab child widget is not type of QFDataFormDialogWidget";
			int i = indexOf(w);
			if(i >= 0) {
				removeTab(i);
				delete w;
			}
		}
	}
	*/
}

void QFDataFormTabWidget::addDataFormDialogWidget(QFDataFormDialogWidget *dfw, const QString &caption, const QIcon &icon)
{
	qfTrash() << QF_FUNC_NAME << "caption:" << caption << "icon serial:" << icon.serialNumber();
	if(dfw) {
		dfw->connectDataFormWidgets();
		addTab(dfw, icon, caption);
		//connect(dfw, SIGNAL(wantClose(QFDialogWidget*)), this, SLOT(closeWidget(QFDataFormDialogWidget*)));
		setCurrentWidget(dfw);
	}
}

