
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDATAFORMDIALOGWIDGET_H
#define QFDATAFORMDIALOGWIDGET_H

#include <qfguiglobal.h>
#include <qfdialogwidget.h>
#include <qfdataformdocument.h>
#include <qfxmlconfigpersistenter.h>
#include <qfuibuilder.h>

//#include <QWidget>

class QFAction;
class QFDataFormWidget;
class QFDataFormDialogWidget;

class QFGUI_DECL_EXPORT QFDataFormDialogWidgetFactory : public QObject
{
	Q_OBJECT
	//		QF_FIELD_RW(QString, c, setC, reatedWidgetXmlConfigPersistentId);
	public:
		//! context_object je objekt, je napr. QFTableView, pokud to ma byt external editor, factory se muze dozvedet, napr pro ktery radek ma byt widget vytvoren
		virtual QFDataFormDialogWidget* createWidget(QFDataFormDocument::Mode mode = QFDataFormDocument::ModeEdit, QObject *context_object = NULL, QWidget *parent = NULL) = 0;
	public:
		QFDataFormDialogWidgetFactory(QObject *parent = NULL)
			: QObject(parent)
		{
			//setCreatedWidgetXmlConfigPersistentId(created_widget_xmlconfigpersistentid);
		}
		virtual ~QFDataFormDialogWidgetFactory() {}
};

//! Widget, ktery je obsahem QFDataFormDialog nebo QFDataFormTabWidget, to znamena,
//! ze vyrobim takovy widget a on muze byt obsahem modalniho dialogu nebo stranky v tabwidget, napr kniha zakazek.
//! pokud ma nastaveno xmlConfigPersistentId a je vlozena do QFDataFormDialogu, prebira toto id dialog.
class QFGUI_DECL_EXPORT QFDataFormDialogWidget : public QFDialogWidget
{
	Q_OBJECT;
	friend class QFDataFormTabWidget;
	friend class QFDataFormFrame;
	private:
		QFUiBuilder f_uiBuilder;
	//QFPart::ActionList my_actions;
	protected:
		bool f_dataformWidgetsConnected;
		//QFToolBar *toolBar;

	signals:
		// Emited after accept, reject, drop. This signal can be used for closing of parent dialog after one of this actions.
		//void actionPerformed(QFDataFormDialogWidget *w, QFDataFormDialogWidget::CloseCode code);
		// @param result je neco z QFDialog::ModalResult
		//void wantClose(QFDataFormDialogWidget* w);
		void documentSaved(QFDataFormDocument *doc, int mode);
		void documentDropped(const QVariant &id);
		//void idInserted(const QVariant &id, const QString &context = QString());
	signals:
		void dropDocumentData();
		void saveDocumentData();

		//! When \a reload() is called, this signal is emitted to reload the widget QFDataFormDocuments with data.
		void loadDocumentData(const QVariant &row_id, QFDataFormDocument::Mode mode);
		//! Rekne dokumentum, aby nahraly znovu svoje data ze serveru pro ID a Mode zadane
		//! predchozim volanim funkce \a loadDocumentData(const QVariant &row_id, QFDataFormDocument::Mode mode) .
		void reloadDocumentData();
	protected slots:
		//void dropTriggered();
		//void onDocumentDataDirtyChanged(bool dirty);
	public slots:
		virtual void accept();
		//virtual void reject();
		void flushFocusedSqlWidget();

		
		//! reload widget and also connect this and children QFDataFormWidgets signals slots if it is called first time.
		virtual void load(const QVariant &row_id, QFDataFormDocument::Mode mode);
		virtual void reload();
		//! vraci vzdycky true.
		virtual bool save() throw(QFException);
		//! Drop data and emit wantClose(..., Dropped);
		virtual void drop();

		//! flushne control, ktery ma prave fokus. Neni potreba, o to se postara flushSqlWidget() ve funkci QFDataFormWidget::saveDocument()
		//virtual void flush();

		virtual void recalculate();
	protected:
		//QWidget *fCentralWidget;

		void connectDataFormWidgets();
		// emits wantClose().
		//virtual void emitWantClose();
	public:
		virtual QFUiBuilder* uiBuilder() {return &f_uiBuilder;}
		QList<QFDataFormWidget*> dataFormWidgets() const;
		/// pokud je jich tam vic, chova se jako \a QObject::findChild()
		QFDataFormWidget* dataFormWidget(const QString &widget_object_name = QString()) const; 

		//! Projde vsechny deti typu \a QFDataFormWidget a hleda v jejich dokumentech ten,
		//! ktery se jmenuje \a document_object_name . Pokud je \a document_object_name
		//! prazdny bere se dokument prvniho widgetu, ktery neni NULL.
		virtual void setDocument(QFDataFormDocument *doc, const QString &dataform_widget_object_name = QString());
		//! Projde vsechny deti typu \a QFDataFormWidget a hleda v jejich dokumentech ten,
		//! ktery se jmenuje \a document_object_name . Pokud je \a document_object_name
		//! prazdny bere se dokument prvniho widgetu, ktery neni NULL.
		virtual QFDataFormDocument* document(const QString &document_object_name, bool throw_exc = Qf::ThrowExc);
		//! vrati prvni dokument, ktery najde nebo vrhne exception, pokud nereknu aby to nedelal, pak vrati NULL.
		virtual QFDataFormDocument* document(bool throw_exc = Qf::ThrowExc) throw(QFException);
		bool isDocumentLoaded() {
			return (document(!Qf::ThrowExc) && !document()->isEmpty());
		}
		
		void connectExternalEditorEditResults(QFTableView *table_view);

		//void connectAsExternalRowEditor(QFTableView *sender);
		///======= QFDialogWidgetInterface ================
		//virtual QFPart::ToolBarList createToolBars();
		///===============================================
		virtual void refreshActions();
	public:
		QFDataFormDialogWidget(QWidget *parent = NULL);
		virtual ~QFDataFormDialogWidget();
};

#endif // QFDATAFORMDIALOGWIDGET_H

