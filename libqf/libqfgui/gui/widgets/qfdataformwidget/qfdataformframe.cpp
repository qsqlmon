
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdataformframe.h"

#include <qfdialog.h>

#include <qflogcust.h>

QFDataFormFrame::QFDataFormFrame(QWidget *parent)
	: QFDialogWidgetFrame(parent)
{
}

QFDataFormFrame::~QFDataFormFrame()
{
}
/*
void QFDataFormFrame::closeDataFormDialogWidget() throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	closeDialogWidget();
}
*/
QFDataFormDialogWidget* QFDataFormFrame::dataFormDialogWidget() const
{
	QWidget *w = currentWidget();
	/// kdyby tam byla neviditelna, tak tam uz neni, je pripravena na vymaz
	if(w && !w->isVisible()) return NULL;
	return w->findChild<QFDataFormDialogWidget *>();
}

void QFDataFormFrame::setDataFormDialogWidget(QFDataFormDialogWidget *dfw)
{
	qfTrash() << QF_FUNC_NAME;// << "caption:" << caption << "icon serial:" << icon.serialNumber();
	/// pokud je tam otevreny widget, tak se podelalo jeho ukladani a novej tam nemuzu dat.
	if(dataFormDialogWidget()) return;
	setDialogWidget(dfw);
	if(dfw) {
		dfw->connectDataFormWidgets();
		//connect(dfw, SIGNAL(wantClose(QFDialogWidget*, int)), this, SLOT(closeDialogWidgetRequest(QFDialogWidget*, int)));//, Qt::QueuedConnection);
	}
}

void QFDataFormFrame::pushDataFormDialogWidget(QFDataFormDialogWidget * dfw)
{
	qfLogFuncFrame();
	pushDialogWidget(dfw);
	if(dfw) {
		dfw->connectDataFormWidgets();
		//connect(dfw, SIGNAL(wantClose(QFDialogWidget*, int)), this, SLOT(closeDialogWidgetRequest(QFDialogWidget*, int)));//, Qt::QueuedConnection);
	}
}
/*
void QFDataFormFrame::closeDialogWidgetRequest(QFDialogWidget *, int )
{
	qfTrash() << QF_FUNC_NAME;
	//if(result == QFDialog::Rejected) closeDialogWidget();
	closeDialogWidget();
}
*/
/*
void QFDataFormFrame::accept()
{
	qfLogFuncFrame();
	//QWidget *w = currentWidget();
	QFDataFormDialogWidget *w = dataFormDialogWidget();
	qfTrash() << "\t dataFormDialogWidget:" << w;
	if(w) {
		w->save();
		closeDialogWidget();
	}
}
*/
/*
void QFDataFormFrame::reject()
{
	qfLogFuncFrame();
	QFDataFormDialogWidget *w = dataFormDialogWidget();
	if(w) {
		qfTrash() << "\t closing dialog widget name:" << w->objectName() << "type:" << w->metaObject()->className();
		closeDialogWidget();
	}
}
*/
