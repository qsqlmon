
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfmdisubwindow.h"

#include <qfdialogwidget.h>
#include <qftoolbar.h>

#include <QHBoxLayout>
#include <QMenuBar>

#include <qflogcust.h>

QFMdiSubWindow::QFMdiSubWindow(QWidget * parent, Qt::WindowFlags flags)
	: QMdiSubWindow(parent, flags)
{
	setAttribute(Qt::WA_DeleteOnClose); /// zavolej na subwindow delete pri jeho zavreni
	f_centralWidget = NULL;
}

QFMdiSubWindow::~QFMdiSubWindow()
{
}

QWidget * QFMdiSubWindow::centralWidget()
{
	if(!f_centralWidget) {
		QBoxLayout *ly = qobject_cast<QBoxLayout*>(layout());
		//qfInfo() << "layout:" << layout();
		if(!ly) {
			ly = new QVBoxLayout(this);
		}
		//ly->setObjectName("QFMdiSubWindow");
		ly->setMargin(0);
		QFrame *frm = new QFrame(this);
		//frm->setFrameShape(QFrame::Box);
		f_centralWidget = frm;
		ly->addWidget(f_centralWidget);
	}
	return f_centralWidget;
}

#define TOOLBARS_INSIDE_WINDOW
void QFMdiSubWindow::setDialogWidget(QFDialogWidget * wd) throw( QFException )
{
	qfLogFuncFrame() << wd;
	{
		QWidget *wc = centralWidget();
		//qfTrash() << "\t centralWidget:" << wc;
		//qDeleteAll(wc->findChildren<QWidget*>());
		//qDeleteAll(wc->findChildren<QLayout*>());
		//qfTrash() << "\t create new layout for centralWidget";
		QHBoxLayout *ly1 = new QHBoxLayout(wc);
		//ly1->setObjectName("f_centralWidget");
		ly1->setMargin(0);
		qfTrash() << "\t adding widget to centralWidget layout";
		ly1->addWidget(wd);
	}

	//if(xmlConfigPersistentId().isEmpty() && !wd->xmlConfigPersistentId().isEmpty()) {
	//	setXmlConfigPersistentId(wd->xmlConfigPersistentId() + "/qfdialog");
	//}

	QBoxLayout *ly = qobject_cast<QBoxLayout*>(layout());
#ifdef TOOLBARS_INSIDE_WINDOW
	QFPart::ToolBarList tool_bars = wd->createToolBars();
	if(tool_bars.count() == 1) {
		QFToolBar *tb = tool_bars[0];
		tb->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
		ly->insertWidget(0, tb);
	}
	else if(!tool_bars.isEmpty()) {
		/// toolbary dej vedle sebe
		QHBoxLayout *ly1 = new QHBoxLayout(NULL);
		foreach(QFToolBar *tb, tool_bars) {
			tb->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
			ly1->addWidget(tb);
		}
		ly1->addStretch();
		ly->insertLayout(0, ly1);
	}
#else
	f_toolBars = wd->createToolBars();
#endif
	int menu_widget_index = 0;
	/*
	if(wd->hasCaption()) {
		///caption
	QWidget *w = wd->createCaption();
	if(w) {
	ly->insertWidget(0, w);
	menu_widget_index = 1;
}
}
	*/
	{
		QMenuBar *mb = new QMenuBar(this);
		//mb->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
		wd->updateMenuOrBar(mb);
		if(mb->actions().isEmpty()) { delete mb; }
		else { ly->insertWidget(menu_widget_index, mb); }
	}
	{
		QFDialogWidgetCaptionFrame *cf = wd->captionFrame();
		if(cf) {
			ly->insertWidget(0, cf);
			//ly->addWidget(cf);
		}
	}

	//connect(wd, SIGNAL(wantClose(QFDialogWidget*, int)), this, SLOT(closeDialogWidget(QFDialogWidget*, int)));
}

