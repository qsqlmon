
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfmdiarea.h"
#include "qfmdisubwindow.h"

#include <qflogcust.h>

QFMdiArea::QFMdiArea(QWidget *parent)
	: QMdiArea(parent)
{
}

QFMdiArea::~QFMdiArea()
{
}

QMdiSubWindow * QFMdiArea::addSubWindow(QWidget * w, Qt::WindowFlags window_flags)
{
	QMdiSubWindow *ret = QMdiArea::addSubWindow(w, window_flags);
	connect(ret, SIGNAL(windowStateChanged ( Qt::WindowStates, Qt::WindowStates )), this, SLOT(on_subWindowStateChanged(Qt::WindowStates , Qt::WindowStates )));
	return ret;
}

QFMdiSubWindow * QFMdiArea::addDialogWidgetSubWindow(QFDialogWidget * dw, Qt::WindowFlags window_flags)
{
	QFMdiSubWindow *sw = new QFMdiSubWindow();
	sw->setDialogWidget(dw);
	addSubWindow(sw, window_flags);
	return sw;
}

void QFMdiArea::on_subWindowStateChanged(Qt::WindowStates old_state, Qt::WindowStates new_state)
{
	QFMdiSubWindow *w = qobject_cast<QFMdiSubWindow*>(sender());
	if(w) {
		bool old_active = old_state.testFlag(Qt::WindowActive);
		bool new_active = new_state.testFlag(Qt::WindowActive);
		if(old_active != new_active) emit subWindowActiveStateChanged(w, new_active);
	}
}

