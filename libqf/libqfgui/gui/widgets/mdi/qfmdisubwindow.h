
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFMDISUBWINDOW_H
#define QFMDISUBWINDOW_H

#include <qfguiglobal.h>
#include <qfexception.h>
#include <qfpart.h>

#include <QMdiSubWindow>

class QFDialogWidget;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFMdiSubWindow : public QMdiSubWindow
{
	Q_OBJECT;
	protected:
		QWidget *f_centralWidget;
		QFPart::ToolBarList f_toolBars;
	public:
		const QFPart::ToolBarList& toolBars() const {return f_toolBars;}
		virtual QWidget* centralWidget();
		virtual void setDialogWidget(QFDialogWidget *w) throw(QFException);
	public:
		QFMdiSubWindow(QWidget * parent = 0, Qt::WindowFlags flags = 0);
		virtual ~QFMdiSubWindow();
};

#endif // QFMDISUBWINDOW_H

