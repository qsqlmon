
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFMDIAREA_H
#define QFMDIAREA_H

#include <qfguiglobal.h>
#include <qfpart.h>

#include <QMdiArea>

class QFDialogWidget;
class QFMdiSubWindow;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFMdiArea : public QMdiArea
{
	Q_OBJECT;
	signals:
		void  subWindowActiveStateChanged(QFMdiSubWindow *w, bool currently_active);
	protected slots:
		void on_subWindowStateChanged(Qt::WindowStates oldState, Qt::WindowStates newState);
	public:
		QFMdiSubWindow* addDialogWidgetSubWindow(QFDialogWidget * dw, Qt::WindowFlags window_flags = 0 );
		QMdiSubWindow* addSubWindow(QWidget * w, Qt::WindowFlags window_flags = 0 );
	public:
		QFMdiArea(QWidget *parent = NULL);
		virtual ~QFMdiArea();
};

#endif // QFMDIAREA_H

