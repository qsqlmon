//
// C++ Interface: qfheaderview
//
// Description:
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef QFHEADERVIEW_H
#define QFHEADERVIEW_H

#include <QHeaderView>

#include <qfguiglobal.h>

class QFTableView;
class QLabel;

/**
@author Fanda Vacek
 */
class QFGUI_DECL_EXPORT QFHeaderView : public QHeaderView
{
	Q_OBJECT;
	public slots:
		//void resizeSectionsToContents();
	public:
		QFHeaderView(Qt::Orientation orientation, QWidget *parent = 0) : QHeaderView(orientation, parent) {}
};

/**
@author Fanda Vacek
*/
class QFGUI_DECL_EXPORT QFTableHeaderView : public QFHeaderView
{
	Q_OBJECT;
			//friend class QFTableView;
	protected:
		Qt::KeyboardModifiers recentMouseReleaseModifiers;
	public:
		struct SectionProperty
		{
			QVariantMap values;
			//int size;
			//bool hidden;
			int size() const {
				if(values.contains("size")) return values.value("size").toInt();
				return -1;
			}
			void setSize(int sz) {values["size"] = sz;}
			int hidden() const {
				if(values.contains("hidden")) return values.value("hidden").toBool();
				return false;
			}
			void setHidden(bool b) {values["hidden"] = b;}

			bool isEmpty() const {return values.isEmpty();}

			SectionProperty(const QVariantMap &m = QVariantMap()) : values(m) {}
		};
		typedef QMap<QString, SectionProperty> SectionProperties;
		SectionProperties sectionProperties;
	public:
		//! kdyz najde v property pro section size == -1 nebo property nenajde,
		//! zkusi najit table a nastavit sirku sloupce automaticky podle obsahu, kdyz to nejde,
		//! zavola resizeToContens(),
		//! jinak nastavi velikos na size.
		void resizeSections();
		//! Save actual section sizes to sectionProperties.
		void saveSectionSizes();
		//void resizeSectionToContents(int logical_index);
	protected slots:
		void sectionClicked_helper(int logical_index);

		//void sizeChanged_helper(int logicalIndex, int oldSize, int newSize);
		void sectionCountChanged_debug(int old_cnt, int new_cnt);
	signals:
		void sortRequest(int section, bool aditive);
		//void sectionClicked(int logical_index, Qt::KeyboardModifiers modifiers);
	protected:
		//! Finds first parent of type \a QFTableView .
		QFTableView* tableView();

		void setSectionHidden(int logical_index, bool b = true);
	protected:
		QLabel *lblSearchString;
	public slots:
		virtual void reset();
	public:
		void setSearchString(const QString &s);
	protected:
		virtual QSize sizeHint() const;
		virtual void mouseReleaseEvent(QMouseEvent *e);
		virtual void contextMenuEvent(QContextMenuEvent *e);
		virtual bool viewportEvent(QEvent *event);
		//virtual void doItemsLayout();
	public:
		QFTableHeaderView(Qt::Orientation orientation, QWidget *parent = 0);
		~QFTableHeaderView();
};

/**
@author Fanda Vacek
 */
class QFGUI_DECL_EXPORT QFTreeHeaderView : public QFHeaderView
{
	Q_OBJECT;
	public:
		QFTreeHeaderView(QWidget *parent = 0);
};


#endif
