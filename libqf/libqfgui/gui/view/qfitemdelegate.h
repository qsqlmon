//
// C++ Interface: qfitemdelegate
//
// Description:
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef QFITEMDELEGATE_H
#define QFITEMDELEGATE_H

#include <qfguiglobal.h>
#include <qfitemdelegatebase.h>

#include <QAbstractItemView>
#include <QPointer>

class QWidget;

/**
@author Fanda Vacek
*/
class QFGUI_DECL_EXPORT QFItemDelegate : public QFItemDelegateBase
{
	Q_OBJECT
	friend class QFTableView;
	protected:
		QPointer<QWidget> currentEditor;
	protected:
		//QAbstractItemView* view() const;
		/**
		If this funtion returns true, current editor can be closed and editing finished.

		Jak to vlastne funguje, jednak odchytavam mys a klavesy, pokud hrozi, ze se policko presune a je otevreny editor
		zavolam \a canCloseEditor() a pokud funkce vrati false, eventy vyhodim. Dale QFItemDelegate filtruje eventy
		prave aktivniho editoru funkce \a eventFilter() a pokud narazi na event, kterej hrozi editor zavrit,
		zavola \a view()->canCloseEditor() a pokud funkce vrati false, event vyhodi.

		Na \a closeEditor() se nelze spolehnout, protoze se nekdy vola az z \a currentChanged() a to uz se neda vratit.

		Co z toho plyne. Pokud potomek QFTableView pretizi spatne funkce \a keyPressEvent() \a mousePressEvent()
		nebo potomek \a QFItemDelegate si vyrobi vlastni editor v \a createEditor() nebo nainstaluje vlastni \a eventFilter()
		muze prestat fungovat funkce \a canCloseEditor() . U  \a keyPressEvent() a \a mousePressEvent() staci pak jeste zavolat
		puvodni handler, ktery validaci vyvola, u delegata s vlastni \a eventFilter() funkci je treba myslet na to, ze eventFiltery se
		volaji v opacnem poradi, nez byly nainstalovany, takze myslet na to, aby na \a QFItemDelegate::eventFilter() vubec doslo.
		A pokud si pretizim \a QFItemDelegate::createEditor() a vyrabim vlastni editory, nezapomenout jim nainstalovat
		\a QFItemDelegate::eventFilter() .

		Default implementation returns true;
		 */
		virtual bool canCloseEditor();
	public:
		virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
		virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
		//virtual void drawDisplay(QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QString &text) const;
		virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;
		virtual bool eventFilter(QObject *object, QEvent *event);
	public slots:
		void commitAndCloseCurrentEditor();
		//bool editorOpened() const {return currentEditor;}
	public:
		QFItemDelegate(QObject *parent = NULL);//, QAbstractItemView *_view = NULL);
		~QFItemDelegate();
};

#endif
