//
// C++ Implementation: qftreeview
//
// Description:
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "qftreeview.h"

#include <qfheaderview.h>

#include <QKeyEvent>

#include <qflogcust.h>

QFTreeView::QFTreeView(QWidget *parent)
	: QTreeView(parent)
{
	qfTrash() << QF_FUNC_NAME;
	//setHeader(new QFHeaderView(Qt::Horizontal, this));
	//QFFlags<QAbstractItemView::EditTrigger> et;
	//et << QAbstractItemView::AnyKeyPressed
	//et << QAbstractItemView::DoubleClicked << QAbstractItemView::EditKeyPressed;
	setEditTriggers(QAbstractItemView::EditTriggers());
	//qfTrash() << "\tcurrent edit triggers:" << editTriggers();
}

QFTreeView::~QFTreeView()
{
}

void QFTreeView::setExpandedRecursively(const QModelIndex &parent, bool set_expanded)
{
	qfLogFuncFrame();
	QAbstractItemModel *m = model();
	if(!m) return;
	QModelIndex ix = parent;
	if(!ix.isValid()) {
		/// pokud tree view ma vice nez jeden visible root node, projdi je vsechny
		int row = 0;
		do {
			ix = m->index(row, 0);
			qfTrash() << "\t" << m->data(ix).toString() << "index for row:" << row << "is valid:" << ix.isValid();
			row++;
			if(ix.isValid()) setExpandedRecursively(ix, set_expanded);
		} while(ix.isValid());
	}
	else {
		int i;
		for(i=0; i<m->rowCount(ix); i++) {
			setExpandedRecursively(m->index(i, 0, ix), set_expanded);
		}
		setExpanded(ix, set_expanded);
	}
}

void QFTreeView::currentChanged(const QModelIndex& current, const QModelIndex& previous)
{
	QTreeView::currentChanged(current, previous);
	//if(current != previous)
	emit selected(current);
}

void QFTreeView::reset()
{
	//qfTrash() << QF_FUNC_NAME;
	QTreeView::reset();
}

void QFTreeView::mouseDoubleClickEvent(QMouseEvent *e)
{
	/*
	if(e->modifiers() == Qt::NoModifier) {
		if(!canExpand(currentIndex())) {
			e->accept();
			return;
		}
	}
	*/
	QTreeView::mouseDoubleClickEvent(e);
}

void QFTreeView::keyPressEvent(QKeyEvent *e)
{
	qfTrash() << QF_FUNC_NAME;
	bool modified = (e->modifiers() != Qt::NoModifier && e->modifiers() != Qt::KeypadModifier);
	bool key_enter = (e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter);
	bool alpha_num = (e->key() >= 0x20 && e->key() < 0x80);
	if(!modified && key_enter) {
			///timhle jsem zajistil, ze se editor otevira na enter a ne jen na F2, jak je defaultne v QT
			/// viz: QAbstractItemView::keyPressEvent(...)
		qfTrash() << "\tkey chci otevrit editor, state:" << state();
		if(!edit(currentIndex(), EditKeyPressed, e)) {
			e->ignore();
		}
		return;
	}
	else if(!modified && alpha_num) {
		/// incremental search
	}
	QTreeView::keyPressEvent(e);
}

void QFTreeView::update()
{
	viewport()->update();
}

bool QFTreeView::edit(const QModelIndex& index, EditTrigger trigger, QEvent* event)
{
	qfLogFuncFrame() << "trigger:" << trigger << "event type:" << ((event)? event->type(): 0) << "editTriggers:" << editTriggers();
	bool ret = QTreeView::edit(index, trigger, event);
	/*
	if(!ret) {
		if(trigger && (trigger & editTriggers())) {
			emit editRequest(index);
		}	
	}
	*/
	qfTrash() << "\t return:" << ret;
	return ret;
}

