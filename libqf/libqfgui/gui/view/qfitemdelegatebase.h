
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFITEMDELEGATEBASE_H
#define QFITEMDELEGATEBASE_H

#include <qfguiglobal.h>

#include <QItemDelegate>


//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFItemDelegateBase : public QItemDelegate
{
		Q_OBJECT
	public slots:
		bool helpEvent(QHelpEvent *event, QAbstractItemView *view, const QStyleOptionViewItem &option, const QModelIndex &index);
	public:
		virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
		virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;
	public:
		QFItemDelegateBase(QObject *parent = NULL);
		virtual ~QFItemDelegateBase();
};

#endif // QFITEMDELEGATEBASE_H

