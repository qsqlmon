//
// C++ Implementation: qftableview
//
// Description:
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "qftableview.h"

#include <qfjson.h>
#include <qfflags.h>
#include <qfapplication.h>
#include <qfexception.h>
#include <qfheaderview.h>
#include <qfaction.h>
#include <qfitemdelegate.h>
#include <qfdlgexception.h>
#include <qfheaderview.h>
#include <qfdataformdialog.h>
#include <qfdataformdialogwidget.h>
#include <qfpixmapcache.h>
#include <qfmessage.h>
#include <qfdlgtextview.h>
#include <qfdlghtmlview.h>
#include <qfhtmlutils.h>
#include <qf7bittextcodec.h>
#include <qfxmltable.h>
#include <qfuibuilder.h>
#include <qftablemodeldelegate.h>
#include <qfcsvimportdialogwidget.h>
#include <qfcsvexportdialogwidget.h>
#include <qfbuttondialog.h>
#include <qftableprintdialogwidget.h>
#include <qfreportviewwidget.h>

#include <QPaintEvent>
#include <QSqlDriver>
#include <QMenu>
#include <QClipboard>
#include <QTextEdit>
//#include <QTimer>

#include <typeinfo>

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>
#include <qffileutils.h>

QFTableView::QFTableView(QWidget *parent)
 : QTableView(parent)
{
	ignoreCurrentChanged = false;
	setIgnoreFocusOutEvent(true);
	ignoreFocusOutEventWhenOpenExternalEditor = false;
	//setEmitCopyRowSignal(false);
	setReadOnly(false);
	fExternalRowEditorFactory = NULL;
	setAlternatingRowColors(true);
	setIdColumnName("id");
	//f_mode = LookupMode;
	setRemoveRowActionVisibleInExternalMode(false);

	QFTableHeaderView *h;
	h = new QFTableHeaderView(Qt::Vertical, this);
	setVerticalHeader(h);
	h = new QFTableHeaderView(Qt::Horizontal, this);
	setHorizontalHeader(h);
	h->disconnect(SIGNAL(sectionPressed(int)));
	//disconnect(h, SIGNAL(sectionPressed(int)), 0, 0);
	//connect(h, SIGNAL(sectionClicked(int)), this, SLOT(sortByColumn(int)));
	connect(h, SIGNAL(sortRequest(int, bool)), this, SLOT(sortByColumn(int, bool)));
	h->setClickable(true);

	/*
	lblTopLeft = new TopLeftLabel(this);
	lblTopLeft->setText("lblTopLeft");
	//lblTopLeft->setGeometry(0, 0, horizontalHeader()->sizeHint().width(), verticalHeader()->sizeHint().height());
	lblTopLeft->setGeometry(5, 5, 30, 30);
	lblTopLeft->addActions(topLeftActions.values());
	lblTopLeft->setContextMenuPolicy(Qt::ActionsContextMenu);
	*/
	createActions();
	{
		/// top left corner actions
		foreach(QAbstractButton *bt, findChildren<QAbstractButton*>()) {
			if(bt->metaObject()->className() == QString("QTableCornerButton")) { /// src/gui/itemviews/qtableview.cpp:103
				//qfInfo() << "addidng actions";
				bt->setContextMenuPolicy(Qt::ActionsContextMenu);
				bt->addActions(contextMenuActionsForGroups(AllActions));
			};
		}
	}

	setContextMenuPolicy(Qt::DefaultContextMenu);

	setItemDelegate(new QFTableModelDelegate(this));

	QFFlags<QAbstractItemView::EditTrigger> et;
	et << QAbstractItemView::AnyKeyPressed << QAbstractItemView::EditKeyPressed << QAbstractItemView::DoubleClicked;
	setEditTriggers(et);

	setRowEditorMode(RowEditorInline);
	refreshActions();
}

QFTableView::~QFTableView()
{
	qfTrash() << QF_FUNC_NAME <<  objectName();
	savePersistentData();
}

void QFTableView::createActions()
{
	QFAction *a;
	{
		a = new QFAction(tr("Resize columns to contents"), this);
		//a->setIcon(QIcon(":/libqf/images/reload.png"));
		//a->setShortcut(QKeySequence(tr("Ctrl+R", "reload SQL table")));
		//a->setShortcutContext(Qt::WidgetShortcut);
		a->setId("resizeColumnsToContents");
		f_actionGroupsIds[SizeActions] << a->id();
		actionList << a;
		connect(a, SIGNAL(triggered()), this, SLOT(resizeColumnsToContents()));
	}
	{
		a = new QFAction(tr(""), this);
		a->setSeparator(true);
		a->setId(QString("sep_%1").arg(a->creationId()));
		actionList << a; 
	}
	{
		a = new QFAction(tr("Reload"), this);
		a->setIcon(QIcon(":/libqfgui/images/reload.png"));
		a->setShortcut(QKeySequence(tr("Ctrl+R", "reload SQL table")));
		a->setShortcutContext(Qt::WidgetShortcut);
		a->setId("reload");
		f_actionGroupsIds[ViewActions] << a->id();
		actionList << a;
		connect(a, SIGNAL(triggered()), this, SLOT(reload()));
	}
	{
		a = new QFAction( this);
		a->setSeparator(true);
		a->setId(QString("sep_%1").arg(a->creationId()));
		actionList << a;
	}
	{
		a = new QFAction(QFPixmapCache::icon("icon.new", ":/libqfgui/images/new.png"), tr("Insert row"), this);
		a->setShortcut(QKeySequence(tr("Ctrl+Ins", "insert row SQL table")));
		a->setShortcutContext(Qt::WidgetShortcut);
		a->setId("insertRow");
		f_actionGroupsIds[RowActions] << a->id();
		actionList << a;
		connect(a, SIGNAL(triggered()), this, SLOT(insertRow()));
	}
	{
		a = new QFAction(QFPixmapCache::icon("icon.delete", ":/libqfgui/images/delete.png"), tr("Delete selected rows"), this);
		a->setShortcut(QKeySequence(tr("Ctrl+Del", "delete row SQL table")));
		a->setShortcutContext(Qt::WidgetShortcut);
		a->setId("removeSelectedRows");
		f_actionGroupsIds[RowActions] << a->id();
		actionList << a;
		connect(a, SIGNAL(triggered()), this, SLOT(removeSelectedRows()));
	}
	{
		a = new QFAction(tr("Post row edits"), this);
		a->setIcon(QIcon(":/libqfgui/images/sql_post.png"));
		a->setShortcut(QKeySequence(tr("Ctrl+Return", "post row SQL table")));
		a->setShortcutContext(Qt::WidgetShortcut);
		a->setId("postRow");
		f_actionGroupsIds[RowActions] << a->id();
		actionList << a;
		connect(a, SIGNAL(triggered()), this, SLOT(postRow()));
	}
	{
		a = new QFAction(tr("Revert row edits"), this);
		a->setIcon(QIcon(":/libqfgui/images/revert.png"));
		a->setShortcut(QKeySequence(tr("Ctrl+Z", "revert edited row")));
		a->setShortcutContext(Qt::WidgetShortcut);
		a->setId("revertRow");
		f_actionGroupsIds[RowActions] << a->id();
		actionList << a;
		connect(a, SIGNAL(triggered()), this, SLOT(revertRow()));
	}
	{
		a = new QFAction(tr("Copy row"), this);
		a->setIcon(QIcon(":/libqfgui/images/clone.png"));
		a->setId("copyRow");
		a->setVisible(false);
		f_actionGroupsIds[RowActions] << a->id();
		a->setShortcut(QKeySequence(tr("Ctrl+D", "insert row copy")));
		a->setShortcutContext(Qt::WidgetShortcut);
		f_actionGroupsIds[RowActions] << a->id();
		actionList << a;
		connect(a, SIGNAL(triggered()), this, SLOT(copyRow()));
	}
	/*
	{
		a = new QFAction(QFPixmapCache::icon("icon.new", ":/libqf/images/new.png"), tr("Vlozit v formulari"), this);
		a->setToolTip(tr("Vlozit radek v formulari"));
		///dve akce nemohou mit stejnej shortcut!!! QT takovej ignorujou.
		//a->setShortcut(QKeySequence(tr("Ctrl+Ins", "insert row SQL table")));
		a->setShortcutContext(Qt::WidgetShortcut);
		connect(a, SIGNAL(triggered()), this, SLOT(emitInsertRowInExternalEditor()));
		a->setId("insertRowExternal");
		actionList << a;
	}
	*/
	{
		a = new QFAction(tr("Zobrazit ve formulari"), this);
		a->setIcon(QIcon(":/libqfgui/images/view.png"));
		a->setToolTip(tr("Zobrazit radek v formulari pro cteni"));
		a->setShortcutContext(Qt::WidgetShortcut);
		connect(a, SIGNAL(triggered()), this, SLOT(emitViewRowInExternalEditor()));
		a->setId("viewRowExternal");
		actionList << a;
	}
	{
		a = new QFAction(tr("Upravit ve formulari"), this);
		a->setIcon(QIcon(":/libqfgui/images/edit.png"));
		a->setToolTip(tr("Upravit radek ve formulari"));
		a->setShortcutContext(Qt::WidgetShortcut);
		connect(a, SIGNAL(triggered()), this, SLOT(emitEditRowInExternalEditor()));
		a->setId("editRowExternal");
		actionList << a;
	}
	{
		a = new QFAction(QFPixmapCache::icon("icon.sortAsc", ":/libqfgui/images/sort-asc.png"), tr("Sort ascending"), this);
		a->setId("sortAsc");
		a->setCheckable(true);
		//a->setToolTip(tr("Upravit radek v externim editoru"));
		a->setShortcutContext(Qt::WidgetShortcut);
		f_actionGroupsIds[SortActions] << a->id();
		connect(a, SIGNAL(triggered(bool)), this, SLOT(sortAsc(bool)));
		actionList << a;
	}
	{
		a = new QFAction(QFPixmapCache::icon("icon.sortDesc", ":/libqfgui/images/sort-desc.png"), tr("Sort descending"), this);
		a->setId("sortDesc");
		a->setCheckable(true);
		//a->setToolTip(tr("Upravit radek v externim editoru"));
		//a->setShortcutContext(Qt::WidgetShortcut);
		f_actionGroupsIds[SortActions] << a->id();
		connect(a, SIGNAL(triggered(bool)), this, SLOT(sortDesc(bool)));
		actionList << a;
	}
	{
		a = new QFAction(tr("Edit cell content"), this);
		//a->setToolTip(tr("Upravit radek v externim editoru"));
		a->setShortcut(QKeySequence(tr("Ctrl+Shift+T", "Edit cell content")));
		a->setShortcutContext(Qt::WidgetShortcut);
		connect(a, SIGNAL(triggered()), this, SLOT(showCurrentCellText()));
		a->setId("showCurrentCellText");
		f_actionGroupsIds[CellActions] << a->id();
		actionList << a;
	}
	{
		a = new QFAction(tr("Save BLOB"), this);
		//a->setToolTip(tr("Upravit radek v externim editoru"));
		//a->setShortcutContext(Qt::WidgetShortcut);
		connect(a, SIGNAL(triggered()), this, SLOT(saveCurrentCellBlob()));
		a->setId("saveCurrentCellBlob");
		f_actionGroupsIds[BlobActions] << a->id();
		actionList << a;
	}
	{
		a = new QFAction(tr("Load BLOB from file"), this);
		//a->setToolTip(tr("Upravit radek v externim editoru"));
		//a->setShortcutContext(Qt::WidgetShortcut);
		connect(a, SIGNAL(triggered()), this, SLOT(loadCurrentCellBlob()));
		a->setId("loadCurrentCellBlob");
		f_actionGroupsIds[BlobActions] << a->id();
		actionList << a;
	}
	{
		a = new QFAction( this);
		a->setSeparator(true);
		a->setId(QString("sep_%1").arg(a->creationId()));
		actionList << a;
	}
	{
		a = new QFAction(tr("Insert rows statement"), this);
		//a->setToolTip(tr("Upravit radek v externim editoru"));
		//a->setShortcutContext(Qt::WidgetShortcut);
		connect(a, SIGNAL(triggered()), this, SLOT(insertRowsStatement()));
		a->setId("insertRowsStatement");
		f_actionGroupsIds[RowActions] << a->id();
		actionList << a;
	}
	{
		a = new QFAction(tr("Set NULL in selection"), this);
		//a->setToolTip(tr("Upravit radek v externim editoru"));
		a->setShortcut(QKeySequence(tr("Ctrl+Shift+L", "Set NULL in selection")));
		a->setShortcutContext(Qt::WidgetShortcut);
		connect(a, SIGNAL(triggered()), this, SLOT(setNullInSelection()));
		a->setId("setNullInSelection");
		f_actionGroupsIds[SetValueActions] << a->id();
		actionList << a;
	}
	{
		a = new QFAction(tr("Set value in selection"), this);
		a->setShortcut(QKeySequence(tr("Ctrl+Shift+E", "Set value in selection")));
		a->setShortcutContext(Qt::WidgetShortcut);
		connect(a, SIGNAL(triggered()), this, SLOT(setValueInSelection()));
		a->setId("setValueInSelection");
		f_actionGroupsIds[SetValueActions] << a->id();
		actionList << a;
	}
	{
		a = new QFAction(tr("Select"), this);
		a->setId("select");
		f_actionGroupsIds[SelectActions] << a->id();
		actionList << a;
		QMenu *m = new QMenu(this);
		a->setMenu(m);
		{
			a = new QFAction(tr("Select current column"), this);
			a->setShortcutContext(Qt::WidgetShortcut);
			connect(a, SIGNAL(triggered()), this, SLOT(selectCurrentColumn()));
			a->setId("selectCurrentColumn");
			m->addAction(a);
		}
		{
			a = new QFAction(tr("Select current row"), this);
			a->setShortcutContext(Qt::WidgetShortcut);
			connect(a, SIGNAL(triggered()), this, SLOT(selectCurrentRow()));
			a->setId("selectCurrentRow");
		}
		m->addAction(a);
	}
	{
		a = new QFAction(tr("Calculate"), this);
		a->setId("calculate");
		f_actionGroupsIds[CalculateActions] << a->id();
		actionList << a;
		QMenu *m = new QMenu(this);
		a->setMenu(m);
		{
			a = new QFAction(tr("Sum column"), this);
			connect(a, SIGNAL(triggered()), this, SLOT(sumColumn()));
			a->setId("sumColumn");
			m->addAction(a);
		}
		{
			a = new QFAction(tr("Sum selection"), this);
			connect(a, SIGNAL(triggered()), this, SLOT(sumSelection()));
			a->setId("sumSelection");
			m->addAction(a);
		}
	}
	{
		a = new QFAction( this);
		a->setSeparator(true);
		a->setId(QString("sep_%1").arg(a->creationId()));
		actionList << a;
	}
	{
		a = new QFAction(tr("Export"), this);
		a->setId("export");
		f_actionGroupsIds[ExportActions] << a->id();
		actionList << a;
		QMenu *m = new QMenu(this);
		a->setMenu(m);
		{
			a = new QFAction(tr("Report"), this);
			connect(a, SIGNAL(triggered()), this, SLOT(exportReport()));
			a->setId("exportReport");
			actionList << a;
			m->addAction(a);
		}
		{
			a = new QFAction(tr("CSV"), this);
			connect(a, SIGNAL(triggered()), this, SLOT(exportCSV()));
			a->setId("exportCSV");
			actionList << a;
			m->addAction(a);
		}
		{
			a = new QFAction(tr("HTML"), this);
			connect(a, SIGNAL(triggered()), this, SLOT(exportHTML()));
			a->setId("exportHTML");
			actionList << a;
			m->addAction(a);
		}
		{
			a = new QFAction(tr("XML"), this);
			connect(a, SIGNAL(triggered()), this, SLOT(exportXML()));
			a->setId("exportXML");
			actionList << a;
			m->addAction(a);
		}
	}
	{
		a = new QFAction(tr("Import"), this);
		a->setId("import");
		f_actionGroupsIds[ImportActions] << a->id();
		actionList << a;
		QMenu *m = new QMenu(this);
		a->setMenu(m);
		{
			a = new QFAction(tr("CSV"), this);
			connect(a, SIGNAL(triggered()), this, SLOT(importCSV()));
			a->setId("importCSV");
			actionList << a;
			m->addAction(a);
		}
	}
	f_toolBarActions << action("insertRow");
	f_toolBarActions << action("copyRow");
	f_toolBarActions << action("removeSelectedRows");
	f_toolBarActions << action("postRow");
	f_toolBarActions << action("revertRow");
	//f_toolBarActions << action("insertRowExternal");
	f_toolBarActions << action("viewRowExternal");
	f_toolBarActions << action("editRowExternal");
	a = new QFAction(this); a->setSeparator(true);
	f_toolBarActions << a;
	f_toolBarActions << action("reload");
	a = new QFAction(this); a->setSeparator(true);
	f_toolBarActions << a;
	f_toolBarActions << action("sortAsc");
	f_toolBarActions << action("sortDesc");
	
	f_contextMenuActions = standardContextMenuActions();

	{
		foreach(QAction *a, actionList) {
			if(!a->shortcut().isEmpty()) {
				//qfInfo() << "\t inserting action" << a->text() << a->shortcut().toString();
				addAction(a); /// aby chodily shortcuty, musi byt akce pridany widgetu
			}
		}
		//qfTrash() << "\t default actions inserted";
	}
}

QFTableModel* QFTableView::model(bool throw_exc) const throw(QFException)
{
	QFTableModel* ret = qobject_cast<QFTableModel*>(QTableView::model());
	if(!ret && throw_exc) QF_EXCEPTION("Model is NULL.");
	return ret;
}

QFBasicTable* QFTableView::table(bool throw_exc) const  throw(QFException)
{
	QFTableModel *m = model(throw_exc);
	if(m) return m->table();
	return NULL;
}
/*
int QFTableView::sizeHintForRow(int row) const
{
	Q_UNUSED(row);
	//qDebug() << "QFTableView::sizeHintForRow()" << 20;
	return 20;
}
*/
void QFTableView::keyPressEvent(QKeyEvent *e)
{
	qfLogFuncFrame() << "key:" << e->key() << "modifiers:" << e->modifiers();
	if(!e) return;
	if(!model(!Qf::ThrowExc)) {e->ignore(); return;} //{QTableView::keyPressEvent(e); return;}
	horizontalHeader()->setSearchString("");

	bool incremental_search = false;
	bool incremental_search_key_accepted = false;
	//bool modified = (e->modifiers() != Qt::NoModifier && e->modifiers() != Qt::KeypadModifier);
	bool key_enter = (e->key() == Qt::Key_Return && e->modifiers() == 0) || (e->key() == Qt::Key_Enter && e->modifiers() == Qt::KeypadModifier);
	/*
	bool suitable_for_incremental_search =
			(e->modifiers() != Qt::AltModifier || e->modifiers() != Qt::ControlModifier)
			&& ((e->key() >= 0x20 && e->key() < 0x80) || e->key() == Qt::Key_Backspace);
	*/
	/*
	// Ctrl-Enter is a postRow shortcut, nevim proc, porad nefunguje
	if(e->modifiers() == Qt::ControlModifier && key_enter) {
		qfTrash() << "\tCtrl-Enter is a postRow shortcut: IGNORED";
		e->ignore();
		return;
	}
	*/
	if(e->modifiers() == Qt::ControlModifier) {
		if(e->key() == Qt::Key_C) {
			QString s;
			s = model()->data(currentIndex(), Qt::DisplayRole).toString();
			qfTrash() << "\tSetting clipboard:" << s;
			QClipboard *clipboard = QApplication::clipboard();
			clipboard->setText(s);
			e->accept();
			return;
		}
		else if(e->key() == Qt::Key_Enter || e->key() == Qt::Key_Return) {
			qfTrash() << "\tCTRL+ENTER";
			postRow();
			e->accept();
			return;
		}
	}
	else if(key_enter) {
		qfTrash() << "\t ENTER pressed";
	}
	else {
		QFBasicTable::SortDef sd = seekSortDefinition();
		if(sd.isValid() && sd.ascending && seekColumn() >= 0 && (currentIndex().column() == seekColumn() || !currentIndex().isValid())) {
			incremental_search = true;
			QString old_seek_string = seekString;
			if(!currentIndex().isValid()) setCurrentIndex(model()->index(0, seekColumn(), QModelIndex()));
			//qfInfo() << "incremental search currentIndex row:" << currentIndex().row() << "col:" << currentIndex().column();
			/// Pokud je nektery sloupec serazen vzestupne zkusi se provest incremental search,
			/// pak se event dal nepropaguje
			QChar seekChar = QFString(e->text())[0];
			if(seekChar == '\n' || seekChar == '\r') seekChar = QChar();
			qfTrash().noSpace() << "\tincremental search seekChar: '" << QString::number(seekChar.unicode(),16) << "'";// << QString::number('\n',16);
			//bool accept = false;
			if(e->key() == Qt::Key_Backspace) {
				seekString = seekString.slice(0, -1);
				incremental_search_key_accepted = true;
			}
			else if(e->key() == Qt::Key_Escape) {
				seekString = QString();
				incremental_search_key_accepted = true;
			}
			else if(seekChar.isNull()) {
				seekString = QString();
			}
			else {
				seekString += e->text();
				incremental_search_key_accepted = true;
			}
			if(!!seekString && seekString != old_seek_string) {
				horizontalHeader()->setSearchString(seekString);
				search(seekString);
				incremental_search_key_accepted = true;
			}
			/// do not pass this events to parent class
			/*
			if(ignore) {
				qfTrash() << "\tUSED for incremental search";
				e->ignore();
				return;
			}
			*/
		}
	}
	if(incremental_search && incremental_search_key_accepted) {
		qfTrash() << "\tUSED for incremental search";
		e->accept();
		return;
	}
	else {
		seekString = QString();
	}
	bool event_should_be_accepted = false;
	/// nejedna se o inkrementalni vyhledavani, zkusime editaci
	if(state() == EditingState) {
		qfTrash() << "\teditor exists";
		//QModelIndex current = currentIndex();
		//QModelIndex newCurrent;
		/// cursor keys moves selection, check editor data before
		/// some of switched keys shoul be filtered by editor
		qfTrash() << "\tHAVE EDITOR key:" << e->key();
		switch (e->key()) {
			case Qt::Key_Down:
			case Qt::Key_Up:
			case Qt::Key_Left:
			case Qt::Key_Right:
			case Qt::Key_Home:
			case Qt::Key_End:
			case Qt::Key_PageUp:
			case Qt::Key_PageDown:
			case Qt::Key_Tab:
			case Qt::Key_Backtab:
			case Qt::Key_Return:
			case Qt::Key_Enter:
				qfTrash() << "accepting event";
				/// je to trochu jedno, protoze to vypada, ze accept flag, kterej prijde dialogu je ten, jak ho nastavi editor (widget dodany delegatem) ve sve funkci keyPressEvent(...)
				//e->accept();
				event_should_be_accepted = true;
				QFItemDelegate *it = qobject_cast<QFItemDelegate*>(itemDelegate());
				if(it) {
					if(!it->canCloseEditor()) return;
				}
				//qfTrash().color(QFLog::Yellow) << "set focus to table view";
				setFocus(); /// jinak se mi zavre delegat a focus skoci na jinej widget
				break;
		}
	}
	else {
		if(key_enter) {
			///timhle jsem zajistil, ze se editor otevira na enter a ne jen na F2, jak je defaultne v QT
			/// viz: QAbstractItemView::keyPressEvent(...)
			qfTrash() << "\tkey chci otevrit editor, state:" << state();
			if(edit(currentIndex(), EditKeyPressed, e)) {
				qfTrash() << "accepting event";
				e->accept();
			}
			else {
				qfTrash() << "ignoring event";
				e->ignore();
			}
			//qfTrash() << "accepting event";
			//e->accept();
			qfTrash() << "\t exiting after open editor try, event accepted:" << e->isAccepted() << "event:" << e;
			return;
		}
	}
	qfTrash() << "\tcalling parent implementation QTableView::keyPressEvent(e), state:" << state() << "event accepted:" << e->isAccepted();
	QTableView::keyPressEvent(e);
	/// parent implementace muze zmenit accepted() flag eventu
	qfTrash() << "\tcalled parent implementation QTableView::keyPressEvent(e), state:" << state() << "event accepted:" << e->isAccepted();
	if(event_should_be_accepted) e->accept();
}

void QFTableView::mousePressEvent(QMouseEvent * e)
{
	qfLogFuncFrame();

	seekString = QString();
	horizontalHeader()->setSearchString(seekString);
	QPoint pos = e->pos();
	QModelIndex ix = indexAt(pos);
	qfTrash() << "\trow:" << ix.row() << "col:" << ix.column();
	QFItemDelegate *it = qobject_cast<QFItemDelegate*>(itemDelegate());
	if(it) {
		//qfTrash() << "\teditor" << w;
		// pokud existuje editor, je pole rozeditovany a melo by se zkontrolovat
		if(!it->canCloseEditor()) return;
	}

	QTableView::mousePressEvent(e);
}

QModelIndex QFTableView::moveCursor(CursorAction cursorAction, Qt::KeyboardModifiers modifiers)
{
	qfTrash() << QF_FUNC_NAME << cursorAction;
	/*
	QFItemDelegate *it = qobject_cast<QFItemDelegate*>(itemDelegate());
	if(it) {
		QWidget *w = it->currentEditor;
		qfTrash() << "\teditor" << w;
		// pokud existuje editor, je pole rozeditovany a melo by se zkontrolovat
		if(w && !validateEditor(w)) return currentIndex();
	}
	*/
	return QTableView::moveCursor(cursorAction, modifiers);
}

void QFTableView::paintEvent(QPaintEvent * event)
{
	//qfTrash() << QF_FUNC_NAME;
	//if(!event->region().isEmpty())
	//	qfTrash() << "\tregion.boundingRect():" << rect2string(event->region().boundingRect());
	//if(!event->rect().isEmpty())
	//	qfTrash() << "\trect:" << rect2string(event->rect());
	QTableView::paintEvent(event);
}

void QFTableView::updateRow(int row)
{
	if(row < 0) row = currentIndex().row();
	if(row < 0) return;
	QModelIndex ix = model()->index(row, 0);
	QRect r = visualRect(ix);
	// expand rect to whole row
	if(r.isEmpty()) return;
	//qfTrash() << "QFSqlQueryView::updateRow():";
	//qfTrash() << "\twidget geometry:" << rect2string(geometry());
	//qfTrash() << "\twidget width:" << width();
	r.setX(0);
	r.setWidth(width());
	//r = geometry();
	//qfTrash() << "\tupdated rect:" << rect2string(r);
	viewport()->update(r);
	// FIXME update only row, not all header
	verticalHeader()->viewport()->update();
}

void QFTableView::updateDataArea()
{
	qfTrash() << QF_FUNC_NAME;
	viewport()->update();
	//viewport()->update(geometry());
}

void QFTableView::updateAll()
{
	qfTrash() << QF_FUNC_NAME;
	viewport()->update();
	verticalHeader()->viewport()->update();
	horizontalHeader()->viewport()->update();
}
/*
QVariant QFTableView::value(const QModelIndex &_ix) const
{
	QModelIndex ix = _ix;
	if(!ix.isValid()) ix = currentIndex();
	QVariant ret = model()->data(ix);
	return ret;
}
*/
void QFTableView::setModel(QFTableModel *mod)
{
	qfTrash() << QF_FUNC_NAME;
	QFTableModel *prev_mod = model(!Qf::ThrowExc);
	if(prev_mod) {
		/// disconnect previous model
		//disconnect(prev_mod, SIGNAL(allDataChanged()), this, SLOT(reset()));
		//disconnect(prev_mod, SIGNAL(allDataChanged()), this, SLOT(refreshActions()));
	}
	QTableView::setModel(mod);
	if(mod) {
		//qfTrash() << "\tconnecting: connect(mod, SIGNAL(allDataChanged()), this, SLOT(updateAll()));";
		//connect(mod, SIGNAL(allDataChanged()), this, SLOT(reset()));
		//connect(mod, SIGNAL(allDataChanged()), this, SLOT(refreshActions()));
	}
	//setMode(QFSqlQuery::ModeReadOnly);
	resizeColumns();
	refreshActions();
}

/*
QFTableView::Mode QFTableView::setMode(QFTableView::Mode _mode)
{
	qfTrash() << QF_FUNC_NAME;
	QFTableView::Mode old_mode = mode();
	f_mode = _mode;
	QAbstractItemView::EditTriggers et;
	if(mode() == QFTableView::EditMode) {
		et |=QAbstractItemView::AnyKeyPressed
				| QAbstractItemView::EditKeyPressed
				| QAbstractItemView::DoubleClicked;
	}
	setEditTriggers(et);
	//refreshActions();
	return old_mode;
}
*/

void QFTableView::resetAppearence()
{
	qfLogFuncFrame();
	//qfInfo() << QFLog::stackTrace(); 
	//static bool first_scan = true;
	/*
	updateAll();
	if(table(!Qf::ThrowExc) && table()->columnCount()) {
		//int i = model()->columnCount();
		//if(i > 0) i--;
		horizontalHeader()->reset();
		//horizontalHeader()->sectionsInserted(QModelIndex(), 0, 0);
		//i = model()->rowCount();
		//if(i > 0) i--;
		verticalHeader()->reset();
	}
	*/
	resizeColumns();
	if(seekColumn() < 0 && horizontalHeader()) horizontalHeader()->setSortIndicatorShown(false);
	refreshActions();
}

void QFTableView::refreshActions()
{
	qfLogFuncFrame() << "row editor mode:" << rowEditorMode();
	//qfInfo() << QFLog::stackTrace();
	enableAllActions(false);
	if(!model(!Qf::ThrowExc)) return;
	action("calculate")->setEnabled(true);
	action("select")->setEnabled(true);
	action("reload")->setEnabled(true);
	action("resizeColumnsToContents")->setEnabled(true);
	action("showCurrentCellText")->setEnabled(true);
	action("saveCurrentCellBlob")->setEnabled(true);
	action("loadCurrentCellBlob")->setEnabled(true);
	action("insertRowsStatement")->setEnabled(true);
	action("import")->setEnabled(true);
	action("importCSV")->setEnabled(true);
	action("export")->setEnabled(true);
	action("exportReport")->setEnabled(true);
	action("exportCSV")->setEnabled(true);
	action("exportXML")->setEnabled(true);
	action("exportHTML")->setEnabled(true);

	//if(isReadOnly()) return;

	if(rowEditorMode() == RowEditorInline) {
		qfTrash() << "\tINLINE editor";
		action("insertRow")->setVisible(true);
		//action("copyRow")->setVisible(true);
		action("removeSelectedRows")->setVisible(true);
		action("postRow")->setVisible(true);
		action("revertRow")->setVisible(true);

		//action("insertRowExternal")->setVisible(false);
		action("viewRowExternal")->setVisible(false);
		action("editRowExternal")->setVisible(false);
	}
	else if(rowEditorMode() == RowEditorExternal) {
		qfTrash() << "\tEXTERNAL";
		action("insertRow")->setVisible(true);
		//action("copyRow")->setVisible(false);
		action("removeSelectedRows")->setVisible(isRemoveRowActionVisibleInExternalMode());
		action("postRow")->setVisible(false);
		action("revertRow")->setVisible(false);

		//action("insertRowExternal")->setVisible(true);
		action("viewRowExternal")->setVisible(true);
		action("editRowExternal")->setVisible(true);
	}
	else if(rowEditorMode() == RowEditorMixed) {
		qfTrash() << "\tMIXED";
		action("insertRow")->setVisible(true);
		//action("copyRow")->setVisible(false);
		action("removeSelectedRows")->setVisible(true);
		action("postRow")->setVisible(true);
		action("revertRow")->setVisible(true);

		//action("insertRowExternal")->setVisible(true);
		action("viewRowExternal")->setVisible(true);
		action("editRowExternal")->setVisible(true);
	}

	QFTableModel *m = model(!Qf::ThrowExc);
	if(!m) return;

	bool is_insert_rows_allowed = m->isInsertRowsAllowed() && !isReadOnly();
	bool is_edit_rows_allowed = m->isEditRowsAllowed() && !isReadOnly();
	bool is_delete_rows_allowed = m->isDeleteRowsAllowed() && !isReadOnly();
	qfTrash() << "\tinsert allowed:" << is_insert_rows_allowed;
	qfTrash() << "\tdelete allowed:" << is_delete_rows_allowed;
	qfTrash() << "\tedit allowed:" << is_edit_rows_allowed;
	action("viewRowExternal")->setVisible(action("viewRowExternal")->isVisible());
	action("insertRow")->setVisible(is_insert_rows_allowed && action("insertRow")->isVisible());
	action("copyRow")->setVisible(is_insert_rows_allowed && action("copyRow")->isVisible());
	//action("insertRowExternal")->setVisible(is_insert_rows_allowed && action("insertRowExternal")->isVisible());
	action("removeSelectedRows")->setVisible(is_delete_rows_allowed && action("removeSelectedRows")->isVisible());
	action("postRow")->setVisible((is_edit_rows_allowed || is_insert_rows_allowed) && action("postRow")->isVisible());
	action("revertRow")->setVisible(action("postRow")->isVisible() && action("revertRow")->isVisible());
	action("editRowExternal")->setVisible(is_edit_rows_allowed && action("editRowExternal")->isVisible());

	QFBasicTable *t = table(!Qf::ThrowExc);
	if(!t) return;
	const QFBasicTable::Row r = t->row(currentIndex().row(), !Qf::ThrowExc);
	/*
	if(r.isNull()) {
		action("insertRow")->setEnabled(action("insertRow")->isVisible());
		action("insertRowExternal")->setEnabled(action("insertRowExternal")->isVisible());
		return;
	}
	else {
		action("postRow")->setEnabled(action("postRow")->isVisible());
	}
	*/
	//qfTrash() << QF_FUNC_NAME << "valid:" << r.isValid() << "dirty:" << r.isDirty();
	if(r.isDirty()) {
		action("postRow")->setEnabled(true);
		action("revertRow")->setEnabled(true);
	}
	else {
		action("insertRow")->setEnabled(action("insertRow")->isVisible());
		//action("insertRowExternal")->setEnabled(action("insertRowExternal")->isVisible());
		action("copyRow")->setEnabled((action("insertRow")->isEnabled()/* || action("insertRowExternal")->isEnabled()*/)
				&& currentIndex().isValid());
		action("reload")->setEnabled(true);
		action("viewRowExternal")->setEnabled(true);
		action("editRowExternal")->setEnabled(action("editRowExternal")->isVisible());
		action("sortAsc")->setEnabled(true);
		action("sortDesc")->setEnabled(true);
		action("setValueInSelection")->setEnabled(true);
		action("setNullInSelection")->setEnabled(true);
		//QFItemDelegate *itdel = qobject_cast<QFItemDelegate*>(itemDelegate());
		//if(itdel) action("postRow")->setEnabled(itdel->editorOpened());
	}
	action("removeSelectedRows")->setEnabled(action("removeSelectedRows")->isVisible());
	action("revertRow")->setEnabled(action("postRow")->isEnabled());
}

void QFTableView::enableAllActions(bool on)
{
	foreach(QFAction *a, actionList) {
		a->setEnabled(on);
		if(on) a->setVisible(true);
	}
}

QFAction* QFTableView::action(const QString &name, bool throw_exc) const throw(QFException)
{
	return QFUiBuilder::findAction(actionList, name, throw_exc);
}

void QFTableView::closeEditor(QWidget * editor, QAbstractItemDelegate::EndEditHint hint)
{
	qfTrash() << QF_FUNC_NAME << "row" << currentIndex().row();
	QTableView::closeEditor(editor, hint);
	refreshActions();
	updateRow(currentIndex().row());
	/// set focus je tady jednak, aby po zavreni editoru kurzorovou sipkou mi nepresel focus na dalsi widget, ale zustal hezky v tableview.
	//qfTrash().color(QFLog::Yellow) << "set focus to table view";
	setFocus();
	//qfTrash() << "\thasFocus:" << hasFocus();
}

void QFTableView::currentChanged(const QModelIndex& current, const QModelIndex& previous)
{
	qfLogFuncFrame() << "row" << previous.row() << "->" << current.row();
	//emit currentChanging(current, previous);
	//qfTrash().color(QFLog::Yellow) << "set focus to table view";
	if(ignoreCurrentChanged) {
		/// ignoreCurrentChanged funguje jen jednou, tim zabranim zaseknuti funkce currentChanged() pri spatnem pouziti ignoreCurrentChanged.
		ignoreCurrentChanged = false; 
		return;
	}
	setFocus(); /// pokud nekdo neco resi s widgety jako reakci na signal currentChanging(), muze prijit o fokus a prestane chodit kurzorova navigace
	QTableView::currentChanged(current, previous);
	if(current.row() != previous.row() && previous.row() >= 0) {
		qfTrash() << "\tsaving previous row:" << previous.row();
			//QFTableModel *m = model();
			//if(!m) return;
		QFBasicTable *t = table(!Qf::ThrowExc);
		if(!t) return;
		//qfTrash() << "\t" << __LINE__;
		try {
			//qfTrash() << "\t" << __LINE__;
			postRow(previous.row());
		}
		catch(QFException &e) {
			//qfTrash() << "\t" << __LINE__;
			QFDlgException::exec(this, e);
			setCurrentIndex(previous);
		}
		qfTrash() << "\t" << __LINE__;
		updateRow(previous.row());
		updateRow(current.row());
	}
	//qfInfo() << __LINE__;
	refreshActions();
	qfTrash() << "\temitting selected(" << current.row() << "," << current.column() << ")";
	emit selected(current);
	/// na selected() muze prijit table o fokus
	//qfTrash().color(QFLog::Yellow) << "set focus to table view";
	setFocus();
}

QFBasicTable::Row QFTableView::tableRow(int row_no) const
{
	//qfTrash() << QF_FUNC_NAME << "ri:" << ri;
	int ri = row_no;
	if(row_no < 0) ri = currentIndex().row();
	//qfTrash() << "\t->" << ri;
	//QFTableModel *m = model();
	//if(!m) return QFBasicTable::nullRow();
	QFBasicTable *t = table(!Qf::ThrowExc);
	if(!t) return QFBasicTable::Row();
	return t->row(ri, !Qf::ThrowExc);
}

QFBasicTable::Row& QFTableView::tableRowRef(int row_no) throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	int ri = row_no;
	if(row_no < 0) ri = currentIndex().row();
	if(ri < 0) QF_EXCEPTION("current row is < 0");
	QFBasicTable *t = table();
	return t->rowRef(ri);
}

void QFTableView::insertRowInline()
{
	qfLogFuncFrame();
	try {
		//qfInfo() << "\tmodel:" << model();
		QModelIndex ix = currentIndex();
		int ri = model()->rowCount();
		if(ix.isValid()) ri = ix.row() + 1;
		model()->insertRow(ri);
		if(ix.isValid()) setCurrentIndex(ix.sibling(ri, ix.column()));
		else setCurrentIndex(model()->index(ri, 0, QModelIndex()));
	}
	catch(QFException &e) {
		QFDlgException::exec(this, e);
	}
}

void QFTableView::insertRow()
{
	qfLogFuncFrame();
	/// kdyz je factory insert probehne v externim editoru
	QFDataFormDialog *dlg = createExternalEditor(QFDataFormDocument::ModeInsert);
	if(dlg) {
		qfTrash() << "\t external editor created";
		dlg->insertExec();
		delete dlg;
	}
	else {
		if(rowEditorMode() == RowEditorInline) {
			qfTrash() << "\t insert row in mode RowEditorInline";
			/// proved insert radku
			insertRowInline();
		}
		else {
				//QVariant id = selectedRow().value(idColumnName());
			qfTrash() << "\t emit insertRowInExternalEditor()";
			emitProcessRowInExternalEditor(QVariant(), QFDataFormDocument::ModeInsert);
			//emit insertRowInExternalEditor();
		}
	}
	refreshActions();
}

void QFTableView::copyRowInline()
{
	qfLogFuncFrame();
	try {
		QFBasicTable::Row r1 = tableRow();
		if(r1.isNull()) return;
		insertRow();
		QFBasicTable::Row &r2 = tableRowRef();
		for(int i=0; i<r1.fieldCount() && i<r2.fieldCount(); i++) {
			r2.setValue(i, r1.value(i));
		}
		r2.prepareForCopy();
	}
	catch(QFException &e) {
		QFDlgException::exec(this, e);
	}
}

void QFTableView::copyRow()
{
	qfLogFuncFrame();
	//if(isEmitCopyRowSignal()) emit copyRow(currentIndex().row());
	//else
	/// kdyz je factory copy probehne v externim editoru
	int ri = currentIndex().row();
	if(ri >= 0) {
		QFDataFormDialog *dlg = createExternalEditor(QFDataFormDocument::ModeEdit);
		if(dlg) {
			QVariant id = selectedRow().value(idColumnName());
			dlg->copyExec(id);
			delete dlg;
		}
		else {
			if(rowEditorMode() == RowEditorInline) {
				/// proved kopii radku
				copyRowInline();
			}
			else {
				QVariant id = selectedRow().value(idColumnName());
				emitProcessRowInExternalEditor(id, QFDataFormDocument::ModeCopy);
				//emit copyRowInExternalEditor(id);
			}
		}
	}
	refreshActions();
}

void QFTableView::insertRowsStatement()
{
	QStringList sl;
	{
		foreach(int rix, selectedRowsIndexes()) {
			QFBasicTable::Row row = tableRow(rix);
			QFSqlStorageDriverProperties props = qvariant_cast<QFSqlStorageDriverProperties>(row.tableProperties().storageDriverProperties());
			QFSqlConnection conn = props.connection();
			QSqlDriver *drv = conn.driver();
			foreach(QString table_id, props.tableIds()) {
				QString table = conn.fullTableNameToQtDriverTableName(table_id);
				QSqlRecord rec;
				int i = -1;
				foreach(QFSqlField fld, row.fields()) {
					i++;
					if(fld.fullTableName() != table_id) continue;
					QVariant v = row.value(i);
					fld.setValue(v);
					fld.setName(fld.fieldName());
					rec.append(fld);
				}
				if(!rec.isEmpty()) {
					//qfTrash() << "updating table inserts" << table;
					QString s = drv->sqlStatement(QSqlDriver::InsertStatement, table, rec, false);
					sl << s;
				}
			}
		}
	}
	if(!sl.isEmpty()) {
		sl << QString();
		QString s = sl.join(";\n");
		QFDlgTextView dlg(this);
		dlg.exec(s, "row.sql", "DlgExportTable");
	}
}

void QFTableView::removeSelectedRows(QFTableView::UserPrompt ask)
{
	qfLogFuncFrame();
	QList<int> rows_to_delete = selectedRowsIndexes();
	if(rows_to_delete.isEmpty()) return;
	if(rowEditorMode() == RowEditorExternal || rowEditorMode() == RowEditorMixed) {
		emit processRowInExternalEditor(rows_to_delete.first(), QFDataFormDocument::ModeDelete);
	}
	else try {
		clearSelection();
		/// zkontroluj, jestli jsou radky bez mezer
		for(int i=1; i<rows_to_delete.count(); i++) {
			if(rows_to_delete[i] - rows_to_delete[i-1] != 1) {
				QFMessage::information(this, tr("Selection has to be continuous."));
				return;
			}
		}
		if(ask == AskUser) {
			if(rows_to_delete.count() == 1) {
				if(!QFMessage::askYesNo(this, tr("Do you realy want to remove row?"), true)) return;
			}
			else {
				if(!QFMessage::askYesNo(this, tr("Do you realy want to remove all selected rows?"), true)) return;
			}
		}
		QModelIndex ix = currentIndex();
		ignoreCurrentChanged = true; /// na false ho nastavi currentChanged()
		model()->removeRows(rows_to_delete[0], rows_to_delete.count());
		int i = ix.row();
		if(i >= model()->rowCount()) i = model()->rowCount() - 1;
		if(i >= 0) {
			setCurrentIndex(ix.sibling(i, ix.column()));
			//updateRow();
		}
		updateAll();
	}
	catch(QFException &e) {
		QFDlgException::exec(this, e);
	}
	refreshActions();
}

void QFTableView::commitAndCloseCurrentEditor()
{
	//qfTrash() << QF_FUNC_NAME;
	QFItemDelegate *itdel = qobject_cast<QFItemDelegate*>(itemDelegate());
	if(itdel) itdel->commitAndCloseCurrentEditor();
	//setFocusProxy(NULL);
}

bool QFTableView::postRow(int _row_no)
{
	qfTrash() << QF_FUNC_NAME;
	bool ret = false;
	commitAndCloseCurrentEditor();
	QFTableModel *m = model(!Qf::ThrowExc);
	if(!m) return ret;
	
	int row_no = (_row_no < 0)? currentIndex().row(): _row_no;
	if(row_no >= 0) {
		try {
			ret = m->postRow(row_no);//) emit rowPosted(row_no);
		}
		catch(QFException &e) {
			QFDlgException::exec(this, e);
		}
		refreshActions();
		updateRow(currentIndex().row());
	}
	//qfTrash() << "\tODCHAZIM";
	return ret;
}

void QFTableView::revertRow()
{
	qfTrash() << QF_FUNC_NAME;
	QFBasicTable *t = table(!Qf::ThrowExc);
	if(!t) return;
	t->revertRow(currentIndex().row());
	updateRow(currentIndex().row());
	refreshActions();
}

void QFTableView::emitViewRowInExternalEditor()
{
	qfTrash() << QF_FUNC_NAME;
	int ri = currentIndex().row();
	if(ri >= 0) {
		QVariant id = selectedRow().value(idColumnName());
		QFDataFormDialog *dlg = createExternalEditor(QFDataFormDocument::ModeView);
		if(dlg) {
			dlg->viewExec(id);
			delete dlg;
		}
		else {
			emitProcessRowInExternalEditor(id, QFDataFormDocument::ModeView);
			//emit viewRowInExternalEditor(id);
		}
	}
}

void QFTableView::emitEditRowInExternalEditor()
{
	qfTrash() << QF_FUNC_NAME;
	int ri = currentIndex().row();
	if(ri >= 0) {
		QVariant id = selectedRow().value(idColumnName());
		QFDataFormDialog *dlg = createExternalEditor(QFDataFormDocument::ModeEdit);
		if(dlg) {
			dlg->editExec(id);
			delete dlg;
		}
		else {
			qfTrash() << "\temitting editRowInExternalEditor(...)";
			emitProcessRowInExternalEditor(id, QFDataFormDocument::ModeEdit);
		}
	}
}
/*
void QFTableView::emitInsertRowInExternalEditor()
{
	qfTrash() << QF_FUNC_NAME;
	QFDataFormDialog *dlg = createExternalEditor(QFDataFormDocument::ModeInsert);
	if(dlg) {
		dlg->insertExec();
		delete dlg;
	}
	else emit insertRowInExternalEditor();
}
*/
/*
void QFTableView::slotEditRowInExternalEditor()
{
	qfTrash() << QF_FUNC_NAME;
	QFBasicTable::Row r = dataRow();
	try {
		if(externalEditor()) {
			externalEditor()->editExec(r.value(idColumnName()));
		}
	}
	catch(QFException &e) {
		QFDlgException::exec(this, e);
	}
}

void QFTableView::slotInsertRowInExternalEditor()
{
	qfTrash() << QF_FUNC_NAME;
	try {
		if(externalEditor()) {
			externalEditor()->insertExec();
		}
	}
	catch(QFException &e) {
		QFDlgException::exec(this, e);
	}
}
*/
void QFTableView::dataExternalySaved(QFDataFormDocument *doc, int mode)
{
	qfLogFuncFrame() << "mode:" << mode;
	if(!doc) return;
	try {
		if(mode == QFDataFormDocument::ModeInsert) {
			qfTrash() << "\tINSERT";
			QFTableDataFormDocument *tdoc = qobject_cast<QFTableDataFormDocument*>(doc);
			if(tdoc && tdoc->model() == model()) {
				int ri = tdoc->currentModelRowIndex();
				qfTrash() << "\tri:" << ri;
				qfTrash() << "\tmodel->rowCount():" << model()->rowCount();
				model()->reloadRow(ri);
				updateRow(ri);
				setCurrentIndex(model()->index(ri, 0, QModelIndex()));
			}
			else {
				QVariant id = doc->dataId();
				if(id.isValid()) {
					qfTrash() << "\texternaly inserted id:" << id.toString();
					qfTrash() << "\tcurrent row:" << currentIndex().row();
					QModelIndex ix = currentIndex();
					int ri = model()->rowCount();
					qfTrash() << "\tri:" << ri;
					qfTrash() << "\tmodel->rowCount():" << model()->rowCount();
					if(ix.isValid()) ri = ix.row() + 1;
					qfTrash() << "\tri:" << ri;
					model()->insertBasicRow(ri);
					qfTrash() << "\tmodel->rowCount():" << model()->rowCount();
					qfTrash() << "\tix.isValid():" << ix.isValid();
					if(ri >= model()->rowCount()) ri = model()->rowCount() - 1;
					if(ix.isValid()) setCurrentIndex(ix.sibling(ri, ix.column()));
					else setCurrentIndex(model()->index(ri, 0, QModelIndex()));
					ix = currentIndex();
					qfTrash() << "\tcurrent row:" << ix.row();
					if(ix.isValid()) {
						QFBasicTable::Row &r = tableRowRef();
						r.setValue(idColumnName(), id);
						r.setInsert(false);
					}
					bool ok = model()->reloadRow(ri);
					//qfInfo() << "ok:" << ok;
					if(ok) updateRow(ri);
					else {
						/// radek neni videt, treba kvuli tomu, ze vlozena data neodpovidaji where podminkam
						model()->removeBasicRow(ri);
					}
				}
			}
		}
		else if(mode == QFDataFormDocument::ModeEdit) {
			qfTrash() << "\tEDIT";
			QFTableDataFormDocument *tdoc = qobject_cast<QFTableDataFormDocument*>(doc);
			if(tdoc && tdoc->model() == model()) {
				int ri = tdoc->currentModelRowIndex();
				qfTrash() << "\tri:" << ri;
				qfTrash() << "\tmodel->rowCount():" << model()->rowCount();
				updateRow(ri);
			}
			else {
				/// tady vznika problem, kdyz nekdo externe poedituje primarni klic, pak se to nenajde a radek pri reloadu zmizi,
				/// takze pro externi editaci pouzivat ID autoincrement
				int ri = currentIndex().row();
				if(ri >= 0) {
					qfTrash() << "\t reloadRow:" << ri;
					model()->reloadRow(ri);
				}
			}
		}
	}
	//catch(QFReloadSqlRowException &e) {
	/// nevim proc, ale catch na QFReloadSqlRowException mi tady nefunguje, musel jsem pouzit vlastni QFException RTTI
	catch(QFException &e) {
		//e.log();
		/// radek se z nejakeho duvodu nepodarilo reloadnout, reloadni celou tabulku, treba protoze query obsahuje UNION
		/*
		qfTrash() << "\t " << e.type() << typeid(e).name() << "exception:" << e.msg() << (typeid(e) == typeid(QFReloadSqlRowException)) << (typeid(e) == typeid(QFSqlException)) << (typeid(e) == typeid(QFException));
		qfTrash() << "\t " << typeid(QFReloadSqlRowException).name();
		qfTrash() << "\t " << typeid(QFSqlException).name();
		qfTrash() << "\t " << typeid(QFException).name();
		qfTrash() << "\t " << typeid(QFReloadSqlRowException&).name();
		qfTrash() << "\t " << typeid(QFSqlException&).name();
		qfTrash() << "\t " << typeid(QFException&).name();
		QFReloadSqlRowException *e2 = dynamic_cast<QFReloadSqlRowException*>(&e);
		QFSqlException *e3 = dynamic_cast<QFSqlException*>(&e);
		if(e2) {
			qfTrash() << "\t " << "QFReloadSqlRowException !!!";
			reload();
		}
		else if(e3) {
			/// tohle se vytiskne a to i presto, ze vrham QFReloadSqlRowException !!! NECHAPU to mozkem
			qfTrash() << "\t " << "QFSqlException !!!";
		}
		*/
		if(e.type() == "QFReloadSqlRowException") {
			reload();
		}
		else {
			throw;
		}
	}
	catch(...) {
		throw;
	}
}

void QFTableView::dataExternalyDropped(const QVariant &id)
{
	qfLogFuncFrame() << "id:" << id.toString();
	QModelIndex ix = currentIndex();
	QFTableModel *m = model();
	if(m) {
		/// tady je to malinko komplikovany, protoze nevim, jestli je model dokumentu a tabulky sdileny nebo ne
		/// pokud ano, radek uz v nem neni, takze to musim vyresit pomalu a hloupe tak, ze projdu celou tabulku a zkusim najit id
		/// jako malou optimalizaci zkusim nejdriv currentRow.
		int id_fldix = model()->fieldIndex(idColumnName(), !Qf::ThrowExc);
		if(id_fldix >= 0) {
			int ri = currentIndex().row();
			if(ri >= 0) {
				QVariant v = m->value(ri, id_fldix, !Qf::ThrowExc);
				//qfTrash() << "\t found id:" << v.toString();
				if(v.isValid() && v != id) ri = -1;
			}
			if(ri < 0) for(ri=0; ri<m->rowCount(); ri++) {
				QVariant v = m->value(ri, id_fldix, !Qf::ThrowExc);
				//qfTrash() << "\t found id:" << v.toString();
				if(v.isValid() && v == id) {
					break;
				}
			}
			if(ri >= 0 && ri < m->rowCount()) {
				m->removeBasicRow(ri);
				m->reset();
				if(ri >= m->rowCount()) ri = model()->rowCount() - 1;
				if(ri >= 0) {
					ix = m->index(ri, (ix.column() >= 0)? ix.column(): 0);
						//qfInfo() << "ix row:" << ix.row() << "col:" << ix.column();
					setCurrentIndex(ix);
				}
			}
		}
	}
}

void QFTableView::reload()
{
	qfLogFuncFrame();
	qfTrash() << "\tmodel:" << model();
	qfTrash() << "\tsaving section sizes";
	if(horizontalHeader()) horizontalHeader()->saveSectionSizes();
	try {
		QModelIndex ix = currentIndex();
		model()->reload();
		qfTrash() << "\t emitting reloaded()";
		emit reloaded();
		qfTrash() << "\ttable:" << table();
		setCurrentIndex(ix);
		updateDataArea();
	}
	catch(QFException &e) {
		QFDlgException::exec(this, e);
	}
	//refreshActions();
}

QFTableHeaderView* QFTableView::horizontalHeader()
{
	return qobject_cast<QFTableHeaderView*>(QTableView::horizontalHeader());
}

QFTableHeaderView* QFTableView::verticalHeader()
{
	return qobject_cast<QFTableHeaderView*>(QTableView::verticalHeader());
}

void QFTableView::resizeColumns()
{
	qfLogFuncFrame();
	if(horizontalHeader()) horizontalHeader()->resizeSections();
	/// do not save default values of column sizes
	/// this can happen if application aborts during view initialization.
	//persistentDataLoaded = true;
}

void  QFTableView::savePersistentData()
{
	if(!xmlConfigPersistentId().isEmpty()) {
		qfTrash() << QF_FUNC_NAME << xmlConfigPersistentId() << objectName();
		//QFXmlConfigElement el = createPersistentPath();
		// save column sizes
		QFTableHeaderView *h = qobject_cast<QFTableHeaderView*>(horizontalHeader());
		if(h) {
			//QF_ASSERT(h, "horizontal header is not a kind of QFHeaderView");
			h->saveSectionSizes();
			QVariantMap m;
			QMapIterator<QString, QFTableHeaderView::SectionProperty> i(h->sectionProperties);
			while (i.hasNext()) {
				i.next();
				m[i.key()] = i.value().values;
			}
			QString s = QFJson::variantToString(m);
			//qfInfo() << "save sectionProperties:" << s;
			setPersistentValue("horizontalheader/columns/sectionProperties", s);
		}
	}
}

void  QFTableView::loadPersistentData()
{
	if(!xmlConfigPersistentId().isEmpty()) {
		qfLogFuncFrame() << xmlConfigPersistentId();
		//QFXmlConfigElement el = persistentPath();
		QFTableHeaderView *h = qobject_cast<QFTableHeaderView *>(horizontalHeader());
		//QFTableModel *m = model();
		if(h) {
			bool old_config_format = true;
			{
				QString s = persistentValue("horizontalheader/columns/sectionProperties").toString();
				//qfInfo() << "sectionProperties:" << s;
				if(!s.isEmpty()) {
					old_config_format = false;
					QVariantMap m = QFJson::stringToVariant(s).toMap();
					QMapIterator<QString, QVariant> i(m);
					while (i.hasNext()) {
						i.next();
						QString key = i.key();
						if(key == "0" || key.toInt()) continue; /// klice, ktere jsou cisla jsou pozustatky stareho formatu a neni treba je ukladat
						QVariantMap m2 = i.value().toMap();
						h->sectionProperties[key] = QFTableHeaderView::SectionProperty(m2);
					}
				}
			}
			if(old_config_format) {
				/// load column sizes
				{
					QString s = persistentValue("horizontalheader/columns/sizes").toString();
					QStringList sl = s.split(" ");
					for(int i=0; i<sl.count(); i++) {
						QString key = QString::number(i); /// hodnoty ze starefo konf. formatu obsahuji pouze indexy sloupcu, proto se jako klic pro jejich SectionProperty bere index prevedeny na string
						QFTableHeaderView::SectionProperty sp = h->sectionProperties.value(key);
						sp.setSize(sl[i].toInt());
						h->sectionProperties[key] = sp;
					}
				}
				/// load hidden sections
				{
					QString s = persistentValue("horizontalheader/columns/hidden-status").toString();
					QStringList sl = s.split(" ");
					for(int i=0; i<sl.count(); i++) {
						QString key = QString::number(i); /// hodnoty ze starefo konf. formatu obsahuji pouze indexy sloupcu, proto se jako klic pro jejich SectionProperty bere index prevedeny na string
						QFTableHeaderView::SectionProperty sp = h->sectionProperties.value(key);
						sp.setHidden(QFString(sl[i]).toBool());
						h->sectionProperties[key] = sp;
					}
				}
			}
		}
		resetAppearence();
	}
}

QVariant QFTableView::currentRowId() const
{
	QVariant ret;
	QFBasicTable::Row r = selectedRow();
	if(!r.isNull()) ret = r.value(idColumnName());
	return ret;
}

bool QFTableView::edit(const QModelIndex& index, EditTrigger trigger, QEvent* event)
{
	qfLogFuncFrame() << "trigger:" << trigger << "event type:" << ((event)? event->type(): 0) << "readonly:" << isReadOnly() << "editTriggers:" << editTriggers();
	bool ret = false;
	ignoreFocusOutEventWhenOpenExternalEditor = true;

	do {
		if(event && (event->type() == QEvent::MouseButtonPress || event->type() == QEvent::MouseButtonRelease)) {
		/// pokud je mousePressEvent, pak v pripade, ze chci preklopit check u bool hodnoty, musim pustit dal
		}
		else if(trigger && !(trigger & editTriggers())) {
		/// ignoruji triggery, ktere neprijmam
			qfTrash() << "\t trigger ignored";
			ret = false;
			break;
		}
	
		if(trigger == QTableView::DoubleClicked || trigger == QTableView::EditKeyPressed) {
			qfTrash() << "\t emitting activated";
			emit activated(currentIndex());
		//activated_emited = true;
			event->accept();
			ret = true;
		//return ret;
		}

		if(isReadOnly()) {
			qfTrash() << "\t widget RO";
			ret = false;
			break;
		}
	
		{
			QFTableModel *m = qobject_cast<QFTableModel*>(model(!Qf::ThrowExc));
			if(!m || m->isReadOnly()) {
				qfTrash() << "\t model RO";
				ret = false;
				break;
			}
		}
		try {
			if(rowEditorMode() == RowEditorInline) {
				qfTrash() << "\t RowEditorInline";
				ret = QTableView::edit(index, trigger, event);
				qfTrash() << "\t QTableView::edit() returned:" << ret;
				if(ret) refreshActions(); /// bez toho mi nechodi refreshActions pro bool hodnoty, ktere se nedelaji prez ItemDelegate, ale pres Qt::CheckStateRole
			}
			else  if(rowEditorMode() == RowEditorExternal) {
				qfTrash() << "\t RowEditorExternal";
				if(action("editRowExternal")->isVisible()) {
					if(trigger == QTableView::DoubleClicked || trigger == QTableView::EditKeyPressed) {
						emitEditRowInExternalEditor();
						event->accept();
						ret = true;
					}
				}
			}
			else if(rowEditorMode() == RowEditorMixed) {
				qfTrash() << "\t RowEditorMixed";
				ret = QTableView::edit(index, trigger, event);
				if(!ret) {
					if(action("editRowExternal")->isVisible()) {
						if(trigger == QTableView::DoubleClicked || trigger == QTableView::EditKeyPressed) {
							emitEditRowInExternalEditor();
							event->accept();
							ret = true;
						}
					}
				}
			}
		}
		catch(QFException &e) {
			QFDlgException::exec(this, e);
		}
	} while(false);

	ignoreFocusOutEventWhenOpenExternalEditor = false;
	//if(activated_emited) qfInfo() << "event accepted:" << event->isAccepted();
	qfTrash() << "\t return:" << ret;
	return ret;
}

void QFTableView::sort(bool asc, bool on)
{
	qfTrash() << QF_FUNC_NAME;
	if(on) {
		if(asc) { action("sortDesc")->setChecked(false); }
		else { action("sortAsc")->setChecked(false); }
	}
	seekString = QString();
	horizontalHeader()->setSearchString(seekString);
	if(!model()) return;
	QFBasicTable::SortDefList sdl;
	if(!on) {
		//sd.setSortByRowNo();
		horizontalHeader()->setSortIndicatorShown(false);
	}
	else {
		QModelIndexList lst = selectedIndexes();
		QSet<int> col_set;
		foreach(const QModelIndex &ix, lst) {
			int col = ix.column();
			if(col < 0) continue;
			if(col_set.contains(col)) continue;
			col_set << col;
		}
		QList<int> fld_ix_lst;
		foreach(int col, col_set) {
			int fld_ix = model()->fieldIndex(col, !Qf::ThrowExc);
			if(fld_ix < 0) continue;
			fld_ix_lst << fld_ix;
		}
		qSort(fld_ix_lst);

		foreach(int fld_ix, fld_ix_lst) {
			bool cs = false;
			bool ascii7bit = true;
			sdl.append(QFBasicTable::SortDef(fld_ix, asc, cs, ascii7bit));
		}
	}
	sort(sdl);
}

void QFTableView::sortByColumn(int col, bool aditive)
{
	static const char state_nosort = 1;
	static const char state_ascending = 2;
	static const char state_descending = 3;
	static char state = state_nosort;
	qfTrash() << QF_FUNC_NAME << "col:" << col << "aditive:" << aditive;
	seekString = QString();
	horizontalHeader()->setSearchString(seekString);
	if(!model()) return;
	QFBasicTable::SortDefList sdl = sortDefinition();
	//QFBasicTable::SortDef sd_seek = seekSortDefinition();
	int fld_ix = model()->column(col, !Qf::ThrowExc).fieldIndex();
	int sd_ix = -1;
	if(fld_ix >= 0) for(sd_ix=0; sd_ix<sdl.count(); sd_ix++) {
		if(sdl[sd_ix].fieldIndex == fld_ix) break;
	}
	QFBasicTable::SortDef sd;
	if(sd_ix >= 0 && sd_ix < sdl.count()) {
		sd = sdl[sd_ix];
	}
	else sd_ix = -1;
	sd.caseSensitive = false;
	sd.ascii7bit = true;
	qfTrash() << "sd.fieldIndex:" << sd.fieldIndex << "model field index:" << model()->column(col, !Qf::ThrowExc).fieldIndex();
	if(sd.fieldIndex >= 0 && sd.fieldIndex == fld_ix) {
		/// podruhy kliknu na sloupec, kterej ma uz sort
		state++;
		if(state == state_descending) {
			qfTrash() << "\tto DESCENDING";
			sd.ascending = false;
			//sdl << sd;
		}
		else {
			//qfTrash() << "\tto INVALID";
			state = state_nosort;
		}
	}
	else {
		qfTrash() << "\tto ASCENDING";
		sd.fieldIndex = fld_ix;
		sd.ascending = true;
		state = state_ascending;
		//sdl << sd;
	}
	if(state == state_nosort) {
		if(sd_ix >= 0) sdl.removeAt(sd_ix);
		else sdl.clear();
	}
	else {
		if(aditive) {
			if(sd_ix < 0) sdl << sd;
			else sdl[sd_ix] = sd;
		}
		else {
			sdl.clear();
			if(state != state_nosort) sdl << sd;
		}
	}
	if(!sdl.isEmpty()) {
		//Qt::SortOrder order = sd.ascending ? Qt::AscendingOrder : Qt::DescendingOrder;
		QFBasicTable::SortDef &sd0 = sdl[0];
		/// me se zda logictejsi, kdyz sipka pro ascending ukazuje dolu
		Qt::SortOrder arrow_order = sd0.ascending ? Qt::DescendingOrder : Qt::AscendingOrder;
		int col0 = model()->columnIndex(sd0.fieldIndex, !Qf::ThrowExc);
		horizontalHeader()->setSortIndicator(col0, arrow_order);
		horizontalHeader()->setSortIndicatorShown(true);
		/// je to divoky, ale kdyz nebudu volat QTableModel::sort(int, Qt::SortOrder) nebude mi s timhle view fungovat QFilterSortProxyModel.
		//model()->sort(col, sd.ascending? Qt::AscendingOrder: Qt::DescendingOrder);
		model()->sort(sdl);
		if(sd0.ascending) {
			/// priprav selekci pro inkrementalni vyhledavani
			int row = currentIndex().row();
			if(row < 0) row = 0;
			setCurrentIndex(model()->index(row, col0, QModelIndex()));
		}
	}
	else {
		horizontalHeader()->setSortIndicatorShown(false);
		model()->sort(sdl);
		//model()->sort(-1, Qt::AscendingOrder);
	}
}

void QFTableView::sort(const QFBasicTable::SortDefList &sdl)
{
	QFBasicTable *t = table(!Qf::ThrowExc);
	if(t) {
		int col = currentIndex().column();
		t->sort(sdl);
		QModelIndex prev_ix = currentIndex();
		QModelIndex ix = prev_ix;
		if(ix.isValid()) ix = ix.sibling(ix.row(), col);
		else if(!t->isEmpty()) ix = model()->index(0, col, QModelIndex());
		setCurrentIndex(ix);
		/// index se nezmenil, ale po presorteni je pod nim policko
		if(ix == prev_ix) emit selected(ix);
	}
	updateDataArea();
	horizontalHeader()->viewport()->update();
}

int QFTableView::seekColumn() const
{
	//qfTrash() << QF_FUNC_NAME << "mode:" << mode();
	int ret = -1;
	QFBasicTable::SortDef sd = seekSortDefinition();
	if(sd.isValid()) {
		//qfTrash() << "\tchecking fieldix:" << sdl[0].fieldIndex << "ascending:" << sdl[0].ascending;
		if(sd.ascending) {
			int ix = sd.fieldIndex;
			if(table()->isValidField(ix)) {
				int col = model()->columnIndex(ix, !Qf::ThrowExc);
				ret = col;
			}
		}
	}
	//qfTrash() << "\treturning:" << ret;
	return ret;
}

QFBasicTable::SortDef QFTableView::seekSortDefinition() const
{
	qfLogFuncFrame();
	QFBasicTable *t = table(!Qf::ThrowExc);
	if(t) {
		if(t->tableProperties().sortDefinition().count() > 0) {
			//int fldix = model()->column(i).fieldIndex();
			//qfTrash() << "\tfield index:" << fldix;
			return t->tableProperties().sortDefinition()[0];
		}
	}
	return QFBasicTable::SortDef();
}

QFBasicTable::SortDefList QFTableView::sortDefinition() const
{
	qfLogFuncFrame();
	QFBasicTable *t = table(!Qf::ThrowExc);
	if(t) {
		return t->tableProperties().sortDefinition();
	}
	return QFBasicTable::SortDefList();
}

void QFTableView::search(const QVariant &what)
{
	qfLogFuncFrame() << what.toString();
	if(!model()) return;
	int col = seekColumn();
	if(col >= 0) {
		/// neni to moc efektivni, ale zatim hleda vzdycky od zacatku
		QByteArray ba1 = QF7BitTextCodec::toAscii7(what.toString().toLower()).toLower();
		for(int i=0; i<model()->rowCount(); i++) {
			QModelIndex ix = model()->index(i, col, QModelIndex());
			QByteArray ba2 = QF7BitTextCodec::toAscii7(model()->data(ix, Qt::DisplayRole).toString()).toLower();
			if(ba2.startsWith(ba1)) {
				setCurrentIndex(ix);
				break;
			}
		}
		/*
		QModelIndex ix = model()->index(0, col, QModelIndex());
		QFFlags<Qt::MatchFlag> flags;//(Qt::MatchFlags);
		flags << Qt::MatchStartsWith;
		if(seekSortDefinition().caseSensitive) flags << Qt::MatchCaseSensitive;
		//qfTrash() << "\tflags:" << (int)flags;
		QModelIndexList ixl = model()->match(ix, Qt::DisplayRole, what, 1, flags);
		//int row = model()->seek(what);
		qfTrash() << "\tindex is valid:" << ix.isValid() << "row:" << ix.row();
		if(!ixl.isEmpty()) {
			setCurrentIndex(ixl[0]);
		}
		*/
	}
}

QFDataFormDialog* QFTableView::createExternalEditor(QFDataFormDocument::Mode mode)
{
	qfLogFuncFrame();
	QFDataFormDialogWidgetFactory *fact = externalRowEditorFactory(!Qf::ThrowExc);
	if(!fact) return NULL;
	QFDataFormDialogWidget *w = externalRowEditorFactory()->createWidget(mode, this);
	if(w) {
		QFDataFormDialog *dlg = new QFDataFormDialog(this);
		dlg->setDataFormDialogWidget(w);
	//connect(this, SIGNAL(viewRowInExternalEditor(const QVariant&)), this, SLOT(viewExec(const QVariant&)));
	//connect(this, SIGNAL(editRowInExternalEditor(const QVariant&)), this, SLOT(editExec(const QVariant&)));
	//connect(this, SIGNAL(insertRowInExternalEditor()), this, SLOT(insertExec()));
		connect(w, SIGNAL(documentSaved(QFDataFormDocument*, int)), this, SLOT(dataExternalySaved(QFDataFormDocument*, int)));
		connect(w, SIGNAL(documentDropped(const QVariant &)), this, SLOT(dataExternalyDropped(const QVariant &)));
		return dlg;
	}
	return NULL;
}
/*
void QFTableView::setExternalEditor(QFDataFormDialog *frm)
{
	qfTrash() << QF_FUNC_NAME;
	if(f_externalEditor) {
		f_externalEditor->disconnect(this);
		f_externalEditor->dataFormDialogWidget()->disconnect(this);
	}
	f_externalEditor = frm;
	if(f_externalEditor) {
		connect(this, SIGNAL(viewRowInExternalEditor(const QVariant&)), f_externalEditor, SLOT(viewExec(const QVariant&)));
		connect(this, SIGNAL(editRowInExternalEditor(const QVariant&)), f_externalEditor, SLOT(editExec(const QVariant&)));
		connect(this, SIGNAL(insertRowInExternalEditor()), f_externalEditor, SLOT(insertExec()));
		QFDataFormDialogWidget *w = f_externalEditor->dataFormDialogWidget();
		connect(w, SIGNAL(documentSaved(QFDataFormDocument*)), this, SLOT(dataExternalySaved(QFDataFormDocument*)));
		connect(w, SIGNAL(documentDropped(QFDataFormDocument*)), this, SLOT(dataExternalyDropped(QFDataFormDocument*)));
	}
}
*/
void QFTableView::showCurrentCellText()
{
	QString s;
	//qfInfo() << "model()->data(currentIndex(), Qt::EditRole)";
	QVariant v = model()->data(currentIndex(), Qt::EditRole);
	s = v.toString();
	QFDlgTextView dlg(this);
	if(dlg.exec(s, "new.txt", "DlgShowCurrentCellText")) {
		QVariant t = model()->data(currentIndex(), Qf::FieldTypeRole);
		//qfInfo() << QVariant::typeToName(t.type());
		if((model()->flags(currentIndex()) & Qt::ItemIsEditable) || (t.type() == QVariant::ByteArray)) {
			v = dlg.editor()->toPlainText();
			if(t.type() == QVariant::ByteArray) v = QByteArray(v.toString().toUtf8());
			//qfTrash() << QF_FUNC_NAME << "text:" << s;
			model()->setData(currentIndex(), v);
		}
	}
}

void QFTableView::saveCurrentCellBlob()
{
	QVariant v = model()->data(currentIndex(), Qt::EditRole);
	if(v.type() == QVariant::ByteArray) {
		QString fn = QFileDialog::getSaveFileName(this, tr("Save File"));
		if(!fn.isEmpty()) {
			QFile f(fn);
			if(f.open(QIODevice::WriteOnly)) {
				f.write(v.toByteArray());
			}
		}
	}
}

void QFTableView::loadCurrentCellBlob()
{
	qfLogFuncFrame();
	QVariant v = model()->data(currentIndex(), Qt::EditRole);
	qfTrash() << "\t variant type:" << QVariant::typeToName(v.type());
	if(v.type() == QVariant::ByteArray) {
		QString fn = QFileDialog::getOpenFileName(this, tr("Open File"));
		if(!fn.isEmpty()) {
			QFile f(fn);
			if(f.open(QIODevice::ReadOnly)) {
				QByteArray ba =  f.readAll();
				model()->setData(currentIndex(), ba);
			}
		}
	}
}
/*
QFCSVImportDialogWidget * QFTableView::createCSVImportDialogWidget()
{
	return new QFCSVImportDialogWidget();
}
*/
void QFTableView::importCSV()
{
	qfLogFuncFrame();
	try {
		QFCSVImportDialogWidget *w = new QFCSVImportDialogWidget();
		//w->extraOptionsFrame()->hide();
		QFButtonDialog dlg;
		dlg.setXmlConfigPersistentId("QFTableView/importCSV/Dialog");
		dlg.setDialogWidget(w);
		QFCSVImportDialogWidget::ColumnMappingList lst;
		QFBasicTable *t = table();
		{
			foreach(const QFSqlField &fld, t->fields()) {
				lst << QFCSVImportDialogWidget::ColumnMapping(fld.fullName());
			}
		}		
		w->setColumnMapping(lst);
		if(dlg.exec()) {
			QFBasicTable *t2 = w->table();
			lst = w->columnMapping();
			foreach(QFBasicTable::Row r2, t2->rows()) {
					//qfInfo() << r.toString();
				QFBasicTable::Row r = t->appendRow();
				foreach(const QFCSVImportDialogWidget::ColumnMapping& cm, lst) {
					int colno = cm.columnIndex();
					if(colno >= 0) {
						QString colname = cm.columnName();
						r.setValue(cm.columnName(), r2.value(colno));
					}
				}
				r.post();
			}
			reload();
		}
	}
	catch(QFException &e) {
		QFDlgException::exec(e);
	}
}
/*
void QFTableView::exportReport()
{
	/// funkce se vola z eventloop a kdyz dojde k vyjjimce, tak se z toho QT poserou
	QTimer::singleShot(0, this, SLOT(exportReport_helper()));
}
*/
void QFTableView::exportReport()
{
	QFTablePrintDialogWidget *w = new QFTablePrintDialogWidget(model(), this);
	{
		QString pers_id = xmlConfigPersistentId();
		if(!pers_id.isEmpty()) {
			pers_id = QFFileUtils::joinPath(pers_id, "QFTablePrintDialogWidget");
			w->setXmlConfigPersistentId(pers_id);
		}
	}
	/*
	QFBasicTable::TextExportOptions opts;
	{
		QList<int> lst = selectedRowsIndexes();
		int from = lst.first();
		int to = lst.last();
		if(from < to) {
			opts.setFromLine(from);
			opts.setToLine(to);
		}
	}
	w->setExportOptions(opts);
*/
	QFButtonDialog dlg;
	dlg.setXmlConfigPersistentId("QFTablePrintDialogWidget");
	dlg.setDialogWidget(w);
	if(dlg.exec()) {
		QFDomDocument xdoc;
		QFXmlTable t_data = QFXmlTable(xdoc, QString());
		xdoc.appendChild(t_data.element());
		QFXmlTableRow xml_row = t_data.appendRow();

		if(!w->reportTitle().isEmpty()) {
			QFXmlTable xt = QFXmlTable(xdoc, "report");
			xt.setValue("title", w->reportTitle());
			xml_row.appendTable(xt);
		}
		
		{
			QFXmlTable xt = QFXmlTable(xdoc, QString());
			QList<int> model_column_indexes;
			QStringList model_col_names = w->modelColumnNames();
			{
				QStringList fld_full_names;
				QFTableModel *m = model();
				foreach(QString col_name, model_col_names) {
					const QFTableModel::ColumnDefinition &cd = m->column(col_name);
					int ix = m->fieldIndex(col_name, Qf::ThrowExc);
					QFSqlField fld = m->table()->field(ix);
					ix = m->columnIndex(col_name, !Qf::ThrowExc);
					model_column_indexes << ix;
					//qfInfo() << "append model column" << col_name << "type:" << QVariant::typeToName(fld.type()) << "caption:" << cd.caption();
					xt.appendColumn(col_name, fld.type(), cd.caption());
					//fld_full_names << fld.fullName();
				}
			}
			QList<int> table_field_indexes;
			QList<int> xtable_field_indexes;
			//QStringList table_fld_full_names;
			{
				QStringList table_fld_names = w->tableFieldNames();
				int xt_ix = xt.columns().count();
				foreach(QString fld_name, table_fld_names) {
					QFSqlField fld = table()->field(fld_name);
					int t_ix = table()->fields().fieldIndex(fld_name);
					//qfInfo() << "append table column" << ("t_" + fld.fullName()) << "type:" << QVariant::typeToName(fld.type()) << "caption:" << fld.fieldName();
					xt.appendColumn("t_" + fld.fullName(), fld.type(), fld.fieldName());
					table_field_indexes << t_ix;
					xtable_field_indexes << xt_ix++;
				}
			}
			if(model_column_indexes.count() + table_field_indexes.count() > 0) {
				bool selected_rows_only = w->isSelectedRowsOnly();
				QSet<int> selected_row_indexes;
				if(selected_rows_only) selected_row_indexes = QSet<int>::fromList(selectedRowsIndexes());
				for(int ix = 0; ix < model()->rowCount(); ix++) {
					if(selected_rows_only && !selected_row_indexes.contains(ix)) continue;
					QFBasicTable::Row r = tableRow(ix);
					QFXmlTableRow xr = xt.appendRow();
					{
						int model_col_no = 0;
						foreach(QString col_name, model_col_names) { 
							QModelIndex mix = model()->index(ix, model_column_indexes.value(model_col_no++));
							QVariant v = model()->data(mix, Qt::DisplayRole);
							//qfInfo() << fld_name << r.value(fld_name).toString();
							xr.setValue(col_name, v);
						}
					}
					{
						for(int ix1=0; ix1<table_field_indexes.count(); ix1++) {
							//qfInfo() << fld_name << r.value(fld_name).toString();
							xr.setValue(xtable_field_indexes.value(ix1), r.value(table_field_indexes.value(ix1)));
						}
					}
				}
			}
			xml_row.appendTable(xt);
		}
		
		//qfInfo() << xdoc.toString();
		
		QFReportViewWidget *rw = new QFReportViewWidget(NULL);
		rw->setData(xdoc);
		QString report_fn = w->reportFileName();
		rw->setReport(report_fn);
		QFDialog rdlg(this);
		rdlg.setXmlConfigPersistentId("DlgReportView");
		rdlg.setDialogWidget(rw);
		rdlg.exec();
	}
}

void QFTableView::exportCSV()
{
	QFCSVExportDialogWidget *w = new QFCSVExportDialogWidget(table(), this);
	QFBasicTable::TextExportOptions opts;
	{
		QList<int> lst = selectedRowsIndexes();
		int from = lst.first();
		int to = lst.last();
		if(from < to) {
			opts.setFromLine(from);
			opts.setToLine(to);
		}
	}
	w->setExportOptions(opts);
	QFButtonDialog dlg;
	dlg.setXmlConfigPersistentId("CSVExportDialogWidget");
	dlg.setDialogWidget(w);
	if(dlg.exec()) {
		QString s;
		opts = w->exportOptions();
		{
			QTextStream ts(&s);
			table()->exportCSV(ts, w->columnNames().join(","), opts);
		}
		QFDlgTextView dlg(this);
		dlg.exec(s, "data.csv", "DlgExportTable");
	}
}

void QFTableView::exportXML()
{
	QFDomDocument doc;
	QFDomElement el = table()->toXmlTable(doc).element();
	QString s = el.toString();
	QFDlgTextView dlg(this);
	dlg.exec(s, "data.xml", "DlgExportTable");
}

void QFTableView::exportHTML()
{
	QFDomDocument doc = QFHtmlUtils::bodyToXHtmlDocument(QString());
	QFDomElement el = table()->toHtmlElement(doc);
	doc.cd("/body").appendChild(el);
	QString s = doc.toString();
	QFDlgHtmlView dlg(this);
	dlg.exec(this, s, "data.html", "HtmlDlgExportTable");
}

void QFTableView::setRowEditorMode(RowEditorMode _mode)
{
	qfTrash() << QF_FUNC_NAME << _mode;
	fRowEditorMode = _mode;
	refreshActions();
}

void QFTableView::setExternalRowEditorFactory(QFDataFormDialogWidgetFactory *fact)
{
	//SAFE_DELETE(fExternalRowEditorFactory);
	fExternalRowEditorFactory = fact;
}

QFDataFormDialogWidgetFactory* QFTableView::externalRowEditorFactory(bool throw_exc) const throw(QFException)
{
	if(!fExternalRowEditorFactory && throw_exc) QF_EXCEPTION("External row editor factory is NULL");
	return fExternalRowEditorFactory;
}

void QFTableView::focusOutEvent(QFocusEvent *ev)
{
	qfTrash() << QF_FUNC_NAME << state();// << "delegate editor:" << d->currentEditor;
	//qfTrash().color(QFLog::Yellow) << QFLog::stackTrace();
	do {
		if(isIgnoreFocusOutEvent() || ignoreFocusOutEventWhenOpenExternalEditor) {
			qfTrash() << "\tignoreFocusOutEvent";
			break;
		}
		if(state() == EditingState) {
			/// focus out zpusobilo otevreni editoru
			break;
		}
		//break;
		/*
		QFItemDelegate *d = qobject_cast<QFItemDelegate*>(itemDelegate());
		if(d && d->currentEditor) {
			/// focus out zpusobilo otevreni editoru
			qfTrash() << "\tdelegate editor:" << d->currentEditor;
			break;
		}
		*/
		//qfTrash() << "\tPOST";
		/// strati-li tableview focus, uloz prave editovany radek
		/// defaultne to nepouzivam, je z toho vic skody nez uzitku
		postRow();
		/// tohle nejde, protoze mi to vola currentChanged 2x pri kliknuti na sousedni policko
		///currentChanged(currentIndex(), QModelIndex());
	} while(false);
	QTableView::focusOutEvent(ev);
}

void QFTableView::showEvent(QShowEvent *ev)
{
	emit connectRequest(this);
	QTableView::showEvent(ev);
}

int QFTableView::sizeHintForColumn(int col) const
{
	int i = QTableView::sizeHintForColumn(col);
	/// nema cenu automaticky delat sloupec vetsi nez 300
	if(i > 300) i = 200;
	return i;
}

void QFTableView::sumColumn()
{
	QModelIndex ix = currentIndex();
	if(ix.isValid()) {
		int fld_ix = model()->fieldIndex(ix.column(), !Qf::ThrowExc);
		if(fld_ix >= 0) {
			QVariant v = table()->sumValue(fld_ix);
			QFMessage::information(tr("Sum of column values: %1").arg(v.toString()));
		}
	}
}

void QFTableView::sumSelection()
{
	QModelIndexList lst = selectedIndexes();
	double d = 0;
	foreach(const QModelIndex &ix, lst) {
		QVariant v = model()->value(ix);
		d += v.toDouble();
	}
	QFMessage::information(tr("Sum of selected values: %1").arg(d));
}

void QFTableView::selectCurrentColumn()
{
	QModelIndex ix = currentIndex();
	if(ix.isValid()) selectColumn(ix.column());
}

void QFTableView::selectCurrentRow()
{
	QModelIndex ix = currentIndex();
	if(ix.isValid()) selectRow(ix.row());
}

void QFTableView::setValueInSelection()
{
	QString new_val_str;
	QModelIndexList lst = selectedIndexes();
	typedef QList<QModelIndex> SelectedRowIndexes;
	QMap<int, SelectedRowIndexes> selection_rows;
	foreach(const QModelIndex &ix, lst) {
		selection_rows[ix.row()] << ix;
		if(new_val_str.isEmpty()) {
			new_val_str = model()->data(ix, Qt::DisplayRole).toString();
		}
	}
	bool ok;
	new_val_str = QInputDialog::getText(this, tr("Enter value"), tr("new value:"), QLineEdit::Normal, new_val_str, &ok);
	if(!ok) return;
	QList<int> selected_row_indexes = selection_rows.keys();
	foreach(int row_ix, selected_row_indexes) {
		foreach(const QModelIndex &ix, selection_rows.value(row_ix)) {
			model()->setData(ix, new_val_str);
		}
		if(selected_row_indexes.count() > 1) if(!postRow(row_ix)) break;
	}
}

void QFTableView::setNullInSelection()
{
	QModelIndexList lst = selectedIndexes();
	typedef QList<QModelIndex> SelectedRowIndexes;
	QMap<int, SelectedRowIndexes> selection_rows;
	foreach(const QModelIndex &ix, lst) {
		selection_rows[ix.row()] << ix;
	}
	QList<int> selected_row_indexes = selection_rows.keys();
	foreach(int row_ix, selected_row_indexes) {
		foreach(const QModelIndex &ix, selection_rows.value(row_ix)) {
			model()->setData(ix, QVariant());
		}
		if(selected_row_indexes.count() > 1) if(!postRow(row_ix)) break;
	}
}

void QFTableView::setReadOnly(bool ro)
{
	qfLogFuncFrame() << ro;
	f_readOnly = ro;
	refreshActions();
}

void QFTableView::setCopyRowActionVisible(bool b)
{
	action("copyRow")->setVisible(b);
}

bool QFTableView::isCopyRowActionVisible() const
{
	return action("copyRow")->isVisible();
}

void QFTableView::setInsertRowActionVisible(bool b)
{
	action("insertRow")->setVisible(b);
}

bool QFTableView::isInsertRowActionVisible() const
{
	return action("insertRow")->isVisible();
}

QList<QAction*> QFTableView::contextMenuActionsForGroups(int action_groups)
{
	qfLogFuncFrame();
	QList<QAction*> alist;
	QList<int> grps;
	grps << SizeActions << SortActions << ViewActions << RowActions << BlobActions << SetValueActions << CellActions << SelectActions << CalculateActions << ExportActions << ImportActions;
	int cnt = 0;
	foreach(int grp, grps) {
		if(action_groups & grp) {
			QStringList sl = f_actionGroupsIds.value(grp);
			if(!sl.isEmpty() && (cnt++ > 0)) {
				if(!f_actionGroupsSeparatorActions.contains(grp)) {
					QAction *a = new QAction(this);
					a->setSeparator(true);
					f_actionGroupsSeparatorActions[grp] = a;
				}
				alist << f_actionGroupsSeparatorActions.value(grp);
				//qfInfo() << f_actionGroupsIds.value(SeparatorActions).value(0) << action(f_actionGroupsIds.value(SeparatorActions).value(0));
			}
			//qfInfo() << sl.join(",");
			foreach(QString s, sl) {
				QFAction *a = action(s);
				alist << a;
				//qfInfo() << s;
			}
		}
	}
	return alist;
}

void QFTableView::contextMenuEvent(QContextMenuEvent * e)
{
	qfLogFuncFrame();
	QMenu::exec(contextMenuActions(), viewport()->mapToGlobal(e->pos()));
}

QList< int > QFTableView::selectedRowsIndexes() const
{
	QModelIndexList lst = selectedIndexes();
	QSet<int> set;
	foreach(const QModelIndex &ix, lst) {
		if(ix.row() >= 0) set << ix.row();
	}
	QList<int> ret = set.toList();
	qSort(ret);
	return ret;
}

void QFTableView::emitProcessRowInExternalEditor(const QVariant & row_id, int mode)
{
	emit processRowInExternalEditor(row_id, mode);
	if(mode == QFDataFormDocument::ModeInsert) emit insertRowInExternalEditor();
	else if(mode == QFDataFormDocument::ModeView) emit viewRowInExternalEditor(row_id);
	else if(mode == QFDataFormDocument::ModeEdit) emit editRowInExternalEditor(row_id);
	else if(mode == QFDataFormDocument::ModeCopy) emit copyRowInExternalEditor(row_id);
}
