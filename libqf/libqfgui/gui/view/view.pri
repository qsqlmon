INCLUDEPATH += $$PWD

RESOURCES +=   \
    $$PWD/view.qrc
#message("gui/view included")

HEADERS +=   \
    $$PWD/qftableview.h   \
    $$PWD/qftreeview.h   \
    $$PWD/qfheaderview.h   \
    $$PWD/qfitemdelegate.h   \
	$$PWD/qftableviewtoolbar.h   \
	$$PWD/qfitemdelegatebase.h  \

SOURCES +=   \
    $$PWD/qftableview.cpp   \
    $$PWD/qftreeview.cpp   \
    $$PWD/qfheaderview.cpp   \
    $$PWD/qfitemdelegate.cpp   \
	$$PWD/qftableviewtoolbar.cpp   \
	$$PWD/qfitemdelegatebase.cpp  \


