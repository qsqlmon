//
// C++ Interface: qftableview
//
// Description:
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef QFTABLEVIEW_H
#define QFTABLEVIEW_H

#include <QTableView>

#include <qfguiglobal.h>
#include <qfclassfield.h>
#include <qfpart.h>
#include <qfbasictable.h>
#include <qftablemodel.h>
#include <qfdataformdocument.h>
#include <qfxmlconfigpersistenter.h>

class QFTableModel;
class QFTableHeaderView;
class QFDataFormDialog;
class QFCSVImportDialogWidget;
class QFDataFormDialogWidgetFactory;

/**
 @author Fanda Vacek
 */
class QFGUI_DECL_EXPORT QFTableView : public QTableView, public QFXmlConfigPersistenter
{
		Q_OBJECT;
		Q_PROPERTY(bool readOnly READ isReadOnly WRITE setReadOnly);
		Q_PROPERTY(QFTableView::RowEditorMode rowEditorMode READ rowEditorMode WRITE setRowEditorMode);
		/// ktery column obsahuje id zaznamu v tabulce
		Q_PROPERTY(QString idColumnName READ idColumnName WRITE setIdColumnName);
		/// ktery column obsahuje caption zaznamu v tabulce
		Q_PROPERTY(QString captionColumnName READ captionColumnName WRITE setCaptionColumnName);
		Q_PROPERTY(bool ignoreFocusOutEvent READ isIgnoreFocusOutEvent WRITE setIgnoreFocusOutEvent);
		/// pokud je true, emituje na copyRow signal, jinak vola copyRow() virtualni funkci
		//Q_PROPERTY(bool emitCopyRowSignal READ isEmitCopyRowSignal WRITE setEmitCopyRowSignal);
		Q_PROPERTY(bool copyRowActionVisible READ isCopyRowActionVisible WRITE setCopyRowActionVisible);
		Q_ENUMS(RowEditorMode);

		friend class QFTableViewWidget;
		//friend class QFTableViewToolBar;

		QF_FIELD_RW(QString, i, setI, dColumnName);
		QF_FIELD_RW(QString, c, setC, aptionColumnName);
		/// pokud ma tabulka externi editor, nema smysl volat post pro focusOutEvent
		QF_FIELD_RW(bool, is, set, IgnoreFocusOutEvent);
		//QF_FIELD_RW(bool, is, set, EmitCopyRowSignal);
		QF_FIELD_RW(bool, is, set, RemoveRowActionVisibleInExternalMode);
		friend class QFItemDelegate;
	public:
		//enum EditMode {ModeView = 0, ModeEdit, ModeInsert};
		enum RowEditorMode {RowEditorInline, RowEditorExternal, RowEditorMixed};
		enum UserPrompt {AskUser, DoNotAskUser};
	protected:
		/// int je neco z enum ActionGroups
		QMap<int, QList<QString> > f_actionGroupsIds;
		/// obsahuje dynamicky tvorene separatory skupin akci pro kontextove nabidky
		QMap<int, QAction*> f_actionGroupsSeparatorActions;
		bool f_readOnly;
		RowEditorMode fRowEditorMode;
		/// pokud se vyvola externi editor, nema smysl volat post pro focusOutEvent
		bool ignoreFocusOutEventWhenOpenExternalEditor;
		//! pri mazani radku, se chce mazany radek nejprve ulozit, protoze pred vymazanim se posune currentIndex() na platny radek.
		bool ignoreCurrentChanged;
	public:
		void setCopyRowActionVisible(bool b = true);
		bool isCopyRowActionVisible() const;
		void setInsertRowActionVisible(bool b = true);
		bool isInsertRowActionVisible() const;
		
		//void setRemoveRowActionVisibleInExternalMode(bool b = true);
		//bool isRemoveRowActionVisibleInExternalMode() const;
		
		void setReadOnly(bool ro = true);
		virtual bool isReadOnly() const {return f_readOnly;}
		
		RowEditorMode rowEditorMode() const {return fRowEditorMode;}
		void setRowEditorMode(RowEditorMode _mode);
	protected:
		QFDataFormDialogWidgetFactory *fExternalRowEditorFactory;
	public:
		/// Object does not take ownership of factory.
		void setExternalRowEditorFactory(QFDataFormDialogWidgetFactory *fact);
		QFDataFormDialogWidgetFactory* externalRowEditorFactory(bool throw_exc = Qf::ThrowExc) const throw(QFException);
	protected:
		QFDataFormDialog* createExternalEditor(QFDataFormDocument::Mode mode);
		/*
		void setExternalEditor(QFDataFormDialog *frm);
		*/
	protected:
		//virtual int sizeHintForRow(int row) const;
		virtual QModelIndex moveCursor(CursorAction cursorAction, Qt::KeyboardModifiers modifiers);
		virtual void keyPressEvent(QKeyEvent *e);
		virtual void mousePressEvent(QMouseEvent * e);
		virtual void paintEvent(QPaintEvent * event);
		virtual void currentChanged(const QModelIndex& current, const QModelIndex& previous);
		virtual void focusOutEvent(QFocusEvent *ev);
		virtual void showEvent(QShowEvent *ev);
		virtual int sizeHintForColumn(int column) const;
		virtual void contextMenuEvent(QContextMenuEvent *e);
	protected slots:
		void commitAndCloseCurrentEditor();
	signals:
		void connectRequest(QFTableView *sender);
		void copyRow(int ri);

		void reloaded();
		//void currentChanging(const QModelIndex& current, const QModelIndex& previous);
		void selected(const QModelIndex& current);
		/// doubleclick nebo enter
		void activated(const QModelIndex& current);
		//void rowExternalyDropped(const QModelIndex& new_current_index);
	public:
		void loadPersistentData();
		void savePersistentData();
	public slots:
		/// Tyto sloty skutecne patri do view a ne do modelu, protoze externi editor je vlastne delegat pro konkretni radek,
		/// view pripadnou zmenu oznami modelu a ten ji pak propaguje do ostatnich pripadnych views pro tento model.
		/// model totiz nevi napr. index radku, ktery se edituje, takze by ho musel vyhledat podle ID dokumentu.
		/// kazdopadne by asi stalo za uvahu napsat stejnou dvojici slotu primo pro medel a situace, kdy to nevadi a resit editaci v externim editoru na urovni modelu.
		/// @param mode je QFDataFormDocument::Edit nebo QFDataFormDocument::ModeInsert
		virtual void dataExternalySaved(QFDataFormDocument *doc, int mode);
		virtual void dataExternalyDropped(const QVariant &id);
		//! select first row that starts with \a what. Case insensitive.
		virtual void search(const QVariant &what);
		// If \a inserted_id is valid, the external editor has inserted row to the table.
		//void externalEditorClosed(ExternalEditorResult result, const QVariant &inserted_id = QVariant());
		/**
		* calls update viewport with rect clipping row \a row.
		* @param row if lower than 0 current row is updated.
		*/
		void updateRow(int row = -1);
		/**
		*  update data area of table
		*/
		void updateDataArea();
		void updateAll();
		//virtual bool edit(const QModelIndex & index, EditTrigger trigger, QEvent * event);
		//virtual void currentChanged(const QModelIndex & current, const QModelIndex & previous);

		//void reloadRow(int i = -1) throw
		
		void sortByColumn(int column, bool aditive);
		void sortAsc(bool on) {sort(true, on);}
		void sortDesc(bool on) {sort(false, on);}
	protected:
		void sort(bool asc, bool on);
		void sort(const QFBasicTable::SortDefList &sdl);
	protected:
		QFString seekString;
	public:
		//! Return column which is first in sort list or -1. Column has to have an ascending sort order.
		int seekColumn() const;
		//! Returns seekcolumn() sort definition.
		QFBasicTable::SortDef seekSortDefinition() const;
		QFBasicTable::SortDefList sortDefinition() const;
	public:
		//! Connect also models signal \a allDataChanged() to view slots \a refreshActions() and \a updateAll() .
		virtual void setModel(QFTableModel *mod);
		QFTableModel* model(bool throw_exc = Qf::ThrowExc) const throw(QFException);
		QFBasicTable* table(bool throw_exc = Qf::ThrowExc) const  throw(QFException);

		// pokud je ix invalid, vraci value na currentIndex()
		//QVariant value(const QModelIndex &ix = QModelIndex()) const;
		QFBasicTable::Row selectedRow() const {return tableRow();}
		//! If \a row_no < 0 row_no = current row.
		QFBasicTable::Row tableRow(int row_no = -1) const;
	protected:
		//! If \a row_no < 0 row_no = current row.
		QFBasicTable::Row& tableRowRef(int row_no = -1) throw(QFException);
	public:
		//! Default implementation returns value of column id in current row.
		virtual QVariant currentRowId() const;
		QList<int> selectedRowsIndexes() const;

		QFTableHeaderView* horizontalHeader();
		QFTableHeaderView* verticalHeader();
	protected:
		QFPart::ActionList actionList;
		QList<QAction*> f_contextMenuActions;
		QList<QFAction*> f_toolBarActions;
		void createActions();
	public:
		virtual QFAction* action(const QString &name, bool throw_exc = Qf::ThrowExc) const throw(QFException);
		virtual void enableAllActions(bool on = true);
		//void enableAction(const QString &name, bool on = true);
		virtual QList<QFAction*> toolBarActions() const {return f_toolBarActions;}
	protected:
		virtual void closeEditor(QWidget * editor, QAbstractItemDelegate::EndEditHint hint);
	public slots:
		//! Called when model is reseted.
		virtual void resetAppearence();
		virtual void refreshActions();
		//! reselect last queried data
		virtual void reload();
		//! insert row after current row
		virtual void insertRow();
		/// vlozi radek do tabulky a modelu
		virtual void insertRowInline();
		virtual void copyRowInline();
		virtual void copyRow();
		virtual void removeSelectedRows(UserPrompt ask = AskUser);
		//! @param row_no pokud je < 0 postne se currentRow.
		virtual bool postRow(int row_no = -1);
		//! discard all the row data changes.
		virtual void revertRow();
		//! resize columns to predefined widths or to fit content.
		void resizeColumns();
		//void resizeColumnsToContents();
		void resetState() {setState(QAbstractItemView::NoState);}

		void selectCurrentColumn();
		void selectCurrentRow();
		
		void showCurrentCellText();
		void saveCurrentCellBlob();
		void loadCurrentCellBlob();
		
		void setValueInSelection();
		void setNullInSelection();
		void insertRowsStatement();

		void sumColumn();
		void sumSelection();

		virtual void importCSV();
		
		void exportCSV();
		void exportXML();
		void exportHTML();
		virtual void exportReport();
	protected:
		//void exportReport_helper();
		//virtual QFCSVImportDialogWidget* createCSVImportDialogWidget();
	protected:
		virtual bool edit(const QModelIndex& index, EditTrigger trigger, QEvent* event);
		/*
	protected slots:
		//! Defaultni implemantace slotu pro externi editaci, pripoji se volanim funkce \a setExternalEditor() .
		virtual void slotViewRowInExternalEditor(const QVariant &row_id);
		virtual void slotEditRowInExternalEditor(const QVariant &row_id);
		virtual void slotInsertRowInExternalEditor();
		*/
	public:
		enum ActionGroups {ViewActions = 1, BlobActions = 2, SetValueActions = 4, RowActions = 8, CellActions = 16, SizeActions = 32,
			CalculateActions = 64, ExportActions = 128, ImportActions = 256, SortActions = 512, SelectActions = 1024, AllActions = 65535};
		QList<QAction*> contextMenuActionsForGroups(int action_groups = AllActions);
		QList<QAction*> standardContextMenuActions() { return contextMenuActionsForGroups(AllActions & ~(SetValueActions | BlobActions)); }
		QList<QAction*> contextMenuActions() { return f_contextMenuActions; }
		void setContextMenuActions(QList<QAction*> lst) { f_contextMenuActions = lst; }
	protected:
		void emitProcessRowInExternalEditor(const QVariant &row_id, int mode);
	signals:
		/// tento signal je vysilan vzdy
		void processRowInExternalEditor(const QVariant &row_id, int mode);
		/// tento signal je vysilan po signalu processRowInExternalEditor, pokud je mode ModeView
		void viewRowInExternalEditor(const QVariant &row_id);
		/// tento signal je vysilan po signalu processRowInExternalEditor, pokud je mode ModeEdit
		void editRowInExternalEditor(const QVariant &row_id);
		/// tento signal je vysilan po signalu processRowInExternalEditor, pokud je mode ModeCopy
		void copyRowInExternalEditor(const QVariant &row_id);
		/// tento signal je vysilan po signalu processRowInExternalEditor, pokud je mode ModeInsert
		void insertRowInExternalEditor();

	private slots:
		// pomocne funkce ktere nastavi signalu xxxRowInExternalEditor() parametr \a row .
		virtual void emitViewRowInExternalEditor();
		virtual void emitEditRowInExternalEditor();
		//void emitInsertRowInExternalEditor();
	public:
		QFTableView(QWidget *parent = 0);
		~QFTableView();
};


#endif
