//
// C++ Interface: qftreeview
//
// Description:
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef QFTREEVIEW_H
#define QFTREEVIEW_H

#include <QTreeView>

#include <qfguiglobal.h>

/**
@author Fanda Vacek
*/
class QFGUI_DECL_EXPORT QFTreeView : public QTreeView
{
	Q_OBJECT;
	public:
		//! @param parent If it is not valid index tree is expanded from root.
		//! @param set_expanded If it is false tree is colapsed.
		void setExpandedRecursively(const QModelIndex &parent = QModelIndex(), bool set_expanded = true);
	public slots:
		virtual void reset();
		void update();
	signals:
		void selected(const QModelIndex& current);
		//void editRequest(const QModelIndex& current);
	protected:
		virtual void currentChanged(const QModelIndex& current, const QModelIndex& previous);
		// Function called before view tries to expand node.
		//virtual bool canExpand(const QModelIndex &ix) {Q_UNUSED(ix); return true;}

		virtual void mouseDoubleClickEvent(QMouseEvent *event);
		virtual void keyPressEvent(QKeyEvent *event);
	protected:
		virtual bool edit(const QModelIndex& index, EditTrigger trigger, QEvent* event);
	public:
		QFTreeView(QWidget *parent = NULL);
		~QFTreeView();
};

#endif
