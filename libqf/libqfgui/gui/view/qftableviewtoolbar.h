
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFTABLEVIEWTOOLBAR_H
#define QFTABLEVIEWTOOLBAR_H

#include <qfguiglobal.h>
#include <qftableview.h>
#include <qfwidgettoolbarbase.h>


//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFTableViewToolBar : public QFWidgetToolBarBase
{
	Q_OBJECT;
	protected:
		//QPointer<QFTableView> tableView;
	public slots:
		/// Kdyz QFTableView emituje signal connectRequest(), vytvori si buttony z akci TableView a odpoji se od signalu.
		virtual void connectTableView(QFTableView *sender);
	public:
		QFTableViewToolBar(QWidget *parent = NULL);
		virtual ~QFTableViewToolBar();
};
   
#endif // QFTABLEVIEWTOOLBAR_H

