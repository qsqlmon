
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfitemdelegatebase.h"

#include <qf.h>
#include <qdatetimeedit.h>

#include <QLineEdit>
#include <QSpinBox>
#include <QHelpEvent>
#include <QToolTip>

#include <float.h>
#include <limits.h>

#include <qflogcust.h>

QFItemDelegateBase::QFItemDelegateBase(QObject *parent)
	: QItemDelegate(parent)
{
}

QFItemDelegateBase::~QFItemDelegateBase()
{
}

QWidget* QFItemDelegateBase::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	qfLogFuncFrame();
	QWidget *ret = NULL;
	if(!index.isValid()) return ret;
	QVariant val = index.data(Qt::EditRole);
	QVariant::Type t = static_cast<QVariant::Type>(val.userType());
	qfTrash() << "\t datatype:" << QVariant::typeToName(t);
	if(t == QVariant::Date) {
		//qfInfo() << "date";
		QLineEdit *ed = new QLineEdit(parent);
		//QFDateEdit *ret = new QFDateEdit(parent);
		ed->setFrame(false);
		ret = ed;
	}
	else if(t == QVariant::Time) {
		QLineEdit *ed = new QLineEdit(parent);
		ed->setFrame(false);
		ret = ed;
	}
	else if(t == QVariant::Bool) {
		qfTrash() << "\t bool";
		ret = QItemDelegate::createEditor(parent, option, index);
#if 0
		QCheckBox *ed = new QCheckBox(parent);
		ed->setText(QString());
		ed->setTristate(false);
		//ed->setFrame(false);
		ret = ed;
#endif
	}
	else if(t == QVariant::Double) {
		/// double spinbox je nekdy fakt nesikovnej
		QLineEdit *ed = new QLineEdit(parent);
		ed->setFrame(false);
		ret = ed;
	}
	else ret = QItemDelegate::createEditor(parent, option, index);
	if(QSpinBox *ed = qobject_cast<QSpinBox*>(ret)) {
		ed->setRange(INT_MIN, INT_MAX);
	}
	else if(QDoubleSpinBox *ed = qobject_cast<QDoubleSpinBox*>(ret)) {
		ed->setRange(-DBL_MAX, DBL_MAX);
	}
	//currentEditor = ret;
	return ret;
}

void QFItemDelegateBase::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	qfLogFuncFrame();
	if(!index.isValid()) return;
	bool call_parent_impl = true;
	
	QVariant val = index.data(Qt::EditRole);
	QVariant::Type t = static_cast<QVariant::Type>(val.userType());
	qfTrash() << "\t datatype:" << QVariant::typeToName(t);
	if(t == QVariant::Date) {
		QLineEdit *ed = qobject_cast<QLineEdit*>(editor);
		if(ed) {
			call_parent_impl = false;
			QDate d = val.toDate();
			QString s = d.toString(Qf::defaultDateFormatStr);
			ed->setText(s);
		}
	}
	else if(t == QVariant::Time) {
		QLineEdit *ed = qobject_cast<QLineEdit*>(editor);
		if(ed) {
			call_parent_impl = false;
			QTime t = val.toTime();
			QString s = t.toString(Qf::defaultTimeFormatStr);
			ed->setText(s);
		}
	}
	if(call_parent_impl) QItemDelegate::setEditorData(editor, index);
}

bool QFItemDelegateBase::helpEvent(QHelpEvent * event, QAbstractItemView * view, const QStyleOptionViewItem & option, const QModelIndex & index)
{
	//qfLogFuncFrame();
	if (!event || !view) return false;
	if(event->type() == QEvent::ToolTip)	{
		QString tooltip = index.data(Qt::ToolTipRole).toString();
		//qfTrash().noSpace() << "\t tooltip: '" << tooltip << "'";
		if(!tooltip.isEmpty()) {
			QString s = option.fontMetrics.elidedText(index.data(Qt::DisplayRole).toString(), option.textElideMode, option.rect.width());
			//qfTrash().noSpace() << "\t elided: '" << s << "'";
			if(s != tooltip) {
				QItemDelegate::helpEvent(event, view, option, index);
				//QToolTip::showText(he->globalPos(), tooltip.toString(), view);
				return true;
			}
		}
		QToolTip::hideText();
		return true;
	}
	return QItemDelegate::helpEvent(event, view, option, index);
}
