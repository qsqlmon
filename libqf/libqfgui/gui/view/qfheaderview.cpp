//
// C++ Implementation: qfheaderview
//
// Description:
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "qfheaderview.h"

#include <qf.h>
#include <qftableview.h>
#include <qflogcust.h>
#include <qfmessage.h>

#include <QToolTip>
#include <QPalette>
#include <QMenu>
#include <QContextMenuEvent>
#include <QLabel>
#include <QTimer>

//===========================================================
//                          QFHeaderView
//===========================================================
/*
void QFHeaderView::resizeSectionsToContents()
{
	resizeSections(QHeaderView::ResizeToContents);
}
*/
//===========================================================
//                          QFTableHeaderView
//===========================================================
QFTableHeaderView::QFTableHeaderView(Qt::Orientation orientation, QWidget *parent)
	: QFHeaderView(orientation, parent)
{
	if(orientation == Qt::Vertical) setDefaultSectionSize((int)(fontMetrics().lineSpacing() * 1.3)); // 20
	else if(orientation == Qt::Horizontal) {
		lblSearchString = new QLabel(this);
		//qfInfo() << "lbl created";
		QPalette palette;
		palette.setColor(QPalette::Active, QPalette::Window, QColor(255, 255, 127));
		lblSearchString->setPalette(palette);
		lblSearchString->setFrameShape(QFrame::StyledPanel);
		lblSearchString->setAutoFillBackground(true);
		lblSearchString->hide();
		//lblSortString->setText("123456");
		//lblSortString->move(20, 20);
	}
	//connect(this, SIGNAL(sectionResized(int, int, int)), this, SLOT(sizeChanged_helper(int, int, int)));
	connect(this, SIGNAL(sectionClicked(int)), this, SLOT(sectionClicked_helper(int)));
	//connect(this, SIGNAL(sectionCountChanged(int, int)), this, SLOT(sectionCountChanged_debug(int, int )));
}

QFTableHeaderView::~QFTableHeaderView()
{
}

QSize QFTableHeaderView::sizeHint() const
{
	QSize ret = QHeaderView::sizeHint();
	//if(orientation() == Qt::Horizontal) ret = QSize(20, 25); // funguje y rozmer, x ignoruje
	//else if(orientation() == Qt::Vertical) ret = QSize(50, 20); // funguje x rozmer, y ignoruje
	//qDebug() << "QHeaderView::sizeHint(): orientation:" << ((orientation() == Qt::Horizontal)? "horizontal": "vertical") << "return:" << ret;
	return ret;
}

void QFTableHeaderView::mouseReleaseEvent(QMouseEvent * e)
{
	//Q_UNUSED(e);
	//qDebug() << "QFTableHeaderView::mousePressEvent()" << e;
	recentMouseReleaseModifiers = e->modifiers();
	/*
	if(orientation() == Qt::Horizontal) {
		if(e->button() == Qt::LeftButton) {
			int col = logicalIndexAt(e->x(), e->y());
			if(e->modifiers() == Qt::ShiftModifier) {
				emit sortRequest(col, true);
			}
			else {
				emit sortRequest(col, false);
			}
			e->accept();
			return;
		}
	}
	*/
	QHeaderView::mouseReleaseEvent(e);
}

void QFTableHeaderView::sectionClicked_helper(int logical_index)
{
	emit sortRequest(logical_index, recentMouseReleaseModifiers == Qt::ShiftModifier);
}

void QFTableHeaderView::contextMenuEvent(QContextMenuEvent *e)
{
	//qDebug() << "QFTableHeaderView::contextMenuEvent()" << e;
	qfTrash() << "contextMenuEvent()";
	if(orientation() == Qt::Horizontal) {
		QMenu menu(this);
		menu.setTitle(tr("Column menu"));
		//QAction *act_hide_column = menu.addAction(tr("Hide column"));
		QMenu *mn_visible_columns = menu.addMenu(tr("Visible columns"));
		QList<QAction*> visibility_actions;
		for(int i=0; i<count(); i++) {
			QString col_name = model()->headerData(i, orientation(), Qf::ColumnSqlIdRole).toString();
			QAction *a = mn_visible_columns->addAction(tr(qPrintable(col_name), "visible columns context menu item"));
			a->setCheckable(true);
			a->setChecked(!isSectionHidden(i));
			visibility_actions << a;
		}
		QAction *act_column_type = menu.addAction(tr("column data type"));
#if defined QT_DEBUG
		QAction *act_column_size = menu.addAction(tr("column size"));
#endif
		if(QFTableView *tv = tableView()) {
			QMenu *m = menu.addMenu(tr("Column"));
			m->addActions(tv->contextMenuActionsForGroups(QFTableView::SortActions | QFTableView::CalculateActions));
		}
		QAction *a = menu.exec(viewport()->mapToGlobal(e->pos()));
		for(int i=0; i<count(); i++) {
			if(a == visibility_actions[i]) {
				setSectionHidden(i, !a->isChecked());
				//model()->setHeaderData(i, orientation(), !a->isChecked(), Qf::AddColumnToBenListRole);
				//QFTableModel *m = qobject_cast<QFTableModel*>(model());
				if(tableView() && a->isChecked()) QTimer::singleShot(0, tableView(), SLOT(reload()));
			}
		}
		/*
		if(a == act_hide_column) {
			hideSection(logicalIndexAt(e->pos()));
		}
		else if(a == act_show_all_columns) {
			for(int i=0; i<count(); i++) showSection(i);
		}
		*/
		if(a == act_column_type) {
			QVariant::Type type = model()->headerData(logicalIndexAt(e->pos()), Qt::Horizontal, Qf::FieldTypeRole).type();
			QFMessage::information(this, QVariant::typeToName(type));
		}
#if defined QT_DEBUG
		if(a == act_column_size) {
			QFMessage::information(this, QString::number(sectionSize(logicalIndexAt(e->pos()))));
		}
#endif
	}
}

// tohle musim implementovat, aby mi chodil tooltip na header
// v 4.0.1 nechodi, protoze
/*
QModelIndex QHeaderView::indexAt(const QPoint &) const
{
	return QModelIndex();
}
*/
bool QFTableHeaderView::viewportEvent(QEvent *event)
{
	//qfTrash() << QF_FUNC_NAME;
	/*
	This was needed to display tooltip in Qt ver. < 4.1.0
	switch (event->type()) {
		case QEvent::ToolTip:
		{
			//qfTrash() << "\tTOOL TIP:" << this->metaObject()->className();
			if(!isActiveWindow())
				break;
			QHelpEvent *he = static_cast<QHelpEvent*>(event);
			if (!he)
				break;
			int section = logicalIndexAt(he->pos());
			//qfTrash() << "\tsection:" << section;
			if(section >= 0) {
				QString tooltip = model()->headerData(section, orientation(), Qt::ToolTipRole).toString();
				QToolTip::showText(he->globalPos(), tooltip, this);
			}
			return true;
		}
		default:
			break;
	}
	*/
	return QHeaderView::viewportEvent(event);
}

QFTableView* QFTableHeaderView::tableView()
{
	QFTableView *ret = qfFindParent<QFTableView*>(this, !Qf::ThrowExc);
	return ret;
}
/*
void QFTableHeaderView::sizeChanged_helper(int logicalIndex, int old_size, int new_size)
{
	//qfTrash() << QF_FUNC_NAME << "index:" << logicalIndex << "old size:" << oldSize << "new size:" << newSize;
	Q_UNUSED(old_size);
	if(logicalIndex < 0) return;
	QAbstractItemModel *m = model();
	if(m) {
		QString col_name = m->headerData(logicalIndex, orientation(), Qf::ColumnSqlIdRole).toString();
		sectionProperties[col_name].setSize(new_size);
	}
}
*/
void QFTableHeaderView::resizeSections()
{
	qfLogFuncFrame() << "section count:" << count();
	//sectionResizedByLibrary = true;
	QAbstractItemModel *m = model();
	if(m) for(int i=0; i<count() && i<model()->columnCount(); i++) {
		SectionProperty sp;
		QString col_name = m->headerData(i, orientation(), Qf::ColumnSqlIdRole).toString();
		/// nejdriv se pokus najit sloupec podle jmena
		sp = sectionProperties.value(col_name);
		if(sp.isEmpty()) {
			/// zkus, jestli to neni stary format konfigurace
			QString s = QString::number(i);
			sp = sectionProperties.value(s);
		}
		int w = sp.size();
		qfTrash() << "\t sectionProperties w:" << w;
		if(w <= 0) w = m->headerData(i, orientation(), Qf::HeaderSectionInitialSizeRole).toInt();
		qfTrash() << "\t headerData sizeHint w:" << w;
		if(w <= 0) {
			if(orientation() == Qt::Horizontal) {
				bool from_content = false;
				QFTableView *tv = tableView();
				if(tv) {
					QFBasicTable *t = tv->table(!Qf::ThrowExc);
					if(t->columnCount() && t->rowCount()) {
						//qfTrash() << "\t col count:" << t->columnCount() << "row count:" << t->rowCount();
						//qfTrash() << "\t calling sectionSizeFromContents(), new size:" << sectionSize(i);
						tv->resizeColumnToContents(i);
						from_content = true;
					}
				}
				if(!from_content) {
					w = sectionSizeFromContents(i).width();
				}
			}
			else w = sectionSizeFromContents(i).height();
		}
		if(w > 0) {
			qfTrash() << "\tresize column" << i << "to w:" << w;
			resizeSection(i, w);
		}
		setSectionHidden(i, sp.hidden());
	}
	//for(int i=0; i<count() && i<model()->columnCount() && i<sectionProperties.count(); i++) {
	//	setSectionHidden(i, sectionProperties[i].hidden);
	//}
}

void QFTableHeaderView::saveSectionSizes()
{
	qfTrash() << QF_FUNC_NAME << "model:" << model();
	QAbstractItemModel *m = model();
	if(m) for(int i=0; i<count() && i<model()->columnCount(); i++) {
		QString col_name = m->headerData(i, orientation(), Qf::ColumnSqlIdRole).toString();
		if(!col_name.isEmpty()) {
			SectionProperty sp;
			sp.setSize(sectionSize(i));
			sp.setHidden(isSectionHidden(i));
			sectionProperties[col_name] = sp;
		}
	}
}

void QFTableHeaderView::setSearchString(const QString &s)
{
	if(s.isEmpty()) {
		lblSearchString->hide();
	}
	else {
		lblSearchString->setText(s);
		lblSearchString->move(sectionViewportPosition(sortIndicatorSection()), 0);
		lblSearchString->show();
	}
}

void QFTableHeaderView::reset()
{
	QAbstractItemModel *m = model();
	qfTrash() << QF_FUNC_NAME << m;
	if(m) qfTrash() << "\tmodel column cnt:" << m->columnCount(QModelIndex());
	// lip to zatim neumim,
	// v soucasny implementaci  QHeaderView::sectionsInserted(const QModelIndex &parent, int logicalFirst, int logicalLast)
	// je parametr logicalLast ignorovan.
	//sectionsInserted(QModelIndex(), 0, 0);
	QHeaderView::reset();
	initializeSections(); /// @todo tohle by meli volat trollove, ne ja neni to ani dokumnetovana funkce
	qfTrash() << "\tsection cnt:" << count();
}

void QFTableHeaderView::sectionCountChanged_debug(int old_cnt, int new_cnt)
{
	qfTrash() << QF_FUNC_NAME << "old:" << old_cnt << "new:" << new_cnt;
}

void QFTableHeaderView::setSectionHidden(int logical_index, bool b)
{
	if(model()) model()->setHeaderData(logical_index, orientation(), b, Qf::AddColumnToBenListRole);
	QHeaderView::setSectionHidden(logical_index, b);
}
/*
void QFTableHeaderView::resizeSectionToContents(int logical_index)
{
	/// moc to nefunguje
	QSize sz = sectionSizeFromContents(logical_index);
	if(orientation() == Qt::Horizontal) {
		qfInfo() << "new size:" << sz.width();
		resizeSection(logical_index, sz.width());
	}
	else if(orientation() == Qt::Vertical) resizeSection(logical_index, sz.height());
}
*/
//===========================================================
//                          QFTreeHeaderView
//===========================================================
QFTreeHeaderView::QFTreeHeaderView(QWidget * parent)
	: QFHeaderView(Qt::Horizontal, parent)
{
}

