
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qftableviewtoolbar.h"

#include <qfaction.h>

#include <qflogcust.h>

//=================================================================
//                                   QFTableViewToolBar
//=================================================================
QFTableViewToolBar::QFTableViewToolBar(QWidget *parent)
	: QFWidgetToolBarBase(parent)
{
}

QFTableViewToolBar::~QFTableViewToolBar()
{
}
		
void QFTableViewToolBar::connectTableView(QFTableView *table_view)
{
	if(table_view) {
		addActions(table_view->toolBarActions());
		disconnect(table_view, 0, this, SLOT(connectTableView(QFTableView *)));
	}
}

