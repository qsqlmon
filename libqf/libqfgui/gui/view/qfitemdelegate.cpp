//
// C++ Implementation: qfitemdelegate
//
// Description:
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include<QObject>
#include<QLineEdit>
#include<QKeyEvent>

#include<qftableview.h>

#include "qfitemdelegate.h"

#include <qflogcust.h>

QFItemDelegate::QFItemDelegate(QObject *parent)//, QAbstractItemView *_view)
	: QFItemDelegateBase(parent)//, f_view(_view)
{
}

QFItemDelegate::~QFItemDelegate()
{
}

QWidget* QFItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	qfTrash() << QF_FUNC_NAME;
	QWidget *w = QFItemDelegateBase::createEditor(parent, option, index);
	const_cast<QFItemDelegate*>(this)->currentEditor = w;
	if(w) w->installEventFilter(const_cast<QFItemDelegate *>(this));
	return w;
}

void QFItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	//qfTrash() << QF_FUNC_NAME;
	//qfTrash() << "\t" << editor->metaObject()->className();
	QItemDelegate::setEditorData(editor, index);
	if(QLineEdit *ed = qobject_cast<QLineEdit*>(editor)) {
		//QVariant v = index.model()->data(index, QAbstractItemModel::DisplayRole);
		ed->selectAll();
	}
	/*

	QLineEditBox *spinBox = static_cast<QSpinBox*>(editor);
	spinBox->setValue(value);
	*/
}
/*
QAbstractItemView* QFItemDelegate::view() const
{
	return f_view;
}
*/
void QFItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	// HACK
	// tuhle fintu pouzivam, abych mohl libovolne barvit bunky v tabulkach a jejich barvu vracet pomoci
	// model()->data() resp. model()->headerData()
	// na to ale potrebuju v modelu vedet neco o stavu view, ktery se zrovna prekresluje.
	// Jinymi slovy, barv bunky nemusi zaviset jen na stavu modelu, ale taky na stavu View a timhle
	// zpusobem se model dozvi pro ktery ze to View data jsou.
	/*
	if(!index.isValid()) return;
	QAbstractItemModel *model = const_cast<QAbstractItemModel*>(index.model());
	if(view() && model) {
		model->setProperty("currentViewAddr", reinterpret_cast<uint>(view()));
	}
	*/
	//qfTrash() << QF_FUNC_NAME;
	QItemDelegate::paint(painter, option, index);
}
/*
void QFItemDelegate::drawDisplay(QPainter *painter, const QStyleOptionViewItem &option,
								const QRect &rect, const QString &text) const
{
	qfTrash() << QF_FUNC_NAME;
	QString s = text;
	s = QVariant().toString();
	if(s.isNull()) s = QFSql::nullString();
	QItemDelegate::drawDisplay(painter, option, rect, s);

}
*/
bool QFItemDelegate::eventFilter(QObject *object, QEvent *event)
{
	//qfTrash() << QF_FUNC_NAME << "key:" << static_cast<QKeyEvent *>(event)->key();
	bool commit = false;
	if(event->type() == QEvent::KeyPress) {
		qfTrash() << QF_FUNC_NAME << "key:" << static_cast<QKeyEvent *>(event)->key();
		switch (static_cast<QKeyEvent *>(event)->key()) {
			case Qt::Key_Tab:
			case Qt::Key_Backtab:
			case Qt::Key_Enter:
			case Qt::Key_Return:
				commit = true;
				break;
			case Qt::Key_Escape:
			default:
				break;
		}
		if(commit) {
			qfTrash() << "\tCOMMIT";
			if(!canCloseEditor()) return true;
		}
	}
	//qfTrash() << "\treturn FALSE";
	return QItemDelegate::eventFilter(object, event);
}

bool QFItemDelegate::canCloseEditor()
{
	return true;
}

void QFItemDelegate::commitAndCloseCurrentEditor()
{
	qfTrash() << QF_FUNC_NAME << currentEditor;//QFLog::stackTrace();
	if(currentEditor) {
		qfTrash() << "\tcurrent editor" << currentEditor;
		if(canCloseEditor()) {
			qfTrash() << "\temitting";
			emit commitData(currentEditor);
			emit closeEditor(currentEditor);
		}
	}
}

