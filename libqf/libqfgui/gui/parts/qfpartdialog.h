
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFPARTDIALOG_H
#define QFPARTDIALOG_H

#include <qfguiglobal.h>
#include <qfexception.h>

#include <QDialog>

class QFPart;

//! Dialog that can adopt a QFPart.
class QFGUI_DECL_EXPORT QFPartDialog : public QDialog
{
	Q_OBJECT
	protected:
		QFPart *fPart;
	public:
		QFPartDialog(QWidget *parent = 0, Qt::WFlags f = 0);
		virtual ~QFPartDialog();

		void setPart(QFPart *part);
		virtual QFPart* part(bool throw_exc = true) const throw(QFException);
	public slots:
		void close(int result);
};
   
#endif // QFPARTDIALOG_H

