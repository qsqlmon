// MainWindow.cpp: implementation of the MainWindow class.
//
//////////////////////////////////////////////////////////////////////
#include <QDir>
#include <QMessageBox>
#include <QWidget>
#include <QPushButton>

#include <qfpartmanager.h>
#include <qfpart.h>
#include <qftoolbar.h>
#include <qfpartcentralwidget.h>
#include <qfmdiarea.h>
#include <qfmdisubwindow.h>
#include <qfxmlkeyvals.h>

#ifdef QT_SQL_LIB
#include <qfsqlcatalog.h>
#endif

#include "qfmainwindow.h"

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

QFMainWindow::QFMainWindow(QWidget *parent, Qt::WFlags flags, QFMainWindow::CentralWidgetStyle style)
	: QMainWindow(parent, flags), fPartManager(NULL)
{
	qfTrash() << QF_FUNC_NAME;
	switch(style) {
		case CentralWidgetStyleTabbed:
			fPartCentralWidget = new QFPartTabbedCentralWidget(this);
			break;
		case CentralWidgetStyleStack:
			fPartCentralWidget = new QFPartStackedCentralWidget(this);
			break;
		default:
			fPartCentralWidget = new QFPartTabbedCentralWidget(this);
			break;
	}
	setCentralWidget(partCentralWidget());
}

QFMainWindow::~QFMainWindow()
{
	qfTrash() << QF_FUNC_NAME;
	//#ifdef QT_SQL_LIB
	//QFSqlCatalog::cleanupCatalog();
	//#endif
	savePersistentData();
}

QFPartManager& QFMainWindow::partManager()
{
	//qfTrash() << "###########" << QF_FUNC_NAME;
	if(!fPartManager) fPartManager = new QFPartManager(this);
	return *fPartManager;
}

void  QFMainWindow::savePersistentData()
{
	if(!xmlConfigPersistentId().isEmpty()) {
		qfLogFuncFrame() << xmlConfigPersistentId();
		/// save window size
		qfTrash() << "\t pos:" << QFXmlKeyVals::variantToString(pos());
		setPersistentValue("window/pos", pos());
		setPersistentValue("window/size", size());
	}
}

void  QFMainWindow::loadPersistentData()
{
	if(!xmlConfigPersistentId().isEmpty()) {
		qfLogFuncFrame()  << xmlConfigPersistentId();
		/// load window size
		QVariant v = persistentValue("window/pos");
		qfTrash() << "\t pos:" << QFXmlKeyVals::variantToString(v);
		if(v.type() == QVariant::Point) {
			qfTrash() << "\t pos before move:" << QFXmlKeyVals::variantToString(pos());
			move(v.toPoint());
			qfTrash() << "\t pos after move:" << QFXmlKeyVals::variantToString(pos());
		}
		v = persistentValue("window/size");
		if(v.type() == QVariant::Size) {
			resize(v.toSize());
		}
		qfTrash() << "\t pos after resize:" << QFXmlKeyVals::variantToString(pos());
	}
}

void QFMainWindow::help_aboutQt()
{
	QString s = tr("<p><b>Qt %1</b></p>"
			"<p><a href=\"http://www.trolltech.com/qt/\">www.trolltech.com/qt/</a><p>").arg(QT_VERSION_STR);
	QMessageBox mb(this);
	mb.setWindowTitle(tr("About Qt"));
	mb.setText(s);
	QPixmap pm(":/libqfgui/images/qt-logo.png");
	mb.setIconPixmap(pm);
	mb.addButton(QMessageBox::Ok);
	mb.exec();
	//QApplication::aboutQt();
}

void QFMainWindow::useMdiArea(QFMdiArea * area)
{
	connect(area, SIGNAL(subWindowActiveStateChanged(QFMdiSubWindow *, bool )), this, SLOT(mdiAreaSubWindowActiveStateChanged(QFMdiSubWindow *, bool )));
}

void QFMainWindow::mdiAreaSubWindowActiveStateChanged(QFMdiSubWindow * w, bool currently_active)
{
	qfLogFuncFrame() << w << "currently_active:" << currently_active;
	const QFPart::ToolBarList &lst = w->toolBars();
	foreach(QFToolBar *tb, lst) {
		qfTrash() << "\t" << (currently_active? "ADD": "REMOVE") << "toolbar:" << tb;
		if(currently_active) {
			addToolBar(tb);
			tb->show();
		}
		else removeToolBar(tb);
	}
}

