// QFPartCentralWidget.cpp: implementation of the QFPartCentralWidget class.
//
//////////////////////////////////////////////////////////////////////

#include "qfpartcentralwidget.h"

//#include <qftabwidget.h>
#include <qfpart.h>
#include <qfpartwidget.h>

#include <QVBoxLayout>
#include <QStackedWidget>

#include <qflogcust.h>

//=======================================================
//                                        QFPartCentralWidget 
//=======================================================

QFPartCentralWidget::QFPartCentralWidget(QWidget *pParent)
	: QWidget(pParent)
{
}

QFPartCentralWidget::~QFPartCentralWidget()
{
}
/*				
void QFPartCentralWidget::setActivePart(QFPart *activated_part)
{
	qfTrash() << QF_FUNC_NAME;
	if(!activated_part) return;
	setActiveWidget(activated_part->centralWidget());
}

void QFPartCentralWidget::removePart(QFPart *_part)
{
	qfTrash() << QF_FUNC_NAME;
	if(!_part) return;
	removeWidget(_part->centralWidget());
}
*/
//=======================================================
//                                        QFPartTabbedCentralWidget 
//=======================================================

QFPartTabbedCentralWidget::QFPartTabbedCentralWidget(QWidget *pParent)
	: QFPartCentralWidget(pParent)
{
	fWidget = new QTabWidget(this);
	fWidget->setTabPosition(QTabWidget::North);
	QVBoxLayout *l = new QVBoxLayout(this);
	l->setSpacing(3);
	l->setMargin(3);
	l->addWidget(widget());
	connect(fWidget, SIGNAL(currentChanged(int)), this, SLOT(currentChanged(int)));
}

void QFPartTabbedCentralWidget::addPart(QFPart *_part)
{
	qfTrash() << QF_FUNC_NAME;
	QFPartWidget *w = _part->partWidget();
	if(!w) return;
	qfTrash() << "\tclassName:" << w->metaObject()->className();
	widget()->addTab(w, _part->caption());
} 

void QFPartTabbedCentralWidget::removePart(QFPart *_part)
{
	QFPartWidget *w = _part->partWidget();
	if(!w) return;
	qfTrash() << QF_FUNC_NAME << "  className:" << w->metaObject()->className();
	int ix = widget()->indexOf(w);
	qfTrash() << "\t  ix:" << ix << "widget parent  className:" << w->parent()->metaObject()->className();
	if(ix >= 0) {
		widget()->removeTab(ix);
		//SAFE_DELETE(w);
	}
}

QFPart* QFPartTabbedCentralWidget::activePart()
{
	QFPartWidget *w = qobject_cast<QFPartWidget*>(widget()->currentWidget());
	if(!w) return NULL;
	return w->part();
}

void QFPartTabbedCentralWidget::setActivePart(QFPart *_part)
{
	QFPartWidget *w = _part->partWidget();
	if(!w) return;
	int ix = widget()->indexOf(w);
	if(ix >= 0) widget()->setCurrentIndex(ix);
}
		
void QFPartTabbedCentralWidget::currentChanged(int pageno)
{
	static int active_page = -1;
	//qfTrash() << QF_FUNC_NAME;
	QFPartWidget *w = qobject_cast<QFPartWidget*>(widget()->widget(pageno));
	if(w && pageno != active_page) {
		// QT bohuzel ve verzi 4.0.1 emituji signal currentChanged() pro TabbedWidget
		// 2x, takze si to musim takhle filtrovat.
		active_page = pageno;
		//qfTrash() << "\temitting activated pageno:" << pageno;
		QFPart *p = w->part();
		if(p) emit partActivated(p);
		//emit w->part()->emitActivated();
	}
}

//=======================================================
//                                        QFPartStackedCentralWidget 
//=======================================================

QFPartStackedCentralWidget::QFPartStackedCentralWidget(QWidget *pParent)
	: QFPartCentralWidget(pParent)
{
	fWidget = new QStackedWidget(this);
	QVBoxLayout *l = new QVBoxLayout(this);
	l->setSpacing(3);
	l->setMargin(3);
	l->addWidget(widget());
	//connect(fWidget, SIGNAL(currentChanged(int)), this, SLOT(currentChanged(int)));
}

void QFPartStackedCentralWidget::addPart(QFPart *_part)
{
	qfTrash() << QF_FUNC_NAME;
	QFPartWidget *w = _part->partWidget();
	if(!w) return;
	w->setCaptionText(_part->caption());
	w->setCaptionIcon(_part->icon());
	qfTrash() << "\twidget:" << w << "className:" << w->metaObject()->className();
	widget()->addWidget(w);
}

void QFPartStackedCentralWidget::removePart(QFPart *_part)
{
	QFPartWidget *w = _part->partWidget();
	if(!w) return;
	widget()->removeWidget(w);
}

QFPart* QFPartStackedCentralWidget::activePart()
{
	QFPartWidget *w = qobject_cast<QFPartWidget*>(widget()->currentWidget());
	if(!w) return NULL;
	return w->part();
}

void QFPartStackedCentralWidget::setActivePart(QFPart *_part)
{
	QFPartWidget *w = _part->partWidget();
	if(!w) return;
	qfTrash() << QF_FUNC_NAME << "part:" << _part << "widget:" << w;
	widget()->setCurrentWidget(w);
}

