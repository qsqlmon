#include <QToolBar>
#include <QDockWidget>
#include <QDebug>

#include <qfassert.h>
#include <qfmainwindow.h>
#include <qfaction.h>
#include <qfpart.h>
#include <qftoolbar.h>
#include <qfdockwidget.h>
#include <qfpartcentralwidget.h>
#include <qfuibuilder.h>

#include "qfpartmanager.h"

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

QFPartManager::QFPartManager(QFMainWindow	*parent)
	: QObject(parent)
{
	qfTrash() << "***************" << QF_FUNC_NAME;
	//registeredPartList << NULL;
	fActivePartIndex = -1;
	connect(this, SIGNAL(activePartChanged(QFPart*, QFPart*)), mainWindow()->partCentralWidget(), SLOT(setActivePart(QFPart*)));
	connect(mainWindow()->partCentralWidget(), SIGNAL(partActivated(QFPart*)), this, SLOT(setActivePart(QFPart*)));
}

QFPartManager ::~QFPartManager ()
{

}

QFMainWindow* QFPartManager::mainWindow()
{
	QFMainWindow *w = qobject_cast<QFMainWindow*>(parent());
	QF_ASSERT(w, "parent is not QFMainWindow type");
	return w;
}

QFPart* QFPartManager::registerPart(const QString &part_name, bool throw_exc) throw(QFException)
{
	Q_UNUSED(part_name);
	Q_UNUSED(throw_exc);
	QF_EXCEPTION(" Not Implemented Yet.");
	return NULL;
}

void QFPartManager::registerPart(QFPart *_part)
{
	QF_ASSERT(_part != NULL, "part is NULL");
	qfTrash() << QF_FUNC_NAME << "  className:" << _part->metaObject()->className();
	qfTrash() << "\tregistered parts cnt:" << registeredParts().size();
	int ix = registeredPartList.indexOf(_part);
	if(ix >= 0){
		qfWarning() << tr("Part 0x%1 '%2' allready registered.").arg((ulong)_part, 0, 16).arg((_part)? _part->partName(): "NULL");
		return;
	}
	qfInfo().noSpace() << "Registering part: " << _part->partName() << " '" << _part->caption() << "'";
	registeredPartList.append(_part);
	emit partRegistered(_part);
}

void QFPartManager::installPart(QFPart *_part) throw(QFException)
{
	QF_ASSERT(_part != NULL, "part is NULL");
	qfTrash() << QF_FUNC_NAME << "  className:" << _part->metaObject()->className();
	if(installedParts().indexOf(_part) >= 0) {
		qfWarning() << "part" << _part->partName() << "allready installed.";
	}
	//connect(_part, SIGNAL(activated(QFPart*)), this, SLOT(setActivePart(QFPart*)));
	//qfTrash() << __LINE__;
	_part->buildUi();
	//_part->connectActions(this);
	foreach(QFToolBar *t, _part->toolBars()) {
		t->setVisible(false);
		Qt::ToolBarArea a = t->preferredArea();
		if(!a) a = Qt::TopToolBarArea;
		mainWindow()->addToolBar(a, t);
	}
	foreach(QFDockWidget *w, _part->dockWidgets()) {
		w->setVisible(false);
		Qt::DockWidgetArea a = w->preferredArea();
		if(!a) a = Qt::TopDockWidgetArea;
		mainWindow()->addDockWidget(a, w);
	}
	/// zakaz vsechny akce, povoli se, az bude part aktivovana
	foreach(QFAction *a, _part->actionList()) a->setEnabled(false);
	//_part->updateMenuBar(mainWindow()->menuBar());
	if(_part->partWidget()) mainWindow()->partCentralWidget()->addPart(_part);
	qfTrash() << "  adding part" << _part->partName() << _part << "central widget:" << _part->partWidget();
	installedPartList.append(_part);
	connect(_part, SIGNAL(wantClose(QFPart*)), this, SLOT(uninstallPart(QFPart*)));
	//qfTrash() << "  index of it:" << installedParts().indexOf(part);
	if(activePartIndex() < 0) setActivePart(_part);
	emit partInstalled(_part);
}

void QFPartManager::setActivePart(QFPart *part)
{
	static QSet<QFPart*> parts_with_generated_menus_list;
	qfLogFuncFrame();

	if(!part) return;
	if(installedParts().indexOf(part) < 0) installPart(part);

	QFPart *prev = activePart();
	if(prev == part) {
		qfTrash() << "\t curren and new active part are the same:" << part;
		return;
	}
	
	emit activePartChanging(part, prev);

	int prev_ix = activePartIndex();
	int ix = installedParts().indexOf(part);

	qfTrash() << "\t deactivating:" << (prev? prev->partName(): "NULL") << "policy:" << (prev? QFPart::mainMenuPolicyToString(prev->mainMenuPolicy()): QString());
	fActivePartIndex = ix;
	if(prev_ix > 0) {
		/// main part ma index 0 a jeji veci se neskryvaji
		/// hide toolbars
		foreach(QToolBar *t, prev->toolBars()) t->setVisible(false);
		/// hide dockwindows
		foreach(QDockWidget *w, prev->dockWidgets()) w->setVisible(false);
		/// disable actions
		foreach(QFAction *a, prev->actionList()) {
			//qfInfo() << "disabling:" << a->id();
			a->setEnabled(false);
		}
		
		if(prev->mainMenuPolicy() == QFPart::ShowInMainMenu) {
			/// skryj menu pro part
			const QStringList &menu_bar_action_ids = prev->uiBuilder()->menuBarActionIds();
			foreach(QAction *a, mainWindow()->menuBar()->actions()) {
				QString id = a->objectName();
				if(menu_bar_action_ids.contains(id)) a->setVisible(false);
			}
		}
	}

	qfTrash() << "\t activating:  " << (part? part->partName(): "NULL") << "policy:" << (part? QFPart::mainMenuPolicyToString(part->mainMenuPolicy()): QString());
	QFPart *active_part = activePart();
	if(active_part) {
		/// show toolbars
		foreach(QToolBar *t, active_part->toolBars()) t->setVisible(true);
		/// show dockwindows
		foreach(QDockWidget *w, active_part->dockWidgets()) w->setVisible(true);
		/// enable actions
		foreach(QFAction *a, active_part->actionList()) a->setEnabled(true);
	}

	//bool update_menu_bar = false;
	bool menu_generated_first_time = !parts_with_generated_menus_list.contains(active_part);
	bool regenerate_menu_bar = !prev; /// prvni aktivni part
	//regenerate_menu_bar = regenerate_menu_bar || (!parts_with_generated_menus_list.contains(p)); /// menu pro tento part dosud nebylo generovano
	regenerate_menu_bar = regenerate_menu_bar || (active_part && active_part->mainMenuPolicy() == QFPart::ReplaceMainMenu); /// replace policy
	regenerate_menu_bar = regenerate_menu_bar || (prev && prev->mainMenuPolicy() == QFPart::ReplaceMainMenu); /// pred tim byl aktivni part s replace policy
	qfTrash() << "\t menu generated first time:" << menu_generated_first_time;
	qfTrash() << "\t regenerate_menu_bar:" << regenerate_menu_bar;
	if(active_part) {
		if(regenerate_menu_bar) {
			/// generate new menu bar
			QMenuBar *new_menu_bar = NULL;
			if(active_part->mainMenuPolicy() == QFPart::ReplaceMainMenu) {
				qfTrash() << "\tgenerating menu bar for replace";
				new_menu_bar = new QMenuBar();
				active_part->updateMenuBar(new_menu_bar);
			}
			else {
				/// rebuild menu bar for all installed parts.
				qfTrash() << "\tgenerating menu bar for installed parts";
				new_menu_bar = new QMenuBar();
				foreach(QFPart *p, installedParts()) {
					if(p->mainMenuPolicy() == QFPart::ExtendMainMenu) {
						qfTrash() << "\t\t" << p->partName();
						p->updateMenuBar(new_menu_bar);
						parts_with_generated_menus_list << p;
					}
				}
			}
			qfTrash() << "\tsetting new menu bar:" << new_menu_bar;
			mainWindow()->setMenuBar(new_menu_bar);
		}
		else {
			QMenuBar *menu_bar = mainWindow()->menuBar();
			if(menu_generated_first_time) {
				/// toto menu se jeste negenerovalo
				qfTrash() << "\tgenerating menu bar for part:" << active_part->partName();
				active_part->updateMenuBar(menu_bar);
				parts_with_generated_menus_list << active_part;
			}
			else {
				/// menu je jiz vygenerovano
				if(active_part->mainMenuPolicy() == QFPart::ShowInMainMenu) {
					/// bude se ale muset zviditelnit
					const QStringList &menu_bar_action_ids = active_part->uiBuilder()->menuBarActionIds();
					foreach(QAction *a, mainWindow()->menuBar()->actions()) {
						QString id = a->objectName();
						if(menu_bar_action_ids.contains(id)) a->setVisible(true);
					}
				}
			}
		}
		active_part->activated();
	}
	emit activePartChanged(activePart(), prev);
}

int QFPartManager::activePartIndex() const
{
	if(fActivePartIndex >= 0 && fActivePartIndex < installedParts().count()) return fActivePartIndex;
	return -1;
}
/*
int QFPartManager::indexOf(QFPart *part) const
{
	int ix = -1;
	if(!part) return ix;
	ix = installedParts().indexOf(part);
	//if(ix < 0) qfWarning() << tr("Part 0x%1 '%2' not found in installedPartList.").arg((uint)part, 0, 16).arg((part)? part->id(): "");
	return ix;
}

QFPart* QFPartManager::part(const QString &part_id, bool throw_exc) const
{
	qfTrash() << QF_FUNC_NAME;
	foreach(QFPart *p, installedParts()) {
		qfTrash() << "\ttrying:" << p->partName();
		if(p->partName() == part_id) return p;
	}
	if(throw_exc) QF_EXCEPTION(tr("Part '%1' not found.").arg(part_id));
	return NULL;
}
*/
QFPart* QFPartManager::installedPart(int ix) const
{
	if(ix >= 0 && ix < installedParts().count()) return installedParts()[ix];
	return NULL;
}

QFPartManager::PartList QFPartManager::registeredParts(const QString &part_name) const
{
	qfTrash() << QF_FUNC_NAME << part_name;
	QList<QFPart*> ret;
	foreach(QFPart *p, registeredParts()) {
		qfTrash() << "\ttrying:" << p->partName();
		if(p->partName() == part_name) ret << p;
	}
	return ret;
}

QFPart* QFPartManager::registeredPart(const QString &part_name, bool throw_exc) const throw(QFException)
{
	QList<QFPart*> lst = registeredParts(part_name);
	if(lst.isEmpty() && throw_exc) QF_EXCEPTION(tr("Part '%1' is not registered.").arg(part_name));
	return lst[0];
}

QFPartManager::PartList QFPartManager::installedParts(const QString &part_name) const
{
	qfTrash() << QF_FUNC_NAME << part_name;
	QList<QFPart*> ret;
	foreach(QFPart *p, installedParts()) {
		qfTrash() << "\ttrying:" << p->partName();
		if(p->partName() == part_name) ret << p;
	}
	return ret;
}

QFPart* QFPartManager::installedPart(const QString &part_name, bool throw_exc) const throw(QFException)
{
	QList<QFPart*> lst = installedParts(part_name);
	if(lst.isEmpty() && throw_exc) QF_EXCEPTION(tr("Part '%1' is not installed.").arg(part_name));
	return lst[0];
}

void QFPartManager::uninstallPart(QFPart *p)
{
	if(!p) return;
	qfTrash() << QF_FUNC_NAME << p << p->caption();
	int ix = installedParts().indexOf(p);
	if(ix < 0) 	return;
	//disconnect(p, SIGNAL(activated(QWidget*)), 0, 0);
	ix = fActivePartIndex;
	setActivePart(NULL);
	installedPartList.removeAt(ix);
	if(installedParts().count()) {
		if(ix >= installedParts().count()) setActivePart(installedPart(0));
		else setActivePart(installedPart(ix));
	}
	mainWindow()->partCentralWidget()->removePart(p);
	emit partUninstalled(p);
	if(installedParts().count() == 0) {
		//qfTrash() << "emiting signal lastPartRemoved()";
		emit lastPartUninstalled(p);
	}
	p->releaseUi();
}
/*
QFPart* QFPartManager::widget2part(QWidget *w)
{
	foreach(QFPart* p, installedPartList) {
		if(p && p->centralWidget() == w) return p;
	}
	QF_ASSERT(false, tr("cann't find part for widget 0x%1").arg((uint)w, 0, 16));
	return NULL;
}
void QFPartManager::setActiveCentralWidget(QWidget *w)
{
	setActivePart(widget2part(w));
}
*/

void QFPartManager::setInstalledPartsActionsEnabled(bool b, const QRegExp &re)
{
	foreach(QFPart *p, installedParts()) {
		QFPart::ActionList lst = p->actionList();
		foreach(QFAction *a, lst) {
			bool set_it = true;
			if(!re.isEmpty()) {
				QString s = a->id();
				if(!re.exactMatch(s)) set_it = false;
			}
			if(set_it) a->setEnabled(b);
		}
	}
}
