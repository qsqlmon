// QFPart.h: interface for the QFPart class.
//
//////////////////////////////////////////////////////////////////////

#ifndef QFPART_H
#define QFPART_H

//#include "qfmainwindow.h"	// Added by ClassView

#include <QObject>
#include <QMap>

#include <qfguiglobal.h>
#include <qf.h>
#include <qfpartwidget.h>
#include <qfpartwidget.h>
#include <qfclassfield.h>

#include <QIcon>

class QFMainWindow;
class QFPartManager;
class QFPartWidget;
class QFAction;
class QFDockWidget;
class QFToolBar;
class QMenuBar;
class QFUiBuilder;

/*
Interface for QFPart plugin
 */
 /*
class QFPartInterface
{
	public:
		virtual ~QFPartInterface() {}
};

Q_DECLARE_INTERFACE(QFPartInterface, "org.fanda.libqf.QFPartInterface/1.0");
 */
/*!
Part.
 */
class QFGUI_DECL_EXPORT QFPart : public QObject 
{
	Q_OBJECT;
	friend class QFPartManager;
	//QF_FIELD_RW(QString, c, setC, aption);
	//QF_FIELD_RW(QIcon, i, setI, con);
	public:
		//! ExtendMainMenu, nabidky partu se natrvalu usidli v mail menu
		//! ReplaceMainMenu, part ma svoje vlastni menu, ktere pri jeho aktivaci nahradi stavajici menu
		//! ShowInMainMenu, part ma svoji entry v menu baru, ktera se zobrazi v okamziku, kdy je part aktivni a zmizi, kdyz je neaktivni
		enum MainMenuPolicy {MainMenuPolicyNotExplicitlySet = 0, ExtendMainMenu, ReplaceMainMenu, ShowInMainMenu};
		//QF_FIELD_RW(MainMenuPolicy, m, setM, ainMenuPolicy);
	public:
		typedef QList<QFAction*> ActionList;
		typedef QList<QFDockWidget*> DockWidgetList;
		typedef QList<QFToolBar*> ToolBarList;
	protected:
		struct Data
		{
			QString caption;
			QIcon icon;
			QFPartManager *partManager;
			QFPartWidget *partWidget;
			QFUiBuilder *uiBuilder;
			ActionList actionList;
			DockWidgetList dockWidgetList;
			ToolBarList toolBarList;
			MainMenuPolicy mainMenuPolicy;

			Data() : partManager(NULL), partWidget(NULL), uiBuilder(NULL), mainMenuPolicy(MainMenuPolicyNotExplicitlySet) {}
		};
		Data data;
		//Data d;
		//QFMainWindow *fMainWindow;
		//void setObject(QObject *pObject);
		//void setXMLGUI(const QString& sName);
		//void setCanDeactivate(bool bCanDeactivate);
		//void setWindowTitle(const QString &sNewTitle);
		//QObject	*m_pObject;
		virtual QString uiFileName() {return ":/parts/" + partName() + "/" + partName() + ".ui.xml";}
	public:
		QFUiBuilder* uiBuilder();
		QFPartManager* partManager(bool throw_exc = Qf::ThrowExc) const throw(QFException);

		//QFMainWindow* mainWindow() const throw(QFException);
		//QString& getWindowTitle();
		//! Returns pointer to widget representating this part or NULL if part has no widget .
		virtual QFPartWidget * partWidget() {return data.partWidget;}
		//virtual bool isWidget() {return widget() != NULL;}
		//! Return action named \a id (if it exists) or NULL.
		QFAction* action(const QString &id, bool throw_exc = Qf::ThrowExc) throw(QFException);

		// musi implementovat kazdy Part.
		/// Prida sam sebe do \a mb .
		virtual void updateMenuBar(QMenuBar *mb);
		virtual ActionList actionList() const {return const_cast<QFPart*>(this)->actionListRef();}
		virtual DockWidgetList dockWidgets() {return data.dockWidgetList;}
		virtual ToolBarList toolBars() {return data.toolBarList;}
		//! Prida dock windows a toolbary k stavajicim.
		virtual void buildUi() throw(QFException);
		/// Smaze svoje toolbary, dock windows a centralWidget. Inverzni funkce k \a buildUi(). Nemaze akce.
		virtual void releaseUi();

		MainMenuPolicy mainMenuPolicy();
		void setMainMenuPolicy(MainMenuPolicy p) {data.mainMenuPolicy = p;}
		void setCaption(const QString& s) {data.caption = s;}
		virtual QString caption();
		virtual QIcon icon();
		void setIcon(const QIcon &ico) {data.icon = ico;}

		static QString mainMenuPolicyToString(MainMenuPolicy policy);
	protected:
		//! Vraci mapu akci pro tento part, pokud jeste neexistuje, je vytvorena.
		/**
		pokud nekdo chce, aby connectActions davala akce jinam, musi pretocit tuto funkci.
		 */
		virtual ActionList& actionListRef();
		virtual DockWidgetList& dockWidgetsRef() {return data.dockWidgetList;}
		virtual ToolBarList& toolBarsRef() {return data.toolBarList;}
	public:
		virtual void connectActions(QObject *receiver, const QString &slot_prefix = QString());
		//! return \a objectName() .
		virtual QString partName() const {return objectName();}
		//virtual QString caption() const {return id();}
		//bool canDeactivate();
		//bool onDeactivate();
		//bool onActivate();
		//inline void emitActivated() {emit activated(this);}
	signals:
		//! Emited when part want to be closed.
		void wantClose(QFPart *_part, int result = 1);
		void partActivated(QFPart *_part);
		//void signalDeactivated();
		//void signalExited();
		//void signalTitleChanged(const QString &sNewTitle, QFPart *pPart);
	protected:
		//! vola partManager po te, co aktivuje part, defaultni implementace vysle signal.
		virtual void activated();
	public:
		//! @param id an alias for \a objectName .
		QFPart(QObject *parent, const QString &id = QString(), QFPartManager *manager = NULL);
		virtual ~QFPart();
};

#endif // !defined(QFPART_H)
