//
// C++ Interface: qfpartwidget
//
// Description: 
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef QFPARTWIDGET_H
#define QFPARTWIDGET_H 

#include <qfguiglobal.h>
#include <qfassert.h>

#include <QWidget>

class QFPart;
class QFDialogWidgetCaptionFrame;
class QFDialogWidgetStack;

/**
Part widget is a widget representating the part, graphical part of a QFPart.
@author Fanda Vacek
*/
class QFGUI_DECL_EXPORT QFPartWidget : public QWidget
{
	Q_OBJECT;
	protected:
		QFPart *fPart;
		QFDialogWidgetCaptionFrame *captionFrame;
		QWidget *f_centralWidget;
	protected:
		virtual void createCaption();
	public slots:
		void setCaptionText(const QString &s);
		void setCaptionIcon(const QIcon &ico);
	public:
		virtual QWidget *centralWidget();
		QFPart* part() {
			QF_ASSERT(fPart, "part of this central widget not set.");
			return fPart;
		}
	public:
		QFPartWidget(QFPart *_part, QWidget *parent = NULL);
		~QFPartWidget();
};

/**
Part widgetwhere the centralWidget() contains stacked widget already
 */
class QFGUI_DECL_EXPORT QFStackedPartWidget : public QFPartWidget
{
	Q_OBJECT;
	public:
		virtual QWidget *centralWidget();
		QFDialogWidgetStack* dialogWidgetStack();
	public:
		QFStackedPartWidget(QFPart *_part, QWidget *parent = NULL);
		//~QFStackedPartWidget();
};

#endif
