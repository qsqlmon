#include <QDebug>
#include "qfaction.h"

#include <qflogcust.h>
#include <qcoreevent.h>
#include <qfstring.h>

#include <QCoreApplication>

int QFAction::creationIdCounter = 0;
const char *QFAction::PN_creationId = "QF_PN_creationId";
const char *QFAction::PN_grant = "QF_PN_grant";
const char *QFAction::PN_merge = "QF_PN_merge";
//const char *QFAction::PN_triggerParam = "QF_PN_triggerParam";

void QFAction::init()
{
	//fAccepted = false;
	setMerge(false);
	setCreationId(nextCreationId());
	if(text() == "---") {
		setText(QString());
		setSeparator(true);
	}
	else if(text().count()) {
		setLocalizedText(text());
	}
	connect(this, SIGNAL(triggered(bool)), this, SLOT(qactionTriggeredHook(bool)));
	//connect(this, SIGNAL(toggled(bool)), this, SLOT(qactionToggledHook(bool)));
}

QFAction::~QFAction()
{
	//qfInfo() << "destroying action id:" << id();
}

void QFAction::qactionTriggeredHook(bool checked) {
	//emit aboutToTrigger(this, checked);
	//emit triggered(this, checked);
	emit triggered(id(), checked);
}

void QFAction::setLocalizedText(const QString &to_localize)
{
	QByteArray ba = to_localize.toUtf8();
	setProperty("qf_toLocalize", ba);
	QString localized = QCoreApplication::translate("uixml", ba.constData(), 0, QCoreApplication::UnicodeUTF8);
	//qfInfo() << to_localize << "->" << localized;
	setText(localized);
}

void QFAction::setMerge(bool b)
{
	setProperty(PN_merge, b);
	if(b) {
		setSeparator(true);
		setVisible(false);
	}
}

/*		
void QFAction::qactionTriggeredHook(bool checked)
{
	//qfTrash() << QF_FUNC_NAME;
	emit triggered(this, checked);
}
*/
bool QFAction::event(QEvent * e)
{
	if(e) {
		//qfInfo() << "action event" << e->type();
		if(e->type() == QEvent::LanguageChange) {
			//qfInfo() << "language changed" << id();
			QByteArray ba = property("qf_toLocalize").toByteArray();
			if(ba.count()) {
				qfInfo() << "to_localize utf8:" << QString::fromUtf8(ba);
				//QString localized = QCoreApplication::translate("uixml", ba.constData());
				QString localized = QCoreApplication::translate("uixml", ba.constData(), 0, QCoreApplication::UnicodeUTF8);
				qfInfo() << "localized:" << localized;
				setText(localized);
			}
		}
	}
	return QAction::event(e);
}

QPair<QString, QString> QFAction::splitIdToNameAndParams(const QString &id_str)
{
	QPair<QString, QString> ret;
	static QRegExp rx("([^\\(]+)(?:\\(([^\\(\\)]+)\\)){,1}");
	if(rx.exactMatch(id_str)) {
		ret.first = rx.cap(1);
		ret.second = rx.cap(2);
	}
	else ret.first = id_str;
	return ret;
}
