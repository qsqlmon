//
// C++ Interface: qfuibuilder
//
// Description: 
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef QFUIBUILDER_H
#define QFUIBUILDER_H

#include <QIcon>
#include <QMap>

#include "qfdom.h"
#include "qfpart.h"

class QFAction;

/**
Object to parse the *. xml.ui files.
@author Fanda Vacek
*/
class QFGUI_DECL_EXPORT QFUiBuilder
{
	Q_DECLARE_TR_FUNCTIONS(QFUiBuilder);
	public:
		static const QString sMenuBar;
		static const QString sMenu;
		static const QString sToolBar;
		static const QString sMerge;
		static const QString sAction;
		static const QString sActionGroup;
		static const QString sSeparator;
		static const QString sLibrary;
		static const QString sId;
		static const QString sCaption;
		static const QString sIcon;
		static const QString sToolTip;
		static const QString sShortCut;

		typedef QList<QFAction*> ActionList;
	private:
		class Data : public QSharedData
		{
			public:
				QString uiFileName;
				QFDomDocument doc;
				QFPart::ActionList actionList;
				bool actionsLoaded;
				QObject *createdActionsParent;
				QFUiBuilder *prevUiBuilder;
				QStringList f_menuBarActionIds;
			public:
				Data() : actionsLoaded(false), createdActionsParent(NULL), prevUiBuilder(NULL) {}
		};
		QSharedDataPointer<Data> d;
	protected:
		QString uiFileName() const {return d->uiFileName;}
	protected:
		const QFDomDocument& document(bool throw_exc = Qf::ThrowExc) throw(QFException);
		//QFDomDocument& documentRef(bool throw_exc = Qf::ThrowExc) throw(QFException);
		QIcon icon(const QFDomElement &el);
		virtual void updateMenu(QWidget *w, const QFDomElement &eldef);
		/// pokud tato funkce vrati false, akce je ignorovana a nevytvori se
		virtual bool createActionHook(const QFDomElement &el_action_def) {Q_UNUSED(el_action_def); return true;}
		QFAction* createAction(const QFDomElement &el_action_def, QObject *created_action_parent = NULL);
	public:
		bool isEmpty() const {return (d->doc.isEmpty() || !d->actionsLoaded);}
		QFUiBuilder* prevUiBuilder() const {return d->prevUiBuilder;}
		void setPrevUiBuilder(QFUiBuilder *uib) {d->prevUiBuilder = uib;}
		const ActionList& actions();

		const QStringList& menuBarActionIds() const {return d->f_menuBarActionIds;}
		
		//! vraci hodnotu id atributu root elementu *.ui.xml souboru nahraneho funkci open()
		QString uiId();

		void setCreatedActionsParent(QObject *parent) {d->createdActionsParent = parent;}
		
		//! nacte XML, ulozi si ho do \a doc a v nem definovane akce prida k \a actionList .
		void open(const QString &ui_file_name, bool throw_exc = Qf::ThrowExc) throw(QFException);
		//! Return action named \a id (if it exists) or NULL.
		QFAction* action(const QString &id, bool throw_exc = Qf::ThrowExc) throw(QFException);

		//! ui.xml document can have caption element, it will be caption of whole part.
		QString caption();
		//! ui.xml document can have icon element, it will be icon of whole part.
		QIcon icon();
		QFPart::MainMenuPolicy mainMenuPolicy();
		
		QFUiBuilder::ActionList& actionsRef() {actions(); return d->actionList;}

		//! Returns true if loaded *.xml.ui contains Menubar definition.
		bool hasMenubar();
		virtual void updateMenuOrBar(QWidget *mb);
		/// vytvori toolbar widgety podle xml a vrati je
		QFPart::ToolBarList createToolBars();
		//virtual void updateMenu(QMenu *m);
		void connectActions(QObject *o, const QString &slot_prefix = QString(), Qt::ConnectionType connection_type = Qt::AutoConnection) {
			connectActions(actions(), o, slot_prefix, connection_type); 
		}
	public:
		static void connectActions(const QList<QFAction*> &alist, QObject *o, const QString &slot_prefix = QString(), Qt::ConnectionType connection_type = Qt::AutoConnection);
		static QFAction* findAction(const QList<QFAction*> &alist, const QString &name, bool throw_exc = true) throw(QFException);
	public:
		//! constructor.
		/// @param created_objects_parent all created objects wil have \a created_objects_parent as parent.
		QFUiBuilder(QObject *created_objects_parent = NULL, const QString &ui_file_name = QString(), QFUiBuilder *prev_ui_builder = NULL);
		virtual ~QFUiBuilder();
};

#endif
