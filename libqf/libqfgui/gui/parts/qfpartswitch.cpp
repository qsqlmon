
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include <qfpart.h>
#include <qfpartmanager.h>

#include "qfpartswitch.h"

#include <qflogcust.h>

//=================================================
//                                            QFPartSwitchToolButton
//=================================================
QFPartSwitchToolButton::QFPartSwitchToolButton(QWidget *parent, QFPart *_part)
	: QToolButton(parent), part(_part)
{
	QPalette palette;
	palette.setColor(QPalette::Active, QPalette::Window, QColor(150, 150, 150)); /// pozadi buttonu
	palette.setColor(QPalette::Active, QPalette::Button, QColor(225, 225, 225)); /// ramecek buttonu
	//palette.setColor(QPalette::Inactive, QPalette::Window, QColor(0, 150, 0));
	//palette.setColor(QPalette::Inactive, QPalette::Button, QColor(0, 5, 125));
	//palette.setBrush(QPalette::Button, QBrush(QColor(0, 125, 125)));
	setPalette(palette);
	setAutoFillBackground(false); /// musi bejt off
	setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
	setText(part->caption());
	setIcon(part->icon());
	setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	setAutoRaise(true);
	setCheckable(true);
	setAutoExclusive(true);
	//setIconSize(QSize(48, 48));
	//this->setMinimumHeight(200);
	connect(this, SIGNAL(clicked()), SLOT(toolButtonClicked()));
}

//=================================================
//                                            QFPartSwitch
//=================================================
QFPartSwitch::QFPartSwitch(QWidget *parent, QFPartManager *manager)
	: QToolBar(parent), fPartManager(manager)
{
	QF_ASSERT(manager, "Part manager is NULL.");

	QPalette palette;
	palette.setColor(QPalette::Window, QColor(100, 150, 150));
	//palette.setColor(QPalette::Button, QColor(0, 125, 125));
	//palette.setBrush(QPalette::Button, QBrush(QColor(0, 125, 125)));
	setPalette(palette);
	setAutoFillBackground(true);

	connect(manager, SIGNAL(partRegistered(QFPart*)), this, SLOT(addPart(QFPart*)));
	connect(manager, SIGNAL(activePartChanged(QFPart *, QFPart *)), this, SLOT(checkButton(QFPart *)));
}

QFPartSwitch::~QFPartSwitch()
{
	qfTrash() << QF_FUNC_NAME;
}

void QFPartSwitch::addPart(QFPart *_part)
{
	qfTrash() << QF_FUNC_NAME;
	QFPartSwitchToolButton *bt = new QFPartSwitchToolButton(NULL, _part);
	connect(bt, SIGNAL(clicked(QFPart*)), this, SLOT(activatePart(QFPart*)));
	if(findChild<QFPartSwitchToolButton*>() == NULL) bt->setChecked(true);
	addWidget(bt);
}

void QFPartSwitch::activatePart(QFPart *_part)
{
	partManager()->setActivePart(_part);
}

void QFPartSwitch::checkButton(QFPart *active_part)
{
	if(!active_part) return;
	QFPartSwitchToolButton *active_bt = NULL;
	foreach(QFPartSwitchToolButton *bt, findChildren<QFPartSwitchToolButton *>()) {
		if(bt->part == active_part) {active_bt = bt; break;}
	}
	if(active_bt && !active_bt->isChecked()) {
		active_bt->setChecked(true);
	}
}
