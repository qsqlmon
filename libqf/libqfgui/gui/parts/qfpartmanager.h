// PartManager.h: interface for the PartManager class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PARTMANAGER_H__43A0B61D_20FE_4602_80C7_580F7AA068F7__INCLUDED_)
#define AFX_PARTMANAGER_H__43A0B61D_20FE_4602_80C7_580F7AA068F7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include <QLocale>
#include <QObject>
#include <QList>
#include <QTabWidget>

#include <qfguiglobal.h>
#include <qf.h>

class QFMainWindow;
class QFPart;
class QFAction;
class QFPartCentralWidget;

//! @todo write documentation.
class QFGUI_DECL_EXPORT QFPartManager : public QObject
{
		Q_OBJECT

		typedef QList<QFPart *>	PartList;
	protected:
		PartList	 registeredPartList;
		PartList	 installedPartList;
		int fActivePartIndex;
		//QFPartCentralWidget fPartCentralWidget;
	public:
		QFMainWindow* mainWindow();
		//QFPart* widget2part(QWidget *w);
		int activePartIndex() const;
		//int indexOf(QFPart *part) const;
		QFPart* installedPart(int ix) const;
		// Returns first part with name \a part_name .
		//QFPart* part(const QString &part_name, bool throw_exc = Qf::ThrowExc) const;
	public:
		QFPartManager(QFMainWindow	*parent);
		virtual ~QFPartManager();
	public:
		//! register part in part manager, only registered part can be used.
		void registerPart(QFPart *part);
		//! try to find part DLL in known places and load it.
		/// XML_UI is loaded as well.
		QFPart* registerPart(const QString &part_id, bool throw_exc = Qf::ThrowExc) throw(QFException);
		/**
		 * Adds part to main window, it can be shown or not, it depends on PartCentralwidget and number of allready added parts.
		 * Part must be registered using registerPart(QFPart *part).
		 * Function also builds a \a part ui and connects actions defined in ui.xml to part handlers  (calling \a connectActions(this); ).
		 * @param part_name name id of part.
		 * @return pointer to added part 
		 */
		void installPart(QFPart *part) throw(QFException);
		//QFPart* addPartToMainWindow(const QString &part_name) throw(QFException);
		QFPart* activePart() const {return installedPart(activePartIndex());}

		//QFPart* part(const QString &part_id, bool throw_exc = Qf::ThrowExc) const;
		PartList installedParts() const {return installedPartList;}
		PartList registeredParts() const {return registeredPartList;}
		//! returns all parts with \a partName() == \a part_name .
		PartList registeredParts(const QString &part_name) const;
		//! return first registered part, which has \a partName() == \a part_name or NULL.
		QFPart* registeredPart(const QString &part_name, bool throw_exc = Qf::ThrowExc) const throw(QFException);
		PartList installedParts(const QString &part_name) const;
		//! return first registered part, which has \a partName() == \a part_name or NULL.
		QFPart* installedPart(const QString &part_name, bool throw_exc = Qf::ThrowExc) const throw(QFException);
		/*
		void setPartCentralWidget(QFPartCentralWidget *w) {fPartCentralWidget = w;}
		QFPartCentralWidget* partCentralWidget() {
			QF_ASSERT(fPartCentralWidget, "part central widget not set.");
			return fPartCentralWidget;
		}
		*/
		void setInstalledPartsActionsEnabled(bool b, const QRegExp &re = QRegExp());
	public slots:
		void uninstallPart(QFPart *part);
		/**
		 *  Sets \a part active, it also regenerates the application main window menu.
		 */
		void setActivePart(QFPart *part); 
		//void setActiveCentralWidget(QWidget *w);
		/*
		void slotActivateNextPart();
		void slotActivatePreviousPart();
		void slotActivatePart(QFPart *pPart);
		void slotClosePart();
		void slotActivatedWidget();
		void slotPartTitleChanged(const QString &sNewPartTitle, QFPart *pPart);
		*/
	signals:
		void partRegistered(QFPart *part);
		void partInstalled(QFPart *part);
		//void partAboutToRemove(QFPart *part, bool &ok_to_remove);
		void partUninstalled(QFPart *part);
		void lastPartUninstalled(QFPart *part);
		//! emited before activation of a new part
		void activePartChanging(QFPart *activated_part, QFPart *deactivated_part);
		//! emited after activation of a new part
		void activePartChanged(QFPart *active_part, QFPart *deactivated_part);
};

#endif // !defined(AFX_PARTMANAGER_H__43A0B61D_20FE_4602_80C7_580F7AA068F7__INCLUDED_)
