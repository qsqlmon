// fqplugin.h: interface for the QFPlugin class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_QFPLUGIN_H__B6E8B7DA_2E1E_4B9A_920D_3419F23C0872__INCLUDED_)
#define AFX_QFPLUGIN_H__B6E8B7DA_2E1E_4B9A_920D_3419F23C0872__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <QObject>
#include "qfglobal.h"

class QFPart;
class QFMainWindow;

ERROR
// QFPlugin * get_plugin(QObject *pParent);
typedef	void *(*t_get_plugin)(void *pParent);

/*!
Plugin
 */
class QFGUI_DECL_EXPORT QFPlugin : public QObject
{
	Q_OBJECT
public:
	QFPlugin(QObject *pParent, const QString& sPluginName = QString());
	virtual ~QFPlugin();
protected:
	//QFMainWindow *m_pMainWindow;
	virtual QFPart * createPart(const QString& sPartName, QObject *pObject = NULL, const QString& sObjectName = QString()) = 0;
};


//typedef QFLib::Plugin QFPlugin;

#endif // !defined(AFX_QFPLUGIN_H__B6E8B7DA_2E1E_4B9A_920D_3419F23C0872__INCLUDED_)
