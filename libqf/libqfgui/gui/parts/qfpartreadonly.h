// QFPartReadOnly.h: interface for the QFPartReadOnly class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PARTREADONLY_H__C1AECCCE_F922_46C5_97E0_3EEC3C75C0AE__INCLUDED_)
#define AFX_PARTREADONLY_H__C1AECCCE_F922_46C5_97E0_3EEC3C75C0AE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <QObject>
#include "qfpart.h"

///ERROR

/*!
Part read only
 */
class QFPartReadOnly:public QFPart
{
	Q_OBJECT
public:
	virtual bool openDocument() = 0;
	QFPartReadOnly(QFMainWindow *pParent = NULL, const char *szName = NULL);
	virtual ~QFPartReadOnly();

};

//typedef QFLib::PartReadOnly QFPartReadOnly;

#endif // !defined(AFX_PARTREADONLY_H__C1AECCCE_F922_46C5_97E0_3EEC3C75C0AE__INCLUDED_)
