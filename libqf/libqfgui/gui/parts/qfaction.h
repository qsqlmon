#if !defined(QFACTION_H)
#define QFACTION_H

#include <qfguiglobal.h>

#include <QAction>
#include <QString>

/**
\class QFAction
\brief Replacement of the QT object QAction for usage with modular functionality.

Merge je specialni akce pro menu. Pokud neobsahuje zadne polozky, je neviditelne.
Po pridani prvni polozky da pred sebe separator, zviditelni ze
(takze ted vypada jako separator) a vsechny polozky jsou pridavany pred nej.
*/
class QFGUI_DECL_EXPORT QFAction : public QAction
{
		Q_OBJECT
	friend class QFPart;
	protected:
		static int creationIdCounter;
	public:
		static bool creationIdLessThan(QFAction *a1, QFAction *a2) {return a1->creationId() < a2->creationId();}
	protected:
		//! abych mohl akce seradit podle poradi, jak byly vytvoreny.
		static const char *PN_creationId;
		static const char *PN_grant;
		static const char *PN_merge;
		//static const char *PN_triggerParam;
		void init();
	public:
		virtual bool event(QEvent * e);
		//void accept(bool b = true) {fAccepted = b;}
		/// pokud pro povoleni akce musi mit uzivatel nejaky grant, je uveden zde.
		QString grant() const {return property(PN_grant).toString();}
		void setGrant(const QString &g) {setProperty(PN_grant, g);}
		//! If returns true, QFParts xmlui are inserted after this action.
		bool isMerge() {return property(PN_merge).toBool();}
		void setMerge(bool b = true);
		void setId(const QString &id) {setObjectName(id);}
		QString id() const {return objectName();}
		int creationId() const {return property(PN_creationId).toInt();}
		void setCreationId(int id) {setProperty(PN_creationId, id);}
		//QString triggerParam() const {return property(PN_triggerParam).toString();}
		//void setTriggerParam(const QString &param) {setProperty(PN_triggerParam, param);}
	protected slots:
		//!  Hook to reemit QAction::trigered(bool) as QFAction::triggered(QFAction*, bool).
		void qactionTriggeredHook(bool checked);
		//inline void qactionToggledHook(bool checked) {emit triggered(this, checked);}
	signals:
		//void triggered(QFAction *source = NULL, bool checked = true);
		void triggered(const QString &param = QString(), bool checked = true);
		// QFPartManager needs it for the signal aboutToNotActivePartActionTrigger()
		//void aboutToTrigger(QFAction *source = NULL, bool checked = true);
	public:
		QPair<QString, QString> idNameAndParams() const {return QFAction::splitIdToNameAndParams(id());}
		static QPair<QString, QString> splitIdToNameAndParams(const QString &id_str);
		static int nextCreationId() {return ++creationIdCounter;}
		static QFAction* createSeparator(QObject *parent) {
			QFAction *a = new QFAction(parent);
			a->setSeparator(true);
			return a;
		}
		/// text, ktery se pri eventu LanguageChange, prelozi podle noveho jazyka, vynikajici pro dinamickou lokalizaci
		void setLocalizedText(const QString &text);
	public:
		QFAction(QObject * parent = 0) : QAction(parent) {init();}
		//! pokud je \a text "---", nastavi se akci separator flag
		QFAction(const QString & text, QObject * parent = 0) : QAction(text, parent) {init();}
		QFAction(const QIcon & icon, const QString & text, QObject * parent = 0) : QAction(icon, text, parent) {init();}
		virtual ~QFAction();
};

#endif // !defined(QFACTION_H)
