// QFPartReadWrite.h: interface for the QFPartReadWrite class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PARTREADWRITE_H__C47A4077_71BE_49FB_AFAE_C1CBAB8AC3B8__INCLUDED_)
#define AFX_PARTREADWRITE_H__C47A4077_71BE_49FB_AFAE_C1CBAB8AC3B8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "qfpartreadonly.h"
///ERROR

/*!
Part read/write.
 */
class QFGUI_DECL_EXPORT QFPartReadWrite : public QFPartReadOnly  
{
	Q_OBJECT

	bool	m_bIsReadWrite;
	bool	m_bIsModified;
public:
	QFPartReadWrite(QFMainWindow *pParent = NULL, const char *szName = NULL);
	virtual ~QFPartReadWrite();

	virtual bool saveDocument() = 0;
	virtual bool isModified() = 0;
	
	virtual bool isReadWrite()
	{
		return m_bIsReadWrite;
	};

public slots:
	void slotSetModified();
};

//typedef QFLib::PartReadWrite QFPartReadWrite;

#endif // !defined(AFX_PARTREADWRITE_H__C47A4077_71BE_49FB_AFAE_C1CBAB8AC3B8__INCLUDED_)
