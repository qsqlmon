// MainWindow.h: interface for the MainWindow class.
//
//////////////////////////////////////////////////////////////////////

#ifndef QF_MAINWINDOW_H
#define QF_MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QStringList>
#include <QList>
#include <QPoint>
#include <QSize>
#include <QMenuBar>

#include <qfdom.h>
#include <qfguiglobal.h>
#include <qfxmlconfigpersistenter.h>

class QFPartWidgetContainer;

class QFPartCentralWidget;
class QFPartManager;
class QFPart;
class QFMdiSubWindow;
class QFMdiArea;
 
/*!
Main window.
 */
class QFGUI_DECL_EXPORT QFMainWindow : public QMainWindow, public QFXmlConfigPersistenter
{
	Q_OBJECT;
	protected:
		QFPartManager		*fPartManager;
		QFPartCentralWidget	*fPartCentralWidget;
	public:
		enum CentralWidgetStyle {CentralWidgetStyleTabbed = 1, CentralWidgetStyleStack, CentralWidgetStyleWorkSpace};
	protected slots:
		void  mdiAreaSubWindowActiveStateChanged(QFMdiSubWindow *w, bool currently_active);
	public slots:
		void help_aboutQt();

		QFPartManager& partManager();
		QFPartCentralWidget* partCentralWidget() {return fPartCentralWidget;}
	protected:
		void loadPersistentData();
		void savePersistentData();
	public:
		void useMdiArea(QFMdiArea *area);
	public:
		QFMainWindow(QWidget *parent = NULL, Qt::WFlags flags = 0, CentralWidgetStyle style = CentralWidgetStyleTabbed);
		virtual ~QFMainWindow();
};

#endif // !defined(QF_MAINWINDOW_H)
