// QFPartReadWrite.cpp: implementation of the QFPartReadWrite class.
//
//////////////////////////////////////////////////////////////////////

#include "qfpartreadwrite.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

QFPartReadWrite::QFPartReadWrite(QFMainWindow *pParent, const char *szName)
	: QFPartReadOnly(pParent, szName), m_bIsReadWrite(true), m_bIsModified(false)
{

}

QFPartReadWrite::~QFPartReadWrite()
{

}

void QFPartReadWrite::slotSetModified()
{
	m_bIsModified = true;
}

