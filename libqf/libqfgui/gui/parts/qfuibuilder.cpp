//
// C++ Implementation: qfuibuilder
//
// Description: 
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "qfuibuilder.h"

#include <qfguiglobal.h>
#include <qffileutils.h>
#include <qfassert.h>
#include <qfpixmapcache.h>
#include <qfaction.h>
#include <qftoolbar.h>

#include <QCoreApplication>
#include <QMetaMethod>
#include <QDir>
#include <QToolBar>
#include <QMenu>
#include <QMenuBar>

#include <qflogcust.h>

// Nazvy tagu a atributu ve vstupnich XML
//const QString	QFUiBuilder::sAccessibleName	= "accessiblename";
const QString	QFUiBuilder::sMenuBar	= "menubar";
const QString	QFUiBuilder::sMenu			= "menu";
const QString	QFUiBuilder::sToolBar		= "toolbar";
const QString	QFUiBuilder::sMerge			= "merge";
const QString	QFUiBuilder::sAction		= "action";
const QString	QFUiBuilder::sActionGroup		= "actionGroup";
const QString	QFUiBuilder::sSeparator	= "separator";
const QString	QFUiBuilder::sLibrary		= "library";
const QString	QFUiBuilder::sId				= "id";
const QString	QFUiBuilder::sCaption		= "caption";
const QString	QFUiBuilder::sIcon		= "icon";
const QString	QFUiBuilder::sToolTip		= "tooltip";
const QString	QFUiBuilder::sShortCut		= "shortcut";

QFUiBuilder::QFUiBuilder(QObject *created_actions_parent, const QString &ui_file_name, QFUiBuilder *prev_ui_builder)
{
	qfLogFuncFrame() << "ui_file_name:" << ui_file_name;
	//QF_ASSERT(createdObjectsParent != NULL, "part is NULL");
	d = new Data();
	d->createdActionsParent = created_actions_parent;
	d->prevUiBuilder = prev_ui_builder;
	d->uiFileName = ui_file_name;
	//open(ui_file_name, Qf::ThrowExc);
}


QFUiBuilder::~QFUiBuilder()
{
	//SAFE_DELETE(actionsCache);
}

void QFUiBuilder::open(const QString &ui_file_name, bool throw_exc) throw(QFException)
{
	qfLogFuncFrame() << "ui_file_name:" << ui_file_name;
	if(!ui_file_name.isEmpty()) {
		QFile f(ui_file_name);
		f.open(QIODevice::ReadOnly);
		if(!f.isOpen()) {
			QString s = TR("Cann't open ui file %1").arg(ui_file_name);
			if(throw_exc) QF_EXCEPTION(s);
			qfWarning() << s;
			return;
		}
		QFDomDocument doc1;
		qfTrash() << "\tparsing:" << f.fileName();
		if(!doc1.setContent(f, !Qf::ThrowExc)) return;
		d->doc = doc1;
		d->uiFileName = ui_file_name;
		//appendActions();
	}
}

QIcon QFUiBuilder::icon(const QFDomElement &_el) 
{
	QFDomElement el = _el.cd("icon", !Qf::ThrowExc);
	QFString id = el.attribute("id");
	QString src = el.attribute("src");
	return QFPixmapCache::icon(id, src);
}
				
QString QFUiBuilder::caption()
{
	QString ret;
	{
		QString to_localize = document().cd(sCaption, !Qf::ThrowExc).text();
		QByteArray ba = to_localize.toUtf8();
		QString localized = QCoreApplication::translate("uixml", ba.constData(), 0, QCoreApplication::UnicodeUTF8);
		ret = localized;
	}
	return ret;
}

QIcon QFUiBuilder::icon()
{
	return icon(document().rootElement());
}

QFAction * QFUiBuilder::createAction(const QFDomElement & el_action_def, QObject *created_actions_parent)
{
	QFAction *ret = NULL;
	QFDomElement el1 = el_action_def;
	if(el1.tagName() == sAction) do {
		QFString id = el1.attribute("id", "");
		if(!id) break;
		if(!createActionHook(el1)) break;
		if(created_actions_parent == NULL) created_actions_parent = d->createdActionsParent;
		if(!created_actions_parent) QF_EXCEPTION("created actions should have a parent");
		QFAction *a = new QFAction(created_actions_parent);
		a->setObjectName(id);
		{
			QString to_localize = el1.cd(sCaption, !Qf::ThrowExc).text();
			a->setLocalizedText(to_localize);
			//QByteArray ba = to_localize.toUtf8();
			//a->setProperty("qf_toLocalize", ba);
			//QString localized = QCoreApplication::translate("uixml", ba.constData(), 0, QCoreApplication::UnicodeUTF8);
			//QString localized = QCoreApplication::translate("uixml", ba.constData());
			//QString localized = QObject::trUtf8(ba.constData());
			//qfInfo() << "localized:" << localized;
			//a->setText(localized);
		}
		a->setToolTip(el1.cd(sToolTip, !Qf::ThrowExc).text());
		a->setShortcut(el1.cd(sShortCut, !Qf::ThrowExc).text());
		a->setIcon(icon(el1));
		bool enabled = el1.attribute("enabled", "true").toBool();
		a->setGrant(el1.attribute("grant", QString()));
				//enabled = enabled && qfApp()->currentUserHasGrant(a->grant());
		a->setEnabled(enabled);
		a->setCheckable(QFString(el1.attribute("checkable")).toBool());
		ret = a;
	} while(false);
	return ret;
}

const QFUiBuilder::ActionList& QFUiBuilder::actions()
{
	qfLogFuncFrame();
	if(!d->actionsLoaded) {
		//QFUiBuilder *noconst_this = const_cast<QFUiBuilder*>(this);
		QFDomElement el = document().cd("/", !Qf::ThrowExc);
		QDomNodeList lst = el.childNodes();
		for(int i=0; i<lst.count(); i++) {
			QFDomElement el1 = lst.item(i).toElement();
			if(el1.tagName() == sActionGroup) {
				QActionGroup *action_group = new QActionGroup(d->createdActionsParent);
				QDomNodeList lst2 = el1.childNodes();
				for(int i1=0; i1<lst2.count(); i1++) {
					QFDomElement el2 = lst2.item(i1).toElement();
					QFAction *a = createAction(el2, action_group);
					if(a) { d->actionList << a; }
				}
			}
			else {
				QFAction *a = createAction(el1);
				if(a) { d->actionList << a; }
			}
		}
		d->actionsLoaded = true;
	}
	return d->actionList;
}

QFPart::ToolBarList QFUiBuilder::createToolBars()
{
	//if(actionsCache.isEmpty()) actionMap();
	qfLogFuncFrame();
	QFPart::ToolBarList ret;
	actions(); /// ujisti se, ze uz byly vytvoreny akce
	if(isEmpty()) return ret;

	QFDomElement el = document().cd("/", !Qf::ThrowExc);
	//qfInfo() << "el:" << el.toString();
	QDomNodeList lst = el.childNodes();
	for(int i=0; i<lst.count(); i++) {
		QFDomElement el1 = lst.item(i).toElement();
		qfTrash() << "\t element:" << el1.tagName();
		if(el1.tagName() == sToolBar) {
			QFToolBar *t = new QFToolBar(NULL);
			t->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
			t->setObjectName(el1.attribute(sId));
			qfTrash() << "\t creating toolbar:" << t->objectName();
			//qfInfo() << t->layout();
			//qfInfo() << t->layout()->metaObject()->className();
			
			QFString s = el1.attribute("allowedareas", "all");
			QStringList sl = s.splitAndTrim('|');
			foreach(s, sl) {
				Qt::ToolBarAreas areas;
				if(s == "left") areas |= Qt::LeftToolBarArea;
				else if(s == "right") areas |= Qt::RightToolBarArea;
				else if(s == "top") areas |= Qt::TopToolBarArea;
				else if(s == "bottom") areas |= Qt::BottomToolBarArea;
				else if(s == "all") areas |= Qt::AllToolBarAreas;
			}
			s = el1.attribute("preferredarea", "top").trimmed();
			if(s == "left") t->setPreferredArea(Qt::LeftToolBarArea);
			else if(s == "right") t->setPreferredArea(Qt::RightToolBarArea);
			else if(s == "top") t->setPreferredArea(Qt::TopToolBarArea);
			else if(s == "bottom") t->setPreferredArea(Qt::BottomToolBarArea);
			
			s = el1.attribute("toolButtonStyle", "iconOnly").trimmed().toLower();
			if(s == "icononly") t->setToolButtonStyle(Qt::ToolButtonIconOnly);
			else if(s == "textonly") t->setToolButtonStyle(Qt::ToolButtonTextOnly);
			else if(s == "textbesideicon") t->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
			else if(s == "textundericon") t->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
			
			QFDomElement el2 = el1.firstChildElement();
			for(; !!el2; el2=el2.nextSiblingElement()) {
				if(el2.tagName() == sAction) {
					QString s = el2.attribute(sId);
					qfTrash() << "\tadding action:" << s;
					QFAction *a = action(s, !Qf::ThrowExc);
					if(a) {
						t->addAction(a);
						/**
						tohle funguje, ale musi se na buttony kliknout 2x
						QAbstractButton *bt = qobject_cast<QAbstractButton*>(t->widgetForAction(a));
						if(bt) {
							bt->setAutoExclusive(QFString(el2.attribute("exclusive", "false")).toBool());
							qfInfo() << "exclusive:" << bt->autoExclusive() << el2.attribute("exclusive", "nic") << QFString(el2.attribute("exclusive", "false")).toBool();
						}
						*/
					}
					else qfWarning().noSpace() << document().fileName() << ": action '" << s << "' not found.";
				}
				else if(el2.tagName() == sSeparator) {  
					QFAction *a = new QFAction(t);
					a->setSeparator(true);
					t->addAction(a);
				}
			}
			ret.append(t);
		}
	}
	return ret;
}

void QFUiBuilder::connectActions(const QList<QFAction*> &alist, QObject *o, const QString &slot_prefix, Qt::ConnectionType connection_type)
{
			/*
	const QMetaObject *mo = a->metaObject();
	qfTrash() << "classname:" << mo->className();
	for(int i=0; i<mo->methodCount(); i++) {
	QMetaMethod mm = mo->method(i);
	if(mm.methodType() != 1) continue;
	qfTrash() << i << "  method signature:" << mm.signature() << "[" << mm.methodType() << "]";
}
			*/
	qfLogFuncFrame();
	//qfTrash() << "\tslot prefix:" << slot_prefix;
	foreach(QFAction *a, alist) {
		//qfWarning() << a;
		if(!a) continue;
		QByteArray ba = QMetaObject::normalizedSignature("triggered(QString, bool)").constData();
		const char *pc = ba.constData();
		int sig_ix = a->metaObject()->indexOfSignal(pc);
		qfTrash() << "looking for index of signal:" << pc << "returning:" << sig_ix;
		if(sig_ix < 0) {
			qfError() << "signal should exist" << ba;
			continue;
		}
		QString id_name = a->idNameAndParams().first;
		qfTrash() << "\taction id:" << id_name << "classname:" << a->metaObject()->className() << a;
		bool ok = false;
		/// try default slots with zero, one or two parameters
		QStringList sl = QStringList() << "(QString, bool)" << "(QString)" << "(bool)" << "()";//<< "(QFAction*)" << "(QFAction*, bool)" ;
		foreach(QString s1, sl) {
			QFString s = slot_prefix + id_name.replace('.', '_') + s1;
			qfTrash() << "\tslot:" << s;
			int slt_ix = o->metaObject()->indexOfSlot(QMetaObject::normalizedSignature(s.str()).data());
			if(slt_ix >= 0) {
				//qfDebug() << QString("connecting action '%4' signal QFAction::%1 to slot %2::%3").arg(pc).arg(o->metaObject()->className()).arg(s).arg(a->id());
				//qfDebug() << "\taction id:" << a->id() << a;
				/// pokud uz je pripojeny, odpoj ho, neumim zjistit jestli connection uz existuje nebo ne
				QMetaObject::disconnect(a, sig_ix, o, slt_ix);
				ok = QMetaObject::connect(a, sig_ix, o, slt_ix, connection_type);
				if(!ok) qfWarning() << "connection ERROR";
				else qfTrash() << "\tOK";
				break;
			}
		}
		if(!ok) {
			//a->setEnabled(false);
			QString s = slot_prefix + id_name.replace('.', '_') + "(...)";
			qfDebug().noSpace() << "no slot " << o->metaObject()->className() << "::" << s << " found.";
		}
	}	
}
		
bool QFUiBuilder::hasMenubar()
{
	QFDomElement el = document().cd("/" + QFUiBuilder::sMenuBar, !Qf::ThrowExc);
	return !el.isNull();
}

void QFUiBuilder::updateMenuOrBar(QWidget *mb)
{
	qfLogFuncFrame() << "uiFileName:" << uiFileName();
	if(!hasMenubar()) return;
	actions(); /// ujisti se, ze uz byly vytvoreny akce
	if(isEmpty()) return;
	
	if(qobject_cast<QMenuBar*>(mb) || qobject_cast<QMenu*>(mb)) {
		QFDomElement el = document().cd("/" + QFUiBuilder::sMenuBar, !Qf::ThrowExc);
		updateMenu(mb, el);
	}
}

void QFUiBuilder::updateMenu(QWidget *w, const QFDomElement &eldef)
{
	qfLogFuncFrame();
	QF_ASSERT(w != NULL, "w != NULL");
	QMenuBar *menu_bar = qobject_cast<QMenuBar*>(w);
	//QMenu *menu = qobject_cast<QMenu*>(w);
	QList<QAction *> actlst;
	actlst = w->actions();
	/// najdi merge action
	QFAction *merge = NULL;
	foreach(QObject *o, actlst) {
		QFAction *a = qobject_cast<QFAction*>(o);
		if(a && a->isMerge()) {
			merge = a;
			break;
		}
	}
	QDomNodeList xlst = eldef.childNodes();
	bool insert_to_merge_section = false;
	for(int i=0; i<xlst.count(); i++) {
		QFDomElement el_part_menu = xlst.item(i).toElement();
		if(el_part_menu.isNull()) continue;
		if(el_part_menu.tagName() != QFUiBuilder::sMenu
				 && el_part_menu.tagName() != QFUiBuilder::sAction
				 && el_part_menu.tagName() != QFUiBuilder::sSeparator
				 && el_part_menu.tagName() != QFUiBuilder::sMerge) continue;
		QFString id = el_part_menu.attribute(QFUiBuilder::sId);
		//if(!id) continue;
		qfTrash() << "\tlooking for menu:" << id << "<" << el_part_menu.tagName() << ">";
		QAction *m = NULL;
		if(!!id) foreach(QAction *a, actlst) {
			//qfTrash() << "\t\ttrying id:" << a->objectName();
			if(a->objectName() == id) {
				m = a;
				break;
			}
		}
		if(!m) {
			/// menu z xml ui neexistuje v dosavadnim menu
			/// akce merge rika, kam se v tomto menu pridava
			/// merge je neviditelny separator v menu
			qfTrash() << "\t\tNOT_FOUND.";
			QFAction *a = NULL;
			if(merge && !insert_to_merge_section) {
				/// pridava se porve do merge sekce
				/// pridej separator zepredu
				a = QFAction::createSeparator(w);
				w->insertAction(merge, a);
				insert_to_merge_section = true;
				//merge->setVisible(true);
			}
			if(el_part_menu.tagName() == QFUiBuilder::sAction) {
				a = action(id, !Qf::ThrowExc);
				if(!a) {
					//qfWarning() << QFLog::nospace << "action '" << id << "' not found";
					continue;
				}
				qfTrash() << "\t\tinserting menu:" << a->text();
				//a->setIconText(a->text());
				w->insertAction(merge, a);
			}
			else if(el_part_menu.tagName() == QFUiBuilder::sMerge) {
				a = new QFAction();
				a->setMerge(true);
				qfTrash() << "\t\tinserting <merge> action";
				w->insertAction(merge, a);
			}
			else if(el_part_menu.tagName() == QFUiBuilder::sSeparator) {
				a = new QFAction();
				a->setSeparator(true);
				w->insertAction(merge, a);
			}
			else if(el_part_menu.tagName() == QFUiBuilder::sMenu) {
				//QFString s;
				a = new QFAction(w);
				{
					QString to_localize = el_part_menu.cd(QFUiBuilder::sCaption, !Qf::ThrowExc).text();
					a->setLocalizedText(to_localize);
					//QByteArray ba = to_localize.toUtf8();
					//a->setProperty("qf_toLocalize", ba);
					//QString localized = QCoreApplication::translate("uixml", ba.constData(), 0, QCoreApplication::UnicodeUTF8);
				}
				//bool add_to_actions = false;
				if(id.isEmpty()) id = QString::number(QFAction::nextCreationId());
				else {
					/// add to builder actions
					//qfInfo() << id;
					if(!action(id, !Qf::ThrowExc)) actionsRef() << a;
				}
				a->setId(id);
				a->setObjectName(id);
				a->setIcon(icon(el_part_menu));
				QMenu *m = new QMenu(w);
				m->setObjectName(id);
				a->setMenu(m);
				qfTrash() << "\t\tinserting menu:" << a->text() << "id" << m->objectName();
				w->insertAction(merge, a);
				updateMenu(a->menu(), el_part_menu);
				if(menu_bar) d->f_menuBarActionIds << id;
			}
			/*
			if(merge && !merge->isVisible()) {
				// merge jiz neco obsahuje, tak ho zviditelnime (vypada jako separator)
			merge->setVisible(true);
		}
			*/
		}
		else {
			/// toto menu uz existuje, takze merging bude mozna az v jeho detech
			if(m->menu()) updateMenu(m->menu(), el_part_menu);
		}
	}
}

QFAction* QFUiBuilder::action(const QString &id, bool throw_exc) throw(QFException)
{
	return QFUiBuilder::findAction(actions(), id, throw_exc);
}
		
QFAction* QFUiBuilder::findAction(const QList<QFAction*> &alist, const QString &name, bool throw_exc) throw(QFException)
{
	QFAction *ret = NULL;
	foreach(QFAction *a, alist) if(a->id() == name) {ret = a; break;}
	if(!ret && throw_exc) {
		QStringList sl; foreach(QFAction *a, alist) sl << a->id();
		QF_EXCEPTION("action '" + name + "' not found, current actions:" + sl.join(","));
	}
	return ret;
}
		
QString QFUiBuilder::uiId() 
{
	return document().cd("/", !Qf::ThrowExc).attribute("id");
}

QFPart::MainMenuPolicy QFUiBuilder::mainMenuPolicy()
{
	qfLogFuncFrame() << "id:" << uiId();
	QFPart::MainMenuPolicy ret = QFPart::ExtendMainMenu;
	QFDomElement el = document().cd(sMenuBar, !Qf::ThrowExc);
	QString s = el.attribute("mainmenupolicy");
	if(s.compare("replaceMainMenu", Qt::CaseInsensitive) == 0) ret = QFPart::ReplaceMainMenu;
	else if(s.compare("showInMainMenu", Qt::CaseInsensitive) == 0) ret = QFPart::ShowInMainMenu;
	qfTrash() << "\t return:" << QFPart::mainMenuPolicyToString(ret);
	return ret;
}

const QFDomDocument & QFUiBuilder::document(bool throw_exc) throw(QFException)
{
	if(d->doc.isEmpty()) {
		/// kdyz je doc i soubor, ze ktereho by se mel nahrat prazdny, je to asi bez *.ui.xml souboru
		if(uiFileName().isEmpty()) return d->doc;
		open(uiFileName(), throw_exc);
	}
	if(throw_exc && d->doc.isEmpty()) QF_EXCEPTION(tr("uixml document '%1' is empty or not loaded.").arg(uiFileName()));
	//qfInfo() << d->doc.toString();
	return d->doc;
}

