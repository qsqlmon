
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#include <qfpart.h>
#include <qftoolbar.h>

#include <QVBoxLayout>

#include "qfpartdialog.h"

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

QFPartDialog::QFPartDialog(QWidget * parent, Qt::WFlags f)
	: QDialog(parent, f), fPart(NULL)
{
}

QFPartDialog::~QFPartDialog()
{
}
		
QFPart* QFPartDialog::part(bool throw_exc) const throw(QFException)
{
	if(fPart) return fPart;
	if(throw_exc) QF_EXCEPTION("Part is NULL.");
	return NULL;
}
		
void QFPartDialog::setPart(QFPart *_part)
{
	fPart = _part;
	QVBoxLayout *ly = new QVBoxLayout(this);
	foreach(QFToolBar *tb, part()->toolBars()) ly->addWidget(tb);
	ly->addWidget(part()->partWidget());
	connect(part(), SIGNAL(wantClose(QFPart*, int)), this, SLOT(accept()));
}

void QFPartDialog::close(int result)
{
	if(result == QDialog::Accepted) accept();
	reject();
}

