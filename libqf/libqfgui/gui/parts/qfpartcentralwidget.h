// QFPartCentralWidget.h: interface for the QFPartCentralWidget class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(QFPARTCENTRALWIDGET_H)
#define QFPARTCENTRALWIDGET_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <QTabWidget>

class QFPart;
class QFPartWidget;
class QStackedWidget;

/**
Widget where all installed parts are placed.
 * @author	Voloda	<vladimir.kloz@centrum.cz>
 * @author	Fanda	<fanda.vacek@volny.cz>
 */
class QFPartCentralWidget : public QWidget
{
	Q_OBJECT
	public:
		QFPartCentralWidget(QWidget *pParent = NULL);
		virtual ~QFPartCentralWidget();
	signals:
		/**
		* Signal for QFPartManager.
	 	* Emited when the active widget has beeen changed
	 	* by the active widget container (user clicked on a page etc).
		*/
		void partActivated(QFPart*);
	public slots:
		//! Set part which will be active (on top of widget).
		virtual void setActivePart(QFPart *activated) = 0;
		//! Remove part from widget.
		virtual void removePart(QFPart *_part) = 0;
	public:
		//virtual void setTitle(QWidget *pWidget, const QString& sNewTitle = QString());
		//! Add part to widget (function does not activate this \a _part ).
		virtual void addPart(QFPart *_part) = 0;
		//! Return currently active part.
		virtual QFPart* activePart() = 0;
		//virtual QFPartWidget* activeWidget() = 0;
	protected:
		//virtual void removeWidget(QFPartWidget *pWidget) = 0;
		//virtual void indexOf(QWidget *widget) = 0;
		//virtual void setActiveWidget(QFPartWidget *pWidget) = 0;
		//virtual int currentIndex() = 0;
		//virtual void setCurrentIndex(int i) = 0;
};

/**
 * @author	fanda
 */
class QFPartTabbedCentralWidget : public QFPartCentralWidget
{
	Q_OBJECT
	private slots:
		void currentChanged(int pageno);
	protected:
		QTabWidget *fWidget;
		QTabWidget* widget() {return fWidget;}
		
		//virtual void setActivePartWidget(QFPart *activated);
	public:
		QFPartTabbedCentralWidget(QWidget *pParent = NULL);
		virtual ~QFPartTabbedCentralWidget() {};
	public:
		virtual void addPart(QFPart *_part);
		virtual void removePart(QFPart *_part);
		//virtual void indexOf(QWidget *widget);
		virtual QFPart* activePart();
		virtual void setActivePart(QFPart *activated);
		//virtual int currentIndex();
		//virtual void setCurrentIndex(int i);
};

/**
 * @author	fanda
 */
class QFPartStackedCentralWidget : public QFPartCentralWidget
{
	Q_OBJECT
	private slots:
		//void currentChanged(int pageno);
	protected:
		QStackedWidget *fWidget;
		QStackedWidget* widget() {return fWidget;}
		
		//virtual void setActivePartWidget(QFPart *activated);
	public:
		QFPartStackedCentralWidget(QWidget *pParent = NULL);
		virtual ~QFPartStackedCentralWidget() {};
	public:
		virtual void addPart(QFPart *_part);
		virtual void removePart(QFPart *_part);
		virtual QFPart* activePart();
		virtual void setActivePart(QFPart *activated);
};

#endif // !defined(QFPARTCENTRALWIDGET_H)
