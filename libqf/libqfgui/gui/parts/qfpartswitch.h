
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFPARTSWITCH_H
#define QFPARTSWITCH_H

#include <qfguiglobal.h>

#include <QToolBar>
#include <QToolButton>

class QFPartManager;
class QFPart;

class QFPartSwitchToolButton : public QToolButton
{
	Q_OBJECT
			friend class QFPartSwitch;
	protected:
		QFPart *part;
	public:
		QFPartSwitchToolButton(QWidget *parent, QFPart *_part);
	private slots:
		void toolButtonClicked() {emit clicked(part);}
	signals:
		void clicked(QFPart *part);
};

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFPartSwitch : public QToolBar
{
	Q_OBJECT
	private:
		QFPartManager *fPartManager;
	protected:
		QFPartManager* partManager() {return fPartManager;}
	public slots:
		virtual void addPart(QFPart *_part);
	private slots:
		void activatePart(QFPart *_part);
		void checkButton(QFPart *active_part);
	public:
		QFPartSwitch(QWidget *parent, QFPartManager *manager);
		virtual ~QFPartSwitch();
};

#endif // QFPARTSWITCH_H

