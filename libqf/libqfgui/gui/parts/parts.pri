# Input

INCLUDEPATH += $$PWD

HEADERS +=    \
    $$PWD/qfaction.h    \
    $$PWD/qfuibuilder.cpp    \
    $$PWD/qfpart.h    \
    $$PWD/qfmainwindow.h    \
    $$PWD/qfpartmanager.h    \
    $$PWD/qfpartwidget.h    \
    $$PWD/qfpartcentralwidget.h    \
	$$PWD/qfpartdialog.h    \
	$$PWD/qfpartswitch.h  \

SOURCES +=    \
    $$PWD/qfaction.cpp    \
    $$PWD/qfuibuilder.cpp    \
    $$PWD/qfpart.cpp    \
    $$PWD/qfmainwindow.cpp    \
    $$PWD/qfpartmanager.cpp    \
    $$PWD/qfpartwidget.cpp    \
    $$PWD/qfpartcentralwidget.cpp    \
	$$PWD/qfpartdialog.cpp    \
	$$PWD/qfpartswitch.cpp  \


