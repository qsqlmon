// QFPart.cpp: implementation of the QFPart class.
//
//////////////////////////////////////////////////////////////////////
#include "qfpart.h"

#include <qfmainwindow.h>
#include <qfaction.h>
#include <qfdockwidget.h>
#include <qfuibuilder.h>
#include <qfpartwidget.h>
#include <qfpartmanager.h>
#include <qftoolbar.h>

#include <QFile>
#include <QList>

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

QFPart::QFPart(QObject *parent, const QString &id, QFPartManager *manager)
	: QObject(parent)
{
	data.partManager = manager;
	setObjectName(id);
	//setMainMenuPolicy(ExtendMainMenu);
	//setCaption(partName());
}

QFPart::~QFPart()
{
	SAFE_DELETE(data.uiBuilder);
	/*
	m_pMainWindow->removePart(this);
	if (m_pObject)
	{
		delete m_pObject;
		m_pObject = NULL;
	}
	
	qDebug() << "~QFPart()";

	emit signalExited();
	*/
}
/*		
QFMainWindow* QFPart::mainWindow() const throw(QFException)
{
	return partManager()->mainWindow();
}
*/		
QFAction* QFPart::action(const QString &id, bool throw_exc) throw(QFException)
{
	return QFUiBuilder::findAction(actionList(), id, throw_exc);
}
		
QFUiBuilder* QFPart::uiBuilder()
{
	if(!data.uiBuilder) {
		data.uiBuilder = new QFUiBuilder(this, uiFileName());
		actionListRef() += data.uiBuilder->actions();
		//setMainMenuPolicy(data.uiBuilder->mainMenuPolicy());
	}
	return data.uiBuilder;
}
			
QFPart::ActionList& QFPart::actionListRef()
{
	uiBuilder(); /// zajisti, ze jsou vytvoreny akce
	return data.actionList;
}
	
void QFPart::releaseUi()
{
	foreach(QFToolBar *o, toolBarsRef()) if(o) o->deleteLater();
	toolBarsRef().clear();
	foreach(QFDockWidget *o, dockWidgetsRef()) if(o) o->deleteLater();
	//qDeleteAll(dockWidgetsRef());
	dockWidgetsRef().clear();
	if(data.partWidget) {
		data.partWidget->deleteLater();
		data.partWidget = NULL;
	}
}

void QFPart::buildUi() throw(QFException)
{
	qfTrash() << QF_FUNC_NAME << "  className:" << this->metaObject()->className();
	releaseUi();
	QFUiBuilder& b = *uiBuilder();
#if 0
	QMapIterator<QString, QFAction*> i(m);
	while (i.hasNext()) {
		i.next();
		//qfTrash() << QFLog::nospace << "\tactionmap[" << i.key() << "] = " << i.value();
		actionMapRef()[i.key()] = i.value();
	}
	{
		qfTrash() << "\tmap:";
		QMapIterator<QString, QFAction*> i(actionMapRef());
		while (i.hasNext()) {
			i.next();
			qfTrash() << QFLog::nospace << "\t\t[" << i.key() << "] = " << i.value();
		}
	}
	#endif
	//actionMapRef() = b.actionMap();
	data.toolBarList = b.createToolBars();
	//fToolBarList = b.toolBars();
	connectActions(this);
	//if(caption().isEmpty()) setCaption(b.caption());
}
/*
void QFPart::objectWalk(QObject *o)
{
	qfTrash() << "object walk:" << o;
}
*/

void QFPart::updateMenuBar(QMenuBar *mb)
{
	qfTrash() << QF_FUNC_NAME;
	uiBuilder()->updateMenuOrBar(mb);
}

void QFPart::connectActions(QObject *receiver, const QString &slot_prefix)
{
	qfLogFuncFrame() << "part name:" << partName() << "slot_prefix:" << slot_prefix;
	//qfInfo() << QFLog::stackTrace();
	QFUiBuilder::connectActions(actionList(), receiver, slot_prefix);
}
		
QFPartManager* QFPart::partManager(bool throw_exc) const throw(QFException)
{
	if(data.partManager) return data.partManager;
	if(throw_exc) QF_EXCEPTION("Part manager is NULL");
	return NULL;
}

QString QFPart::caption()
{
	if(data.caption.isEmpty()) data.caption = uiBuilder()->caption();
	if(data.caption.isEmpty()) data.caption = partName();
	return data.caption;
}

QIcon QFPart::icon()
{
	if(data.icon.isNull()) data.icon = uiBuilder()->icon();
	return data.icon;
}

QFPart::MainMenuPolicy QFPart::mainMenuPolicy()
{
	qfLogFuncFrame();
	if(data.mainMenuPolicy == QFPart::MainMenuPolicyNotExplicitlySet) data.mainMenuPolicy = uiBuilder()->mainMenuPolicy();
	return data.mainMenuPolicy;
}

void QFPart::activated()
{
	qfLogFuncFrame();
	emit partActivated(this);
}

QString QFPart::mainMenuPolicyToString(MainMenuPolicy policy)
{
	QString ret;
	switch(policy) {
		case ExtendMainMenu: ret = "ExtendMainMenu"; break;
		case ReplaceMainMenu: ret = "ReplaceMainMenu"; break;
		case ShowInMainMenu: ret = "ShowInMainMenu"; break;
		default: ret = "MainMenuPolicyNotExplicitlySet"; break;
	}
	return ret;
}


