//
// C++ Implementation: qfpartwidget
//
// Description: 
//
//
// Author: Fanda Vacek <fanda.vacek@volny.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "qfpartwidget.h"
#include "qfdialogwidget.h"

#include <qfdialogwidgetstack.h>

#include <QVBoxLayout>

#include <qflogcust.h>

//================================================================
//                          QFPartWidget
//================================================================
QFPartWidget::QFPartWidget(QFPart *_part, QWidget *parent)
 : QWidget(parent), fPart(_part)
{
	qfLogFuncFrame();
	captionFrame = NULL;
	/*
	createCaption();
	if(captionFrame) {
		QString s = _part->caption();
		qfTrash() << "\t captionFrame:" << captionFrame;
		qfTrash() << "\t caption text:" << s;
	//qfTrash() << "\t caption label:" << captionFrame->;
		if(!s.isEmpty()) captionFrame->setText(s);
		else captionFrame->setVisible(false);
	}
	*/
	f_centralWidget = NULL;
}

QFPartWidget::~QFPartWidget()
{
}

QWidget * QFPartWidget::centralWidget()
{
	if(!f_centralWidget) {
		f_centralWidget = new QWidget();
		QBoxLayout *ly = new QVBoxLayout(this);
		ly->setMargin(0);
		ly->setSpacing(1);
		ly->addWidget(f_centralWidget);
	}
	return f_centralWidget;
}

void QFPartWidget::createCaption()
{
	if(captionFrame == NULL) {
		captionFrame = new QFDialogWidgetCaptionFrame();
		captionFrame->setCloseButtonVisible(false);
		//connect(captionFrame, SIGNAL(closeButtonClicked()), this, SLOT(captionFrameCloseButtonClicked()));
		QBoxLayout *ly = qobject_cast<QBoxLayout*>(this->layout());
		ly->insertWidget(0, captionFrame);
	}
}

void QFPartWidget::setCaptionText(const QString & s)
{
	qfLogFuncFrame() << s;
	if(!s.isEmpty()) {
		createCaption();
		qfLogFuncFrame() << "captionFrame:" << captionFrame;
		if(captionFrame) captionFrame->setText(s);
	}
}

void QFPartWidget::setCaptionIcon(const QIcon & ico)
{
	if(!ico.isNull()) {
		createCaption();
		if(captionFrame) captionFrame->setIcon(ico);
	}
}

//================================================================
//                          QFStackedPartWidget
//================================================================
QFStackedPartWidget::QFStackedPartWidget(QFPart * _part, QWidget * parent)
	: QFPartWidget(_part, parent)
{
}

QWidget * QFStackedPartWidget::centralWidget()
{
	if(!f_centralWidget) {
		f_centralWidget = new QFDialogWidgetStack();
		QBoxLayout *ly = new QVBoxLayout(this);
		ly->setMargin(0);
		ly->setSpacing(1);
		ly->addWidget(f_centralWidget);
	}
	return f_centralWidget;
}

QFDialogWidgetStack * QFStackedPartWidget::dialogWidgetStack()
{
	QFDialogWidgetStack *ret = qobject_cast<QFDialogWidgetStack*>(centralWidget());
	QF_ASSERT(ret, "Central widget is not a QStackedWidget");
	return ret;
}
