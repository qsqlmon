#! /usr/bin/python
# -*- coding: utf-8 -*-

#from __future__ import with_statement
import os, sys
import xml.dom.minidom #import parse #, parseString
from lxml import etree
import codecs

def processDb():
	import MySQLdb
	db = MySQLdb.connect('localhost', 'root', 'toor', 'woffice', use_unicode=True, charset='utf8')
	c =db.cursor()
	c.execute('SELECT groupName, abbreviation, caption FROM enumz')
	for r in c:
		group_name = r[0]
		abbr = encodeOctalUtf8(r[1])
		caption = encodeOctalUtf8(r[2])
		#print 'repr:', repr(caption)
		#caption = encodeOctalUtf8(r[2])
		if caption:
			s = 'QT_TRANSLATE_NOOP_UTF8("db.enumz.%s", "%s", "%s");' % (group_name, caption, 'caption')
			print s.encode('utf-8')
		if abbr:
			s = 'QT_TRANSLATE_NOOP_UTF8("db.enumz.%s", "%s", "%s");' % (group_name, abbr, 'abbreviation')
			print s.encode('utf-8')

def processDir(dir_path, localize_what):
	#print '//processing dir:', dir_path
	#sys.stderr.write('processing dir: %s localize_what: %s\n' % (dir_path, localize_what))
	for root, dirs, files in os.walk(dir_path):
		#sys.stderr.write('\t root: %s dirs: %s files: %s\n' % (root, dirs, files))
		#print '\t root:', root, dirs, files
		for fn in files:
			processFile(os.path.join(root, fn), localize_what)
		for name in dirs:
			processDir(os.path.join(root, name), localize_what)

def localizeConfXmlNode(node, file_path):
	for n in node.childNodes:
		if n.nodeType == n.ELEMENT_NODE:
			nn = n.nodeName
			if nn in ['value', 'options', 'help', 'default']: continue
			caption = n.getAttribute('caption')
			if not caption: caption = n.nodeName
			if caption:
				s = 'QT_TRANSLATE_NOOP_UTF8("confxml", "%s", "%s");' % (caption, file_path)
				print s.encode('utf-8')
			localizeConfXmlNode(n, file_path)
			
def processFile(file_path, localize_what):
	if localize_what == 'reports':
		if file_path.endswith('.rep') or file_path.endswith('.inc.xml'):
			print '//processing file:', file_path
			sys.stderr.write('processing file: %s\n' % file_path)
			dom = xml.dom.minidom.parse(file_path)
			for node in dom.getElementsByTagName('para'):
				cnt = 1
				s = ''
				has_text = False
				for n in node.childNodes:
					if n.nodeType == n.TEXT_NODE:
						s = s + n.data
						if not n.data.isspace():
							has_text = True
					else:
						s = s + '%' + str(cnt)
						cnt = cnt + 1
				if has_text:
					#f.write(ivan_uni.encode('utf-8'))
					s = s.replace('\n', '\\n')
					s = 'QT_TRANSLATE_NOOP_UTF8("%s", "%s", "%s");' % (localize_what, s, file_path)
					print s.encode('utf-8')
	elif localize_what == 'uixml':
		if file_path.endswith('.ui.xml'):
			print '//processing file:', file_path
			sys.stderr.write('processing file: %s\n' % file_path)
			dom = xml.dom.minidom.parse(file_path)
			for node in dom.getElementsByTagName('caption'):
				for n in node.childNodes:
					if n.nodeType == n.TEXT_NODE:
						s = n.data
						s = 'QT_TRANSLATE_NOOP_UTF8("uixml", "%s", "%s");' % (s, file_path)
						print s.encode('utf-8')
						break
	elif localize_what == 'confxml':
		if file_path.endswith('.conf.xml'):
			print '//processing file:', file_path
			sys.stderr.write('processing file: %s\n' % file_path)
			dom = xml.dom.minidom.parse(file_path)
			localizeConfXmlNode(dom, file_path)
	elif localize_what == 'ui':
		if file_path.endswith('.ui'):
			print '//processing file:', file_path
			sys.stderr.write('processing file: %s\n' % file_path)
			dom = xml.dom.minidom.parse(file_path)
			class_name = ''
			for node in dom.getElementsByTagName('class'):
				for n in node.childNodes:
					if n.nodeType == n.TEXT_NODE:
						class_name = n.data
						break
				break
			print '// class_name:', class_name
			for el in dom.getElementsByTagName('property'):
				if el.getAttribute('name') == 'styleSheet': continue
				for n in el.childNodes:
					#print '//\t\t child', n
					if n.nodeType == n.ELEMENT_NODE:
						#print '//\t\t child', n.tagName
						if n.tagName == 'string' and n.getAttribute('notr') != 'true':
							for n1 in n.childNodes:
								if n1.nodeType == n1.TEXT_NODE:
									print '//\t property:', el.getAttribute('name')
									s = n1.data
									s = s.replace('\n', '\\n')
									#sys.stderr.write('text: %s\n' % n1)
									#sys.stderr.write('text: %s\n' % s.encode('utf-8'))
									s = 'QT_TRANSLATE_NOOP_UTF8("%s", "%s", "%s");' % (class_name, s, file_path)
									print s.encode('utf-8')
									break
			for el in dom.getElementsByTagName('attribute'):
				if el.getAttribute('name') != 'title': continue
				for n in el.childNodes:
					#print '//\t\t child', n
					if n.nodeType == n.ELEMENT_NODE:
						#print '//\t\t child', n.tagName
						if n.tagName == 'string' and n.getAttribute('notr') != 'true':
							for n1 in n.childNodes:
								if n1.nodeType == n1.TEXT_NODE:
									print '//\t attribute:', el.getAttribute('name')
									s = n1.data
									s = s.replace('\n', '\\n')
									#sys.stderr.write('text: %s\n' % n1)
									#sys.stderr.write('text: %s\n' % s.encode('utf-8'))
									s = 'QT_TRANSLATE_NOOP_UTF8("%s", "%s", "%s");' % (class_name, s, file_path)
									print s.encode('utf-8')
									break
		
def encodeOctalUtf8(unicode_str):
	#if isinstance(obj, basestring):
		#if not isinstance(obj, unicode):
			#obj = unicode(obj, encoding)	
	s = unicode_str.encode('utf-8')
	ret = ''
	for c in s:
		if ord(c) < 128: ret = ret + c
		else: ret = ret + '\\%o' % ord(c)
	return ret
			
def help():
	print
	print help_msg % (app_name)
	print
	sys.exit(0)

#===========================================================
if __name__ == '__main__':
	help_msg = '''SYNOPSIS: %s [options] dir

Create dummy C++ source file to help to localize reports in dir and subdirs, output is written to stdout.

OPTIONS:
	-h : this help screen
	-reports : localize report files
	-uixml : localize *.ui.xml files
	-ui : localize *.ui files
	-db : localize database content
'''
	argv = sys.argv
	app_name = argv[0]
	argv = argv[1:]
	
	root_dir = os.getcwd()
	localize_what = ''

	nsmap={'a':'http://www.w3.org/1999/xhtml'}

	while len(argv) > 0:
		s = argv[0]
		if s[0:1] == '-':
			s = s[1:]
			if s == 'h':
				help()
			elif s == 'reports':
				localize_what = s
			elif s == 'uixml':
				localize_what = s
			elif s == 'confxml':
				localize_what = s
			elif s == 'ui':
				localize_what = s
			elif s == 'db':
				localize_what = s
			argv = argv[1:]
		else:
			root_dir = s 
			break

	if not localize_what:
		help()

	print '#if 0 // dont realy compile translations to application'
	if localize_what == 'db':
		processDb()
	else:
		processDir(root_dir, localize_what = localize_what)
	print '#endif'
