
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdbenum.h"

#include <qfsqlquery.h>
#include <qfdbapplication.h>

#include <QSqlRecord>

#include <qflogcust.h>

//============================================================
//                              QFDbEnum
//============================================================
QMap<QString, int> QFDbEnum::fieldMapping;

QFDbEnum::QFDbEnum(int id, const QString & group_name, const QString & group_id, const QString & caption)
{
	setId(id);
	setGroupName(group_name);
	setGroupId(group_id);
	setCaption(caption);
}

QFDbEnum::QFDbEnum(const QFSqlQuery & q)
{
	qfLogFuncFrame();
	QSqlRecord rec = q.record();
	qfTrash() << "\t" << rec.value(0).toString() << rec.value(1).toString();
	if(fieldMapping.isEmpty()) {
		for(int i=0; i<rec.count(); i++) {
			QString fld_name = rec.fieldName(i);
			if(QFSql::sqlIdCmp(fld_name, "id")) fieldMapping[fld_name] = FieldId;
			else if(QFSql::sqlIdCmp(fld_name, "groupName")) fieldMapping[fld_name] = FieldGroupName;
			else if(QFSql::sqlIdCmp(fld_name, "groupId")) fieldMapping[fld_name] = FieldGroupId;
			else if(QFSql::sqlIdCmp(fld_name, "pos")) fieldMapping[fld_name] = FieldPos;
			else if(QFSql::sqlIdCmp(fld_name, "abbreviation")) fieldMapping[fld_name] = FieldAbbreviation;
			else if(QFSql::sqlIdCmp(fld_name, "value")) fieldMapping[fld_name] = FieldValue;
			else if(QFSql::sqlIdCmp(fld_name, "caption")) fieldMapping[fld_name] = FieldCaption;
			else if(QFSql::sqlIdCmp(fld_name, "userText")) fieldMapping[fld_name] = FieldUserText;
		}
	}
	for(int i=0; i<LastFieldIndex; i++) values << QVariant();
	for(int i=0; i<rec.count(); i++) {
		qfTrash() << "\t" << i << "field:" << rec.fieldName(i);
		QString fld_name = rec.fieldName(i);
		int fld_ix = fieldMapping.value(fld_name, -1);
		if(fld_ix < 0) continue;
		QVariant v = rec.value(i);
		if(fld_ix == FieldValue) {
			QString s = v.toString();
			if(s.contains("config://")) { /// startsWith() tu nemuze bejt, protoze je ot neco jako appconfig://.....
				qfTrash() << "\t config key:" << s;
				v = qfDbApp()->configValue(s);
				qfTrash() << "\t config value:" << v.toString();
			}
		}
		values[fld_ix] = v;
	}
	if(caption().isEmpty()) values[FieldCaption] = groupId();
}

void QFDbEnum::setValue(QFDbEnum::FieldIndexes ix, const QVariant & val)
{
	if(values.isEmpty()) for(int i=0; i<LastFieldIndex; i++) values << QVariant();
	values[ix] = val;
}

//============================================================
//                              QFDbEnumCache
//============================================================
void QFDbEnumCache::clear(const QString & group_name)
{
	if(group_name.isEmpty()) enumCache.clear();
	else enumCache.remove(group_name);
}

void QFDbEnumCache::reload(const QString & group_name)
{
	clear(group_name);
	QFSqlQuery q(qfDbApp()->connection());
	q.exec("SELECT * FROM enumz WHERE groupName="SARG(group_name)" ORDER BY pos");
	while(q.next()) {
		QFDbEnum en(q);
		enumCache[group_name] << en;
	}
}

void QFDbEnumCache::ensure(const QString & group_name)
{
	if(!enumCache.contains(group_name)) reload(group_name);
}

QFDbEnumCache::EnumList QFDbEnumCache::dbEnumsForGroup(const QString & group_name)
{
	ensure(group_name);
	return enumCache.value(group_name);
}

QFDbEnum QFDbEnumCache::dbEnum(const QString & group_name, const QString & group_id)
{
	EnumList enmlst = dbEnumsForGroup(group_name);
	foreach(QFDbEnum en, enmlst) if(en.groupId() == group_id) return en;
	//QF_EXCEPTION(QString("group_id"));
	return QFDbEnum();
}

