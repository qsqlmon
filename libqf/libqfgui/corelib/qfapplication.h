#ifndef QFAPPLICATION_H
#define QFAPPLICATION_H

#include "qfappconfiginterface.h"
#include "qfappinterface.h"

#include <qfguiglobal.h>

#include <QApplication>
#include <QFileDialog>
#include <QDir>

class QFSqlConnection;

class QFGUI_DECL_EXPORT QFApplicationNotify
{
	public:
		enum {Last=99};
};

//! Tady chybi dokumentace.
class QFGUI_DECL_EXPORT QFApplication : public QApplication, public QFAppConfigInterface, public QFAppInterface
{
	Q_OBJECT
	public:
		typedef QSet<QString> StringSet;
	protected:
		//! id zprav, ktere se nemaji priste zobrazovat
		StringSet *fMessagesIdNotToShow;
		StringSet& messagesIdNotToShowRef();
	public:
		const StringSet& messagesIdNotToShow() {return messagesIdNotToShowRef();}
		void addMessageIdNotToShow(const QString &id);
		void saveMessagesIdNotToShow();
		void clearMessagesIdNotToShow();
	protected:
		//QFSplittedXmlConfig f_dbconfig;
	public:
		typedef QMap<QString, QVariant> Params;
	public:
		enum AppNotifyFlag {
			NotifyAppWide = 0x1,
			NotifySystemWide = 0x2 /// use DBUS
		};
		Q_DECLARE_FLAGS(AppNotifyFlags, AppNotifyFlag);

		//! params nemohou byt typu QVariant, protoze by neslo pouzivat QueuedConnection
		virtual void emitAppNotify(int notify_id, const QVariantMap &params = QVariantMap(), AppNotifyFlags flags = NotifyAppWide)
		{
			Q_UNUSED(flags); /// do budoucna muze tyto notifikace aplikace posilat dal skrz DBUS pro NotifySystemWide.
			emit appNotify(notify_id, params);
		}
	signals:
		void appNotify(int notify_id, const QVariantMap &params = QVariantMap());
	public:
		/// read only data pro applikaci, napr. pod jakym heslem se ma prihlasit na server
		virtual QFXmlConfig* globalConfig() {QF_EXCEPTION("globalConfig not set."); return NULL;}
		/// persistentni data pro stanici, napr jmeno naposledy prihlaseneho uzivatele
		virtual QFXmlConfig* localConfig() {QF_EXCEPTION("localConfig not set."); return NULL;}
		/// persistentni data per user, ulozene v SQL serveru
		virtual QFXmlConfig* userConfig() {QF_EXCEPTION("userConfig not set."); return NULL;}
		/// persistentni data per application, ulozene v SQL serveru
		virtual QFXmlConfig* appConfig() {QF_EXCEPTION("appConfig not set."); return NULL;}
		
		/// default config
		virtual QFXmlConfig* config();
		
		//! path muze zacinat na appconfig://, globalconfig://, localconfig://, userconfig://
		//! pokud neobsahuje oddelovac ://, prida se pred cestu appconfig://
		virtual QVariant configValue(const QString &path, const QVariant &default_value = QVariant());
		virtual void setConfigValue(const QString &path, const QVariant &value);
		
		virtual bool currentUserHasGrant(const QString &grant) {Q_UNUSED(grant); return true;}
		
		static QFApplication* instance();
	public:
		QString getOpenFileName(QWidget *parent = 0, const QString &caption = QString(),
					const QString &dir = QString(), const QString &filter = QString(),
					QString *selectedFilter = 0, QFileDialog::Options options = 0);
		QStringList getOpenFileNames(QWidget *parent = 0, const QString &caption = QString(),
					const QString &dir = QString(), const QString &filter = QString(),
					QString *selectedFilter = 0, QFileDialog::Options options = 0);
		QString getSaveFileName(QWidget * parent = 0, const QString & caption = QString(),
					const QString & dir = QString(), const QString & filter = QString(),
					QString * selectedFilter = 0, QFileDialog::Options options = 0);
	protected:
		/// vraci adresar, ktery se otviral pri posledni operaci open
		QString recentOpenDir();
		void setRecentOpenDir(const QString &s);
		QString recentSaveDir();
		void setRecentSaveDir(const QString &s);
	public:
		QFApplication(int & argc, char ** argv, bool GUIenabled = true);
		~QFApplication();
};

Q_DECLARE_OPERATORS_FOR_FLAGS(QFApplication::AppNotifyFlags);

inline QFApplication* qfApp() {return QFApplication::instance();}

#endif // QFAPPLICATION_H
