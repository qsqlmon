#include <qfassert.h>
#include <qfdlgexception.h>
#include <qfmsgshowagain.h>
#include <qffileutils.h>

#include "qfapplication.h"

#include <QCleanlooksStyle>

//#define QF_NO_TRASH_OUTPUT
#include <qflogcust.h>

QFApplication::QFApplication(int & argc, char ** argv, bool GUIenabled)
	: QApplication(argc, argv, GUIenabled)
{
	fMessagesIdNotToShow = NULL;
	setStyle(new QCleanlooksStyle());
#ifdef Q_WS_X11
#endif
}

QFApplication::~QFApplication()
{
	qfTrash() << QF_FUNC_NAME;
	/// tady je pozde neco ukladat, config vetsinou uz neexistuje
	//QFMsgShowAgain::savePersistentData();
	SAFE_DELETE(fMessagesIdNotToShow);
}

QFXmlConfig* QFApplication::config()
{
	if(f_config.templateDocument().isEmpty()) {
		qfTrash() << QF_FUNC_NAME;
		try {
			QFAppConfigInterface::config();
		}
		catch(QFException &e) {
			QFDlgException dlg; dlg.exec(e);
		}
		//QF_ASSERT(!f_config.templateDocument().isEmpty(), tr("Nenalezena konfigurace"));
	}
	return &f_config;
}

QFApplication* QFApplication::instance()
{
	QFApplication *a = qobject_cast<QFApplication*>(QApplication::instance());
	if(!a) QFLog(QFLog::LOG_FATAL, "ASSERT") << QFLog::stackTrace();
	//QF_ASSERT(a, "aplikace dosud neni inicializovana");
	return a;
}

/*
QDir QFApplication::ensureSettingsDir() throw(QFException)
{
	QString dir = settingsDir();
	QDir d(dir);
	if(!d.exists()) {
		if(!d.mkpath(dir)) {
			QF_EXCEPTION(QObject::tr("Cann't create configuration directory '%1'").arg(dir));
		}
		/// vypada to, ze ve windovs, pokud cesta existovala, tak se vytvori, ale bez nasledujiciho radku to vrati currentDir a ne settingsDir, jako v linuxu
		d = QDir(dir);
	}
	return d;
}
*/
QString QFApplication::recentOpenDir()
{
	qfLogFuncFrame();
	QString s = config()->persistentPath("recentOpenDir").value(QVariant()).toString();
	return s;
}


void QFApplication::setRecentOpenDir(const QString &s)
{
	qfLogFuncFrame() << s;
	QFXmlConfigElement el = config()->createPersistentPath("recentOpenDir");
	config()->setValue(el.path(), s);
}

QString QFApplication::recentSaveDir()
{
	QString s = config()->persistentPath("recentSaveDir").value(QVariant()).toString();
	return s;
}

void QFApplication::setRecentSaveDir(const QString &s)
{
	qfLogFuncFrame() << s;
	QFXmlConfigElement el = config()->createPersistentPath("recentSaveDir");
	config()->setValue(el.path(), s);
}

QString QFApplication::getOpenFileName(QWidget *parent, const QString &caption,
								const QString &_dir, const QString &filter,
								QString *selectedFilter, QFileDialog::Options options)
{
	QString fn = _dir;
	QString dir = QFFileUtils::path(fn);
	if(dir.isEmpty()) {
		dir = recentOpenDir();
		fn = QFFileUtils::joinPath(dir, fn);
	}
	QString ret = QFileDialog::getOpenFileName(parent, caption, fn, filter, selectedFilter, options);
	if(!ret.isEmpty()) {
		setRecentOpenDir(QFFileUtils::path(ret));
	}
	return ret;
}

QStringList QFApplication::getOpenFileNames(QWidget *parent, const QString &caption,
				       const QString &_dir, const QString &filter,
	   				QString *selectedFilter, QFileDialog::Options options)
{
	QString fn = _dir;
	QString dir = QFFileUtils::path(fn);
	if(dir.isEmpty()) {
		dir = recentOpenDir();
		fn = QFFileUtils::joinPath(dir, fn);
	}
	QStringList ret = QFileDialog::getOpenFileNames(parent, caption, fn, filter, selectedFilter, options);
	if(!ret.isEmpty()) {
		setRecentOpenDir(QFFileUtils::path(ret[0]));
	}
	return ret;
}

QString QFApplication::getSaveFileName(QWidget * parent, const QString & caption,
								const QString & _dir, const QString & filter,
								QString * selectedFilter, QFileDialog::Options options)
{
	qfLogFuncFrame();
	QString fn = _dir;
	qfTrash() << "\t fn:" << fn;
	QString dir = QFFileUtils::path(fn);
	qfTrash() << "\t dir:" << dir << "isAbsolutePath:" << QDir::isAbsolutePath(dir);
	if(dir.isEmpty() || !QDir::isAbsolutePath(dir)) {
		dir = recentSaveDir();
		fn = QFFileUtils::joinPath(dir, fn);
	}
	qfTrash() << "\t fn2:" << fn;
	QString ret = QFileDialog::getSaveFileName(parent, caption, fn, filter, selectedFilter, options);
	if(!ret.isEmpty()) {
		setRecentSaveDir(QFFileUtils::path(ret));
	}
	return ret;
}

QFApplication::StringSet& QFApplication::messagesIdNotToShowRef()
{
	if(!fMessagesIdNotToShow) {
		fMessagesIdNotToShow = new StringSet();
		QFXmlConfigElement el = config()->persistentPath("messages");
		if(el) {
			QString s = el.value("showagainbenlist", QVariant()).toString();
			foreach(s, s.split("{sep}")) *fMessagesIdNotToShow << s;
		}
	}
	return *fMessagesIdNotToShow;
}

void QFApplication::clearMessagesIdNotToShow()
{
	messagesIdNotToShowRef().clear();
	saveMessagesIdNotToShow();
}

void QFApplication::saveMessagesIdNotToShow()
{
	qfLogFuncFrame();
	QStringList sl;
	foreach(QString s, messagesIdNotToShow()) {
		//qfInfo() << s;
		if(!s.isEmpty()) sl << s;
	}
	QFXmlConfigElement el = qfApp()->config()->createPersistentPath("messages");
	QString path = el.path() + "/showagainbenlist";
	//qfInfo() << path << sl.join("{sep}");
	qfApp()->config()->setValue(path, sl.join("{sep}"));
}

void QFApplication::addMessageIdNotToShow(const QString & id)
{
	qfLogFuncFrame() << "id:" << id;
	messagesIdNotToShowRef() << id;
}

QVariant QFApplication::configValue(const QString & _path, const QVariant & default_value)
{
	qfLogFuncFrame() << "path:" << _path;
	QString config_str;
	QFString path = _path;
	int ix = path.indexOf("://");
	if(ix > 0) {
		config_str = path.slice(0, ix);
		path = path.slice(ix + 2);
	}
	QVariant ret;
	if(config_str.isEmpty()) config_str  = "appconfig";
	qfTrash() << "\t config:" << config_str << "path:" << path;
	if(config_str == "globalconfig") ret = globalConfig()->value(path, default_value);
	else if(config_str == "localconfig") ret = localConfig()->value(path, default_value);
	else if(config_str == "userconfig") ret = userConfig()->value(path, default_value);
	else ret = appConfig()->value(path, default_value);
	qfTrash() << "\t return:" << ret.toString();
	return ret;
}

void QFApplication::setConfigValue(const QString & _path, const QVariant & value)
{
	qfLogFuncFrame() << "path:" << _path;
	QString config_str;
	QFString path = _path;
	int ix = path.indexOf("://");
	if(ix > 0) {
		config_str = path.slice(0, ix);
		path = path.slice(ix + 2);
	}
	if(config_str.isEmpty()) config_str  = "appconfig";
	//qfTrash() << "\t config:" << config_str << "path:" << path;
	if(config_str == "globalconfig") globalConfig()->setValue(path, value);
	else if(config_str == "localconfig") localConfig()->setValue(path, value);
	else if(config_str == "userconfig") userConfig()->setValue(path, value);
	else appConfig()->setValue(path, value);
}

