
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfgui.h"

#include <QList>
#include<QWidget>
#include<QLayoutItem>
#include<QLayout>

#include <qflogcust.h>

void QFGui::setPropertyInChildWidgets(QWidget * parent, const char * property, const QVariant & value)
{
	QList<QWidget*> lst = parent->findChildren<QWidget*>();
	foreach(QWidget *w, lst) {
		int ix = w->metaObject()->indexOfProperty(property);
		if(ix >= 0) {
			w->setProperty(property, value);
		}
	}
}

static QLayout* find_widget_layout(QLayout *parent_layout, QWidget *widget)
{
	QLayout *ret = NULL;
	if(!parent_layout || !widget) return ret;
	qfLogFuncFrame() << "\t parent layout:" << parent_layout;
	for(int i=0; i<parent_layout->count(); i++) {
		QLayoutItem *lit = parent_layout->itemAt(i);
		qfTrash() << "\t layout item:" << lit;
		if(QLayout *ly = lit->layout()) {
			qfTrash() << "\t looking in layout:" << ly->objectName();
			ret = find_widget_layout(ly, widget);
			if(ret) break;
		}
		else if(QWidget *w = lit->widget()) {
			qfTrash() << "\t looking in widget:" << w->objectName();
			if(w == widget) {
				ret = parent_layout;
			}
			else {
				ret = find_widget_layout(w->layout(), widget);
			}
			if(ret) break;
		}
	}
	return ret;
}

QLayout * QFGui::findWidgetLayout(QWidget * widget)
{
	QLayout *ret = NULL;
	if(widget) {
		QWidget *w = widget->parentWidget();
		if(w) ret = find_widget_layout(w->layout(), widget);
	}
	return ret;
}

void QFGui::deleteChildWidgetsAndLayouts(QWidget * parent_widget)
{
	if(!parent_widget) return;
	foreach(QObject *o, parent_widget->children()) {
		if(QWidget *w = qobject_cast<QWidget*>(o)) {
			delete w;
		}
	}
	qDeleteAll(parent_widget->findChildren<QLayout*>());
}
