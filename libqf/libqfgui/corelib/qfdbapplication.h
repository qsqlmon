
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDBAPPLICATION_H
#define QFDBAPPLICATION_H

#include "qfappdbconnectioninterface.h"

#include <qfguiglobal.h>
#include <qfapplication.h>
#include <qfdbenum.h>

//! Tady chybi dokumentace.
class QFGUI_DECL_EXPORT QFDbApplication : public QFApplication, public QFAppDbConnectionInterface
{
	Q_OBJECT;
	protected:
		QFDbEnumCache *f_enumCache;
	protected:
	public:
		static QFDbApplication* instance();

		/// takes ownership of \a enum_cache
		void setEnumCache(QFDbEnumCache *enum_cache) {SAFE_DELETE(f_enumCache); f_enumCache = enum_cache;}

		QFDbEnumCache* enumCache();
		QFDbEnum dbEnum(const QString &group_name, const QString &group_id);
		QFDbEnumCache::EnumList dbEnumsForGroup(const QString &group_name);
	public:
		QFDbApplication(int & argc, char ** argv, bool GUIenabled = true);
		~QFDbApplication();
};

inline QFDbApplication* qfDbApp() {return QFDbApplication::instance();}
   
#endif // QFDBAPPLICATION_H

