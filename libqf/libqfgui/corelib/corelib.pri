# Input

INCLUDEPATH += $$PWD

HEADERS +=               \
	$$PWD/qfapplication.h               \
	$$PWD/qfdbapplication.h         \
	$$PWD/qfgui.h   \
	$$PWD/qfdbenum.h  \

SOURCES +=               \
	$$PWD/qfapplication.cpp               \
	$$PWD/qfdbapplication.cpp         \
	$$PWD/qfgui.cpp   \
	$$PWD/qfdbenum.cpp  \


