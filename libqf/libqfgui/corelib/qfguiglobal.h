#ifndef QFGUIGLOBAL_H
#define	QFGUIGLOBAL_H

#include <qglobal.h>

/// Declaration of macros required for exporting symbols
/// into shared libraries
#if defined(QFGUI_BUILD_DLL)
#  define QFGUI_DECL_EXPORT Q_DECL_EXPORT
#else
#  define QFGUI_DECL_EXPORT Q_DECL_IMPORT
#endif

#endif // QFGUIGLOBAL_H
