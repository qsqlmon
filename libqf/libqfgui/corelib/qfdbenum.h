
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFDBENUM_H
#define QFDBENUM_H

#include <QVariantList>

#include <qfguiglobal.h>

class QFSqlQuery;

class QFGUI_DECL_EXPORT QFDbEnum
{
	protected:
		QVariantList values;
		static QMap<QString, int> fieldMapping;
	public:
		enum FieldIndexes {FieldId = 0, FieldGroupName, FieldGroupId, FieldPos, FieldAbbreviation, FieldValue, FieldCaption, FieldUserText, LastFieldIndex};
	protected:
		void setValue(FieldIndexes ix, const QVariant &val);
	public:
		QString id() const {return values.value(FieldId).toString();}
		void setId(int id) {setValue(FieldId, id);}
		QString groupName() const {return values.value(FieldGroupName).toString();}
		void setGroupName(const QString &s) {setValue(FieldGroupName, s);}
		QString groupId() const {return values.value(FieldGroupId).toString();}
		void setGroupId(const QString &s) {setValue(FieldGroupId, s);}
		int pos() const {return values.value(FieldPos).toInt();}
		QString abbreviation() const {return values.value(FieldAbbreviation).toString();}
		QVariant value() const {return values.value(FieldValue);}
		QString caption() const {return values.value(FieldCaption).toString();}
		void setCaption(const QString &s) {setValue(FieldCaption, s);}
		QString userText() const {return values.value(FieldUserText).toString();}
		void setUserText(const QString &s) {setValue(FieldUserText, s);}
		bool isValid() const {return !values.isEmpty();}
	public:
		QFDbEnum() {}
		QFDbEnum(const QFSqlQuery &q);
		QFDbEnum(int id, const QString &group_name, const QString &group_id, const QString &caption);
};

class QFGUI_DECL_EXPORT QFDbEnumCache
{
	public:
		typedef QList<QFDbEnum> EnumList;
	protected:
		QMap<QString, EnumList> enumCache;
	public:
		EnumList dbEnumsForGroup(const QString &group_name);
		QFDbEnum dbEnum(const QString &group_name, const QString &group_id);

		bool isEmpty() const {return enumCache.isEmpty();}
		virtual void reload(const QString &group_name);
		//! kdyz je group_name prazdne, vymaze se vse
		void clear(const QString &group_name = QString());
		/// pokud group_name neni v kesi, pokusi se ho nacist
		void ensure(const QString & group_name);
	public:
		QFDbEnumCache() {}
		virtual ~QFDbEnumCache() {}
};

#endif // QFDBENUM_H

