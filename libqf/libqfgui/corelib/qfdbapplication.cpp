
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfdbapplication.h" 

#include <qflogcust.h>

//============================================================
//                              QFDbApplication
//============================================================
QFDbApplication::QFDbApplication(int & argc, char ** argv, bool GUIenabled)
	: QFApplication(argc, argv, GUIenabled)
{
	f_enumCache = NULL;
}

QFDbApplication::~ QFDbApplication()
{
	setEnumCache(NULL);
}

QFDbEnumCache * QFDbApplication::enumCache()
{
	if(!f_enumCache) {
		f_enumCache = new QFDbEnumCache;
	}
	return f_enumCache;
}

QFDbApplication * QFDbApplication::instance()
{
	QFDbApplication *a = qobject_cast<QFDbApplication*>(QApplication::instance());
	QF_ASSERT(a, "aplikace dosud neni inicializovana nebo neni typu QFDbApplication" + QFLog::stackTrace());
	return a;
}

QFDbEnum QFDbApplication::dbEnum(const QString & group_name, const QString & group_id)
{
	QFDbEnumCache *cache = enumCache();
	if(!cache) {
		qfInfo() << QFLog::stackTrace();
		qfError() << "Enum cache is NULL.";
	}
	return cache->dbEnum(group_name, group_id);
}

QFDbEnumCache::EnumList QFDbApplication::dbEnumsForGroup(const QString & group_name)
{
	QFDbEnumCache *cache = enumCache();
	if(!cache) {
		qfInfo() << QFLog::stackTrace();
		qfError() << "Enum cache is NULL.";
	}
	return cache->dbEnumsForGroup(group_name);
}



