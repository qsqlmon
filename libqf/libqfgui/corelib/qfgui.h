
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFGUI_H
#define QFGUI_H

#include <qfguiglobal.h>

class QWidget;
class QLayout;
class QVariant;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFGui 
{
	public:
		static void setPropertyInChildWidgets(QWidget *parent, const char *property, const QVariant &value);
		static QLayout* findWidgetLayout(QWidget *widget);
		/// vymaze vsechny child layouty a widgety
		static void deleteChildWidgetsAndLayouts(QWidget *parent_widget);
};

#endif // QFGUI_H

