<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name></name>
    <message>
        <source>Question</source>
        <translation type="obsolete">Otázka</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation type="obsolete">&amp;Ano</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation type="obsolete">&amp;Ne</translation>
    </message>
    <message>
        <source>Enter the text</source>
        <translation type="obsolete">Vložte text</translation>
    </message>
</context>
<context>
    <name>ConfigFrame</name>
    <message>
        <location filename="gui/widgets/qfxmlconfigwidget/configframe.cpp" line="74"/>
        <source>Changes in option &apos;%1&apos; will be applied after the application restart.</source>
        <translation>Změny v nastavení &apos;%1&apos; se projeví až po restartu aplikace.</translation>
    </message>
</context>
<context>
    <name>ConfigTreeModel</name>
    <message>
        <location filename="gui/widgets/qfxmlconfigwidget/xmlconfigwidget_p.cpp" line="47"/>
        <source>konfigurace</source>
        <translation>konfigurace</translation>
    </message>
</context>
<context>
    <name>ConfigWidget</name>
    <message>
        <location filename="gui/widgets/qfxmlconfigwidget/configframe.cpp" line="190"/>
        <source>default value</source>
        <translation>defaultní hodnota</translation>
    </message>
</context>
<context>
    <name>Day</name>
    <message>
        <location filename="gui/widgets/qfdateedit/datetable.cpp" line="64"/>
        <source>Mon</source>
        <translation>Po</translation>
    </message>
    <message>
        <location filename="gui/widgets/qfdateedit/datetable.cpp" line="69"/>
        <source>Tue</source>
        <translation>Út</translation>
    </message>
    <message>
        <location filename="gui/widgets/qfdateedit/datetable.cpp" line="74"/>
        <source>Wed</source>
        <translation>St</translation>
    </message>
    <message>
        <location filename="gui/widgets/qfdateedit/datetable.cpp" line="79"/>
        <source>Thu</source>
        <translation>Čt</translation>
    </message>
    <message>
        <location filename="gui/widgets/qfdateedit/datetable.cpp" line="84"/>
        <source>Fri</source>
        <translation>Pá</translation>
    </message>
    <message>
        <location filename="gui/widgets/qfdateedit/datetable.cpp" line="90"/>
        <source>Sat</source>
        <translation>So</translation>
    </message>
    <message>
        <location filename="gui/widgets/qfdateedit/datetable.cpp" line="100"/>
        <source>Sun</source>
        <translation>Ne</translation>
    </message>
</context>
<context>
    <name>DirConfigWidget</name>
    <message>
        <location filename="gui/widgets/qfxmlconfigwidget/configframe.cpp" line="628"/>
        <source>Vyberte adresar</source>
        <translation>Vyberte adresář</translation>
    </message>
</context>
<context>
    <name>DlgException</name>
    <message>
        <location filename="gui/dialogs/dlgexception/dlgexception.ui" line="13"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/dialogs/dlgexception/dlgexception.ui" line="51"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Storno</translation>
    </message>
    <message>
        <location filename="gui/dialogs/dlgexception/dlgexception.ui" line="72"/>
        <source>Exception</source>
        <translation>Vyjjímka</translation>
    </message>
    <message>
        <location filename="gui/dialogs/dlgexception/dlgexception.ui" line="95"/>
        <source>Where</source>
        <translation>Kde</translation>
    </message>
    <message>
        <location filename="gui/dialogs/dlgexception/dlgexception.ui" line="115"/>
        <source>Stack trace</source>
        <translation>Obsah zásobníku</translation>
    </message>
    <message>
        <location filename="gui/dialogs/dlgexception/dlgexception.ui" line="58"/>
        <source>Send report</source>
        <translation>Poslat sestavu</translation>
    </message>
    <message>
        <location filename="gui/dialogs/dlgexception/dlgexception.ui" line="140"/>
        <source>File</source>
        <translation>Soubor</translation>
    </message>
    <message>
        <location filename="gui/dialogs/dlgexception/dlgexception.ui" line="145"/>
        <source>Function</source>
        <translation>Funkce</translation>
    </message>
    <message>
        <location filename="gui/dialogs/dlgexception/dlgexception.ui" line="150"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
</context>
<context>
    <name>FileConfigWidget</name>
    <message>
        <location filename="gui/widgets/qfxmlconfigwidget/configframe.cpp" line="663"/>
        <location filename="gui/widgets/qfxmlconfigwidget/configframe.cpp" line="666"/>
        <source>Vyberte soubor</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/widgets/qfxmlconfigwidget/configframe.cpp" line="663"/>
        <location filename="gui/widgets/qfxmlconfigwidget/configframe.cpp" line="666"/>
        <source>Vsechny soubory (*)</source>
        <translation>Všechny soubory (*)</translation>
    </message>
</context>
<context>
    <name>QFButtonDialog</name>
    <message>
        <location filename="gui/dialogs/qfdialog/qfbuttondialog.cpp" line="24"/>
        <source>None</source>
        <translation>Žádný</translation>
    </message>
    <message>
        <location filename="gui/dialogs/qfdialog/qfbuttondialog.cpp" line="86"/>
        <source>Button %1 does not exist.</source>
        <translation>Button %1 does not exist.</translation>
    </message>
    <message>
        <location filename="gui/dialogs/qfdialog/qfbuttondialog.ui" line="21"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QFCSVExportDialogWidget</name>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulář</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="29"/>
        <source>quotes</source>
        <translation>úvozovky</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="35"/>
        <source>always</source>
        <translation>vždy</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="42"/>
        <source>when necessary</source>
        <translation>pokud jsou nutné</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="52"/>
        <source>never</source>
        <translation>nikdy</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="61"/>
        <source>quote char</source>
        <translation>znak uvození</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="74"/>
        <source>&quot;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="102"/>
        <source>separator</source>
        <translation>oddělovač</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="110"/>
        <source>other</source>
        <translation>jiný</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="126"/>
        <source>,</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="151"/>
        <source>Tab</source>
        <translation>Tab</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="172"/>
        <source>codec</source>
        <translation>kodek</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="200"/>
        <source>header</source>
        <translation>záhlaví</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="218"/>
        <source>lines from</source>
        <translation>řádky od</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="247"/>
        <source>to</source>
        <translation>do</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="260"/>
        <source>value -1 means all lines</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="297"/>
        <source>columns</source>
        <translation>sloupce</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="305"/>
        <source>select all</source>
        <translation>označit vše</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="308"/>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="322"/>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="336"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="319"/>
        <source>select none</source>
        <translation>zrušit označení</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvexportdialogwidget.ui" line="333"/>
        <source>invert selection</source>
        <translation>invertovat označení</translation>
    </message>
</context>
<context>
    <name>QFCSVImportDialogWidget</name>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvimportdialogwidget.ui" line="13"/>
        <source>Form</source>
        <translation>Formulář</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvimportdialogwidget.ui" line="28"/>
        <source>file</source>
        <translation>soubor</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvimportdialogwidget.ui" line="40"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvimportdialogwidget.ui" line="49"/>
        <source>reload</source>
        <translation>načíst</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvimportdialogwidget.ui" line="56"/>
        <source>separator</source>
        <translation>oddělovač</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvimportdialogwidget.ui" line="63"/>
        <source>Tab</source>
        <translation>Tab</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvimportdialogwidget.ui" line="70"/>
        <source>codec</source>
        <translation>kodek</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvimportdialogwidget.ui" line="95"/>
        <source>other</source>
        <translation>jiný</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvimportdialogwidget.ui" line="111"/>
        <source>,</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvimportdialogwidget.ui" line="136"/>
        <source>quotes</source>
        <translation>úvozovky</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvimportdialogwidget.ui" line="149"/>
        <source>&quot;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvimportdialogwidget.ui" line="159"/>
        <source>ignore lines</source>
        <translation>ignorovat řádky</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvimportdialogwidget.ui" line="176"/>
        <source>header</source>
        <translation>záhlaví</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvimportdialogwidget.ui" line="186"/>
        <source>mapping</source>
        <translation>mapování</translation>
    </message>
    <message>
        <location filename="gui/widgets/csvwidgets/qfcsvimportdialogwidget.ui" line="208"/>
        <source>-&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QFDataFormWidget</name>
    <message>
        <location filename="gui/widgets/qfdataformwidget/qfdataformwidget.cpp" line="319"/>
        <source>New</source>
        <translation>Nový</translation>
    </message>
    <message>
        <location filename="gui/widgets/qfdataformwidget/qfdataformwidget.cpp" line="321"/>
        <source>Ctrl+Ins</source>
        <comment>new</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/widgets/qfdataformwidget/qfdataformwidget.cpp" line="327"/>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location filename="gui/widgets/qfdataformwidget/qfdataformwidget.cpp" line="329"/>
        <source>Ctrl+Del</source>
        <comment>delete</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/widgets/qfdataformwidget/qfdataformwidget.cpp" line="341"/>
        <source>Ulozit</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="gui/widgets/qfdataformwidget/qfdataformwidget.cpp" line="343"/>
        <source>Ctrl+Return</source>
        <comment>post</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/widgets/qfdataformwidget/qfdataformwidget.cpp" line="349"/>
        <source>Znovu nacist data</source>
        <translation>Znovu načíst data</translation>
    </message>
    <message>
        <location filename="gui/widgets/qfdataformwidget/qfdataformwidget.cpp" line="351"/>
        <source>Ctrl+R</source>
        <comment>reload</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/widgets/qfdataformwidget/qfdataformwidget.cpp" line="362"/>
        <source>toolbar action &apos;%1&apos; not found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/widgets/qfdataformwidget/qfdataformwidget.cpp" line="454"/>
        <source>Pole &apos;%1&apos; je povinne.</source>
        <translation>Pole &apos;%1&apos; je povinné.</translation>
    </message>
</context>
<context>
    <name>QFDatePicker</name>
    <message>
        <location filename="gui/widgets/qfdateedit/qfdatepicker.cpp" line="234"/>
        <source>Week %1</source>
        <translation>Týden %1</translation>
    </message>
    <message>
        <location filename="gui/widgets/qfdateedit/qfdatepicker.cpp" line="16"/>
        <source>None</source>
        <translation>Žádný</translation>
    </message>
</context>
<context>
    <name>QFDialogShowAgain</name>
    <message>
        <location filename="gui/dialogs/qfdialog/qfdialogshowagain.ui" line="16"/>
        <source>Message</source>
        <translation>Oznámení</translation>
    </message>
    <message>
        <location filename="gui/dialogs/qfdialog/qfdialogshowagain.ui" line="39"/>
        <source>Show this message again</source>
        <translation>Zobrazovat tuto zprávu znova</translation>
    </message>
</context>
<context>
    <name>QFDlgChangePassword</name>
    <message>
        <location filename="gui/dialogs/misc/qfdlgchangepassword.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/dialogs/misc/qfdlgchangepassword.ui" line="20"/>
        <source>old password</source>
        <translation>původní heslo</translation>
    </message>
    <message>
        <location filename="gui/dialogs/misc/qfdlgchangepassword.ui" line="44"/>
        <source>retype new password</source>
        <translation>nové heslo znovu</translation>
    </message>
    <message>
        <location filename="gui/dialogs/misc/qfdlgchangepassword.ui" line="51"/>
        <source>new password</source>
        <translation>nové heslo</translation>
    </message>
</context>
<context>
    <name>QFDlgDataTable</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Formulář</translation>
    </message>
</context>
<context>
    <name>QFDlgHtmlView</name>
    <message>
        <location filename="gui/dialogs/dlghtmlview/qfdlghtmlview.ui" line="16"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/dialogs/dlghtmlview/qfdlghtmlview.ui" line="28"/>
        <source>Widget for the QFHtmlViewDialog.</source>
        <translation>Widget pro QFHtmlViewDialog.</translation>
    </message>
</context>
<context>
    <name>QFDlgOpenUrl</name>
    <message>
        <location filename="gui/dialogs/dlgopenurl/qfdlgopenurl.cpp" line="76"/>
        <source>Save File</source>
        <translation>Uložit soubor</translation>
    </message>
    <message>
        <location filename="gui/dialogs/dlgopenurl/qfdlgopenurl.cpp" line="83"/>
        <source>Chyba pri ukladani souboru &apos;&apos;%1&apos;</source>
        <translation>Chyba při ukládání souboru &apos;&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="gui/dialogs/dlgopenurl/qfdlgopenurl.ui" line="13"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/dialogs/dlgopenurl/qfdlgopenurl.ui" line="21"/>
        <source>&amp;Url</source>
        <translation>&amp;Url</translation>
    </message>
    <message utf8="true">
        <location filename="gui/dialogs/dlgopenurl/qfdlgopenurl.ui" line="55"/>
        <source>&amp;Otevřít</source>
        <translation>&amp;Otevřít</translation>
    </message>
    <message utf8="true">
        <location filename="gui/dialogs/dlgopenurl/qfdlgopenurl.ui" line="65"/>
        <source>&amp;Uložit</source>
        <translation>&amp;Uložit</translation>
    </message>
    <message>
        <location filename="gui/dialogs/dlgopenurl/qfdlgopenurl.ui" line="72"/>
        <source>&amp;Storno</source>
        <translation>&amp;Storno</translation>
    </message>
</context>
<context>
    <name>QFDlgProgress</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Storno</translation>
    </message>
</context>
<context>
    <name>QFDlgXmlConfig</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Formulář</translation>
    </message>
</context>
<context>
    <name>QFHtmlViewUtils</name>
    <message>
        <location filename="utils/htmlutils/qfhtmlviewutils.cpp" line="27"/>
        <source>Dotaz</source>
        <translation></translation>
    </message>
    <message>
        <location filename="utils/htmlutils/qfhtmlviewutils.cpp" line="27"/>
        <source>Jak chcete sestavu zobrazit?</source>
        <translation>Jak chcete sestavu zobrazit?</translation>
    </message>
    <message>
        <location filename="utils/htmlutils/qfhtmlviewutils.cpp" line="32"/>
        <source>Zobrazit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="utils/htmlutils/qfhtmlviewutils.cpp" line="33"/>
        <source>Zobrazit v externim prohlizeci</source>
        <translation>Zobrazit v externím prohlížeči</translation>
    </message>
    <message>
        <location filename="utils/htmlutils/qfhtmlviewutils.cpp" line="39"/>
        <source>Ulozit jako ...</source>
        <translation>Uložit jako ...</translation>
    </message>
    <message>
        <location filename="utils/htmlutils/qfhtmlviewutils.cpp" line="40"/>
        <source>soubory HTML (*.html)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="utils/htmlutils/qfhtmlviewutils.cpp" line="47"/>
        <source>Pripravuje se zobrazeni sestavy</source>
        <translation>Připravuje se zobrazení sestavy</translation>
    </message>
    <message>
        <location filename="utils/htmlutils/qfhtmlviewutils.cpp" line="54"/>
        <source>Zobrazuje se sestava</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QFHtmlViewWidget</name>
    <message>
        <source>Zpet</source>
        <translation type="obsolete">Zpět</translation>
    </message>
    <message>
        <source>Dopredu</source>
        <translation type="obsolete">Dopředu</translation>
    </message>
    <message>
        <source>Na zacatek</source>
        <translation type="obsolete">Na začátek</translation>
    </message>
    <message>
        <source>Ulozit</source>
        <translation type="obsolete">Uložit</translation>
    </message>
    <message>
        <source>Ulozit dokument</source>
        <translation type="obsolete">Uložit dokument</translation>
    </message>
    <message>
        <source>Ulozit jako ...</source>
        <translation type="obsolete">Uložit jako ...</translation>
    </message>
    <message>
        <source>Soubor %1 nelze otevrit pro cteni.</source>
        <translation type="obsolete">Soubor %1 nelze otevřít pro čtení.</translation>
    </message>
    <message>
        <source>Form</source>
        <translation type="obsolete">Formulář</translation>
    </message>
</context>
<context>
    <name>QFLogModel</name>
    <message>
        <location filename="utils/log/qflogmodel.cpp" line="34"/>
        <source>level</source>
        <translation>úroveň</translation>
    </message>
    <message>
        <location filename="utils/log/qflogmodel.cpp" line="35"/>
        <source>domain</source>
        <translation>doména</translation>
    </message>
    <message>
        <location filename="utils/log/qflogmodel.cpp" line="36"/>
        <source>message</source>
        <translation>zpráva</translation>
    </message>
</context>
<context>
    <name>QFLogViewWidget</name>
    <message>
        <location filename="utils/log/qflogviewwidget.ui" line="13"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="utils/log/qflogviewwidget.ui" line="51"/>
        <source>Expand node</source>
        <translation>Rozbalit</translation>
    </message>
    <message>
        <location filename="utils/log/qflogviewwidget.ui" line="54"/>
        <location filename="utils/log/qflogviewwidget.ui" line="76"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="utils/log/qflogviewwidget.ui" line="73"/>
        <source>Colapse node</source>
        <translation>Zabalit</translation>
    </message>
    <message utf8="true">
        <location filename="utils/log/qflogviewwidget.ui" line="95"/>
        <source>Závažnost</source>
        <translation></translation>
    </message>
    <message>
        <location filename="utils/log/qflogviewwidget.ui" line="106"/>
        <source>--- all ---</source>
        <translation>--- vše ---</translation>
    </message>
    <message>
        <location filename="utils/log/qflogviewwidget.ui" line="111"/>
        <source>Fatal</source>
        <translation>Osudová</translation>
    </message>
    <message>
        <location filename="utils/log/qflogviewwidget.ui" line="116"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="utils/log/qflogviewwidget.ui" line="121"/>
        <source>Warning</source>
        <translation>Varování</translation>
    </message>
    <message>
        <location filename="utils/log/qflogviewwidget.ui" line="126"/>
        <source>Info</source>
        <translation></translation>
    </message>
    <message>
        <location filename="utils/log/qflogviewwidget.ui" line="131"/>
        <source>Debug</source>
        <translation></translation>
    </message>
    <message>
        <location filename="utils/log/qflogviewwidget.ui" line="136"/>
        <source>Trash</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QFMainWindow</name>
    <message>
        <location filename="gui/parts/qfmainwindow.cpp" line="93"/>
        <source>&lt;p&gt;&lt;b&gt;Qt %1&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://www.trolltech.com/qt/&quot;&gt;www.trolltech.com/qt/&lt;/a&gt;&lt;p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Qt %1&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://www.trolltech.com/qt/&quot;&gt;www.trolltech.com/qt/&lt;/a&gt;&lt;p&gt;</translation>
    </message>
    <message>
        <location filename="gui/parts/qfmainwindow.cpp" line="96"/>
        <source>About Qt</source>
        <translation>O knihovně Qt</translation>
    </message>
</context>
<context>
    <name>QFMessage</name>
    <message>
        <location filename="gui/dialogs/message/qfmessage.h" line="40"/>
        <source>Question</source>
        <translation>Otázka</translation>
    </message>
    <message>
        <location filename="gui/dialogs/message/qfmessage.h" line="41"/>
        <source>&amp;Yes</source>
        <translation>&amp;Ano</translation>
    </message>
    <message>
        <location filename="gui/dialogs/message/qfmessage.h" line="41"/>
        <source>&amp;No</source>
        <translation>&amp;Ne</translation>
    </message>
    <message>
        <location filename="gui/dialogs/message/qfmessage.h" line="53"/>
        <source>Enter the text</source>
        <translation>Vložte text</translation>
    </message>
</context>
<context>
    <name>QFMsgShowAgainDialogWidget</name>
    <message>
        <location filename="gui/dialogs/message/qfmsgshowagaindialogwidget.ui" line="13"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/dialogs/message/qfmsgshowagaindialogwidget.ui" line="19"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QFPartManager</name>
    <message>
        <location filename="gui/parts/qfpartmanager.cpp" line="56"/>
        <source>Part 0x%1 &apos;%2&apos; allready registered.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QFReportProcessor</name>
    <message>
        <source>File &apos;%1&apos; can not be resolved.</source>
        <translation type="obsolete">Soubor &apos;%1&apos; nelze nahrát nebo najít.</translation>
    </message>
    <message>
        <location filename="reports/processor/qfreportprocessor.cpp" line="224"/>
        <source>Element &apos;%1&apos; is not processible and it will be ignored.</source>
        <translation>Element &apos;%1&apos; is not processible and it will be ignored.</translation>
    </message>
</context>
<context>
    <name>QFReportProcessorItem</name>
    <message>
        <source>Report path &apos;%1&apos; does not exist. Domain: %2 Element path: &apos;%3&apos;</source>
        <translation type="obsolete">Cesta v sestavě &apos;%1&apos; nebyla nalezena. Doména: &apos;%2&apos; Cesta k elementu: &apos;%3&apos;</translation>
    </message>
</context>
<context>
    <name>QFReportProcessorScriptDriver</name>
    <message>
        <location filename="reports/processor/qfreportprocessor.cpp" line="33"/>
        <source>JavaScript funkce &apos;%1&apos; neni definovana.</source>
        <translation>JavaScript funkce &apos;%1&apos; není definována.</translation>
    </message>
</context>
<context>
    <name>QFReportViewWidget</name>
    <message>
        <location filename="reports/widgets/qfreportviewwidget.cpp" line="702"/>
        <source>empty file name</source>
        <translation>prázdné jméno souboru</translation>
    </message>
    <message>
        <location filename="reports/widgets/qfreportviewwidget.cpp" line="733"/>
        <source>Save as PDF</source>
        <translation>Uložit jako PDF</translation>
    </message>
</context>
<context>
    <name>QFReportViewWidget::PainterWidget</name>
    <message>
        <location filename="reports/widgets/qfreportviewwidget.cpp" line="98"/>
        <source>Item menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="reports/widgets/qfreportviewwidget.cpp" line="99"/>
        <location filename="reports/widgets/qfreportviewwidget.cpp" line="103"/>
        <source>Editovat text</source>
        <translation>Editovat text</translation>
    </message>
    <message>
        <location filename="reports/widgets/qfreportviewwidget.cpp" line="103"/>
        <source>Novy text:</source>
        <translation>Nový text:</translation>
    </message>
</context>
<context>
    <name>QFScriptEditorWidget</name>
    <message>
        <location filename="script/gui/widgets/qfscripteditorwidget/qfscripteditorwidget.cpp" line="39"/>
        <source>Script editor</source>
        <translation>Editor skriptů</translation>
    </message>
    <message>
        <location filename="script/gui/widgets/qfscripteditorwidget/qfscripteditorwidget.cpp" line="174"/>
        <source>vlozte text</source>
        <translation>vložte text</translation>
    </message>
    <message>
        <location filename="script/gui/widgets/qfscripteditorwidget/qfscripteditorwidget.cpp" line="174"/>
        <source>Jmeno skriptu:</source>
        <translation>Jméno skriptu:</translation>
    </message>
    <message>
        <location filename="script/gui/widgets/qfscripteditorwidget/qfscripteditorwidget.cpp" line="179"/>
        <source>Skript %1 jiz existuje, prepsat ?</source>
        <translation>Skript %1 již existuje, přepsat ?</translation>
    </message>
    <message>
        <location filename="script/gui/widgets/qfscripteditorwidget/qfscripteditorwidget.cpp" line="211"/>
        <source>Opravdu smazat skript &apos;%1&apos; ?</source>
        <translation>Opravdu smazat skript &apos;%1&apos; ?</translation>
    </message>
    <message>
        <location filename="script/gui/widgets/qfscripteditorwidget/qfscripteditorwidget.cpp" line="248"/>
        <source>Aktivni dokument obsahuje neulozene zmeny, ulozit ?</source>
        <translation>Aktivní dokument obsahuje neuložené změny, uložit ?</translation>
    </message>
    <message>
        <location filename="script/gui/widgets/qfscripteditorwidget/qfscripteditorwidget.ui" line="13"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="script/gui/widgets/qfscripteditorwidget/qfscripteditorwidget.ui" line="44"/>
        <source>Editor</source>
        <translation></translation>
    </message>
    <message>
        <location filename="script/gui/widgets/qfscripteditorwidget/qfscripteditorwidget.ui" line="61"/>
        <source>Console</source>
        <translation>Konzole</translation>
    </message>
</context>
<context>
    <name>QFSplashScreen</name>
    <message>
        <location filename="gui/widgets/others/qfsplashscreen.ui" line="16"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/widgets/others/qfsplashscreen.ui" line="327"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QFSqlClearValueButton</name>
    <message>
        <location filename="gui/widgets/sql/qfsqleditforeignitemsbutton.cpp" line="29"/>
        <source>Clear current value</source>
        <translation>Vymazat hodnotu</translation>
    </message>
</context>
<context>
    <name>QFSqlEditForeignItemsButton</name>
    <message>
        <location filename="gui/widgets/sql/qfsqleditforeignitemsbutton.cpp" line="19"/>
        <source>Edit items</source>
        <translation>Editovat položky</translation>
    </message>
</context>
<context>
    <name>QFSqlEditWithButton</name>
    <message>
        <location filename="gui/widgets/sql/qfsqleditwithbutton.cpp" line="21"/>
        <source>id ${ID} nenalezeno</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QFSqlEnumItemsEditor</name>
    <message>
        <location filename="gui/widgets/sql/qfsqlenumitemseditor.cpp" line="30"/>
        <source>id</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/widgets/sql/qfsqlenumitemseditor.cpp" line="30"/>
        <source>groupId</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui/widgets/sql/qfsqlenumitemseditor.cpp" line="30"/>
        <source>název</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/widgets/sql/qfsqlenumitemseditor.ui" line="14"/>
        <source>Form</source>
        <translation>Formulář</translation>
    </message>
    <message>
        <location filename="gui/widgets/sql/qfsqlenumitemseditor.ui" line="45"/>
        <source>Add item</source>
        <translation>Přidat položku</translation>
    </message>
    <message>
        <location filename="gui/widgets/sql/qfsqlenumitemseditor.ui" line="65"/>
        <source>Edit item</source>
        <translation>Změnit položku</translation>
    </message>
    <message>
        <location filename="gui/widgets/sql/qfsqlenumitemseditor.ui" line="85"/>
        <location filename="gui/widgets/sql/qfsqlenumitemseditor.ui" line="108"/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>QFSqlEnumItemsEditorItemEditor</name>
    <message>
        <location filename="gui/widgets/sql/qfsqlenumitemseditoritemeditor.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui/widgets/sql/qfsqlenumitemseditoritemeditor.ui" line="22"/>
        <source>Název</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/widgets/sql/qfsqlenumitemseditoritemeditor.ui" line="32"/>
        <source>Group id</source>
        <translation>Identifikátor</translation>
    </message>
    <message>
        <location filename="gui/widgets/sql/qfsqlenumitemseditoritemeditor.ui" line="48"/>
        <source>Text</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QFSqlForeignSetBase</name>
    <message utf8="true">
        <location filename="gui/widgets/sql/qfsqlenumset.cpp" line="38"/>
        <source>jiná možnost</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/widgets/sql/qfsqlenumset.cpp" line="232"/>
        <source>Vlozte text</source>
        <translation>Vložte text</translation>
    </message>
    <message>
        <location filename="gui/widgets/sql/qfsqlenumset.cpp" line="232"/>
        <source>Vlozte text:</source>
        <translation>Vložte text:</translation>
    </message>
    <message>
        <location filename="gui/widgets/sql/qfsqlenumset.cpp" line="257"/>
        <source>menu</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui/widgets/sql/qfsqlenumset.cpp" line="259"/>
        <source>Editovat položky</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QFSqlFormDocumentRowLock</name>
    <message>
        <source>Row cannot be locked, because id does not exists. Row id: %1, table: &apos;%2&apos;</source>
        <translation type="obsolete">Řádek nemůže být uzamknut, protože rádek s tímto id neexistuje. Řádek id: %1, tabulka: &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="gui/dialogs/qfdataform/qfdataformdocument.cpp" line="764"/>
        <source>Row cannot be locked, because row id does not exists. Row id: %1, table: &apos;%2&apos;</source>
        <translation>Řádek nemůže být uzamčen, protože jeho id neexistuje. Id řádku: %1, tabulka: &apos;%2&apos;</translation>
    </message>
</context>
<context>
    <name>QFSqlQueryViewWidget</name>
    <message>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <source>rows</source>
        <translation type="obsolete">řádky</translation>
    </message>
</context>
<context>
    <name>QFSqlXmlConfigWidget</name>
    <message>
        <location filename="gui/widgets/sql/qfsqlxmlconfigwidget.cpp" line="29"/>
        <source>Defaults</source>
        <translation>Defaultní</translation>
    </message>
</context>
<context>
    <name>QFStatusBar</name>
    <message>
        <location filename="gui/widgets/others/qfstatusbar.cpp" line="98"/>
        <source>Label no %1 does not exist in status bar.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QFTableHeaderView</name>
    <message>
        <location filename="gui/view/qfheaderview.cpp" line="106"/>
        <source>Column menu</source>
        <translation>Nabídka sloupce</translation>
    </message>
    <message>
        <location filename="gui/view/qfheaderview.cpp" line="108"/>
        <source>Visible columns</source>
        <translation>Zobrazené sloupce</translation>
    </message>
    <message>
        <location filename="gui/view/qfheaderview.cpp" line="117"/>
        <source>column data type</source>
        <translation>typ dat ve sloupci</translation>
    </message>
    <message>
        <location filename="gui/view/qfheaderview.cpp" line="119"/>
        <source>column size</source>
        <translation>šířka sloupce</translation>
    </message>
    <message>
        <location filename="gui/view/qfheaderview.cpp" line="122"/>
        <source>Column</source>
        <translation>Sloupec</translation>
    </message>
</context>
<context>
    <name>QFTableModel</name>
    <message>
        <location filename="gui/model/qftablemodel.cpp" line="583"/>
        <source>Column %1 out of range (%2)</source>
        <translation>Sloupec %1 je mimo rozsah (%2)</translation>
    </message>
    <message>
        <source>Field index %1 not found in column list.</source>
        <translation type="obsolete">Iindex sloupce &apos;%1&apos; nebyl nalezen v seznamu sloupců.</translation>
    </message>
    <message>
        <source>Field index %1 is less tha 0.</source>
        <translation type="obsolete">Iindex sloupce &apos;%1&apos; je menší než 0.</translation>
    </message>
    <message>
        <source>Column named &apos;%1&apos; not found in column list. Existing columns: [%2]</source>
        <translation type="obsolete">Jméno sloupce &apos;%1&apos; nebylo nalezeno ve výsledku dotazu. Dostupná jména jsou: [%2]</translation>
    </message>
    <message>
        <location filename="gui/model/qftablemodel.cpp" line="790"/>
        <source>Model column named &apos;%1&apos; not found.</source>
        <translation>Model neobsahuje sloupec jménem &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>invalid column: %1</source>
        <translation type="obsolete">neplatný sloupec: %1</translation>
    </message>
    <message>
        <source>invalid row: %1</source>
        <translation type="obsolete">neplatný řádek: %1</translation>
    </message>
    <message>
        <source>invalid field index: %1</source>
        <translation type="obsolete">neplatný index sloupce: %1</translation>
    </message>
    <message>
        <source>Row index %1 out of range (%2).</source>
        <translation type="obsolete">Index řádku %1 je mimo rozsah (%2).</translation>
    </message>
</context>
<context>
    <name>QFTableModelDelegateEditWithButton</name>
    <message>
        <location filename="gui/model/qftablemodeldelegate.cpp" line="30"/>
        <source>Chooser is NULL.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QFTablePrintDialogWidget</name>
    <message>
        <location filename="reports/widgets/qftableprintdialogwidget/qftableprintdialogwidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulář</translation>
    </message>
    <message>
        <location filename="reports/widgets/qftableprintdialogwidget/qftableprintdialogwidget.ui" line="20"/>
        <source>settings</source>
        <translation>nastavení</translation>
    </message>
    <message>
        <location filename="reports/widgets/qftableprintdialogwidget/qftableprintdialogwidget.ui" line="33"/>
        <location filename="reports/widgets/qftableprintdialogwidget/qftableprintdialogwidget.ui" line="53"/>
        <location filename="reports/widgets/qftableprintdialogwidget/qftableprintdialogwidget.ui" line="150"/>
        <location filename="reports/widgets/qftableprintdialogwidget/qftableprintdialogwidget.ui" line="164"/>
        <location filename="reports/widgets/qftableprintdialogwidget/qftableprintdialogwidget.ui" line="178"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="reports/widgets/qftableprintdialogwidget/qftableprintdialogwidget.ui" line="98"/>
        <source>report</source>
        <translation></translation>
    </message>
    <message>
        <location filename="reports/widgets/qftableprintdialogwidget/qftableprintdialogwidget.ui" line="106"/>
        <source>title</source>
        <translation>nadpis</translation>
    </message>
    <message>
        <location filename="reports/widgets/qftableprintdialogwidget/qftableprintdialogwidget.ui" line="132"/>
        <source>only selected rows</source>
        <translation>pouze označené řádky</translation>
    </message>
    <message>
        <location filename="reports/widgets/qftableprintdialogwidget/qftableprintdialogwidget.ui" line="139"/>
        <source>columns</source>
        <translation>sloupce</translation>
    </message>
    <message>
        <location filename="reports/widgets/qftableprintdialogwidget/qftableprintdialogwidget.ui" line="147"/>
        <source>select all</source>
        <translation>označit vše</translation>
    </message>
    <message>
        <location filename="reports/widgets/qftableprintdialogwidget/qftableprintdialogwidget.ui" line="161"/>
        <source>select none</source>
        <translation>zrušit označení</translation>
    </message>
    <message>
        <location filename="reports/widgets/qftableprintdialogwidget/qftableprintdialogwidget.ui" line="175"/>
        <source>invert selection</source>
        <translation>invertovat označení</translation>
    </message>
    <message>
        <location filename="reports/widgets/qftableprintdialogwidget/qftableprintdialogwidget.ui" line="208"/>
        <source>model</source>
        <translation></translation>
    </message>
    <message>
        <location filename="reports/widgets/qftableprintdialogwidget/qftableprintdialogwidget.ui" line="218"/>
        <source>table</source>
        <translation>tabulka</translation>
    </message>
</context>
<context>
    <name>QFTableView</name>
    <message>
        <source>Automaticky nastavit sirky sloupcu</source>
        <translation type="obsolete">Automaticky nastavit šířky sloupců</translation>
    </message>
    <message>
        <source>Znovu nacist data</source>
        <translation type="obsolete">Znovu načíst data</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="139"/>
        <source>Ctrl+R</source>
        <comment>reload SQL table</comment>
        <translation></translation>
    </message>
    <message>
        <source>Vlozit radek</source>
        <translation type="obsolete">Vložit řádek</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="154"/>
        <source>Ctrl+Ins</source>
        <comment>insert row SQL table</comment>
        <translation></translation>
    </message>
    <message>
        <source>Odstranit oznacene radky</source>
        <translation type="obsolete">Odstranit označené řádky</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="163"/>
        <source>Ctrl+Del</source>
        <comment>delete row SQL table</comment>
        <translation></translation>
    </message>
    <message>
        <source>Ulozit zmeny v radku</source>
        <translation type="obsolete">Uložit změny v řádku</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="173"/>
        <source>Ctrl+Return</source>
        <comment>post row SQL table</comment>
        <translation></translation>
    </message>
    <message>
        <source>Vratit zmeny v radku</source>
        <translation type="obsolete">Vratit změny v řádku</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="183"/>
        <source>Ctrl+Z</source>
        <comment>revert edited row</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="215"/>
        <source>Zobrazit ve formulari</source>
        <translation>Zobrazit ve formuláři</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="217"/>
        <source>Zobrazit radek v formulari pro cteni</source>
        <translation>Zobrazit řádek ve formuláři pro čtení</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="224"/>
        <source>Upravit ve formulari</source>
        <translation>Upravit ve formuláři</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="226"/>
        <source>Upravit radek ve formulari</source>
        <translation>Upravit řádek ve formuláři</translation>
    </message>
    <message>
        <source>Setridit vzestupne</source>
        <translation type="obsolete">Setřídit vzestupně</translation>
    </message>
    <message>
        <source>Setridit sestupne</source>
        <translation type="obsolete">Setřídit sestupně</translation>
    </message>
    <message>
        <source>Zobrazit text bunky</source>
        <translation type="obsolete">Zobrazit text buňky</translation>
    </message>
    <message>
        <source>Ulozit BLOB</source>
        <translation type="obsolete">Uložit BLOB</translation>
    </message>
    <message>
        <source>Nacist BLOB se souboru</source>
        <translation type="obsolete">Načíst BLOB se souboru</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="1111"/>
        <source>Selection has to be continuous.</source>
        <translation>Selekce musí být souvislá.</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="1117"/>
        <source>Do you realy want to remove row?</source>
        <translation>Opravdu si přejete odstranit řádek?</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="1120"/>
        <source>Do you realy want to remove all selected rows?</source>
        <translation>Opravdu si přejete odstranit všechny označené řádky?</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="1885"/>
        <source>Save File</source>
        <translation>Uložit soubor</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="1899"/>
        <source>Open File</source>
        <translation>Otevřít soubor</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="196"/>
        <source>Ctrl+D</source>
        <comment>insert row copy</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="121"/>
        <source>Resize columns to contents</source>
        <translation>Šířka sloupců podle obsahu</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="137"/>
        <source>Reload</source>
        <translation>Znovu načíst data</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="153"/>
        <source>Insert row</source>
        <translation>Vložit řádek</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="162"/>
        <source>Delete selected rows</source>
        <translation>Odstranit označené řádky</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="171"/>
        <source>Post row edits</source>
        <translation>Uložit editovaný řádek</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="181"/>
        <source>Revert row edits</source>
        <translation>Zrušit změny v řádku</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="191"/>
        <source>Copy row</source>
        <translation>Kopírovat řádek</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="233"/>
        <source>Sort ascending</source>
        <translation>Setřídit vzestupně</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="243"/>
        <source>Sort descending</source>
        <translation>Setřídit sestupně</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="253"/>
        <source>Edit cell content</source>
        <translation>Editovat obsah buňky</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="255"/>
        <source>Ctrl+Shift+T</source>
        <comment>Edit cell content</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="263"/>
        <source>Save BLOB</source>
        <translation>Uložit BLOB</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="272"/>
        <source>Load BLOB from file</source>
        <translation>Načíst BLOB ze souboru</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="306"/>
        <source>Set value in selection</source>
        <translation>Nastavit hodnotu v označených buňkách</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="307"/>
        <source>Ctrl+Shift+E</source>
        <comment>Set value in selection</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="315"/>
        <source>Select</source>
        <translation>Označit</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="322"/>
        <source>Select current column</source>
        <translation>Označit aktuální sloupec</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="329"/>
        <source>Select current row</source>
        <translation>Označit aktuální řádek</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="337"/>
        <source>Calculate</source>
        <translation>Výpočty</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="344"/>
        <source>Sum column</source>
        <translation>Součet sloupce</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="350"/>
        <source>Sum selection</source>
        <translation>Součet hodnot označených buněk</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="370"/>
        <source>Report</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="2193"/>
        <source>Sum of column values: %1</source>
        <translation>Součet hodnot v označených buňkách: %1</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="2206"/>
        <source>Sum of selected values: %1</source>
        <translation>Součet hodnot v označených buňkách: %1</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="2234"/>
        <source>Enter value</source>
        <translation>Vložte hodnotu</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="2234"/>
        <source>new value:</source>
        <translation>nová hodnota:</translation>
    </message>
    <message>
        <source>Vlozit kopii radku</source>
        <translation type="obsolete">Vložit kopii řádku</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="287"/>
        <source>Insert rows statement</source>
        <translation>SQL dotaz pro vložení řádku</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="363"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="377"/>
        <location filename="gui/view/qftableview.cpp" line="406"/>
        <source>CSV</source>
        <translation>CSV</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="384"/>
        <source>HTML</source>
        <translation>HTML</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="391"/>
        <source>XML</source>
        <translation>XML</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="296"/>
        <source>Set NULL in selection</source>
        <translation>Nastav hodnotu označených buněk na NULL</translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="298"/>
        <source>Ctrl+Shift+L</source>
        <comment>Set NULL in selection</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="399"/>
        <source>Import</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/view/qftableview.cpp" line="118"/>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QFTableViewWidget</name>
    <message>
        <location filename="gui/widgets/qftableviewwidget/qftableviewwidget.ui" line="14"/>
        <source>QFSqlQueryView Widget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/widgets/qftableviewwidget/qftableviewwidget.ui" line="546"/>
        <source>rows</source>
        <translation>řádky</translation>
    </message>
    <message>
        <location filename="gui/widgets/qftableviewwidget/qftableviewwidget.ui" line="520"/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>QFTextViewWidget</name>
    <message>
        <source>Soubor %1 nelze otevrit pro cteni.</source>
        <translation type="obsolete">Soubor %1 nelze otevřít pro čtení.</translation>
    </message>
    <message>
        <source>Ulozit</source>
        <translation type="obsolete">Uložit</translation>
    </message>
    <message>
        <source>Ulozit dokument</source>
        <translation type="obsolete">Uložit dokument</translation>
    </message>
    <message>
        <source>Ulozit jako</source>
        <translation type="obsolete">Uložit jako</translation>
    </message>
    <message>
        <source>Ulozit dokument pod jinym jmenem</source>
        <translation type="obsolete">Uložit dokument pod jiným jménem</translation>
    </message>
    <message>
        <source>Zalomit radky</source>
        <translation type="obsolete">Zalomit řádky</translation>
    </message>
    <message>
        <source>textove soubory (*.txt)</source>
        <translation type="obsolete">textové soubory (*.txt)</translation>
    </message>
    <message>
        <source>Ulozit jako ...</source>
        <translation type="obsolete">Uložit jako ...</translation>
    </message>
    <message>
        <source>Nelze otevrit soubor %1 pro zapis.</source>
        <translation type="obsolete">Nelze otevřít soubor %1 pro zápis.</translation>
    </message>
    <message>
        <source>Form</source>
        <translation type="obsolete">Formulář</translation>
    </message>
</context>
<context>
    <name>QFUiBuilder</name>
    <message>
        <location filename="gui/parts/qfuibuilder.cpp" line="71"/>
        <source>Cann&apos;t open ui file %1</source>
        <translation>Nelze otevřít ui soubor %1</translation>
    </message>
    <message>
        <location filename="gui/parts/qfuibuilder.cpp" line="474"/>
        <source>uixml document &apos;%1&apos; is empty or not loaded.</source>
        <translation>ui.xml dokument &apos;%1&apos; je prázdný nebo není nahrán.</translation>
    </message>
</context>
<context>
    <name>QFXmlConfigWidget</name>
    <message>
        <location filename="gui/widgets/qfxmlconfigwidget/qfxmlconfigwidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulář</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="gui/dialogs/message/qfmessage.h" line="23"/>
        <location filename="gui/dialogs/message/qfmessage.h" line="27"/>
        <source>informace</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/dialogs/message/qfmessage.h" line="31"/>
        <location filename="gui/dialogs/message/qfmessage.h" line="35"/>
        <source>error</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="gui/widgets/qfdialogwidget/qfdialogwidget.cpp" line="33"/>
        <source>&amp;Menu</source>
        <comment>toolbar menu button</comment>
        <translation>&amp;Menu</translation>
    </message>
</context>
<context>
    <name>uixml</name>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="3"/>
        <location filename="i18n/uixml_i18n.cpp" line="9"/>
        <location filename="i18n/uixml_i18n.cpp" line="15"/>
        <location filename="i18n/uixml_i18n.cpp" line="21"/>
        <location filename="i18n/uixml_i18n.cpp" line="27"/>
        <location filename="i18n/uixml_i18n.cpp" line="33"/>
        <location filename="i18n/uixml_i18n.cpp" line="39"/>
        <location filename="i18n/uixml_i18n.cpp" line="45"/>
        <location filename="i18n/uixml_i18n.cpp" line="137"/>
        <location filename="i18n/uixml_i18n.cpp" line="143"/>
        <location filename="i18n/uixml_i18n.cpp" line="149"/>
        <location filename="i18n/uixml_i18n.cpp" line="155"/>
        <location filename="i18n/uixml_i18n.cpp" line="161"/>
        <location filename="i18n/uixml_i18n.cpp" line="167"/>
        <location filename="i18n/uixml_i18n.cpp" line="173"/>
        <location filename="i18n/uixml_i18n.cpp" line="179"/>
        <source>main</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/script/gui/widgets/qfscripteditorwidget/qfscripteditorwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="i18n/uixml_i18n.cpp" line="4"/>
        <location filename="i18n/uixml_i18n.cpp" line="10"/>
        <location filename="i18n/uixml_i18n.cpp" line="16"/>
        <location filename="i18n/uixml_i18n.cpp" line="22"/>
        <location filename="i18n/uixml_i18n.cpp" line="28"/>
        <location filename="i18n/uixml_i18n.cpp" line="34"/>
        <location filename="i18n/uixml_i18n.cpp" line="40"/>
        <location filename="i18n/uixml_i18n.cpp" line="46"/>
        <location filename="i18n/uixml_i18n.cpp" line="138"/>
        <location filename="i18n/uixml_i18n.cpp" line="144"/>
        <location filename="i18n/uixml_i18n.cpp" line="150"/>
        <location filename="i18n/uixml_i18n.cpp" line="156"/>
        <location filename="i18n/uixml_i18n.cpp" line="162"/>
        <location filename="i18n/uixml_i18n.cpp" line="168"/>
        <location filename="i18n/uixml_i18n.cpp" line="174"/>
        <location filename="i18n/uixml_i18n.cpp" line="180"/>
        <source>&amp;Nový skript</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/script/gui/widgets/qfscripteditorwidget/qfscripteditorwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="i18n/uixml_i18n.cpp" line="5"/>
        <location filename="i18n/uixml_i18n.cpp" line="11"/>
        <location filename="i18n/uixml_i18n.cpp" line="17"/>
        <location filename="i18n/uixml_i18n.cpp" line="23"/>
        <location filename="i18n/uixml_i18n.cpp" line="29"/>
        <location filename="i18n/uixml_i18n.cpp" line="35"/>
        <location filename="i18n/uixml_i18n.cpp" line="41"/>
        <location filename="i18n/uixml_i18n.cpp" line="47"/>
        <location filename="i18n/uixml_i18n.cpp" line="139"/>
        <location filename="i18n/uixml_i18n.cpp" line="145"/>
        <location filename="i18n/uixml_i18n.cpp" line="151"/>
        <location filename="i18n/uixml_i18n.cpp" line="157"/>
        <location filename="i18n/uixml_i18n.cpp" line="163"/>
        <location filename="i18n/uixml_i18n.cpp" line="169"/>
        <location filename="i18n/uixml_i18n.cpp" line="175"/>
        <location filename="i18n/uixml_i18n.cpp" line="181"/>
        <source>&amp;Uložit skript</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/script/gui/widgets/qfscripteditorwidget/qfscripteditorwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="i18n/uixml_i18n.cpp" line="6"/>
        <location filename="i18n/uixml_i18n.cpp" line="12"/>
        <location filename="i18n/uixml_i18n.cpp" line="18"/>
        <location filename="i18n/uixml_i18n.cpp" line="24"/>
        <location filename="i18n/uixml_i18n.cpp" line="30"/>
        <location filename="i18n/uixml_i18n.cpp" line="36"/>
        <location filename="i18n/uixml_i18n.cpp" line="42"/>
        <location filename="i18n/uixml_i18n.cpp" line="48"/>
        <location filename="i18n/uixml_i18n.cpp" line="140"/>
        <location filename="i18n/uixml_i18n.cpp" line="146"/>
        <location filename="i18n/uixml_i18n.cpp" line="152"/>
        <location filename="i18n/uixml_i18n.cpp" line="158"/>
        <location filename="i18n/uixml_i18n.cpp" line="164"/>
        <location filename="i18n/uixml_i18n.cpp" line="170"/>
        <location filename="i18n/uixml_i18n.cpp" line="176"/>
        <location filename="i18n/uixml_i18n.cpp" line="182"/>
        <source>&amp;Uložit skript jako</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/script/gui/widgets/qfscripteditorwidget/qfscripteditorwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="7"/>
        <location filename="i18n/uixml_i18n.cpp" line="13"/>
        <location filename="i18n/uixml_i18n.cpp" line="19"/>
        <location filename="i18n/uixml_i18n.cpp" line="25"/>
        <location filename="i18n/uixml_i18n.cpp" line="31"/>
        <location filename="i18n/uixml_i18n.cpp" line="37"/>
        <location filename="i18n/uixml_i18n.cpp" line="43"/>
        <location filename="i18n/uixml_i18n.cpp" line="49"/>
        <location filename="i18n/uixml_i18n.cpp" line="141"/>
        <location filename="i18n/uixml_i18n.cpp" line="147"/>
        <location filename="i18n/uixml_i18n.cpp" line="153"/>
        <location filename="i18n/uixml_i18n.cpp" line="159"/>
        <location filename="i18n/uixml_i18n.cpp" line="165"/>
        <location filename="i18n/uixml_i18n.cpp" line="171"/>
        <location filename="i18n/uixml_i18n.cpp" line="177"/>
        <location filename="i18n/uixml_i18n.cpp" line="183"/>
        <source>&amp;Smazat skript</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/script/gui/widgets/qfscripteditorwidget/qfscripteditorwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="51"/>
        <location filename="i18n/uixml_i18n.cpp" line="62"/>
        <location filename="i18n/uixml_i18n.cpp" line="73"/>
        <location filename="i18n/uixml_i18n.cpp" line="84"/>
        <location filename="i18n/uixml_i18n.cpp" line="185"/>
        <location filename="i18n/uixml_i18n.cpp" line="196"/>
        <location filename="i18n/uixml_i18n.cpp" line="207"/>
        <location filename="i18n/uixml_i18n.cpp" line="218"/>
        <source>main</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/gui/widgets/qfxmlconfigwidget/qfxmlconfigwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="52"/>
        <location filename="i18n/uixml_i18n.cpp" line="63"/>
        <location filename="i18n/uixml_i18n.cpp" line="74"/>
        <location filename="i18n/uixml_i18n.cpp" line="85"/>
        <location filename="i18n/uixml_i18n.cpp" line="186"/>
        <location filename="i18n/uixml_i18n.cpp" line="197"/>
        <location filename="i18n/uixml_i18n.cpp" line="208"/>
        <location filename="i18n/uixml_i18n.cpp" line="219"/>
        <source>&amp;Expand tree</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/gui/widgets/qfxmlconfigwidget/qfxmlconfigwidget.ui.xml</comment>
        <translation>&amp;Rozbalit strom</translation>
    </message>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="53"/>
        <location filename="i18n/uixml_i18n.cpp" line="64"/>
        <location filename="i18n/uixml_i18n.cpp" line="75"/>
        <location filename="i18n/uixml_i18n.cpp" line="86"/>
        <location filename="i18n/uixml_i18n.cpp" line="187"/>
        <location filename="i18n/uixml_i18n.cpp" line="198"/>
        <location filename="i18n/uixml_i18n.cpp" line="209"/>
        <location filename="i18n/uixml_i18n.cpp" line="220"/>
        <source>&amp;Colapse tree</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/gui/widgets/qfxmlconfigwidget/qfxmlconfigwidget.ui.xml</comment>
        <translation>&amp;Zabalit strom</translation>
    </message>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="54"/>
        <location filename="i18n/uixml_i18n.cpp" line="65"/>
        <location filename="i18n/uixml_i18n.cpp" line="76"/>
        <location filename="i18n/uixml_i18n.cpp" line="87"/>
        <location filename="i18n/uixml_i18n.cpp" line="188"/>
        <location filename="i18n/uixml_i18n.cpp" line="199"/>
        <location filename="i18n/uixml_i18n.cpp" line="210"/>
        <location filename="i18n/uixml_i18n.cpp" line="221"/>
        <source>&amp;Defaults</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/gui/widgets/qfxmlconfigwidget/qfxmlconfigwidget.ui.xml</comment>
        <translation>&amp;Defaultní hodnoty</translation>
    </message>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="56"/>
        <location filename="i18n/uixml_i18n.cpp" line="67"/>
        <location filename="i18n/uixml_i18n.cpp" line="78"/>
        <location filename="i18n/uixml_i18n.cpp" line="89"/>
        <location filename="i18n/uixml_i18n.cpp" line="190"/>
        <location filename="i18n/uixml_i18n.cpp" line="201"/>
        <location filename="i18n/uixml_i18n.cpp" line="212"/>
        <location filename="i18n/uixml_i18n.cpp" line="223"/>
        <source>Smazat</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/gui/widgets/qfdataformwidget/qfdataformdialogwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="57"/>
        <location filename="i18n/uixml_i18n.cpp" line="68"/>
        <location filename="i18n/uixml_i18n.cpp" line="79"/>
        <location filename="i18n/uixml_i18n.cpp" line="90"/>
        <location filename="i18n/uixml_i18n.cpp" line="191"/>
        <location filename="i18n/uixml_i18n.cpp" line="202"/>
        <location filename="i18n/uixml_i18n.cpp" line="213"/>
        <location filename="i18n/uixml_i18n.cpp" line="224"/>
        <source>Storno</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/gui/widgets/qfdataformwidget/qfdataformdialogwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="58"/>
        <location filename="i18n/uixml_i18n.cpp" line="69"/>
        <location filename="i18n/uixml_i18n.cpp" line="80"/>
        <location filename="i18n/uixml_i18n.cpp" line="91"/>
        <location filename="i18n/uixml_i18n.cpp" line="192"/>
        <location filename="i18n/uixml_i18n.cpp" line="203"/>
        <location filename="i18n/uixml_i18n.cpp" line="214"/>
        <location filename="i18n/uixml_i18n.cpp" line="225"/>
        <source>Ok</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/gui/widgets/qfdataformwidget/qfdataformdialogwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="i18n/uixml_i18n.cpp" line="59"/>
        <location filename="i18n/uixml_i18n.cpp" line="70"/>
        <location filename="i18n/uixml_i18n.cpp" line="81"/>
        <location filename="i18n/uixml_i18n.cpp" line="92"/>
        <location filename="i18n/uixml_i18n.cpp" line="193"/>
        <location filename="i18n/uixml_i18n.cpp" line="204"/>
        <location filename="i18n/uixml_i18n.cpp" line="215"/>
        <location filename="i18n/uixml_i18n.cpp" line="226"/>
        <source>Uložit</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/gui/widgets/qfdataformwidget/qfdataformdialogwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="i18n/uixml_i18n.cpp" line="60"/>
        <location filename="i18n/uixml_i18n.cpp" line="71"/>
        <location filename="i18n/uixml_i18n.cpp" line="82"/>
        <location filename="i18n/uixml_i18n.cpp" line="93"/>
        <location filename="i18n/uixml_i18n.cpp" line="194"/>
        <location filename="i18n/uixml_i18n.cpp" line="205"/>
        <location filename="i18n/uixml_i18n.cpp" line="216"/>
        <location filename="i18n/uixml_i18n.cpp" line="227"/>
        <source>Znovu načíst</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/gui/widgets/qfdataformwidget/qfdataformdialogwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="95"/>
        <location filename="i18n/uixml_i18n.cpp" line="116"/>
        <location filename="i18n/uixml_i18n.cpp" line="229"/>
        <location filename="i18n/uixml_i18n.cpp" line="250"/>
        <source>&amp;Soubor</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="96"/>
        <location filename="i18n/uixml_i18n.cpp" line="117"/>
        <location filename="i18n/uixml_i18n.cpp" line="230"/>
        <location filename="i18n/uixml_i18n.cpp" line="251"/>
        <source>&amp;Export</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="97"/>
        <location filename="i18n/uixml_i18n.cpp" line="118"/>
        <location filename="i18n/uixml_i18n.cpp" line="231"/>
        <location filename="i18n/uixml_i18n.cpp" line="252"/>
        <source>&amp;Report</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="98"/>
        <location filename="i18n/uixml_i18n.cpp" line="119"/>
        <location filename="i18n/uixml_i18n.cpp" line="232"/>
        <location filename="i18n/uixml_i18n.cpp" line="253"/>
        <source>&amp;Data</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="i18n/uixml_i18n.cpp" line="99"/>
        <location filename="i18n/uixml_i18n.cpp" line="120"/>
        <location filename="i18n/uixml_i18n.cpp" line="233"/>
        <location filename="i18n/uixml_i18n.cpp" line="254"/>
        <source>&amp;Zobrazení</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="100"/>
        <location filename="i18n/uixml_i18n.cpp" line="121"/>
        <location filename="i18n/uixml_i18n.cpp" line="234"/>
        <location filename="i18n/uixml_i18n.cpp" line="255"/>
        <source>main</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="101"/>
        <location filename="i18n/uixml_i18n.cpp" line="122"/>
        <location filename="i18n/uixml_i18n.cpp" line="235"/>
        <location filename="i18n/uixml_i18n.cpp" line="256"/>
        <source>&amp;Tisk</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="102"/>
        <location filename="i18n/uixml_i18n.cpp" line="123"/>
        <location filename="i18n/uixml_i18n.cpp" line="236"/>
        <location filename="i18n/uixml_i18n.cpp" line="257"/>
        <source>&amp;acrobat PDF</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="103"/>
        <location filename="i18n/uixml_i18n.cpp" line="124"/>
        <location filename="i18n/uixml_i18n.cpp" line="237"/>
        <location filename="i18n/uixml_i18n.cpp" line="258"/>
        <source>&amp;HTML</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="104"/>
        <location filename="i18n/uixml_i18n.cpp" line="125"/>
        <location filename="i18n/uixml_i18n.cpp" line="238"/>
        <location filename="i18n/uixml_i18n.cpp" line="259"/>
        <source>&amp;poslat report e-mailem</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="i18n/uixml_i18n.cpp" line="105"/>
        <location filename="i18n/uixml_i18n.cpp" line="126"/>
        <location filename="i18n/uixml_i18n.cpp" line="239"/>
        <location filename="i18n/uixml_i18n.cpp" line="260"/>
        <source>&amp;Editovat formulář</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="i18n/uixml_i18n.cpp" line="106"/>
        <location filename="i18n/uixml_i18n.cpp" line="127"/>
        <location filename="i18n/uixml_i18n.cpp" line="240"/>
        <location filename="i18n/uixml_i18n.cpp" line="261"/>
        <source>&amp;Zobrazit jako HTML</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="i18n/uixml_i18n.cpp" line="107"/>
        <location filename="i18n/uixml_i18n.cpp" line="128"/>
        <location filename="i18n/uixml_i18n.cpp" line="241"/>
        <location filename="i18n/uixml_i18n.cpp" line="262"/>
        <source>&amp;Zvětšit</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="i18n/uixml_i18n.cpp" line="108"/>
        <location filename="i18n/uixml_i18n.cpp" line="129"/>
        <location filename="i18n/uixml_i18n.cpp" line="242"/>
        <location filename="i18n/uixml_i18n.cpp" line="263"/>
        <source>&amp;Zmenšit</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="i18n/uixml_i18n.cpp" line="109"/>
        <location filename="i18n/uixml_i18n.cpp" line="130"/>
        <location filename="i18n/uixml_i18n.cpp" line="243"/>
        <location filename="i18n/uixml_i18n.cpp" line="264"/>
        <source>Zvětšit na šířku stránky</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="i18n/uixml_i18n.cpp" line="110"/>
        <location filename="i18n/uixml_i18n.cpp" line="131"/>
        <location filename="i18n/uixml_i18n.cpp" line="244"/>
        <location filename="i18n/uixml_i18n.cpp" line="265"/>
        <source>Zvětšit na výšku stránky</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="i18n/uixml_i18n.cpp" line="111"/>
        <location filename="i18n/uixml_i18n.cpp" line="132"/>
        <location filename="i18n/uixml_i18n.cpp" line="245"/>
        <location filename="i18n/uixml_i18n.cpp" line="266"/>
        <source>&amp;První strana</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="i18n/uixml_i18n.cpp" line="112"/>
        <location filename="i18n/uixml_i18n.cpp" line="133"/>
        <location filename="i18n/uixml_i18n.cpp" line="246"/>
        <location filename="i18n/uixml_i18n.cpp" line="267"/>
        <source>&amp;Předchozí strana</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="i18n/uixml_i18n.cpp" line="113"/>
        <location filename="i18n/uixml_i18n.cpp" line="134"/>
        <location filename="i18n/uixml_i18n.cpp" line="247"/>
        <location filename="i18n/uixml_i18n.cpp" line="268"/>
        <source>&amp;Další strana</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="i18n/uixml_i18n.cpp" line="114"/>
        <location filename="i18n/uixml_i18n.cpp" line="135"/>
        <location filename="i18n/uixml_i18n.cpp" line="248"/>
        <location filename="i18n/uixml_i18n.cpp" line="269"/>
        <source>&amp;Poslední strana</source>
        <comment>/mnt/sda7/fanda/qt/libqf/libqfgui/reports/widgets/qfreportviewwidget.ui.xml</comment>
        <translation></translation>
    </message>
</context>
</TS>
