/********************************************************************
	created:	2005/01/02 14:27
	filename: 	fqxmltreemodel.cpp
	author:		Fanda Vacek (fanda.vacek@volny.cz)
*********************************************************************/

#include <QIcon>
#include <QApplication>
#include <QStyle>
#include <QDebug>
#include <QDomDocument>

#include "qfexception.h"
#include "qfstring.h"
#include "qfxmltreemodel.h"

#include <qflogcust.h>
//#define DEBUG_QFXMLTREEMODEL
#ifdef DEBUG_QFXMLTREEMODEL
#	define qd qfTrash
#else
#	define qd if(1) ; else qfTrash
#endif

//=============================================
//                QFXmlTreeModel
//=============================================
QFXmlTreeModel::~QFXmlTreeModel()
{
}

void QFXmlTreeModel::setContent(const QDomNode &root) throw(QFException)
{
	qfLogFuncFrame() << root.nodeName() << root.nodeType();
	if(root.isNull()) {
		//qfWarning() << "QFXmlTreeModel::setContent() - DomNode is not Element or Document.";//QF_EXCEPTION("QFXmlTreeModel::setContent() - DomNode is not Element or Document.");
		f_document = QFDomDocument();
		f_root = f_document;
	}
	else if(!(root.isElement() || root.isDocument())) {
		qfWarning() << "QFXmlTreeModel::setContent() - DomNode is not Element or Document.";//QF_EXCEPTION("QFXmlTreeModel::setContent() - DomNode is not Element or Document.");
		f_document = QFDomDocument();
		f_root = f_document;
	}
	else {
		f_document = root.ownerDocument();
		f_root = root;
	}
	reset();/// tuhle zasranou chybu jsem hledal celej den a noc, kurva, kurva, kurva
}

void QFXmlTreeModel::setContent(QFile &f) throw(QFException)
{
	QFDomDocument doc;
	doc.setContent(f);
	setContent(doc);
}
/*
void QFXmlTreeModel::setContent(const QDomDocument &_doc)
{
	QDomDocument d(_doc);
	root = d;
}
*/

void QFXmlTreeModel::reset()
{
	indexMap.clear();
	QAbstractItemModel::reset();
}

bool QFXmlTreeModel::isNodeAccepted(const QDomNode& nd) const
{
	//qfTrash() << QF_FUNC_NAME;
	Q_UNUSED(nd);
	return true;
}

bool QFXmlTreeModel::isNodeCachedOrAccepted(const QDomNode& nd) const
{
	void *d = getNodeImpl(nd);
	if(indexMap.contains(d)) return true;
	return isNodeAccepted(nd);
}

QDomNode QFXmlTreeModel::index2node(const QModelIndex &ix) const
{
	//if(!ix.isValid()) return root;
	if(!ix.isValid()) return QDomNode();
	void *d = ix.internalPointer();
	QDomNode ret = indexMap.value(d);
	//if(d && ret.isNull()) qfError() << QF_FUNC_NAME << d << "NULL";
	return ret;
}

QModelIndex QFXmlTreeModel::createIndex(int row, int col, const QDomNode& nd) const
{
	QModelIndex ret;
	if(nd.isNull()) return ret;
	if(nd == f_root) return ret;

	// insert node to index map
	// QDomNode is EXPLICITLY shared, this is why it works !
	void *d = getNodeImpl(nd);
	IndexMap &m = const_cast<IndexMap &>(indexMap);
	/*
	while(0) { // DEBUG PURPOSES
	QDomNode tmp = indexMap.value(d);
	if(tmp.isNull()) qd() << "adding node '" << nd.nodeName() << "' to index map";
	break;
}
	*/
	m[d] = nd;
	return QAbstractItemModel::createIndex(row, col, d);
}

QModelIndex QFXmlTreeModel::node2index(const QDomNode& nd) const
{
	//qfTrash() << QF_FUNC_NAME;
	QModelIndex ret;
	if(nd.isNull()) return ret;
	if(nd == f_root) return ret;

	// find node in its parent node children list
	QDomNode parent = nd.parentNode();
	if(parent.isNull()) return ret;
	//Q_ASSERT_X(!parent.isNull(), "QFXmlTreeModel::node2index()", "index node should have parent node.");
	int row = -1;
	QDomNodeList lst = parent.childNodes();
	for(int i=0; i<lst.count(); i++) {
		QDomNode n = lst.item(i);
		if(!isNodeCachedOrAccepted(n)) continue;
		row++;
		if(n == nd) {
			break;
		}
	}

	if(row < 0) {
		qd() << "QFXmlTreeModel::node2index()" << " index of " << nd.nodeName() << " NOT FOUND";
		return ret;
	}
	return createIndex(row, 0, nd);
}


QModelIndex QFXmlTreeModel::index(int row, int column, const QModelIndex &parent) const
{
	qd() << QString().sprintf("index() called model: %p data: %p row: %i col: %i", parent.model(), parent.internalPointer(), row, column);
	QModelIndex ret = QModelIndex();
	do {
		QDomNode nd = (parent.isValid())? index2node(parent): f_root;
		//if(nd.isNull()) {nd = root;}// row = column = 0;}
		int cnt = nd.childNodes().count();
		if(row < 0 || row >= cnt) break;
		QDomNodeList lst = nd.childNodes();
		int r = 0;
		for(int i=0; i<lst.count(); i++) {
			nd = lst.item(i);
			if(!isNodeCachedOrAccepted(nd)) continue;
			if(r == row) {
				ret = createIndex(row, column, nd);
				break;
			}
			r++;
		}
	} while(false);
	qd() << "return:" << ret.internalPointer();
	return ret;
}


QModelIndex QFXmlTreeModel::parent(const QModelIndex &child) const
{
	//qfTrash() << QF_FUNC_NAME;
	qd() << "QFXmlTreeModel::parent()" << " child: " << child.internalPointer();
    //QString s = "NULL";
	QModelIndex ret;
	do {
		QDomNode node = index2node(child);
        //s = node.toElement().tagName();
		//if(s.length() == 0) s = "NO_ELEMENT";
        //qDebug() << "\tnode: " << node.nodeName();
        //s = "NULL";
		if(node.isNull()) break;
		QDomNode parent_node = node.parentNode();
		ret = node2index(parent_node);
	} while(false);
	qd() << "\tRETURN: " << "row: "<<ret.row()<<" col: "<<ret.column()<<" data: "<<ret.internalPointer();
    //qDebug() << __PRETTY_FUNCTION__ << " ... out";
	return ret;
}


int QFXmlTreeModel::rowCount(const QModelIndex & parent) const
{
	int ret = 0;
	qd() << QString().sprintf("rowCount() called model: %p data: %p row: %i col: %i", parent.model(), parent.internalPointer(), parent.row(), parent.column());
	QDomNode nd = (parent.isValid())? index2node(parent): f_root;
    //int row = parent.row();
	if(nd.isNull()) nd = f_root;
	qd() << "\tparent name:" << nd.toElement().tagName();
	QDomNodeList lst = nd.childNodes();
	for(int i=0; i<lst.count(); i++) {
		nd = lst.item(i);
		if(!isNodeCachedOrAccepted(nd)) continue;
		ret++;
	}
	qd() << "\treturning:" << ret;
	return ret;
}


int QFXmlTreeModel::columnCount(const QModelIndex & parent) const
{
	Q_UNUSED(parent);
        //qDebug("columnCount() called model: %p data: %p row: %i col: %i ret: %i", parent.model(), parent.data(), parent.row(), parent.column(), ret);

	/*
	zda se, ze se nastavi tolik sloupcu, kolik vrati prvni volani teto funkce,
	neni to tak, nasledujici kod vyrobil 5 sloupcu
	static cnt=0;
	cnt++;
	return cnt;
	*/
	return 2;
	/*
	QDomNode nd = backupDomNode(parent);
	if(nd.isNull()) return 3;
	QDomElement el = nd.toElement();
	if(!el.isNull()) return 2;
	return 1;
	*/
}


bool QFXmlTreeModel::hasChildren(const QModelIndex & parent) const
{
	return (rowCount(parent) > 0);
}


QVariant QFXmlTreeModel::data(const QModelIndex& index, int role) const
{
    static bool first_scan = true;
	static QIcon icoNode;
	static QIcon icoProcessInstr;
	static QIcon icoOther;

	if(first_scan) {
		first_scan = false;
		icoNode.addPixmap(QApplication::style()->standardPixmap(QStyle::SP_DirClosedIcon), QIcon::Normal, QIcon::Off);
		icoNode.addPixmap(QApplication::style()->standardPixmap(QStyle::SP_DirOpenIcon), QIcon::Normal, QIcon::On);
		icoProcessInstr.addPixmap(QApplication::style()->standardPixmap(QStyle::SP_FileIcon));
		icoOther.addPixmap(QApplication::style()->standardPixmap(QStyle::SP_FileIcon));
	}

	//if(!index.isValid()) return QVariant();
	//qDebug() << QF_FUNC_NAME;;
	QDomNode nd = index2node(index);
	//qDebug() << "\t" << nd.nodeName();
	if(nd.isNull()) return QVariant("NODE NOT FOUND");
	QVariant ret;

	if(role == Qt::DecorationRole) {
		if(index.column() == 0) {
			if(nd.isElement()) ret = qVariantFromValue(icoNode);
			else if(nd.isProcessingInstruction()) ret = qVariantFromValue(icoProcessInstr);
			else ret = qVariantFromValue(icoOther);
		}
	}
	else if(role == Qt::DisplayRole) {
		if(nd.isElement()) {
			if(index.column() == 0)
				ret = QVariant(" " + nd.nodeName());
			else if(index.column() == 1) {
				QDomElement el = nd.toElement();
				ret = nd.nodeName();
				if(!el.isNull()) {
					QDomNamedNodeMap attrs = el.attributes();
					QString s;
					for(int i=0; i<attrs.count(); i++) {
						QDomNode n = attrs.item(i);
						if(i > 0) s += " ";
						s += n.nodeName() + "=\"" + n.nodeValue() + "\"";
					}
					ret = s;
				}
			}
		}
		else {
			if(index.column() == 0)
				ret = " " + nd.nodeName();
			else if(index.column() == 1) {
				ret = nd.nodeValue();
			}
		}
	}
	return ret;
}


QVariant QFXmlTreeModel::headerData(int section, Qt::Orientation o, int role) const
{
	QVariant ret;
	if (o == Qt::Horizontal) {
        if(role == Qt::DisplayRole) switch(section) {
			case 0: ret = "Name"; break;
			case 1: ret = "Attributes"; break;
        }
    }
	if(ret.isNull()) ret = QAbstractItemModel::headerData(section, o, role);
	return ret;
}


QModelIndex QFXmlTreeModel::insertBefore(const QDomNode &nd, const QModelIndex &parent_ix, int row) throw(QFException)
{
	qfTrash() << QF_FUNC_NAME;
	//qfTrash() << "\t" << __LINE__;
	QModelIndex ret;
	QDomNode ins_nd;
	QDomNode par_nd = (parent_ix.isValid())? index2node(parent_ix): f_root;
	if(par_nd.isNull()) {
		qfTrash() << "\tparent node is NULL";
		return ret;
	}
	if(row < 0 || row >= rowCount(parent_ix)) {
		row = rowCount(parent_ix);
		emit beginInsertRows(parent_ix, row, row);
		ins_nd = par_nd.insertAfter(nd, QDomNode());
	}
	else {
		QModelIndex sibling_ix = index(row, 0, parent_ix);
		QDomNode sibling_nd = index2node(sibling_ix);
		emit beginInsertRows(parent_ix, row, row);
		ins_nd = par_nd.insertBefore(nd, sibling_nd);
	}
	emit endInsertRows();
	if(!ins_nd.isNull()) {
		ret = node2index(ins_nd);
	}
	return ret;
}

void QFXmlTreeModel::replace(const QDomNode & nd, const QModelIndex & ix) throw( QFException )
{
	qfLogFuncFrame();
	QDomNode old_nd = index2node(ix);
	if(old_nd.isNull()) {
		qfTrash() << "\t old node is NULL";
	}
	else {
		/// bohuzel musim stary node vyjmout a novy pridat, jinak mi blbnou vsechny QModelIndexy na zmeneny node, protoze se zmenila adresa impl a oni maji jeste starou
		/*
		parent_nd.replaceChild(nd, old_nd);
		emit dataChanged(ix, ix);
		*/
		int row = ix.row();
		insertAfter(nd, ix.parent(), row);
		take(ix);
		/*
		QDomNode parent_nd = old_nd.parentNode();
		if(parent_nd.isNull()) {
			qfError() << QF_FUNC_NAME << "parent_node is NULL.";
		}
		else {
		}
		*/
	}
}

QDomNode QFXmlTreeModel::takeNode(const QDomNode & nd)
{
	QModelIndex ix = node2index(nd);
	if(!ix.isValid()) return QDomNode();
	QModelIndex par_ix = ix.parent();
	QDomNode par_nd = (par_ix.isValid())? index2node(par_ix): f_root;
	beginRemoveRows(par_ix, ix.row(), ix.row());
	QDomNode ret = par_nd.removeChild(nd);
	void *d = getNodeImpl(nd);
	indexMap.remove(d);
	endRemoveRows();
	return ret;
}

QDomNode QFXmlTreeModel::take(const QModelIndex &ix)
{
	if(!ix.isValid()) return QDomNode();
	QDomNode nd = index2node(ix);
	return takeNode(nd);
}

bool QFXmlTreeModel::removeRows(int row, int count, const QModelIndex & parent)
{
	bool ret = true;
	QDomNode par_nd = (parent.isValid())? index2node(parent): f_root;
	QList<QDomNode> lst;
	for(int i=0; i<count; i++) {
		QModelIndex child_ix = parent.child(row + i, 0);
		QDomNode nd = index2node(child_ix);
		if(!nd.isNull()) lst << nd;
	}
	//QDomNode child_nd = index2node(child_ix);
	if(lst.count() == count) {
		beginRemoveRows(parent, row, row + count - 1);
		foreach(QDomNode nd, lst) {
			par_nd.removeChild(nd);
			void *d = getNodeImpl(nd);
			indexMap.remove(d);
		}
		endRemoveRows();
	}
	else {
		qfError() << "Different node coutns, index count:" << count << "dodes count:" << lst.count();
		ret = false;
	}
	return ret;
}

void QFXmlTreeModel::removeChildren(const QModelIndex &parent_ix)
{
	qfTrash() << QF_FUNC_NAME;
	if(!parent_ix.isValid()) return; /// neumi vymazat deti celyho dokumentu
	QFDomElement el = index2node(parent_ix).toElement();
	//if(o) o->dumpObjectTree(); else qfTrash() << "NULL";
	if(el) {
		int n2 = rowCount(parent_ix)-1;
		if(n2 >= 0) {
			//qfTrash() << "\tbeginRemoveRows():" << o->objectName() << "from" << 0 << "to" << n2;
			beginRemoveRows(parent_ix, 0, n2);
			QDomNodeList lst = el.childNodes();
			for(int i=0;  i<lst.count(); i++) {
				QDomNode nd = lst.at(i);
				void *d = getNodeImpl(nd);
				indexMap.remove(d);
			}
			el.removeChildren();
			//qfTrash() << "\tendRemoveRows():";
			endRemoveRows();
		}
	}
}






