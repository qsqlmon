#ifndef QFXMLTREEMODEL_H
#define QFXMLTREEMODEL_H

/********************************************************************
	created:	2005/01/02 14:27
	filename: 	fqxmltreemodel.h
	author:		Fanda Vacek (fanda.vacek@volny.cz)
*********************************************************************/

#include <QDomDocument>
#include <QAbstractItemModel>
#include <QFile>
#include <QMap>
#include <QHash>

#include <qfguiglobal.h>
#include <qfexception.h>
#include <qfdom.h>

//=============================================
//                QFXmlTreeModel
//=============================================
/**
    Data model for XML and QTreeView.
    Pointer in this model's QModelIndex points to implementation of indexed node.
*/
class QFGUI_DECL_EXPORT QFXmlTreeModel : public QAbstractItemModel
{
	friend class QFXmlConfigWidget;
	//=============================================
	//                PublicDomNode
	//=============================================
	class PublicDomNode : public QDomNode
	{
		public:
			void* getImpl() const {return impl;}

			PublicDomNode(const QDomNode& nd) : QDomNode(nd) {}
			//PublicDomNode(void *impl) : QDomNode((QDomNodePrivate*)impl) {}
	};
	public:
		typedef QHash<void*, QDomNode> IndexMap;
	protected:
		IndexMap indexMap;
	protected:
		QDomDocument f_document; /// radsi si drzim dokument elementu, aby mi nezustaly v modelu elementy, kterych dokument uz neexistuje
		QDomNode f_root;
	public:
		/// Sets model contens to part of a XML file.
		void setContent(const QDomNode &root) throw(QFException);
		/// Sets model contens to XML file.
		void setContent(QFile &f) throw(QFException);
		/// Sets model contens to other XML document. (Xml nodes are explicitly shared)
		//void setContent(const QDomDocument &d);

		// tyhle musim implementovat pro ReadOnly
		QModelIndex index(int row, int column, const QModelIndex &parent) const;
		QModelIndex parent ( const QModelIndex & index ) const;
		int rowCount( const QModelIndex & parent_nd = QModelIndex() ) const;
		int columnCount ( const QModelIndex & parent_nd = QModelIndex() ) const;
		bool hasChildren ( const QModelIndex & parent_nd = QModelIndex() ) const;
		QVariant data ( const QModelIndex & index, int role = Qt::DisplayRole ) const;
		QVariant headerData ( int section, Qt::Orientation o, int role = Qt::DisplayRole ) const;
	public:
		virtual QDomNode index2node(const QModelIndex &ix) const;
		virtual QModelIndex node2index(const QDomNode& node) const;
	protected:
		void reset();
		QModelIndex createIndex(int row, int col, const QDomNode& node) const; //< help function for speed & efectivity
		//QModelIndex node2index(int row, int column, const QDomNode& node) const;
		void* getNodeImpl(const QDomNode& nd) const {return PublicDomNode(nd).getImpl();}
		//! If this function returns false, node is simply ignored by model. Thats mean it is not dislayed in treeview.
		virtual bool isNodeAccepted(const QDomNode& nd) const;
		/// Pokud je node v cachi, neni treba zkoumat, jestli je taky accepted, to uz se stalo.
		inline bool isNodeCachedOrAccepted(const QDomNode& nd) const;
	public:
		/// insert object before \a row , if \a row >= rowCount() or row < 0 the node is appended.
		virtual QModelIndex insertBefore(const QDomNode &nd, const QModelIndex &parent_ix, int row) throw(QFException);
		QModelIndex insertAfter(const QDomNode &nd, const QModelIndex &parent_ix, int row) throw(QFException) {
			return insertBefore(nd, parent_ix, row+1);
		}
		QModelIndex append(const QDomNode &nd, const QModelIndex &parent_ix) throw(QFException) {
			return insertBefore(nd, parent_ix, rowCount(parent_ix));
		}
		virtual bool removeRows(int row, int count, const QModelIndex & parent = QModelIndex());
		virtual void replace(const QDomNode &nd, const QModelIndex &ix) throw(QFException);
		/// pozor na opakovane pouziti teto funkce napr. pro vymazani oznacenych indexu cyklem for
		/// volani funkce take() zpusobi, ze selectedIndexes() uz nejsou to co byly puvodne
		virtual QDomNode take(const QModelIndex &ix);
		QDomNode takeNode(const QDomNode &nd);
		void removeChildren(const QModelIndex &parent_ix);

		QDomDocument document() const {return f_document;}
		QDomNode root() const {return f_root;}
	public:
		QFXmlTreeModel(QObject *parent = NULL) : QAbstractItemModel(parent) { }
		~QFXmlTreeModel();
};
//=============================================

#endif // QFXMLTREEMODEL_H
