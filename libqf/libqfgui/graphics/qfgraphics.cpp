
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfgraphics.h"

#include <qfassert.h>

#include <QPaintDevice>

#include <qflogcust.h>

QFGraphics::Rect QFGraphics::Rect::united(const QFGraphics::Rect & _r2) const
{
	Rect r1 = this->normalized();
	Rect r2 = _r2.normalized();
	qreal l = qMin(r1.left(), r2.left());
	qreal r = qMax(r1.right(), r2.right());
	qreal t = qMin(r1.top(), r2.top());
	qreal b = qMax(r1.bottom(), r2.bottom());
	return Rect(l, t, r-l, b-t);
}

qreal QFGraphics::x2device(qreal x, QPaintDevice *dev)
{
	QF_ASSERT(dev, "dev is NULL");
	double dpmm = dev->logicalDpiX() / 25.4;
	return x * dpmm;
}

qreal QFGraphics::y2device(qreal y, QPaintDevice *dev)
{
	QF_ASSERT(dev, "dev is NULL");
	double dpmm = dev->logicalDpiY() / 25.4;
	return y * dpmm;
}

qreal QFGraphics::device2x(qreal x, QPaintDevice *dev)
{
	QF_ASSERT(dev, "dev is NULL");
	double dpmm = dev->logicalDpiX() / 25.4;
	return x / dpmm;
}

qreal QFGraphics::device2y(qreal y, QPaintDevice *dev)
{
	QF_ASSERT(dev, "dev is NULL");
	double dpmm = dev->logicalDpiY() / 25.4;
	return y / dpmm;
}

QFGraphics::Rect QFGraphics::mm2device(const QFGraphics::Rect &r, QPaintDevice *dev)
{
	Rect ret;
	ret.setLeft(x2device(r.left(), dev));
	ret.setTop(y2device(r.top(), dev));
	ret.setWidth(x2device(r.width(), dev));
	ret.setHeight(y2device(r.height(), dev));
	return ret;
}

QFGraphics::Point QFGraphics::mm2device(const QFGraphics::Point &p, QPaintDevice *dev)
{
	Point ret;
	ret.setX(x2device(p.x(), dev));
	ret.setY(y2device(p.y(), dev));
	return ret;
}

QFGraphics::Point QFGraphics::device2mm(const QFGraphics::Point &p, QPaintDevice *dev)
{
	QF_ASSERT(dev, "dev is NULL");
	double x_dpmm = dev->logicalDpiX() / 25.4;
	double y_dpmm = dev->logicalDpiY() / 25.4;
	Point ret;
	ret.setX(p.x() / x_dpmm);
	ret.setY(p.y() / y_dpmm);
	return ret;
}

QFGraphics::Rect QFGraphics::device2mm(const QFGraphics::Rect &r, QPaintDevice *dev)
{
	QF_ASSERT(dev, "dev is NULL");
	double x_dpmm = dev->logicalDpiX() / 25.4;
	double y_dpmm = dev->logicalDpiY() / 25.4;
	Rect ret;
	ret.setLeft(r.left() / x_dpmm);
	ret.setTop(r.top() / y_dpmm);
	ret.setWidth(r.width() / x_dpmm);
	ret.setHeight(r.height() / y_dpmm);
	return ret;
}


