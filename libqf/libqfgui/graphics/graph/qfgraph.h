
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFGRAPH_H
#define QFGRAPH_H

#include <qfguiglobal.h>
#include <qfexception.h>
#include <qfxmltable.h>
#include <qfgraphicsstylecache.h>
#include <qfgraphics.h>

#include <QSharedData>
#include <QPainter>

#include <cmath>
//class QPainter;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFGraph
{
	public:
		typedef QFGraphicsStyleCache::Style TextStyle;

		typedef QFGraphics::Point Point;
		typedef QFGraphics::Size Size;
		typedef QFGraphics::Rect Rect;
	public:
		class QFGUI_DECL_EXPORT Axis
		{
			public:
				enum ValuesFrom {TakeValues, TakeOrder};
				enum Direction {DirectionX, DirectionY};
			private:
				class SharedDummyHelper {};
				struct Data : public QSharedData {
					QString label;
					QString labelStyle;
					QString labelFormat;
					double min, max, tick;
					QString serieName;
					ValuesFrom valuesFrom;
					Direction direction;
					Rect boundingRect;
					bool hidden;
					bool gridLines;

					Data( ): min(HUGE_VAL), max(-HUGE_VAL), tick(0), valuesFrom(TakeValues), direction(DirectionY), hidden(false),gridLines(false) {}
				};
				QSharedDataPointer<Data> d;
				static const Axis& sharedNull();
				Axis(SharedDummyHelper); /// null row constructor
			public:
				bool isNull() const {return d == sharedNull().d;}

				QString serieName() const {return d->serieName;}
				void setSerieName(const QString &nm) {d->serieName = nm;}
				QString label() const {return d->label;}
				void setLabel(const QString &lbl) {d->label = lbl;}
				QString labelStyle() const {return d->labelStyle;}
				void setLabelStyle(const QString &lblst) {d->labelStyle = lblst;}
				QString labelFormat() const {return d->labelFormat;}
				void setLabelFormat(const QString &fmt) {d->labelFormat = fmt;}
				double min() const {return d->min;}
				void setMin(double dd) {d->min = dd;}
				double max() const {return d->max;}
				void setMax(double dd) {d->max = dd;}
				double tick() const {return d->tick;}
				void setTick(double dd) {d->tick = dd;}
				Direction direction() const {return d->direction;}
				void setDirection(Direction dd) {d->direction = dd;}
				ValuesFrom valuesFrom() const {return d->valuesFrom;}
				void setValuesFrom(ValuesFrom dd) {d->valuesFrom = dd;}
				Rect boundingRect() const {return d->boundingRect;}
				void setBoundingRect(const Rect &r) {d->boundingRect = r;}
				bool isHidden() const {return d->hidden;}
				void setHidden(bool b) {d->hidden = b;}
				bool isGridLines() const {return d->gridLines;}
				void setGridLines(bool b) {d->gridLines = b;}

				/// vraci mm souradnici value v grafu
				qreal value2pos(qreal value, const Rect &grid_rect);
				qreal tickSize(const Rect &grid_rect);

				QString formatTickLabel(const QVariant &label_value);
				
				QString toString() const {
					return QString("min: %2, max: %3, tick: %4").arg(min()).arg(max()).arg(tick());
				}
			public:
				Axis();
		};
		class QFGUI_DECL_EXPORT Legend
		{
			public:
				typedef QList<QColor> ColorList;
			private:
				class SharedDummyHelper {};
				struct Data : public QSharedData {
					//ColorList colors;
					//QStringList labels;
					Rect boundingRect;
				};
				QSharedDataPointer<Data> d;
				static const Legend& sharedNull();
				Legend(SharedDummyHelper); /// null row constructor
			public:
				bool isNull() const {return d == sharedNull().d;}

				//const ColorList colors() const {return d->colors;}
				//void setColors(const ColorList &clst) {d->colors = clst;}
				//const QStringList labels() const {return d->labels;}
				//void setLabels(const QStringList &lbls) {d->labels = lbls;}
				Rect boundingRect() const {return d->boundingRect;}
				void setBoundingRect(const Rect &r) {d->boundingRect = r;}
			public:
				Legend();
		};
		class QFGUI_DECL_EXPORT Serie
		{
			public:
				//typedef QList<QColor> ColorList;
			private:
				class SharedDummyHelper {};
				struct Data : public QSharedData {
					QString columnCaption;
					QColor color;
					QVariantList values;
					QList<int> sortedValuesIndexes;
				};
				QSharedDataPointer<Data> d;
				static const Serie& sharedNull();
				Serie(SharedDummyHelper); /// null row constructor
			public:
				bool isNull() const {return d == sharedNull().d;}

				const QVariantList& values() const {return d->values;}
				QVariantList& valuesRef() {return d->values;}
				void setValues(const QVariantList &vals) {d->values = vals;}
				const QList<int>& sortedValuesIndexes() const {return d->sortedValuesIndexes;}
				void setSortedValuesIndexes(const QList<int> &ixs) {d->sortedValuesIndexes = ixs;}
				QColor color() const {return d->color;}
				void setColor(const QColor &c) {d->color = c;}

				QString columnCaption() const {return d->columnCaption;}
				void setColumnCaption(const QString &s) {d->columnCaption = s;}

				bool isSorted() const {return !sortedValuesIndexes().isEmpty();}
			public:
				Serie();
		};
	protected:
		typedef QMap<QString, Serie> SeriesMap;
		typedef QMap<QString, Axis> AxesMap;
		typedef QMap<QString, Legend> LegendMap;
		struct Data// : public QSharedData
		{
			QFXmlTable data;
			QFDomElement definition;
			QPainter *painter;
			Rect boundingRect;/// obdelnik celeho grafu vcetne os [mm]
			Rect gridRect; /// obdelnik grafu bez os (pouce to, kam se vykresluji data) [mm]
			QFGraphicsStyleCache styleCache;
			SeriesMap seriesMap;
			AxesMap axesMap;
			LegendMap legendMap;
			//int currentlyPolledColorIndex;

			Data() {
				painter = NULL;
				//currentlyPolledColorIndex = -1;
			}
		};
		Data _d;
		Data *d;
	protected:
		QPainter* painter() throw(QFException) {
			if(!d->painter) QF_EXCEPTION("painter is NULL");
			return d->painter;
		}
		const Rect& boundingRect() const {return d->boundingRect;}
		const Rect& gridRect() const {return d->gridRect;}

		AxesMap& axesMapRef() {return d->axesMap;}
		const AxesMap& axesMap() const {return d->axesMap;}
		//Axis& axisRef(const QString &colname) throw(QFException);
		Axis axisForSerie(const QString &colname);

		const LegendMap& legendMap() const {return d->legendMap;}
		const SeriesMap& seriesMap() const {return d->seriesMap;}

		qreal x2device(qreal x) {return QFGraphics::x2device(x, painter()->device());}
		qreal y2device(qreal y) {return QFGraphics::y2device(y, painter()->device());}
		qreal device2x(qreal x) {return QFGraphics::device2x(x, painter()->device());}
		qreal device2y(qreal y) {return QFGraphics::device2y(y, painter()->device());}
		Point mm2device(const Point &p) {return QFGraphics::mm2device(p, painter()->device());}
		Rect mm2device(const Rect &r) {return QFGraphics::mm2device(r, painter()->device());}
		Point device2mm(const Point &p) {return QFGraphics::device2mm(p, painter()->device());}
		Rect device2mm(const Rect &r) {return QFGraphics::device2mm(r, painter()->device());}

		//QRectF gridRect();
		QString title() const;
		/*
		virtual TextStyle textStyleFromString(const QString &str);
		virtual QColor colorFromString(const QString &str) {return QColor(str);}
		virtual QBrush brushFromString(const QString &str) {return QBrush(QColor(str));}
		virtual QPen penFromString(const QString &str) {return QPen(QColor(str));}
		*/
		virtual void createSeries();
		virtual void createAxes();

		virtual void drawAxis(const QString &colname, const QFGraph::Rect &_bounding_rect, bool do_not_draw = false);
		/// vraci legend id
		virtual QString drawLegend(const QFDomElement &el_legend, const QFGraph::Rect &_bounding_rect = Rect(), bool do_not_draw = false);
		// vraci legend id
		//virtual QString createLegend(const QFDomElement &el_legend);

		virtual void drawTitle();
		virtual Rect drawLegends(bool do_not_draw);
		virtual void drawAxes();
		virtual void drawSeries() {}
		virtual void drawBox();
		virtual void drawGrid();

		static QStringList colorNamesPull;
		static QColor colorForIndex(int ix);
	public:
		void setDefinition(const QDomElement el_def) {d->definition = el_def;}
		const QFDomElement& definition() const {return d->definition;}
		void setData(const QFXmlTable _data) {d->data = _data;}
		const QFXmlTable& data() const {return d->data;}

		void setStyleCache(const QFGraphicsStyleCache &cache) {d->styleCache = cache;}
		QFGraphicsStyleCache& styleCacheRef() {return d->styleCache;}
		const QFGraphicsStyleCache& styleCache() const {return d->styleCache;}

		virtual void draw(QPainter *painter, const QSizeF &size);
	public:
		static QFGraph* createGraph(const QDomElement el_def, const QFXmlTable &data = QFXmlTable());
	public:
		QFGraph();
		QFGraph(const QDomElement el_def, const QFXmlTable &data = QFXmlTable());
		virtual ~QFGraph();
};

class QFGUI_DECL_EXPORT QFHistogramGraph : public QFGraph
{
	protected:
		virtual void drawSeries();
	public:
		QFHistogramGraph() : QFGraph() {}
		QFHistogramGraph(const QDomElement el_def, const QFXmlTable &data = QFXmlTable()) : QFGraph(el_def, data) {}
};

class QFGUI_DECL_EXPORT QFPieGraph : public QFGraph
{
	protected:
		virtual void drawBox() {}
		virtual void drawSeries();
	public:
		QFPieGraph() : QFGraph() {}
		QFPieGraph(const QDomElement el_def, const QFXmlTable &data = QFXmlTable()) : QFGraph(el_def, data) {}
};

#endif // QFGRAPH_H

