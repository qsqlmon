
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFSCRIPTEDITORWIDGET_H
#define QFSCRIPTEDITORWIDGET_H

#include <qfguiglobal.h>

#include <qfdialogwidget.h>

class QFConsole;
class QFConsoleEvaluator;
class QFScriptDriver;
class QListWidgetItem;
namespace Ui {class QFScriptEditorWidget;};

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFScriptEditorWidget : public QFDialogWidget
{
	Q_OBJECT;
	public:
		enum {ScriptCodeRole = Qt::UserRole};
	private:
		Ui::QFScriptEditorWidget *ui;
	protected:
		QPointer<QFScriptDriver> f_scriptDriver;
		//QMap<QString, QString> scriptsMap;
		//int f_editedScriptIndex;
		QString f_defaultScriptCode;
		bool f_oneScriptNoSaveCheck; 
		QString f_domain;
		QString wholeCodeToEvaluate;
	public:
		QString currentScriptName() {return scriptName(-1);}
		QString currentScriptCode() {return scriptCode(-1);}
	protected:
		//! pokud script_name nebylo nalezeno, pouzije se \a new_script_code
		QString resolveScriptCode(const QString &script_name);
		
		int scriptIndex(const QString &script_name);
		QListWidgetItem* scriptItem(int ix);
		QListWidgetItem* scriptItem(const QString &script_name) {return scriptItem(scriptIndex(script_name));}
		QString scriptName(int ix);
		QString scriptCode(int ix); 

		QListWidgetItem* currentScriptItem() {return scriptItem(-1);}
		int currentScriptIndex();

		QString editorScriptCode();

		void setItemModifiedFlag(QListWidgetItem *it, bool b);
		bool hasCurrentItemModifiedFlag();
		//! vymaze skript vcetne itemu v list widgetu, vraci puvodni index vymazaneho itemu
		int removeScript(const QString &script_name);
		virtual bool eventFilter(QObject *obj, QEvent *event);

		void itemSelected(QListWidgetItem *current, QListWidgetItem *previous);
	signals:
		void checkModifiedRequest();
		void evaluatedForConsole(const QString &ret, int status);
	protected slots:
		void on_lstScripts_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous) {itemSelected(current, previous);}
		//void on_txtScript_undoAvailable(bool available);
		//void currentScriptModificationChanged(bool changed);
		void checkModified();
		QString fullScriptName(const QString &script_name);

		void script_new();
		void script_save(bool save_anyway = false);
		void script_saveAs();
		void script_delete();
		
		virtual void evaluateForConsole(const QString &code);
	public:
		virtual QFScriptDriver* scriptDriver() throw(QFException);

		virtual void setCurrentScript(const QString &script_name);
		void setDefaultScriptCode(const QString &code) {f_defaultScriptCode = code;}

		void setOneScriptNoSaveCheck(bool b) {f_oneScriptNoSaveCheck = b;}
		virtual bool canBeClosed(int done_result);

		virtual void save() {script_save();}
		
		QString domain() const {return f_domain;}

		virtual void loadPersistentData();
		virtual void savePersistentData();

		QFConsole* consoleWidget();
	public:
		QFScriptEditorWidget(QFScriptDriver *drv, const QString &domain = QString(), QWidget *parent = NULL);
		virtual ~QFScriptEditorWidget();
};
   
#endif // QFSCRIPTEDITORWIDGET_H

