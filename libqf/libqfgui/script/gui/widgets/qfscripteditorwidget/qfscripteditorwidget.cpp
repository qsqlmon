
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "ui_qfscripteditorwidget.h"
#include "qfscripteditorwidget.h"

#include <qfscriptdriver.h>
#include <qfaction.h>
#include <qfmessage.h>
#include <qfpixmapcache.h>
#include <qfdlgexception.h>

#include <QScriptEngine>
#include <QKeyEvent>

#include <qflogcust.h>

QFScriptEditorWidget::QFScriptEditorWidget(QFScriptDriver * drv, const QString & _domain, QWidget * parent)
	: QFDialogWidget(parent), f_scriptDriver(drv), f_domain(_domain)
{
	f_oneScriptNoSaveCheck = false;

	ui = new Ui::QFScriptEditorWidget;
	ui->setupUi(centralWidget());
	Qf::connectSlotsByName(centralWidget(), this);
	
	connect(consoleWidget(), SIGNAL(evaluateLine(const QString&)), this, SLOT(evaluateForConsole(const QString&)));
	connect(this, SIGNAL(evaluatedForConsole(const QString&, int)), consoleWidget(), SLOT(writeOutput(const QString&, int)));
	
	uiBuilder()->setCreatedActionsParent(this);
	uiBuilder()->open(":/libqfgui/script/gui/widgets/qfscripteditorwidget/qfscripteditorwidget.ui.xml");
	uiBuilder()->connectActions(this);
	f_actions += uiBuilder()->actions();
	
	setCaptionText(tr("Script editor"));

	QString domain_prefix = domain();
	if(!domain_prefix.isEmpty()) domain_prefix += '.';
	foreach(QString s, scriptDriver()->availableScriptNames()) {
		if(!domain_prefix.isEmpty()) {
			if(!s.startsWith(domain_prefix)) continue;
			s = s.mid(domain_prefix.length());
		}
		//QListWidgetItem *it = new QListWidgetItem();
		//it->setText(s.mid(domain_prefix.length());
		//it->setData(FullScriptNameRole, s);
		//ui->lstScripts->addItem(it);
		ui->lstScripts->addItem(s);
	}

	connect(this, SIGNAL(checkModifiedRequest()), this, SLOT(checkModified()), Qt::QueuedConnection);
	ui->txtScript->installEventFilter(this);
}

QFScriptEditorWidget::~QFScriptEditorWidget()
{
	savePersistentData();
	delete ui;
}

QFScriptDriver * QFScriptEditorWidget::scriptDriver() throw( QFException )
{
	if(f_scriptDriver == NULL) QF_EXCEPTION("Script driver is NULL");
	return f_scriptDriver;
}

void QFScriptEditorWidget::setCurrentScript(const QString & _script_name)
{
	qfLogFuncFrame() << _script_name;
	QString script_name = _script_name;
	if(script_name.isEmpty()) script_name = "new";
	QListWidget *lw = ui->lstScripts;
	int ix = scriptIndex(script_name);
	if(ix < 0) {
		lw->addItem(script_name);
		ix = lw->count() - 1;
		setItemModifiedFlag(scriptItem(ix), true);
	}
	lw->setCurrentRow(ix);

	if(f_oneScriptNoSaveCheck) {
		action("script.new")->setEnabled(false);
		action("script.saveAs")->setEnabled(false);
		action("script.delete")->setEnabled(false);
	}
}

QString QFScriptEditorWidget::resolveScriptCode(const QString &script_name)
{
	qfLogFuncFrame() << "script_name:" << script_name;
	//QListWidget *lw = ui->lstScripts;
	QListWidgetItem *it = scriptItem(scriptIndex(script_name));
	QVariant v;
	if(it) v = it->data(ScriptCodeRole);
	if(!v.isNull()) return v.toString();
	QString found_fn;
	QString code = scriptDriver()->loadScriptCode(fullScriptName(script_name), &found_fn);
	qfTrash() << "\t found fn:" << found_fn;
	//qfTrash() << "\t f_defaultScriptCode:" << f_defaultScriptCode;
	if(found_fn.isEmpty()) {
		code = f_defaultScriptCode;
		code.replace("${SCRIPT_NAME}", script_name);
	}
	if(it) it->setData(ScriptCodeRole, code);
	qfTrash() << "\t return:" << code;
	return code;
}

void QFScriptEditorWidget::savePersistentData()
{
	QFDialogWidget::savePersistentData();
}

void QFScriptEditorWidget::loadPersistentData()
{
	QFDialogWidget::loadPersistentData();
	if(!xmlConfigPersistentId().isEmpty()) {
		qfTrash() << QF_FUNC_NAME << xmlConfigPersistentId();
		ui->splitter->setXmlConfigPersistentId(xmlConfigPersistentId() + "/splitter");
	}
}
/*
static const QString default_script_code =
		"// value script example:\n"
		"function ${SCRIPT_NAME}()\n"
		"{\n"
		"    var jmeno = data.value(\"JMENO\");\n"
		"    var prijmeni = data.value(\"PRIJMENI\");\n"
		"    var jmeno_a_prijmeni = prijmeni + \" \" + jmeno;\n"
		"    data.setValue(1, jmeno_a_prijmeni);\n"
		"}";
*/
void QFScriptEditorWidget::script_new()
{
	setCurrentScript(QString());
}

void QFScriptEditorWidget::script_save(bool save_anyway)
{
	qfLogFuncFrame() << "save_anyway:" << save_anyway << "modified:" << hasCurrentItemModifiedFlag();
	if(save_anyway || hasCurrentItemModifiedFlag()) {
		QString code = editorScriptCode();
		QString name = currentScriptName();
		scriptDriver()->saveScriptCode(fullScriptName(name), code);
		/// toto zpusobi, ze ulozeny kod se spusti v script enginu a promenne v nem budou aktualizovane zmenami v kodu
		scriptDriver()->evaluate(code, !Qf::ThrowExc);
		QListWidgetItem *it = currentScriptItem();
		if(it) {
			it->setData(ScriptCodeRole, code);
			it->setIcon(QIcon());
		}
	}
}

int QFScriptEditorWidget::removeScript(const QString &script_name)
{
	QListWidget *lw = ui->lstScripts;
	int ix = scriptIndex(script_name);
	if(ix >= 0) {
		QListWidgetItem *it = lw->takeItem(ix);
		SAFE_DELETE(it);
		scriptDriver()->deleteScriptCode(fullScriptName(script_name));
	}
	return ix;
}

void QFScriptEditorWidget::script_saveAs()
{
	bool ok;
	QString script_name = QInputDialog::getText(this, tr("vlozte text"), tr("Jmeno skriptu:"), QLineEdit::Normal, QString(), &ok);
	if(ok && !script_name.isEmpty()) {
		QListWidget *lw = ui->lstScripts;
		int ix = scriptIndex(script_name);
		if(ix >= 0) {
			if(!QFMessage::askYesNo(this, tr("Skript %1 jiz existuje, prepsat ?").arg(script_name))) return;
		}
		if(ix < 0) {
			/// prejmenuj stavajici item
			QListWidgetItem *it = currentScriptItem();
			if(it) {
				QString old_script_name = it->text();
				it->setText(script_name);
			}
		}
		else {
			/// prepis existujici item a vymaz stavajici
			QListWidgetItem *it = scriptItem(ix);
			if(it) {
				QString old_script_name = currentScriptName();
				QString script_code = editorScriptCode();
				
				it->setText(script_name);
				it->setData(ScriptCodeRole, script_code);
				ui->txtScript->setPlainText(script_code);
				lw->setCurrentRow(ix);
				
				removeScript(old_script_name);
			}
		}
		script_save(true);
	}
}

void QFScriptEditorWidget::script_delete()
{
	QString script_name = currentScriptName();
	if(QFMessage::askYesNo(this, tr("Opravdu smazat skript '%1' ?").arg(script_name), true)) {
		int ix = removeScript(script_name);
		if(ix >= 0) {
			QListWidget *lw = ui->lstScripts;
			while(ix >= lw->count() && ix >= 0) ix--;
			lw->setCurrentRow(ix);
		}
	}
}

bool QFScriptEditorWidget::hasCurrentItemModifiedFlag()
{
	bool ret = false;
	QListWidgetItem *it = currentScriptItem();
	if(it) ret = !it->icon().isNull();
	return ret;
}
/*
bohuzel signal prestane prichazet po zavolani funkce toPlainText
void QFScriptEditorWidget::on_txtScript_undoAvailable(bool available)
{
	qfLogFuncFrame();
	QListWidgetItem *it = currentScriptItem();
	if(it) {
		if(available) {
			it->setIcon(QFPixmapCache::icon("current", "tuzka.png"));
}
		else {
			it->setIcon(QIcon());
}
}
}
*/
bool QFScriptEditorWidget::canBeClosed(int done_result)
{
	if(done_result == QFDialog::Accepted) {
		if(!f_oneScriptNoSaveCheck && hasCurrentItemModifiedFlag()) {
			if(!QFMessage::askYesNo(this, tr("Aktivni dokument obsahuje neulozene zmeny, ulozit ?"))) return false;
			script_save();
		}
	}
	return true;
}

bool QFScriptEditorWidget::eventFilter(QObject *obj, QEvent *event)
{
	/// bohuzel undoAvailable() signal prestane fungovat po volani funkce toPlainText(),
	/// tak to musim takhle obejit
	if(obj == ui->txtScript) {
		//qfTrash() << "\t event type:" << event->type();
		if(event->type() == QEvent::KeyPress) {
			//qfLogFuncFrame() << "object:" << obj->objectName();
			QKeyEvent *key_event = static_cast<QKeyEvent*>(event);
			if(key_event->text().length() > 0) emit checkModifiedRequest();
			//qfTrash() << "\t key: " << keyEvent->key() << "\t modifiers: " << keyEvent->modifiers();
			//qfTrash().noSpace() << "\t text: '" << keyEvent->text() << "' len: " << keyEvent->text().length();
		}
	}
	return false;
}

void QFScriptEditorWidget::checkModified()
{
	bool modified = ui->txtScript->document()->isModified();
	qfLogFuncFrame() << "modified:" << modified;
	if(modified != hasCurrentItemModifiedFlag()) {
		QListWidgetItem *it = currentScriptItem();
		setItemModifiedFlag(it, modified);
	}
}

QString QFScriptEditorWidget::fullScriptName(const QString & script_name)
{
	QString ret = script_name;
	if(!domain().isEmpty()) ret = domain() + '.' + script_name;
	return ret;
}

int QFScriptEditorWidget::currentScriptIndex()
{
	return ui->lstScripts->currentRow();
}

int QFScriptEditorWidget::scriptIndex(const QString & script_name)
{
	QListWidget *lw = ui->lstScripts;
	int ix = -1;
	for(int i=0; i<lw->count(); i++) if(scriptName(i) == script_name) {ix = i; break;}
	return ix;
}

QString QFScriptEditorWidget::scriptName(int ix)
{
	QListWidgetItem *it = scriptItem(ix);
	if(!it) return QString();
	return it->text();
}

QString QFScriptEditorWidget::scriptCode(int ix)
{
	QListWidgetItem *it = scriptItem(ix);
	if(!it) return QString();
	return it->data(ScriptCodeRole).toString();
}

QListWidgetItem * QFScriptEditorWidget::scriptItem(int ix)
{
	QListWidget *lw = ui->lstScripts;
	if(ix < 0) ix = lw->currentRow();
	if(ix < 0 || ix >= lw->count()) return NULL;
	return lw->item(ix);
}

void QFScriptEditorWidget::itemSelected(QListWidgetItem * current, QListWidgetItem * previous)
{
	qfLogFuncFrame();
	if(previous) {
		//qfTrash() << "\t caching script:" << currentScriptName();
		//qfTrash() << "\t code:\n" << currentScriptCode();
		//QListWidgetItem *it = scriptItem(currentScriptIndex());
		previous->setData(ScriptCodeRole, ui->txtScript->toPlainText());
	}
	if(current) {
		QString script_name = current->text();
		QString code = resolveScriptCode(script_name);
		ui->txtScript->setPlainText(code);
	}
	else {
		ui->txtScript->setPlainText(QString());
	}
	qfTrash() << "\t loaded script:" << currentScriptName();
	qfTrash() << "\t code:\n" << currentScriptCode();
}

QString QFScriptEditorWidget::editorScriptCode()
{
	return ui->txtScript->toPlainText();
}
/*
void QFScriptEditorWidget::script_evaluateFirstFunction()
{
	bool dry_run = scriptDriver()->isDryRun();
	try {
		script_save(true);
		scriptDriver()->setDryRun(true);
		QScriptValue sv = scriptDriver()->evaluateFirstFunction(fullScriptName(currentScriptName()));
		QFMessage::information(this, sv.toString());
	}
	catch(QFException &e) {
		QFDlgException::exec(this, e);
	}
	scriptDriver()->setDryRun(dry_run);
}
*/
void QFScriptEditorWidget::setItemModifiedFlag(QListWidgetItem * it, bool b)
{
	if(it) {
		if(b) {
			it->setIcon(QFPixmapCache::icon("current", "tuzka.png"));
		}
		else {
			it->setIcon(QIcon());
		}
	}
}

QFConsole * QFScriptEditorWidget::consoleWidget()
{
	return ui->txtConsole;
}

void QFScriptEditorWidget::evaluateForConsole(const QString & code_line)
{
	QFString fs = code_line.trimmed();
	if(fs[-1] == '\\') {
		wholeCodeToEvaluate += fs.slice(0, -1) + '\n';
	}
	else try {
		wholeCodeToEvaluate += code_line;
		QScriptValue ret = scriptDriver()->evaluate(wholeCodeToEvaluate, !Qf::ThrowExc);
		QString s = ret.toString();
		int status = QFConsole::EvalOk;
		if(ret.isError() || scriptDriver()->scriptEngine()->hasUncaughtException()) {
			status = QFConsole::EvalError;
			s = s + "\n" + wholeCodeToEvaluate;
		}
		emit evaluatedForConsole(s, status);
		wholeCodeToEvaluate = QString();
	}
	catch(QFException &e) {
		emit evaluatedForConsole(e.what(), QFConsole::EvalError);
		wholeCodeToEvaluate = QString();
	}
}



