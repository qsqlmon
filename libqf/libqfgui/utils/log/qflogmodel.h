
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFLOGMODEL_H
#define QFLOGMODEL_H

#include <qfguiglobal.h>

#include <QStandardItemModel>


//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFLogModel : public QStandardItemModel
{
	Q_OBJECT;
	public:
		enum {LevelRole = Qt::UserRole + 1, StackLevelRole};
		enum {ColLevel = 0, ColMessage, ColDomain};
	public:
		virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
		virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
		int minLoggedLevel() const;
	public:
		QFLogModel(QObject *parent = NULL);
		virtual ~QFLogModel();
};

#endif // QFLOGMODEL_H

