
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qflogmodel.h"

#include <qfpixmapcache.h>

#include <limits.h>

#include <qflogcust.h>

QFLogModel::QFLogModel(QObject *parent)
	: QStandardItemModel(parent)
{
	//QStringList sl;
	//sl << tr("level") << tr("domain") << tr("message");
	//setHorizontalHeaderLabels(sl);
}

QFLogModel::~QFLogModel()
{
}

QVariant QFLogModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant ret;
	if(orientation == Qt::Horizontal) {
		if(role == Qt::DisplayRole) {
			switch(section) {
				case ColLevel: ret = tr("level"); break;
				case ColDomain: ret = tr("domain"); break;
				case ColMessage: ret = tr("message"); break;
			}
		}
	}
	if(!ret.isValid()) ret = QStandardItemModel::headerData(section, orientation, role);
	return ret;
}

QVariant QFLogModel::data(const QModelIndex &ix, int role) const
{
	QVariant ret = QStandardItemModel::data(ix, role);
	if(ix.column() == ColLevel) {
		if(role ==  Qt::DisplayRole) {
			QString level_str;
			int level = QStandardItemModel::data(ix, LevelRole).toInt();
			if(level > 0 && level <= QFLog::LOG_TRASH) { level_str = QFLogDevice::levelNames[level-1]; }
			else { level_str = QString::number(level); }
			ret = level_str;
		}
		else if(role == Qt::DecorationRole) {
			static QIcon ico_red = QFPixmapCache::icon("light-red");
			static QIcon ico_yellow = QFPixmapCache::icon("light-yellow");
			static QIcon ico_cyan = QFPixmapCache::icon("light-cyan");
			static QIcon ico_blind = QFPixmapCache::icon("light-blind");
			int level = QStandardItemModel::data(ix, LevelRole).toInt();
			if(level == QFLog::LOG_INF) ret = ico_yellow;
			else if(level == QFLog::LOG_WARN) ret = ico_cyan;
			else if(level < QFLog::LOG_WARN) ret = ico_red;
			else ret = ico_blind;
		}
	}
	return ret;
}

int QFLogModel::minLoggedLevel() const
{
	QModelIndex ix = index(0, 0);
	int ret = INT_MAX;
	if(ix.isValid()) {
		QVariant v = data(ix, LevelRole);
		if(v.isValid()) ret = v.toInt();
	}
	return ret;
}
