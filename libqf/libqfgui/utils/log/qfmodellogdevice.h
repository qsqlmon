
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFMODELLOGDEVICE_H
#define QFMODELLOGDEVICE_H

#include <qfguiglobal.h>

#include <qflog.h>

class QFLogModel;
class QStandardItem;

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFModelLogDevice : public QFLogDevice
{
	protected:
		QFLogModel *fModel;
		QStandardItem *recentlyLoggedItem;
		//int recentlyLoggedStackLevel;
		virtual void push();
		virtual void pop();
	public:
		QFLogModel* model();
		void clearLog();
		virtual bool checkLogPermisions(const QString &_domain, int _level);
		virtual void log(const QString &domain, int level, const QString &message, int flags = 0);
	public:
		QFModelLogDevice();
		virtual ~QFModelLogDevice();
};

#endif // QFMODELLOGDEVICE_H

