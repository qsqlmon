
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "ui_qflogviewwidget.h"
#include "qflogviewwidget.h"
#include "qflogmodel.h"

#include <qfheaderview.h>

#include <qflogcust.h>

//==================================================
//                             QFLogViewWidgetProxyModel
//==================================================
QFLogViewWidgetProxyModel::QFLogViewWidgetProxyModel(QObject *parent)
	: QSortFilterProxyModel(parent)
{
	setFilterRole(QFLogModel::LevelRole);
	setFilterTreshold(INT_MAX);
}

bool QFLogViewWidgetProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
	//qfTrash() << QF_FUNC_NAME;
	//return true;
	QModelIndex ix = sourceModel()->index(sourceRow, QFLogModel::ColLevel, sourceParent);
	return sourceModel()->data(ix, filterRole()).toInt() <= filterTreshold();
}

//==================================================
//                             QFLogViewWidget
//==================================================
QFLogViewWidget::QFLogViewWidget(QWidget *parent)
	: QFDialogWidget(parent)
{
	proxy = new QFLogViewWidgetProxyModel(this);

	ui = new Ui::QFLogViewWidget;
	ui->setupUi(centralWidget());
	Qf::connectSlotsByName(centralWidget(), this);

	ui->treeLog->setModel(proxy);
	//ui->tblLog->verticalHeader()->setDefaultSectionSize(15);
	//ui->treeLog->verticalHeader()->setResizeMode(QHeaderView::ResizeToContents);
}

QFLogViewWidget::~QFLogViewWidget()
{
	savePersistentData();
	delete ui;
}

void QFLogViewWidget::setModel(QAbstractItemModel *m)
{
	proxy->setSourceModel(m);
	ui->treeLog->header()->resizeSections(QHeaderView::ResizeToContents);
	//ui->tblLog->resizeRowsToContents();
}

void QFLogViewWidget::on_lstLevel_currentIndexChanged(int ix)
{
	qfTrash() << QF_FUNC_NAME;
	if(ix <= 0) proxy->setFilterTreshold(INT_MAX);
	else proxy->setFilterTreshold(ix);
	ui->treeLog->reset();
	ui->treeLog->setCurrentIndex(ui->treeLog->model()->index(0, 0, QModelIndex()));
	on_btTreeExpand_clicked();
}

void QFLogViewWidget::on_btTreeExpand_clicked()
{
	ui->treeLog->setExpandedRecursively(ui->treeLog->currentIndex(), true);
	ui->treeLog->header()->resizeSections(QHeaderView::ResizeToContents);
}

void QFLogViewWidget::on_btTreeColapse_clicked()
{
	ui->treeLog->setExpandedRecursively(ui->treeLog->currentIndex(), false);
}

int QFLogViewWidget::filterTreshold()
{
	return ui->lstLevel->currentIndex();
}

void QFLogViewWidget::setFilterTreshold(int level)
{
	//proxy->setFilterTreshold(level);
	if(level >= ui->lstLevel->count()) level = 0;
	ui->lstLevel->setCurrentIndex(level);
}

void  QFLogViewWidget::savePersistentData()
{
	if(!xmlConfigPersistentId().isEmpty()) {
		qfTrash() << QF_FUNC_NAME << xmlConfigPersistentId();
		//QFXmlConfigElement el = createPersistentPath();
		/// save log level filter
		setPersistentValue("filtertreshold", filterTreshold());
	}
}

void  QFLogViewWidget::loadPersistentData()
{
	if(!xmlConfigPersistentId().isEmpty()) {
		qfTrash() << QF_FUNC_NAME << xmlConfigPersistentId();
		//QFXmlConfigElement el = persistentPath();
		/// load log level filter
		int lev = persistentValue("filtertreshold", INT_MAX).toInt();
		qfTrash() << "\t level:" << lev;
		setFilterTreshold(lev);
	}
}
