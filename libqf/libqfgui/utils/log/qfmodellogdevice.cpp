
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfmodellogdevice.h"
#include "qflogmodel.h"

#include <qf.h>
#include <qflogcust.h>

QFModelLogDevice::QFModelLogDevice()
	: QFLogDevice(), fModel(NULL), recentlyLoggedItem(NULL)
{
	setLogTreshold(INT_MAX);
}

QFModelLogDevice::~QFModelLogDevice()
{
	SAFE_DELETE(fModel);
}

void QFModelLogDevice::push()
{
	qfTrash() << QF_FUNC_NAME << stackLevel;
	QFLogDevice::push();
	qfTrash() << "\tRETURN" << stackLevel;
}

void QFModelLogDevice::pop()
{
	qfTrash() << QF_FUNC_NAME << stackLevel;
	QFLogDevice::pop();
	qfTrash() << "\tRETURN" << stackLevel;
}

QFLogModel* QFModelLogDevice::model()
{
	if(!fModel) fModel = new QFLogModel();
	return fModel;
}

void QFModelLogDevice::clearLog()
{
	if(fModel) fModel->clear();
	recentlyLoggedItem = NULL;
}

void QFModelLogDevice::log(const QString &domain, int level, const QString &message, int flags)
{
	Q_UNUSED(flags);
	qfLogFuncFrame() << "level:" << level << "flags:" << flags << "stackLevel:" << stackLevel << "\n\tmessage:" << message;
	QList<QStandardItem *> items;
	QStandardItem *it = NULL;

	it = new QStandardItem();
	//it->setTextAlignment(Qt::AlignRight);
	it->setData(level, QFLogModel::LevelRole);
	if(stackLevel > 0) it->setData(stackLevel, QFLogModel::StackLevelRole);
	items << it;
	QStandardItem *it1 = it;

	it = new QStandardItem(message);
	items << it;

	it = new QStandardItem(domain);
	items << it;

	int recently_logged_stack_level = 0;
	if(recentlyLoggedItem) recently_logged_stack_level = recentlyLoggedItem->data(QFLogModel::StackLevelRole).toInt();
	qfTrash() << "\t stackLevel:" << stackLevel << "recently_logged_stack_level:" << recently_logged_stack_level;
	if(recentlyLoggedItem == NULL) {
		model()->appendRow(items);
	}
	else {
		QStandardItem *pi = recentlyLoggedItem;
		if(stackLevel > recently_logged_stack_level) {
			/// insert into
		}
		else if(stackLevel == recently_logged_stack_level) {
			/// insert sibling
			pi = pi->parent();
		}
		else {
			/// insert sibling to parent item with same stack level
			while(pi && pi->data(QFLogModel::StackLevelRole).toInt() > stackLevel) {
				pi = pi->parent();
			}
			if(pi) pi = pi->parent();
		}
		if(pi) pi->appendRow(items);
		else model()->appendRow(items);

		///nastav rodicum log level ne mensi, nez ma pridavany item
		//int stack_level = it1->data(QFLogModel::StackLevelRole).toInt();
		for(pi=it1->parent(); pi; pi=pi->parent()) {
			int parent_level = pi->data(QFLogModel::LevelRole).toInt();
			qfTrash() << "\t parent level:" << parent_level << "my level:" << level;
			if(parent_level > level) {
				pi->setData(level, QFLogModel::LevelRole);
				qfTrash() << "\t changing parent level to:" << level;
			}
			else break;
		}
	}
	recentlyLoggedItem = it1;
}

bool QFModelLogDevice::checkLogPermisions(const QString & _domain, int _level)
{
	//fprintf(stderr, "QFLogDevice::checkLogPermisions() domain: %s level: %i\n", qPrintable(_domain), _level);
	bool ret = QFLogDevice::checkLogPermisions(_domain, _level);
	//fprintf(stderr, "\t return: %i\n", ret);
	return ret;
}
