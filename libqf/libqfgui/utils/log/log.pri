message(including module '$$PWD')

HEADERS +=      \
	$$PWD/qflogmodel.h    \
	$$PWD/qfmodellogdevice.h   \
	$$PWD/qflogviewwidget.h  \

SOURCES +=      \
	$$PWD/qflogmodel.cpp    \
	$$PWD/qfmodellogdevice.cpp   \
	$$PWD/qflogviewwidget.cpp  \

FORMS +=    \
	$$PWD/qflogviewwidget.ui  \


