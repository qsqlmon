
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFLOGVIEWWIDGET_H
#define QFLOGVIEWWIDGET_H

#include <qfdialogwidget.h>

#include <qfguiglobal.h>

#include <QSortFilterProxyModel>

namespace Ui {class QFLogViewWidget;};
//class QModelIndex;

class QFLogViewWidgetProxyModel : public QSortFilterProxyModel
{
	Q_OBJECT;
	protected:
		int fFilterTreshold;
	protected:
		virtual bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const;
	public:
		int filterTreshold() const {return fFilterTreshold;}
		void setFilterTreshold(int tr)
		{
			fFilterTreshold = tr;
			invalidateFilter();
		}
	public:
		QFLogViewWidgetProxyModel(QObject *parent);
};

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFLogViewWidget : public QFDialogWidget
{
	Q_OBJECT
	private:
		Ui::QFLogViewWidget *ui;
	protected:
		QFLogViewWidgetProxyModel *proxy;
	protected:
		virtual void  savePersistentData();
		virtual void  loadPersistentData();
	protected slots:
		void on_lstLevel_currentIndexChanged(int ix);
		void on_btTreeExpand_clicked();
		void on_btTreeColapse_clicked();
	public:
		void setModel(QAbstractItemModel *m);
		void setFilterTreshold(int level);
		int filterTreshold();
	public:
		QFLogViewWidget(QWidget *parent = NULL);
		virtual ~QFLogViewWidget();
};

#endif // QFLOGVIEWWIDGET_H

