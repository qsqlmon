
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#include "qfhtmlviewutils.h"

#include <qflogcust.h>
#include <qfapplication.h>
#include <qffileutils.h>
#include <qfhtmlutils.h>
#include <qfdlghtmlview.h>
#include <qfdlgexception.h>

#include <QMessageBox>
#include <QPushButton>
#include <QDesktopServices>
#include <QUrl>

QFHtmlViewUtils::Result QFHtmlViewUtils::showOrSaveHtml(const QString &content, const QString &file_name, const QString &parent_persistent_id)
{
	qfLogFuncFrame();
	//Result ret = Cancel;
	QWidget *active_window = qApp->activeWindow();
	QMessageBox msgbox(QMessageBox::Question, tr("Dotaz"), tr("Jak chcete sestavu zobrazit?"),
					   QMessageBox::Save | QMessageBox::Cancel, active_window);
	//int ret = QMessageBox::question(this, tr("Dotaz"), tr("Chcete sestavu zobrazit nebo ulozit?"),
	//								QMessageBox::Open | QMessageBox::Save | QMessageBox::Cancel, QMessageBox::Open);
	//tr("Zobrazit"), tr("Ulozit"), tr("Zrusit"), 0, 2);
	QPushButton *btShow = msgbox.addButton(tr("Zobrazit"), QMessageBox::AcceptRole);
	QPushButton *btShowExternal = msgbox.addButton(tr("Zobrazit v externim prohlizeci"), QMessageBox::AcceptRole);
	msgbox.setDefaultButton(btShow);
	int ret = msgbox.exec();
	if(ret == QMessageBox::Cancel) return Cancel;
	try {
		if(ret == QMessageBox::Save) {
			QString fn = qfApp()->getSaveFileName(active_window, tr("Ulozit jako ..."),
					file_name, tr("soubory HTML (*.html)"));
			if(fn.isEmpty()) return Cancel;
			QFHtmlUtils::saveHtmlWithResources(content, QString(), fn);
			return Save;
		}
		else if(msgbox.clickedButton() == btShow) {
			if(!f_useWebKit) {
				emit progressValue(-1, tr("Pripravuje se zobrazeni sestavy"));
				QFDlgHtmlView d(active_window);
				QString persistent_id = "dlgHtmlView";
				if(!parent_persistent_id.isEmpty()) persistent_id += parent_persistent_id + "/" + persistent_id;
				d.setXmlConfigPersistentId(persistent_id);
				d.loadPersistentData();
				d.setHtmlText(content, file_name);
				emit progressValue(-1, tr("Zobrazuje se sestava"));
				d.exec();
				emit progressValue(-1);
				d.savePersistentData();
			}
			return Show;
		}
		else if(msgbox.clickedButton() == btShowExternal) {
			QString fn = QFFileUtils::joinPath(qfApp()->tempDir(), "sasd_temp.html");
			QFHtmlUtils::saveHtmlWithResources(content, QString(), fn);
			QDesktopServices::openUrl(fn);
			return ShowExternal;
		}
	}
	catch(QFException &e) {
		QFDlgException::exec(this, e);
	}
	return Cancel;
}



