
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFHTMLVIEWUTILS_H
#define QFHTMLVIEWUTILS_H

#include <qfguiglobal.h>
#include <qfdom.h>
#include <qfexception.h>

//! TODO: write class documentation.
class QFGUI_DECL_EXPORT QFHtmlViewUtils : public QObject
{
	Q_OBJECT;
	protected:
		bool f_useWebKit;
	public:
		enum Result {Save, Show, ShowExternal, Cancel};
		static const bool UseWebKit = true;
	signals:
		void progressValue(double, const QString& = QString());
	public:
		Result showOrSaveHtml(const QString &content, const QString &file_name, const QString &parent_persistent_id = QString());
	public:
		/// pokud je use_webkit a nekdo klikne na show, nezobrazi na show funkce showOrSaveHtml nic a pouze vrati Result::Show
		QFHtmlViewUtils(bool use_webkit, QObject *parent = NULL) : QObject(parent), f_useWebKit(use_webkit) {}
		//virtual ~QFHtmlUtils();
};

#endif // QFHTMLVIEWUTILS_H

