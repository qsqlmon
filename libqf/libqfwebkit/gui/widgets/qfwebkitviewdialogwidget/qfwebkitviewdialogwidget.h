
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef QFWEBKITVIEWDIALOGWIDGET_H
#define QFWEBKITVIEWDIALOGWIDGET_H

#include <qfwebkitglobal.h>

#include <qfdialogwidget.h>

namespace Ui {class QFWebKitViewDialogWidget;};

//! TODO: write class documentation.
class QFWEBKIT_DECL_EXPORT QFWebKitViewDialogWidget : public QFDialogWidget
{
	Q_OBJECT;
	private:
		Ui::QFWebKitViewDialogWidget *ui;
		QFUiBuilder f_uiBuilder;
	protected:
		QString f_saveFileName;
	public:
		virtual QFUiBuilder* uiBuilder() {return &f_uiBuilder;}

		void setHtml(const QString &content, const QUrl & base_url, const QString & save_file_name);
		void load(const QUrl &url, const QString & save_file_name);
	public slots:
		void page_save();
		void page_forward();
		void page_back();
		void page_reload();
	public:
		QFWebKitViewDialogWidget(QWidget *parent = NULL);
		virtual ~QFWebKitViewDialogWidget();
};

#endif // QFWEBKITVIEWDIALOGWIDGET_H

