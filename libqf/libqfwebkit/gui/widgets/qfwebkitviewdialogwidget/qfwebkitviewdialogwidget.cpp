
//
// Author: Frantisek Vacek <fanda.vacek@volny.cz>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//

#include "ui_qfwebkitviewdialogwidget.h"
#include "qfwebkitviewdialogwidget.h"

#include <qfapplication.h>
#include <qffileutils.h>
#include <qfhtmlutils.h>
#include <qfaction.h>

#include <QWebFrame>

#include <qflogcust.h>

QFWebKitViewDialogWidget::QFWebKitViewDialogWidget(QWidget *parent)
	: QFDialogWidget(parent)
{
	ui = new Ui::QFWebKitViewDialogWidget;
	ui->setupUi(centralWidget());
	Qf::connectSlotsByName(centralWidget(), this);

	//f_uiBuilder.setPrevUiBuilder(KZDataFormDialogWidget::uiBuilder());
	f_uiBuilder.setCreatedActionsParent(this);
	f_uiBuilder.open(":/libqfwebkit/gui/widgets/qfwebkitviewdialogwidget/qfwebkitviewdialogwidget.ui.xml");
	f_uiBuilder.connectActions(this);
	f_actions += f_uiBuilder.actions();

	QWebView *v = ui->webView;
	action("page.reload")->setIcon(v->pageAction(QWebPage::Reload)->icon());
	action("page.forward")->setIcon(v->pageAction(QWebPage::Forward)->icon());
	action("page.back")->setIcon(v->pageAction(QWebPage::Back)->icon());
}

QFWebKitViewDialogWidget::~QFWebKitViewDialogWidget()
{
	delete ui;
}

void QFWebKitViewDialogWidget::setHtml(const QString & content, const QUrl & base_url, const QString & save_file_name)
{
	QWebView *v = ui->webView;
	f_saveFileName = save_file_name;
	v->setHtml(content, base_url);
}

void QFWebKitViewDialogWidget::load(const QUrl & url, const QString & save_file_name)
{
	QWebView *v = ui->webView;
	f_saveFileName = save_file_name;
	v->load(url);
}

void QFWebKitViewDialogWidget::page_save()
{
	qfLogFuncFrame();
	QString fn = f_saveFileName;
	if(!fn.endsWith(".html")) fn += ".html";
	fn = qfApp()->getSaveFileName(this, tr("Ulozit jako ..."),
		   fn, tr("soubory Html (*.html)"));
	if(fn.isEmpty()) return;
	if(fn.indexOf('.') < 0) fn += ".html";
	qfTrash() << "saving to file" << fn;
	QWebView *v = ui->webView;
	QWebFrame *frm = v->page()->mainFrame();
	QString content;
	if(frm) content = frm->toHtml();
	QString location = v->url().path();
	QFHtmlUtils::saveHtmlWithResources(content, QFFileUtils::path(location), fn);
}

void QFWebKitViewDialogWidget::page_forward()
{
	QWebView *v = ui->webView;
	v->triggerPageAction(QWebPage::Forward);
}

void QFWebKitViewDialogWidget::page_back()
{
	QWebView *v = ui->webView;
	v->triggerPageAction(QWebPage::Back);
}

void QFWebKitViewDialogWidget::page_reload()
{
	QWebView *v = ui->webView;
	v->triggerPageAction(QWebPage::Reload);
}
