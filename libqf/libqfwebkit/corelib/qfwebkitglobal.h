#ifndef QFWEBKITGLOBAL_H
#define	QFWEBKITGLOBAL_H

#include <qglobal.h>

/// Declaration of macros required for exporting symbols
/// into shared libraries
#if defined(QFWEBKIT_BUILD_DLL)
#  define QFWEBKIT_DECL_EXPORT Q_DECL_EXPORT
#else
#  define QFWEBKIT_DECL_EXPORT Q_DECL_IMPORT
#endif

#endif // QFWEBKITGLOBAL_H
