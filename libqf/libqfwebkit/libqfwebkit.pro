#CONFIG += qmake_debug
MY_SUBPROJECT = libqfwebkit
!exists(go_to_top.pri) :error("cann't find go_to_top.pri")
include(go_to_top.pri)

TEMPLATE = lib
win32:TARGET = $$MY_SUBPROJECT
unix:TARGET = $$replace(MY_SUBPROJECT, lib, )
TARGET = $${TARGET}$$QF_LIBRARY_DEBUG_EXT
message(Target: $$TARGET)

QT += webkit xml sql

CONFIG += qt dll
#staticlib
#dll

win32:DESTDIR = $$MY_BUILD_DIR/bin
unix:DESTDIR = $$MY_BUILD_DIR/lib
#DLLDESTDIR = $$MY_BUILD_DIR/bin
INCLUDEPATH += $$PWD/../include

DEFINES +=    \
    QFWEBKIT_BUILD_DLL

# Use Precompiled headers (PCH)
#CONFIG += precompile_header
#PRECOMPILED_HEADER  = precompiled.h
precompile_header:!isEmpty(PRECOMPILED_HEADER) {
	message("using precompiled_headers")
}

CONFIG += hide_symbols

win32:   {
  LIBS +=     \
	-L$$MY_BUILD_DIR/bin
}
else {
  LIBS +=     \
	-L$$MY_BUILD_DIR/lib
}

LIBS +=     \
	-lqfgui$$QF_LIBRARY_DEBUG_EXT \
	-lqfcore$$QF_LIBRARY_DEBUG_EXT \

message(LIBS: $$LIBS)

#corelib because the core is reserved for the Linux core dump
include(corelib/corelib.pri)
include(gui/gui.pri)

RESOURCES +=    \
#	lib$${MY_SUBPROJECT}.qrc

TRANSLATIONS    =                          \
	lib$${MY_SUBPROJECT}.cs_CZ.ts                          \
#	lib$${MY_SUBPROJECT}.fr_FR.ts                          \

