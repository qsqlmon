MY_TOP_DIR = $$PWD
# nemuzu pouzivat absolutni cesty, protoze qmake generuje
# do Makefile mix relativnich a absolutnich a i kdyz ukazuji na stejny
# soubor, nmake si s tim neporadi
MY_BUILD_DIR = $$MY_PATH_TO_TOP/_build

OBJECTS_DIR = $${MY_BUILD_DIR}/$${MY_SUBPROJECT}
MOC_DIR = $$OBJECTS_DIR/moc
UI_DIR = $$OBJECTS_DIR/ui
RCC_DIR = $$OBJECTS_DIR/rcc

#QT_DIR = /Users/fanda/Programs/Qt4/Current
#INCLUDEPATH += $$QTDIR/include
#message(QT_BUILD_TREE: $$QT_BUILD_TREE)
#message(QT_INSTALL_PREFIX: $$[QT_INSTALL_PREFIX])
count(QT_BUILD_TREE, 0) {
	QT_BUILD_TREE = $$[QT_INSTALL_PREFIX]
	count(QT_BUILD_TREE, 0) {
		unix: QT_BUILD_TREE = /usr/lib/qt4
		else: win32 {
			win32-g++: QT_BUILD_TREE = d:/app/qt4-mingw
			else: QT_BUILD_TREE = d:/app/qt4-msvc
		}
	}
}
message(QT_BUILD_TREE: $$QT_BUILD_TREE)

DEFINES += QF_PATCH
#pokud jsou soucasne uvedeny debug a release, vitezi ten, ktery byl do CONFIGu pridan posledni
CONFIG += debug
#CONFIG += release
win32:CONFIG += release
#win32:!win32-g++: CONFIG += qf_dll
CONFIG(debug, debug|release) {
	message(Building in debug mode.)
	QF_LIBRARY_DEBUG_EXT = _debug
}
else {
	message(Building in release mode. Ignoring debug if it is available.)
}
#message(top dir config: $$CONFIG)

qmake_debug {
	message(QT_DIR: $$QT_DIR)
	message(QT_BUILD_TREE: $$QT_BUILD_TREE)
	message(MY_PATH_TO_TOP: $$MY_PATH_TO_TOP)
	message(MY_TOP_DIR: $$MY_TOP_DIR)
	message(MY_BUILD_DIR: $$MY_BUILD_DIR)
	message(OBJECTS_DIR: $$OBJECTS_DIR)
	message(MOC_DIR: $$MOC_DIR)
	message(UI_DIR: $$UI_DIR)
}

